# DMS (Document Management System)
###### Multisistemas DMS - Basado en [SeedDMS](https://www.seeddms.org/index.php?id=2)

**Requerimientos**

- PHP 5.6 o superior (se recomienda utilizar 7.2 para mejor rendimiento)
- MySQL 5 o superior
- Un servidor web configurado (Se recomienda Apache Server o Nginx)
- Git como gestor de versionamiento
- La instalación de PHP debe tener soporte para: pdo_mysql or pdo_sqlite, php_gd2, php_mbstring
- Programas para convertir archivos en texto plano para indexado: pdftotext (requerido)
- Para corroborar que pdftotext se encuentra instalado ejecutar el comando: dpkg -s poppler-utils
- Si las librerías no se encuentran, entonces instalar utilizando el comando:

> sudo apt-get install poppler-utils

- Composer como gestor de dependencias de PHP.

**Instalación**

- Paso 1: Clonar el repositorio del DMS

El DMS puede ser clonado desde su repositorio en la nube utilizando git en la carpeta asignada para la instalación del sistema, comúnmente ubicada en sistemas LAMP (Linux, Apache, MySQL, PHP) en la ruta < /var/www/html/ >. Para ello utilizar el comando:

> git clone git@<repositorios-git.com:user>/dms.git

- Paso 2: Crear carpeta data

Dentro de la carpeta “data” se almacenan todos los archivos correspondientes a los documentos en el DMS. Fuera de la carpeta htdocs crear la carpeta data, y dentro de la misma crear otras tres carpetas: “cache”, “lucene” y “staging”.

- Paso 3: Crear base de datos y usuario

El motor de base de datos a utilizar el MySQL, por lo tanto se deberá crear una BD y un usuario para la misma a través de la terminal.

- Paso 4: Instalar las librerías de composer

El DMS utiliza varias librerías las cuales no se encuentran agregadas en el repositorio. Para instalar dichas librerías bastará con colocarnos dentro de la carpeta sgaip, raíz del sistema, y ejecutar el comando:

> composer install

- Paso 5: Ejecutar la instalación del DMS

Ejecutar el instalador web ingresando a la URL del DMS. Seguir las indicaciones del instalador y configurar los campos requeridos con las rutas correspondientes a nuestra instalación en el servidor.
