<?php
/**
 * Implementation of the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2010, Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Include some files
 */
require_once("inc.AccessUtils.php");
require_once("inc.FileUtils.php");
require_once("inc.ClassAccess.php");
require_once("inc.ClassObject.php");
require_once("inc.ClassFolder.php");
require_once("inc.ClassDocument.php");
require_once("inc.ClassDocumentType.php");
require_once("inc.ClassGroup.php");
require_once("inc.ClassUser.php");
require_once("inc.ClassAxe.php");
require_once("inc.ClassInstrument.php");
require_once("inc.ClassInstrumentVar.php");
require_once("inc.ClassInstrumentEval.php");
require_once("inc.ClassKeywords.php");
require_once("inc.ClassNotification.php");
require_once("inc.ClassNotificationSchedule.php");
require_once("inc.ClassAttribute.php");
require_once("inc.ClassAttributeCategory.php");
require_once("inc.ClassReport.php");
require_once("inc.ClassTypeCategory.php");
require_once("inc.ClassDepartment.php");

/**
 * Class to represent the complete document management system.
 * This class is needed to do most of the dms operations. It needs
 * an instance of {@link SeedDMS_Core_DatabaseAccess} to access the
 * underlying database. Many methods are factory functions which create
 * objects representing the entities in the dms, like folders, documents,
 * users, or groups.
 *
 * Each dms has its own database for meta data and a data store for document
 * content. Both must be specified when creating a new instance of this class.
 * All folders and documents are organized in a hierachy like
 * a regular file system starting with a {@link $rootFolderID}
 *
 * This class does not enforce any access rights on documents and folders
 * by design. It is up to the calling application to use the methods
 * {@link SeedDMS_Core_Folder::getAccessMode()} and
 * {@link SeedDMS_Core_Document::getAccessMode()} and interpret them as desired.
 * Though, there are two convenient functions to filter a list of
 * documents/folders for which users have access rights for. See
 * {@link filterAccess()}
 * and {@link filterUsersByAccess()}
 *
 * Though, this class has a method to set the currently logged in user
 * ({@link setUser}), it does not have to be called, because
 * there is currently no class within the SeedDMS core which needs the logged
 * in user. {@link SeedDMS_Core_DMS} itself does not do any user authentication.
 * It is up to the application using this class.
 *
 * <code>
 * <?php
 * include("inc/inc.ClassDMS.php");
 * $db = new SeedDMS_Core_DatabaseAccess($type, $hostname, $user, $passwd, $name);
 * $db->connect() or die ("Could not connect to db-server");
 * $dms = new SeedDMS_Core_DMS($db, $contentDir);
 * $dms->setRootFolderID(1);
 * ...
 * ?>
 * </code>
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2010, Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_DMS {
	/**
	 * @var object $db reference to database object. This must be an instance
	 *      of {@link SeedDMS_Core_DatabaseAccess}.
	 * @access protected
	 */
	protected $db;

	/**
	 * @var array $classnames list of classnames for objects being instanciate
	 *      by the dms
	 * @access protected
	 */
	protected $classnames;

	/**
	 * @var object $user reference to currently logged in user. This must be
	 *      an instance of {@link SeedDMS_Core_User}. This variable is currently not
	 *      used. It is set by {@link setUser}.
	 * @access private
	 */
	private $user;

	/**
	 * @var string $contentDir location in the file system where all the
	 *      document data is located. This should be an absolute path.
	 * @access public
	 */
	public $contentDir;

	/**
	 * @var integer $rootFolderID ID of root folder
	 * @access public
	 */
	public $rootFolderID;

	/**
	 * @var integer $maxDirID maximum number of documents per folder on the
	 *      filesystem. If this variable is set to a value != 0, the content
	 *      directory will have a two level hierarchy for document storage.
	 * @access public
	 */
	public $maxDirID;

	/**
	 * @var boolean $enableConverting set to true if conversion of content
	 *      is desired
	 * @access public
	 */
	public $enableConverting;

	/**
	 * @var boolean $forceRename use renameFile() instead of copyFile() when
	 *      copying the document content into the data store. The default is
	 *      to copy the file. This parameter only affects the methods
	 *      SeedDMS_Core_Document::addDocument() and
	 *      SeedDMS_Core_Document::addDocumentFile(). Setting this to true
	 *      may save resources especially for large files.
	 * @access public
	 */
	public $forceRename;

	/**
	 * @var array $convertFileTypes list of files types that shall be converted
	 * @access public
	 */
	public $convertFileTypes;

	/**
	 * @var array $viewOnlineFileTypes list of files types that can be viewed
	 *      online
	 * @access public
	 */
	public $viewOnlineFileTypes;

	/**
	 * @var array $noReadForStatus list of status without read right
	 *      online.
	 * @access public
	 */
	public $noReadForStatus;

	/**
	 * @var string $version version of pear package
	 * @access public
	 */
	public $version;

	/**
	 * @var array $callbacks list of methods called when certain operations,
	 * like removing a document, are executed. Set a callback with
	 * {@link SeedDMS_Core_DMS::setCallback()}.
	 * The key of the array is the internal callback function name. Each
	 * array element is an array with two elements: the function name
	 * and the parameter passed to the function.
	 *
	 * Currently implemented callbacks are:
	 *
	 * onPreRemoveDocument($user_param, $document);
	 *   called before deleting a document. If this function returns false
	 *   the document will not be deleted.
	 *
	 * onPostRemoveDocument($user_param, $document_id);
	 *   called after the successful deletion of a document.
	 *
	 * @access public
	 */
	public $callbacks;


	/**
	 * Checks if two objects are equal by comparing their IDs
	 *
	 * The regular php check done by '==' compares all attributes of
	 * two objects, which is often not required. The method will first check
	 * if the objects are instances of the same class and than if they
	 * have the same id.
	 *
	 * @param object $object1 first object to be compared
	 * @param object $object2 second object to be compared
	 * @return boolean true if objects are equal, otherwise false
	 */
	static function checkIfEqual($object1, $object2) { /* {{{ */
		if(get_class($object1) != get_class($object2))
			return false;
		if($object1->getID() != $object2->getID())
			return false;
		return true;
	} /* }}} */

	/**
	 * Checks if a list of objects contains a single object by comparing their IDs
	 *
	 * This function is only applicable on list containing objects which have
	 * a method getID() because it is used to check if two objects are equal.
	 * The regular php check on objects done by '==' compares all attributes of
	 * two objects, which isn't required. The method will first check
	 * if the objects are instances of the same class.
	 *
	 * The result of the function can be 0 which happens if the first element
	 * of an indexed array matches.
	 *
	 * @param object $object1 object to look for (needle)
	 * @param array $list list of objects (haystack)
	 * @return boolean/integer index in array if object was found, otherwise false
	 */
	static function inList($object, $list) { /* {{{ */
		foreach($list as $i=>$item) {
			if(get_class($item) == get_class($object) && $item->getID() == $object->getID())
				return $i;
		}
		return false;
	} /* }}} */

	/**
	 * Checks if date conforms to a given format
	 *
	 * @param string $date date to be checked
	 * @param string $format format of date. Will default to 'Y-m-d H:i:s' if
	 * format is not given.
	 * @return boolean true if date is in propper format, otherwise false
	 */
	static function checkDate($date, $format='Y-m-d H:i:s') { /* {{{ */
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	} /* }}} */

	/**
	 * Filter out objects which are not accessible in a given mode by a user.
	 *
	 * The list of objects to be checked can be of any class, but has to have
	 * a method getAccessMode($user) which checks if the given user has at
	 * least access rights to the object as passed in $minMode.
	 *
	 * @param array $objArr list of objects (either documents or folders)
	 * @param object $user user for which access is checked
	 * @param integer $minMode minimum access mode required (M_ANY, M_NONE,
	 *        M_READ, M_READWRITE, M_ALL)
	 * @return array filtered list of objects
	 */
	static function filterAccess($objArr, $user, $minMode) { /* {{{ */
		if (!is_array($objArr)) {
			return array();
		}
		$newArr = array();
		foreach ($objArr as $obj) {
			if ($obj->getAccessMode($user) >= $minMode)
				array_push($newArr, $obj);
		}
		return $newArr;
	} /* }}} */

	/**
	 * Filter out users which cannot access an object in a given mode.
	 *
	 * The list of users to be checked can be of any class, but has to have
	 * a method getAccessMode($user) which checks if a user has at least
	 * access rights as passed in $minMode.
	 *
	 * @param object $obj object that shall be accessed
	 * @param array $users list of users which are to check for sufficient
	 *        access rights
	 * @param integer $minMode minimum access right on the object for each user
	 *        (M_ANY, M_NONE, M_READ, M_READWRITE, M_ALL)
	 * @return array filtered list of users
	 */
	static function filterUsersByAccess($obj, $users, $minMode) { /* {{{ */
		$newArr = array();
		foreach ($users as $currUser) {
			if ($obj->getAccessMode($currUser) >= $minMode)
				array_push($newArr, $currUser);
		}
		return $newArr;
	} /* }}} */

	/**
	 * Filter out document links which can not be accessed by a given user
	 *
	 * Returns a filtered list of links which are accessible by the
	 * given user. A link is only accessible, if it is publically visible,
	 * owned by the user, or the accessing user is an administrator.
	 *
	 * @param array $links list of objects of type SeedDMS_Core_DocumentLink
	 * @param object $user user for which access is being checked
	 * @param string $access set if source or target of link shall be checked
	 * for sufficient access rights. Set to 'source' if the source document
	 * of a link is to be checked, set to 'target' for the target document.
	 * If not set, then access right aren't checked at all.
	 * @return array filtered list of links
	 */
	static function filterDocumentLinks($user, $links, $access='') { /* {{{ */
		$tmp = array();
		foreach ($links as $link) {
			if ($link->isPublic() || ($link->getUser()->getID() == $user->getID()) || $user->isAdmin()){
				if($access == 'source') {
					$obj = $link->getDocument();
					if ($obj->getAccessMode($user) >= M_READ)
						array_push($tmp, $link);
				} elseif($access == 'target') {
					$obj = $link->getTarget();
					if ($obj->getAccessMode($user) >= M_READ)
						array_push($tmp, $link);
				} else {
					array_push($tmp, $link);
				}
			}
		}
		return $tmp;
	} /* }}} */

	/**
	 * Filter out document attachments which can not be accessed by a given user
	 *
	 * Returns a filtered list of files which are accessible by the
	 * given user. A file is only accessible, if it is publically visible,
	 * owned by the user, or the accessing user is an administrator.
	 *
	 * @param array $files list of objects of type SeedDMS_Core_DocumentFile
	 * @param object $user user for which access is being checked
	 * @return array filtered list of files
	 */
	static function filterDocumentFiles($user, $files) { /* {{{ */
		$tmp = array();
		foreach ($files as $file)
			if ($file->isPublic() || ($file->getUser()->getID() == $user->getID()) || $user->isAdmin() || ($file->getDocument()->getOwner()->getID() == $user->getID()))
				array_push($tmp, $file);
		return $tmp;
	} /* }}} */

	/**
	 * Create a new instance of the dms
	 *
	 * @param object $db object of class {@link SeedDMS_Core_DatabaseAccess}
	 *        to access the underlying database
	 * @param string $contentDir path in filesystem containing the data store
	 *        all document contents is stored
	 * @return object instance of {@link SeedDMS_Core_DMS}
	 */
	function __construct($db, $contentDir) { /* {{{ */
		$this->db = $db;
		if(substr($contentDir, -1) == '/')
			$this->contentDir = $contentDir;
		else
			$this->contentDir = $contentDir.'/';
		$this->rootFolderID = 1;
		$this->maxDirID = 0; //31998;
		$this->forceRename = false;
		$this->enableConverting = false;
		$this->convertFileTypes = array();
		$this->noReadForStatus = array();
		$this->classnames = array();
		$this->classnames['folder'] = 'SeedDMS_Core_Folder';
		$this->classnames['document'] = 'SeedDMS_Core_Document';
		$this->classnames['documentcontent'] = 'SeedDMS_Core_DocumentContent';
		$this->classnames['user'] = 'SeedDMS_Core_User';
		$this->classnames['group'] = 'SeedDMS_Core_Group';
		$this->classnames['report'] = 'SeedDMS_Core_Report';
		$this->classnames['category'] = 'SeedDMS_Core_DocumentCategory';
		$this->classnames['typecat'] = 'SeedDMS_Core_TypeCategory';
		$this->classnames['type'] = 'SeedDMS_Core_DocumentType';
		$this->classnames['instrument'] = 'SeedDMS_Core_Instrument';
		$this->classnames['instrumentvar'] = 'SeedDMS_Core_InstrumentVar';
		$this->classnames['instrumenteval'] = 'SeedDMS_Core_InstrumentEval';
		$this->classnames['axe'] = 'SeedDMS_Core_Axe';
		$this->classnames['department'] = 'SeedDMS_Core_Department';
		$this->classnames['notifyschedule'] = 'SeedDMS_Core_NotificationSchedule';
		$this->callbacks = array();
		$this->version = '5.1.5';
		if($this->version[0] == '@')
			$this->version = '5.1.5';
	} /* }}} */

	/**
	 * Return class name of instantiated objects
	 *
	 * This method returns the class name of those objects being instatiated
	 * by the dms. Each class has an internal place holder, which must be
	 * passed to function.
	 *
	 * @param string placeholder (can be one of 'folder', 'document',
	 * 'documentcontent', 'user', 'group'
	 *
	 * @return string/boolean name of class or false if placeholder is invalid
	 */
	function getClassname($objectname) { /* {{{ */
		if(isset($this->classnames[$objectname]))
			return $this->classnames[$objectname];
		else
			return false;
	} /* }}} */

	/**
	 * Set class name of instantiated objects
	 *
	 * This method sets the class name of those objects being instatiated
	 * by the dms. It is mainly used to create a new class (possible
	 * inherited from one of the available classes) implementing new
	 * features. The method should be called in the postInitDMS hook.
	 *
	 * @param string placeholder (can be one of 'folder', 'document',
	 * 'documentcontent', 'user', 'group'
	 * @param string name of class
	 *
	 * @return string/boolean name of old class or false if not set
	 */
	function setClassname($objectname, $classname) { /* {{{ */
		if(isset($this->classnames[$objectname]))
			$oldclass =  $this->classnames[$objectname];
		else
			$oldclass = false;
		$this->classnames[$objectname] = $classname;
		return $oldclass;
	} /* }}} */

	/**
	 * Return database where meta data is stored
	 *
	 * This method returns the database object as it was set by the first
	 * parameter of the constructor.
	 *
	 * @return object database
	 */
	function getDB() { /* {{{ */
		return $this->db;
	} /* }}} */

	/**
	 * Return the database version
	 *
	 * @return array array with elements major, minor, subminor, date
	 */
	function getDBVersion() { /* {{{ */
		$tbllist = $this->db->TableList();
		$tbllist = explode(',',strtolower(join(',',$tbllist)));
		if(!array_search('tblversion', $tbllist))
			return false;
		$queryStr = "SELECT * FROM `tblVersion` order by `major`,`minor`,`subminor` limit 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && $resArr == false)
			return false;
		if (count($resArr) != 1)
			return false;
		$resArr = $resArr[0];
		return $resArr;
	} /* }}} */

	/**
	 * Check if the version in the database is the same as of this package
	 * Only the major and minor version number will be checked.
	 *
	 * @return boolean returns false if versions do not match, but returns
	 *         true if version matches or table tblVersion does not exists.
	 */
	function checkVersion() { /* {{{ */
		$tbllist = $this->db->TableList();
		$tbllist = explode(',',strtolower(join(',',$tbllist)));
		if(!array_search('tblversion', $tbllist))
			return true;
		$queryStr = "SELECT * FROM `tblVersion` order by `major`,`minor`,`subminor` limit 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && $resArr == false)
			return false;
		if (count($resArr) != 1)
			return false;
		$resArr = $resArr[0];
		$ver = explode('.', $this->version);
		if(($resArr['major'] != $ver[0]) || ($resArr['minor'] != $ver[1]))
			return false;
		return true;
	} /* }}} */

	/**
	 * Set id of root folder
	 * This function must be called right after creating an instance of
	 * {@link SeedDMS_Core_DMS}
	 *
	 * @param interger $id id of root folder
	 */
	function setRootFolderID($id) { /* {{{ */
		$this->rootFolderID = $id;
	} /* }}} */

	/**
	 * Set maximum number of subdirectories per directory
	 *
	 * The value of maxDirID is quite crucial, because each document is
	 * stored within a directory in the filesystem. Consequently, there can be
	 * a maximum number of documents, because depending on the file system
	 * the maximum number of subdirectories is limited. Since version 3.3.0 of
	 * SeedDMS an additional directory level has been introduced, which
	 * will be created when maxDirID is not 0. All documents
	 * from 1 to maxDirID-1 will be saved in 1/<docid>, documents from maxDirID
	 * to 2*maxDirID-1 are stored in 2/<docid> and so on.
	 *
	 * Modern file systems like ext4 do not have any restrictions on the number
	 * of subdirectories anymore. Therefore it is best if this parameter is
	 * set to 0. Never change this parameter if documents has already been
	 * created.
	 *
	 * This function must be called right after creating an instance of
	 * {@link SeedDMS_Core_DMS}
	 *
	 * @param interger $id id of root folder
	 */
	function setMaxDirID($id) { /* {{{ */
		$this->maxDirID = $id;
	} /* }}} */

	/**
	 * Get root folder
	 *
	 * @return object/boolean return the object of the root folder or false if
	 *        the root folder id was not set before with {@link setRootFolderID}.
	 */
	function getRootFolder() { /* {{{ */
		if(!$this->rootFolderID) return false;
		return $this->getFolder($this->rootFolderID);
	} /* }}} */

	function setEnableConverting($enable) { /* {{{ */
		$this->enableConverting = $enable;
	} /* }}} */

	function setConvertFileTypes($types) { /* {{{ */
		$this->convertFileTypes = $types;
	} /* }}} */

	function setViewOnlineFileTypes($types) { /* {{{ */
		$this->viewOnlineFileTypes = $types;
	} /* }}} */

	function setForceRename($enable) { /* {{{ */
		$this->forceRename = $enable;
	} /* }}} */

	/**
	 * Set the logged in user
	 *
	 * If user authentication was done externally, this function can
	 * be used to tell the dms who is currently logged in.
	 *
	 * @param object $user
	 *
	 */
	function setUser($user) { /* {{{ */
		$this->user = $user;
	} /* }}} */

	/**
	 * Get the logged in user
	 *
	 * If user authentication was done externally, this function can
	 * be used to tell the dms who is currently logged in.
	 *
	 * @return object $user
	 *
	 */
	function getLoggedInUser() { /* {{{ */
		return $this->user;
	} /* }}} */

	/**
	 * Return a document by its id
	 *
	 * This function retrieves a document from the database by its id.
	 *
	 * @param integer $id internal id of document
	 * @return object instance of {@link SeedDMS_Core_Document} or false
	 */
	function getDocument($id) { /* {{{ */
		$classname = $this->classnames['document'];
		return $classname::getInstance($id, $this);
	} /* }}} */

	/**
	 * Returns all documents of a given user
	 *
	 * @param object $user
	 * @return array list of documents
	 */
	function getDocumentsByUser($user) { /* {{{ */
		return $user->getDocuments();
	} /* }}} */

	/**
	 * Returns all documents locked by a given user
	 *
	 * @param object $user
	 * @return array list of documents
	 */
	function getDocumentsLockedByUser($user) { /* {{{ */
		return $user->getDocumentsLocked();
	} /* }}} */

	/**
	 * Returns all documents which already expired or will expire in the future
	 *
	 * @param string $date date in format YYYY-MM-DD or an integer with the number
	 *   of days. A negative value will cover the days in the past.
	 * @return array list of documents
	 */
	function getDocumentsExpired($date, $user=null) { /* {{{ */
		$db = $this->getDB();

		if(is_int($date)) {
			$ts = mktime(0, 0, 0) + $date * 86400;
		} elseif(is_string($date)) {
			$tmp = explode('-', $date, 3);
			if(count($tmp) != 3)
				return false;
			$ts = mktime(0, 0, 0, $tmp[1], $tmp[2], $tmp[0]);
		} else
			return false;

		$tsnow = mktime(0, 0, 0); /* Start of today */
		if($ts < $tsnow) { /* Check for docs expired in the past */
			$startts = $ts;
			$endts = $tsnow+86400; /* Use end of day */
			$updatestatus = true;
		} else { /* Check for docs which will expire in the future */
			$startts = $tsnow;
			$endts = $ts+86400; /* Use end of day */
			$updatestatus = false;
		}

		/* Get all documents which have an expiration date. It doesn't check for
		 * the latest status which should be S_EXPIRED, but doesn't have to, because
		 * status may have not been updated after the expiration date has been reached.
		 **/
		$queryStr = "SELECT `tblDocuments`.`id`, `tblDocumentStatusLog`.`status`  FROM `tblDocuments` ".
			"LEFT JOIN `ttcontentid` ON `ttcontentid`.`document` = `tblDocuments`.`id` ".
			"LEFT JOIN `tblDocumentContent` ON `tblDocuments`.`id` = `tblDocumentContent`.`document` AND `tblDocumentContent`.`version` = `ttcontentid`.`maxVersion` ".
			"LEFT JOIN `tblDocumentStatus` ON `tblDocumentStatus`.`documentID` = `tblDocumentContent`.`document` AND `tblDocumentContent`.`version` = `tblDocumentStatus`.`version` ".
			"LEFT JOIN `ttstatid` ON `ttstatid`.`statusID` = `tblDocumentStatus`.`statusID` ".
			"LEFT JOIN `tblDocumentStatusLog` ON `tblDocumentStatusLog`.`statusLogID` = `ttstatid`.`maxLogID`";
		$queryStr .= 
			" WHERE `tblDocuments`.`expires` > ".$startts." AND `tblDocuments`.`expires` < ".$endts;
		if($user)
			$queryStr .=
				" AND `tblDocuments`.`owner` = '".$user->getID()."' ";
		$queryStr .= 
			" ORDER BY `expires` DESC";

		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$documents = array();
		$ts = mktime(0, 0, 0) + 86400;
		foreach ($resArr as $row) {
			$document = $this->getDocument($row["id"]);
			if($updatestatus)
				$document->verifyLastestContentExpriry();
			$documents[] = $document;
		}
		return $documents;
	} /* }}} */

	/**
	 * Returns a document by its name
	 *
	 * This function searches a document by its name and restricts the search
	 * to given folder if passed as the second parameter.
	 *
	 * @param string $name
	 * @param object $folder
	 * @return object/boolean found document or false
	 */
	function getDocumentByName($name, $folder=null) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT `tblDocuments`.*, `tblDocumentLocks`.`userID` as `lockUser` ".
			"FROM `tblDocuments` ".
			"LEFT JOIN `tblDocumentLocks` ON `tblDocuments`.`id`=`tblDocumentLocks`.`document` ".
			"WHERE `tblDocuments`.`name` = " . $this->db->qstr($name);
		if($folder)
			$queryStr .= " AND `tblDocuments`.`folder` = ". $folder->getID();
		$queryStr .= " LIMIT 1";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		if(!$resArr)
			return false;

		$row = $resArr[0];
		$document = new $this->classnames['document']($row["id"], $row["name"], $row["comment"], $row["date"], $row["expires"], $row["owner"], $row["folder"], $row["inheritAccess"], $row["defaultAccess"], $row["lockUser"], $row["keywords"], $row["sequence"]);
		$document->setDMS($this);
		return $document;
	} /* }}} */

	/**
	 * Returns a document by the original file name of the last version
	 *
	 * This function searches a document by the name of the last document
	 * version and restricts the search
	 * to given folder if passed as the second parameter.
	 *
	 * @param string $name
	 * @param object $folder
	 * @return object/boolean found document or false
	 */
	function getDocumentByOriginalFilename($name, $folder=null) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT `tblDocuments`.*, `tblDocumentLocks`.`userID` as `lockUser` ".
			"FROM `tblDocuments` ".
			"LEFT JOIN `ttcontentid` ON `ttcontentid`.`document` = `tblDocuments`.`id` ".
			"LEFT JOIN `tblDocumentContent` ON `tblDocumentContent`.`document` = `tblDocuments`.`id` AND `tblDocumentContent`.`version` = `ttcontentid`.`maxVersion` ".
			"LEFT JOIN `tblDocumentLocks` ON `tblDocuments`.`id`=`tblDocumentLocks`.`document` ".
			"WHERE `tblDocumentContent`.`orgFileName` = " . $this->db->qstr($name);
		if($folder)
			$queryStr .= " AND `tblDocuments`.`folder` = ". $folder->getID();
		$queryStr .= " LIMIT 1";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		if(!$resArr)
			return false;

		$row = $resArr[0];
		$document = new $this->classnames['document']($row["id"], $row["name"], $row["comment"], $row["date"], $row["expires"], $row["owner"], $row["folder"], $row["inheritAccess"], $row["defaultAccess"], $row["lockUser"], $row["keywords"], $row["sequence"]);
		$document->setDMS($this);
		return $document;
	} /* }}} */

	/**
	 * Return a document content by its id
	 *
	 * This function retrieves a document content from the database by its id.
	 *
	 * @param integer $id internal id of document content
	 * @return object instance of {@link SeedDMS_Core_DocumentContent} or false
	 */
	function getDocumentContent($id) { /* {{{ */
		if (!is_numeric($id)) return false;

		$queryStr = "SELECT * FROM `tblDocumentContent` WHERE `id` = ".(int) $id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && $resArr == false)
			return false;
		if (count($resArr) != 1)
			return false;
		$row = $resArr[0];

		$document = $this->getDocument($row['document']);
		$version = new $this->classnames['documentcontent']($row['id'], $document, $row['version'], $row['comment'], $row['date'], $row['createdBy'], $row['dir'], $row['orgFileName'], $row['fileType'], $row['mimeType'], $row['fileSize'], $row['checksum']);
		return $version;
	} /* }}} */

	/**
	 * Returns all documents with a predefined search criteria
	 *
	 * The records return have the following elements
	 *
	 * From Table tblDocuments
	 * [id] => id of document
	 * [name] => name of document
	 * [comment] => comment of document
	 * [date] => timestamp of creation date of document
	 * [expires] => timestamp of expiration date of document
	 * [owner] => user id of owner
	 * [folder] => id of parent folder
	 * [folderList] => column separated list of folder ids, e.g. :1:41:
	 * [inheritAccess] => 1 if access is inherited
	 * [defaultAccess] => default access mode
	 * [locked] => always -1 (TODO: is this field still used?)
	 * [keywords] => keywords of document
	 * [sequence] => sequence of document
	 *
	 * From Table tblDocumentLocks
	 * [lockUser] => id of user locking the document
	 *
	 * From Table tblDocumentStatusLog
	 * [version] => latest version of document
	 * [statusID] => id of latest status log
	 * [documentID] => id of document
	 * [status] => current status of document
	 * [statusComment] => comment of current status
	 * [statusDate] => datetime when the status was entered, e.g. 2014-04-17 21:35:51
	 * [userID] => id of user who has initiated the status change
	 *
	 * From Table tblUsers
	 * [ownerName] => name of owner of document
	 * [statusName] => name of user who has initiated the status change
	 *
	 * @param string $listtype type of document list, can be 'AppRevByMe',
	 * 'AppRevOwner', 'ReceiptByMe', 'ReviseByMe', 'LockedByMe', 'MyDocs'
	 * @param object $param1 user
	 * @param string $param2 set to true
	 * if 'AppRevByMe', 'ReviseByMe', 'ReceiptByMe' shall return even documents
	 * І have already taken care of.
	 * if 'ExpiredOwner' contains the date in days or as 'yyyy-mm-dd'
	 * @param string $param3 sort list by this field
	 * @param string $param4 order direction
	 * @return array list of documents records
	 */
	function getDocumentList($listtype, $param1=null, $param2=false, $param3='', $param4='') { /* {{{ */
		/* The following query will get all documents and lots of additional
		 * information. It requires the two temporary tables ttcontentid and
		 * ttstatid.
		 */
		if (!$this->db->createTemporaryTable("ttstatid") || !$this->db->createTemporaryTable("ttcontentid")) {
			return false;
		}
		/* The following statement retrieves the status of the last version of all
		 * documents. It must be restricted by further where clauses.
		 */
/*
		$queryStr = "SELECT `tblDocuments`.*, `tblDocumentLocks`.`userID` as `lockUser`, ".
			"`tblDocumentContent`.`version`, `tblDocumentStatus`.*, `tblDocumentStatusLog`.`status`, ".
			"`tblDocumentStatusLog`.`comment` AS `statusComment`, `tblDocumentStatusLog`.`date` as `statusDate`, ".
			"`tblDocumentStatusLog`.`userID`, `oTbl`.`fullName` AS `ownerName`, `sTbl`.`fullName` AS `statusName` ".
			"FROM `tblDocumentContent` ".
			"LEFT JOIN `tblDocuments` ON `tblDocuments`.`id` = `tblDocumentContent`.`document` ".
			"LEFT JOIN `tblDocumentStatus` ON `tblDocumentStatus`.`documentID` = `tblDocumentContent`.`document` ".
			"LEFT JOIN `tblDocumentStatusLog` ON `tblDocumentStatusLog`.`statusID` = `tblDocumentStatus`.`statusID` ".
			"LEFT JOIN `ttstatid` ON `ttstatid`.`maxLogID` = `tblDocumentStatusLog`.`statusLogID` ".
			"LEFT JOIN `ttcontentid` ON `ttcontentid`.`maxVersion` = `tblDocumentStatus`.`version` AND `ttcontentid`.`document` = `tblDocumentStatus`.`documentID` ".
			"LEFT JOIN `tblDocumentLocks` ON `tblDocuments`.`id`=`tblDocumentLocks`.`document` ".
			"LEFT JOIN `tblUsers` AS `oTbl` on `oTbl`.`id` = `tblDocuments`.`owner` ".
			"LEFT JOIN `tblUsers` AS `sTbl` on `sTbl`.`id` = `tblDocumentStatusLog`.`userID` ".
			"WHERE `ttstatid`.`maxLogID`=`tblDocumentStatusLog`.`statusLogID` ".
			"AND `ttcontentid`.`maxVersion` = `tblDocumentContent`.`version` ";
 */
		/* New sql statement which retrieves all documents, its latest version and
		 * status, the owner and user initiating the latest status.
		 * It doesn't need the where clause anymore. Hence the statement could be
		 * extended with further left joins.
		 */
		$selectStr = "SELECT `tblDocuments`.*, `tblDocumentLocks`.`userID` as `lockUser`, ".
			"`tblDocumentContent`.`version`, `tblDocumentStatus`.*, `tblDocumentStatusLog`.`status`, ".
			"`tblDocumentStatusLog`.`comment` AS `statusComment`, `tblDocumentStatusLog`.`date` as `statusDate`, ".
			"`tblDocumentStatusLog`.`userID`, `oTbl`.`fullName` AS `ownerName`, `sTbl`.`fullName` AS `statusName` ";
		$queryStr =
			"FROM `ttcontentid` ".
			"LEFT JOIN `tblDocuments` ON `tblDocuments`.`id` = `ttcontentid`.`document` ".
			"LEFT JOIN `tblDocumentContent` ON `tblDocumentContent`.`document` = `ttcontentid`.`document` AND `tblDocumentContent`.`version` = `ttcontentid`.`maxVersion` ".
			"LEFT JOIN `tblDocumentStatus` ON `tblDocumentStatus`.`documentID`=`ttcontentid`.`document` AND `tblDocumentStatus`.`version`=`ttcontentid`.`maxVersion` ".
			"LEFT JOIN `ttstatid` ON `ttstatid`.`statusID` = `tblDocumentStatus`.`statusID` ".
			"LEFT JOIN `tblDocumentStatusLog` ON `ttstatid`.`statusID` = `tblDocumentStatusLog`.`statusID` AND `ttstatid`.`maxLogID` = `tblDocumentStatusLog`.`statusLogID` ".
			"LEFT JOIN `tblDocumentLocks` ON `ttcontentid`.`document`=`tblDocumentLocks`.`document` ".
			"LEFT JOIN `tblUsers` `oTbl` ON `oTbl`.`id` = `tblDocuments`.`owner` ".
			"LEFT JOIN `tblUsers` `sTbl` ON `sTbl`.`id` = `tblDocumentStatusLog`.`userID` ";

//		echo $queryStr;

		switch($listtype) {
		case 'AppRevByMe': // Documents I have to review/approve {{{
			$queryStr .= "WHERE 1=1 ";

			$user = $param1;
			// Get document list for the current user.
			$reviewStatus = $user->getReviewStatus();
			$approvalStatus = $user->getApprovalStatus();

			// Create a comma separated list of all the documentIDs whose information is
			// required.
			// Take only those documents into account which hasn't be touched by the user
			$dList = array();
			foreach ($reviewStatus["indstatus"] as $st) {
				if (($st["status"]==0 || $param2) && !in_array($st["documentID"], $dList)) {
					$dList[] = $st["documentID"];
				}
			}
			foreach ($reviewStatus["grpstatus"] as $st) {
				if (($st["status"]==0 || $param2) && !in_array($st["documentID"], $dList)) {
					$dList[] = $st["documentID"];
				}
			}
			foreach ($approvalStatus["indstatus"] as $st) {
				if (($st["status"]==0 || $param2) && !in_array($st["documentID"], $dList)) {
					$dList[] = $st["documentID"];
				}
			}
			foreach ($approvalStatus["grpstatus"] as $st) {
				if (($st["status"]==0 || $param2) && !in_array($st["documentID"], $dList)) {
					$dList[] = $st["documentID"];
				}
			}
			$docCSV = "";
			foreach ($dList as $d) {
				$docCSV .= (strlen($docCSV)==0 ? "" : ", ")."'".$d."'";
			}

			if (strlen($docCSV)>0) {
				$queryStr .= "AND `tblDocumentStatusLog`.`status` IN (".S_DRAFT_REV.", ".S_DRAFT_APP.", ".S_EXPIRED.") ".
							"AND `tblDocuments`.`id` IN (" . $docCSV . ") ".
							"ORDER BY `statusDate` DESC";
			} else {
				$queryStr = '';
			}
			break; // }}}
		case 'ReviewByMe': // Documents I have to review {{{
			if (!$this->db->createTemporaryTable("ttreviewid")) {
				return false;
			}
			$user = $param1;
			$orderby = $param3;
			if($param4 == 'desc')
				$orderdir = 'DESC';
			else
				$orderdir = 'ASC';

			$groups = array();
			$tmp = $user->getGroups();
			foreach($tmp as $group)
				$groups[] = $group->getID();

			$selectStr .= ", `tblDocumentReviewLog`.`date` as `duedate` ";
			$queryStr .=
				"LEFT JOIN `tblDocumentReviewers` on `ttcontentid`.`document`=`tblDocumentReviewers`.`documentID` AND `ttcontentid`.`maxVersion`=`tblDocumentReviewers`.`version` ".
				"LEFT JOIN `ttreviewid` ON `ttreviewid`.`reviewID` = `tblDocumentReviewers`.`reviewID` ".
				"LEFT JOIN `tblDocumentReviewLog` ON `tblDocumentReviewLog`.`reviewLogID`=`ttreviewid`.`maxLogID` ";

			if(1) {
			$queryStr .= "WHERE (`tblDocumentReviewers`.`type` = 0 AND `tblDocumentReviewers`.`required` = ".$user->getID()." ";
			if($groups)
				$queryStr .= "OR `tblDocumentReviewers`.`type` = 1 AND `tblDocumentReviewers`.`required` IN (".implode(',', $groups).") ";
			$queryStr .= ") ";
			$queryStr .= "AND `tblDocumentStatusLog`.`status` IN (".S_DRAFT_REV.", ".S_EXPIRED.") ";
			if(!$param2)
				$queryStr .= " AND `tblDocumentReviewLog`.`status` = 0 ";
			if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
			else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
			else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
			else $queryStr .= "ORDER BY `name`";
			$queryStr .= " ".$orderdir;
			} else {
			$queryStr .= "WHERE 1=1 ";

			// Get document list for the current user.
			$reviewStatus = $user->getReviewStatus();

			// Create a comma separated list of all the documentIDs whose information is
			// required.
			// Take only those documents into account which hasn't be touched by the user
			// ($st["status"]==0)
			$dList = array();
			foreach ($reviewStatus["indstatus"] as $st) {
				if (($st["status"]==0 || $param2) && !in_array($st["documentID"], $dList)) {
					$dList[] = $st["documentID"];
				}
			}
			foreach ($reviewStatus["grpstatus"] as $st) {
				if (($st["status"]==0 || $param2) && !in_array($st["documentID"], $dList)) {
					$dList[] = $st["documentID"];
				}
			}
			$docCSV = "";
			foreach ($dList as $d) {
				$docCSV .= (strlen($docCSV)==0 ? "" : ", ")."'".$d."'";
			}

			if (strlen($docCSV)>0) {
				$queryStr .= "AND `tblDocumentStatusLog`.`status` IN (".S_DRAFT_REV.", ".S_EXPIRED.") ".
							"AND `tblDocuments`.`id` IN (" . $docCSV . ") ";
				//$queryStr .= "ORDER BY `statusDate` DESC";
				if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
				else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
				else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
				else $queryStr .= "ORDER BY `name`";
				$queryStr .= " ".$orderdir;
			} else {
				$queryStr = '';
			}
			}
			break; // }}}
		case 'ApproveByMe': // Documents I have to approve {{{
			if (!$this->db->createTemporaryTable("ttapproveid")) {
				return false;
			}
			$user = $param1;
			$orderby = $param3;
			if($param4 == 'desc')
				$orderdir = 'DESC';
			else
				$orderdir = 'ASC';

			$groups = array();
			$tmp = $user->getGroups();
			foreach($tmp as $group)
				$groups[] = $group->getID();

			$selectStr .= ", `tblDocumentApproveLog`.`date` as `duedate` ";
			$queryStr .=
				"LEFT JOIN `tblDocumentApprovers` on `ttcontentid`.`document`=`tblDocumentApprovers`.`documentID` AND `ttcontentid`.`maxVersion`=`tblDocumentApprovers`.`version` ".
				"LEFT JOIN `ttapproveid` ON `ttapproveid`.`approveID` = `tblDocumentApprovers`.`approveID` ".
				"LEFT JOIN `tblDocumentApproveLog` ON `tblDocumentApproveLog`.`approveLogID`=`ttapproveid`.`maxLogID` ";

			if(1) {
			$queryStr .= "WHERE (`tblDocumentApprovers`.`type` = 0 AND `tblDocumentApprovers`.`required` = ".$user->getID()." ";
			if($groups)
				$queryStr .= "OR `tblDocumentApprovers`.`type` = 1 AND `tblDocumentApprovers`.`required` IN (".implode(',', $groups).")";
			$queryStr .= ") ";
			$queryStr .= "AND `tblDocumentStatusLog`.`status` IN (".S_DRAFT_APP.", ".S_EXPIRED.") ";
			if(!$param2)
				$queryStr .= " AND `tblDocumentApproveLog`.`status` = 0 ";
			if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
			else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
			else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
			else $queryStr .= "ORDER BY `name`";
			$queryStr .= " ".$orderdir;
			} else {
			$queryStr .= "WHERE 1=1 ";

			// Get document list for the current user.
			$approvalStatus = $user->getApprovalStatus();

			// Create a comma separated list of all the documentIDs whose information is
			// required.
			// Take only those documents into account which hasn't be touched by the user
			// ($st["status"]==0)
			$dList = array();
			foreach ($approvalStatus["indstatus"] as $st) {
				if (($st["status"]==0 || $param2) && !in_array($st["documentID"], $dList)) {
					$dList[] = $st["documentID"];
				}
			}
			foreach ($approvalStatus["grpstatus"] as $st) {
				if (($st["status"]==0 || $param2) && !in_array($st["documentID"], $dList)) {
					$dList[] = $st["documentID"];
				}
			}
			$docCSV = "";
			foreach ($dList as $d) {
				$docCSV .= (strlen($docCSV)==0 ? "" : ", ")."'".$d."'";
			}

			if (strlen($docCSV)>0) {
				$queryStr .= "AND `tblDocumentStatusLog`.`status` IN (".S_DRAFT_APP.", ".S_EXPIRED.") ".
							"AND `tblDocuments`.`id` IN (" . $docCSV . ") ";
				//$queryStr .= "ORDER BY `statusDate` DESC";
				if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
				else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
				else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
				else $queryStr .= "ORDER BY `name`";
				$queryStr .= " ".$orderdir;
			} else {
				$queryStr = '';
			}
			}
			break; // }}}
		case 'WorkflowByMe': // Documents I to trigger in Worklflow {{{
			$queryStr .= "WHERE 1=1 ";

			$user = $param1;
			// Get document list for the current user.
			$workflowStatus = $user->getWorkflowStatus();

			// Create a comma separated list of all the documentIDs whose information is
			// required.
			$dList = array();
			foreach ($workflowStatus["u"] as $st) {
				if (!in_array($st["document"], $dList)) {
					$dList[] = $st["document"];
				}
			}
			foreach ($workflowStatus["g"] as $st) {
				if (!in_array($st["document"], $dList)) {
					$dList[] = $st["document"];
				}
			}
			$docCSV = "";
			foreach ($dList as $d) {
				$docCSV .= (strlen($docCSV)==0 ? "" : ", ")."'".$d."'";
			}

			if (strlen($docCSV)>0) {
				$queryStr .=
							//"AND `tblDocumentStatusLog`.`status` IN (".S_IN_WORKFLOW.", ".S_EXPIRED.") ".
							"AND `tblDocuments`.`id` IN (" . $docCSV . ") ".
							"ORDER BY `statusDate` DESC";
			} else {
				$queryStr = '';
			}
			break; // }}}
		case 'AppRevOwner': // Documents waiting for review/approval/revision I'm owning {{{
			$queryStr .= "WHERE 1=1 ";

			$user = $param1;
			$orderby = $param3;
			if($param4 == 'desc')
				$orderdir = 'DESC';
			else
				$orderdir = 'ASC';
			$queryStr .=	"AND `tblDocuments`.`owner` = '".$user->getID()."' ".
				"AND `tblDocumentStatusLog`.`status` IN (".S_DRAFT_REV.", ".S_DRAFT_APP.", ".S_IN_REVISION.") ";
			if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
			else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
			else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
			else $queryStr .= "ORDER BY `name`";
			$queryStr .= " ".$orderdir;
//			$queryStr .= "AND `tblDocuments`.`owner` = '".$user->getID()."' ".
//				"AND `tblDocumentStatusLog`.`status` IN (".S_DRAFT_REV.", ".S_DRAFT_APP.") ".
//				"ORDER BY `statusDate` DESC";
			break; // }}}
		case 'RejectOwner': // Documents that has been rejected and I'm owning {{{
			$queryStr .= "WHERE 1=1 ";

			$user = $param1;
			$orderby = $param3;
			if($param4 == 'desc')
				$orderdir = 'DESC';
			else
				$orderdir = 'ASC';
			$queryStr .= "AND `tblDocuments`.`owner` = '".$user->getID()."' ".
				"AND `tblDocumentStatusLog`.`status` IN (".S_REJECTED.") ";
			//$queryStr .= "ORDER BY `statusDate` DESC";
			if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
			else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
			else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
			else $queryStr .= "ORDER BY `name`";
			$queryStr .= " ".$orderdir;
			break; // }}}
		case 'LockedByMe': // Documents locked by me {{{
			$queryStr .= "WHERE 1=1 ";

			$user = $param1;
			$orderby = $param3;
			if($param4 == 'desc')
				$orderdir = 'DESC';
			else
				$orderdir = 'ASC';

			$qs = 'SELECT `document` FROM `tblDocumentLocks` WHERE `userID`='.$user->getID();
			$ra = $this->db->getResultArray($qs);
			if (is_bool($ra) && !$ra) {
				return false;
			}
			$docs = array();
			foreach($ra as $d) {
				$docs[] = $d['document'];
			}

			if ($docs) {
				$queryStr .= "AND `tblDocuments`.`id` IN (" . implode(',', $docs) . ") ";
				if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
				else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
				else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
				else $queryStr .= "ORDER BY `name`";
				$queryStr .= " ".$orderdir;
			} else {
				$queryStr = '';
			}
			break; // }}}
		case 'ExpiredOwner': // Documents expired and owned by me {{{
			if(is_int($param2)) {
				$ts = mktime(0, 0, 0) + $param2 * 86400;
			} elseif(is_string($param2)) {
				$tmp = explode('-', $param2, 3);
				if(count($tmp) != 3)
					return false;
				$ts = mktime(0, 0, 0, $tmp[1], $tmp[2], $tmp[0]);
			} else
				$ts = mktime(0, 0, 0)-365*86400; /* Start of today - 1 year */

			$tsnow = mktime(0, 0, 0); /* Start of today */
			if($ts < $tsnow) { /* Check for docs expired in the past */
				$startts = $ts;
				$endts = $tsnow+86400; /* Use end of day */
			} else { /* Check for docs which will expire in the future */
				$startts = $tsnow;
				$endts = $ts+86400; /* Use end of day */
			}

			$queryStr .= 
				"WHERE `tblDocuments`.`expires` > ".$startts." AND `tblDocuments`.`expires` < ".$endts." ";

			$user = $param1;
			$orderby = $param3;
			if($param4 == 'desc')
				$orderdir = 'DESC';
			else
				$orderdir = 'ASC';
			$queryStr .=	"AND `tblDocuments`.`owner` = '".$user->getID()."' ";
			if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
			else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
			else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
			else $queryStr .= "ORDER BY `name`";
			$queryStr .= " ".$orderdir;
			break; // }}}
		case 'WorkflowOwner': // Documents waiting for workflow trigger I'm owning {{{
			$queryStr .= "WHERE 1=1 ";

			$user = $param1;
			$queryStr .= "AND `tblDocuments`.`owner` = '".$user->getID()."' ".
				"AND `tblDocumentStatusLog`.`status` IN (".S_IN_WORKFLOW.") ".
				"ORDER BY `statusDate` DESC";
			break; // }}}
		case 'MyDocs': // Documents owned by me {{{
			$queryStr .= "WHERE 1=1 ";

			$user = $param1;
			$orderby = $param3;
			if($param4 == 'desc')
				$orderdir = 'DESC';
			else
				$orderdir = 'ASC';
			$queryStr .=	"AND `tblDocuments`.`owner` = '".$user->getID()."' ";
			if ($orderby=='e') $queryStr .= "ORDER BY `expires`";
			else if ($orderby=='u') $queryStr .= "ORDER BY `statusDate`";
			else if ($orderby=='s') $queryStr .= "ORDER BY `status`";
			else $queryStr .= "ORDER BY `name`";
			$queryStr .= " ".$orderdir;
			break; // }}}
		}

		if($queryStr) {
			$resArr = $this->db->getResultArray($selectStr.$queryStr);
			if (is_bool($resArr) && !$resArr) {
				return false;
			}
			/*
			$documents = array();
			foreach($resArr as $row)
				$documents[] = $this->getDocument($row["id"]);
			 */
		} else {
			return array();
		}

		return $resArr;
	} /* }}} */

	function makeTimeStamp($hour, $min, $sec, $year, $month, $day) { /* {{{ */
		$thirtyone = array (1, 3, 5, 7, 8, 10, 12);
		$thirty = array (4, 6, 9, 11);

		// Very basic check that the terms are valid. Does not fail for illegal
		// dates such as 31 Feb.
		if (!is_numeric($hour) || !is_numeric($min) || !is_numeric($sec) || !is_numeric($year) || !is_numeric($month) || !is_numeric($day) || $month<1 || $month>12 || $day<1 || $day>31 || $hour<0 || $hour>23 || $min<0 || $min>59 || $sec<0 || $sec>59) {
			return false;
		}
		$year = (int) $year;
		$month = (int) $month;
		$day = (int) $day;

		if (array_search($month, $thirtyone)) {
			$max=31;
		}
		else if (array_search($month, $thirty)) {
			$max=30;
		}
		else {
			$max=(($year % 4 == 0) && ($year % 100 != 0 || $year % 400 == 0)) ? 29 : 28;
		}

		// If the date falls out of bounds, set it to the maximum for the given
		// month. Makes assumption about the user's intention, rather than failing
		// for absolutely everything.
		if ($day>$max) {
			$day=$max;
		}

		return mktime($hour, $min, $sec, $month, $day, $year);
	} /* }}} */

	/*
	 * Search the database for documents
	 *
	 * Note: the creation date will be used to check againts the
	 * date saved with the document
	 * or folder. The modification date will only be used for documents. It
	 * is checked against the creation date of the document content. This
	 * meanѕ that updateѕ of a document will only result in a searchable
	 * modification if a new version is uploaded.
	 *
	 * @param query string seach query with space separated words
	 * @param limit integer number of items in result set
	 * @param offset integer index of first item in result set
	 * @param logicalmode string either AND or OR
	 * @param searchin array() list of fields to search in
	 *        1 = keywords, 2=name, 3=comment, 4=attributes
	 * @param startFolder object search in the folder only (null for root folder)
	 * @param owner object search for documents owned by this user
	 * @param status array list of status
	 * @param creationstartdate array search for documents created after this date
	 * @param creationenddate array search for documents created before this date
	 * @param modificationstartdate array search for documents modified after this date
	 * @param modificationenddate array search for documents modified before this date
	 * @param categories array list of categories the documents must have assigned
	 * @param attributes array list of attributes. The key of this array is the
	 * attribute definition id. The value of the array is the value of the
	 * attribute. If the attribute may have multiple values it must be an array.
	 * @param mode int decide whether to search for documents/folders
	 *        0x1 = documents only
	 *        0x2 = folders only
	 *        0x3 = both
	 * @param expirationstartdate array search for documents expiring after this date
	 * @param expirationenddate array search for documents expiring before this date
	 * @param publishstartdate array search for documents published after this date
	 * @param publishenddate array search for documents published before this date
	 * @return array containing the elements total and docs
	 */
	function search($query, $limit=0, $offset=0, $logicalmode='AND', $searchin=array(), $startFolder=null, $owner=null, $status = array(), $creationstartdate=array(), $creationenddate=array(), $modificationstartdate=array(), $modificationenddate=array(), $categories=array(), $attributes=array(), $mode=0x3, $expirationstartdate=array(), $expirationenddate=array(), $publishstartdate=array(), $publishenddate=array()) { /* {{{ */
		// Split the search string into constituent keywords.
		$tkeys=array();
		if (strlen($query)>0) {
			$tkeys = preg_split("/[\t\r\n ,]+/", $query);
		}

		// if none is checkd search all
		if (count($searchin)==0)
			$searchin=array(1, 2, 3, 4, 5);

		/*--------- Do it all over again for folders -------------*/
		$totalFolders = 0;
		if($mode & 0x2) {
			$searchKey = "";

			$classname = $this->classnames['folder'];
			$searchFields = $classname::getSearchFields($this, $searchin);

			if (count($searchFields)>0) {
				foreach ($tkeys as $key) {
					$key = trim($key);
					if (strlen($key)>0) {
						$searchKey = (strlen($searchKey)==0 ? "" : $searchKey." ".$logicalmode." ")."(".implode(" like ".$this->db->qstr("%".$key."%")." OR ", $searchFields)." like ".$this->db->qstr("%".$key."%").")";
					}
				}
			}

			// Check to see if the search has been restricted to a particular sub-tree in
			// the folder hierarchy.
			$searchFolder = "";
			if ($startFolder) {
				$searchFolder = "`tblFolders`.`folderList` LIKE '%:".$startFolder->getID().":%'";
			}

			// Check to see if the search has been restricted to a particular
			// document owner.
			$searchOwner = "";
			if ($owner) {
				if(is_array($owner)) {
					$ownerids = array();
					foreach($owner as $o)
						$ownerids[] = $o->getID();
					if($ownerids)
						$searchOwner = "`tblFolders`.`owner` IN (".implode(',', $ownerids).")";
				} else {
					$searchOwner = "`tblFolders`.`owner` = '".$owner->getId()."'";
				}
			}

			// Check to see if the search has been restricted to a particular
			// attribute.
			$searchAttributes = array();
			if ($attributes) {
				foreach($attributes as $attrdefid=>$attribute) {
					if($attribute) {
						$attrdef = $this->getAttributeDefinition($attrdefid);
						if($attrdef->getObjType() == SeedDMS_Core_AttributeDefinition::objtype_folder || $attrdef->getObjType() == SeedDMS_Core_AttributeDefinition::objtype_all) {
							if($valueset = $attrdef->getValueSet()) {
								if($attrdef->getMultipleValues()) {
									$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblFolderAttributes` WHERE `tblFolderAttributes`.`attrdef`=".$attrdefid." AND (`tblFolderAttributes`.`value` like '%".$valueset[0].implode("%' OR `tblFolderAttributes`.`value` like '%".$valueset[0], $attribute)."%') AND `tblFolderAttributes`.`folder`=`tblFolders`.`id`)";
								} else
									$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblFolderAttributes` WHERE `tblFolderAttributes`.`attrdef`=".$attrdefid." AND `tblFolderAttributes`.`value`='".$attribute."' AND `tblFolderAttributes`.`folder`=`tblFolders`.`id`)";
							} else
								$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblFolderAttributes` WHERE `tblFolderAttributes`.`attrdef`=".$attrdefid." AND `tblFolderAttributes`.`value` like '%".$attribute."%' AND `tblFolderAttributes`.`folder`=`tblFolders`.`id`)";
						}
					}
				}
			}

			// Is the search restricted to documents created between two specific dates?
			$searchCreateDate = "";
			if ($creationstartdate) {
				$startdate = SeedDMS_Core_DMS::makeTimeStamp($creationstartdate['hour'], $creationstartdate['minute'], $creationstartdate['second'], $creationstartdate['year'], $creationstartdate["month"], $creationstartdate["day"]);
				if ($startdate) {
					$searchCreateDate .= "`tblFolders`.`date` >= ".$startdate;
				}
			}
			if ($creationenddate) {
				$stopdate = SeedDMS_Core_DMS::makeTimeStamp($creationenddate['hour'], $creationenddate['minute'], $creationenddate['second'], $creationenddate["year"], $creationenddate["month"], $creationenddate["day"]);
				if ($stopdate) {
					if($startdate)
						$searchCreateDate .= " AND ";
					$searchCreateDate .= "`tblFolders`.`date` <= ".$stopdate;
				}
			}
			
			// Is the search restricted to documents published between two specific dates?
			$searchPublishDate = "";
			if ($publishstartdate) {
				$startdate = SeedDMS_Core_DMS::makeTimeStamp($publishstartdate['hour'], $publishstartdate['minute'], $publishstartdate['second'], $publishstartdate['year'], $publishstartdate["month"], $publishstartdate["day"]);
				if ($startdate) {
					$searchPublishDate .= "`tblFolders`.`date` >= ".$startdate;
				}
			}
			if ($publishenddate) {
				$stopdate = SeedDMS_Core_DMS::makeTimeStamp($publishenddate['hour'], $publishenddate['minute'], $publishenddate['second'], $publishenddate["year"], $publishenddate["month"], $publishenddate["day"]);
				if ($stopdate) {
					if($startdate)
						$searchPublishDate .= " AND ";
					$searchPublishDate .= "`tblFolders`.`date` <= ".$stopdate;
				}
			}

			$searchQuery = "FROM ".$classname::getSearchTables()." WHERE 1=1";

			if (strlen($searchKey)>0) {
				$searchQuery .= " AND (".$searchKey.")";
			}
			if (strlen($searchFolder)>0) {
				$searchQuery .= " AND ".$searchFolder;
			}
			if (strlen($searchOwner)>0) {
				$searchQuery .= " AND (".$searchOwner.")";
			}
			if (strlen($searchCreateDate)>0) {
				$searchQuery .= " AND (".$searchCreateDate.")";
			}
			if (strlen($searchPublishDate)>0) {
				$searchQuery .= " AND (".$searchPublishDate.")";
			}
			if ($searchAttributes) {
				$searchQuery .= " AND (".implode(" AND ", $searchAttributes).")";
			}

			/* Do not search for folders if not at least a search for a key,
			 * an owner, or creation date is requested.
			 */
			if($searchKey || $searchOwner || $searchCreateDate || $searchPublishDate || $searchAttributes) {
				// Count the number of rows that the search will produce.
				$resArr = $this->db->getResultArray("SELECT COUNT(*) AS num FROM (SELECT DISTINCT `tblFolders`.id ".$searchQuery.") a");
				if ($resArr && isset($resArr[0]) && is_numeric($resArr[0]["num"]) && $resArr[0]["num"]>0) {
					$totalFolders = (integer)$resArr[0]["num"];
				}

				// If there are no results from the count query, then there is no real need
				// to run the full query. TODO: re-structure code to by-pass additional
				// queries when no initial results are found.

				// Only search if the offset is not beyond the number of folders
				if($totalFolders > $offset) {
					// Prepare the complete search query, including the LIMIT clause.
					$searchQuery = "SELECT DISTINCT `tblFolders`.`id` ".$searchQuery." GROUP BY `tblFolders`.`id`";

					if($limit) {
						$searchQuery .= " LIMIT ".$limit." OFFSET ".$offset;
					}

					// Send the complete search query to the database.
					$resArr = $this->db->getResultArray($searchQuery);
				} else {
					$resArr = array();
				}

				// ------------------- Ausgabe der Ergebnisse ----------------------------
				$numResults = count($resArr);
				if ($numResults == 0) {
					$folderresult = array('totalFolders'=>$totalFolders, 'folders'=>array());
				} else {
					foreach ($resArr as $folderArr) {
						$folders[] = $this->getFolder($folderArr['id']);
					}
					$folderresult = array('totalFolders'=>$totalFolders, 'folders'=>$folders);
				}
			} else {
				$folderresult = array('totalFolders'=>0, 'folders'=>array());
			}
		} else {
			$folderresult = array('totalFolders'=>0, 'folders'=>array());
		}

		/*--------- Do it all over again for documents -------------*/

		$totalDocs = 0;
		if($mode & 0x1) {
			$searchKey = "";

			$classname = $this->classnames['document'];
			$searchFields = $classname::getSearchFields($this, $searchin);

			if (count($searchFields)>0) {
				foreach ($tkeys as $key) {
					$key = trim($key);
					if (strlen($key)>0) {
						$searchKey = (strlen($searchKey)==0 ? "" : $searchKey." ".$logicalmode." ")."(".implode(" like ".$this->db->qstr("%".$key."%")." OR ", $searchFields)." like ".$this->db->qstr("%".$key."%").")";
					}
				}
			}

			// Check to see if the search has been restricted to a particular sub-tree in
			// the folder hierarchy.
			$searchFolder = "";
			if ($startFolder) {
				$searchFolder = "`tblDocuments`.`folderList` LIKE '%:".$startFolder->getID().":%'";
			}

			// Check to see if the search has been restricted to a particular
			// document owner.
			$searchOwner = "";
			if ($owner) {
				if(is_array($owner)) {
					$ownerids = array();
					foreach($owner as $o)
						$ownerids[] = $o->getID();
					if($ownerids)
						$searchOwner = "`tblDocuments`.`owner` IN (".implode(',', $ownerids).")";
				} else {
					$searchOwner = "`tblDocuments`.`owner` = '".$owner->getId()."'";
				}
			}

			// Check to see if the search has been restricted to a particular
			// document category.
			$searchCategories = "";
			if ($categories) {
				$catids = array();
				foreach($categories as $category)
					$catids[] = $category->getId();
				$searchCategories = "`tblDocumentCategory`.`categoryID` in (".implode(',', $catids).")";
			}

			// Check to see if the search has been restricted to a particular
			// attribute.
			$searchAttributes = array();
			if ($attributes) {
				foreach($attributes as $attrdefid=>$attribute) {
					if($attribute) {
						$attrdef = $this->getAttributeDefinition($attrdefid);
						if($attrdef->getObjType() == SeedDMS_Core_AttributeDefinition::objtype_document || $attrdef->getObjType() == SeedDMS_Core_AttributeDefinition::objtype_all) {
							if($valueset = $attrdef->getValueSet()) {
								if($attrdef->getMultipleValues()) {
									$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblDocumentAttributes` WHERE `tblDocumentAttributes`.`attrdef`=".$attrdefid." AND (`tblDocumentAttributes`.`value` like '%".$valueset[0].implode("%' OR `tblDocumentAttributes`.`value` like '%".$valueset[0], $attribute)."%') AND `tblDocumentAttributes`.`document` = `tblDocuments`.`id`)";
								} else
									$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblDocumentAttributes` WHERE `tblDocumentAttributes`.`attrdef`=".$attrdefid." AND `tblDocumentAttributes`.`value`='".$attribute."' AND `tblDocumentAttributes`.`document` = `tblDocuments`.`id`)";
							} else
								$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblDocumentAttributes` WHERE `tblDocumentAttributes`.`attrdef`=".$attrdefid." AND `tblDocumentAttributes`.`value` like '%".$attribute."%' AND `tblDocumentAttributes`.`document` = `tblDocuments`.`id`)";
						} elseif($attrdef->getObjType() == SeedDMS_Core_AttributeDefinition::objtype_documentcontent) {
							if($attrdef->getValueSet()) {
								if($attrdef->getMultipleValues()) {
									$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblDocumentContentAttributes` WHERE `tblDocumentContentAttributes`.`attrdef`=".$attrdefid." AND (`tblDocumentContentAttributes`.`value` like '%".$valueset[0].implode("%' OR `tblDocumentContentAttributes`.`value` like '%".$valueset[0], $attribute)."%') AND `tblDocumentContentAttributes`.`document` = `tblDocumentContent`.`id`)";
								} else {
									$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblDocumentContentAttributes` WHERE `tblDocumentContentAttributes`.`attrdef`=".$attrdefid." AND `tblDocumentContentAttributes`.`value`='".$attribute."' AND `tblDocumentContentAttributes`.content = `tblDocumentContent`.id)";
								}
							} else
								$searchAttributes[] = "EXISTS (SELECT NULL FROM `tblDocumentContentAttributes` WHERE `tblDocumentContentAttributes`.`attrdef`=".$attrdefid." AND `tblDocumentContentAttributes`.`value` like '%".$attribute."%' AND `tblDocumentContentAttributes`.content = `tblDocumentContent`.id)";
						}
					}
				}
			}

			// Is the search restricted to documents created between two specific dates?
			$searchCreateDate = "";
			if ($creationstartdate) {
				$startdate = SeedDMS_Core_DMS::makeTimeStamp($creationstartdate['hour'], $creationstartdate['minute'], $creationstartdate['second'], $creationstartdate['year'], $creationstartdate["month"], $creationstartdate["day"]);
				if ($startdate) {
					$searchCreateDate .= "`tblDocuments`.`date` >= ".$startdate;
				}
			}
			if ($creationenddate) {
				$stopdate = SeedDMS_Core_DMS::makeTimeStamp($creationenddate['hour'], $creationenddate['minute'], $creationenddate['second'], $creationenddate["year"], $creationenddate["month"], $creationenddate["day"]);
				if ($stopdate) {
					if($searchCreateDate)
						$searchCreateDate .= " AND ";
					$searchCreateDate .= "`tblDocuments`.`date` <= ".$stopdate;
				}
			}
			
			$searchPublishDate = "";
			if ($publishstartdate) {
				$startdate = SeedDMS_Core_DMS::makeTimeStamp($publishstartdate['hour'], $publishstartdate['minute'], $publishstartdate['second'], $publishstartdate['year'], $publishstartdate["month"], $publishstartdate["day"]);
				if ($startdate) {
					$searchPublishDate .= "`tblDocumentStatusLog`.`status` = 2 AND `tblDocumentStatusLog`.`date` >= '".date("Y-m-d H:i:s",mktime($publishstartdate['hour'], $publishstartdate['minute'], $publishstartdate['second'], $publishstartdate['month'], $publishstartdate["day"], $publishstartdate["year"]))."'";
				}
			}
			if ($publishenddate) {
				$stopdate = SeedDMS_Core_DMS::makeTimeStamp($publishenddate['hour'], $publishenddate['minute'], $publishenddate['second'], $publishenddate["year"], $publishenddate["month"], $publishenddate["day"]);
				if ($stopdate) {
					if($searchPublishDate)
						$searchPublishDate .= " AND ";
					$searchPublishDate .= "`tblDocumentStatusLog`.`date` <= '".date("Y-m-d H:i:s",mktime($publishenddate['hour'], $publishenddate['minute'], $publishenddate['second'], $publishenddate["month"], $publishenddate["day"], $publishenddate["year"]))."'";
				}
			}
			
			if ($modificationstartdate) {
				$startdate = SeedDMS_Core_DMS::makeTimeStamp($modificationstartdate['hour'], $modificationstartdate['minute'], $modificationstartdate['second'], $modificationstartdate['year'], $modificationstartdate["month"], $modificationstartdate["day"]);
				if ($startdate) {
					if($searchCreateDate)
						$searchCreateDate .= " AND ";
					$searchCreateDate .= "`tblDocumentContent`.`date` >= ".$startdate;
				}
			}
			if ($modificationenddate) {
				$stopdate = SeedDMS_Core_DMS::makeTimeStamp($modificationenddate['hour'], $modificationenddate['minute'], $modificationenddate['second'], $modificationenddate["year"], $modificationenddate["month"], $modificationenddate["day"]);
				if ($stopdate) {
					if($searchCreateDate)
						$searchCreateDate .= " AND ";
					$searchCreateDate .= "`tblDocumentContent`.`date` <= ".$stopdate;
				}
			}
			$searchExpirationDate = '';
			if ($expirationstartdate) {
				$startdate = SeedDMS_Core_DMS::makeTimeStamp($expirationstartdate['hour'], $expirationstartdate['minute'], $expirationstartdate['second'], $expirationstartdate['year'], $expirationstartdate["month"], $expirationstartdate["day"]);
				if ($startdate) {
					if($searchExpirationDate)
						$searchExpirationDate .= " AND ";
					$searchExpirationDate .= "`tblDocuments`.`expires` >= ".$startdate;
				}
			}
			if ($expirationenddate) {
				$stopdate = SeedDMS_Core_DMS::makeTimeStamp($expirationenddate['hour'], $expirationenddate['minute'], $expirationenddate['second'], $expirationenddate["year"], $expirationenddate["month"], $expirationenddate["day"]);
				if ($stopdate) {
					if($searchExpirationDate)
						$searchExpirationDate .= " AND ";
					$searchExpirationDate .= "`tblDocuments`.`expires` <= ".$stopdate;
				}
			}

			// ---------------------- Suche starten ----------------------------------

			//
			// Construct the SQL query that will be used to search the database.
			//

			if (!$this->db->createTemporaryTable("ttcontentid") || !$this->db->createTemporaryTable("ttstatid")) {
				return false;
			}

			$searchQuery = "FROM `tblDocumentContent` ".
				"LEFT JOIN `tblDocuments` ON `tblDocuments`.`id` = `tblDocumentContent`.`document` ".
				"LEFT JOIN `tblDocumentAttributes` ON `tblDocuments`.`id` = `tblDocumentAttributes`.`document` ".
				"LEFT JOIN `tblDocumentContentAttributes` ON `tblDocumentContent`.`id` = `tblDocumentContentAttributes`.`content` ".
				"LEFT JOIN `tblDocumentStatus` ON `tblDocumentStatus`.`documentID` = `tblDocumentContent`.`document` ".
				"LEFT JOIN `tblDocumentStatusLog` ON `tblDocumentStatusLog`.`statusID` = `tblDocumentStatus`.`statusID` ".
				"LEFT JOIN `ttstatid` ON `ttstatid`.`maxLogID` = `tblDocumentStatusLog`.`statusLogID` ".
				"LEFT JOIN `ttcontentid` ON `ttcontentid`.`maxVersion` = `tblDocumentStatus`.`version` AND `ttcontentid`.`document` = `tblDocumentStatus`.`documentID` ".
				"LEFT JOIN `tblDocumentLocks` ON `tblDocuments`.`id`=`tblDocumentLocks`.`document` ".
				"LEFT JOIN `tblDocumentCategory` ON `tblDocuments`.`id`=`tblDocumentCategory`.`documentID` ".
				"WHERE `ttstatid`.`maxLogID`=`tblDocumentStatusLog`.`statusLogID` ".
				"AND `ttcontentid`.`maxVersion` = `tblDocumentContent`.`version`";

			if (strlen($searchKey)>0) {
				$searchQuery .= " AND (".$searchKey.")";
			}
			if (strlen($searchFolder)>0) {
				$searchQuery .= " AND ".$searchFolder;
			}
			if (strlen($searchOwner)>0) {
				$searchQuery .= " AND (".$searchOwner.")";
			}
			if (strlen($searchCategories)>0) {
				$searchQuery .= " AND (".$searchCategories.")";
			}
			if (strlen($searchCreateDate)>0) {
				$searchQuery .= " AND (".$searchCreateDate.")";
			}
			if (strlen($searchPublishDate)>0) {
				$searchQuery .= " AND (".$searchPublishDate.")";
			}
			if (strlen($searchExpirationDate)>0) {
				$searchQuery .= " AND (".$searchExpirationDate.")";
			}
			if ($searchAttributes) {
				$searchQuery .= " AND (".implode(" AND ", $searchAttributes).")";
			}

			// status
			if ($status) {
				$searchQuery .= " AND `tblDocumentStatusLog`.`status` IN (".implode(',', $status).")";
			}

			if($searchKey || $searchOwner || $searchCategories || $searchCreateDate || $searchPublishDate || $searchExpirationDate || $searchAttributes || $status) {
				// Count the number of rows that the search will produce.
				$resArr = $this->db->getResultArray("SELECT COUNT(*) AS num FROM (SELECT DISTINCT `tblDocuments`.`id` ".$searchQuery.") a");
				$totalDocs = 0;
				if (is_numeric($resArr[0]["num"]) && $resArr[0]["num"]>0) {
					$totalDocs = (integer)$resArr[0]["num"];
				}

				// If there are no results from the count query, then there is no real need
				// to run the full query. TODO: re-structure code to by-pass additional
				// queries when no initial results are found.

				// Prepare the complete search query, including the LIMIT clause.
				$searchQuery = "SELECT DISTINCT `tblDocuments`.*, ".
					"`tblDocumentContent`.`version`, ".
					"`tblDocumentStatusLog`.`status`, `tblDocumentLocks`.`userID` as `lockUser` ".$searchQuery;
//var_dump($searchQuery);
				// calculate the remaining entrїes of the current page
				// If page is not full yet, get remaining entries
				if($limit) {
					$remain = $limit - count($folderresult['folders']);
					if($remain) {
						if($remain == $limit)
							$offset -= $totalFolders;
						else
							$offset = 0;
						if($limit)
							$searchQuery .= " LIMIT ".$limit." OFFSET ".$offset;

						// Send the complete search query to the database.
						$resArr = $this->db->getResultArray($searchQuery);
					} else {
						$resArr = array();
					}
				} else {
					// Send the complete search query to the database.
					$resArr = $this->db->getResultArray($searchQuery);
				}

				// ------------------- Ausgabe der Ergebnisse ----------------------------
				$numResults = count($resArr);
				if ($numResults == 0) {
					$docresult = array('totalDocs'=>$totalDocs, 'docs'=>array());
				} else {
					foreach ($resArr as $docArr) {
						$docs[] = $this->getDocument($docArr['id']);
					}
					$docresult = array('totalDocs'=>$totalDocs, 'docs'=>$docs);
				}
			} else {
				$docresult = array('totalDocs'=>0, 'docs'=>array());
			}
		} else {
			$docresult = array('totalDocs'=>0, 'docs'=>array());
		}

		if($limit) {
			$totalPages = (integer)(($totalDocs+$totalFolders)/$limit);
			if ((($totalDocs+$totalFolders)%$limit) > 0) {
				$totalPages++;
			}
		} else {
			$totalPages = 1;
		}

		return array_merge($docresult, $folderresult, array('totalPages'=>$totalPages));
	} /* }}} */

	/**
	 * Return a folder by its id
	 *
	 * This function retrieves a folder from the database by its id.
	 *
	 * @param integer $id internal id of folder
	 * @return object instance of SeedDMS_Core_Folder or false
	 */
	function getFolder($id) { /* {{{ */
		$classname = $this->classnames['folder'];
		return $classname::getInstance($id, $this);
	} /* }}} */

	/**
	 * Return a folder by its name
	 *
	 * This function retrieves a folder from the database by its name. The
	 * search covers the whole database. If
	 * the parameter $folder is not null, it will search for the name
	 * only within this parent folder. It will not be done recursively.
	 *
	 * @param string $name name of the folder
	 * @param object $folder parent folder
	 * @return object/boolean found folder or false
	 */
	function getFolderByName($name, $folder=null) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblFolders` WHERE `name` = " . $this->db->qstr($name);
		if($folder)
			$queryStr .= " AND `parent` = ". $folder->getID();
		$queryStr .= " LIMIT 1";
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		if(!$resArr)
			return false;

		$resArr = $resArr[0];
		$folder = new $this->classnames['folder']($resArr["id"], $resArr["name"], $resArr["parent"], $resArr["comment"], $resArr["date"], $resArr["owner"], $resArr["inheritAccess"], $resArr["defaultAccess"], $resArr["sequence"]);
		$folder->setDMS($this);
		return $folder;
	} /* }}} */

	/**
	 * Returns a list of folders and error message not linked in the tree
	 *
	 * This function checks all folders in the database.
	 *
	 * @return array list of errors
	 */
	function checkFolders() { /* {{{ */
		$queryStr = "SELECT * FROM `tblFolders`";
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr === false)
			return false;

		$cache = array();
		foreach($resArr as $rec) {
			$cache[$rec['id']] = array('name'=>$rec['name'], 'parent'=>$rec['parent'], 'folderList'=>$rec['folderList']);
		}
		$errors = array();
		foreach($cache as $id=>$rec) {
			if(!array_key_exists($rec['parent'], $cache) && $rec['parent'] != 0) {
				$errors[$id] = array('id'=>$id, 'name'=>$rec['name'], 'parent'=>$rec['parent'], 'msg'=>'Missing parent');
			} else {
				$tmparr = explode(':', $rec['folderList']);
				array_shift($tmparr);
				if(count($tmparr) != count(array_unique($tmparr))) {
					$errors[$id] = array('id'=>$id, 'name'=>$rec['name'], 'parent'=>$rec['parent'], 'msg'=>'Duplicate entry in folder list ('.$rec['folderList'].')');
				}
			}
		}

		return $errors;
	} /* }}} */

	/**
	 * Returns a list of documents and error message not linked in the tree
	 *
	 * This function checks all documents in the database.
	 *
	 * @return array list of errors
	 */
	function checkDocuments() { /* {{{ */
		$queryStr = "SELECT * FROM `tblFolders`";
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr === false)
			return false;

		$fcache = array();
		foreach($resArr as $rec) {
			$fcache[$rec['id']] = array('name'=>$rec['name'], 'parent'=>$rec['parent'], 'folderList'=>$rec['folderList']);
		}

		$queryStr = "SELECT * FROM `tblDocuments`";
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr === false)
			return false;

		$dcache = array();
		foreach($resArr as $rec) {
			$dcache[$rec['id']] = array('name'=>$rec['name'], 'parent'=>$rec['folder'], 'folderList'=>$rec['folderList']);
		}
		$errors = array();
		foreach($dcache as $id=>$rec) {
			if(!array_key_exists($rec['parent'], $fcache) && $rec['parent'] != 0) {
				$errors[$id] = array('id'=>$id, 'name'=>$rec['name'], 'parent'=>$rec['parent'], 'msg'=>'Missing parent');
			} else {
				$tmparr = explode(':', $rec['folderList']);
				array_shift($tmparr);
				if(count($tmparr) != count(array_unique($tmparr))) {
					$errors[$id] = array('id'=>$id, 'name'=>$rec['name'], 'parent'=>$rec['parent'], 'msg'=>'Duplicate entry in folder list ('.$rec['folderList'].'');
				}
			}
		}

		return $errors;
	} /* }}} */

	/**
	 * Return a user by its id
	 *
	 * This function retrieves a user from the database by its id.
	 *
	 * @param integer $id internal id of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getUser($id) { /* {{{ */
		$classname = $this->classnames['user'];
		return $classname::getInstance($id, $this);
	} /* }}} */
	
	/**
	 * Return a category by its id
	 *
	 * This function retrieves a category from the database by its id.
	 *
	 * @param integer $id internal id of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getCategory($id) { /* {{{ */
		$classname = $this->classnames['category'];
		return $classname::getInstance($id, $this);
	} /* }}} */

	/**
	 * Return a user by its login
	 *
	 * This function retrieves a user from the database by its login.
	 * If the second optional parameter $email is not empty, the user must
	 * also have the given email.
	 *
	 * @param string $login internal login of user
	 * @param string $email email of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getUserByLogin($login, $email='') { /* {{{ */
		$classname = $this->classnames['user'];
		return $classname::getInstance($login, $this, 'name', $email);
	} /* }}} */

	/**
	 * Return a user by its email
	 *
	 * This function retrieves a user from the database by its email.
	 * It is needed when the user requests a new password.
	 *
	 * @param integer $email email address of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getUserByEmail($email) { /* {{{ */
		$classname = $this->classnames['user'];
		return $classname::getInstance($email, $this, 'email');
	} /* }}} */

	/**
	 * Return list of all users
	 *
	 * @return array of instances of {@link SeedDMS_Core_User} or false
	 */
	function getAllUsers($orderby = '', $activeOnly = false) { /* {{{ */
		$classname = $this->classnames['user'];
		return $classname::getAllInstances($orderby, $this, $activeOnly);
	} /* }}} */

	/**
	 * Get a group by its id
	 *
	 * @param integer $id id of group
	 * @return object/boolean group or false if no group was found
	 */
	function getReport($id) { /* {{{ */
		$classname = $this->classnames['report'];
		return $classname::getInstance($id, $this, 'id');
	} /* }}} */
	
	/**
	 * Get a list of all reports by userid
	 *
	 * @return array array of instances of {@link SeedDMS_Core_Group}
	 */
	function getAllReports($userid) { /* {{{ */
		$classname = $this->classnames['report'];
		return $classname::getAllInstancesByUser($userid, $this);
	} /* }}} */

	/**
	 * Get a report by its name
	 *
	 * @param string $name name of group
	 * @return object/boolean group or false if no group was found
	 */
	function getReportByName($name) { /* {{{ */
		$classname = $this->classnames['report'];
		return $classname::getInstance($name, $this, 'name');
	} /* }}} */
	

	/**
	 * Create a new user group
	 *
	 * @param string $name name of group
	 * @param string $comment comment of group
	 * @return object/boolean instance of {@link SeedDMS_Core_Group} or false in
	 *         case of an error.
	 */
	function addReport($name, $comment, $query) { /* {{{ */
		if (is_object($this->getReportByName($name))) {
			return false;
		}

		$queryStr = "INSERT INTO `tblReports` (`name`, `comment`, `query`, `userID`, `date`) VALUES (".$this->db->qstr($name).", ".$this->db->qstr($comment).", ".$this->db->qstr($query).", ".$this->user->getID().", UNIX_TIMESTAMP())";
		
		if (!$this->db->getResult($queryStr))
			return false;

		$report = $this->getReport($this->db->getInsertID());

		/* Check if 'onPostAddGroup' callback is set */
		if(isset($this->_dms->callbacks['onPostAddReport'])) {
			foreach($this->_dms->callbacks['onPostAddReport'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $report)) {
				}
			}
		}

		return $report;
	} /* }}} */

	/**
	 * Return a instrument by its id
	 *
	 * This function retrieves a user from the database by its id.
	 *
	 * @param integer $id internal id of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getInstrument($id) { /* {{{ */
		$classname = $this->classnames['instrument'];
		return $classname::getInstance($id, $this);
	} /* }}} */

	/**
	 * Return a instrument by its id
	 *
	 * This function retrieves a user from the database by its id.
	 *
	 * @param integer $id internal id of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getInstrumentEval($instrumentID, $date) { /* {{{ */
		$classname = $this->classnames['instrumenteval'];
		return $classname::getInstance($instrumentID, $date, $this);
	} /* }}} */
	
	/**
	 * Return a instrument by its id
	 *
	 * This function retrieves a user from the database by its id.
	 *
	 * @param integer $id internal id of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getInstrumentVar($id) { /* {{{ */
		$classname = $this->classnames['instrumentvar'];
		return $classname::getInstance($id, $this, 'id');
	} /* }}} */
	
	function getInstrumentVars() { /* {{{ */
		$classname = $this->classnames['instrumentvar'];
		return $classname::getAllInstances('code', $this);
	} /* }}} */
	
	function getInstrumentVarsByInstrumentID($id) { /* {{{ */
		$classname = $this->classnames['instrumentvar'];
		return $classname::getInstancesByInstrumentID($id, 'code', $this);
	} /* }}} */

	/**
	 * Return a instrument by its name
	 *
	 * This function retrieves a user from the database by its email.
	 * It is needed when the user requests a new password.
	 *
	 * @param integer $email email address of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getInstrumentByName($name) { /* {{{ */
		$classname = $this->classnames['instrument'];
		return $classname::getInstance($name, $this, 'name');
	} /* }}} */

	/**
	 * Return list of all instruments
	 *
	 * @return array of instances of {@link SeedDMS_Core_User} or false
	 */
	function getAllInstruments($orderby = '') { /* {{{ */
		$classname = $this->classnames['instrument'];
		return $classname::getAllInstances($orderby, $this);
	} /* }}} */
	
	
	/**
	 * Return list of all instruments
	 *
	 * @return array of instances of {@link SeedDMS_Core_User} or false
	 */
	function getAllInstrumentVars($orderby = '') { /* {{{ */
		$classname = $this->classnames['instrumentvar'];
		return $classname::getAllInstances($orderby, $this);
	} /* }}} */	
	
	function getAllEvalDates() { /* {{{ */
		$queryStr = "SELECT DISTINCT `date` FROM `tblInstrumentEvals`";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;
		else
			return $resArr;
	} /* }}} */

	function getAxeMembers($axeID){
		if (!$axeID) return false;

		$queryStr = "SELECT * FROM `tblAxeMembers` where `axeID`=".(int)$axeID;
		$resArr = $this->db->getResultArray($queryStr);
		if (!$resArr){
			return false;
		} else {
			return $resArr;
		}
	}
	
	/**
	 * Return list of all instruments
	 *
	 * @return array of instances of {@link SeedDMS_Core_User} or false
	 */
	function getDiamond($date) { /* {{{ */
		
		$queryStr = "
			SELECT a.name, a.maxScore, COALESCE(SUM(ie.result),0) score
			  FROM tblAxes a,
			       tblInstrumentEvals ie,
			       tblInstruments i
			 WHERE ie.instrumentID = i.id
			   AND a.id IN (SELECT am.axeID FROM tblAxeMembers am WHERE am.instrumentID = i.id)
			   AND ie.date = ".$this->db->qstr($date)."
			GROUP BY 1,2;
		";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;
		else
			return $resArr;
	} /* }}} */	


	function getDiamondYM_old($year, $month) { /* {{{ */
	
		$diamond = array();

		$queryMain = "
			SELECT a.`id` axeID,
			       a.`name` axeName,
			       a.`maxScore` axeMaxScore,
			       i.`id` instrumentID,
			       i.`name` instrumentName,
			       i.`weight` instrumentWeight,
			       iv.`id` varID,
			       iv.`code` varCode,
			       iv.`name` varName,
			       iv.`is_manual` isManual,
			       iv.`weight` varWeight,
			       iv.`evalJan` varEvalJan,
			       iv.`evalFeb` varEvalFeb,
			       iv.`evalMar` varEvalMar,
			       iv.`evalApr` varEvalApr,
			       iv.`evalMay` varEvalMay,
			       iv.`evalJun` varEvalJun,
			       iv.`evalJul` varEvalJul,
			       iv.`evalAug` varEvalAug,
			       iv.`evalSep` varEvalSep,
			       iv.`evalOct` varEvalOct,
			       iv.`evalNov` varEvalNov,
			       iv.`evalDec` varEvalDec,
			       ivt.`typeID` varDocType
			      FROM `tblAxes` a
			INNER JOIN `tblAxeMembers` am
			        ON a.id = am.`axeID`
			INNER JOIN `tblInstruments` i
			        ON i.`id` = am.`instrumentID`
			 LEFT JOIN `tblInstrumentVars` iv
			        ON am.`instrumentID` = iv.`instrumentID`
			 LEFT JOIN `tblInstrumentVarType` ivt
			        ON iv.id = ivt.`instrumentVarID`
			  ORDER BY a.id, am.`instrumentID`, iv.id;
		";

/*
		$queryStr = "
			SELECT a.name, a.maxScore, COALESCE(SUM(ie.result),0) score
			  FROM tblAxes a,
			       tblInstrumentEvals ie,
			       tblInstruments i
			 WHERE ie.instrumentID = i.id
			   AND a.id IN (SELECT am.axeID FROM tblAxeMembers am WHERE am.instrumentID = i.id)
			   AND ie.date = ".$this->db->qstr($date)."
			GROUP BY 1,2;
		";
*/
		$resArr = $this->db->getResultArray($queryMain);

		/*var_dump($resArr);*/
		

		if (is_bool($resArr) && !$resArr)
			return false;
		
		$count = count($resArr);
		$i = 0;
		$var_total = 0;
		$arrVar = array();
		$period = (string)$month."-".(string)$year;
		
		foreach($resArr as $idx => $row) {

			if($row['isManual'] != null && $row['isManual'] === '1'){
				$accomplish = $this->getManualVariableEval($row['axeID'], $row['instrumentID'], $row['varID'], $period);

				if(!empty($accomplish) && !is_bool($accomplish)){
	                if ($accomplish[0]['accomplish'] === "1") {
	                		$arrVar[$i]['var_score'] = (int)$row['varWeight'] * 1;
							$arrVar[$i]['var_accomplished'] = 1;
							$arrVar[$i]['documents'] = null;
	                		$arrVar[$i]['axe'] = $row['axeName'];
							$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
							$arrVar[$i]['instrument'] = $row['instrumentName'];
							$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
							$arrVar[$i]['var'] = $row['varName'];
							$arrVar[$i]['var_weight'] = $row['varWeight'];
							$arrVar[$i]['varCode'] = $row['varCode'];
	                } else {
	                		$arrVar[$i]['var_score'] = 0;
							$arrVar[$i]['var_accomplished'] = 0;
	                		$arrVar[$i]['axe'] = $row['axeName'];
							$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
							$arrVar[$i]['instrument'] = $row['instrumentName'];
							$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
							$arrVar[$i]['var'] = $row['varName'];
							$arrVar[$i]['var_weight'] = $row['varWeight'];
							$arrVar[$i]['varCode'] = $row['varCode'];
	                
	                }

	            } else {
	                	$arrVar[$i]['var_score'] = 0;
						$arrVar[$i]['var_accomplished'] = 0;
	                	$arrVar[$i]['axe'] = $row['axeName'];
						$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
						$arrVar[$i]['instrument'] = $row['instrumentName'];
						$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
						$arrVar[$i]['var'] = $row['varName'];
						$arrVar[$i]['var_weight'] = $row['varWeight'];
						$arrVar[$i]['varCode'] = $row['varCode'];
	            }

				$i++;
			} else {

				$currAxe = $row['axeName'];
				$currInst = $row['instrumentName'];
				$currVar = $row['varName'];

				if ($idx == 0) {
					$prevAxe = $currAxe;
					$prevInst = $currInst;
				}

				// Get document types of current var by year and month published
				$monthNum  = $month;
				$dateObj   = DateTime::createFromFormat('!m', $monthNum);
				$monthName = $dateObj->format('M'); // Month Name
				$requiredDocuments = $row['varEval'.$monthName];

				if ($requiredDocuments > 0) {
					if ($row['varDocType'] != null) {
						$documentsByType = "SELECT * FROM tblDocumentType WHERE typeID = ".$row['varDocType'].";";
						$documents = $this->db->getResultArray($documentsByType);
						if(!empty($documents)){

							$doc_count = count($documents);
							$real_doc_count = 0;
							$real_docs = array();
							foreach ($documents as $doc) {
								$temp_doc = $this->getDocument((int)$doc['documentID']); // Get Document by Id
								
								$lc = $temp_doc->getLatestContent();
								$status_data = $lc->getStatus();
								$doc_status = $status_data['status']; // Get document status: 2 == "Released"

								// Get document versions
								$versions = array();
							    $lcs = $temp_doc->getContent();
							    foreach($lcs as $lcc) {
							        $versions[] = array(
							            'version'=>$lcc->getVersion(),
							            'date'=>date('Y-m-d H:i:s', $lcc->getDate()),
							            'workflow_log' =>$lcc->getWorkflowLog(),
							        );
							    }

							    // Get the actual version of document workflow if exists or not
							    $versions_counter = count($versions);
							    $actual_version = $versions[$versions_counter - 1];

							    // Check if log exists for last version and get the updated date. If not, get document's original date
							    $workflow_log = $versions[$versions_counter - 1]['workflow_log'];
							    if (!empty($workflow_log)) {
							        $log_counter = count($workflow_log);
							        if ($log_counter >= 1) {
							            $doc_timestamp = strtotime($workflow_log[$log_counter - 1]->getDate());
							            $updated_doc_date = date('d-m-Y',$doc_timestamp);
							            $version_month = date('m',$doc_timestamp);
							            $version_year = date('Y',$doc_timestamp);
							        }
							    } else {
							       $version_month = date('m', $temp_doc->getDate()); 
							       $version_year = date('Y', $temp_doc->getDate());
							       $updated_doc_date = date('d-m-Y', $temp_doc->getDate());
							    }

							    if ($doc_status === '2' && $version_month === $month && $version_year === $year) {
							    	$real_docs[$real_doc_count]['id'] = $temp_doc->getID();
							    	$real_docs[$real_doc_count]['name'] = $temp_doc->getName();
							    	$real_docs[$real_doc_count]['updated_date'] = $updated_doc_date;

							    	$real_doc_count++;						   
							    }
							}

							// If variable documents count is == 1 and real documents count is == 1
							if ($requiredDocuments == 1 && $real_doc_count == 1) {								
								$arrVar[$i]['var_score'] = $row['varWeight'] * 1;
								$arrVar[$i]['var_accomplished'] = 1;
								$arrVar[$i]['documents'] = $real_docs;
							} else if ($requiredDocuments > 1 && $real_doc_count >= 1) {								
	//							$arrVar[$i]['var_score'] = 100 / $real_doc_count;
								$arrVar[$i]['var_score'] = $row['varWeight'] * ((($real_doc_count / $requiredDocuments) > 1) ? 1 : ($real_doc_count / $requiredDocuments));
								$arrVar[$i]['var_accomplished'] = (($real_doc_count / $requiredDocuments) > 1) ? 1 : ($real_doc_count / $requiredDocuments);
								$arrVar[$i]['documents'] = $real_docs;
							} else if ($requiredDocuments >= 1 && $real_doc_count == 0) {							
								$arrVar[$i]['var_score'] = 0;
								$arrVar[$i]['var_accomplished'] = 0;
							} else {
								$arrVar[$i]['var_score'] = null;
								$arrVar[$i]['var_accomplished'] = 0;
							}

							$arrVar[$i]['axe'] = $row['axeName'];
							$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
							$arrVar[$i]['instrument'] = $row['instrumentName'];
							$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
							$arrVar[$i]['var'] = $row['varName'];
							$arrVar[$i]['var_weight'] = $row['varWeight'];
							$arrVar[$i]['varCode'] = $row['varCode'];

							$i++;
						}
					}				
				} else {
					// No evaluated axe has a score 0
					$arrVar[$i]['axe'] = $row['axeName'];
					$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
					$arrVar[$i]['instrument'] = $row['instrumentName'];
					$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
					$arrVar[$i]['var'] = $row['varName'];
					$arrVar[$i]['var_weight'] = $row['varWeight'];
					$arrVar[$i]['varCode'] = $row['varCode'];
					$arrVar[$i]['var_accomplished'] = 0;
					$arrVar[$i]['var_score'] = null;
					$i++;	
				}
			}
		}

		//var_dump($arrVar);

		
		$all_variables = $arrVar;

		// Scan and get total for all instruments
		if (!empty($arrVar)) {

			$arrInst = array();
			$prev_axe = "";
			$prev_inst = "";
			
			$j = 0;
			$k = 0;
			$inst_docs = array();
			$inst_points = 0;
			foreach ($arrVar as $var) {			
				if ($k == 0) {
					$prev_inst = $var['instrument'];
					$prev_axe = $var['axe'];
				}

				if ($var['instrument'] == $prev_inst && $var['axe'] == $prev_axe) {
					$arrInst[$j]['axe'] = $var['axe'];
					$arrInst[$j]['axe_max_score'] = $var['axe_max_score'];
					$arrInst[$j]['instrument'] = $var['instrument'];
					$arrInst[$j]['instrument_weight'] = $var['instrument_weight'];
					$inst_points += $var['var_score'];
					$arrInst[$j]['points'] = $inst_points;
					
					if (isset($var['documents']) && $var['documents'] != null) {
						array_push($inst_docs, $var['documents']);
						$arrInst[$j]['documents'] = $inst_docs;	
					}		

				} else {
					$j++;
					$inst_points = 0;
					unset($inst_docs);
					$inst_docs = array();
					$arrInst[$j]['axe'] = $var['axe'];
					$arrInst[$j]['axe_max_score'] = $var['axe_max_score'];
					$arrInst[$j]['instrument'] = $var['instrument'];
					$arrInst[$j]['instrument_weight'] = $var['instrument_weight'];
					$inst_points += $var['var_score'];
					$arrInst[$j]['points'] = $inst_points;
					
					if (isset($var['documents']) && $var['documents'] != null) {
						array_push($inst_docs, $var['documents']);
						$arrInst[$j]['documents'] = $inst_docs;	
					}
				}

				if ($var['instrument'] != $prev_inst) {
					$prev_inst = $var['instrument'];
				}

				if ($var['axe'] != $prev_axe) {
					$prev_axe = $var['axe'];
					
				}

				$k++;
			}			
		} else {
			return false;
		}

		//var_dump($arrInst);
		$all_instruments = $arrInst;

		// Scan and get total for all instruments
		if (!empty($arrInst)) {

			$arrAxes = array();
			$previous_axe = "";
			
			$l = 0;
			$m = 0;
			$axe_docs = array();
			$axe_score = 0;
			foreach ($arrInst as $instrument) {			
				if ($m == 0) {
					$previous_axe = $instrument['axe'];
				}

				if ($instrument['axe'] == $previous_axe) {
					
					$arrAxes['axes'][$l]['name'] = $instrument['axe'];
					$arrAxes['axes'][$l]['maxScore'] = $instrument['axe_max_score'];
					$axe_score += ((int)$instrument['instrument_weight'] * (int)$instrument['points']) / 100;	
					$arrAxes['axes'][$l]['score'] = $axe_score;

					if (isset($instrument['documents']) && $instrument['documents'] != null) {
						array_push($axe_docs, $instrument['documents']);
						$arrAxes['axes'][$l]['documents'] = $axe_docs;	
					}

				} else {
					$l++;
					$axe_score = 0;
					unset($axe_docs);
					$axe_docs = array();
					
					$arrAxes['axes'][$l]['name'] = $instrument['axe'];
					$arrAxes['axes'][$l]['maxScore'] = $instrument['axe_max_score'];
					$axe_score += ((int)$instrument['instrument_weight'] * (int)$instrument['points']) / 100;	
					$arrAxes['axes'][$l]['score'] = $axe_score;

					if (isset($instrument['documents']) && $instrument['documents'] != null) {
						array_push($axe_docs, $instrument['documents']);
						$arrAxes['axes'][$l]['documents'] = $axe_docs;	
					}
					
				}

				if ($instrument['axe'] != $previous_axe) {
					$previous_axe = $instrument['axe'];
				}

				$m++;
			}			
		} else {
			return false;
		}

		// For every axe, list all documents for it
		$n = 0;
		$axe_documents = array();
		$axe_documents_temp = array();
		$axe_global_docs = array();

		foreach ($arrAxes['axes'] as $idx => $axe) {
			if (isset($axe['documents']) && $axe['documents'] != null) {
			
			foreach ($axe['documents'] as $documents) {				
				foreach ($documents as $document) {
					foreach ($document as $doc) {
						array_push($axe_documents_temp, $doc); 		
						$axe_documents = $axe_documents_temp;
					}
					
				}	
			}

			// Remove duplicated elements for documents list
			$doc_temp=array();
			foreach ($axe_documents as $index => $temp) {
			    if (isset($doc_temp[$temp["id"]])) {
			        unset($axe_documents[$index]);
			        continue;
			    }
			    $doc_temp[$temp["id"]]=true;
			}

			// Quit old list and add new document list to response
			unset($arrAxes['axes'][$idx]["documents"]);
			$arrAxes['axes'][$idx]["docs"] = $axe_documents;
			
			// Clean arrays
			unset($axe_documents);
			unset($axe_documents_temp);
			$axe_documents = array();
			$axe_documents_temp = array();

			$n++;

			}		
		}

		$arrAxes['all_variables'] = $all_variables;
		$arrAxes['all_instruments'] = $all_instruments;
		/*var_dump($arrAxes);*/
		return $arrAxes;


	} /* }}} */	


	//---------------------------------------------------------------------------------

	// Function updated to support document types and publish groups
	//
	function getDiamondYM($year, $month) { /* {{{ */
	
		$diamond = array();

		$queryMain = "
			SELECT a.`id` axeID,
			       a.`name` axeName,
			       a.`maxScore` axeMaxScore,
			       i.`id` instrumentID,
			       i.`name` instrumentName,
			       i.`weight` instrumentWeight,
			       iv.`id` varID,
			       iv.`code` varCode,
			       iv.`name` varName,
			       iv.`is_manual` isManual,
			       iv.`weight` varWeight
			       
			      FROM `tblAxes` a
			INNER JOIN `tblAxeMembers` am
			        ON a.id = am.`axeID`
			INNER JOIN `tblInstruments` i
			        ON i.`id` = am.`instrumentID`
			 LEFT JOIN `tblInstrumentVars` iv
			        ON am.`instrumentID` = iv.`instrumentID`
			 
			  ORDER BY a.id, am.`instrumentID`, iv.id;
		";

		$resArr = $this->db->getResultArray($queryMain);

		/*var_dump($resArr);*/
		

		if (is_bool($resArr) && !$resArr)
			return false;
		
		$count = count($resArr);
		$i = 0;
		$var_total = 0;
		$arrVar = array();
		$period = (string)$month."-".(string)$year;

		// Get month's name
		$monthNum  = $month;
		$dateObj   = DateTime::createFromFormat('!m', $monthNum);
		$monthName = $dateObj->format('M'); // Month Name
		
		foreach($resArr as $idx => $row) {

			if($row['isManual'] != null && $row['isManual'] === '1'){
				$accomplish = $this->getManualVariableEval($row['axeID'], $row['instrumentID'], $row['varID'], $period);

				if(!empty($accomplish) && !is_bool($accomplish)){
	                if ($accomplish[0]['accomplish'] === "1") {
	                		$arrVar[$i]['var_score'] = (int)$row['varWeight'] * 1;
							$arrVar[$i]['var_accomplished'] = 1;
							$arrVar[$i]['documents'] = null;
	                		$arrVar[$i]['axe'] = $row['axeName'];
							$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
							$arrVar[$i]['instrument'] = $row['instrumentName'];
							$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
							$arrVar[$i]['var'] = $row['varName'];
							$arrVar[$i]['var_weight'] = $row['varWeight'];
							$arrVar[$i]['varCode'] = $row['varCode'];
	                } else {
	                		$arrVar[$i]['var_score'] = 0;
							$arrVar[$i]['var_accomplished'] = 0;
	                		$arrVar[$i]['axe'] = $row['axeName'];
							$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
							$arrVar[$i]['instrument'] = $row['instrumentName'];
							$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
							$arrVar[$i]['var'] = $row['varName'];
							$arrVar[$i]['var_weight'] = $row['varWeight'];
							$arrVar[$i]['varCode'] = $row['varCode'];
	                
	                }

	            } else {
	                	$arrVar[$i]['var_score'] = 0;
						$arrVar[$i]['var_accomplished'] = 0;
	                	$arrVar[$i]['axe'] = $row['axeName'];
						$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
						$arrVar[$i]['instrument'] = $row['instrumentName'];
						$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
						$arrVar[$i]['var'] = $row['varName'];
						$arrVar[$i]['var_weight'] = $row['varWeight'];
						$arrVar[$i]['varCode'] = $row['varCode'];
	            }

				$i++;

			} else {

				$query = "
				SELECT 	ivsch.`typeID` varDocType,
						ivsch.`year` year,
						ivsch.`groupID` groupID,
				 		ivsch.`evalJan` varEvalJan,
				       	ivsch.`evalFeb` varEvalFeb,
				       	ivsch.`evalMar` varEvalMar,
				       	ivsch.`evalApr` varEvalApr,
				       	ivsch.`evalMay` varEvalMay,
				       	ivsch.`evalJun` varEvalJun,
				       	ivsch.`evalJul` varEvalJul,
				       	ivsch.`evalAug` varEvalAug,
				       	ivsch.`evalSep` varEvalSep,
				       	ivsch.`evalOct` varEvalOct,
				       	ivsch.`evalNov` varEvalNov,
				       	ivsch.`evalDec` varEvalDec										       	

			    FROM `tblInstrumentVarScheduling` ivsch
			    WHERE ivsch.`variableID` = ".(int)$row['varID']." AND ivsch.`year` = ".(int)$year;

				$result_from_var = $this->db->getResultArray($query);

				$requiredDocuments = 0;
				$real_doc_count = 0;
				$real_docs = array();

				if (!empty($result_from_var)) {

					foreach ($result_from_var as $row_var) {
						$requiredDocs = (int)$row_var['varEval'.$monthName];

						if ($requiredDocs >= 1) {
							$requiredDocuments += $requiredDocs;
								
								$documentsByType = "SELECT * FROM tblDocumentType WHERE typeID = ".(int)$row_var['varDocType'].";";
								$documents = $this->db->getResultArray($documentsByType);
								
								if(!empty($documents)){
									$doc_count = count($documents);
									$real_docs_by_group = 0;
									
									foreach ($documents as $doc) {
										$temp_doc = $this->getDocument((int)$doc['documentID']); // Get Document by Id
										
										// Find the group owner
										$owner = $temp_doc->getOwner();
									    $groups = $owner->getGroups();
									    if($groups) {
									        $tmp = [];
									        foreach($groups as $group){
									            $tmp[] = array(
											        'type'=>'group',
											        'id'=>(int)$group->getID(),
											        'name'=>$group->getName(),
											        'comment'=>$group->getComment(),
											    );
									        }

									        $groupID = $tmp[0]['id'];

									    } else {
									        $groupID = 0;
									    }

									    if ((int)$row_var['groupID'] === (int)$groupID) {
									    	
									
											$lc = $temp_doc->getLatestContent();
											$status_data = $lc->getStatus();
											$doc_status = $status_data['status']; // Get document status: 2 == "Released"

											// Get document versions
											$versions = array();
										    $lcs = $temp_doc->getContent();
										    foreach($lcs as $lcc) {
										        $versions[] = array(
										            'version'=>$lcc->getVersion(),
										            'date'=>date('Y-m-d H:i:s', $lcc->getDate()),
										            'workflow_log' =>$lcc->getWorkflowLog(),
										        );
										    }

										    // Get the actual version of document workflow if exists or not
										    $versions_counter = count($versions);
										    $actual_version = $versions[$versions_counter - 1];

										    // Check if log exists for last version and get the updated date. If not, get document's original date
										    $workflow_log = $versions[$versions_counter - 1]['workflow_log'];
										    if (!empty($workflow_log)) {
										        $log_counter = count($workflow_log);
										        if ($log_counter >= 1) {
										            $doc_timestamp = strtotime($workflow_log[$log_counter - 1]->getDate());
										            $updated_doc_date = date('d-m-Y',$doc_timestamp);
										            $version_month = date('m',$doc_timestamp);
										            $version_year = date('Y',$doc_timestamp);
										        }
										    } else {
										       $version_month = date('m', $temp_doc->getDate()); 
										       $version_year = date('Y', $temp_doc->getDate());
										       $updated_doc_date = date('d-m-Y', $temp_doc->getDate());
										    }

										    if ($doc_status === '2' && (int)$version_month === (int)$month && (int)$version_year === (int)$year) {

										    	$real_docs[$real_doc_count]['id'] = $temp_doc->getID();
										    	$real_docs[$real_doc_count]['name'] = $temp_doc->getName();
										    	$real_docs[$real_doc_count]['updated_date'] = $updated_doc_date;

										    	$real_doc_count++;						   
										    }
									    }
									}
								}
						}
					}
				}

				/*if($requiredDocuments > 0) {
					var_dump($requiredDocuments);
					var_dump($real_doc_count);	
				}*/
				

				if ((int)$requiredDocuments >= 1) {
					// If variable documents count is == 1 and real documents count is == 1
					if ((int)$requiredDocuments === 1 && (int)$real_doc_count === 1) {								
						$arrVar[$i]['var_score'] = $row['varWeight'] * 1;
						$arrVar[$i]['var_accomplished'] = 1;
						$arrVar[$i]['documents'] = $real_docs;

					} else if ((int)$requiredDocuments === 1 && (int)$real_doc_count >= 1) {								
						$arrVar[$i]['var_score'] = $row['varWeight'] * 1;
						$arrVar[$i]['var_accomplished'] = 1;
						$arrVar[$i]['documents'] = $real_docs;
					
					} else if ((int)$requiredDocuments > 1 && (int)$real_doc_count >= 1) {								
						$arrVar[$i]['var_score'] = $row['varWeight'] * ((($real_doc_count / $requiredDocuments) > 1) ? 1 : ($real_doc_count / $requiredDocuments));
						$arrVar[$i]['var_accomplished'] = (($real_doc_count / $requiredDocuments) > 1) ? 1 : ($real_doc_count / $requiredDocuments);
						$arrVar[$i]['documents'] = $real_docs;
					} else if ((int)$requiredDocuments >= 1 && (int)$real_doc_count === 0) {							
						$arrVar[$i]['var_score'] = 0;
						$arrVar[$i]['var_accomplished'] = 0;
					} else {
						$arrVar[$i]['var_score'] = null;
						$arrVar[$i]['var_accomplished'] = 0;
					}

					$arrVar[$i]['axe'] = $row['axeName'];
					$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
					$arrVar[$i]['instrument'] = $row['instrumentName'];
					$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
					$arrVar[$i]['var'] = $row['varName'];
					$arrVar[$i]['var_weight'] = $row['varWeight'];
					$arrVar[$i]['varCode'] = $row['varCode'];

					$i++;

				} else if (!empty($result_from_var) && !(int)$requiredDocuments) {
					$arrVar[$i]['var_score'] = $row['varWeight'] * 1;
					$arrVar[$i]['var_accomplished'] = 1;
					$arrVar[$i]['documents'] = array();
					$arrVar[$i]['axe'] = $row['axeName'];
					$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
					$arrVar[$i]['instrument'] = $row['instrumentName'];
					$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
					$arrVar[$i]['var'] = $row['varName'];
					$arrVar[$i]['var_weight'] = $row['varWeight'];
					$arrVar[$i]['varCode'] = $row['varCode'];					
					
					$i++;	
				
				} else if (empty($result_from_var) && !(int)$requiredDocuments) {

					// No evaluated variable has not a score
					$arrVar[$i]['axe'] = $row['axeName'];
					$arrVar[$i]['axe_max_score'] = $row['axeMaxScore'];
					$arrVar[$i]['instrument'] = $row['instrumentName'];
					$arrVar[$i]['instrument_weight'] = $row['instrumentWeight'];
					$arrVar[$i]['var'] = $row['varName'];
					$arrVar[$i]['var_weight'] = $row['varWeight'];
					$arrVar[$i]['varCode'] = $row['varCode'];
					$arrVar[$i]['var_accomplished'] = 0;
					$arrVar[$i]['var_score'] = null;

					$i++;
				}	

			}
		
		}		

		/*var_dump($arrVar);*/
		
		$all_variables = $arrVar;

		// Scan and get total for all instruments
		if (!empty($arrVar)) {

			$arrInst = array();
			$prev_axe = "";
			$prev_inst = "";
			
			$j = 0;
			$k = 0;
			$inst_docs = array();
			$inst_points = 0;
			foreach ($arrVar as $var) {			
				if ($k == 0) {
					$prev_inst = $var['instrument'];
					$prev_axe = $var['axe'];
				}

				if ($var['instrument'] == $prev_inst && $var['axe'] == $prev_axe) {
					$arrInst[$j]['axe'] = $var['axe'];
					$arrInst[$j]['axe_max_score'] = $var['axe_max_score'];
					$arrInst[$j]['instrument'] = $var['instrument'];
					$arrInst[$j]['instrument_weight'] = $var['instrument_weight'];
					$inst_points += $var['var_score'];
					$arrInst[$j]['points'] = $inst_points;
					
					if (isset($var['documents']) && $var['documents'] != null) {
						array_push($inst_docs, $var['documents']);
						$arrInst[$j]['documents'] = $inst_docs;	
					}		

				} else {
					$j++;
					$inst_points = 0;
					unset($inst_docs);
					$inst_docs = array();
					$arrInst[$j]['axe'] = $var['axe'];
					$arrInst[$j]['axe_max_score'] = $var['axe_max_score'];
					$arrInst[$j]['instrument'] = $var['instrument'];
					$arrInst[$j]['instrument_weight'] = $var['instrument_weight'];
					$inst_points += $var['var_score'];
					$arrInst[$j]['points'] = $inst_points;
					
					if (isset($var['documents']) && $var['documents'] != null) {
						array_push($inst_docs, $var['documents']);
						$arrInst[$j]['documents'] = $inst_docs;	
					}
				}

				if ($var['instrument'] != $prev_inst) {
					$prev_inst = $var['instrument'];
				}

				if ($var['axe'] != $prev_axe) {
					$prev_axe = $var['axe'];
					
				}

				$k++;
			}			
		} else {
			return false;
		}

		//var_dump($arrInst);
		$all_instruments = $arrInst;

		// Scan and get total for all instruments
		if (!empty($arrInst)) {

			$arrAxes = array();
			$previous_axe = "";
			
			$l = 0;
			$m = 0;
			$axe_docs = array();
			$axe_score = 0;
			foreach ($arrInst as $instrument) {			
				if ($m == 0) {
					$previous_axe = $instrument['axe'];
				}

				if ($instrument['axe'] == $previous_axe) {
					
					$arrAxes['axes'][$l]['name'] = $instrument['axe'];
					$arrAxes['axes'][$l]['maxScore'] = $instrument['axe_max_score'];
					$axe_score += ((int)$instrument['instrument_weight'] * (int)$instrument['points']) / 100;	
					$arrAxes['axes'][$l]['score'] = $axe_score;

					if (isset($instrument['documents']) && $instrument['documents'] != null) {
						array_push($axe_docs, $instrument['documents']);
						$arrAxes['axes'][$l]['documents'] = $axe_docs;	
					}

				} else {
					$l++;
					$axe_score = 0;
					unset($axe_docs);
					$axe_docs = array();
					
					$arrAxes['axes'][$l]['name'] = $instrument['axe'];
					$arrAxes['axes'][$l]['maxScore'] = $instrument['axe_max_score'];
					$axe_score += ((int)$instrument['instrument_weight'] * (int)$instrument['points']) / 100;	
					$arrAxes['axes'][$l]['score'] = $axe_score;

					if (isset($instrument['documents']) && $instrument['documents'] != null) {
						array_push($axe_docs, $instrument['documents']);
						$arrAxes['axes'][$l]['documents'] = $axe_docs;	
					}
					
				}

				if ($instrument['axe'] != $previous_axe) {
					$previous_axe = $instrument['axe'];
				}

				$m++;
			}			
		} else {
			return false;
		}

		// For every axe, list all documents for it
		$n = 0;
		$axe_documents = array();
		$axe_documents_temp = array();
		$axe_global_docs = array();

		foreach ($arrAxes['axes'] as $idx => $axe) {
			if (isset($axe['documents']) && $axe['documents'] != null) {
			
			foreach ($axe['documents'] as $documents) {				
				foreach ($documents as $document) {
					foreach ($document as $doc) {
						array_push($axe_documents_temp, $doc); 		
						$axe_documents = $axe_documents_temp;
					}
					
				}	
			}

			// Remove duplicated elements for documents list
			$doc_temp=array();
			foreach ($axe_documents as $index => $temp) {
			    if (isset($doc_temp[$temp["id"]])) {
			        unset($axe_documents[$index]);
			        continue;
			    }
			    $doc_temp[$temp["id"]]=true;
			}

			// Quit old list and add new document list to response
			unset($arrAxes['axes'][$idx]["documents"]);
			$arrAxes['axes'][$idx]["docs"] = $axe_documents;
			
			// Clean arrays
			unset($axe_documents);
			unset($axe_documents_temp);
			$axe_documents = array();
			$axe_documents_temp = array();

			$n++;

			}		
		}

		$arrAxes['all_variables'] = $all_variables;
		$arrAxes['all_instruments'] = $all_instruments;
		/*var_dump($arrAxes);*/
		return $arrAxes;


	} /* }}} */	


	//---------------------------------------------------------------------------------



	
// 	function getAllEvalDates() { /* {{{ */
// 		$queryStr = "SELECT DISTINCT `date` FROM `tblInstrumentEvals`";
// 		$resArr = $this->db->getResultArray($queryStr);
// 		if (is_bool($resArr) && !$resArr)
// 			return false;
// 		else
// 			return $resArr;
// 	} /* }}} */
	
	function getInstrumentEvalsByDate($date) {
		$classname = $this->classnames['instrumenteval'];
		return $classname::getAllInstances($date, $this);
	}
	
	/**
	 * Add a new instrument
	 *
	 * @param string $name instrument name
	 * @param string $weight weight of new instrument
	 * @param string $score score of new instrument
	 * @param string $comment comment of new instrument
	 * @return object of {@link SeedDMS_Core_Instrument}
	 */
	function addInstrument($name, $weight, $score, $comment) { /* {{{ */
		$db = $this->db;
		if (is_object($this->getInstrumentByName($name))) {
			return false;
		}
		if(empty($weight)) $weight = 0;
		if(empty($score)) $score = 0;
		$queryStr = "INSERT INTO `tblInstruments` (`name`, `weight`, `score`, `comment`) VALUES (".$db->qstr($name).", ".$db->qstr($weight).", ".$db->qstr($score).", ".$db->qstr($comment).")";
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;

		$instrument = $this->getInstrument($this->db->getInsertID('tblInstruments'));

		/* Check if 'onPostAddUser' callback is set */
		if(isset($this->_dms->callbacks['onPostAddInstrument'])) {
			foreach($this->_dms->callbacks['onPostInstrument'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $instrument)) {
				}
			}
		}

		return $instrument;
	} /* }}} */

	/**
	 * Return a instrument by its name
	 *
	 * This function retrieves a user from the database by its email.
	 * It is needed when the user requests a new password.
	 *
	 * @param integer $email email address of user
	 * @return object instance of {@link SeedDMS_Core_User} or false
	 */
	function getInstrumentVarByCode($code) { /* {{{ */
		$classname = $this->classnames['instrumentvar'];
		return $classname::getInstance($code, $this, 'code');
	} /* }}} */

	/**
	 * Add a new instrument
	 *
	 * @param string $name instrument name
	 * @param string $weight weight of new instrument
	 * @param string $score score of new instrument
	 * @param string $comment comment of new instrument
	 * @return object of {@link SeedDMS_Core_Instrument}
	 */
	function addInstrumentVar($instrumentID, $code, $name, $weight, $comment, $is_manual, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec) { /* {{{ */
		$db = $this->db;
		if (is_object($this->getInstrumentVarByCode($code))) {
			return false;
		}
		if(empty($weight)) $weight = 0;
		$queryStr = "INSERT INTO `tblInstrumentVars` (`instrumentID`,
													  `code`,
													  `name`, 
													  `weight`, 
													  `comment`,
													  `is_manual`,
													  `evalJan`,
													  `evalFeb`,
													  `evalMar`,
													  `evalApr`,
													  `evalMay`,
													  `evalJun`,
													  `evalJul`,
													  `evalAug`,
													  `evalSep`,
													  `evalOct`,
													  `evalNov`,
													  `evalDec`
													  ) 
											  VALUES (".$instrumentID[0].", ".
											  		    $db->qstr($code).", ".
											  		    $db->qstr($name).", ".
											  		    $db->qstr($weight).", ".
											  		    $db->qstr($comment).", ".
											  		    (int)$is_manual.", ".
											  		    (int)$evalJan.", ".
											  		    (int)$evalFeb.", ".
											  		    (int)$evalMar.", ".
											  		    (int)$evalApr.", ".
											  		    (int)$evalMay.", ".
											  		    (int)$evalJun.", ".
											  		    (int)$evalJul.", ".
											  		    (int)$evalAug.", ".
											  		    (int)$evalSep.", ".
											  		    (int)$evalOct.", ".
											  		    (int)$evalNov.", ".
											  		    (int)$evalDec.
											  		    ")";
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;

		$instrumentVar = $this->getInstrumentVar($this->db->getInsertID('tblInstrumentVars'));

		/* Check if 'onPostAddUser' callback is set */
		if(isset($this->_dms->callbacks['onPostAddInstrument'])) {
			foreach($this->_dms->callbacks['onPostInstrument'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $instrumentVar)) {
				}
			}
		}

		return $instrumentVar;
	} /* }}} */

	/**
	 * Add a new instrument eval
	 *
	 * @param string $name instrument name
	 * @param string $weight weight of new instrument
	 * @param string $score score of new instrument
	 * @param string $comment comment of new instrument
	 * @return object of {@link SeedDMS_Core_Instrument}
	 */
	function addInstrumentEval($instrumentID, $date, $score, $result, $comment) { /* {{{ */
		$db = $this->db;
		if (count($this->getInstrumentEval($instrumentID, $date)) > 0) {
			return false;
		}
		if(empty($result)) $weight = 0;
		if(empty($score)) $score = 0;
		$queryStr = "INSERT INTO `tblInstrumentEvals` (`instrumentID`, `date`, `score`, `result` ,`comment`) VALUES (".$instrumentID.", ".$db->qstr($date).", ".$score.", ".$result.", ".$db->qstr($comment).")";
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	} /* }}} */

	/**
	 * Update existing instrument eval
	 *
	 * @param string $name instrument name
	 * @param string $weight weight of new instrument
	 * @param string $score score of new instrument
	 * @param string $comment comment of new instrument
	 * @return object of {@link SeedDMS_Core_Instrument}
	 */
	function updateInstrumentEval($instrumentID, $date, $score, $result, $comment) { /* {{{ */
		$db = $this->db;
		if (!$this->getInstrumentEval($instrumentID, $date)) {
			return false;
		}
		if(empty($result)) $weight = 0;
		if(empty($score)) $score = 0;
		$queryStr = "UPDATE `tblInstrumentEvals` SET `score`=".$score.", `result`=".$result." ,`comment`=".$db->qstr($comment);
		$queryStr .= " WHERE `instrumentID` = ".$instrumentID." AND `date` = ".$db->qstr($date);
		
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	} /* }}} */
	
	/**
	 * Add a new user
	 *
	 * @param string $login login name
	 * @param string $pwd password of new user
	 * @param string $email Email of new user
	 * @param string $language language of new user
	 * @param string $comment comment of new user
	 * @param integer $role role of new user (can be 0=normal, 1=admin, 2=guest)
	 * @param integer $isHidden hide user in all lists, if this is set login
	 *        is still allowed
	 * @param integer $isDisabled disable user and prevent login
	 * @return object of {@link SeedDMS_Core_User}
	 */
	function addUser($login, $pwd, $fullName, $email, $language, $theme, $comment, $role='0', $isHidden=0, $isDisabled=0, $pwdexpiration='', $quota=0, $homefolder=null, $code=0) { /* {{{ */
		$db = $this->db;
		if (is_object($this->getUserByLogin($login))) {
			return false;
		}
		if($role == '')
			$role = '0';
		if(trim($pwdexpiration) == '' || trim($pwdexpiration) == 'never') {
			$pwdexpiration = 'NULL';
		} elseif(trim($pwdexpiration) == 'now') {
			$pwdexpiration = $db->qstr(date('Y-m-d H:i:s'));
		} else {
			$pwdexpiration = $db->qstr($pwdexpiration);
		}
		$queryStr = "INSERT INTO `tblUsers` (`login`, `pwd`, `fullName`, `email`, `language`, `theme`, `comment`, `role`, `hidden`, `disabled`, `pwdExpiration`, `quota`, `homefolder`, `code`) VALUES (".$db->qstr($login).", ".$db->qstr($pwd).", ".$db->qstr($fullName).", ".$db->qstr($email).", '".$language."', '".$theme."', ".$db->qstr($comment).", '".intval($role)."', '".intval($isHidden)."', '".intval($isDisabled)."', ".$pwdexpiration.", '".intval($quota)."', ".($homefolder ? intval($homefolder) : "NULL").", ".($code ? $db->qstr($code) : "0").")";

		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;

		$user = $this->getUser($this->db->getInsertID('tblUsers'));

		/* Check if 'onPostAddUser' callback is set */
		if(isset($this->_dms->callbacks['onPostAddUser'])) {
			foreach($this->_dms->callbacks['onPostUser'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $user)) {
				}
			}
		}

		return $user;
	} /* }}} */

	/**
	 * Get a group by its id
	 *
	 * @param integer $id id of group
	 * @return object/boolean group or false if no group was found
	 */
	function getGroup($id) { /* {{{ */
		$classname = $this->classnames['group'];
		return $classname::getInstance($id, $this, '');
	} /* }}} */

	/**
	 * Get a group by its name
	 *
	 * @param string $name name of group
	 * @return object/boolean group or false if no group was found
	 */
	function getGroupByName($name) { /* {{{ */
		$classname = $this->classnames['group'];
		return $classname::getInstance($name, $this, 'name');
	} /* }}} */

	/**
	 * Get a list of all groups
	 *
	 * @return array array of instances of {@link SeedDMS_Core_Group}
	 */
	function getAllGroups() { /* {{{ */
		$classname = $this->classnames['group'];
		return $classname::getAllInstances('name', $this);
	} /* }}} */

	/**
	 * Create a new user group
	 *
	 * @param string $name name of group
	 * @param string $comment comment of group
	 * @return object/boolean instance of {@link SeedDMS_Core_Group} or false in
	 *         case of an error.
	 */
	function addGroup($name, $code, $comment, $categories=[], $departmentID = null) { /* {{{ */
		if (is_object($this->getGroupByName($name))) {
			return false;
		}

		$queryStr = "INSERT INTO `tblGroups` (`departmentID`, `code`,`name`, `comment`) VALUES (".$departmentID.", ".$this->db->qstr($code).", ".$this->db->qstr($name).", ".$this->db->qstr($comment).")";
		if (!$this->db->getResult($queryStr))
			return false;

		$groupid = $this->db->getInsertID('tblGroups');
		$group = $this->getGroup($groupid);
		
		if(count($categories) > 0 ) {
//			$attribute = $this->getAttributeDefinitionByName($name);
			$group->setCategories($categories);
		}		

		/* Check if 'onPostAddGroup' callback is set */
		if(isset($this->_dms->callbacks['onPostAddGroup'])) {
			foreach($this->_dms->callbacks['onPostAddGroup'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $group)) {
				}
			}
		}

		return $group;
	} /* }}} */
	
////////
	/**
	 * Get a axe by its id
	 *
	 * @param integer $id id of group
	 * @return object/boolean group or false if no group was found
	 */
	function getAxe($id) { /* {{{ */
		$classname = $this->classnames['axe'];
		return $classname::getInstance($id, $this, '');
	} /* }}} */

	/**
	 * Get a axe by its name
	 *
	 * @param string $name name of group
	 * @return object/boolean group or false if no group was found
	 */
	function getAxeByName($name) { /* {{{ */
		$classname = $this->classnames['axe'];
		return $classname::getInstance($name, $this, 'name');
	} /* }}} */

	/**
	 * Get a list of all axes
	 *
	 * @return array array of instances of {@link SeedDMS_Core_Group}
	 */
	function getAllAxes() { /* {{{ */
		$classname = $this->classnames['axe'];
		return $classname::getAllInstances('name', $this);
	} /* }}} */

	/**
	 * Create a new user group
	 *
	 * @param string $name name of group
	 * @param string $comment comment of group
	 * @return object/boolean instance of {@link SeedDMS_Core_Group} or false in
	 *         case of an error.
	 */
	function addAxe($name, $comment, $maxScore=100) { /* {{{ */
		if (is_object($this->getAxeByName($name))) {
			return false;
		}
		
		if(empty($maxScore)) $maxScore = 100;

		$queryStr = "INSERT INTO `tblAxes` (`name`, `comment`, `maxScore`) VALUES (".$this->db->qstr($name).", ".$this->db->qstr($comment).", ".$maxScore.")";
		if (!$this->db->getResult($queryStr))
			return false;

		$axe = $this->getAxe($this->db->getInsertID('tblAxes'));

		/* Check if 'onPostAddGroup' callback is set */
		if(isset($this->_dms->callbacks['onPostAddAxe'])) {
			foreach($this->_dms->callbacks['onPostAddAxe'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $axe)) {
				}
			}
		}

		return $axe;
	} /* }}} */

	/**
	 * Get a dep by its id
	 *
	 * @param integer $id id of group
	 * @return object/boolean group or false if no group was found
	 */
	function getDepartment($id) { /* {{{ */
		$classname = $this->classnames['department'];
		return $classname::getInstance($id, $this, '');
	} /* }}} */

	/**
	 * Get a dep by its name
	 *
	 * @param string $name name of group
	 * @return object/boolean group or false if no group was found
	 */
	function getDepartmentByName($name) { /* {{{ */
		$classname = $this->classnames['department'];
		return $classname::getInstance($name, $this, 'name');
	} /* }}} */

	/**
	 * Get a list of all departments
	 *
	 * @return array array of instances of {@link SeedDMS_Core_Group}
	 */
	function getAllDepartments() { /* {{{ */
		$classname = $this->classnames['department'];
		return $classname::getAllInstances('name', $this);
	} /* }}} */

	/**
	 * Create a new department
	 *
	 * @param string $name name of group
	 * @param string $comment comment of group
	 * @return object/boolean instance of {@link SeedDMS_Core_Group} or false in
	 *         case of an error.
	 */
	function addDepartment($name, $comment, $code=100) { /* {{{ */
		if (is_object($this->getDepartmentByName($name))) {
			return false;
		}

		$queryStr = "INSERT INTO `tblDepartments` (`name`, `comment`, `code`) VALUES (".$this->db->qstr($name).", ".$this->db->qstr($comment).", ".$this->db->qstr($code).")";
		if (!$this->db->getResult($queryStr))
			return false;

		$dep = $this->getDepartment($this->db->getInsertID('tblDepartments'));

		/* Check if 'onPostAddGroup' callback is set */
		if(isset($this->_dms->callbacks['onPostAddAxe'])) {
			foreach($this->_dms->callbacks['onPostAddAxe'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $axe)) {
				}
			}
		}

		return $dep;
	} /* }}} */

	function getKeywordCategory($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblKeywordCategories` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1))
			return false;

		$resArr = $resArr[0];
		$cat = new SeedDMS_Core_Keywordcategory($resArr["id"], $resArr["owner"], $resArr["name"]);
		$cat->setDMS($this);
		return $cat;
	} /* }}} */

	function getKeywordCategoryByName($name, $userID) { /* {{{ */
		$queryStr = "SELECT * FROM `tblKeywordCategories` WHERE `name` = " . $this->db->qstr($name) . " AND `owner` = " . (int) $userID;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1))
			return false;

		$resArr = $resArr[0];
		$cat = new SeedDMS_Core_Keywordcategory($resArr["id"], $resArr["owner"], $resArr["name"]);
		$cat->setDMS($this);
		return $cat;
	} /* }}} */

	function getAllKeywordCategories($userIDs = array()) { /* {{{ */
		$queryStr = "SELECT * FROM `tblKeywordCategories`";
		if ($userIDs)
			$queryStr .= " WHERE `owner` IN (".implode(',', $userIDs).")";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$categories = array();
		foreach ($resArr as $row) {
			$cat = new SeedDMS_Core_KeywordCategory($row["id"], $row["owner"], $row["name"]);
			$cat->setDMS($this);
			array_push($categories, $cat);
		}

		return $categories;
	} /* }}} */

	/**
	 * This function should be replaced by getAllKeywordCategories()
	 */
	function getAllUserKeywordCategories($userID) { /* {{{ */
		$queryStr = "SELECT * FROM `tblKeywordCategories`";
		if ($userID != -1)
			$queryStr .= " WHERE `owner` = " . (int) $userID;

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$categories = array();
		foreach ($resArr as $row) {
			$cat = new SeedDMS_Core_KeywordCategory($row["id"], $row["owner"], $row["name"]);
			$cat->setDMS($this);
			array_push($categories, $cat);
		}

		return $categories;
	} /* }}} */

	function addKeywordCategory($userID, $name) { /* {{{ */
		if (is_object($this->getKeywordCategoryByName($name, $userID))) {
			return false;
		}
		$queryStr = "INSERT INTO `tblKeywordCategories` (`owner`, `name`) VALUES (".(int) $userID.", ".$this->db->qstr($name).")";
		if (!$this->db->getResult($queryStr))
			return false;

		$category = $this->getKeywordCategory($this->db->getInsertID('tblKeywordCategories'));

		/* Check if 'onPostAddKeywordCategory' callback is set */
		if(isset($this->_dms->callbacks['onPostAddKeywordCategory'])) {
			foreach($this->_dms->callbacks['onPostAddKeywordCategory'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $category)) {
				}
			}
		}

		return $category;
	} /* }}} */

	function getDocumentCategory($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblCategory` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1))
			return false;

		$resArr = $resArr[0];
		$cat = new SeedDMS_Core_DocumentCategory($resArr["id"], $resArr["name"]);
		$cat->setDMS($this);
		return $cat;
	} /* }}} */

	function getDocumentCategories() { /* {{{ */
		$queryStr = "SELECT * FROM `tblCategory` order by `name`";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$categories = array();
		foreach ($resArr as $row) {
			$cat = new SeedDMS_Core_DocumentCategory($row["id"], $row["name"]);
			$cat->setDMS($this);
			array_push($categories, $cat);
		}

		return $categories;
	} /* }}} */
	
	function getTypeCategory($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblCategory` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1))
			return false;

		$resArr = $resArr[0];
		$cat = new SeedDMS_Core_TypeCategory($resArr["id"], $resArr["name"]);
		$cat->setDMS($this);
		return $cat;
	} /* }}} */

	function getTypeCategories() { /* {{{ */
		$queryStr = "SELECT * FROM `tblCategory` order by `name`";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$categories = array();
		foreach ($resArr as $row) {
			$cat = new SeedDMS_Core_DocumentCategory($row["id"], $row["name"]);
			$cat->setDMS($this);
			array_push($categories, $cat);
		}

		return $categories;
	} /* }}} */

	/**
	 * Get a category by its name
	 *
	 * The name of a category is by default unique.
	 *
	 * @param string $name human readable name of category
	 * @return object instance of {@link SeedDMS_Core_DocumentCategory}
	 */
	function getDocumentCategoryByName($name) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblCategory` where `name`=".$this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);
		if (!$resArr)
			return false;

		$row = $resArr[0];
		$cat = new SeedDMS_Core_DocumentCategory($row["id"], $row["name"]);
		$cat->setDMS($this);

		return $cat;
	} /* }}} */

	function addDocumentCategory($name) { /* {{{ */
		if (is_object($this->getDocumentCategoryByName($name))) {
			return false;
		}
		$queryStr = "INSERT INTO `tblCategory` (`name`) VALUES (".$this->db->qstr($name).")";
		if (!$this->db->getResult($queryStr))
			return false;

		$category = $this->getDocumentCategory($this->db->getInsertID('tblCategory'));

		/* Check if 'onPostAddDocumentCategory' callback is set */
		if(isset($this->_dms->callbacks['onPostAddDocumentCategory'])) {
			foreach($this->_dms->callbacks['onPostAddDocumentCategory'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $category)) {
				}
			}
		}

		return $category;
	} /* }}} */

	function getDocumentType($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblType` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1))
			return false;

		$resArr = $resArr[0];
		$type = new SeedDMS_Core_DocumentType($resArr["id"], $resArr["name"], $resArr["codeMask"]);
		$type->setDMS($this);
		return $type;
	} /* }}} */

	function getTypesByCategoryID($id){ /* {{{ */
		$queryStr = "SELECT * FROM `tblType` where `id` in (select `typeID` from `tblTypeCategory` where `categoryID` = ".(int)$id.")";

		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	} /* }}} */

	function getDocumentTypes() { /* {{{ */
		$queryStr = "SELECT * FROM `tblType` order by `name`";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$types = array();
		foreach ($resArr as $row) {
			$type = new SeedDMS_Core_DocumentType($row["id"], $row["name"], $row["codeMask"]);
			$type->setDMS($this);
			array_push($types, $type);
		}

		return $types;
	} /* }}} */

	function getSensitiveWords(){ /* {{{ */
		$queryStr = "SELECT * FROM `tblSensitiveWords` order by `name`";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	} /* }}} */

	function getSensitiveWordByName($name) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblSensitiveWords` where `name`=".$this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);
		if (!$resArr){
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function addSensitiveWord($name) { /* {{{ */
		$queryStr = "INSERT INTO `tblSensitiveWords` (`name`) VALUES (".$this->db->qstr($name).")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}

	} /* }}} */

	function getSensitiveWord($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblSensitiveWords` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1)){
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function delSensitiveWord($id) { /* {{{ */
		$queryStr = "DELETE from `tblSensitiveWords` WHERE `id` = " . (int) $id;
		if (!$this->db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		return true;
	} /* }}} */

	// Inspections
	function getInspection($id){ /* {{{ */
		$queryStr = "SELECT * FROM `tblInspection` WHERE `id` = ".$id;

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	} /* }}} */

	function getAllInspections(){ /* {{{ */
		$queryStr = "SELECT * FROM `tblInspection` order by `id`";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	} /* }}} */

	function addInspection($year, $start, $end){
		$queryStr = "INSERT INTO `tblInspection`(`year`, `duration_start`, `duration_stop`) VALUES (".$this->db->qstr($year).", ".$this->db->qstr($start).", ".$this->db->qstr($end).")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function updateInspection($id, $year, $start, $stop){
		$queryStr = "UPDATE `tblInspection` SET `year`= ".$this->db->qstr($year).",`duration_start`=".$this->db->qstr($start).",`duration_stop`= ".$this->db->qstr($stop)." WHERE `id` = ".(int)$id."";
		
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	}

	function getAllThematics(){
		$queryStr = "SELECT * FROM `tblThematics` order by `id`";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	}

	function getItemsByThematic($id){
		$queryStr = "SELECT `id`, `thematicID`, `name`, `comment`, `qualification_type`, `evaluation_type` FROM `tblThematicItems` WHERE `thematicID` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	}

	function deleteInspection($id){
		$queryStr = "DELETE from `tblInspection` WHERE `id` = " . (int) $id;
		if (!$this->db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		return true;
	}

	// Ends Inspections

	// Thematics
	function addThematic($name, $comment) { /* {{{ */
		$queryStr = "INSERT INTO `tblThematics`(`name`, `comment`) VALUES (".$this->db->qstr($name).", ".$this->db->qstr($comment).")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function getThematicById($id){
		$queryStr = "SELECT * FROM `tblThematics` WHERE `id` = ".$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	}

	function updateThematic($id, $name, $comment){
		$queryStr = "UPDATE `tblThematics` SET `name`= ".$this->db->qstr($name).",`comment`=".$this->db->qstr($comment)." WHERE `id` = ".$id;
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	}

	function deleteThematic($id){
		$queryStr = "DELETE from `tblThematics` WHERE `id` = " . (int) $id;
		if (!$this->db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		return true;
	}

	// Ends Thematics


	// Items 

	function getThematicItem($id){
		$queryStr = "SELECT * FROM `tblThematicItems` WHERE `id` = ".$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	}

	function addThematicItem($thematicID, $name, $comment, $qualification_type, $evaluation_type, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec){
		$queryStr = "INSERT INTO `tblThematicItems`(`thematicID`, `name`, `comment`, `qualification_type`, `evaluation_type`, `evalJan`, `evalFeb`, `evalMar`, `evalApr`, `evalMay`, `evalJun`, `evalJul`, `evalAug`, `evalSep`, `evalOct`, `evalNov`, `evalDec`) VALUES 
		(".(int)$thematicID.", ".$this->db->qstr($name).", ".$this->db->qstr($comment).", ".(int)$qualification_type.", ".(int)$evaluation_type.", ".(int)$evalJan.", ".(int)$evalFeb.", ".(int)$evalMar.", ".(int)$evalApr.", ".(int)$evalMay.", ".(int)$evalJun.", ".(int)$evalJul.", ".(int)$evalAug.", ".(int)$evalSep.", ".(int)$evalOct.", ".(int)$evalNov.", ".(int)$evalDec.")";

		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function updateThematicItem($id, $name, $comment, $qualification_type, $evaluation_type, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec){
		$queryStr = "UPDATE `tblThematicItems` SET `name`= ".$this->db->qstr($name)." , `comment`=".$this->db->qstr($comment).",`qualification_type`=".$qualification_type.",`evaluation_type`=".$evaluation_type.",`evalJan`=".$evalJan.",`evalFeb`=".$evalFeb.",`evalMar`=".$evalMar.",`evalApr`=".$evalApr.",`evalMay`=".$evalMay.",`evalJun`=".$evalJun.",`evalJul`=".$evalJul.",`evalAug`=".$evalAug.",`evalSep`=".$evalSep.",`evalOct`=".$evalOct.",`evalNov`=".$evalNov.",`evalDec`=".$evalDec." WHERE `id` = ".$id;

	
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	}

	function deleteItem($id){
		$queryStr = "DELETE from `tblThematicItems` WHERE `id` = " . (int) $id;
		if (!$this->db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		return true;
	}

	function getAllThematicItems(){
		$queryStr = "SELECT * FROM `tblThematicItems`";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	}

	function getAllThematicItemsAuto(){
		$queryStr = "SELECT * FROM `tblThematicItems` WHERE `qualification_type` = 0 AND `evaluation_type` = 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	}

	function getItemsByType($id) { /* {{{ */
		$queryStr = "SELECT * 
		 			  FROM `tblThematicItems` 
		              WHERE `id` IN (SELECT `thematicItemID` 
		              FROM `tblThematicItemType` 
		              WHERE `typeID` = ".(int)$id.")";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return [];
			
		return $resArr;
	}

	function addThematicItemType($typeID, $typeItems){

		$this->db->startTransaction();
		$queryStr = "DELETE from `tblThematicItemType` WHERE `typeID` = ". (int)$typeID;

		if (!$this->db->getResult($queryStr)) {
			$this->db->rollbackTransaction();
			return false;
		}

		foreach($typeItems as $item) {
			$queryStr = "INSERT INTO `tblThematicItemType` (`typeID`, `thematicItemID`) VALUES (". (int)$typeID .", ". (int)$item[0]['id'] .")";

			if (!$this->db->getResult($queryStr)) {
				$this->db->rollbackTransaction();
				return false;
			}
		}

		$this->db->commitTransaction();		
		return true;
	}

	function removeThematicItemType($typeID){
		$queryStr = "DELETE from `tblThematicItemType` WHERE `typeID` = ". (int)$typeID;

		if (!$this->db->getResult($queryStr)) {
			$this->db->rollbackTransaction();
			return false;
		}

		return true;
	}

	function addInspectionManualItems($inspectionID, $items){
		$this->db->startTransaction();
		$queryStr = "DELETE from `tblInspectionManualItems` WHERE `inspectionID` = ". (int)$inspectionID;

		if (!$this->db->getResult($queryStr)) {
			$this->db->rollbackTransaction();
			return false;
		}

		foreach($items as $item) {
			$queryStr = "INSERT INTO `tblInspectionManualItems` (`inspectionID`, `itemID`, `value`) VALUES (". (int)$inspectionID .", ". (int)$item['itemID'] .", ".(int)$item['value'].")";

			if (!$this->db->getResult($queryStr)) {
				$this->db->rollbackTransaction();
				return false;
			}
		}

		$this->db->commitTransaction();		
		return true;
	}

	function addInspectionManualItem($inspectionID, $item){ /* {{{ */
		
		$queryStr = "SELECT * FROM `tblInspectionManualItems` 
									WHERE `inspectionID` = ".(int)$inspectionID." AND `itemID` = ".(int)$item['itemID'];
		
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || empty($resArr) || $resArr === null) {

			$queryStr = "INSERT INTO `tblInspectionManualItems` (`inspectionID`, `itemID`, `value`) VALUES (". (int)$inspectionID .", ". (int)$item['itemID'] .", ".(int)$item['value'].")";

			if (!$this->db->getResult($queryStr)) {			
				return false;
			}
			

		} else {
			$queryStr = "UPDATE `tblInspectionManualItems` SET `value` = ".(int)$item['value']." WHERE `inspectionID` = ".(int)$inspectionID." AND `itemID` = ".(int)$item['itemID'];
			$res = $this->db->getResult($queryStr);
			if (!$res){
				return false;
			}
			

		}

		return true;
	} /* }}} */

	function getInspectionManualItem($inspectionID, $itemID){
		$queryStr = "SELECT `value` FROM `tblInspectionManualItems` 
									WHERE `inspectionID` = ".(int)$inspectionID." AND `itemID` = ".(int)$itemID;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || !$resArr || $resArr === null || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}

	function getManualItemsByInspection($inspectionID){
		$queryStr = "SELECT `value` FROM `tblInspectionManualItems` 
									WHERE `inspectionID` = ".(int)$inspectionID;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	}

	function getManualVariableEval($axeID, $instID, $varID, $period){
		$queryStr = "SELECT `accomplish` FROM `tblInstrumentManualVarEval` 
									WHERE `axeID` = ".(int)$axeID."
									AND `instID` = ".(int)$instID." 
									AND `varID` = ".(int)$varID."
									AND `period` = ".$this->db->qstr($period);
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	}

	function addManualVariableEval($period, $var){
		$queryStr = "SELECT * FROM `tblInstrumentManualVarEval` 
									WHERE `axeID` = ".(int)$var['axeID']."
									AND `instID` = ".(int)$var['instID']." 
									AND `varID` = ".(int)$var['varID']."
									AND `period` = ".$this->db->qstr($period);
		
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || empty($resArr) || $resArr === null) {

			$queryStr = "INSERT INTO `tblInstrumentManualVarEval` (`axeID`, `instID`, `varID`, `accomplish`, `period`) VALUES (". (int)$var['axeID'] .", ". (int)$var['instID'] .", ".(int)$var['varID'].", ".(int)$var['accomplish'].", ".$this->db->qstr($period).")";

			if (!$this->db->getResult($queryStr)) {			
				return false;
			}
			

		} else {
			$queryStr = "UPDATE `tblInstrumentManualVarEval` SET `accomplish` = ".(int)$var['accomplish']." 
						WHERE `axeID` = ".(int)$var['axeID']."
						AND `instID` = ".(int)$var['instID']." 
						AND `varID` = ".(int)$var['varID']."
						AND `period` = ".$this->db->qstr($period);
			$res = $this->db->getResult($queryStr);
			if (!$res){
				return false;
			}
			
		}

		return true;
	}

	function calculateInspection($year, $start, $end){ /* {{{ */
		$queryMain = "
			SELECT 
			       t.`id` thematicID,
			       t.`name` thematicName,
			       t.`comment` thematicComment,
			       ti.`id` itemID,
			       ti.`name` itemName,
			       ti.`comment` itemComment,
			       ti.`qualification_type` qualificationType,
			       ti.`evaluation_type` evaluationType,
			       ti.`evalJan` itemEvalJan,
			       ti.`evalFeb` itemEvalFeb,
			       ti.`evalMar` itemEvalMar,
			       ti.`evalApr` itemEvalApr,
			       ti.`evalMay` itemEvalMay,
			       ti.`evalJun` itemEvalJun,
			       ti.`evalJul` itemEvalJul,
			       ti.`evalAug` itemEvalAug,
			       ti.`evalSep` itemEvalSep,
			       ti.`evalOct` itemEvalOct,
			       ti.`evalNov` itemEvalNov,
			       ti.`evalDec` itemEvalDec,
			       tit.`typeID` itemDocType
			      FROM `tblThematics` t
			INNER JOIN `tblThematicItems` ti
			        ON t.`id` = ti.`thematicID`
			 LEFT JOIN `tblThematicItemType` tit
			        ON ti.`id` = tit.`thematicItemID`
			  ORDER BY t.`id`, ti.`id`;
		";

		$resArr = $this->db->getResultArray($queryMain);

		if (is_bool($resArr) && !$resArr)
			return false;
		
		$count = count($resArr);

		list($startM, $startY) = explode("-", $start);
		list($endM, $endY) = explode("-", $end);
		$evaluations_total = 0; // Automatic and manual items to evaluate

		$startMonth = (int)$startM;
		$startYear = (int)$startY;
		$endMonth = (int)$endM;
		$endYear = (int)$endY;
 
		foreach($resArr as $idx => $row) {
			if ($row['evaluationType'] === '1') {
				$evaluations_total++;
			}
		}

		foreach($resArr as $idx => $row) {
			//var_dump($row);
		}

		$global_accomplished_items = 0;

		foreach($resArr as $idx => $row) {
			// '0' = Automatic
			// '1' = Evaluated
			// itemDocTye = is set to some document type

			if ($row['qualificationType'] === '0' && $row['evaluationType'] === '1') {
				if ($row['itemDocType'] === null) {
					$resArr[$idx]['accomplished'] = false;
				} else {
					$accomplished = false;
					// For 12 months
					for ($i=1; $i <= 12; $i++) {

						$dateObj   = DateTime::createFromFormat('!m', $i);
						$monthName = $dateObj->format('M'); // Month Name
						
						// The current month requires a document ?
						if ($row['itemEval'.$monthName] !== '0') {
							$requiredDocuments = $row['itemEval'.$monthName];
							
							// The current month is in the date range ?
							// $i . $year for the comparisson
							$actual_date = $i + (int)$year;
							$actualStart = $startMonth + $startYear;
							$actualEnd = $endMonth + $endYear;
							if ($actual_date >= $actualStart && $actual_date <= $actualEnd) {
								
								// Get the documents related by the type
								$documentsByType = "SELECT * FROM tblDocumentType 
													WHERE typeID = ".$row['itemDocType'].";";
								$documents = $this->db->getResultArray($documentsByType);

								if(!empty($documents)){
									$doc_count = count($documents);
									$real_doc_count = 0;
									$real_docs = array();
									foreach ($documents as $doc) {
										$temp_doc = $this->getDocument((int)$doc['documentID']); // Get Document by Id
							
										$lc = $temp_doc->getLatestContent();
										$status_data = $lc->getStatus();
										$doc_status = $status_data['status']; // Get document status: 2 == "Released"

										// Get document versions
										$versions = array();
									    $lcs = $temp_doc->getContent();
									    foreach($lcs as $lcc) {
									        $versions[] = array(
									            'version'=>$lcc->getVersion(),
									            'date'=>date('Y-m-d H:i:s', $lcc->getDate()),
									            'workflow_log' =>$lcc->getWorkflowLog(),
									        );
									    }

									    $versions_counter = count($versions);
									    $actual_version = $versions[$versions_counter - 1];
									    $workflow_log = $versions[$versions_counter - 1]['workflow_log'];
									    if (!empty($workflow_log)) {
									        $log_counter = count($workflow_log);
									        if ($log_counter >= 1) {
									            $doc_timestamp = strtotime($workflow_log[$log_counter - 1]->getDate());
									            
									            $version_month = date('m',$doc_timestamp);
									            $version_year = date('Y',$doc_timestamp);
									        }
									    } else {
									       $version_month = date('m', $temp_doc->getDate()); 
									       $version_year = date('Y', $temp_doc->getDate());
									       
									    }

									    // Check if document is released and
									    if ($doc_status === '2' && (int)$version_month == $i && ((int)$version_year == $startYear || (int)$version_year == $endYear)) {
									    	$real_doc_count++;
									    }
									}
								}
							}

							if ($real_doc_count >= (int)$requiredDocuments) {
								$accomplished = true;
							} else {
								$accomplished = false;
							}
						}
					} // 
					
					$resArr[$idx]['accomplished'] = $accomplished;

					if ($accomplished) {
						$global_accomplished_items++;
					}
					

				} // Ends if for types setted //
			} // Ends if for evaluated items //
		} // Ends foreach //


		//var_dump($resArr);

		$l = 1;
        $m = 0;
        $response = array();
        $items = array();

        $response['evaluations_total'] = $evaluations_total;
        $response['global_accomplished_items'] = $global_accomplished_items;
		foreach ($resArr as $index => $them) {
            if ($l == 1) {
                $prev_them = $them['thematicName'];
                $response['thematic'][$m]['thematicName'] = $them['thematicName'];
            }

            if ($them['thematicName'] == $prev_them) {
            	$item = array(
            		'itemID' => $them['itemID'],
            		'itemName' => $them['itemName'],
            		'qualificationType' => $them['qualificationType'],
            		'evaluationType' => $them['evaluationType'],
            		'itemDocType' => $them['itemDocType'],
            		'accomplished' => (isset($them['accomplished']) ? $them['accomplished'] : null),
            	);
                array_push($items,$item);    
            }
            
            if ($them['thematicName'] != $prev_them) {
                $response['thematic'][$m]['items'] = $items;
                unset($items);
                $items = array();
             
                $item = array(
                	'itemID' => $them['itemID'],
            		'itemName' => $them['itemName'],
            		'qualificationType' => $them['qualificationType'],
            		'evaluationType' => $them['evaluationType'],
            		'itemDocType' => $them['itemDocType'],
            		'accomplished' => (isset($them['accomplished']) ? $them['accomplished'] : null),
            	);
                array_push($items,$item);

                $m++;

                $prev_them = $them['thematicName'];
                $response['thematic'][$m]['thematicName'] = $them['thematicName'];
            }

            // Last var
            if (count($resArr) == $l) {
                $response['thematic'][$m]['items'] = $items;
            }

            $l++;
        }

		return $response;

	} /* }}} */


	// Ends item


	/* --- Varibles Scheduling ---- */

	function addVariableScheduling($axeID, $instID, $varID, $groupID, $typeID, $year, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec) { /* {{{ */

		$queryStr = "SELECT * FROM `tblInstrumentVarScheduling` 
									WHERE `axeID` = ".(int)$axeID." 
									AND `instrumentID` = ".(int)$instID."
									AND `variableID` = ".(int)$varID."
									AND `groupID` = ".(int)$groupID."
									AND `typeID` = ".(int)$typeID."
									AND `year` = ".(int)$year;
		
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || empty($resArr) || $resArr === null) {

			$queryStr = "INSERT INTO `tblInstrumentVarScheduling` (`axeID`, 
									`instrumentID`, 
									`variableID`, 
									`groupID`, 
									`typeID`, 
									`year`, 
									`evalJan`, 
									`evalFeb`, 
									`evalMar`, 
									`evalApr`, 
									`evalMay`, 
									`evalJun`, 
									`evalJul`, 
									`evalAug`, 
									`evalSep`, 
									`evalOct`, 
									`evalNov`, 
									`evalDec`) 
									VALUES (". 
									(int)$axeID .", ".
									(int)$instID .", ".
									(int)$varID.", ".
									(int)$groupID.", ".
									(int)$typeID.", ".
									(int)$year.", ".
									(int)$evalJan.", ".
									(int)$evalFeb.", ".
									(int)$evalMar.", ".
									(int)$evalApr.", ".
									(int)$evalMay.", ".
									(int)$evalJun.", ".
									(int)$evalJul.", ".
									(int)$evalAug.", ".
									(int)$evalSep.", ".
									(int)$evalOct.", ".
									(int)$evalNov.", ".
									(int)$evalDec.")";

			if (!$this->db->getResult($queryStr)) {			
				return false;
			}

			return $this->db->getInsertID('tblInstrumentVarScheduling');
			

		} else {
			$queryStr = "UPDATE `tblInstrumentVarScheduling` SET 	
									`evalJan` = ".(int)$evalJan.", 
									`evalFeb` = ".(int)$evalFeb.", 
									`evalMar` = ".(int)$evalMar.", 
									`evalApr` = ".(int)$evalApr.", 
									`evalMay` = ".(int)$evalMay.", 
									`evalJun` = ".(int)$evalJun.", 
									`evalJul` = ".(int)$evalJul.", 
									`evalAug` = ".(int)$evalAug.", 
									`evalSep` = ".(int)$evalSep.", 
									`evalOct` = ".(int)$evalOct.", 
									`evalNov` = ".(int)$evalNov.", 
									`evalDec` = ".(int)$evalDec." 
									WHERE `axeID` = ".(int)$axeID." 
									AND `instrumentID` = ".(int)$instID."
									AND `variableID` = ".(int)$varID."
									AND `groupID` = ".(int)$groupID."
									AND `typeID` = ".(int)$typeID."
									AND `year` = ".(int)$year;

			$res = $this->db->getResult($queryStr);
			if (!$res){
				return false;
			} else {
				return true;
			}

		}

	} /* }}} */

	function getSpecificInstrumentVarSchedule($axeID, $instID, $varID, $groupID, $typeID, $year){
		$queryStr = "SELECT * FROM `tblInstrumentVarScheduling` 
									WHERE `axeID` = ".(int)$axeID." 
									AND `instrumentID` = ".(int)$instID."
									AND `variableID` = ".(int)$varID."
									AND `groupID` = ".(int)$groupID."
									AND `typeID` = ".(int)$typeID."
									AND `year` = ".(int)$year;

		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || empty($resArr) || $resArr === null) {
			return false;
		} else {
			return $resArr;
		}
	}

	function getInstrumentVarScheduling($axe, $instrument, $variable, $year){ /* {{{ */ 
		$queryStr = "SELECT * FROM `tblInstrumentVarScheduling` 
									WHERE `axeID` = ".(int)$axe." 
									AND `instrumentID` = ".(int)$instrument."
									AND `variableID` = ".(int)$variable."
									AND `year` = ".(int)$year;
		
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || empty($resArr) || $resArr === null) {
			return false;
		} else {
			return $resArr;
		}

	} /* }}} */

	function getInstrumentVarSchedulingByID($id){ /* {{{ */ 
		$queryStr = "SELECT * FROM `tblInstrumentVarScheduling` 
									WHERE `id` = ".(int)$id;
		
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || empty($resArr) || $resArr === null) {
			return false;
		} else {
			return $resArr;
		}

	} /* }}} */


	function deleteInstrumentVariableSchedule($id){ /* {{{ */
		$this->db->startTransaction();
		$queryStr = "DELETE from `tblInstrumentVarScheduling` WHERE `id` = ". (int)$id;

		if (!$this->db->getResult($queryStr)) {
			$this->db->rollbackTransaction();
			return false;
		}

		$this->db->commitTransaction();		
		return true;
	} /* }}} */

	/* ---------------------------- */


	/**
	 * Get a category by its name
	 *
	 * The name of a category is by default unique.
	 *
	 * @param string $name human readable name of category
	 * @return object instance of {@link SeedDMS_Core_DocumentCategory}
	 */
	function getDocumentTypeByName($name) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblType` where `name`=".$this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);
		if (!$resArr)
			return false;

		$row = $resArr[0];
		$type = new SeedDMS_Core_DocumentType($row["id"], $row["name"]);
		$type->setDMS($this);

		return $type;
	} /* }}} */

	function addDocumentType($name, $codeMask) { /* {{{ */
		if (is_object($this->getDocumentTypeByName($name))) {
			return false;
		}
		$queryStr = "INSERT INTO `tblType` (`name`, `codeMask`) VALUES (".$this->db->qstr($name).", ".$this->db->qstr($codeMask)." )";
		if (!$this->db->getResult($queryStr))
			return false;

		$type = $this->getDocumentType($this->db->getInsertID('tblType'));

		/* Check if 'onPostAddDocumentCategory' callback is set */
		if(isset($this->_dms->callbacks['onPostAddDocumentType'])) {
			foreach($this->_dms->callbacks['onPostAddDocumentType'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $category)) {
				}
			}
		}

		return $type;
	} /* }}} */

	function getDocumentInstrumentVar($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblInstrumentVars` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1))
			return false;

		$resArr = $resArr[0];
		$instVar = new SeedDMS_Core_DocumentInstrumentVar($resArr["id"], $resArr["name"]);
		$instVar->setDMS($this);
		return $instVar;
	} /* }}} */

	function getDocumentInstrumentVars() { /* {{{ */
		$queryStr = "SELECT * FROM `tblInstrumentVars` order by `name`";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$instrumentVars = array();
		foreach ($resArr as $row) {
			$instVar = new SeedDMS_Core_DocumentInstrumentVar($row["id"], $row["name"]);
			$instVar->setDMS($this);
			array_push($instrumentVars, $instVar);
		}

		return $instrumentVars;
	} /* }}} */
	
	/**
	 * Get a instrument var by its name
	 *
	 * The name of a instrument var is by default unique.
	 *
	 * @param string $name human readable name of category
	 * @return object instance of {@link SeedDMS_Core_DocumentCategory}
	 */
	function getDocumentInstrumentVarByName($name) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblInstrumentVars` where `name`=".$this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);
		if (!$resArr)
			return false;

		$row = $resArr[0];
		$instVar = new SeedDMS_Core_DocumentInstrumentVar($row["id"], $row["name"]);
		$instVar->setDMS($this);

		return $instVar;
	} /* }}} */

	function getLastInWorkflowLog(){
		$queryStr = "SELECT id, document, date FROM `tblWorkflowLog` ORDER BY date DESC LIMIT 50";
		$resArr = $this->db->getResultArray($queryStr);
		if (!$resArr)
			return false;

		return $resArr;
	}

	function updateStatusLogDate($id, $date){ /* {{{ */
		$queryStr = "UPDATE `tblDocumentStatusLog` SET `date` = ".$this->db->qstr($date)." 
						WHERE `statusLogID` = ".(int)$id;
			$res = $this->db->getResult($queryStr);
			if (!$res){
				return false;
			} else {
				return true;
			}
			
	} /* }}} */
	
	/**
	 * Get a instrument var by its code
	 *
	 * The name of a instrument var is by default unique.
	 *
	 * @param string $name human readable name of category
	 * @return object instance of {@link SeedDMS_Core_DocumentCategory}
	 */
	function getDocumentInstrumentVarByCode($code) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblInstrumentVars` where `code`=".$this->db->qstr($code);
		$resArr = $this->db->getResultArray($queryStr);
		if (!$resArr)
			return false;

		$row = $resArr[0];
		$instVar = new SeedDMS_Core_DocumentInstrumentVar($row["id"], $row["name"]);
		$instVar->setDMS($this);

		return $instVar;
	} /* }}} */

	function addDocumentInstrumentVar($code, $name) { /* {{{ */
		if (is_object($this->getDocumentInstrumentVarByName($name))) {
			return false;
		}
		$queryStr = "INSERT INTO `tblInstrumentVars` (`code`, `name`) VALUES (".$this->db->qstr($code).",".$this->db->qstr($name).")";
		if (!$this->db->getResult($queryStr))
			return false;

		$instrumentVar = $this->getDocumentInstrumentVar($this->db->getInsertID('tblInstrumentVars'));

		/* Check if 'onPostAddDocumentCategory' callback is set */
		if(isset($this->_dms->callbacks['onPostAddDocumentInstrumentVar'])) {
			foreach($this->_dms->callbacks['onPostAddDocumentInstrumentVar'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $instrumentVar)) {
				}
			}
		}

		return $instrumentVar;
	} /* }}} */


	function getNotificationSchedule($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblNotifySchedules` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1))
			return false;

		$resArr = $resArr[0];
		$NotiSched = new SeedDMS_Core_NotificationSchedule($resArr["id"], $resArr["userID"], $resArr["date"], $resArr["dateSchedule"], $resArr["dateSent"], $resArr["toUsers"], $resArr["toGroups"], $resArr["subject"], $resArr["message"], $resArr["sent"]);
		$NotiSched->setDMS($this);
		return $NotiSched;
	} /* }}} */

	function addNotifySchedule($userid, $dateSchedule, $users, $groups, $subject, $message) { /* {{{ */
/*
		if (is_object($this->getDocumentInstrumentVarByName($name))) {
			return false;
		}
*/
		$date = date("Y-m-d H:i:s");
		if(empty($dateSchedule) || $dateSchedule == '') {
			$dateSchedule = $date;
		}

		if($users == 'all') {
			$toUsers = '*';
		} else {
			foreach($users as $u) {
				$toUsers[] = $u->getID();
			}
		}

		foreach($groups as $g) {
			$toGroups[] = $g->getID();
		}

		if(isset($toUsers) && is_array($toUsers)) $toUsers = implode(',',$toUsers);
		if(isset($toGroups) && is_array($toGroups)) $toGroups = implode(',',$toGroups);

		if(empty($toUsers) || $toUsers == '') $toUsers = -1;
		if(empty($toGroups) || $toGroups == '') $toGroups = -1;

		$queryStr = "INSERT INTO `tblNotifySchedules` (`userID`, `date`, `dateSchedule`, `toUsers`, `toGroups`, `subject`, `message`) VALUES (".$this->db->qstr($userid).",".$this->db->qstr($date).",".$this->db->qstr($dateSchedule).",".$this->db->qstr($toUsers).",".$this->db->qstr($toGroups).",".$this->db->qstr($subject).",".$this->db->qstr($message).")";

		if (!$this->db->getResult($queryStr))
			return false;

		$notifySchedule = $this->getNotificationSchedule($this->db->getInsertID('tblNotifySchedules'));

		/* Check if 'onPostAddDocumentCategory' callback is set */
		if(isset($this->_dms->callbacks['onPostAddNotifySchedule'])) {
			foreach($this->_dms->callbacks['onPostAddNotifySchedule'] as $callback) {
				if(!call_user_func($callback[0], $callback[1], $notifySchedule)) {
				}
			}
		}

		return $notifySchedule;
	} /* }}} */

	/**
	 * Get all notifications for a group
	 *
	 * deprecated: User {@link SeedDMS_Core_Group::getNotifications()}
	 *
	 * @param object $group group for which notifications are to be retrieved
	 * @param integer $type type of item (T_DOCUMENT or T_FOLDER)
	 * @return array array of notifications
	 */
	function getNotificationsByGroup($group, $type=0) { /* {{{ */
		return $group->getNotifications($type);
	} /* }}} */

	/**
	 * Get all notifications for a user
	 *
	 * deprecated: User {@link SeedDMS_Core_User::getNotifications()}
	 *
	 * @param object $user user for which notifications are to be retrieved
	 * @param integer $type type of item (T_DOCUMENT or T_FOLDER)
	 * @return array array of notifications
	 */
	function getNotificationsByUser($user, $type=0) { /* {{{ */
		return $user->getNotifications($type);
	} /* }}} */


	/**
	 * Get a list of all notify schedules
	 *
	 * @return array array of instances of {@link SeedDMS_Core_Group}
	 */
	function getAllNotifySchedules($userid, $sent) { /* {{{ */
		$classname = $this->classnames['notifyschedule'];
		return $classname::getAllInstancesByUser($userid, $this, $sent);
	} /* }}} */

	/**
	 * Get a list of all notify schedules
	 *
	 * @return array array of instances of {@link SeedDMS_Core_Group}
	 */
	function getNotificationsToSend() { /* {{{ */
		$classname = $this->classnames['notifyschedule'];
		return $classname::getAllInstances($this, 'N');
	} /* }}} */

	/**
	 * Create a token to request a new password.
	 * This function will not delete the password but just creates an entry
	 * in tblUserRequestPassword indicating a password request.
	 *
	 * @return string hash value of false in case of an error
	 */
	function createPasswordRequest($user) { /* {{{ */
		$hash = md5(uniqid(time()));
		$queryStr = "INSERT INTO `tblUserPasswordRequest` (`userID`, `hash`, `date`) VALUES (" . $user->getId() . ", " . $this->db->qstr($hash) .", ".$this->db->getCurrentDatetime().")";
		$resArr = $this->db->getResult($queryStr);
		if (is_bool($resArr) && !$resArr) return false;
		return $hash;

	} /* }}} */

	/**
	 * Check if hash for a password request is valid.
	 * This function searches a previously create password request and
	 * returns the user.
	 *
	 * @param string $hash
	 */
	function checkPasswordRequest($hash) { /* {{{ */
		/* Get the password request from the database */
		$queryStr = "SELECT * FROM `tblUserPasswordRequest` where `hash`=".$this->db->qstr($hash);
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		if (count($resArr) != 1)
			return false;
		$resArr = $resArr[0];

		return $this->getUser($resArr['userID']);

	} /* }}} */

	/**
	 * Delete a password request
	 *
	 * @param string $hash
	 */
	function deletePasswordRequest($hash) { /* {{{ */
		/* Delete the request, so nobody can use it a second time */
		$queryStr = "DELETE FROM `tblUserPasswordRequest` WHERE `hash`=".$this->db->qstr($hash);
		if (!$this->db->getResult($queryStr))
			return false;
		return true;
	} /* }}} */

	/**
	 * Return a attribute definition by its id
	 *
	 * This function retrieves a attribute definitionr from the database by
	 * its id.
	 *
	 * @param integer $id internal id of attribute defintion
	 * @return object instance of {@link SeedDMS_Core_AttributeDefinition} or false
	 */
	function getAttributeDefinition($id, $catid=0) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT ad.id, 
		                    ad.name, 
		                    ad.objtype, 
		                    ad.type, 
		                    ad.multiple, 
		                    ad.minvalues, 
		                    ad.maxvalues, 
		                    ad.valueset, 
		                    ad.regex 
		               FROM `tblAttributeDefinitions` ad";
		
		if($catid > 0) {
			$queryStr .= " INNER JOIN `tblAttributeCategory` ac ON ad.id = ac.attributeID";
			$queryStr .=" WHERE ad.`id` = " . (int) $id. " AND ac.`categoryID` = ". (int) $catid;
		} else {
			$queryStr .= " WHERE ad.`id` = " . (int) $id;
		}

		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		$resArr = $resArr[0];
		$attributeCategories = new SeedDMS_Core_AttributeCategory($resArr["id"], $resArr["name"]);
		$attributeCategories->setDMS($this);
		$cats = $attributeCategories->getCategories();

		$attrdef = new SeedDMS_Core_AttributeDefinition($resArr["id"], $resArr["name"], $resArr["objtype"], $resArr["type"], $resArr["multiple"], $resArr["minvalues"], $resArr["maxvalues"], $resArr["valueset"], $resArr["regex"], $cats);
		$attrdef->setDMS($this);
		return $attrdef;
	} /* }}} */
	
	/**
	 * Return a attribute definition by its id
	 *
	 * This function retrieves a attribute definitionr from the database by
	 * its id.
	 *
	 * @param integer $id internal id of attribute defintion
	 * @return object instance of {@link SeedDMS_Core_AttributeDefinition} or false
	 */
	function getDocumentAttributeValue($attrdef, $document) { /* {{{ */
		if (!is_numeric($attrdef) || !is_numeric($document))
			return false;

		$queryStr = "SELECT * FROM `tblDocumentAttributes` WHERE `attrdef` = " . (int) $attrdef . " AND `document`=" . (int) $document;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		$resArr = $resArr[0];

		return $resArr["value"];
	} /* }}} */

	/**
	 * Return a attribute definition by its name
	 *
	 * This function retrieves an attribute def. from the database by its name.
	 *
	 * @param string $name internal name of attribute def.
	 * @return object instance of {@link SeedDMS_Core_AttributeDefinition} or false
	 */
	function getAttributeDefinitionByName($name) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblAttributeDefinitions` WHERE `name` = " . $this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		$resArr = $resArr[0];
		
		$attributeCategories = new SeedDMS_Core_AttributeCategory($resArr["id"], $resArr["name"]);
		$attributeCategories->setDMS($this);
		$cats = $attributeCategories->getCategories();

		$attrdef = new SeedDMS_Core_AttributeDefinition($resArr["id"], $resArr["name"], $resArr["objtype"], $resArr["type"], $resArr["multiple"], $resArr["minvalues"], $resArr["maxvalues"], $resArr["valueset"], $resArr["regex"], $cats);
		$attrdef->setDMS($this);
		return $attrdef;
	} /* }}} */

	/**
	 * Return list of all attributes definitions
	 *
	 * @param integer $objtype select those attributes defined for an object type
	 * @return array of instances of {@link SeedDMS_Core_AttributeDefinition} or false
	 */
	function getAllAttributeDefinitions($objtype=0) { /* {{{ */
		$queryStr = "SELECT * FROM `tblAttributeDefinitions`";
		if($objtype) {
			if(is_array($objtype))
				$queryStr .= ' WHERE `objtype` in (\''.implode("','", $objtype).'\')';
			else
				$queryStr .= ' WHERE `objtype`='.intval($objtype);
		}
		$queryStr .= ' ORDER BY `id`';
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$attrdefs = array();

		for ($i = 0; $i < count($resArr); $i++) {
			
			$attributeCategories = new SeedDMS_Core_AttributeCategory($resArr[$i]["id"], $resArr[$i]["name"]);
			$attributeCategories->setDMS($this);
			$cats = $attributeCategories->getCategories();
			
			$attrdef = new SeedDMS_Core_AttributeDefinition($resArr[$i]["id"], $resArr[$i]["name"], $resArr[$i]["objtype"], $resArr[$i]["type"], $resArr[$i]["multiple"], $resArr[$i]["minvalues"], $resArr[$i]["maxvalues"], $resArr[$i]["valueset"], $resArr[$i]["regex"], $cats);
			$attrdef->setDMS($this);
			$attrdefs[$i] = $attrdef;
		}

		return $attrdefs;
	} /* }}} */

	/**
	 * Return list of all attributes definitions
	 *
	 * @param integer $objtype select those attributes defined for an object type
	 * @return array of instances of {@link SeedDMS_Core_AttributeDefinition} or false
	 */
	function getAllAttributeDefinitionsByCategory($objtype=0, $category) { /* {{{ */
		$queryStr = "SELECT ad.* FROM `tblAttributeDefinitions` ad";
		$queryStr .= " INNER JOIN `tblAttributeCategory` ac ON ad.id = ac.attributeID ";
		if($objtype) {
			if(is_array($objtype))
				$queryStr .= ' WHERE ad.`objtype` in (\''.implode("','", $objtype).'\')';
			else
				$queryStr .= ' WHERE ad.`objtype`='.intval($objtype);
		}
		
		$queryStr .= " AND ac.`categoryID`=".intval($category);
		
		$queryStr .= ' ORDER BY ad.`name`';

		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$attrdefs = array();

		for ($i = 0; $i < count($resArr); $i++) {
			
			$attributeCategories = new SeedDMS_Core_AttributeCategory($resArr[$i]["id"], $resArr[$i]["name"]);
			$attributeCategories->setDMS($this);
			$cats = $attributeCategories->getCategories();
			
			$attrdef = new SeedDMS_Core_AttributeDefinition($resArr[$i]["id"], $resArr[$i]["name"], $resArr[$i]["objtype"], $resArr[$i]["type"], $resArr[$i]["multiple"], $resArr[$i]["minvalues"], $resArr[$i]["maxvalues"], $resArr[$i]["valueset"], $resArr[$i]["regex"], $cats);
			$attrdef->setDMS($this);
			$attrdefs[$i] = $attrdef;
		}

		return $attrdefs;
	} /* }}} */

	/**
	 * Add a new attribute definition
	 *
	 * @param string $name name of attribute
	 * @param string $type type of attribute
	 * @param boolean $multiple set to 1 if attribute has multiple attributes
	 * @param integer $minvalues minimum number of values
	 * @param integer $maxvalues maximum number of values if multiple is set
	 * @param string $valueset list of allowed values (csv format)
	 * @return object of {@link SeedDMS_Core_User}
	 */
	function addAttributeDefinition($name, $objtype, $type, $multiple=0, $minvalues=0, $maxvalues=1, $valueset='', $regex='', $categories=array()) { /* {{{ */
		$attribute = $this->getAttributeDefinitionByName($name);
		if (is_object($attribute)) {
			return false;
		}
		if(!$type)
			return false;
		if(trim($valueset)) {
			$valuesetarr = array_map('trim', explode($valueset[0], substr($valueset, 1)));
			$valueset = $valueset[0].implode($valueset[0], $valuesetarr);
		} else {
			$valueset = '';
		}
		$queryStr = "INSERT INTO `tblAttributeDefinitions` (`name`, `objtype`, `type`, `multiple`, `minvalues`, `maxvalues`, `valueset`, `regex`) VALUES (".$this->db->qstr($name).", ".intval($objtype).", ".intval($type).", ".intval($multiple).", ".intval($minvalues).", ".intval($maxvalues).", ".$this->db->qstr($valueset).", ".$this->db->qstr($regex).")";
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
			
		$attrdefid = $this->db->getInsertID('tblAttributeDefinitions');
		
		if(count($categories) > 0 ) {
			$attribute = $this->getAttributeDefinitionByName($name);
			$attribute->setCategories($categories);
		}

		return $this->getAttributeDefinition($attrdefid);
	} /* }}} */

	/**
	 * Return list of all workflows
	 *
	 * @return array of instances of {@link SeedDMS_Core_Workflow} or false
	 */
	function getAllWorkflows() { /* {{{ */
		$queryStr = "SELECT * FROM `tblWorkflows` ORDER BY `name`";
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$queryStr = "SELECT * FROM `tblWorkflowStates` ORDER BY `name`";
		$ressArr = $this->db->getResultArray($queryStr);

		if (is_bool($ressArr) && $ressArr == false)
			return false;

		for ($i = 0; $i < count($ressArr); $i++) {
			$wkfstates[$ressArr[$i]["id"]] = new SeedDMS_Core_Workflow_State($ressArr[$i]["id"], $ressArr[$i]["name"], $ressArr[$i]["maxtime"], $ressArr[$i]["precondfunc"], $ressArr[$i]["documentstatus"]);
		}

		$workflows = array();
		for ($i = 0; $i < count($resArr); $i++) {
			$workflow = new SeedDMS_Core_Workflow($resArr[$i]["id"], $resArr[$i]["name"], $wkfstates[$resArr[$i]["initstate"]]);
			$workflow->setDMS($this);
			$workflows[$i] = $workflow;
		}

		return $workflows;
	} /* }}} */

	/**
	 * Return workflow by its Id
	 *
	 * @param integer $id internal id of workflow
	 * @return object of instances of {@link SeedDMS_Core_Workflow} or false
	 */
	function getWorkflow($id) { /* {{{ */
		$queryStr = "SELECT * FROM `tblWorkflows` WHERE `id`=".intval($id);
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		if(!$resArr)
			return false;

		$initstate = $this->getWorkflowState($resArr[0]['initstate']);

		$workflow = new SeedDMS_Core_Workflow($resArr[0]["id"], $resArr[0]["name"], $initstate);
		$workflow->setDMS($this);

		return $workflow;
	} /* }}} */

	/**
	 * Return workflow by its name
	 *
	 * @param string $name name of workflow
	 * @return object of instances of {@link SeedDMS_Core_Workflow} or false
	 */
	function getWorkflowByName($name) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblWorkflows` WHERE `name`=".$this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		if(!$resArr)
			return false;

		$initstate = $this->getWorkflowState($resArr[0]['initstate']);

		$workflow = new SeedDMS_Core_Workflow($resArr[0]["id"], $resArr[0]["name"], $initstate);
		$workflow->setDMS($this);

		return $workflow;
	} /* }}} */

	/**
	 * Add a new workflow
	 *
	 * @param string $name name of workflow
	 * @param string $initstate initial state of workflow
	 */
	function addWorkflow($name, $initstate) { /* {{{ */
		$db = $this->db;
		if (is_object($this->getWorkflowByName($name))) {
			return false;
		}
		$queryStr = "INSERT INTO `tblWorkflows` (`name`, `initstate`) VALUES (".$db->qstr($name).", ".$initstate->getID().")";
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		return $this->getWorkflow($db->getInsertID('tblWorkflows'));
	} /* }}} */

	/**
	 * Return a workflow state by its id
	 *
	 * This function retrieves a workflow state from the database by its id.
	 *
	 * @param integer $id internal id of workflow state
	 * @return object instance of {@link SeedDMS_Core_Workflow_State} or false
	 */
	function getWorkflowState($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblWorkflowStates` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		$resArr = $resArr[0];

		$state = new SeedDMS_Core_Workflow_State($resArr["id"], $resArr["name"], $resArr["maxtime"], $resArr["precondfunc"], $resArr["documentstatus"]);
		$state->setDMS($this);
		return $state;
	} /* }}} */

	/**
	 * Return workflow state by its name
	 *
	 * @param string $name name of workflow state
	 * @return object of instances of {@link SeedDMS_Core_Workflow_State} or false
	 */
	function getWorkflowStateByName($name) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblWorkflowStates` WHERE `name`=".$this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		if(!$resArr)
			return false;

		$resArr = $resArr[0];

		$state = new SeedDMS_Core_Workflow_State($resArr["id"], $resArr["name"], $resArr["maxtime"], $resArr["precondfunc"], $resArr["documentstatus"]);
		$state->setDMS($this);

		return $state;
	} /* }}} */

	/**
	 * Return workflow specific transition
	 *
	 * @param integer $workflow_id
	 * @param integer $state_id
	 * @return object of instances of {@link SeedDMS_Core_Workflow_Transition} or false
	 */
	function getWorkflowNextTransition($workflow_id, $state_id){ /* {{{ */
		if (!is_numeric($workflow_id) || !is_numeric($state_id)){
			return false;
		}

		$queryStr = "SELECT * FROM `tblWorkflowTransitions` WHERE `workflow` = ".(int)$workflow_id." AND `state` = ".(int)$state_id;
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		$resArr = $resArr[0];

		$transition = new SeedDMS_Core_Workflow_Transition($resArr["id"], $this->getWorkflow($resArr["workflow"]), $this->getWorkflowState($resArr["state"]), $this->getWorkflowAction($resArr["action"]), $this->getWorkflowState($resArr["nextstate"]), $resArr["maxtime"], $resArr['file_change_allowed'], $resArr['user_allowed']);
		$transition->setDMS($this);

		return $transition;
	} /* }}} */

	/**
	 * Return list of all workflow states
	 *
	 * @return array of instances of {@link SeedDMS_Core_Workflow_State} or false
	 */
	function getAllWorkflowStates() { /* {{{ */
		$queryStr = "SELECT * FROM `tblWorkflowStates` ORDER BY `name`";
		$ressArr = $this->db->getResultArray($queryStr);

		if (is_bool($ressArr) && $ressArr == false)
			return false;

		$wkfstates = array();
		for ($i = 0; $i < count($ressArr); $i++) {
			$wkfstate = new SeedDMS_Core_Workflow_State($ressArr[$i]["id"], $ressArr[$i]["name"], $ressArr[$i]["maxtime"], $ressArr[$i]["precondfunc"], $ressArr[$i]["documentstatus"]);
			$wkfstate->setDMS($this);
			$wkfstates[$i] = $wkfstate;
		}

		return $wkfstates;
	} /* }}} */

	/**
	 * Add new workflow state
	 *
	 * @param string $name name of workflow state
	 * @param integer $docstatus document status when this state is reached
	 * @return object instance of new workflow state
	 */
	function addWorkflowState($name, $docstatus) { /* {{{ */
		$db = $this->db;
		if (is_object($this->getWorkflowStateByName($name))) {
			return false;
		}
		$queryStr = "INSERT INTO `tblWorkflowStates` (`name`, `documentstatus`) VALUES (".$db->qstr($name).", ".(int) $docstatus.")";
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		return $this->getWorkflowState($db->getInsertID('tblWorkflowStates'));
	} /* }}} */

	/**
	 * Return a workflow action by its id
	 *
	 * This function retrieves a workflow action from the database by its id.
	 *
	 * @param integer $id internal id of workflow action
	 * @return object instance of {@link SeedDMS_Core_Workflow_Action} or false
	 */
	function getWorkflowAction($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblWorkflowActions` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		$resArr = $resArr[0];

		$action = new SeedDMS_Core_Workflow_Action($resArr["id"], $resArr["name"], $resArr["make_resolution"]);
		$action->setDMS($this);
		return $action;
	} /* }}} */

	/**
	 * Return a workflow action by its name
	 *
	 * This function retrieves a workflow action from the database by its name.
	 *
	 * @param string $name name of workflow action
	 * @return object instance of {@link SeedDMS_Core_Workflow_Action} or false
	 */
	function getWorkflowActionByName($name) { /* {{{ */
		if (!$name) return false;

		$queryStr = "SELECT * FROM `tblWorkflowActions` WHERE `name` = " . $this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		$resArr = $resArr[0];

		$action = new SeedDMS_Core_Workflow_Action($resArr["id"], $resArr["name"], $resArr["make_resolution"]);
		$action->setDMS($this);
		return $action;
	} /* }}} */

	/**
	 * Return list of workflow action
	 *
	 * @return array list of instances of {@link SeedDMS_Core_Workflow_Action} or false
	 */
	function getAllWorkflowActions() { /* {{{ */
		$queryStr = "SELECT * FROM `tblWorkflowActions`";
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$wkfactions = array();
		for ($i = 0; $i < count($resArr); $i++) {
			$action = new SeedDMS_Core_Workflow_Action($resArr[$i]["id"], $resArr[$i]["name"], $resArr[$i]["make_resolution"]);
			$action->setDMS($this);
			$wkfactions[$i] = $action;
		}

		return $wkfactions;
	} /* }}} */

	/**
	 * Add new workflow action
	 *
	 * @param string $name name of workflow action
	 * @return object instance new workflow action
	 */
	function addWorkflowAction($name, $make_resolution) { /* {{{ */
		$db = $this->db;
		if (is_object($this->getWorkflowActionByName($name))) {
			return false;
		}
		$queryStr = "INSERT INTO `tblWorkflowActions` (`name`, `make_resolution`) VALUES (".$db->qstr($name).",".(int)$make_resolution.")";
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		return $this->getWorkflowAction($db->getInsertID('tblWorkflowActions'));
	} /* }}} */

	/**
	 * Return a workflow transition by its id
	 *
	 * This function retrieves a workflow transition from the database by its id.
	 *
	 * @param integer $id internal id of workflow transition
	 * @return object instance of {@link SeedDMS_Core_Workflow_Transition} or false
	 */
	function getWorkflowTransition($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblWorkflowTransitions` WHERE `id` = " . (int) $id;
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		$resArr = $resArr[0];

		$transition = new SeedDMS_Core_Workflow_Transition($resArr["id"], $this->getWorkflow($resArr["workflow"]), $this->getWorkflowState($resArr["state"]), $this->getWorkflowAction($resArr["action"]), $this->getWorkflowState($resArr["nextstate"]), $resArr["maxtime"], $resArr['file_change_allowed'], $resArr['user_allowed']);
		$transition->setDMS($this);
		return $transition;
	} /* }}} */


	function addInformationTypeRequested($name) { /* {{{ */
		$queryStr = "INSERT INTO `tblInformationTypeRequested` (`name`) VALUES (".$this->db->qstr($name).")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function addResolutionType($abbreviation, $name, $comment, $make_memo = 0){ /* {{{ */
		$queryStr = "INSERT INTO `tblResolutionTypes` (`abbreviation`, `make_memo`, `name`,`comment`) VALUES (".$this->db->qstr($abbreviation).", ".(int)$make_memo.", ".$this->db->qstr($name).", ".$this->db->qstr($comment).")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	} /* }}} */


	function editResolutionType($id, $name, $comment, $abbreviation, $make_memo){
		$queryStr = "UPDATE `tblResolutionTypes` SET `abbreviation` = ".$this->db->qstr($abbreviation).", `make_memo` = ".(int)$make_memo.", `name` = ".$this->db->qstr($name).", `comment` = ".$this->db->qstr($comment)." WHERE `id` = ".(int)$id;
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}


	function getInformationTypesRequested() { /* {{{ */
		$queryStr = "SELECT * FROM `tblInformationTypeRequested` ORDER BY `id`";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}
		
		return $resArr;
	} /* }}} */

	function editInformationType($id, $name) { /* {{{ */
		$queryStr = "UPDATE `tblInformationTypeRequested` SET `name` = ".$this->db->qstr($name)." WHERE `id` = ".(int)$id;
		if (!$this->db->getResult($queryStr)){
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function getInformationTypeRequestedById($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblInformationTypeRequested` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function getResolutionTypes() { /* {{{ */
		$queryStr = "SELECT * FROM `tblResolutionTypes` ORDER BY `id`";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	} /* }}} */

	function getResolutionTypeById($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblResolutionTypes` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function getResolutionTypeRecordById($id) { /* {{{ */
		if (!is_numeric($id))
			return false;

		$queryStr = "SELECT * FROM `tblResolutionTypes` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		} else {
			return $resArr[0];
		}
	} /* }}} */

	function getResolutionTypeRecordByName($name) { /* {{{ */

		$queryStr = "SELECT * FROM `tblResolutionTypes` WHERE `name` = ".$this->db->qstr($name);
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		} else {
			return $resArr[0];
		}
	} /* }}} */

	function delInformationTypeRequested($id) { /* {{{ */
		$queryStr = "DELETE from `tblInformationTypeRequested` WHERE `id` = " . (int) $id;
		if (!$this->db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		return true;
	} /* }}} */

	function delResolutionType($id) { /* {{{ */
		$queryStr = "DELETE from `tblResolutionTypes` WHERE `id` = " . (int) $id;
		if (!$this->db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		return true;
	} /* }}} */

	function getTemplates() { /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentTemplate` ORDER BY `id`";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr;
	} /* }}} */

	function getTemplate($id) { /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentTemplate` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1)){
			return false;
		} else {
			return $resArr;
		}
	} /* }}} */

	function addTemplate($name, $content) { /* {{{ */
		$queryStr = "INSERT INTO `tblDocumentTemplate` (`name`, `content`) VALUES (".$this->db->qstr($name).", ".$this->db->qstr($content).")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function updateTemplateName($id, $name){
		$queryStr = "UPDATE `tblDocumentTemplate` SET `name`= ".$this->db->qstr($name)." WHERE `id` = ".(int)$id."";
		
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	}

	function updateTemplateContent($id, $content){
		$queryStr = "UPDATE `tblDocumentTemplate` SET `content`= ".$this->db->qstr($content)." WHERE `id` = ".(int)$id;
		//return $queryStr;

		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	}

	function deleteTemplate($id) { /* {{{ */
		$this->db->startTransaction();
		$queryStr = "DELETE FROM `tblDocumentTemplate` WHERE `id` = ".(int)$id;
		if (!$this->db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		$this->db->commitTransaction();
		return true;
	} /* }}} */

	function getDocumentMemoContent($document_id){ /* {{{ */ 
		$queryStr = "SELECT * FROM `tblDocumentMemoContent` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)) {
			return false;
		} else {
			return $resArr[0];
		}
	} /* }}} */

	function getDocumentResolutionContent($document_id){ /* {{{ */ 
		$queryStr = "SELECT * FROM `tblDocumentResolutionContent` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)) {
			return false;
		} else {
			return $resArr[0];
		}
	} /* }}} */

	function saveDocumentMemoContent($document_id, $content) { /* {{{ */
		$queryStr1 = "SELECT * FROM `tblDocumentMemoContent` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr1);

		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1)){
			$queryStr2 = "INSERT INTO `tblDocumentMemoContent` (`document_id`, `content`) VALUES (".(int)$document_id.", ".$this->db->qstr($content).")";
			$res = $this->db->getResult($queryStr2);
			if (!$res){
				return false;
			}
		} else {
			$queryStr3 = "UPDATE `tblDocumentMemoContent` SET `content`= ".$this->db->qstr($content)." WHERE `document_id` = ".(int)$document_id;
			$res = $this->db->getResult($queryStr3);
			if (!$res){
				return false;
			}
		}

		return true;
	} /* }}} */

	function saveDocumentResolutionContent($document_id, $content) { /* {{{ */
		$queryStr1 = "SELECT * FROM `tblDocumentResolutionContent` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr1);

		if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1)){
			$queryStr2 = "INSERT INTO `tblDocumentResolutionContent` (`document_id`, `content`) VALUES (".(int)$document_id.", ".$this->db->qstr($content).")";
			$res = $this->db->getResult($queryStr2);
			if (!$res){
				return false;
			}
		} else {
			$queryStr3 = "UPDATE `tblDocumentResolutionContent` SET `content`= ".$this->db->qstr($content)." WHERE `document_id` = ".(int)$document_id;
			$res = $this->db->getResult($queryStr3);
			if (!$res){
				return false;
			}
		}

		return true;
	} /* }}} */

	/**
	 * Adds a new document date end
	 * @param integer $document_id 
	 * @param timestamp $start_date 
	 * @param timestamp $end_date 
	 * @return bool
	 */
	function addDocumentDate($document_id, $start_date, $end_date){ /* {{{ */
		$queryStr = "INSERT INTO `tblDocumentDates` (`document_id`,`start_date`,`end_date`) VALUES (".(int)$document_id.", ".(int)$start_date.", ".(int)$end_date.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function checkDocumentDates($document_id){
		$queryStr = "SELECT * FROM `tblDocumentDates` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return true;
	}

	function deleteDocumentDates($document_id){
		$queryStr = "DELETE FROM `tblDocumentDates` WHERE `document_id` = ".(int)$document_id;
		if (!$this->db->getResult($queryStr)){
			return false;
		} else {
			return true;	
		}
	}

	function getDocumentDate($document_id){ /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentDates` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr))
			return false;

		return $resArr[0];
	} /* }}} */

	function pauseDocumentDate($document_id, $paused = 0) {		
		$queryStr = "UPDATE `tblDocumentDates` SET `paused` = ".(int)$paused." WHERE `document_id` = ".(int)$document_id;
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;		
	}

	function updateDocumentSIPDates($document_id, $start_date, $end_date) {		
		$queryStr = "UPDATE `tblDocumentDates` SET `start_date` = ".(int)$start_date.", `end_date` = ".(int)$end_date." WHERE `document_id` = ".(int)$document_id;
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;		
	}

	function updateEndDate($document_id, $days) {

		$dates = $this->getDocumentDate($document_id);

		if ($dates) {
			$str_days = '+'.$days.' day';
			$new_end_date = strtotime($str_days, (int)$dates['end_date']);

			$queryStr = "UPDATE `tblDocumentDates` SET `end_date` = ".(int)$new_end_date." WHERE `document_id` = ".(int)$document_id;
			$res = $this->db->getResult($queryStr);
			if (!$res){
				return false;
			}

			return true;

		} else {
			return false;
		}
		
	}

	function updateDocumentEndDate($document_id, $end_date) {	
		$queryStr = "UPDATE `tblDocumentDates` SET `end_date` = ".(int)$end_date." WHERE `document_id` = ".(int)$document_id;
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;		
	}


	/**
	 * Adds a new document group assignment
	 * @param integer $document_id
	 * @param integer $group_id
	 * @return bool
	 */
	function addDocumentGroupAssignment($document_id, $group_id){ /* {{{ */
		$queryStr = "INSERT INTO `tblDocumentGroupAssignments` (`document_id`,`group_id`) VALUES (".(int)$document_id.", ".(int)$group_id.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function getDocumentGroupAssignment($document_id){ /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentGroupAssignments` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr))
			return false;

		return $resArr[0];
	} /* }}} */

	function checkIfDocumentGroupAssignmentExists($group_id, $document_id){
		$queryStr = "SELECT * FROM `tblDocumentGroupAssignments` WHERE `document_id` = ".(int)$document_id." AND group_id = ".(int)$group_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		} else {
			return true;
		}

	}


	function getGroupDocumentAssignments($group_id){ /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentGroupAssignments` WHERE `group_id` = ".(int)$group_id. " AND `is_revision_active` = 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr))
			return false;

		return $resArr;
	} /* }}} */



	/**
	 * Adds a new document group assignment
	 * @param integer $document_id
	 * @param integer $group_id
	 * @return bool
	 */
	function addDocumentUserAssignment($document_id, $user_id){ /* {{{ */
		$queryStr = "INSERT INTO `tblDocumentUserAssignments` (`document_id`,`user_id`) VALUES (".(int)$document_id.", ".(int)$user_id.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function getDocumentUserAssignment($document_id){ /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentUserAssignments` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		} else {
			return $resArr[0];
		}

	} /* }}} */

	function checkDocumentUserAssignment($document_id){ /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentUserAssignments` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return true;
	} /* }}} */

	function checkDocumentGroupAssignment($document_id){ /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentGroupAssignments` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return true;
	} /* }}} */


	function deleteDocumentGroupAssignment($documentid){
		$queryStr = "DELETE FROM `tblDocumentGroupAssignments` WHERE `document_id` = ".(int)$documentid;
		if (!$this->db->getResult($queryStr)){
			return false;
		} else {
			return true;	
		}
	}

	function deleteDocumentUserAssignment($documentid){
		$queryStr = "DELETE FROM `tblDocumentUserAssignments` WHERE `document_id` = ".(int)$documentid;
		if (!$this->db->getResult($queryStr)){
			return false;
		} else {
			return true;	
		}
	}


	/**
	 * Returns document content which is not linked to a document
	 *
	 * This method is for finding straying document content without
	 * a parent document. In normal operation this should not happen
	 * but little checks for database consistency and possible errors
	 * in the application may have left over document content though
	 * the document is gone already.
	 */
	function getUnlinkedDocumentContent() { /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentContent` WHERE `document` NOT IN (SELECT id FROM `tblDocuments`)";
		$resArr = $this->db->getResultArray($queryStr);
		if ($resArr === false)
			return false;

		$versions = array();
		foreach($resArr as $row) {
			$document = new $this->classnames['document']($row['document'], '', '', '', '', '', '', '', '', '', '', '');
			$document->setDMS($this);
			$version = new $this->classnames['documentcontent']($row['id'], $document, $row['version'], $row['comment'], $row['date'], $row['createdBy'], $row['dir'], $row['orgFileName'], $row['fileType'], $row['mimeType'], $row['fileSize'], $row['checksum']);
			$versions[] = $version;
		}
		return $versions;

	} /* }}} */

	/**
	 * Returns document content which has no file size set
	 *
	 * This method is for finding document content without a file size
	 * set in the database. The file size of a document content was introduced
	 * in version 4.0.0 of SeedDMS for implementation of user quotas.
	 */
	function getNoFileSizeDocumentContent() { /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentContent` WHERE `fileSize` = 0 OR `fileSize` is null";
		$resArr = $this->db->getResultArray($queryStr);
		if ($resArr === false)
			return false;

		$versions = array();
		foreach($resArr as $row) {
			$document = new $this->classnames['document']($row['document'], '', '', '', '', '', '', '', '', '', '', '');
			$document->setDMS($this);
			$version = new $this->classnames['documentcontent']($row['id'], $document, $row['version'], $row['comment'], $row['date'], $row['createdBy'], $row['dir'], $row['orgFileName'], $row['fileType'], $row['mimeType'], $row['fileSize'], $row['checksum'], $row['fileSize'], $row['checksum']);
			$versions[] = $version;
		}
		return $versions;

	} /* }}} */

	/**
	 * Returns document content which has no checksum set
	 *
	 * This method is for finding document content without a checksum
	 * set in the database. The checksum of a document content was introduced
	 * in version 4.0.0 of SeedDMS for finding duplicates.
	 */
	function getNoChecksumDocumentContent() { /* {{{ */
		$queryStr = "SELECT * FROM `tblDocumentContent` WHERE `checksum` = '' OR `checksum` is null";
		$resArr = $this->db->getResultArray($queryStr);
		if ($resArr === false)
			return false;

		$versions = array();
		foreach($resArr as $row) {
			$document = new $this->classnames['document']($row['document'], '', '', '', '', '', '', '', '', '', '', '');
			$document->setDMS($this);
			$version = new $this->classnames['documentcontent']($row['id'], $document, $row['version'], $row['comment'], $row['date'], $row['createdBy'], $row['dir'], $row['orgFileName'], $row['fileType'], $row['mimeType'], $row['fileSize'], $row['checksum']);
			$versions[] = $version;
		}
		return $versions;

	} /* }}} */

	/**
	 * Returns document content which is duplicated
	 *
	 * This method is for finding document content which is available twice
	 * in the database. The checksum of a document content was introduced
	 * in version 4.0.0 of SeedDMS for finding duplicates.
	 */
	function getDuplicateDocumentContent() { /* {{{ */
		$queryStr = "SELECT a.*, b.`id` as dupid FROM `tblDocumentContent` a LEFT JOIN `tblDocumentContent` b ON a.`checksum`=b.`checksum` where a.`id`!=b.`id` ORDER by a.`id` LIMIT 1000";
		$resArr = $this->db->getResultArray($queryStr);
		if (!$resArr)
			return false;

		$versions = array();
		foreach($resArr as $row) {
			$document = $this->getDocument($row['document']);
			$version = new $this->classnames['documentcontent']($row['id'], $document, $row['version'], $row['comment'], $row['date'], $row['createdBy'], $row['dir'], $row['orgFileName'], $row['fileType'], $row['mimeType'], $row['fileSize'], $row['checksum']);
			if(!isset($versions[$row['dupid']])) {
				$versions[$row['id']]['content'] = $version;
				$versions[$row['id']]['duplicates'] = array();
			} else
				$versions[$row['dupid']]['duplicates'][] = $version;
		}
		return $versions;

	} /* }}} */

	/**
	 * Returns a list of reviews, approvals which are not linked
	 * to a user, group anymore
	 *
	 * This method is for finding reviews or approvals whose user
	 * or group  was deleted and not just removed from the process.
	 */
	function getProcessWithoutUserGroup($process, $usergroup) { /* {{{ */
		switch($process) {
		case 'review':
			$queryStr = "SELECT a.*, b.`name` FROM `tblDocumentReviewers`";
			break;
		case 'approval':
			$queryStr = "SELECT a.*, b.`name` FROM `tblDocumentApprovers`";
			break;
		}
		$queryStr .= " a LEFT JOIN `tblDocuments` b ON a.`documentID`=b.`id` where";
		switch($usergroup) {
		case 'user':
			$queryStr .= " a.`type`=0 and a.`required` not in (select `id` from `tblUsers`) ORDER by b.`id`";
			break;
		case 'group':
			$queryStr .= " a.`type`=1 and a.`required` not in (select `id` from `tblGroups`) ORDER by b.`id`";
			break;
		}
		return $this->db->getResultArray($queryStr);
	} /* }}} */

	/**
	 * Removes all reviews, approvals which are not linked
	 * to a user, group anymore
	 *
	 * This method is for removing all reviews or approvals whose user
	 * or group  was deleted and not just removed from the process.
	 * If the optional parameter $id is set, only this user/group id is removed.
	 */
	function removeProcessWithoutUserGroup($process, $usergroup, $id=0) { /* {{{ */
		/* Entries of tblDocumentReviewLog or tblDocumentApproveLog are deleted
		 * because of CASCADE ON
		 */
		switch($process) {
		case 'review':
			$queryStr = "DELETE FROM tblDocumentReviewers";
			break;
		case 'approval':
			$queryStr = "DELETE FROM tblDocumentApprovers";
			break;
		}
		$queryStr .= " WHERE";
		switch($usergroup) {
		case 'user':
			$queryStr .= " type=0 AND";
			if($id)
				$queryStr .= " required=".((int) $id)." AND";
			$queryStr .= " required NOT IN (SELECT id FROM tblUsers)";
			break;
		case 'group':
			$queryStr .= " type=1 AND";
			if($id)
				$queryStr .= " required=".((int) $id)." AND";
			$queryStr .= " required NOT IN (SELECT id FROM tblGroups)";
			break;
		}
		return $this->db->getResultArray($queryStr);
	} /* }}} */

	/**
	 * Returns statitical information
	 *
	 * This method returns all kind of statistical information like
	 * documents or used space per user, recent activity, etc.
	 *
	 * @param string $type type of statistic
	 * @return array statistical data
	 */
	function getStatisticalData($type='') { /* {{{ */
		switch($type) {
			case 'docsperuser':
				$queryStr = "select b.`fullName` as `key`, count(`owner`) as total from `tblDocuments` a left join `tblUsers` b on a.`owner`=b.`id` group by `owner`, b.`fullName`";
				$resArr = $this->db->getResultArray($queryStr);
				if (!$resArr)
					return false;

				return $resArr;
			case 'docspermimetype':
				$queryStr = "select b.`mimeType` as `key`, count(`mimeType`) as total from `tblDocuments` a left join `tblDocumentContent` b on a.`id`=b.`document` group by b.`mimeType`";
				$resArr = $this->db->getResultArray($queryStr);
				if (!$resArr)
					return false;

				return $resArr;
			case 'docspercategory':
				$queryStr = "select b.`name` as `key`, count(a.`categoryID`) as total from `tblDocumentCategory` a left join `tblCategory` b on a.`categoryID`=b.id group by a.`categoryID`, b.`name`";
				$resArr = $this->db->getResultArray($queryStr);
				if (!$resArr)
					return false;

				return $resArr;
			case 'docsperstatus':
				$queryStr = "select b.`status` as `key`, count(b.`status`) as total from (select a.id, max(b.version), max(c.`statusLogID`) as maxlog from `tblDocuments` a left join `tblDocumentStatus` b on a.id=b.`documentID` left join `tblDocumentStatusLog` c on b.`statusID`=c.`statusID` group by a.`id`, b.`version` order by a.`id`, b.`statusID`) a left join `tblDocumentStatusLog` b on a.`maxlog`=b.`statusLogID` group by b.`status`";
				$queryStr = "select b.`status` as `key`, count(b.`status`) as total from (select a.`id`, max(c.`statusLogID`) as maxlog from `tblDocuments` a left join `tblDocumentStatus` b on a.id=b.`documentID` left join `tblDocumentStatusLog` c on b.`statusID`=c.`statusID` group by a.`id`  order by a.id) a left join `tblDocumentStatusLog` b on a.maxlog=b.`statusLogID` group by b.`status`";
				$resArr = $this->db->getResultArray($queryStr);
				if (!$resArr)
					return false;

				return $resArr;
			case 'docspermonth':
				$queryStr = "select *, count(`key`) as total from (select ".$this->db->getDateExtract("date", '%Y-%m')." as `key` from `tblDocuments`) a group by `key` order by `key`";
				$resArr = $this->db->getResultArray($queryStr);
				if (!$resArr)
					return false;

				return $resArr;
			case 'docsaccumulated':
				$queryStr = "select *, count(`key`) as total from (select ".$this->db->getDateExtract("date")." as `key` from `tblDocuments`) a group by `key` order by `key`";
				$resArr = $this->db->getResultArray($queryStr);
				if (!$resArr)
					return false;

				$sum = 0;
				foreach($resArr as &$res) {
					$sum += $res['total'];
					/* auxially variable $key is need because sqlite returns
					 * a key '`key`'
					 */
					$res['key'] = mktime(12, 0, 0, substr($res['key'], 5, 2), substr($res['key'], 8, 2), substr($res['key'], 0, 4)) * 1000;
					$res['total'] = $sum;
				}
				return $resArr;
			case 'sizeperuser':
				$queryStr = "select c.`fullName` as `key`, sum(`fileSize`) as total from `tblDocuments` a left join `tblDocumentContent` b on a.id=b.`document` left join `tblUsers` c on a.`owner`=c.`id` group by a.`owner`, c.`fullName`";
				$resArr = $this->db->getResultArray($queryStr);
				if (!$resArr)
					return false;

				return $resArr;
			default:
				return array();
		}
	} /* }}} */

	/**
	 * Returns changes with a period of time
	 *
	 * This method returns a list of all changes happened in the database
	 * within a given period of time. It currently just checks for
	 * entries in the database tables tblDocumentContent, tblDocumentFiles,
	 * and tblDocumentStatusLog
	 *
	 * @param string $start start date, defaults to start of current day
	 * @param string $end end date, defaults to end of start day
	 * @return array list of changes
	 */
	function getTimeline($startts='', $endts='') { /* {{{ */
		if(!$startts)
			$startts = mktime(0, 0, 0);
		if(!$endts)
			$endts = $startts+86400;
		$timeline = array();

		$queryStr = "SELECT DISTINCT document FROM `tblDocumentContent` WHERE `date` > ".$startts." AND `date` < ".$endts." UNION SELECT DISTINCT document FROM `tblDocumentFiles` WHERE `date` > ".$startts." AND `date` < ".$endts;
		$resArr = $this->db->getResultArray($queryStr);
		if ($resArr === false)
			return false;
		foreach($resArr as $rec) {
			$document = $this->getDocument($rec['document']);
			$timeline = array_merge($timeline, $document->getTimeline());
		}
		return $timeline;

	} /* }}} */

	/**
	 * Set a callback function
	 *
	 * @param string $name internal name of callback
	 * @param mixed $func function name as expected by {call_user_method}
	 * @param mixed $params parameter passed as the first argument to the
	 *        callback
	 */
	function setCallback($name, $func, $params=null) { /* {{{ */
		if($name && $func)
			$this->callbacks[$name] = array(array($func, $params));
	} /* }}} */

	/**
	 * Add a callback function
	 *
	 * @param string $name internal name of callback
	 * @param mixed $func function name as expected by {call_user_method}
	 * @param mixed $params parameter passed as the first argument to the
	 *        callback
	 */
	function addCallback($name, $func, $params=null) { /* {{{ */
		if($name && $func)
			$this->callbacks[$name][] = array($func, $params);
	} /* }}} */

	/**
	 * Create an sql dump of the complete database
	 *
	 * @param string $filename name of dump file
	 */
	function createDump($filename) { /* {{{ */
		$h = fopen($filename, "w");
		if(!$h)
			return false;

		$tables = $this->db->TableList('TABLES');
		foreach($tables as $table) {
			$query = "SELECT * FROM `".$table."`";
			$records = $this->db->getResultArray($query);
			fwrite($h,"\n-- TABLE: ".$table."--\n\n");
			foreach($records as $record) {
				$values="";
				$i = 1;
				foreach ($record as $column) {
					if (is_numeric($column)) $values .= $column;
					else $values .= $this->db->qstr($column);

					if ($i<(count($record))) $values .= ",";
					$i++;
				}

				fwrite($h, "INSERT INTO `".$table."` VALUES (".$values.");\n");
			}
		}

		fclose($h);
		return true;
	} /* }}} */


	function addDateRule($years, $days){
		$queryStr = "INSERT INTO `tblDateRules` (`years`,`days`) VALUES (".(int)$years.", ".(int)$days.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function getAllDateRules(){
		$queryStr = "SELECT * FROM `tblDateRules`";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function findDateRuleByYear($years){
		$queryStr = "SELECT * FROM `tblDateRules` WHERE `years` = ".(int)$years;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return true;
	}

	function checkDateRule($id){
		$queryStr = "SELECT * FROM `tblDateRules` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return true;	
	}

	function getDateRule($id){
		$queryStr = "SELECT * FROM `tblDateRules` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];	
	}

	function deleteDateRule($id){
		$this->db->startTransaction();
		$queryStr = "DELETE FROM `tblDateRules` WHERE `id` = ".(int)$id;
		if (!$this->db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		$this->db->commitTransaction();
		return true;
	}

	function addExtensionTime($days_before, $extension){
		$queryStr = "INSERT INTO `tblExtensionTime` (`days_before`, `extension`) VALUES (".(int)$days_before.",".(int)$extension.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function editExtensionTime($id, $days_before, $extension){
		$queryStr = "UPDATE `tblExtensionTime` SET `days_before`= ".(int)$days_before.", `extension` = ".(int)$extension." WHERE `id` = ".$id;
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	}

	function getExtensionTime($id){
		$queryStr = "SELECT * FROM `tblExtensionTime` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}

	function getExtensionTimeRule(){
		$queryStr = "SELECT * FROM `tblExtensionTime` ORDER BY `id` DESC LIMIT 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}

	function selectAllFromTable($table){
		$queryStr = "SELECT * FROM `".$table."` ORDER BY id DESC LIMIT 5";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return true;
	}

	function selectLastRowFromTable($table){
		$queryStr = "SELECT * FROM `".$table."` ORDER BY id DESC LIMIT 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}

	function insertCorrelative($table, $correlative, $document_id, $year){
		$queryStr = "INSERT INTO `".$table."` (`correlative`, `document_id`, `year`) VALUES (".(int)$correlative.", ".(int)$document_id.", ".(int)$year.")";

		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function addDocumentCorrelative($document_id, $document_type) {
		$current_year = date("Y");
		$correlative = 0;
		switch ($document_type) {
			case 'memo':
				$table = "tblMemoCorrelatives";
				$all = $this->selectAllFromTable($table);
								
				if (!$all) {
					$insert_correlative = $this->insertCorrelative($table, 1, $document_id, $current_year);
				} else {

					$last_row = $this->selectLastRowFromTable($table);
					
					if ((int)$last_row['year'] == (int)$current_year) {
						$correlative = $last_row['correlative'] + 1;
						$insert_correlative = $this->insertCorrelative($table, $correlative, $document_id, $current_year);
					} else {
						$insert_correlative = $this->insertCorrelative($table, 1, $document_id, $current_year);
					}
				}

				break;
			
			case 'resolution':
				
				$table = "tblResolutionCorrelatives";
				$all = $this->selectAllFromTable($table);
								
				if (!$all) {
					$insert_correlative = $this->insertCorrelative($table, 1, $document_id, $current_year);
				} else {

					$last_row = $this->selectLastRowFromTable($table);

					if ((int)$last_row['year'] == (int)$current_year) {
						$correlative = $last_row['correlative'] + 1;
						$insert_correlative = $this->insertCorrelative($table, $correlative, $document_id, $current_year);
					} else {
						$insert_correlative = $this->insertCorrelative($table, 1, $document_id, $current_year);
					}
				}

				break;
			
			case 'sip':
				
				$table = "tblSIPCorrelatives";

				$document = $this->getDocument($document_id);
				$sip_number = substr($document->getName(), 7);
				$name_array = explode('-', $sip_number);
				$document_correlative = $name_array[0];
				$document_year = $name_array[1];

				$insert_correlative = $this->insertCorrelative($table, $document_correlative, $document_id, $document_year);

				break;

			case 'cc':
				$table = "tblCCCorrelatives";				

				$document = $this->getDocument($document_id);
				$sip_number = substr($document->getName(), 6);
				$name_array = explode('-', $sip_number);
				$document_correlative = $name_array[0];
				$document_year = $name_array[1];

				$insert_correlative = $this->insertCorrelative($table, $document_correlative, $document_id, $document_year);

				break;

			default:
				return false;
				break;
		}
	}

	function getDocumentCorrelative($document_id, $document_type) {
		$queryStr = "";
		switch ($document_type) {
			case 'memo':
				$queryStr = "SELECT * FROM `tblMemoCorrelatives` WHERE document_id = ".(int)$document_id;
				break;

			case 'resolution':
				$queryStr = "SELECT * FROM `tblResolutionCorrelatives` WHERE document_id = ".(int)$document_id;
				break;
			
			case 'sip':
				$queryStr = "SELECT * FROM `tblSIPCorrelatives` WHERE document_id = ".(int)$document_id;
				break;

			case 'cc':
				$queryStr = "SELECT * FROM `tblCCCorrelatives` WHERE document_id = ".(int)$document_id;
				break;

			default:
				return false;
				break;
		}
		
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}


	function getAllCCAssignments($user_id){

		$queryStr = "SELECT * FROM `tblCCAssignments` WHERE `user_id` = ".(int)$user_id;

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function getAllSIPAssignments($user_id){
		$queryStr = "SELECT * FROM `tblSIPAssignments` WHERE `user_id` = ".(int)$user_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}
		return $resArr;
	}

	function getActiveCCAssignments($user_id){
		$queryStr = "SELECT * FROM `tblCCAssignments` WHERE `user_id` = ".(int)$user_id." AND `is_revision_active` = 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}
		return $resArr;
	}

	function getActiveSIPAssignments($user_id){
		$queryStr = "SELECT * FROM `tblSIPAssignments` WHERE `user_id` = ".(int)$user_id." AND `is_revision_active` = 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}
		return $resArr;
	}

	function checkSIPAssignment($document_id){
		$queryStr = "SELECT * FROM `tblSIPAssignments` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}
		return true;
	}

	function checkCCAssignment($document_id){
		$queryStr = "SELECT * FROM `tblCCAssignments` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}
		return true;
	}


	function addCCAssignment($document_id, $user_id){
		$queryStr = "INSERT INTO `tblCCAssignments` (`is_revision_active`,`document_id`,`user_id`) VALUES (1, ".(int)$document_id.", ".(int)$user_id.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function addSIPAssignment($document_id, $user_id){
		$queryStr = "INSERT INTO `tblSIPAssignments` (`is_revision_active`,`document_id`,`user_id`) VALUES (1, ".(int)$document_id.", ".(int)$user_id.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function finishCCAssignment($document_id){
		$queryStr = "UPDATE `tblCCAssignments` SET `is_revision_active`= 0 WHERE `document_id` = ".(int)$document_id;
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	}

	function deleteCCAssignment($document_id){
		$queryStr = "DELETE FROM `tblCCAssignments` WHERE `document_id` = ".(int)$document_id;
		if (!$this->db->getResult($queryStr)){
			return false;
		} else {
			return true;	
		}
	}

	function finishSIPAssignment($document_id){
		$queryStr = "UPDATE `tblSIPAssignments` SET `is_revision_active`= 0 WHERE `document_id` = ".(int)$document_id;
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;
	}

	function checkMemoAssignment($document_id){
		$queryStr = "SELECT * FROM `tblDocumentGroupAssignments` WHERE `document_id` = ".(int)$document_id." AND `is_revision_active` = 1";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}
		return true;
	}

	function finishMemoAssignment($document_id){
		$queryStr = "UPDATE `tblDocumentGroupAssignments` SET `is_revision_active` = 0 WHERE `document_id` = ".(int)$document_id;
		$res = $this->db->getResult($queryStr);
		if (!$res){
			return false;
		} else {
			return true;	
		}		
	}


	function deleteSIPAssignment($document_id){
		$queryStr = "DELETE FROM `tblSIPAssignments` WHERE `document_id` = ".(int)$document_id;
		if (!$this->db->getResult($queryStr)){
			return false;
		} else {
			return true;	
		}
		
	}

	function getPDFHtmlStart($margin_left = 1, $margin_top = 1, $margin_right = 1, $margin_bottom = 1){
		// Specific html for temporal pdf
		$html = "<!DOCTYPE html><html><head><title></title></head>";
		$html .= '<style>';
		$html .= '@page { 	margin-left: '.$margin_left.'cm; 
							margin-top: '.$margin_top.'cm; 
							margin-right: '.$margin_right.'cm; 
							margin-bottom: '.$margin_bottom.'cm; }';
		$html .= '.cke_editable,pre{word-wrap:break-word}blockquote,span[lang]{font-style:italic}body{font-family:sans-serif,Arial,Verdana,"Trebuchet MS","Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:12px;color:#333;background-color:#fff;margin:20px}.cke_editable{font-size:13px;line-height:1.6}blockquote{font-family:Georgia,Times,"Times New Roman",serif;padding:2px 0;border-style:solid;border-color:#ccc;border-width:0}.cke_contents_ltr blockquote{padding-left:20px;padding-right:8px;border-left-width:5px}.cke_contents_rtl blockquote{padding-left:8px;padding-right:20px;border-right-width:5px}a{color:#0782C1}dl,ol,ul{padding:0 40px}h1,h2,h3,h4,h5,h6{font-weight:400;line-height:1.2}hr{border:0;border-top:1px solid #ccc}img.left,img.right{border:1px solid #ccc;padding:5px}img.right{float:right;margin-left:15px}img.left{float:left;margin-right:15px}pre{white-space:pre-wrap;-moz-tab-size:4;tab-size:4}.marker{background-color:#ff0}figure{text-align:center;outline:#ccc solid 1px;background:rgba(0,0,0,.05);padding:10px;margin:10px 20px;display:inline-block}figure>figcaption{text-align:center;display:block}a>img{padding:1px;margin:1px;border:none;outline:#0782C1 solid 1px}.code-featured{border:5px solid red}.math-featured{padding:20px;box-shadow:0 0 2px rgba(200,0,0,1);background-color:rgba(255,0,0,.05);margin:10px}.image-clean{border:0;background:0 0;padding:0}.image-clean>figcaption{font-size:.9em;text-align:right}.image-grayscale{background-color:#fff;color:#666}.image-grayscale img,img.image-grayscale{filter:grayscale(100%)}.embed-240p{max-width:426px;max-height:240px;margin:0 auto}.embed-360p{max-width:640px;max-height:360px;margin:0 auto}.embed-480p{max-width:854px;max-height:480px;margin:0 auto}.embed-720p{max-width:1280px;max-height:720px;margin:0 auto}.embed-1080p{max-width:1920px;max-height:1080px;margin:0 auto}
			@import url(https://fonts.googleapis.com/css?family=Nunito+Sans:200,200i,400,400i,600,600i,700,700i);';
		$html .= '</style>';
		$html .= '<body class="document-editor cke_editable cke_editable_themed cke_contents_ltr">';

		return $html;
	}

	function getPDFHtmlEnd(){
		$html = "</body></html>";
		return $html;
	}

	function updateDocumentPrincipalFileInfo($filesize, $checksum, $version){
		$this->db->startTransaction();
		$queryStr = "UPDATE `tblDocumentContent` set `date`=".$this->db->getCurrentTimestamp().", `fileSize`=".$filesize.", `checksum`=".$this->db->qstr($checksum)." WHERE `id`=".$version;
		if (!$this->db->getResult($queryStr)) {
			$this->db->rollbackTransaction();
			return false;
		}

		$this->db->commitTransaction();
		return true;
	}

	function checkEvents($date, $year = 0){
		if ($year > 0) {
			$actual_year = $year;
		} else {
			$actual_year = date("Y");
		}

		//var_dump(date("m",$date));
		$queryStr = "SELECT * FROM `tblEvents` WHERE `year` = ".(int)$actual_year;		
		$resArr = $this->db->getResultArray($queryStr);

		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		$result = array();
		foreach ($resArr as $event) {
			if(((int)$date >= (int)$event['start']) && ((int)$date <= (int)$event['stop'])) {
				$result[0] = $event;
			}
		}

		if (empty($result)) {
			return false;
		} else {
			return $result[0];
		}
		
	}

	function getResolutionHtmlTemp($name, $comment){

		$html = "<!DOCTYPE html><html><head><title></title></head><body>";
		$html .= "<div>";
		$html .= "<p><strong>Tipo de resolución:</strong></p>";
		$html .= "<p>".$name."</p>";
		$html .= "<p><strong>Descripción:</strong></p>";
		$html .= "<p>".$comment."</p>";
		$html .= "</div>";
		$html .= "</body></html>";

		return $html;
	}

	function calculateDocumentSIPDates($document_created_at, $date_rule){

		// Compare hours
		if(strtotime(date("H:i:s", $document_created_at)) >= strtotime('16:01:00')) {

	 		$timestamp_created = date("Y-m-d H:i:s", strtotime('+1 day', $document_created_at));
	 		$date = new DateTime($timestamp_created);
			$date->setTime(0,0,0);
			$start_date = $date->format('Y-m-d H:i:s');
	 		
		} else {
			
			$date = new DateTime(date('Y-m-d H:i:s', $document_created_at));
			$date->setTime(0,0,0);
			$start_date = $date->format('Y-m-d H:i:s');
					
		}

		$unix_start_date = strtotime($start_date);

		do {
					
			$event = $this->checkEvents($unix_start_date, (int)date("Y", (int)$unix_start_date));
			if ($event) {
				$unix_start_date = strtotime('+1 day', (int)$event['stop']);
				$unix_start_date = $this->checkWeekends((int)$unix_start_date);
			}

		} while ($event);

		$real_start_date = 0;
		$the_real_start_date = date('Y-m-d H:i:s', $unix_start_date);


		// La fecha de finalizacion es:
		// La fecha de inicio mas los dias ingresados en la regla
	    $final_date = new DateTime( $the_real_start_date );
	    $timestamp = $final_date->getTimestamp(); // unix
	    $one_day = 86400;

	    for($i=0; $i < (int)$date_rule['days']; $i++){

		        // Que numero es el día actual en el ciclo
		        $nextDay = date('w', ($timestamp));
		        
		        if ($nextDay == 0 || $nextDay == 6) {
		        	// if it's Saturday or Sunday get $i-1
		        	$timestamp = strtotime('+1 day', (int)$timestamp);
		            $i--;

		        } else {

		        	//var_dump(date("Y-m-d", (int)$timestamp));
					$event = $this->checkEvents($timestamp, (int)date("Y", (int)$timestamp));
					
					if ($event) {
						// SI habia evento, no se agregan dias, el contador disminuye		
						$timestamp = strtotime('+1 day', (int)$event['stop']);
						$i--;
					} else {
						// NO es evento agrego un dia mas, el contador suma
						$timestamp = $timestamp + $one_day;
					}
				}			
	    }

	    //var_dump(date("Y-m-d", $timestamp));

	    // Verificar que el ultimo dia no sea evento
	    do {
					
			$event = $this->checkEvents($timestamp, (int)date("Y", (int)$timestamp));
			if ($event) {
				$timestamp = strtotime('+1 day', (int)$event['stop']);				
			}

			$timestamp = $this->checkWeekends((int)$timestamp);

		} while ($event);

	    $final_date->setTimestamp($timestamp);
	    $final_date->setTime(23,59,59);
	    $the_real_end_date = $final_date->format('Y-m-d H:i:s');

	    /*var_dump("Fecha de inicio:");
	    var_dump($the_real_start_date);
	    var_dump("Fecha de vencimiento:");
	    var_dump($the_real_end_date);
	    exit;*/

	    $response = [0 => $the_real_start_date, 1 => $the_real_end_date];

	    return $response;

	}

	function recalculateSIPStartEndDates($unix_start_date, $date_rule){
		// Check events
	 	$start = strtotime('+1 day', $unix_start_date);
		do {
					
			$event = $this->checkEvents($start);
			if ($event) {
				$start = strtotime('+1 day', (int)$event['stop']);
			}

		} while ($event);

		$real_start_date = 0;
		$date_created = new DateTime(date('Y-m-d H:i:s', $start));

		//var_dump(date('Y-m-d H:i:s', $start));

	 	switch ($date_created->format("D")) {
	 		case 'Fri':
	 			
				// Start date after 3 days => Monday
				$real_start_date = strtotime('+3 day', $start);

	 			break;

	 		case 'Sat':
	 			
				// Start date after 2 days => Monday
				$real_start_date = strtotime('+2 day', $start);

	 			break;

	 		case 'Sun':
	 			
				// Start date after 1 days => Monday
				$real_start_date = strtotime('+1 day', $start);

	 			break;

	 		default:
	 			
	 			// Start date after 1 days (Current day + one more)
				$real_start_date = strtotime('+1 day', $start);

	 			break;
	 	}

	 	//var_dump(date('Y-m-d H:i:s', $real_start_date));

	 	$real_date = new DateTime(date('Y-m-d H:i:s', $real_start_date));
		$real_date->setTime(0,0,0);
		$the_real_start_date = $real_date->format('Y-m-d H:i:s');

		// La fecha de finalizacion es:
		// La fecha de inicio mas los dias ingresados en la regla
	    $d = new DateTime( $the_real_start_date );
	    $t = $d->getTimestamp();
	    //var_dump($date_rule['days']);
	    // loop for X days
	    for($i=0; $i < (int)$date_rule['days']; $i++){

	        // add 1 day to timestamp
	        $addDay = 86400;

	        // get what day it is next day
	        $nextDay = date('w', ($t));

	        //var_dump(date('Y-m-d H:i:s', $t));

	        // if it's Saturday or Sunday get $i-1
	        if ($nextDay == 0 || $nextDay == 6) {
	            $i--;
	        }

	        // Search events
	        
			$event = $this->checkEvents($t);

			//var_dump($event);

			if ($event) {			
				$after_event = new DateTime(date('Y-m-d H:i:s', strtotime('+1 day', $event['stop'])));
				$after_event->setTime(0,0,0);
				$unix_date = strtotime($after_event->format('Y-m-d H:i:s'));
				$t = $unix_date;
			}

			if(is_bool($event) && !$event){
				$t = $t+$addDay;
			} else {
				$i--;
			}
			
	    }

	    $t = $t-86400;

	    $d->setTimestamp($t);
	    $d->setTime(23,59,59);
	    $the_real_end_date = $d->format('Y-m-d H:i:s');

	    $response = [0 => $the_real_start_date, 1 => $the_real_end_date];

	    return $response;
	}

	function checkWeekends($timestamp){
		$end_date = (int)$timestamp;
		$day = new DateTime(date('Y-m-d H:i:s', $end_date));

	 	switch ($day->format("D")) {
	 		case 'Sat':
	 			
				// Start date after 2 days => Monday
				$real_start_date = strtotime('+2 day', $end_date);

	 			break;

	 		case 'Sun':
	 			
				// Start date after 1 days => Monday
				$real_start_date = strtotime('+1 day', $end_date);

	 			break;

	 		default:
	 			$real_start_date = $end_date;
	 			break;
	 	}

	 	return $real_start_date;
	}

	/**
	 * Calculates the extension time for sip.
	 *
	 * @param      array  $document_dates  The document dates
	 * @param      array  $extension_rule  The general extension rule
	 * @return     string The new end date for SIP
	 */
	function calculateExtensionTimeForSIP($document_dates, $extension_rule){

		$real_start_date = 0;
	 	$real_start_date = $this->checkWeekends((int)$document_dates['end_date']);
	 	
	 	$extension = (int)$extension_rule['extension']; // Dias de la prorroga

	 	$real_date = new DateTime(date('Y-m-d H:i:s', $real_start_date));
		$the_real_start_date = $real_date->format('Y-m-d H:i:s');

	    $final_date = new DateTime( $the_real_start_date );

	    $timestamp = $final_date->getTimestamp(); // unix
	    $one_day = 86400;

	    for($i=0; $i < (int)$extension; $i++){

		        // Que numero es el día actual en el ciclo
		        $nextDay = date('w', ($timestamp));
		        
		        if ($nextDay == 0 || $nextDay == 6) {
		        	// if it's Saturday or Sunday get $i-1
		        	$timestamp = strtotime('+1 day', (int)$timestamp);
		            $i--;

		        } else {

		        	//var_dump(date("Y-m-d", (int)$timestamp));
					$event = $this->checkEvents($timestamp, (int)date("Y", (int)$timestamp));
					
					if ($event) {
						// SI habia evento, no se agregan dias, el contador disminuye		
						$timestamp = strtotime('+1 day', (int)$event['stop']);
						$i--;
					} else {
						// NO es evento agrego un dia mas, el contador suma
						$timestamp = $timestamp + $one_day;
					}
				}			
	    }

	    // Verificar que el ultimo dia no sea evento
	    do {
					
			$event = $this->checkEvents($timestamp, (int)date("Y", (int)$timestamp));
			if ($event) {
				$timestamp = strtotime('+1 day', (int)$event['stop']);				
			}

			$timestamp = $this->checkWeekends((int)$timestamp);

		} while ($event);

	    $final_date->setTimestamp($timestamp);
	    $final_date->setTime(23,59,59);
	    $the_real_end_date = $final_date->format('Y-m-d H:i:s');

	    /*var_dump("Fecha de vencimiento anterior:");
	    var_dump(date('Y-m-d H:i:s', (int)$document_dates['end_date']));
	    var_dump("Fecha de vencimiento prorrogada:");
	    var_dump($the_real_end_date);
	    exit;*/
	    return $the_real_end_date;

	}


	function addCCResponse($documentid, $attrdef, $value) { /* {{{ */
		if (!is_numeric($attrdef) || !is_numeric($documentid))
			return false;

		$queryStr = "INSERT INTO `tblDocumentAttributes` (`document`,`attrdef`,`value`) VALUES (".(int)$documentid.", ".(int)$attrdef.", ".$this->db->qstr($value).")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}

	} /* }}} */

	function updateCCResponse($documentid, $attrdef, $value) { /* {{{ */
		if (!is_numeric($attrdef) || !is_numeric($documentid))
			return false;

		$queryStr = "UPDATE `tblDocumentAttributes` SET `value`= ".$this->db->qstr($value)." WHERE `document` = ".(int)$documentid." AND `attrdef` = ".(int)$attrdef;
		$res = $this->db->getResult($queryStr);
		if (!$res)
			return false;
		return true;

	} /* }}} */


	function getAllActiveSIP(){
		$queryStr = "SELECT	tblDD.document_id, 
					tblDD.start_date,
			        tblDD.end_date,
			        tblDD.paused,
			        tblSIP.is_revision_active,
			        tblSIP.user_id
		FROM tblDocumentDates tblDD
		INNER JOIN tblSIPAssignments tblSIP 
				ON tblDD.document_id = tblSIP.document_id AND tblSIP.is_revision_active = 1
		ORDER BY tblSIP.user_id";

		// PAUSED = 1 => IN PREVENTION
		// IS_REVISION_ACTIVE = 0 => PUBLISHED, REJECTED

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function getAllActiveCC(){
		$queryStr = "SELECT	tblDD.document_id, 
					tblDD.start_date,
			        tblDD.end_date,
			        tblDD.paused,
			        tblCC.is_revision_active,
			        tblCC.user_id
		FROM tblDocumentDates tblDD
		INNER JOIN tblCCAssignments tblCC 
				ON tblDD.document_id = tblCC.document_id AND tblCC.is_revision_active = 1
		ORDER BY tblCC.user_id";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function getAllActiveMemo(){
		$queryStr = "SELECT	tblDD.document_id, 
					tblDD.start_date,
			        tblDD.end_date,
			        tblDD.paused,
			        tblGA.group_id
		FROM tblDocumentDates tblDD
		INNER JOIN tblDocumentGroupAssignments tblGA 
				ON tblDD.document_id = tblGA.document_id AND tblGA.is_revision_active = 1
		ORDER BY tblGA.group_id";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function getAllGroupActiveMemo($group_id){
		$queryStr = "SELECT	tblDD.document_id, 
					tblDD.start_date,
			        tblDD.end_date,
			        tblDD.paused,
			        tblGA.group_id
		FROM tblDocumentDates tblDD
		INNER JOIN tblDocumentGroupAssignments tblGA 
				ON tblDD.document_id = tblGA.document_id AND tblGA.group_id = ".(int)$group_id." 
		ORDER BY tblDD.document_id";

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}	

	function getAllUserActiveSIP($user_id){
		$queryStr = "SELECT	
					tblDD.document_id, 
					tblDD.start_date,
			        tblDD.end_date,
			        tblDD.paused,
			        tblSIP.is_revision_active,
			        tblSIP.user_id
		FROM tblDocumentDates tblDD
		INNER JOIN tblSIPAssignments tblSIP 
				ON tblDD.document_id = tblSIP.document_id AND tblSIP.is_revision_active = 1 AND tblSIP.user_id = ".(int)$user_id." ORDER BY tblSIP.document_id";

		// PAUSED = 1 => IN PREVENTION
		// IS_REVISION_ACTIVE = 0 => PUBLISHED, REJECTED

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function getAllUserActiveCC($user_id){
		$queryStr = "SELECT	
					tblDD.document_id, 
					tblDD.start_date,
			        tblDD.end_date,
			        tblDD.paused,
			        tblCC.is_revision_active,
			        tblCC.user_id
		FROM tblDocumentDates tblDD
		INNER JOIN tblCCAssignments tblCC 
				ON tblDD.document_id = tblCC.document_id AND tblCC.is_revision_active = 1 AND tblCC.user_id = ".(int)$user_id." ORDER BY tblCC.document_id";

		// PAUSED = 1 => IN PREVENTION
		// IS_REVISION_ACTIVE = 0 => PUBLISHED, REJECTED

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function getAllDocumentsByYear($year){
		$queryStr = "SELECT * FROM `tblDocuments` WHERE FROM_UNIXTIME(date,'%Y') = ".(string)$year;

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function evaluateEndDate($end_date){

		$diff = (int)$end_date - time(); // time returns current time in seconds
		$days_remaining = floor($diff/(60*60*24)); // seconds/minute*minutes/hour*hours/day)

		if ( (int)$days_remaining >= 5){ // is green
			$background_color = "#00a65a";
		} else if ( (int)$days_remaining <= 4 && (int)$days_remaining >= 3 ){ // is yellow
			$background_color = "#f39c12";
		} else if ( (int)$days_remaining <= 2 ){ // is red
			$background_color = "#dd4b39";
		}

		return $background_color;
	}

	function getActionResolution($action_id){
		$queryStr = "SELECT * FROM `tblActionResolution` WHERE `action_id` = ".(string)$action_id;

		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}

	function addActionResolution($action_id, $resolution_id){
		$queryStr = "INSERT INTO `tblActionResolution` (`action_id`,`resolution_id`) VALUES (".(int)$action_id.", ".(int)$resolution_id.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function updateActionResolution($action_id, $resolution_id){

		$resolution_action = $this->getActionResolution($action_id);

		if ($resolution_action) {
			$queryStr = "UPDATE `tblActionResolution` SET `resolution_id`= ".$this->db->qstr($resolution_id)." WHERE `action_id` = ".(int)$action_id;
		} else {
			$queryStr = "INSERT INTO `tblActionResolution` (`action_id`,`resolution_id`) VALUES (".(int)$action_id.", ".(int)$resolution_id.")";
		}

		
		$res = $this->db->getResult($queryStr);
		if (!$res){
			return false;
		}
		return true;
	}

	function saveDocumentInfoType($document_id, $information_type_id, $count){
		$queryStr = "INSERT INTO `tblDocumentInformationTypeCount` (`document_id`,`information_type_id`, `count`) VALUES (".(int)$document_id.", ".(int)$information_type_id.", ".(int)$count.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function getDocumentInfoTypes($document_id){
		$queryStr = "SELECT * FROM `tblDocumentInformationTypeCount` WHERE `document_id` = ".(string)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function getInformationTypeRequestedRow($id) { /* {{{ */
		$queryStr = "SELECT * FROM `tblInformationTypeRequested` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	} /* }}} */


	function addDocumentContentMargins($document_id, $margin_left, $margin_top, $margin_right, $margin_bottom){
		$queryStr = "INSERT INTO `tblDocumentContentMargins` (`document_id`, `margin_left`, `margin_top`, `margin_right`, `margin_bottom`) VALUES (".(int)$document_id.", ".(float)$margin_left.", ".(float)$margin_top.", ".(float)$margin_right.", ".(float)$margin_bottom.")";
		if (!$this->db->getResult($queryStr)) {
			return false;
		} else {
			return true;
		}
	}

	function editDocumentContentMargins($document_id, $margin_left, $margin_top, $margin_right, $margin_bottom){
		$queryStr = "UPDATE `tblDocumentContentMargins` SET 
					`margin_left`= ".(float)$margin_left.", 
					`margin_top`= ".(float)$margin_top.", 
					`margin_right`= ".(float)$margin_right.", 
					`margin_bottom`= ".(float)$margin_bottom." 
					WHERE `document_id` = ".(int)$document_id;
		$res = $this->db->getResult($queryStr);
		if (!$res){
			return false;
		}

		return true;
	}

	function getDocumentContentMargins($document_id){
		$queryStr = "SELECT * FROM `tblDocumentContentMargins` WHERE `document_id` = ".(int)$document_id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}

	/* Document Initials */
	function editDocumentInitials($id, $initials){
		$queryStr = "UPDATE `tblDocumentInititals` SET 
					`initials`= ".$this->db->qstr($initials)." WHERE `id` = ".(int)$id;
		$res = $this->db->getResult($queryStr);
		if (!$res){
			return false;
		}

		return true;
	}

	function getDocumentInitials(){
		$queryStr = "SELECT * FROM `tblDocumentInititals`";
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr;
	}

	function getDocumentInitialByType($type){
		$queryStr = "SELECT * FROM `tblDocumentInititals` WHERE `doctype` = ".$this->db->qstr($type);
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}

	function getDocumentInitialById($id){
		$queryStr = "SELECT * FROM `tblDocumentInititals` WHERE `id` = ".(int)$id;
		$resArr = $this->db->getResultArray($queryStr);
		if (is_bool($resArr) || empty($resArr)){
			return false;
		}

		return $resArr[0];
	}
	/* Document Initials */
}
?>
