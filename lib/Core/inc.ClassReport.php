<?php
/**
 * Implementation of a report object
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent a notification schedule
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_Report { /* {{{ */
	/**
	 * @var integer id of report
	 *
	 * @access protected
	 */
	protected $_id;

	/**
	 * @var string name of report
	 *
	 * @access protected
	 */
	protected $_name;

	/**
	 * @var string comment for report
	 *
	 * @access protected
	 */
	protected $_comment;
	
	/**
	 * @var string query report
	 *
	 * @access protected
	 */
	protected $_query;

	/**
	 * @var string date report created
	 *
	 * @access protected
	 */
	protected $_date;

	/**
	 * @var integer userID of user who created the report
	 *
	 * @access protected
	 */
	protected $_userID;

	/**
	 * @var object reference to the dms instance this user belongs to
	 *
	 * @access protected
	 */
	protected $_dms;

	/**
	 * Constructor
	 *
	 * @param integer $target id of document/folder this notification is
	 * attached to.
	 * @param integer $targettype 1 = target is document, 2 = target is a folder
	 * @param integer $userid id of user. The id is -1 if the notification is
	 * for a group.
	 * @param integer $groupid id of group. The id is -1 if the notification is
	 * for a user.
	 */
	function __construct($id, $name, $comment, $query, $date, $userID) { /* {{{ */
		$this->_id = $id;
		$this->_name = $name;
		$this->_comment = $comment;
		$this->_query = $query;
		$this->_date = $date;
		$this->_userID = $userID;

	} /* }}} */

	/**
	 * Set instance of dms this object belongs to
	 *
	 * @param object $dms instance of dms
	 */
	function setDMS($dms) { /* {{{ */
		$this->_dms = $dms;
	} /* }}} */
	
	/**
	 * Create an instance of a user object
	 *
	 * @param string|integer $id Id, login name, or email of user, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by [name|email]. If 'name' is passed, the method
	 * will check for the 4th paramater and also filter by email. If this
	 * parameter is left empty, the user will be search by its Id.
	 * @param string $email optional email address if searching for name
     * @return object instance of class SeedDMS_Core_User if user was found, null
     * if user was not found, false in case of error
	 */
	public static function getInstance($id, $dms, $by='') { /* {{{ */
		$db = $dms->getDB();

		switch($by) {
		case 'id':
			$queryStr = "SELECT * FROM `tblReports` WHERE `id` = ". (int) $id . " ORDER BY `date`";
			break;
		default:
			$queryStr = "SELECT * FROM `tblReports` WHERE `name` = " . $db->qstr($id) . " ORDER BY `name`";
		}
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return null;

		$resArr = $resArr[0];

		$report = new self($resArr["id"], $resArr["name"], $resArr["comment"], $resArr["query"], $resArr["date"], $resArr["userID"]);
		$report->setDMS($dms);
		$report->setDMS($dms);
		return $report;
	} /* }}} */

	public static function getAllInstancesByUser($userid, $dms) { /* {{{ */
		$db = $dms->getDB();

		$queryStr = "SELECT * FROM `tblReports` WHERE `userID` = ". (int) $userid;
		$queryStr .= " ORDER BY `date`";
		
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$reports = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$report = new self($resArr[$i]["id"], $resArr[$i]["name"], $resArr[$i]["comment"], $resArr[$i]["query"], $resArr[$i]["date"], $resArr[$i]["userID"]);
			$report->setDMS($dms);
			$reports[$i] = $report;
		}

		return $reports;
	} /* }}} */
	
	public static function getAllInstances($dms) { /* {{{ */
		$db = $dms->getDB();

		$queryStr = "SELECT * FROM `tblReports`";
		$queryStr .= " ORDER BY `date`";
		
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$reports = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$report = new self($resArr[$i]["id"], $resArr[$i]["name"], $resArr[$i]["comment"], $resArr[$i]["query"], $resArr[$i]["date"], $resArr[$i]["userID"]);
			$report->setDMS($dms);
			$reports[$i] = $report;
		}

		return $reports;
	} /* }}} */

	/**
	 * Get id of target (document/object) this notification is attachted to
	 *
	 * @return integer id of target
	 */
	function getID() { return $this->_id; }

	/**
	 * Get user for this notification
	 *
	 * @return integer id of user or -1 if this notification does not belong
	 * to a user
	 */
	function getUser() { return $this->_dms->getUser($this->_userid); }

	function setUser($newUser) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblReports` SET `userID` =".$db->qstr($newUser)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_userID = $newUser;
		return true;
	} /* }}} */

	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getDate() { return $this->_date; }
	
	function setDate($newDate) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblReports` SET `date` =".$db->qstr($newDate)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_date = $newDate;
		return true;
	} /* }}} */
	
	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getName() { return $this->_name; }
	
	function setName($newName) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblReports` SET `name` =".$db->qstr($newName)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_name = $newName;
		return true;
	} /* }}} */
	
	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getComment() { return $this->_comment; }
	
	function setComment($newComment) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblReports` SET `comment` =".$db->qstr($newComment)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_name = $newComment;
		return true;
	} /* }}} */

	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getQuery() { return $this->_query; }
	
	function setQuery($newQuery) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblReports` SET `query` =".$db->qstr($newQuery)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_query = $newQuery;
		return true;
	} /* }}} */

	/**
	 * Remove the user and also remove all its keywords, notifications, etc.
	 * Do not remove folders and documents of the user, but assign them
	 * to a different user.
	 *
	 * @param object $user the user doing the removal (needed for entry in
	 *        review and approve log).
	 * @param object $assignToUser the user who is new owner of folders and
	 *        documents which previously were owned by the delete user.
	 * @return boolean true on success or false in case of an error
	 */
	function remove($id=0) { /* {{{ */
		$db = $this->_dms->getDB();
		
		if($id == 0) {
			$id = $this->_id;
		}

		// Delete user itself
		$queryStr = "DELETE FROM `tblReports` WHERE `id` = " . $id;
		var_dump($queryStr);
		if (!$db->getResult($queryStr)) {
			return false;
		}

		return true;
	} /* }}} */
} /* }}} */
?>
