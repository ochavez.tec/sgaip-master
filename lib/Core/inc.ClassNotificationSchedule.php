<?php
/**
 * Implementation of a notification schedule object
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent a notification schedule
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_NotificationSchedule { /* {{{ */
	/**
	 * @var integer id of target (document or folder)
	 *
	 * @access protected
	 */
	protected $_id;

	/**
	 * @var integer id of user to notify
	 *
	 * @access protected
	 */
	protected $_userid;

	/**
	 * @var integer id of group to notify
	 *
	 * @access protected
	 */
	protected $_date;


	/**
	 * @var integer id of group to notify
	 *
	 * @access protected
	 */
	protected $_dateSchedule;

	/**
	 * @var integer id of group to notify
	 *
	 * @access protected
	 */
	protected $_dateSent;
	
	/**
	 * @var integer id of group to notify
	 *
	 * @access protected
	 */
	protected $_toUsers;

	/**
	 * @var integer id of group to notify
	 *
	 * @access protected
	 */
	protected $_toGroups;

	/**
	 * @var integer id of group to notify
	 *
	 * @access protected
	 */
	protected $_subject;
	
	/**
	 * @var integer id of group to notify
	 *
	 * @access protected
	 */
	protected $_message;
	
	/**
	 * @var integer id of group to notify
	 *
	 * @access protected
	 */
	protected $_sent;

	/**
	 * @var object reference to the dms instance this user belongs to
	 *
	 * @access protected
	 */
	protected $_dms;

	/**
	 * Constructor
	 *
	 * @param integer $target id of document/folder this notification is
	 * attached to.
	 * @param integer $targettype 1 = target is document, 2 = target is a folder
	 * @param integer $userid id of user. The id is -1 if the notification is
	 * for a group.
	 * @param integer $groupid id of group. The id is -1 if the notification is
	 * for a user.
	 */
	function __construct($id, $userid, $date, $dateschedule, $datesent, $tousers, $togroups, $subject, $message, $sent='N') { /* {{{ */
		$this->_id = $id;
		$this->_userid = $userid;
		$this->_date = $date;
		$this->_dateSchedule = $dateschedule;
		$this->_dateSent = $datesent;
		$this->_toUsers = $tousers;
		$this->_toGroups = $togroups;
		$this->_subject = $subject;
		$this->_message = $message;
		$this->_sent = $sent;
	} /* }}} */

	/**
	 * Set instance of dms this object belongs to
	 *
	 * @param object $dms instance of dms
	 */
	function setDMS($dms) { /* {{{ */
		$this->_dms = $dms;
	} /* }}} */
	
	/**
	 * Create an instance of a user object
	 *
	 * @param string|integer $id Id, login name, or email of user, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by [name|email]. If 'name' is passed, the method
	 * will check for the 4th paramater and also filter by email. If this
	 * parameter is left empty, the user will be search by its Id.
	 * @param string $email optional email address if searching for name
     * @return object instance of class SeedDMS_Core_User if user was found, null
     * if user was not found, false in case of error
	 */
	public static function getInstance($id, $dms, $by='', $email='') { /* {{{ */
		$db = $dms->getDB();

		switch($by) {
		case 'date':
			$queryStr = "SELECT * FROM `tblNotifySchedules` WHERE `id` = ". (int) $id . " ORDER BY `date`";
			if($email)
				$queryStr .= " AND `email`=".$db->qstr($email);
			break;
		default:
			$queryStr = "SELECT * FROM `tblNotifySchedules` WHERE `id` = " . (int) $id . " ORDER BY `sent`";
		}
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return null;

		$resArr = $resArr[0];

			$notification = new self($resArr[$i]["id"], $resArr[$i]["userID"], $resArr[$i]["date"], $resArr[$i]["dateSchedule"], $resArr[$i]["dateSent"], $resArr[$i]["toUsers"], $resArr[$i]["toGroups"], $resArr[$i]["subject"],$resArr[$i]["message"], $resArr[$i]["sent"]);
			$notification->setDMS($dms);
		$notification->setDMS($dms);
		return $notification;
	} /* }}} */

	public static function getAllInstancesByUser($userid, $dms, $sent='N') { /* {{{ */
		$db = $dms->getDB();

		$queryStr = "SELECT * FROM `tblNotifySchedules` WHERE `userID` = ". (int) $userid;
			
		if($sent == 'N') {
			$queryStr .= " AND `sent` = 'N'";
		} else {
			$queryStr .= " AND `sent` = 'Y'";
		}
		
		$queryStr .= " ORDER BY `date`";
		
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$notifications = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$notification = new self($resArr[$i]["id"], $resArr[$i]["userID"], $resArr[$i]["date"], $resArr[$i]["dateSchedule"], $resArr[$i]["dateSent"], $resArr[$i]["toUsers"], $resArr[$i]["toGroups"], $resArr[$i]["subject"],$resArr[$i]["message"], $resArr[$i]["sent"]);
			$notification->setDMS($dms);
			$notifications[$i] = $notification;
		}

		return $notifications;
	} /* }}} */
	
	public static function getAllInstances($dms, $sent='N') { /* {{{ */
		$db = $dms->getDB();

		$queryStr = "SELECT * FROM `tblNotifySchedules`";
			
		if($sent == 'N') {
			$queryStr .= " WHERE `sent` = 'N'";
		} else {
			$queryStr .= " WHERE `sent` = 'Y'";
		}
		
		$queryStr .= " ORDER BY `date`";
		
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$notifications = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$notification = new self($resArr[$i]["id"], $resArr[$i]["userID"], $resArr[$i]["date"], $resArr[$i]["dateSchedule"], $resArr[$i]["dateSent"], $resArr[$i]["toUsers"], $resArr[$i]["toGroups"], $resArr[$i]["subject"],$resArr[$i]["message"], $resArr[$i]["sent"]);
			$notification->setDMS($dms);
			$notifications[$i] = $notification;
		}

		return $notifications;
	} /* }}} */

	/**
	 * Get id of target (document/object) this notification is attachted to
	 *
	 * @return integer id of target
	 */
	function getID() { return $this->_id; }

	/**
	 * Get user for this notification
	 *
	 * @return integer id of user or -1 if this notification does not belong
	 * to a user
	 */
	function getUser() { return $this->_dms->getUser($this->_userid); }

	function setUser($newUser) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `userID` =".$db->qstr($newUser)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_userid = $newUser;
		return true;
	} /* }}} */

	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getDate() { return $this->_date; }
	
	function setDate($newDate) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `date` =".$db->qstr($newDate)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_date = $newDate;
		return true;
	} /* }}} */
	
	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getDateSchedule() { return $this->_dateSchedule; }	

	function setDateSchedule($newDate) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `dateSchedule` =".$db->qstr($newDate)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_dateSchedule = $newDate;
		return true;
	} /* }}} */

	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getDateSent() { return $this->_dateSent; }
	
	function setDateSent($newDate) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `dateSent` =".$db->qstr($newDate)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_dateSent = $newDate;
		return true;
	} /* }}} */

	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getToUsers() { return $this->_toUsers; }
	
	function setToUsers($newToUsers) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `toUsers` =".$db->qstr($newToUsers)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_toUsers = $newToUsers;
		return true;
	} /* }}} */

	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getToGroups() { return $this->_toGroups; }

	function setToGroups($newToGroups) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `toGroups` =".$db->qstr($newToGroups)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_toGroups = $newToGroups;
		return true;
	} /* }}} */
	
	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getSubject() { return $this->_subject; }

	function setSubject($newSubject) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `subject` =".$db->qstr($newSubject)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_subject = $newSubject;
		return true;
	} /* }}} */
	
	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function getMessage() { return $this->_message; }

	function setMessage($newMessage) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `message` =".$db->qstr($newMessage)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_message = $newMessage;
		return true;
	} /* }}} */
	
	/**
	 * Get group for this notification
	 *
	 * @return integer id of group or -1 if this notification does not belong
	 * to a group
	 */
	function isSent() { return $this->_sent; }
	
	function setSent() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblNotifySchedules` SET `sent` = 'Y' WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_sent = 'Y';
		return true;
	} /* }}} */
	
	/**
	 * Remove the user and also remove all its keywords, notifications, etc.
	 * Do not remove folders and documents of the user, but assign them
	 * to a different user.
	 *
	 * @param object $user the user doing the removal (needed for entry in
	 *        review and approve log).
	 * @param object $assignToUser the user who is new owner of folders and
	 *        documents which previously were owned by the delete user.
	 * @return boolean true on success or false in case of an error
	 */
	function remove($id=0) { /* {{{ */
		$db = $this->_dms->getDB();
		
		if($id == 0) {
			$id = $this->_id;
		}

		// Delete user itself
		$queryStr = "DELETE FROM `tblNotifySchedules` WHERE `id` = " . $id;
		var_dump($queryStr);
		if (!$db->getResult($queryStr)) {
			return false;
		}

		return true;
	} /* }}} */
} /* }}} */
?>
