<?php
/**
 * Implementation of the instrument object in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent an instrument in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_InstrumentVar { /* {{{ */
	/**
	 * @var integer id of instrument
	 *
	 * @access protected
	 */
	var $_id;

	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_instrumentID;
	
	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_code;
	
	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_name;

	/**
	 * @var integer weight of instrument
	 *
	 * @access protected
	 */
	var $_weight;

	/**
	 * @var string comment of instrument
	 *
	 * @access protected
	 */
	var $_comment;

	/**
	 * @var string is_manual of instrument
	 *
	 * @access protected
	 */
	var $_is_manual;
	
	var $_evalJan;
	var $_evalFeb;
	var $_evalMar;
	var $_evalApr;
	var $_evalMay;
	var $_evalJun;
	var $_evalJul;
	var $_evalAug;
	var $_evalSep;
	var $_evalOct;
	var $_evalNov;
	var $_evalDec;

	/**
	 * @var object reference to the dms instance this instrument belongs to
	 *
	 * @access protected
	 */
	var $_dms;

/*
	const role_user = '0';
	const role_admin = '1';
	const role_guest = '2';
*/

	function __construct($id, $instrumentID, $code, $name, $weight, $comment, $is_manual, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec) {
		$this->_id = $id;
		$this->_instrumentID = $instrumentID;
		$this->_code = $code;
		$this->_name = $name;
		$this->_weight = $weight;
		$this->_comment = $comment;
		$this->_is_manual = $is_manual;
		$this->_evalJan = $evalJan;
		$this->_evalFeb = $evalFeb;
		$this->_evalMar = $evalMar;
		$this->_evalApr = $evalApr;
		$this->_evalMay = $evalMay;
		$this->_evalJun = $evalJun;
		$this->_evalJul = $evalJul;
		$this->_evalAug = $evalAug;
		$this->_evalSep = $evalSep;
		$this->_evalOct = $evalOct;
		$this->_evalNov = $evalNov;
		$this->_evalDec = $evalDec;
		
		$this->_dms = null;
	}

	/**
	 * Create an instance of an instrument object
	 *
	 * @param string|integer $id Id, login name, or email of user, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by [name|email]. If 'name' is passed, the method
	 * will check for the 4th paramater and also filter by email. If this
	 * parameter is left empty, the user will be search by its Id.
	 * @param string $email optional email address if searching for name
     * @return object instance of class SeedDMS_Core_User if user was found, null
     * if user was not found, false in case of error
	 */
	public static function getInstance($id, $dms, $by='', $name='') { /* {{{ */
		$db = $dms->getDB();

		switch($by) {
		case 'name':
			$queryStr = "SELECT * FROM `tblInstrumentVars` WHERE `id` = ".$db->qstr($id);
			if($name)
				$queryStr .= " AND `name`=".$db->qstr($name);
			break;
		case 'id':
			$queryStr = "SELECT * FROM `tblInstrumentVars` WHERE `id` = " . (int) $id;
			break;
		default:
			$queryStr = "SELECT * FROM `tblInstrumentVars` WHERE `code` = " . $db->qstr($id);
		}
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return [];
		if (count($resArr) != 1) return [];

		$resArr = $resArr[0];

		$instrumentVar = new self($resArr["id"], $resArr["instrumentID"], $resArr["code"], $resArr["name"], $resArr["weight"], $resArr["comment"], $resArr["is_manual"], $resArr["evalJan"], $resArr["evalFeb"], $resArr["evalMar"], $resArr["evalApr"], $resArr["evalMay"], $resArr["evalJun"], $resArr["evalJul"], $resArr["evalAug"], $resArr["evalSep"], $resArr["evalOct"], $resArr["evalNov"], $resArr["evalDec"]);
		$instrumentVar->setDMS($dms);
		return $instrumentVar;
	} /* }}} */

	public static function getAllInstances($orderby, $dms) { /* {{{ */
		$db = $dms->getDB();

		if($orderby == 'name')
			$queryStr = "SELECT * FROM `tblInstrumentVars` ORDER BY `name`";
		else
			$queryStr = "SELECT * FROM `tblInstrumentVars` ORDER BY `code`";
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return [];

		$instrumentVars = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$instrumentVar = new self($resArr[$i]["id"], $resArr[$i]["instrumentID"], $resArr[$i]["code"], $resArr[$i]["name"], $resArr[$i]["weight"], $resArr[$i]["comment"], $resArr[$i]["is_manual"], $resArr[$i]["evalJan"], $resArr[$i]["evalFeb"], $resArr[$i]["evalMar"], $resArr[$i]["evalApr"], $resArr[$i]["evalMay"], $resArr[$i]["evalJun"], $resArr[$i]["evalJul"], $resArr[$i]["evalAug"], $resArr[$i]["evalSep"], $resArr[$i]["evalOct"], $resArr[$i]["evalNov"], $resArr[$i]["evalDec"]);
			$instrumentVar->setDMS($dms);
			$instrumentVars[$i] = $instrumentVar;
		}

		return $instrumentVars;
	} /* }}} */
	
	public static function getInstancesByInstrumentID($instrumentID, $orderby, $dms) { /* {{{ */
		$db = $dms->getDB();
		
		$queryStr = "SELECT * FROM `tblInstrumentVars` WHERE instrumentID = ".(int)$instrumentID;

		if($orderby == 'name')
			$queryStr .= " ORDER BY `name`";
		else
			$queryStr .= " ORDER BY `code`";
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return [];

		$instrumentVars = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$instrumentVar = new self($resArr[$i]["id"], $resArr[$i]["instrumentID"], $resArr[$i]["code"], $resArr[$i]["name"], $resArr[$i]["weight"], $resArr[$i]["comment"], $resArr[$i]["is_manual"], $resArr[$i]["evalJan"], $resArr[$i]["evalFeb"], $resArr[$i]["evalMar"], $resArr[$i]["evalApr"], $resArr[$i]["evalMay"], $resArr[$i]["evalJun"], $resArr[$i]["evalJul"], $resArr[$i]["evalAug"], $resArr[$i]["evalSep"], $resArr[$i]["evalOct"], $resArr[$i]["evalNov"], $resArr[$i]["evalDec"]);
			$instrumentVar->setDMS($dms);
			$instrumentVars[$i] = $instrumentVar;
		}

		return $instrumentVars;
	} /* }}} */

	function setDMS($dms) {
		$this->_dms = $dms;
	}

	function getID() { return $this->_id; }

	function getCode() { return $this->_code; }

	function setCode($newCode) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `code` = ".$db->qstr($newCode)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_code = $newCode;
		return true;
	} /* }}} */

	function getName() { return $this->_name; }

	function setName($newName) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `name` = ".$db->qstr($newName)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_name = $newName;
		return true;
	} /* }}} */

	function getWeight() { return $this->_weight; }

	function setWeight($newWeight) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `weight` =".$db->qstr($newWeight)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_weight = $newWeight;
		return true;
	} /* }}} */

	function getInstrumentID() { return $this->_instrumentID; }

	function setInstrumentID($newInstrumentID) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `instrumentID` =".$db->qstr($newInstrumentID)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_instrumentID = $newInstrumentID;
		return true;
	} /* }}} */

	function getComment() { return $this->_comment; }

	function setComment($newComment) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `comment` =".$db->qstr($newComment)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_comment = $newComment;
		return true;
	} /* }}} */

	function getIsManual() { return $this->_is_manual; }

	function setIsManual($newIsManual) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `is_manual` =".(int)$newIsManual." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_is_manual = $newIsManual;
		return true;
	} /* }}} */

	function getEvalJan() { return $this->_evalJan; }

	function setEvalJan($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalJan` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalJan = $newValue;
		return true;
	} /* }}} */

	function getEvalFeb() { return $this->_evalFeb; }

	function setEvalFeb($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalFeb` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalFeb = $newValue;
		return true;
	} /* }}} */

	function getEvalMar() { return $this->_evalMar; }

	function setEvalMar($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalMar` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalMar = $newValue;
		return true;
	} /* }}} */

	function getEvalApr() { return $this->_evalApr; }

	function setEvalApr($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalApr` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalApr = $newValue;
		return true;
	} /* }}} */
	
	function getEvalMay() { return $this->_evalMay; }

	function setEvalMay($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalMay` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalMay = $newValue;
		return true;
	} /* }}} */
	
	function getEvalJun() { return $this->_evalJun; }

	function setEvalJun($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalJun` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalJun = $newValue;
		return true;
	} /* }}} */
	
	function getEvalJul() { return $this->_evalJul; }

	function setEvalJul($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalJul` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalJul = $newValue;
		return true;
	} /* }}} */
	
	function getEvalAug() { return $this->_evalAug; }

	function setEvalAug($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalAug` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalAug = $newValue;
		return true;
	} /* }}} */
	
	function getEvalSep() { return $this->_evalSep; }

	function setEvalSep($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalSep` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalSep = $newValue;
		return true;
	} /* }}} */
	
	function getEvalOct() { return $this->_evalOct; }

	function setEvalOct($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalOct` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalOct = $newValue;
		return true;
	} /* }}} */
	
	function getEvalNov() { return $this->_evalNov; }

	function setEvalNov($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalNov` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalNov = $newValue;
		return true;
	} /* }}} */
	
	function getEvalDec() { return $this->_evalDec; }

	function setEvalDec($newValue) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `evalDec` =".(int)$newValue." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_evalDec = $newValue;
		return true;
	} /* }}} */

	/**
	 * Remove the user and also remove all its keywords, notifications, etc.
	 * Do not remove folders and documents of the user, but assign them
	 * to a different user.
	 *
	 * @param object $user the user doing the removal (needed for entry in
	 *        review and approve log).
	 * @param object $assignToUser the user who is new owner of folders and
	 *        documents which previously were owned by the delete user.
	 * @return boolean true on success or false in case of an error
	 */
	function remove($id=0) { /* {{{ */
		$db = $this->_dms->getDB();
		
		if($id == 0) {
			$id = $this->_id;
		}

		// Delete user itself
		$queryStr = "DELETE FROM `tblInstrumentVars` WHERE `id` = " . $id;
		if (!$db->getResult($queryStr)) {
			return false;
		}

		return true;
	} /* }}} */
} /* }}} */
?>
