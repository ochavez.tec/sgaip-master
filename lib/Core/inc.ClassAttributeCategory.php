<?php
/**
 * Implementation of attribute categories in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Herson Cruz <herson@multisistemas.com.sv>
 * @copyright  Copyright (C) 2018 Herson Cruz
 * @version    Release: 5.1.5
 */

/**
 * Class to represent an attribute category in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Herson Cruz <herson@multisistemas.com.sv>
 * @copyright  Copyright (C)2018 Herson Cruz
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_AttributeCategory {
	/**
	 * @var integer $_id id of document category
	 * @access protected
	 */
	protected $_id;

	/**
	 * @var string $_name name of category
	 * @access protected
	 */
	protected $_name;

	/**
	 * @var object $_dms reference to dms this category belongs to
	 * @access protected
	 */
	protected $_dms;
	
	protected $_categories;

	function __construct($id, $name) { /* {{{ */
		$this->_id = $id;
		$this->_name = $name;
		$this->_dms = null;
		$this->_categories = array();
	} /* }}} */

	function setDMS($dms) { /* {{{ */
		$this->_dms = $dms;
	} /* }}} */

	function getID() { return $this->_id; }

	function getName() { return $this->_name; }

	function setName($newName) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblCategory` SET `name` = ".$db->qstr($newName)." WHERE `id` = ". $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		$this->_name = $newName;
		return true;
	} /* }}} */

	function isUsed() { /* {{{ */
		$db = $this->_dms->getDB();
		
		$queryStr = "SELECT * FROM `tblAttributeCategory` WHERE `categoryID`=".$this->_id;
		$resArr = $db->getResultArray($queryStr);
		if (is_array($resArr) && count($resArr) == 0)
			return false;
		return true;
	} /* }}} */

	function remove() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "DELETE FROM `tblCategory` WHERE `id` = " . $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		return true;
	} /* }}} */

	function getAttributesByCategory($limit=0, $offset=0) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "SELECT * FROM `tblAttributeCategory` where `categoryID`=".$this->_id;
		if($limit && is_numeric($limit))
			$queryStr .= " LIMIT ".(int) $limit;
		if($offset && is_numeric($offset))
			$queryStr .= " OFFSET ".(int) $offset;
		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$attributes = array();
		foreach ($resArr as $row) {
			if($attr = $this->_dms->getAttribute($row["attributeID"])) // NO NEED to Create function getAttribute
				array_push($attributes, $attr);
		}
		return $attributes;
	} /* }}} */

	function countAttributesByCategory() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "SELECT COUNT(*) as `c` FROM `tblAttributeCategory` where `categoryID`=".$this->_id;
		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr[0]['c'];
	} /* }}} */

	/**
	 * Retrieve a list of all categories this document belongs to
	 *
	 * @return array list of category objects
	 */
	function getCategories() { /* {{{ */
		$db = $this->_dms->getDB();

		if(!$this->_categories) {
			$queryStr = "SELECT * FROM `tblCategory` where `id` in (select `categoryID` from `tblAttributeCategory` where `attributeID` = ".$this->_id.")";
			$resArr = $db->getResultArray($queryStr);
			if (is_bool($resArr) && !$resArr)
				return false;

			foreach ($resArr as $row) {
				$cat = new SeedDMS_Core_AttributeCategory($row['id'], $row['name']);
				$cat->setDMS($this->_dms);
				$this->_categories[] = $cat;
			}
		}
		return $this->_categories;
	} /* }}} */

	/**
	 * Set a list of categories for the document
	 * This function will delete currently assigned categories and sets new
	 * categories.
	 *
	 * @param array $newCategories list of category objects
	 */
	function setCategories($newCategories) { /* {{{ */
		$db = $this->_dms->getDB();

		$db->startTransaction();
		$queryStr = "DELETE from `tblAttributeCategory` WHERE `attributeID` = ". $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}

		foreach($newCategories as $cat) {
			$queryStr = "INSERT INTO `tblAttributeCategory` (`categoryID`, `attributeID`) VALUES (". $cat->getId() .", ". $this->_id .")";
			if (!$db->getResult($queryStr)) {
				$db->rollbackTransaction();
				return false;
			}
		}

		$db->commitTransaction();
		$this->_categories = $newCategories;
		return true;
	} /* }}} */

	/**
	 * Add a list of categories to the document
	 * This function will add a list of new categories to the document.
	 *
	 * @param array $newCategories list of category objects
	 */
	function addCategories($newCategories) { /* {{{ */
		$db = $this->_dms->getDB();

		if(!$this->_categories)
			self::getCategories();

		$catids = array();
		foreach($this->_categories as $cat)
			$catids[] = $cat->getID();

		$db->startTransaction();
		$ncat = array(); // Array containing actually added new categories
		foreach($newCategories as $cat) {
			if(!in_array($cat->getID(), $catids)) {
				$queryStr = "INSERT INTO `tblAttributeCategory` (`categoryID`, `attributeID`) VALUES (". $cat->getId() .", ". $this->_id .")";
				if (!$db->getResult($queryStr)) {
					$db->rollbackTransaction();
					return false;
				}
				$ncat[] = $cat;
			}
		}
		$db->commitTransaction();
		$this->_categories = array_merge($this->_categories, $ncat);
		return true;
	} /* }}} */

	/**
	 * Remove a list of categories from the document
	 * This function will remove a list of assigned categories to the document.
	 *
	 * @param array $newCategories list of category objects
	 */
	function removeCategories($categories) { /* {{{ */
		$db = $this->_dms->getDB();

		$catids = array();
		foreach($categories as $cat)
			$catids[] = $cat->getID();

		$queryStr = "DELETE from `tblAttributeCategory` WHERE `attributeID` = ". $this->_id ." AND `categoryID` IN (".implode(',', $catids).")";
		if (!$db->getResult($queryStr)) {
			return false;
		}

		$this->_categories = null;
		return true;
	} /* }}} */


}

?>
