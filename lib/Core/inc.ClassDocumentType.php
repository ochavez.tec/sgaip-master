<?php
/**
 * Implementation of document categories in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent a document Type in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C)2011 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_DocumentType {
	/**
	 * @var integer $_id id of document Type
	 * @access protected
	 */
	protected $_id;
	
	protected $_codeMask;

	/**
	 * @var string $_name name of Type
	 * @access protected
	 */
	protected $_name;
	
	protected $_instrumentVars;
	
	protected $_categories;

	/**
	 * @var object $_dms reference to dms this Type belongs to
	 * @access protected
	 */
	protected $_dms;

	function __construct($id, $name, $codeMask) { /* {{{ */
		$this->_id = $id;
		$this->_name = $name;
		$this->_codeMask = $codeMask;
		$this->_instrumentVars = null;
		$this->_categories = null;
		$this->_dms = null;
	} /* }}} */

	function setDMS($dms) { /* {{{ */
		$this->_dms = $dms;
	} /* }}} */
	
	/**
	 * Return an instance of a group object
	 *
	 * @param string|integer $id Id, name of group, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by group name if set to 'name'. 
	 * Search by Id of group if left empty.
	 * @return object instance of class SeedDMS_Core_Group if group was found, null
	 * if group was not found, false in case of error
	 */
	public static function getInstance($id, $dms, $by='') { /* {{{ */
		$db = $dms->getDB();

		switch($by) {
		case 'name':
			$queryStr = "SELECT * FROM `tblType` WHERE `name` = ".$db->qstr($id);
			break;
		default:
			$queryStr = "SELECT * FROM `tblType` WHERE `id` = " . (int) $id;
		}

		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && $resArr == false)
			return false;
		else if (count($resArr) != 1) //wenn, dann wohl eher 0 als > 1 ;-)
			return null;

		$resArr = $resArr[0];

		$cat = new self($resArr["id"], $resArr["name"], $resArr["codeMask"]);
		$cat->setDMS($dms);
		return $cat;
	} /* }}} */

	function getID() { return $this->_id; }

	function getCodeMask() { return $this->_codeMask; }

	function setCodeMask($newCodeMask) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblType` SET `codeMask` = ".$db->qstr($newCodeMask)." WHERE `id` = ". $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		$this->_codeMask = $newCodeMask;
		return true;
	} /* }}} */


	function getName() { return $this->_name; }

	function setName($newName) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblType` SET `name` = ".$db->qstr($newName)." WHERE `id` = ". $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		$this->_name = $newName;
		return true;
	} /* }}} */

	function isUsed() { /* {{{ */
		$db = $this->_dms->getDB();
		
		$queryStr = "SELECT * FROM `tblDocumentType` WHERE `typeID`=".$this->_id;
		$resArr = $db->getResultArray($queryStr);
		if (is_array($resArr) && count($resArr) == 0)
			return false;
		return true;
	} /* }}} */

	function remove() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "DELETE FROM `tblType` WHERE `id` = " . $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		return true;
	} /* }}} */

	function getDocumentsByType($limit=0, $offset=0) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "SELECT * FROM `tblDocumentType` where `typeID`=".$this->_id;
		if($limit && is_numeric($limit))
			$queryStr .= " LIMIT ".(int) $limit;
		if($offset && is_numeric($offset))
			$queryStr .= " OFFSET ".(int) $offset;
		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$documents = array();
		foreach ($resArr as $row) {
			if($doc = $this->_dms->getDocument($row["documentID"]))
				array_push($documents, $doc);
		}
		return $documents;
	} /* }}} */

	function countDocumentsByType() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "SELECT COUNT(*) as `c` FROM `tblDocumentType` where `typeID`=".$this->_id;
		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr[0]['c'];
	} /* }}} */

	/**
	 * Retrieve a list of all categories this document belongs to
	 *
	 * @return array list of category objects
	 */
	function getInstrumentVars() { /* {{{ */
		$db = $this->_dms->getDB();

		if(!is_array($this->_instrumentVars)) {
			$queryStr = "SELECT * 
			 			   FROM `tblInstrumentVars` 
			              WHERE `id` IN (SELECT `instrumentVarID` 
			              				   FROM `tblInstrumentVarType` 
			              				  WHERE `typeID` = ".$this->_id.")";
			$resArr = $db->getResultArray($queryStr);
			if (is_bool($resArr) && !$resArr)
				return [];
			
			$this->_instrumentVars = array();
			foreach ($resArr as $row) {
				$instrumentVar = new SeedDMS_Core_InstrumentVar($row["id"], $row["instrumentID"], $row["code"], $row["name"], $row["weight"], $row["comment"], $row["is_manual"], $row["evalJan"], $row["evalFeb"], $row["evalMar"], $row["evalApr"], $row["evalMay"], $row["evalJun"], $row["evalJul"], $row["evalAug"], $row["evalSep"], $row["evalOct"], $row["evalNov"], $row["evalDec"]);
				$instrumentVar->setDMS($this->_dms);
				$this->_instrumentVars[] = $instrumentVar;
			}
		}
		return $this->_instrumentVars;
	} /* }}} */

	/**
	 * Set a list of categories for the document
	 * This function will delete currently assigned categories and sets new
	 * categories.
	 *
	 * @param array $newCategories list of category objects
	 */
	function setInstrumentVars($newInstrumentVars) { /* {{{ */
		$db = $this->_dms->getDB();

		$db->startTransaction();
		$queryStr = "DELETE from `tblInstrumentVarType` WHERE `typeID` = ". $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}

		foreach($newInstrumentVars as $instvar) {
			$queryStr = "INSERT INTO `tblInstrumentVarType` (`typeID`, `instrumentVarID`) VALUES (". $this->_id .", ". $instvar->getID() .")";
			if (!$db->getResult($queryStr)) {
				$db->rollbackTransaction();
				return false;
			}
		}

		$db->commitTransaction();
		$this->_instrumentVars = $newInstrumentVars;
		return true;
	} /* }}} */
	
	/**
	 * Set a list of categories for the document
	 * This function will delete currently assigned categories and sets new
	 * categories.
	 *
	 * @param array $newCategories list of category objects
	 */
	function delInstrumentVars() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "DELETE from `tblInstrumentVarType` WHERE `typeID` = ". $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		
		$this->_instrumentVars = null;
		return true;
	} /* }}} */

	/**
	 * Retrieve a list of all categories this document belongs to
	 *
	 * @return array list of category objects
	 */
	function getCategories() { /* {{{ */
		$db = $this->_dms->getDB();

		if(!is_array($this->_categories)) {
			$queryStr = "SELECT * 
			 			   FROM `tblCategory` 
			              WHERE `id` IN (SELECT `categoryID` 
			              				   FROM `tblTypeCategory` 
			              				  WHERE `typeID` = ".$this->_id.")";
			$resArr = $db->getResultArray($queryStr);
			if (is_bool($resArr) && !$resArr)
				return [];
			
			$this->_categories = array();
			foreach ($resArr as $row) {
				$category = new SeedDMS_Core_DocumentCategory($row["id"], $row["name"]);
				$category->setDMS($this->_dms);
				$this->_categories[] = $category;
			}
		}
		return $this->_categories;
	} /* }}} */

	/**
	 * Retrieve a list of all categories this document belongs to
	 *
	 * @return array list of category objects
	 */
	function getCategoriesID() { /* {{{ */
		$db = $this->_dms->getDB();
		
		$categories = array();

		$queryStr = "SELECT * FROM `tblCategory` where `id` in (select `categoryID` from `tblTypeCategory` where `typeID` = ".$this->_id.")";
		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		foreach ($resArr as $row) {
			$categories[] = $row['id'];
		}
		
		return $categories;
	} /* }}} */

	/**
	 * Set a list of categories for the document
	 * This function will delete currently assigned categories and sets new
	 * categories.
	 *
	 * @param array $newCategories list of category objects
	 */
	function setCategories($newCategories) { /* {{{ */
		$db = $this->_dms->getDB();

		$db->startTransaction();
		$queryStr = "DELETE from `tblTypeCategory` WHERE `typeID` = ". $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}

		foreach($newCategories as $cat) {
			$queryStr = "INSERT INTO `tblTypeCategory` (`typeID`, `categoryID`) VALUES (". $this->_id .", ". $cat->getID() .")";
			if (!$db->getResult($queryStr)) {
				$db->rollbackTransaction();
				return false;
			}
		}

		$db->commitTransaction();
		$this->_categories = $newCategories;
		return true;
	} /* }}} */
	
	/**
	 * Set a list of categories for the document
	 * This function will delete currently assigned categories and sets new
	 * categories.
	 *
	 * @param array $newCategories list of category objects
	 */
	function delCategories() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "DELETE from `tblTypeCategory` WHERE `typeID` = ". $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		
		$this->_instrumentVars = null;
		return true;
	} /* }}} */


}

?>
