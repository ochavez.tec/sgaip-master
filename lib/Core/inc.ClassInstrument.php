<?php
/**
 * Implementation of the instrument object in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent an instrument in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_Instrument { /* {{{ */
	/**
	 * @var integer id of instrument
	 *
	 * @access protected
	 */
	var $_id;

	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_name;

	/**
	 * @var integer weight of instrument
	 *
	 * @access protected
	 */
	var $_weight;

	/**
	 * @var integer score of instrument
	 *
	 * @access protected
	 */
	var $_score;

	/**
	 * @var string comment of instrument
	 *
	 * @access protected
	 */
	var $_comment;

	/**
	 * @var object reference to the dms instance this instrument belongs to
	 *
	 * @access protected
	 */
	var $_dms;

/*
	const role_user = '0';
	const role_admin = '1';
	const role_guest = '2';
*/

	function __construct($id, $name, $weight, $score, $comment) {
		$this->_id = $id;
		$this->_name = $name;
		$this->_weight = $weight;
		$this->_score = $score;
		$this->_comment = $comment;
		$this->_dms = null;
	}

	/**
	 * Create an instance of an instrument object
	 *
	 * @param string|integer $id Id, login name, or email of user, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by [name|email]. If 'name' is passed, the method
	 * will check for the 4th paramater and also filter by email. If this
	 * parameter is left empty, the user will be search by its Id.
	 * @param string $email optional email address if searching for name
     * @return object instance of class SeedDMS_Core_User if user was found, null
     * if user was not found, false in case of error
	 */
	public static function getInstance($id, $dms, $by='', $name='') { /* {{{ */
		$db = $dms->getDB();

		switch($by) {
		case 'name':
			$queryStr = "SELECT * FROM `tblInstruments` WHERE `id` = ".$db->qstr($id);
			if($name)
				$queryStr .= " AND `name`=".$db->qstr($name);
			break;
		default:
			$queryStr = "SELECT * FROM `tblInstruments` WHERE `id` = " . (int) $id;
		}
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return null;

		$resArr = $resArr[0];

		$instrument = new self($resArr["id"], $resArr["name"], $resArr["weight"], $resArr["score"], $resArr["comment"]);
		$instrument->setDMS($dms);
		return $instrument;
	} /* }}} */

	public static function getAllInstances($orderby, $dms) { /* {{{ */
		$db = $dms->getDB();

		if($orderby == 'name')
			$queryStr = "SELECT * FROM `tblInstruments` ORDER BY `name`";
		else
			$queryStr = "SELECT * FROM `tblInstruments` ORDER BY `weight`";
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$instruments = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$instrument = new self($resArr[$i]["id"], $resArr[$i]["name"], $resArr[$i]["weight"], $resArr[$i]["score"], $resArr[$i]["comment"]);
			$instrument->setDMS($dms);
			$instruments[$i] = $instrument;
		}

		return $instruments;
} /* }}} */

	function setDMS($dms) {
		$this->_dms = $dms;
	}

	function getID() { return $this->_id; }

	function getName() { return $this->_name; }

	function setName($newName) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstruments` SET `name` = ".$db->qstr($newName)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_name = $newName;
		return true;
	} /* }}} */

	function getWeight() { return $this->_weight; }

	function setWeight($newWeight) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstruments` SET `weight` =".$db->qstr($newWeight)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_weight = $newWeight;
		return true;
	} /* }}} */

	function getScore() { return $this->_score; }

	function setScore($newScore) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstruments` SET `score` =".$db->qstr($newScore)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_score = $newScore;
		return true;
	} /* }}} */

	function getComment() { return $this->_comment; }

	function setComment($newComment) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstruments` SET `comment` =".$db->qstr($newComment)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_comment = $newComment;
		return true;
	} /* }}} */

	/**
	 * Remove the user and also remove all its keywords, notifications, etc.
	 * Do not remove folders and documents of the user, but assign them
	 * to a different user.
	 *
	 * @param object $user the user doing the removal (needed for entry in
	 *        review and approve log).
	 * @param object $assignToUser the user who is new owner of folders and
	 *        documents which previously were owned by the delete user.
	 * @return boolean true on success or false in case of an error
	 */
	function remove($instrument) { /* {{{ */
		$db = $this->_dms->getDB();

		$db->startTransaction();

		// Delete instrument from all axes
		$queryStr = "DELETE FROM `tblAxeMembers` WHERE `instrumentID` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}

		// Delete user itself
		$queryStr = "DELETE FROM `tblInstruments` WHERE `id` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}

		$db->commitTransaction();
		return true;
	} /* }}} */

	/**
	 * Make the instrument a member of an axis
	 * This function uses {@link SeedDMS_Group::addUser} but checks before if
	 * the user is already a member of the group.
	 *
	 * @param object $group group to be the member of
	 * @return boolean true on success or false in case of an error or the user
	 *        is already a member of the group
	 */
	function joinAxe($axe) { /* {{{ */
		if ($axe->isMember($this))
			return false;

		if (!$axe->addInstrument($this))
			return false;

		unset($this->_axes);
		return true;
	} /* }}} */

	/**
	 * Removes the instrument from an axe
	 * This function uses {@link SeedDMS_Group::removeUser} but checks before if
	 * the user is a member of the group at all.
	 *
	 * @param object $group group to leave
	 * @return boolean true on success or false in case of an error or the user
	 *        is not a member of the group
	 */
	function leaveAxe($axe) { /* {{{ */
		if (!$axe->isMember($this))
			return false;

		if (!$axe->removeInstrument($this))
			return false;

		unset($this->_axes);
		return true;
	} /* }}} */

	/**
	 * Get all axes the instrument is a member of
	 *
	 * @return array list of axes
	 */
	function getAxes() { /* {{{ */
		$db = $this->_dms->getDB();

		if (!isset($this->_axes))
		{
			$queryStr = "SELECT `tblAxes`.*, `tblAxeMembers`.`instrumentID` FROM `tblAxes` ".
				"LEFT JOIN `tblAxeMembers` ON `tblAxes`.`id` = `tblAxeMembers`.`axeID` ".
				"WHERE `tblAxeMembers`.`instrumentID`='". $this->_id ."'";
			$resArr = $db->getResultArray($queryStr);
			if (is_bool($resArr) && $resArr == false)
				return false;

			$this->_axes = array();
			$classname = $this->_dms->getClassname('axe');
			foreach ($resArr as $row) {
				$axe = new $classname($row["id"], $row["name"], $row["comment"], $row["maxScore"]);
				$axe->setDMS($this->_dms);
				array_push($this->_axes, $axe);
			}
		}
		return $this->_axes;
	} /* }}} */

	/**
	 * Checks if instrument is member of a given axe
	 *
	 * @param object $group
	 * @return boolean true if user is member of the given group otherwise false
	 */
	function isMemberOfAxe($axe) { /* {{{ */
		return $axe->isMember($this);
	} /* }}} */
	
	function getEvalDate() {
		$db = $this->_dms->getDB();

		$queryStr = "SELECT DISTINCT `date` FROM `tblInstrumentEvalss` WHERE `instrumentID` = ".$this->_id." AND `date` = ".$date." ORDER BY `date`";
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$dates = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$dates[$i] = $instrument;
		}

		return $instruments;
		
	}
} /* }}} */
?>
