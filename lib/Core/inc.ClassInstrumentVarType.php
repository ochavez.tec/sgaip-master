<?php
/**
 * Implementation of the instrument object in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent an instrument in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_InstrumentVarType { /* {{{ */
	/**
	 * @var integer id of instrument
	 *
	 * @access protected
	 */
	var $_typeID;

	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_instrumentVarID;
	
	/**
	 * @var object reference to the dms instance this instrument belongs to
	 *
	 * @access protected
	 */
	var $_dms;
	
	var $_instrumentVars;

/*
	const role_user = '0';
	const role_admin = '1';
	const role_guest = '2';
*/

	function __construct($typeID, $instrumentVarID) {
		$this->_typeID = $typeID;
		$this->_instrumentVarID = $instrumentVarID;
		$this->_instrumentVars = null;
		$this->_dms = null;
	}

	/**
	 * Create an instance of an instrument object
	 *
	 * @param string|integer $id Id, login name, or email of user, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by [name|email]. If 'name' is passed, the method
	 * will check for the 4th paramater and also filter by email. If this
	 * parameter is left empty, the user will be search by its Id.
	 * @param string $email optional email address if searching for name
     * @return object instance of class SeedDMS_Core_User if user was found, null
     * if user was not found, false in case of error
	 */
	public static function getInstance($typeID, $instrumentVarID, $dms) { /* {{{ */
		$db = $dms->getDB();

		$queryStr = "SELECT * FROM `tblInstrumentVarType` WHERE `typeID` = ". (int)$typeID;
		$queryStr .= " AND `instrumentVarID` = ". (int)$instrumentVarID;
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return null;

		$resArr = $resArr[0];

		$instrumentVarType = new self($resArr["typeID"], $resArr["instrumentVarID"]);
		$instrumentVarType->setDMS($dms);
		return $instrumentVarType;
	} /* }}} */

	public static function getAllInstances($orderby, $dms) { /* {{{ */
		$db = $dms->getDB();

		if($orderby == 'name')
			$queryStr = "SELECT * FROM `tblInstrumentVarType` ORDER BY `name`";
		else
			$queryStr = "SELECT * FROM `tblInstrumentVarType` ORDER BY `code`";
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$instrumentVarTypes = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$instrumentVarType = new self($resArr[$i]["typeID"], $resArr[$i]["instrumentVarID"]);
			$instrumentVarType->setDMS($dms);
			$instrumentVarTypes[$i] = $instrumentVarType;
		}

		return $instrumentVarTypes;
	} /* }}} */

	function setDMS($dms) {
		$this->_dms = $dms;
	}

	function getTypeID() { return $this->_typeID; }

	function getInstrumentVarID() { return $this->_instrumentVarID; }

} /* }}} */
?>
