<?php
/**
 * Implementation of document categories in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent a document category in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C)2011 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_DocumentInstrumentVar {
	/**
	 * @var integer $_id id of document intrument var
	 * @access protected
	 */
	protected $_id;

	/**
	 * @var string $_name name of instrument var
	 * @access protected
	 */
	protected $_name;

	/**
	 * @var object $_dms reference to dms this category belongs to
	 * @access protected
	 */
	protected $_dms;

	function __construct($id, $name) { /* {{{ */
		$this->_id = $id;
		$this->_name = $name;
		$this->_dms = null;
	} /* }}} */

	function setDMS($dms) { /* {{{ */
		$this->_dms = $dms;
	} /* }}} */

	function getID() { return $this->_id; }
	
	function getCode() {
		$db = $this->_dms->getDB();
		$queryStr = "SELECT code FROM `tblInstrumentVars` WHERE `id`=".$this->_id;
		$resArr = $db->getResultArray($queryStr);
		if (is_array($resArr) && count($resArr) == 0 && count($resArr) > 1)
			return false;
		return $resArr[0]['code'];
	}

	function getName() { return $this->_name; }

	function setName($newName) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `name` = ".$db->qstr($newName)." WHERE `id` = ". $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		$this->_name = $newName;
		return true;
	} /* }}} */

	function isUsed() { /* {{{ */
		$db = $this->_dms->getDB();
		
		$queryStr = "SELECT * FROM `tblDocumentInstrumentVar` WHERE `instrumentVarID`=".$this->_id;
		$resArr = $db->getResultArray($queryStr);
		if (is_array($resArr) && count($resArr) == 0)
			return false;
		return true;
	} /* }}} */

	function remove() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "DELETE FROM `tblInstrumentVars` WHERE `id` = " . $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		return true;
	} /* }}} */

	function getDocumentsByInstrumentVar($limit=0, $offset=0) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "SELECT * FROM `tblDocumentInstrumentVar` where `instrumentVarID`=".$this->_id;
		if($limit && is_numeric($limit))
			$queryStr .= " LIMIT ".(int) $limit;
		if($offset && is_numeric($offset))
			$queryStr .= " OFFSET ".(int) $offset;
		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		$documents = array();
		foreach ($resArr as $row) {
			if($doc = $this->_dms->getDocument($row["documentID"]))
				array_push($documents, $doc);
		}
		return $documents;
	} /* }}} */

	function countDocumentsByInstrumentVar() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "SELECT COUNT(*) as `c` FROM `tblDocumentInstrumentvar` where `instrumentVarID`=".$this->_id;
		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && !$resArr)
			return false;

		return $resArr[0]['c'];
	} /* }}} */

}

?>
