<?php
/**
 * Implementation of the instrument object in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent an instrument in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_InstrumentEval { /* {{{ */
	/**
	 * @var integer id of instrument
	 *
	 * @access protected
	 */
	var $_instrumentID;

	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_date;

	/**
	 * @var integer score of instrument
	 *
	 * @access protected
	 */
	var $_score;

	/**
	 * @var integer weight of instrument
	 *
	 * @access protected
	 */
	var $_result;
	
	/**
	 * @var string comment of instrument
	 *
	 * @access protected
	 */
	var $_comment;

	/**
	 * @var object reference to the dms instance this instrument belongs to
	 *
	 * @access protected
	 */
	var $_dms;

/*
	const role_user = '0';
	const role_admin = '1';
	const role_guest = '2';
*/

	function __construct($instrumentID, $date, $score, $result, $comment) {
		$this->_instrumentID = $instrumentID;
		$this->_date = $date;
		$this->_score = $score;
		$this->_result = $result;
		$this->_comment = $comment;
		$this->_dms = null;
	}
	
	public static function add() { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "INSERT INTO `tblInstrumentEvals`(`instrumentID`, `date`, `score`, `result`, `comment`) VALUES (".(int) $this->_instrumentID.", ".$db->qstr($this->_date).", ".(float)$this->_score.", ".(float)$this->_result.",".$db->qstr($this->_comment).") ";
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return null;

		$resArr = $resArr[0];

		$instrumentEval = new self($resArr["instrumentID"], $resArr["date"], $resArr["score"], $resArr["result"], $resArr["comment"]);
		$instrumentEval->setDMS($dms);
		return $instrumentEval;
	} /* }}} */

	/**
	 * Create an instance of an instrument object
	 *
	 * @param string|integer $id Id, login name, or email of user, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by [name|email]. If 'name' is passed, the method
	 * will check for the 4th paramater and also filter by email. If this
	 * parameter is left empty, the user will be search by its Id.
	 * @param string $email optional email address if searching for name
     * @return object instance of class SeedDMS_Core_User if user was found, null
     * if user was not found, false in case of error
	 */
	public static function getInstance($instrumentID, $date, $dms) { /* {{{ */
		$db = $dms->getDB();

		$queryStr = "SELECT * FROM `tblInstrumentEvals` WHERE `instrumentID` = ". (int) $instrumentID;
		$queryStr .= " AND `date`=".$db->qstr($date);
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return null;

		$resArr = $resArr[0];

		$instrumentEval = new self($resArr["instrumentID"], $resArr["date"], $resArr["score"], $resArr["result"], $resArr["comment"]);
		$instrumentEval->setDMS($dms);
		return $instrumentEval;
	} /* }}} */

	public static function getAllInstances($date, $dms) { /* {{{ */
		$db = $dms->getDB();

		$queryStr = "SELECT * FROM `tblInstrumentEvals` WHERE `date`=".$db->qstr($date);
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$instrumentEvals = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$instrumentEval = new self($resArr[$i]["instrumentID"], $resArr[$i]["date"], $resArr[$i]["score"], $resArr[$i]["result"], $resArr[$i]["comment"]);
			$instrumentEval->setDMS($dms);
			$instrumentEvals[$i] = $instrumentEval;
		}

		return $instrumentEvals;
	} /* }}} */

	function setDMS($dms) {
		$this->_dms = $dms;
	}

	function getID() { return ['instrumentID' => $this->_instrumentID, 'date' => $this->_date]; }

	function getInstrumentID() { return $this->_instrumentID; }
	function getDate() { return $this->_date; }

// 	function setDate($newDate) { /* {{{ */
// 		$db = $this->_dms->getDB();
// 
// 		$queryStr = "UPDATE `tblInstrumentEvals` SET date` = ".$db->qstr($newDate)." WHERE `instrumentID` = " . $this->_instrumentID;
// 		$res = $db->getResult($queryStr);
// 		if (!$res)
// 			return false;
// 
// 		$this->_date = $newDate;
// 		return true;
// 	} /* }}} */

	function getScore() { return $this->_score; }

	function setScore($newScore) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentEvals` SET `score` =".$db->qstr($newScore)." WHERE `instrumentID` = " . $this->_instrumentID;
		$queryStr .= " AND `date`=".$db->qstr($this->_date);
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_score = $newScore;
		return true;
	} /* }}} */
	
	function getResult() { return $this->_result; }

	function setResult($newResult) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentEvals` SET `result` =".$db->qstr($newResult)." WHERE `instrumentID` = " . $this->_id;
		$queryStr .= " AND `date`=".$db->qstr($this->_date);
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_weight = $newWeight;
		return true;
	} /* }}} */

	function getComment() { return $this->_comment; }

	function setComment($newComment) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentEvals` SET `comment` =".$db->qstr($newComment)." WHERE `instrumentID` = " . $this->_instrumentID;
		$queryStr .= " AND `date`=".$db->qstr($this->_date);
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_comment = $newComment;
		return true;
	} /* }}} */

	/**
	 * Remove the user and also remove all its keywords, notifications, etc.
	 * Do not remove folders and documents of the user, but assign them
	 * to a different user.
	 *
	 * @param object $user the user doing the removal (needed for entry in
	 *        review and approve log).
	 * @param object $assignToUser the user who is new owner of folders and
	 *        documents which previously were owned by the delete user.
	 * @return boolean true on success or false in case of an error
	 */
	function remove() { /* {{{ */
		$db = $this->_dms->getDB();

		$db->startTransaction();

		// Delete instrument from all axes
		$queryStr = "DELETE FROM `tblInstrumentEvals` WHERE `instrumentID` = " . $this->_instrumentID;
		$queryStr .= " AND `date`=".$db->qstr($this->_date);
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}

		$db->commitTransaction();
		return true;
	} /* }}} */
	
	function removeByDate($date) { /* {{{ */
		$db = $this->_dms->getDB();

		$db->startTransaction();

		// Delete instrument from all axes
		$queryStr = "DELETE FROM `tblInstrumentEvals` WHERE `date` = " . $db->qstr($date);
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}

		$db->commitTransaction();
		return true;
	} /* }}} */
	
	function getEvalDate() {
		$db = $this->_dms->getDB();

		$queryStr = "SELECT DISTINCT `date` FROM `tblInstrumentEvals` WHERE `instrumentID` = ".$this->_id." AND `date` = ".$db->qstr($this->_date)." ORDER BY `date`";
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$dates = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$dates[$i] = $instrument;
		}

		return $instruments;
		
	}
} /* }}} */
?>
