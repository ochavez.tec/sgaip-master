<?php
/**
 * Implementation of the instrument object in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent an instrument in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_InstrumentVar { /* {{{ */
	/**
	 * @var integer id of instrument
	 *
	 * @access protected
	 */
	var $_id;

	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_instrumentID;
	
	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_code;
	
	/**
	 * @var string login name of instrument
	 *
	 * @access protected
	 */
	var $_name;

	/**
	 * @var integer weight of instrument
	 *
	 * @access protected
	 */
	var $_weight;

	/**
	 * @var string comment of instrument
	 *
	 * @access protected
	 */
	var $_comment;

	/**
	 * @var object reference to the dms instance this instrument belongs to
	 *
	 * @access protected
	 */
	var $_dms;

/*
	const role_user = '0';
	const role_admin = '1';
	const role_guest = '2';
*/

	function __construct($id, $instrumentID, $code, $name, $weight, $comment) {
		$this->_id = $id;
		$this->_instrumentID = $instrumentID;
		$this->_code = $code;
		$this->_name = $name;
		$this->_weight = $weight;
		$this->_comment = $comment;
		$this->_dms = null;
	}

	/**
	 * Create an instance of an instrument object
	 *
	 * @param string|integer $id Id, login name, or email of user, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by [name|email]. If 'name' is passed, the method
	 * will check for the 4th paramater and also filter by email. If this
	 * parameter is left empty, the user will be search by its Id.
	 * @param string $email optional email address if searching for name
     * @return object instance of class SeedDMS_Core_User if user was found, null
     * if user was not found, false in case of error
	 */
	public static function getInstance($id, $dms, $by='', $name='') { /* {{{ */
		$db = $dms->getDB();

		switch($by) {
		case 'name':
			$queryStr = "SELECT * FROM `tblInstrumentVars` WHERE `id` = ".$db->qstr($id);
			if($name)
				$queryStr .= " AND `name`=".$db->qstr($name);
			break;
		case 'id':
			$queryStr = "SELECT * FROM `tblInstrumentVars` WHERE `id` = " . (int) $id;
			break;
		default:
			$queryStr = "SELECT * FROM `tblInstrumentVars` WHERE `code` = " . $db->qstr($id);
		}
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return null;

		$resArr = $resArr[0];

		$instrumentVar = new self($resArr["id"], $resArr["instrumentID"], $resArr["code"], $resArr["name"], $resArr["weight"], $resArr["comment"]);
		$instrumentVar->setDMS($dms);
		return $instrumentVar;
	} /* }}} */

	public static function getAllInstances($orderby, $dms) { /* {{{ */
		$db = $dms->getDB();

		if($orderby == 'name')
			$queryStr = "SELECT * FROM `tblInstrumentVars` ORDER BY `name`";
		else
			$queryStr = "SELECT * FROM `tblInstrumentVars` ORDER BY `code`";
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$instrumentVars = array();

		for ($i = 0; $i < count($resArr); $i++) {
			$instrumentVar = new self($resArr[$i]["id"], $resArr[$i]["instrumentID"], $resArr[$i]["code"], $resArr[$i]["name"], $resArr[$i]["weight"], $resArr[$i]["comment"]);
			$instrumentVar->setDMS($dms);
			$instrumentVars[$i] = $instrumentVar;
		}

		return $instrumentVars;
} /* }}} */

	function setDMS($dms) {
		$this->_dms = $dms;
	}

	function getID() { return $this->_id; }

	function getCode() { return $this->_code; }

	function setCode($newCode) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `code` = ".$db->qstr($newCode)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_code = $newCode;
		return true;
	} /* }}} */

	function getName() { return $this->_name; }

	function setName($newName) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `name` = ".$db->qstr($newName)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_name = $newName;
		return true;
	} /* }}} */

	function getWeight() { return $this->_weight; }

	function setWeight($newWeight) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `weight` =".$db->qstr($newWeight)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_weight = $newWeight;
		return true;
	} /* }}} */

	function getInstrumentID() { return $this->_instrumentID; }

	function setInstrumentID($newInstrumentID) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `instrumentID` =".$db->qstr($newInstrumentID)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_instrumentID = $newInstrumentID;
		return true;
	} /* }}} */

	function getComment() { return $this->_comment; }

	function setComment($newComment) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblInstrumentVars` SET `comment` =".$db->qstr($newComment)." WHERE `id` = " . $this->_id;
		$res = $db->getResult($queryStr);
		if (!$res)
			return false;

		$this->_comment = $newComment;
		return true;
	} /* }}} */

	/**
	 * Remove the user and also remove all its keywords, notifications, etc.
	 * Do not remove folders and documents of the user, but assign them
	 * to a different user.
	 *
	 * @param object $user the user doing the removal (needed for entry in
	 *        review and approve log).
	 * @param object $assignToUser the user who is new owner of folders and
	 *        documents which previously were owned by the delete user.
	 * @return boolean true on success or false in case of an error
	 */
	function remove($id=0) { /* {{{ */
		$db = $this->_dms->getDB();
		
		if($id == 0) {
			$id = $this->_id;
		}

		// Delete user itself
		$queryStr = "DELETE FROM `tblInstrumentVars` WHERE `id` = " . $id;
		if (!$db->getResult($queryStr)) {
			return false;
		}

		return true;
	} /* }}} */
} /* }}} */
?>
