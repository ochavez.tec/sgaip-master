<?php
/**
 * Implementation of the axe object in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe,
 *             2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */

/**
 * Class to represent a user group in the document management system
 *
 * @category   DMS
 * @package    SeedDMS_Core
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal, 2006-2008 Malcolm Cowe, 2010 Uwe Steinmann
 * @version    Release: 5.1.5
 */
class SeedDMS_Core_Axe { /* {{{ */
	/**
	 * The id of the axis
	 *
	 * @var integer
	 */
	protected $_id;

	/**
	 * The name of the axis
	 *
	 * @var string
	 */
	protected $_name;

	/**
	 * The comment of the axis
	 *
	 * @var string
	 */
	protected $_comment;

	/**
	 * The max score of the axis
	 *
	 * @var string
	 */
	protected $_maxScore;

	/**
	 * Back reference to DMS this axis belongs to
	 *
	 * @var object
	 */
	protected $_dms;

	function __construct($id, $name, $comment, $maxScore=100) { /* {{{ */
		$this->_id = $id;
		$this->_name = $name;
		$this->_comment = $comment;
		$this->_maxScore = $maxScore;
		$this->_dms = null;
	} /* }}} */

	/**
	 * Return an instance of a group object
	 *
	 * @param string|integer $id Id, name of group, depending
	 * on the 3rd parameter.
	 * @param object $dms instance of dms
	 * @param string $by search by group name if set to 'name'. 
	 * Search by Id of group if left empty.
	 * @return object instance of class SeedDMS_Core_Group if group was found, null
	 * if group was not found, false in case of error
	 */
	public static function getInstance($id, $dms, $by='') { /* {{{ */
		$db = $dms->getDB();

		switch($by) {
		case 'name':
			$queryStr = "SELECT * FROM `tblAxes` WHERE `name` = ".$db->qstr($id);
			break;
		default:
			$queryStr = "SELECT * FROM `tblAxes` WHERE `id` = " . (int) $id;
		}

		$resArr = $db->getResultArray($queryStr);
		if (is_bool($resArr) && $resArr == false)
			return false;
		else if (count($resArr) != 1) //wenn, dann wohl eher 0 als > 1 ;-)
			return null;

		$resArr = $resArr[0];

		$axe = new self($resArr["id"], $resArr["name"], $resArr["comment"], $resArr["maxScore"]);
		$axe->setDMS($dms);
		return $axe;
	} /* }}} */

	public static function getAllInstances($orderby, $dms) { /* {{{ */
		$db = $dms->getDB();

		switch($orderby) {
		default:
			$queryStr = "SELECT * FROM `tblAxes` ORDER BY `name`";
		}
		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false)
			return false;

		$axes = array();
		for ($i = 0; $i < count($resArr); $i++) {
			$axe = new self($resArr[$i]["id"], $resArr[$i]["name"], $resArr[$i]["comment"], $resArr[$i]["maxScore"]);
			$axe->setDMS($dms);
			$axes[$i] = $axe;
		}

		return $axes;
	} /* }}} */

	function setDMS($dms) { /* {{{ */
		$this->_dms = $dms;
	} /* }}} */

	function getID() { return $this->_id; }

	function getName() { return $this->_name; }

	function setName($newName) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblAxes` SET `name` = ".$db->qstr($newName)." WHERE `id` = " . $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		$this->_name = $newName;
		return true;
	} /* }}} */

	function getComment() { return $this->_comment; }

	function setComment($newComment) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblAxes` SET `comment` = ".$db->qstr($newComment)." WHERE `id` = " . $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		$this->_comment = $newComment;
		return true;
	} /* }}} */
	
	function getMaxScore() { return $this->_maxScore; }

	function setMaxScore($newMaxScore) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "UPDATE `tblAxes` SET `maxScore` = ".$db->qstr($newMaxScore)." WHERE `id` = " . $this->_id;
		if (!$db->getResult($queryStr))
			return false;

		$this->_maxScore = $newMaxScore;
		return true;
	} /* }}} */

	function getInstruments() { /* {{{ */
		$db = $this->_dms->getDB();

		if (!isset($this->_instruments)) {
			$queryStr = "SELECT `tblInstruments`.* FROM `tblInstruments` ".
				"LEFT JOIN `tblAxeMembers` ON `tblAxeMembers`.`instrumentID`=`tblInstruments`.`id` ".
				"WHERE `tblAxeMembers`.`axeID` = '". $this->_id ."'";
			$resArr = $db->getResultArray($queryStr);
			if (is_bool($resArr) && $resArr == false)
				return false;

			$this->_instruments = array();

			$classname = $this->_dms->getClassname('instrument');
			foreach ($resArr as $row) {
				$instrument = new $classname($row["id"], $row["name"], $row["weight"], $row["score"], $row["comment"]);
				array_push($this->_instruments, $instrument);
			}
		}
		return $this->_instruments;
	} /* }}} */

// 	function getManagers() { /* {{{ */
// 		$db = $this->_dms->getDB();
// 
// 		$queryStr = "SELECT `tblUsers`.* FROM `tblUsers` ".
// 			"LEFT JOIN `tblGroupMembers` ON `tblGroupMembers`.`userID`=`tblUsers`.`id` ".
// 			"WHERE `tblGroupMembers`.`groupID` = '". $this->_id ."' AND `tblGroupMembers`.`manager` = 1";
// 		$resArr = $db->getResultArray($queryStr);
// 		if (is_bool($resArr) && $resArr == false)
// 			return false;
// 
// 		$managers = array();
// 
// 		$classname = $this->_dms->getClassname('user');
// 		foreach ($resArr as $row) {
// 			$user = new $classname($row["id"], $row["login"], $row["pwd"], $row["fullName"], $row["email"], $row["language"], $row["theme"], $row["comment"], $row["role"], $row['hidden']);
// 			array_push($managers, $user);
// 		}
// 		return $managers;
// 	} /* }}} */

	function addInstrument($instrument) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "INSERT INTO `tblAxeMembers` (`axeID`, `instrumentID`) VALUES (".$this->_id.", ".$instrument->getID(). ")";
		$res = $db->getResult($queryStr);

		if (!$res) return false;

		unset($this->_users);
		return true;
	} /* }}} */

	function removeInstrument($instument) { /* {{{ */
		$db = $this->_dms->getDB();

		$queryStr = "DELETE FROM `tblAxeMembers` WHERE `axeID` = ".$this->_id." AND `instrumentID` = ".$instrument->getID();
		$res = $db->getResult($queryStr);

		if (!$res) return false;
		unset($this->_instruments);
		return true;
	} /* }}} */

	/**
	 * Check if instrument is member of axis
	 *
	 * @param object $user user to be checked
	 * @param boolean $asManager also check whether user is manager of group if
	 * set to true, otherwise does not care about manager status
	 * @return boolean true if user is member, otherwise false
	 */
	function isMember($instrument) { /* {{{ */
		if (isset($this->_instruments)) {
			foreach ($this->_instruments as $instrument)
				if ($instrument->getID() == $instrument->getID())
					return true;
			return false;
		}

		$db = $this->_dms->getDB();
		$queryStr = "SELECT * FROM `tblAxeMembers` WHERE `axeID` = " . $this->_id . " AND `instrumentID` = " . $instrument->getID();

		$resArr = $db->getResultArray($queryStr);

		if (is_bool($resArr) && $resArr == false) return false;
		if (count($resArr) != 1) return false;

		return true;
	} /* }}} */

	/**
	 * Toggle manager status of user
	 *
	 * @param object $user
	 * @return boolean true if operation was successful, otherwise false
	 */
// 	function toggleManager($user) { /* {{{ */
// 		$db = $this->_dms->getDB();
// 
// 		if (!$this->isMember($user)) return false;
// 
// 		if ($this->isMember($user,true)) $queryStr = "UPDATE `tblGroupMembers` SET `manager` = 0 WHERE `groupID` = ".$this->_id." AND `userID` = ".$user->getID();
// 		else $queryStr = "UPDATE `tblGroupMembers` SET `manager` = 1 WHERE `groupID` = ".$this->_id." AND `userID` = ".$user->getID();
// 
// 		if (!$db->getResult($queryStr)) return false;
// 		return true;
// 	} /* }}} */

	/**
	 * Delete instrument axis
	 * This function deletes the user group and all it references, like access
	 * control lists, notifications, as a child of other groups, etc.
	 *
	 * @param object $user the user doing the removal (needed for entry in
	 *        review log.
	 * @return boolean true on success or false in case of an error
	 */
	function remove($instrument) { /* {{{ */
		$db = $this->_dms->getDB();

		$db->startTransaction();

		$queryStr = "DELETE FROM `tblAxeMembers` WHERE `axeID` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
/*
		$queryStr = "DELETE FROM `tblACLs` WHERE `groupID` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		$queryStr = "DELETE FROM `tblNotify` WHERE `groupID` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		$queryStr = "DELETE FROM `tblMandatoryReviewers` WHERE `reviewerGroupID` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		$queryStr = "DELETE FROM `tblMandatoryApprovers` WHERE `approverGroupID` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
		$queryStr = "DELETE FROM `tblWorkflowTransitionGroups` WHERE `groupid` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}
*/
		$queryStr = "DELETE FROM `tblAxes` WHERE `id` = " . $this->_id;
		if (!$db->getResult($queryStr)) {
			$db->rollbackTransaction();
			return false;
		}

		// TODO : update document status if reviewer/approver has been deleted


/*
		$reviewStatus = $this->getReviewStatus();
		foreach ($reviewStatus as $r) {
			$queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`, `comment`, `date`, `userID`) ".
				"VALUES ('". $r["reviewID"] ."', '-2', 'Review group removed from process', ".$db->getCurrentDatetime().", '". $user->getID() ."')";
			$res=$db->getResult($queryStr);
			if(!$res) {
				$db->rollbackTransaction();
				return false;
			}
		}

		$approvalStatus = $this->getApprovalStatus();
		foreach ($approvalStatus as $a) {
			$queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`, `comment`, `date`, `userID`) ".
				"VALUES ('". $a["approveID"] ."', '-2', 'Approval group removed from process', ".$db->getCurrentDatetime().", '". $user->getID() ."')";
			$res=$db->getResult($queryStr);
			if(!$res) {
				$db->rollbackTransaction();
				return false;
			}
		}
*/

		$db->commitTransaction();

		return true;
	} /* }}} */

// 	function getReviewStatus($documentID=null, $version=null) { /* {{{ */
// 		$db = $this->_dms->getDB();
// 
// 		if (!$db->createTemporaryTable("ttreviewid")) {
// 			return false;
// 		}
// 
// 		$status = array();
// 
// 		// See if the group is assigned as a reviewer.
// 		$queryStr = "SELECT `tblDocumentReviewers`.*, `tblDocumentReviewLog`.`status`, ".
// 			"`tblDocumentReviewLog`.`comment`, `tblDocumentReviewLog`.`date`, ".
// 			"`tblDocumentReviewLog`.`userID` ".
// 			"FROM `tblDocumentReviewers` ".
// 			"LEFT JOIN `tblDocumentReviewLog` USING (`reviewID`) ".
// 			"LEFT JOIN `ttreviewid` on `ttreviewid`.`maxLogID` = `tblDocumentReviewLog`.`reviewLogID` ".
// 			"WHERE `ttreviewid`.`maxLogID`=`tblDocumentReviewLog`.`reviewLogID` ".
// 			($documentID==null ? "" : "AND `tblDocumentReviewers`.`documentID` = '". (int) $documentID ."' ").
// 			($version==null ? "" : "AND `tblDocumentReviewers`.`version` = '". (int) $version ."' ").
// 			"AND `tblDocumentReviewers`.`type`='1' ".
// 			"AND `tblDocumentReviewers`.`required`='". $this->_id ."' ";
// 		$resArr = $db->getResultArray($queryStr);
// 		if (is_bool($resArr) && $resArr == false)
// 			return false;
// 		if (count($resArr)>0) {
// 			foreach ($resArr as $res)
// 				$status[] = $res;
// 		}
// 		return $status;
// 	} /* }}} */

// 	function getApprovalStatus($documentID=null, $version=null) { /* {{{ */
// 		$db = $this->_dms->getDB();
// 
// 		if (!$db->createTemporaryTable("ttapproveid")) {
// 			return false;
// 		}
// 
// 		$status = array();
// 
// 		// See if the group is assigned as an approver.
// 		$queryStr = "SELECT `tblDocumentApprovers`.*, `tblDocumentApproveLog`.`status`, ".
// 			"`tblDocumentApproveLog`.`comment`, `tblDocumentApproveLog`.`date`, ".
// 			"`tblDocumentApproveLog`.`userID` ".
// 			"FROM `tblDocumentApprovers` ".
// 			"LEFT JOIN `tblDocumentApproveLog` USING (`approveID`) ".
// 			"LEFT JOIN `ttapproveid` on `ttapproveid`.`maxLogID` = `tblDocumentApproveLog`.`approveLogID` ".
// 			"WHERE `ttapproveid`.`maxLogID`=`tblDocumentApproveLog`.`approveLogID` ".
// 			($documentID==null ? "" : "AND `tblDocumentApprovers`.`documentID` = '". (int) $documentID ."' ").
// 			($version==null ? "" : "AND `tblDocumentApprovers`.`version` = '". (int) $version ."' ").
// 			"AND `tblDocumentApprovers`.`type`='1' ".
// 			"AND `tblDocumentApprovers`.`required`='". $this->_id ."' ";
// 		$resArr = $db->getResultArray($queryStr);
// 		if (is_bool($resArr) && $resArr == false)
// 			return false;
// 		if (count($resArr)>0) {
// 			foreach ($resArr as $res)
// 				$status[] = $res;
// 		}
// 
// 		return $status;
// 	} /* }}} */

	/**
	 * Get a list of documents with a workflow
	 *
	 * @param int $documentID optional document id for which to retrieve the
	 *        reviews
	 * @param int $version optional version of the document
	 * @return array list of all workflows
	 */
// 	function getWorkflowStatus($documentID=null, $version=null) { /* {{{ */
// 		$db = $this->_dms->getDB();
// 
// 		$queryStr = 'select distinct d.*, c.`groupid` from `tblWorkflowTransitions` a left join `tblWorkflows` b on a.`workflow`=b.`id` left join `tblWorkflowTransitionGroups` c on a.`id`=c.`transition` left join `tblWorkflowDocumentContent` d on b.`id`=d.`workflow` where d.`document` is not null and a.`state`=d.`state` and c.`groupid`='.$this->_id;
// 		if($documentID) {
// 			$queryStr .= ' AND d.`document`='.(int) $documentID;
// 			if($version)
// 				$queryStr .= ' AND d.`version`='.(int) $version;
// 		}
// 		$resArr = $db->getResultArray($queryStr);
// 		if (is_bool($resArr) && $resArr == false)
// 			return false;
// 		$result = array();
// 		if (count($resArr)>0) {
// 			foreach ($resArr as $res) {
// 				$result[] = $res;
// 			}
// 		}
// 		return $result;
// 	} /* }}} */

	/**
	 * Get all notifications of group
	 *
	 * @param integer $type type of item (T_DOCUMENT or T_FOLDER)
	 * @return array array of notifications
	 */
// 	function getNotifications($type=0) { /* {{{ */
// 		$db = $this->_dms->getDB();
// 		$queryStr = "SELECT `tblNotify`.* FROM `tblNotify` ".
// 		 "WHERE `tblNotify`.`groupID` = ". $this->_id;
// 		if($type) {
// 			$queryStr .= " AND `tblNotify`.`targetType` = ". (int) $type;
// 		}
// 
// 		$resArr = $db->getResultArray($queryStr);
// 		if (is_bool($resArr) && !$resArr)
// 			return false;
// 
// 		$notifications = array();
// 		foreach ($resArr as $row) {
// 			$not = new SeedDMS_Core_Notification($row["target"], $row["targetType"], $row["userID"], $row["groupID"]);
// 			$not->setDMS($this);
// 			array_push($notifications, $not);
// 		}
// 
// 		return $notifications;
// 	} /* }}} */

} /* }}} */
?>
