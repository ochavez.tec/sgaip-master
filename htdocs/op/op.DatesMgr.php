<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action = $_POST["action"];
else $action = null;

// Create new department --------------------------------------------------------
if ($action == "adddaterule") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('adddaterule')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$years = $_POST["years"];
	$days = $_POST["days"];

	/*if($check = $dms->findDateRuleByYear($_POST["years"])){
		$session->setSplashMsg(array('type'=>'warning', 'msg'=>getMLText('splash_add_rule_founded')));
	} else {
		$new_date_rule = $dms->addDateRule($years, $days);
		if (!$new_date_rule) {
			UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
		}
		
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_date_rule')));
	}*/

	$new_date_rule = $dms->addDateRule($years, $days);
	if (!$new_date_rule) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
		
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_date_rule')));

	add_log_line("&action=adddaterule&name=");
}

// Delete department -------------------------------------------------------------
else if ($action == "removedaterule") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removedaterule')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["id"]) || intval($_POST["id"]) < 1 ) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_date_rule_id"));
	}
	
	$date_rule = $dms->checkDateRule($_POST["id"]);
	if (!$date_rule) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_date_rule_id"));
	}

	if (!$date_rule = $dms->deleteDateRule($_POST["id"])) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_date_rule_deleted')));

	add_log_line("?dateruleid=".$_POST["id"]."&action=removedaterule");
}

else if ($action == "addextensiontime") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addextensiontime')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$days_before = $_POST["days_before"];
	$extension = $_POST["extension"];
	$result = $dms->addExtensionTime($days_before, $extension);
	
	if (!$result) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_extension_time_added')));
} 

else if ($action == "editextensiontime") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editextensiontime')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$id = $_POST["id"];
	$days_before = $_POST["days_before"];
	$extension = $_POST["extension"];
	$result = $dms->editExtensionTime($id, $days_before, $extension);
	
	if (!$result) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_extension_time_edited')));
} 



header("Location:../out/out.DatesMgr.php");

?>
