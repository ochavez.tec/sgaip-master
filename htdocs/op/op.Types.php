<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action=$_POST["action"];
else $action=NULL;

$instvars = isset($_POST["instvars"]) ? $_POST["instvars"] : null;

$instrumentVars = array();
if($instvars) {
	foreach($instvars as $instvarid) {
		$instrumentVars[] = $dms->getInstrumentVar($instvarid);
	}
}

$items = isset($_POST["items"]) ? $_POST["items"] : null;

$typeItems = array();
if($items) {
	foreach($items as $itemID) {
		$typeItems[] = $dms->getThematicItem($itemID);
	}
}

$categories = isset($_POST["categories"]) ? $_POST["categories"] : null;
$cats = array();
if($categories) {
	foreach($categories as $catid) {
		$cats[] = $dms->getDocumentCategory($catid);
	}
}

//Neue Kategorie anlegen -----------------------------------------------------------------------------
if ($action == "addtype") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addtype')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$name = trim($_POST["name"]);
	$codeMask = trim($_POST["codeMask"]);
	if($name == '') {
		UI::exitError(getMLText("admin_tools"),getMLText("type_noname"));
	}
	if (is_object($dms->getDocumentTypeByName($name))) {
		UI::exitError(getMLText("admin_tools"),getMLText("type_exists"));
	}
	$newType = $dms->addDocumentType($name, $codeMask);
	
	if (!$newType) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
	
	$newType->setInstrumentVars($instrumentVars);
	
	$newType->setCategories($cats);
	
	$typeid=$newType->getID();

	if (!empty($typeItems)) {
		$result = $dms->addThematicItemType($typeid, $typeItems);	
	}
}

//Kategorie l�schen ----------------------------------------------------------------------------------
else if ($action == "removetype") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removetype')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["typeid"]) || !is_numeric($_POST["typeid"]) || intval($_POST["typeid"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("unknown_document_type"));
	}
	$typeid = $_POST["typeid"];
	$type = $dms->getDocumentType($typeid);
	if (!is_object($type)) {
		UI::exitError(getMLText("admin_tools"),getMLText("unknown_document_type"));
	}

	$type->delInstrumentVars();
	$type->delCategories();
	if (!$type->remove()) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}

	$result = $dms->removeThematicItemType((int)$typeid);

	$typeid=-1;
}

//Kategorie bearbeiten: Neuer Name --------------------------------------------------------------------
else if ($action == "edittype") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('edittype')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["typeid"]) || !is_numeric($_POST["typeid"]) || intval($_POST["typeid"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("unknown_document_type"));
	}
	$typeid = $_POST["typeid"];
	$type = $dms->getDocumentType($typeid);
	if (!is_object($type)) {
		UI::exitError(getMLText("admin_tools"),getMLText("unknown_document_type"));
	}

	$name = $_POST["name"];
	if (!$type->setName($name)) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}

	$codeMask = $_POST["codeMask"];	
	if (!$type->setCodeMask($codeMask)) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
	
	$type->setInstrumentVars($instrumentVars);
	$type->setCategories($cats);
	
	if (!empty($typeItems)) {
		$result = $dms->addThematicItemType($typeid, $typeItems);	
	} else {
		$result = $dms->removeThematicItemType($typeid);
	}
}

else if ($action == "addinformationtyperequested") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addinformationtyperequested')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$name = trim($_POST["information_type_requested"]);

	$result = $dms->addInformationTypeRequested($name);
	
	if (!$result) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}

}

else if ($action == "addresolutiontype") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addresolutiontype')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$abbreviation = trim($_POST["resolution_type_abbreviation"]);
	$name = trim($_POST["resolution_type_name"]);
	$comment = trim($_POST["resolution_type_comment"]);

	if (isset($_POST['make_memo'])) {
		$make_memo = 1;
	} else {
		$make_memo = 0;
	}

	$result = $dms->addResolutionType($abbreviation, $name, $comment, $make_memo);
	
	if (!$result) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
	
}

else if ($action == "removeinformationtype") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeinformationtype')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["information_type_id"]) || !is_numeric($_POST["information_type_id"]) || intval($_POST["information_type_id"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("unknown_identifier"));
	}

	$id = $_POST["information_type_id"];
	$data = $dms->getInformationTypeRequestedById($id);
	if (!$data) {
		UI::exitError(getMLText("admin_tools"),getMLText("element_not_found"));
	}

	$deleted = $dms->delInformationTypeRequested($id);
	if (!$deleted) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}

}

else if ($action == "removeresolutiontype") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeresolutiontype')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["resolution_type_id"]) || !is_numeric($_POST["resolution_type_id"]) || intval($_POST["resolution_type_id"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("unknown_identifier"));
	}

	$id = $_POST["resolution_type_id"];
	$data = $dms->getResolutionTypeById($id);
	if (!$data) {
		UI::exitError(getMLText("admin_tools"),getMLText("element_not_found"));
	}

	$deleted = $dms->delResolutionType($id);
	if (!$deleted) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
}

else {
	UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));
}

header("Location:../out/out.Types.php?typeid=".$typeid);

?>
