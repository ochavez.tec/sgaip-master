<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.Authentication.php");

// composer require spatie/pdf-to-text
require dirname(dirname(dirname(__FILE__)))."/vendor/autoload.php";
use Spatie\PdfToText\Pdf;

$sensitive_words = $dms->getSensitiveWords();
if (empty($sensitive_words)) {
	echo false;
} else {
	//echo json_encode($_FILES['file-0'], true);
	if (isset($_FILES['file-0'])) {
		if ($_FILES['file-0']['type'] === 'application/pdf') {
			$pdf_text = Pdf::getText($_FILES['file-0']['tmp_name']);
			$i = 0;
			$words_finded = array();
			foreach ($sensitive_words as $word) {
				if (stripos($pdf_text, $word['name']) !== false) {
					$words_finded[$i] = $word['name'];			
					$i++;
				}
			}

			if (empty($words_finded)) {
				$data = null;
			} else {
				$data = implode(", ", $words_finded);
			}

			if ($data == null) {
				echo false;
			} else {
				echo $data;
			}
		} else {
			echo "file-not-allowed";
		}
	} else {
		echo false;
	}
}



