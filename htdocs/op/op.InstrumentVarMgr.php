 <?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action=$_POST["action"];
else $action=NULL;

// add new instrument var ---------------------------------------------------------
if ($action == "addinstrumentvar") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addinstrumentvar')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$name    		= $_POST["name"];
	$code    		= $_POST["code"];
	$weight  		= $_POST["weight"];
	$instrumentID   = $_POST["instruments"];
	$comment 		= $_POST["comment"];
	if (isset($_POST['is_manual'])) {
		$is_manual = $_POST['is_manual'];	
	} else {
		$is_manual = 0;
	}
	$evalJan 		= (isset($_POST["evalJan"]) ? $_POST["evalJan"] : 0);
	$evalFeb 		= (isset($_POST["evalFeb"]) ? $_POST["evalFeb"] : 0);
	$evalMar 		= (isset($_POST["evalMar"]) ? $_POST["evalMar"] : 0);
	$evalApr 		= (isset($_POST["evalApr"]) ? $_POST["evalApr"] : 0);
	$evalMay 		= (isset($_POST["evalMay"]) ? $_POST["evalMay"] : 0);
	$evalJun 		= (isset($_POST["evalJun"]) ? $_POST["evalJun"] : 0);
	$evalJul 		= (isset($_POST["evalJul"]) ? $_POST["evalJul"] : 0);
	$evalAug 		= (isset($_POST["evalAug"]) ? $_POST["evalAug"] : 0);
	$evalSep 		= (isset($_POST["evalSep"]) ? $_POST["evalSep"] : 0);
	$evalOct 		= (isset($_POST["evalOct"]) ? $_POST["evalOct"] : 0);
	$evalNov 		= (isset($_POST["evalNov"]) ? $_POST["evalNov"] : 0);
	$evalDec 		= (isset($_POST["evalDec"]) ? $_POST["evalDec"] : 0);

	if (is_object($dms->getInstrumentVarByCode($code))) {
		UI::exitError(getMLText("admin_tools"),getMLText("instrumentvar_exists"));
	}

	$newInstrumentVar = $dms->addInstrumentVar($instrumentID, $code, $name, $weight, $comment, $is_manual, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec);
	
	$instrumentvarid=$newInstrumentVar->getID();
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_instrumentvar')));

	add_log_line(".php&action=addinstrumentvar&login=".$user->getLogin());
}

// delete instrument var ------------------------------------------------------------
else if ($action == "removeinstrumentvar") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeinstrumentvar')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (isset($_POST["instrumentvarid"])) {
		$instrumentvarid = $_POST["instrumentvarid"];
	}

	if (!isset($instrumentvarid) || !is_numeric($instrumentvarid) || intval($instrumentvarid)<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrumentvar_id"));
	}

// 	/* This used to be a check if an admin is deleted. Now it checks if one
// 	 * wants to delete herself.
// 	 */
// 	if ($instrumentid==$instrument->getID()) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("cannot_delete_yourself"));
// 	}

	$instrumentVarToRemove = $dms->getInstrumentVar($instrumentvarid);
	if (!is_object($instrumentVarToRemove)) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrumentvar_id"));
	}

	if (!$instrumentVarToRemove->remove($instrumentvarid)) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
		
	add_log_line(".php&action=removeinstrumentvar&instrumentvarid=".$instrumentvarid);
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_instrumentvar')));
	$instrumentvarid=-1;
}

// modify instrument ------------------------------------------------------------
else if ($action == "editinstrumentvar") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editinstrumentvar')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["instrumentvarid"]) || !is_numeric($_POST["instrumentvarid"]) || intval($_POST["instrumentvarid"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrumentvar_id"));
	}
	
	$instrumentvarid=$_POST["instrumentvarid"];
	$editedInstrumentVar = $dms->getInstrumentVar($instrumentvarid);
	
	if (!is_object($editedInstrumentVar)) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrumentvar_id"));
	}

	$code    	  = $_POST["code"];
	$name    	  = $_POST["name"];
	$weight  	  = $_POST["weight"];
	$comment 	  = $_POST["comment"];
	if (isset($_POST['is_manual'])) {
		$is_manual = $_POST['is_manual'];	
	} else {
		$is_manual = 0;
	}
	$evalJan 	  = (isset($_POST["evalJan"]) ? $_POST["evalJan"] : 0);
	$evalFeb 	  = (isset($_POST["evalFeb"]) ? $_POST["evalFeb"] : 0);
	$evalMar 	  = (isset($_POST["evalMar"]) ? $_POST["evalMar"] : 0);
	$evalApr 	  = (isset($_POST["evalApr"]) ? $_POST["evalApr"] : 0);
	$evalMay 	  = (isset($_POST["evalMay"]) ? $_POST["evalMay"] : 0);
	$evalJun 	  = (isset($_POST["evalJun"]) ? $_POST["evalJun"] : 0);
	$evalJul 	  = (isset($_POST["evalJul"]) ? $_POST["evalJul"] : 0);
	$evalAug 	  = (isset($_POST["evalAug"]) ? $_POST["evalAug"] : 0);
	$evalSep 	  = (isset($_POST["evalSep"]) ? $_POST["evalSep"] : 0);
	$evalOct 	  = (isset($_POST["evalOct"]) ? $_POST["evalOct"] : 0);
	$evalNov 	  = (isset($_POST["evalNov"]) ? $_POST["evalNov"] : 0);
	$evalDec 	  = (isset($_POST["evalDec"]) ? $_POST["evalDec"] : 0);
	$instrumentID = $_POST["instruments"];

	if ($editedInstrumentVar->getCode() != $code)
		$editedInstrumentVar->setCode($code);
	if ($editedInstrumentVar->getName() != $name)
		$editedInstrumentVar->setName($name);
	if ($editedInstrumentVar->getWeight() != $weight)
		$editedInstrumentVar->setWeight($weight);
	if ($editedInstrumentVar->getComment() != $comment)
		$editedInstrumentVar->setComment($comment);
	if ($editedInstrumentVar->getIsManual() != $is_manual)
		$editedInstrumentVar->setIsManual($is_manual);
	if ($editedInstrumentVar->getInstrumentID() != $instrumentID[0])
		$editedInstrumentVar->setInstrumentID($instrumentID[0]);
	if ($editedInstrumentVar->getEvalJan() != $evalJan)
		$editedInstrumentVar->setEvalJan($evalJan);
	if ($editedInstrumentVar->getEvalFeb() != $evalFeb)
		$editedInstrumentVar->setEvalFeb($evalFeb);
	if ($editedInstrumentVar->getEvalMar() != $evalMar)
		$editedInstrumentVar->setEvalMar($evalMar);
	if ($editedInstrumentVar->getEvalApr() != $evalApr)
		$editedInstrumentVar->setEvalApr($evalApr);
	if ($editedInstrumentVar->getEvalMay() != $evalMay)
		$editedInstrumentVar->setEvalMay($evalMay);
	if ($editedInstrumentVar->getEvalJun() != $evalJun)
		$editedInstrumentVar->setEvalJun($evalJun);
	if ($editedInstrumentVar->getEvalJul() != $evalJul)
		$editedInstrumentVar->setEvalJul($evalJul);
	if ($editedInstrumentVar->getEvalAug() != $evalAug)
		$editedInstrumentVar->setEvalAug($evalAug);
	if ($editedInstrumentVar->getEvalSep() != $evalSep)
		$editedInstrumentVar->setEvalSep($evalSep);
	if ($editedInstrumentVar->getEvalOct() != $evalOct)
		$editedInstrumentVar->setEvalOct($evalOct);
	if ($editedInstrumentVar->getEvalNov() != $evalNov)
		$editedInstrumentVar->setEvalNov($evalNov);
	if ($editedInstrumentVar->getEvalDec() != $evalDec)
		$editedInstrumentVar->setEvalDec($evalDec);

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_instrumentvar')));
	add_log_line(".php&action=editinstrumentvar&instrumentvarid=".$instrumentvarid);
}
else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));

header("Location:../out/out.InstrumentVarMgr.php?instrumentvarid=".$instrumentvarid);

?>
