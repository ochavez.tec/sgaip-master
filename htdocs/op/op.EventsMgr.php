<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.ClassCalendar.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; } 



// delete------------------------------------------------------------
if ($action == "removeevent") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeevent', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$calendar = new SeedDMS_Calendar($dms->getDB(), $user);
	$event = $calendar->getEvent($_GET["event_id"]);

	if (($user->getID()!=$event["userID"])&&(!$user->isAdmin())){
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('access_denied'), 'data'=>''));
	}

	$result = $calendar->delEvent($_GET["event_id"]);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('event_removed')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}

}

else if ($action == "getevent") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('getevent', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$calendar = new SeedDMS_Calendar($dms->getDB(), $user);
	$event = $calendar->getEvent($_GET["event_id"]);

	if ($event) {

		$data = array(
			'id' => $event['id'],
			'is_holiday' => $event['is_holiday'], 
			'event_color' => $event['event_color'],
			'name' => $event['name'],
			'comment' => $event['comment'],
			'start' => date("d-m-Y", $event['start']),
			'stop' => date("d-m-Y", $event['stop']),
			'date' => date("d-m-Y", $event['date']),
			'userID' => $event['userID']
		);

		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('splash_success'), 'event' => $data));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}

}

else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));



?>
