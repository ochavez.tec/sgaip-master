<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.ClassCalendar.php");
include("../inc/inc.Authentication.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; }

if ($action == "editinitial") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editinitial')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$id = $_POST['initial_id'];
	$initials = $_POST['initials'];

	$result = $dms->editDocumentInitials($id, $initials);

	if ($result) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_success')));	
	} else {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('error_occured')));
	}
	

} else if ($action == "editresolutiontype") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editresolutiontype')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}
	
	$id = $_POST['resolution_type_id'];
	$name = $_POST['resolution_type_name'];
	$comment = $_POST['resolution_type_comment'];
	$abbreviation = $_POST['resolution_type_abbreviation'];
	$make_memo = (isset($_POST['make_memo'])) ? 1 : 0;

	$result = $dms->editResolutionType($id, $name, $comment, $abbreviation, $make_memo);

	if ($result) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_success')));	
	} else {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('error_occured')));
	}

} else if ($action == "editinformationtype") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editinformationtype')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$id = $_POST['information_type_id'];
	$name = $_POST['name'];

	$result = $dms->editInformationType($id, $name);

	if ($result) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_success')));	
	} else {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('error_occured')));
	}
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_success')));
}  




header("Location:../out/out.Types.php");

?>
