<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; } 

// add new instrument ---------------------------------------------------------
if ($action == "additem") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('additem')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}
	
	$thematicID = $_POST["thematic_id"];
	$name = $_POST['name'];
	$comment = $_POST['comment'];
	if ($_POST['qualification_type'] !== null) {
		$qualification_type = (int)$_POST['qualification_type'];
	} else {
		$qualification_type = 0;
	}

	if ($_POST['evaluation_type'] !== null) {
		$evaluation_type = $_POST['evaluation_type'];	
	} else {
		$evaluation_type = 0;
	}
	
	$evalJan = $_POST['evalJan'];
	$evalFeb = $_POST['evalFeb'];
	$evalMar = $_POST['evalMar'];
	$evalApr = $_POST['evalApr'];
	$evalMay = $_POST['evalMay'];
	$evalJun = $_POST['evalJun'];
	$evalJul = $_POST['evalJul'];
	$evalAug = $_POST['evalAug'];
	$evalSep = $_POST['evalSep'];
	$evalOct = $_POST['evalOct'];
	$evalNov = $_POST['evalNov'];
	$evalDec = $_POST['evalDec'];
	
	$new_item = $dms->addThematicItem($thematicID, $name, $comment, $qualification_type, $evaluation_type, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec);

	if ($new_item) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_item_added_success')));	
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}

	add_log_line(".php&action=additem&login=".$user->getLogin());

	header("Location:../out/out.Inspections.php");
}

// delete instrument ------------------------------------------------------------
else if ($action == "removeitem") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeitem')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$result = $dms->deleteItem($_POST['item_id']);

	if ($result) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_item_success')));
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}

	add_log_line(".php&action=removeitem&login=".$user->getLogin());

	header("Location:../out/out.Inspections.php");

}

// modify instrument ------------------------------------------------------------
else if ($action == "edititem") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('edititem')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$id = $_POST["item_id"];
	$name = $_POST['name'];
	$comment = $_POST['comment'];
	if (isset($_POST['qualification_type']) && $_POST['qualification_type'] !== null) {
		$qualification_type = (int)$_POST['qualification_type'];
	} else {
		$qualification_type = 0;
	}

	if (isset($_POST['evaluation_type']) && $_POST['evaluation_type'] !== null) {
		$evaluation_type = $_POST['evaluation_type'];	
	} else {
		$evaluation_type = 0;
	}
	
	$evalJan = (int)$_POST['evalJan'];
	$evalFeb = (int)$_POST['evalFeb'];
	$evalMar = (int)$_POST['evalMar'];
	$evalApr = (int)$_POST['evalApr'];
	$evalMay = (int)$_POST['evalMay'];
	$evalJun = (int)$_POST['evalJun'];
	$evalJul = (int)$_POST['evalJul'];
	$evalAug = (int)$_POST['evalAug'];
	$evalSep = (int)$_POST['evalSep'];
	$evalOct = (int)$_POST['evalOct'];
	$evalNov = (int)$_POST['evalNov'];
	$evalDec = (int)$_POST['evalDec'];
	
	$item = $dms->updateThematicItem($id, $name, $comment, $qualification_type, $evaluation_type, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec);

	if ($item) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_item_updated_success')));	
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}

	add_log_line(".php&action=edititem&login=".$user->getLogin());

	header("Location:../out/out.Inspections.php");
} 

else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));



?>
