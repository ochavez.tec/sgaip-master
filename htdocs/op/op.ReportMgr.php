<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

$UI = new UI();

$badWords = ['DELETE', 'DROP', 'ALTER', 'INSERT', 'CREATE', 'UPDATE'];

if (!$user->isAdmin()) {
	$UI->exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action = $_POST["action"];
else $action = null;

// Create new group --------------------------------------------------------
if ($action == "addreport") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addreport')) {
		$UI->exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$name = $_POST["name"];
	$comment = $_POST["comment"];
	$query = $_POST["query"];
	
	$dirtyMouth = false;
	foreach ($badWords as $bw) {
	    if (strpos($query, $bw) !== FALSE) {
		    $dirtyMouth = true;
	    }
	}
	
	if($dirtyMouth) {
		$UI->exitError(getMLText("admin_tools"),getMLText("bard_words_found"));
	}

	if (is_object($dms->getReportByName($name))) {
		$UI->exitError(getMLText("admin_tools"),getMLText("report_exists"));
	}

	$newReport = $dms->addReport($name, $comment, $query);
	if (!$newReport) {
		$UI->exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
	
	$reportid=$newReport->getID();
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_report')));

	add_log_line("&action=addreport&id=".$reportid);
}

// Delete group -------------------------------------------------------------
else if ($action == "removereport") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removereport')) {
		$UI->exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["reportid"]) || !is_numeric($_POST["reportid"]) || intval($_POST["reportid"])<1) {
		$UI->exitError(getMLText("admin_tools"),getMLText("invalid_report_id"));
	}
	
	$report = $dms->getReport($_POST["reportid"]);
	if (!is_object($report)) {
		$UI->exitError(getMLText("admin_tools"),getMLText("invalid_report_id"));
	}

	if (!$report->remove($report->getID())) {
		$UI->exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
	
	$reportid = '';

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_report')));

	add_log_line("?reportid=".$_POST["reportid"]."&action=removereport");
}

// Modifiy group ------------------------------------------------------------
else if ($action == "editreport") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editreport')) {
		$UI->exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["reportid"]) || !is_numeric($_POST["reportid"]) || intval($_POST["reportid"])<1) {
		$UI->exitError(getMLText("admin_tools"),getMLText("invalid_report_id"));
	}
	
	$reportid=$_POST["reportid"];
	$report = $dms->getReport($reportid);
	
	if (!is_object($report)) {
		$UI->exitError(getMLText("admin_tools"),getMLText("invalid_report_id"));
	}
	
	$name = $_POST["name"];
	$comment = $_POST["comment"];
	$query = $_POST["query"];
	
	$dirtyMouth = false;
	foreach ($badWords as $bw) {
	    if (strpos($query, $bw) !== FALSE) {
		    $dirtyMouth = true;
	    }
	}
	
	if($dirtyMouth) {
		$UI->exitError(getMLText("admin_tools"),getMLText("bard_words_found"));
	}

	if ($report->getName() != $name)
		$report->setName($name);
	if ($report->getComment() != $comment)
		$report->setComment($comment);
	if ($report->getQuery() != $query)
		$report->setQuery($query);
		
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_report')));

	add_log_line("?reportid=".$_POST["reportid"]."&action=editreport");
}

header("Location:../out/out.ReportMgr.php?reportid=".$reportid);

?>
