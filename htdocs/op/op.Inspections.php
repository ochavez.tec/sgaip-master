<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; } 

// add new instrument ---------------------------------------------------------
if ($action == "addinspection") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addinspection')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}
	$year = str_replace(' ','',$_POST["inspection_year"]);
	$start_date = str_replace(' ','',$_POST["inspection_start"]);
	$end_date = str_replace(' ','',$_POST["inspection_stop"]);
	
	$new_inspection = $dms->addInspection($year, $start_date, $end_date);

	if ($new_inspection) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_inspection_added')));	
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}

	add_log_line(".php&action=addinspection&login=".$user->getLogin());

	header("Location:../out/out.Inspections.php");
}

// delete instrument ------------------------------------------------------------
else if ($action == "removeinspection") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeinspection', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$result = $dms->deleteInspection($_GET['inspection_id']);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('inspection_removed')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}

}

// modify instrument ------------------------------------------------------------
else if ($action == "editinspection") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editinspection')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if(isset($_POST["inspection_id"])) {
		$id = $_POST["inspection_id"];
	}

	if (isset($_POST["inspection_year"])) {
		$year = $_POST["inspection_year"];
	}

	if(isset($_POST["inspection_start"])) {
		$start = $_POST["inspection_start"];
	}

	if(isset($_POST["inspection_stop"])) {
		$stop = $_POST["inspection_stop"];
	}

	$result = $dms->updateInspection($id, $year, $start, $stop);

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_inspection_success')));
	add_log_line(".php&action=addinspection&login=".$user->getLogin());
	header("Location:../out/out.Inspections.php?selected_inspection=".$id);
} 

else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));



?>
