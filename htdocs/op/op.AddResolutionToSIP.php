<?php
//    MyDMS. Document Management System
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2106 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

if (!isset($_POST["documentid"]) || !is_numeric($_POST["documentid"]) || intval($_POST["documentid"])<1) {
	UI::exitError(getMLText("document_title", array("documentname" => getMLText("invalid_doc_id"))),getMLText("invalid_doc_id"));
}

// Esta es la resolucion
$documentid = $_POST["documentid"];
$document = $dms->getDocument($documentid); 

if (!is_object($document)) {
	UI::exitError(getMLText("document_title", array("documentname" => getMLText("invalid_doc_id"))),getMLText("invalid_doc_id"));
}

$folder = $document->getFolder();
$folder_name = explode("-",$folder->getName());

$sip_initials = $dms->getDocumentInitialByType("SIP");
if ($sip_initials) {
    $initials = $sip_initials['initials'];
} else {
    $initials = "TPTRSS";
}

// Obtener la SIP respectiva
$correlative = $folder_name[1];
$year = $folder_name[2];
$year_two = substr($folder_name[2], -2);
$result = $dms->getDocumentByName("DOCSIP-".$correlative."-".$year);
if(is_bool($result) && !$result){
    $result_two = $dms->getDocumentByName($initials.$correlative.$year_two);        
    if(is_bool($result_two) && !$result_two){
        $result_three = $dms->getDocumentByName($correlative."/".$year);
        if(is_bool($result_three) && !$result_three){
            $document_sip = false;
        } else {
            $document_sip = $result_three;
        }
    } else {
        $document_sip = $result_two;    
    }
} else {
    $document_sip = $result;
}

if (!is_object($document_sip)) {
	UI::exitError(getMLText("document_title", array("documentname" => $document->getName())),getMLText("error_ocurred"));
}

// Obtener el archivo principal de la resolucion
$resolution_content = $document->getContentByVersion(1);
$efilename = $resolution_content->getOriginalFileName();

$userfiletmp = $dms->contentDir.$resolution_content->getPath();
$userfiletype = "application/pdf";
$fileType = ".pdf";

$name = substr($efilename, 0, -4);
$comment = getMLText("resolution_attached");

$res = $document_sip->addDocumentFile($name, $comment, $user, $userfiletmp, utf8_basename($efilename), $fileType, $userfiletype, 1, 0);

if (is_bool($res) && !$res) {
	UI::exitError(getMLText("folder_title", array("foldername" => $folder->getName())),getMLText("error_occured"));
}

add_log_line("?attached_resolution_to_documentid=".$document_sip->getID());

header("Location:../out/out.ViewDocument.php?documentid=".$document_sip->getID()."&currenttab=attachments");

?>