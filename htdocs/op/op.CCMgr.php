<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; } 

$documentid = (int)$_POST["documentid"];

// add new ---------------------------------------------------------
if ($action == "addccresponse") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('ccresponseform')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}
	
	
	$cc_response = $_POST["cc_response_text"];
	
	$response = $dms->addCCResponse($documentid, 85, $cc_response);

	if ($response) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_cc_response_added')));	
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}

}

// modify ------------------------------------------------------------
else if ($action == "editccresponse") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('ccresponseform')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$cc_response = $_POST["cc_response_text"];
	
	$response = $dms->updateCCResponse($documentid, 85, $cc_response);

	if ($response) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_cc_response_edited')));	
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}
} 

else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));

header("Location:../out/out.ViewDocument.php?documentid=".$documentid."&currenttab=docinfo");

?>
