<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action=$_POST["action"];
else $action=NULL;

// modify workflow ---------------------------------------------------------
if ($action == "editworkflowtransition") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editworkflowtransition', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}
	
	$workflow_id 			= (int)$_POST["workflow_id"];
	$transition_id 			= (int)$_POST["transition_id"];
	$state_id 				= (int)$_POST["state"];
	$nextstate_id 			= (int)$_POST["nextstate"];
	$transition_action_id 	= (int)$_POST["transition_action"];
	$users 					= $_POST["users"];
	$groups 				= $_POST["groups"];
	$file_change_allowed 	= (int)$_POST["file_change_allowed"];
	$user_allowed 			= (int)$_POST["user_allowed"];
	$maxtime 			 	= (int)$_POST["maxtime"];

	$workflow = $dms->getWorkflow($workflow_id);

	if (!is_object($workflow)) {	
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_workflow_id'), 'data'=>''));
	}

	$transition = $workflow->getTransition($transition_id);

	if($transition->getState()->getID() != $state_id){
		$newState = $dms->getWorkflowState($state_id);
		$transition->setState($newState);
	}

	if($transition->getNextState()->getID() != $nextstate_id){
		$newNextState = $dms->getWorkflowState($nextstate_id);
		$transition->setNextState($newNextState);
	}

	if($transition->getAction()->getID() != $transition_action_id){
		$newAction = $dms->getWorkflowAction($transition_action_id);
		$transition->setAction($newAction);
	}

	if ($users != "" || $groups != "") {
		
		if ($users != "" && $groups == "") {
			$theusers = array();
			foreach($users as $userid) {
				$theusers[] = $dms->getUser($userid);
			}

			$transition->delGroups();
			$transition->setUsers($theusers);

		} else if ($groups != "" && $users == "") {
			$thegroups = array();
			foreach($groups as $groupid) {
				$thegroups[] = $dms->getGroup($groupid);
			}
			
			$transition->delUsers();
			$transition->setGroups($thegroups);

		} else if ($users != "" && $groups != "") {
			$theusers = array();
			foreach($users as $userid) {
				$theusers[] = $dms->getUser($userid);
			}
			$transition->setUsers($theusers);

			$thegroups = array();
			foreach($groups as $groupid) {
				$thegroups[] = $dms->getGroup($groupid);
			}
			$transition->setGroups($thegroups);
		}
	
	}

	$transition->setFileChangeAllowed($file_change_allowed);

	$transition->setUserAllowed($user_allowed);

	if($transition->getMaxTime() != $maxtime){
		$transition->setmaxTime($maxtime);
	}

	if ($transusers = $transition->getUsers()) {
		$users_output = "";
		foreach($transusers as $transuser) {
			$u = $transuser->getUser();
			$users_output .= getMLText('user').": ".$u->getFullName();
			$users_output .= "<br />";
		}	
	} else {
		$users_output = "<span></span>";
	}
	
	if ($transgroups = $transition->getGroups()) {
		foreach($transgroups as $transgroup) {
			$g = $transgroup->getGroup();
			$groups_output .= getMLText('at_least_n_users_of_group', array("number_of_users" => $transgroup->getNumOfUsers(), "group" => $g->getName()));
			$groups_output .= "<br />";
		}
	} else {
		$groups_output = "<span></span>";
	}

	$response = array(
		"workflow_id" 			=> $workflow_id, 
		"transition_id" 		=> $transition_id, 
		"state" 				=> $transition->getState()->getName(), 
		"nextstate" 			=> $transition->getNextState()->getName(),
		"doc_status" 			=> $transition->getNextState()->getDocumentStatus(),
		"transition_action"		=> $transition->getAction()->getName(),
		"file_change_allowed" 	=> $transition->getFileChangeAllowed(),
		"user_allowed" 			=> $transition->getUserAllowed(),
		"maxtime" 				=> $transition->getMaxTime(),
		"users"					=> $users_output,
		"groups"				=> $groups_output,
	);

	//$response_encode = json_encode($response);

	add_log_line(".php&action=editworkflowtransition&workflowid=".$workflow_id);

	header('Content-Type: application/json');
	echo json_encode(array('success'=>true, 'message'=>getMLText('data_saved'), 'data'=>$response));

} else if ($action == "removeworkflowtransition") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeworkflowtransition', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	if (!isset($_POST["workflow"])) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_workflow_id'), 'data'=>''));
	}

	$workflow = $dms->getWorkflow((int)$_POST["workflow"]);
	if (!is_object($workflow)) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_workflow_id'), 'data'=>''));
	}

	if (!isset($_POST["transition"])) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_workflow_id'), 'data'=>''));
	}

	$transition = $workflow->getTransition((int)$_POST['transition']);
	if (!is_object($transition)) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_workflow_id'), 'data'=>''));
	}

	add_log_line(".php&action=removeworkflowtransition&workflowid=".$workflow->getID());

	if($workflow->removeTransition($transition)) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('transition_removed_success')));
	}
	
} else {
	header('Content-Type: application/json');
	echo json_encode(array('success'=>false, 'message'=>getMLText('unknown_command'), 'data'=>''));
}

?>
