<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2106 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.ClassController.php");

require dirname(dirname(dirname(__FILE__)))."/vendor/autoload.php";

/**
* Create and save a temporal pdf to use in the document's creation
* This pdf should will be replace with the final memo
*/
// Reference the Dompdf namespace
use Dompdf\Dompdf;

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
$controller = Controller::factory($tmp[1]);

if (!isset($_POST["folder_id"]) || !is_numeric($_POST["folder_id"]) || intval($_POST["folder_id"])<1) {
	UI::exitError(getMLText("folder_title", array("foldername" => getMLText("invalid_folder_id"))),getMLText("invalid_folder_id"));
}

$folder_id = $_POST["folder_id"];
$folder = $dms->getFolder($folder_id);

if (!is_object($folder)) {
	UI::exitError(getMLText("folder_title", array("foldername" => getMLText("invalid_folder_id"))),getMLText("invalid_folder_id"));
}

$document_id = (int)$_POST['documentid'];
$parent_document = $dms->getDocument($document_id);
if (!is_object($parent_document)) {
	UI::exitError(getMLText("folder_title", array("foldername" => getMLText("invalid_document_id"))),getMLText("invalid_document_id"));
}

//$parent_document->getCategory();

/* No empty data */
if ($_POST['information_type_requested'] == "-1" || $_POST['resolution_type'] == "-1" || $_POST['description'] == "") {
	UI::exitError(getMLText("folder_title", array("foldername" => getMLText("invalid_requeriment_values"))),getMLText("invalid_requeriment_values"));
}

$information_type = $_POST['information_type_requested'];

$group_asigned = (isset($_POST['group_asigned']) && $_POST['group_asigned'] != '-1') ? $_POST['group_asigned'] : "N/A"; // 164 = Colaboradores juridicos grupo


$description = $_POST['description'];
$resolution_id = $_POST['resolution_type'];


// Global on this process
$memo_name = "";

$resolution = $dms->getResolutionTypeRecordById($_POST['resolution_type']);
$resolution_name = $resolution['name'];
$resolution_abbr = $resolution['abbreviation'];

// GET SIP correlative
$sip_correlative = $dms->getDocumentCorrelative($document_id, "sip");

if($sip_correlative){
	$document_correlative = $sip_correlative['correlative'];
	$document_year = substr($sip_correlative['year'],-2);

	$sip_name = $document_correlative."".$document_year;
} else {
	$sip_name = "0000";
}


$make_resolution = (isset($_POST['make_resolution']) && $_POST['make_resolution'] != null ) ? true : false;


//////////////////////////////////////////// CREATE MEMO //////////////////////////////////////////
if ((int)$resolution['make_memo']) {

	$folderPathHTML = getFolderPathHTML($folder, true);

	if ($folder->getAccessMode($user) < M_READWRITE) {
		UI::exitError(getMLText("folder_title", array("foldername" => $folder->getName())),getMLText("access_denied_on_this_folder"));
	}

	/* The memo owner is the user making it */
	$owner = $user;

	/* The comment is just static text */
	$comment  = getMLText("new_requirement_created").": ".$information_type;
	$version_comment = trim($comment);

	/* Keywords */
	$keywords = "memo";
	
	/* Categories */
	$cats[] = $dms->getDocumentCategory(19); // 19 = Memo

	/* Types */
	$types = array();

	/* Attributes */
	$attributes = array();
	$attributes[81] = trim($information_type);
	$attributes[82] = trim($resolution_name);
	$attributes[83] = $group_asigned; 
	$attributes[84] = $description;

	$attributes_version = array();

	/* Version */
	$reqversion = 1;

	/* Final position */
	$sequence = 2;

	$expires = 0;
	$workflow = $dms->getWorkflow(59); // 59 = Memos	

	$docsource = 'upload';

	$dompdf = new Dompdf();
	// Specific html for temporal pdf
	$html = "<!DOCTYPE html><html><head><title></title></head><body>";
	$html .= "<div>";
	$html .= "<p><strong>".getMLText("information_type_requested").":</strong></p>";
	$html .= "<p>".$information_type."</p>";
	$html .= "<p><strong>".getMLText("resolution_type").":</strong></p>";
	$html .= "<p>".$resolution_name."</p>";
	$html .= "<p><strong>".getMLText("group_asigned").":</strong></p>";
	$html .= "<p>".$group_asigned."</p>";
	$html .= "<p><strong>".getMLText("requirement_description").":</strong></p>";
	$html .= "<p>".$description."</p>";
	$html .= "</div>";
	$html .= "</body></html>";

	$dompdf->loadHtml($html);
	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('letter', 'portrait');
	// Render the HTML as PDF
	$dompdf->render();
	$pdf = $dompdf->output();
	$temp_dir = dirname(dirname(dirname(__FILE__)))."/htdocs/temp/memos";
	if (!file_exists($temp_dir)) {
	    mkdir($temp_dir, 0765, true);
	}

	$date_now = preg_replace('/\s+/', '', date("Y-m-d H:i:s"));
	$file_name = "Requerimiento-".$date_now.".pdf";
	$name = "Requerimiento-".$date_now;

	file_put_contents($temp_dir."/".$file_name, $pdf);
	$userfiletmp = $temp_dir."/".$file_name;
	$userfiletype = "application/pdf";
	$fileType = ".pdf";
	$userfilename = $file_name;

	/* Create document */ 
	$res = $folder->addDocument($name, $comment, 0, $owner, $keywords, $cats, $userfiletmp, $userfilename, $fileType, $userfiletype, 0, array(), array(), 0, $comment, $attributes, array(), $workflow);	

	if($res) {

		$document = $res[0];

		$document_assignment = $dms->addDocumentUserAssignment($document->getID(), $user->getID());
		
		// Correlative for MEMO document
		$correlative_calculate = $dms->addDocumentCorrelative($document->getID(), "memo");
		$memo_correlative = $dms->getDocumentCorrelative($document->getID(), "memo");		
		
		// Generate memo name
		$new_memo_name = "UAIP/".$sip_name."/".$memo_correlative['correlative']."/".$memo_correlative['year']." (".$user->getCode().")";

		$document->setName($new_memo_name);
		
		// Send notification to users in the workflow
		if($notifier) {
			if($workflow && $settings->_enableNotificationWorkflow) {
				$subject = "request_workflow_action_email_subject";
				$message = "request_workflow_action_email_body";
				$params = array();
				$params['name'] = $document->getName();
				$params['version'] = $reqversion;
				$params['workflow'] = $workflow->getName();
				$params['folder_path'] = $folder->getFolderPathPlain();
				$params['current_state'] = $workflow->getInitState()->getName();
				$params['username'] = $user->getFullName();
				$params['sitename'] = $settings->_siteName;
				$params['http_root'] = $settings->_httpRoot;
				$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();

				$notified_users = array();
				$notified_groups = array();

				foreach($workflow->getNextTransitions($workflow->getInitState()) as $ntransition) {
					if ($ntransition->getUsers()) {						
						foreach($ntransition->getUsers() as $tuser) {
							// If users array aren't empty
							if (!empty($notified_users)) {

								// If user was already notified then is in the array, dont send other notification
								if (!in_array($tuser->getUser()->getID(), $notified_users)) {
									$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
									$notified_users[] = $tuser->getUser()->getID();
								}

							} else {
								// Send first notification
								$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
								$notified_users[] = $tuser->getUser()->getID();
							}
						}
					}

					if ($ntransition->getGroups()) {					
						foreach($ntransition->getGroups() as $tgroup) {
							if (!empty($notified_groups)) {

								if (!in_array($tgroup->getGroup()->getID(), $notified_groups)) {
									$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
									$notified_groups[] = $tgroup->getGroup()->getID();
								}
								
							} else {
								$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
								$notified_groups[] = $tgroup->getGroup()->getID();
							}
						}
					}
				}

			}
		}

		if (file_exists($userfiletmp)) {
			unlink($userfiletmp); // removes	
		}

		add_log_line("?name=".$document->getName()."&folderid=".$folder_id);
	}
	
}



/* ------------------------------------------------------------------------ */
$folderPathHTML = getFolderPathHTML($folder, true);

if ($folder->getAccessMode($user) < M_READWRITE) {
	UI::exitError(getMLText("folder_title", array("foldername" => $folder->getName())),getMLText("access_denied_on_this_folder"));
}
/* ------------------------------------------------------------------------ */

if ((int)$resolution['make_memo'] && $make_resolution) {
	$owner = $user;
	$comment  = getMLText("new_resolution_created").": ".$resolution_name;
	$version_comment = trim($comment);
	$keywords = "Resolución de ".$resolution_name;
	$categories[] = $dms->getDocumentCategory(10);
	$types = array();

	/* Attributes */
	$attributes = array();
	$attributes[82] = trim($resolution_name);
	$attributes[84] = $description;

	$attributes_version = array();
	$reqversion = 1;
	$sequence = 2;
	$expires = 0;

	$resolution_workflow = $dms->getWorkflow(60); // 60 = Resoluciones
			
	$docsource = 'upload';

	$dompdf = new Dompdf();

	$html_temp = $dms->getResolutionHtmlTemp($resolution_name, $description);

	$dompdf->loadHtml($html_temp);
	$dompdf->setPaper('letter', 'portrait');
	$dompdf->render();
	$pdf = $dompdf->output();
	$temp_dir = dirname(dirname(dirname(__FILE__)))."/htdocs/temp/resolutions";
	if (!file_exists($temp_dir)) {
		mkdir($temp_dir, 0765, true);
	}

	$date_now = preg_replace('/\s+/', '', date("Y-m-d H:i:s"));
	$file_name = "Resolucion-".$resolution_name."-".$date_now.".pdf";
	$name = "Resolucion-".$date_now;

	file_put_contents($temp_dir."/".$file_name, $pdf);
	$userfiletmp = $temp_dir."/".$file_name;
	$userfiletype = "application/pdf";
	$fileType = ".pdf";
	$userfilename = $file_name;

	/* Create document */ 
	$result = $folder->addDocument($name, $comment, 0, $owner, $keywords, $categories, $userfiletmp, $userfilename, $fileType, $userfiletype, 0, array(), array(), 0, $comment, $attributes, array(), $resolution_workflow);

	if($result) {
		$resolution_document = $result[0];

		$document_assignment = $dms->addDocumentUserAssignment($resolution_document->getID(), $user->getID());
		$correlative_calculate = $dms->addDocumentCorrelative($resolution_document->getID(), "resolution");
		$resolution_correlative = $dms->getDocumentCorrelative($resolution_document->getID(), "resolution");		

			
		$new_name = "UAIP/".$sip_name."/".strtoupper($resolution_abbr)."/".$resolution_correlative['correlative']."/".$resolution_correlative['year']." (".$user->getCode().")";

		$resolution_document->setName($new_name);
		// Send notification to users in the workflow
		if($notifier) {
				if($resolution_workflow && $settings->_enableNotificationWorkflow) {
					$subject = "request_workflow_action_email_subject";
					$message = "request_workflow_action_email_body";
					$params = array();
					$params['name'] = $resolution_document->getName();
					$params['version'] = $reqversion;
					$params['workflow'] = $resolution_workflow->getName();
					$params['folder_path'] = $folder->getFolderPathPlain();
					$params['current_state'] = $resolution_workflow->getInitState()->getName();
					$params['username'] = $user->getFullName();
					$params['sitename'] = $settings->_siteName;
					$params['http_root'] = $settings->_httpRoot;
					$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$resolution_document->getID();

					$notified_users = array();
					$notified_groups = array();

					foreach($resolution_workflow->getNextTransitions($resolution_workflow->getInitState()) as $ntransition) {
						if ($ntransition->getUsers()) {						
							foreach($ntransition->getUsers() as $tuser) {
								// If users array aren't empty
								if (!empty($notified_users)) {

									// If user was already notified then is in the array, dont send other notification
									if (!in_array($tuser->getUser()->getID(), $notified_users)) {
										$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
										$notified_users[] = $tuser->getUser()->getID();
									}

								} else {
									// Send first notification
									$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
									$notified_users[] = $tuser->getUser()->getID();
								}
							}
						}

						if ($ntransition->getGroups()) {					
							foreach($ntransition->getGroups() as $tgroup) {
								if (!empty($notified_groups)) {

									if (!in_array($tgroup->getGroup()->getID(), $notified_groups)) {
										$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
										$notified_groups[] = $tgroup->getGroup()->getID();
									}
									
								} else {
									$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
									$notified_groups[] = $tgroup->getGroup()->getID();
								}
							}
						}
					}
				}
		}

		if (file_exists($userfiletmp)) {
			unlink($userfiletmp); // removes	
		}

		add_log_line("?name=".$resolution_document->getName()."&folderid=".$folder_id);
	}

} else if (!((int)$resolution['make_memo'])) {
	$owner = $user;
	$comment  = getMLText("new_resolution_created").": ".$resolution_name;
	$version_comment = trim($comment);
	$keywords = "Resolución de ".$resolution_name;
	$categories[] = $dms->getDocumentCategory(10); // 10 = Resoluciones de la UAIP
	$types = array();

	/* Attributes */
	$attributes = array();
	$attributes[82] = trim($resolution_name);
	$attributes[84] = $description;

	$attributes_version = array();
	$reqversion = 1;
	$sequence = 2;
	$expires = 0;

	$resolution_workflow = $dms->getWorkflow(60); // 60 = Resoluciones
			
	$docsource = 'upload';

	$dompdf = new Dompdf();

	$html_temp = $dms->getResolutionHtmlTemp($resolution_name, $description);

	$dompdf->loadHtml($html_temp);
	$dompdf->setPaper('letter', 'portrait');
	$dompdf->render();
	$pdf = $dompdf->output();
	$temp_dir = dirname(dirname(dirname(__FILE__)))."/htdocs/temp/resolutions";
	if (!file_exists($temp_dir)) {
		mkdir($temp_dir, 0765, true);
	}

	$date_now = preg_replace('/\s+/', '', date("Y-m-d H:i:s"));
	$file_name = "Resolucion-".$resolution_name."-".$date_now.".pdf";
	$name = "Resolucion-".$date_now;

	file_put_contents($temp_dir."/".$file_name, $pdf);
	$userfiletmp = $temp_dir."/".$file_name;
	$userfiletype = "application/pdf";
	$fileType = ".pdf";
	$userfilename = $file_name;

	/* Create document */ 
	$result = $folder->addDocument($name, $comment, 0, $owner, $keywords, $categories, $userfiletmp, $userfilename, $fileType, $userfiletype, 0, array(), array(), 0, $comment, $attributes, array(), $resolution_workflow);

	if($result) {
		$resolution_document = $result[0];

		$document_assignment = $dms->addDocumentUserAssignment($resolution_document->getID(), $user->getID());
		$correlative_calculate = $dms->addDocumentCorrelative($resolution_document->getID(), "resolution");
		$resolution_correlative = $dms->getDocumentCorrelative($resolution_document->getID(), "resolution");		

			
		$new_name = "UAIP/".$sip_name."/".strtoupper($resolution_abbr)."/".$resolution_correlative['correlative']."/".$resolution_correlative['year']." (".$user->getCode().")";

		$resolution_document->setName($new_name);
		// Send notification to users in the workflow
		if($notifier) {
				if($resolution_workflow && $settings->_enableNotificationWorkflow) {
					$subject = "request_workflow_action_email_subject";
					$message = "request_workflow_action_email_body";
					$params = array();
					$params['name'] = $resolution_document->getName();
					$params['version'] = $reqversion;
					$params['workflow'] = $resolution_workflow->getName();
					$params['folder_path'] = $folder->getFolderPathPlain();
					$params['current_state'] = $resolution_workflow->getInitState()->getName();
					$params['username'] = $user->getFullName();
					$params['sitename'] = $settings->_siteName;
					$params['http_root'] = $settings->_httpRoot;
					$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$resolution_document->getID();

					$notified_users = array();
					$notified_groups = array();

					foreach($resolution_workflow->getNextTransitions($resolution_workflow->getInitState()) as $ntransition) {
						if ($ntransition->getUsers()) {						
							foreach($ntransition->getUsers() as $tuser) {
								// If users array aren't empty
								if (!empty($notified_users)) {

									// If user was already notified then is in the array, dont send other notification
									if (!in_array($tuser->getUser()->getID(), $notified_users)) {
										$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
										$notified_users[] = $tuser->getUser()->getID();
									}

								} else {
									// Send first notification
									$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
									$notified_users[] = $tuser->getUser()->getID();
								}
							}
						}

						if ($ntransition->getGroups()) {					
							foreach($ntransition->getGroups() as $tgroup) {
								if (!empty($notified_groups)) {

									if (!in_array($tgroup->getGroup()->getID(), $notified_groups)) {
										$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
										$notified_groups[] = $tgroup->getGroup()->getID();
									}
									
								} else {
									$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
									$notified_groups[] = $tgroup->getGroup()->getID();
								}
							}
						}
					}
				}
		}

		if (file_exists($userfiletmp)) {
			unlink($userfiletmp); // removes	
		}

		add_log_line("?name=".$resolution_document->getName()."&folderid=".$folder_id);
	}
}


/* ------------------------------------------------------------------------ */


header("Location:../out/out.ViewFolder.php?folderid=".$folder_id."&showtree=1");

?>
