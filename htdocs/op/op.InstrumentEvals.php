<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action=$_POST["action"];
else $action=NULL;

// add new instrument ---------------------------------------------------------
if ($action == "addinstrumenteval") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addinstrumenteval')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}
	
	if (isset($_POST["evaldateInput"])) {
		$seldate = $_POST["evaldateInput"];
	}

	if(isset($_POST["evalscore"])) {
		$scores    = $_POST["evalscore"];
	}

	foreach($scores as $scoreid => $scoreval) {
		$instObj = $dms->getInstrument($scoreid);
		$result = $instObj->getWeight() * $scoreval / 100;
		$newEval = $dms->addInstrumentEval($scoreid,$seldate,$scoreval,$result,'');
	}

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_instrumenteval')));

	add_log_line(".php&action=addinstrumenteval&login=".$login);
}

// delete instrument ------------------------------------------------------------
else if ($action == "removeinstrumenteval") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeinstrumenteval')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (isset($_POST["seldate"])) {
		$seldate = $_POST["seldate"];
	}

	if (!isset($seldate) || !is_numeric(strtotime(str_replace('/', '-', $seldate))) ) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_seldate"));
	}

	$evalsToRemove = new SeedDMS_Core_InstrumentEval(1,$seldate,0,0,'');
	
	$evalsToRemove = $evalsToRemove->getAllInstances($seldate, $dms);
	
	
	foreach($evalsToRemove as $evalToRemove) {
		if (!$evalToRemove->removeByDate($seldate)) {
			UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
		}
	}
		
	add_log_line(".php&action=removeinstrumenteval&evaldate=".$seldate);
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_instrumenteval')));
	$instrumentid=-1;
}

// modify instrument ------------------------------------------------------------
else if ($action == "editinstrumenteval") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editinstrumenteval')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (isset($_POST["evaldateInput"])) {
		$seldate = $_POST["evaldateInput"];
	}

	if(isset($_POST["evalscore"])) {
		$scores    = $_POST["evalscore"];
	}

	foreach($scores as $scoreid => $scoreval) {
		$instObj = $dms->getInstrument($scoreid);
		$result = $instObj->getWeight() * $scoreval / 100;
		$newEval = $dms->updateInstrumentEval($scoreid,$seldate,$scoreval,$result,'');
	}

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_instrumenteval')));

	add_log_line(".php&action=editinstrumenteval&login=".$login);
}
else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));

header("Location:../out/out.InstrumentEvals.php?seldate=".$seldate);

?>
