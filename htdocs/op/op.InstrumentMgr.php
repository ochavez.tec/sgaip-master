<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action=$_POST["action"];
else $action=NULL;

// add new instrument ---------------------------------------------------------
if ($action == "addinstrument") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addinstrument')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$name    = $_POST["name"];
	$weight  = $_POST["weight"];
	$score   = $_POST["score"];
	$comment = $_POST["comment"];

	if (is_object($dms->getInstrumentByName($name))) {
		UI::exitError(getMLText("admin_tools"),getMLText("instrument_exists"));
	}

	$newInstrument = $dms->addInstrument($name, $weight, $score, $comment);
	if ($newInstrument) {
		/* Set groups if set */
		if(isset($_POST["axes"]) && $_POST["axes"]) {
			foreach($_POST["axes"] as $axeid) {
				$axe = $dms->getAxe($axeid);
				$axe->addInstrument($newInstrument);
			}
		}
	}
	else UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
	
	$instrumentid=$newInstrument->getID();
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_instrument')));

	add_log_line(".php&action=addinstrument&login=".$login);
}

// delete instrument ------------------------------------------------------------
else if ($action == "removeinstrument") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removeinstrument')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (isset($_POST["instrumentid"])) {
		$instrumentid = $_POST["instrumentid"];
	}

	if (!isset($instrumentid) || !is_numeric($instrumentid) || intval($instrumentid)<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrument_id"));
	}

// 	/* This used to be a check if an admin is deleted. Now it checks if one
// 	 * wants to delete herself.
// 	 */
// 	if ($instrumentid==$instrument->getID()) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("cannot_delete_yourself"));
// 	}

	$instrumentToRemove = $dms->getInstrument($instrumentid);
	if (!is_object($instrumentToRemove)) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrument_id"));
	}

	if (!$instrumentToRemove->remove($instrumentid)) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
		
	add_log_line(".php&action=removeinstrument&instrumentid=".$instrumentid);
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_instrument')));
	$instrumentid=-1;
}

// modify instrument ------------------------------------------------------------
else if ($action == "editinstrument") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editinstrument')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["instrumentid"]) || !is_numeric($_POST["instrumentid"]) || intval($_POST["instrumentid"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrument_id"));
	}
	
	$instrumentid=$_POST["instrumentid"];
	$editedInstrument = $dms->getInstrument($instrumentid);
	
	if (!is_object($editedInstrument)) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrument_id"));
	}

	$name    = $_POST["name"];
	$weight  = $_POST["weight"];
	$score   = $_POST["score"];
	$comment = $_POST["comment"];

	if ($editedInstrument->getName() != $name)
		$editedInstrument->setName($name);
	if ($editedInstrument->getWeight() != $weight)
		$editedInstrument->setWeight($weight);
	if ($editedInstrument->getComment() != $comment)
		$editedInstrument->setComment($comment);
	
	/* Updates axes */
	if(isset($_POST["axes"]))
		$newaxes = $_POST["axes"];
	else
		$newaxes = array();
	$oldaxes = array();
	foreach($editedInstrument->getAxes() as $k)
		$oldaxes[] = $k->getID();

	$addaxes = array_diff($newaxes, $oldaxes);
	foreach($addaxes as $axeid) {
		$axe = $dms->getAxe($axeid);
		$axe->addInstrument($editedInstrument);
	}
	$delaxes = array_diff($oldaxes, $newaxes);
	foreach($delaxes as $axeid) {
		$axe = $dms->getAxe($axeid);
		$axe->removeInstrument($editedInstrument);
	}

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_instrument')));
	add_log_line(".php&action=editinstrument&instrumentid=".$instrumentid);
}
else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));

header("Location:../out/out.InstrumentMgr.php?instrumentid=".$instrumentid);

?>
