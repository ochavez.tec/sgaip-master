<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action=$_POST["action"];
else $action=NULL;

// add new instrument var ---------------------------------------------------------
if ($action == "addnotify") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addnotify')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$name    		= $_POST["name"];
	$code    		= $_POST["code"];
	$weight  		= $_POST["weight"];
	$instrumentID   = $_POST["instruments"];
	$comment 		= $_POST["comment"];

	if (is_object($dms->getInstrumentVarByCode($code))) {
		UI::exitError(getMLText("admin_tools"),getMLText("instrumentvar_exists"));
	}

	$newInstrumentVar = $dms->addInstrumentVar($instrumentID, $code, $name, $weight, $comment);
// 	if ($newInstrumentVar) {
// 		/* Set groups if set */
// 		if(isset($_POST["axes"]) && $_POST["axes"]) {
// 			foreach($_POST["axes"] as $axeid) {
// 				$axe = $dms->getAxe($axeid);
// 				$axe->addInstrument($newInstrument);
// 			}
// 		}
// 	}
// 	else UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
	
	$instrumentvarid=$newInstrumentVar->getID();
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_instrumentvar')));

	add_log_line(".php&action=addinstrumentvar&login=".$login);
}

// delete instrument var ------------------------------------------------------------
else if ($action == "removenotify") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removenotify')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (isset($_POST["id"])) {
		$id = $_POST["id"];
	}

	if (!isset($id) || !is_numeric($id) || intval($id)<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_notify_id"));
	}

// 	/* This used to be a check if an admin is deleted. Now it checks if one
// 	 * wants to delete herself.
// 	 */
// 	if ($instrumentid==$instrument->getID()) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("cannot_delete_yourself"));
// 	}

	$notifyToRemove = $dms->getNotificationSchedule($id);
	if (!is_object($notifyToRemove)) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_notify_id"));
	}

	if (!$notifyToRemove->remove($id)) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
		
	add_log_line(".php&action=removenotify&id=".$id);
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_notify')));
	$id=-1;
}

// modify instrument ------------------------------------------------------------
else if ($action == "editinstrumentvar") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editinstrumentvar')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["instrumentvarid"]) || !is_numeric($_POST["instrumentvarid"]) || intval($_POST["instrumentvarid"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrumentvar_id"));
	}
	
	$instrumentvarid=$_POST["instrumentvarid"];
	$editedInstrumentVar = $dms->getInstrumentVar($instrumentvarid);
	
	if (!is_object($editedInstrumentVar)) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrumentvar_id"));
	}

	$code    = $_POST["code"];
	$name    = $_POST["name"];
	$weight  = $_POST["weight"];
	$comment = $_POST["comment"];
	$instrumentID   = $_POST["instruments"];

	if ($editedInstrumentVar->getName() != $name)
		$editedInstrumentVar->setName($name);
	if ($editedInstrumentVar->getWeight() != $weight)
		$editedInstrumentVar->setWeight($weight);
	if ($editedInstrumentVar->getComment() != $comment)
		$editedInstrumentVar->setComment($comment);
	if ($editedInstrumentVar->getInstrumentID() != $instrumentID[0])
		$editedInstrumentVar->setInstrumentID($instrumentID[0]);
		

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_instrumentvar')));
	add_log_line(".php&action=editinstrumentvar&instrumentvarid=".$instrumentvarid);
}
else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));

header("Location:../out/out.NotifyMgr.php?id=".$id);

?>
