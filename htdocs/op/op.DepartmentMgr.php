<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) $action = $_POST["action"];
else $action = null;

// Create new department --------------------------------------------------------
if ($action == "adddep") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('adddep')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$name = $_POST["name"];
	$code = $_POST["code"];
	$comment = $_POST["comment"];

	if (is_object($dms->getDepartmentByName($name))) {
		UI::exitError(getMLText("admin_tools"),getMLText("dep_exists"));
	}

	$newDep = $dms->addDepartment($name, $comment, $code);
	if (!$newDep) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
	
	$depid=$newDep->getID();
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_dep')));

	add_log_line("&action=adddep&name=".$name);
}

// Delete department -------------------------------------------------------------
else if ($action == "removedep") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removedep')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["depid"]) || !is_numeric($_POST["depid"]) || intval($_POST["depid"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_dep_id"));
	}
	
	$dep = $dms->getDepartment($_POST["depid"]);
	if (!is_object($dep)) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_dep_id"));
	}

	if (!$dep->remove($group)) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));
	}
	
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_dep')));

	add_log_line("?depid=".$_POST["depid"]."&action=removedep");
}

// Modifiy department ------------------------------------------------------------
else if ($action == "editdep") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editdep')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if (!isset($_POST["depid"]) || !is_numeric($_POST["depid"]) || intval($_POST["depid"])<1) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_dep_id"));
	}
	
	$depid=$_POST["depid"];
	$dep = $dms->getDepartment($depid);
	
	if (!is_object($dep)) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_dep_id"));
	}
	
	$name = $_POST["name"];
	$code = $_POST["code"];
	$comment = $_POST["comment"];

	if ($dep->getName() != $name)
		$dep->setName($name);
	if ($dep->getCode() != $code)
		$dep->setCode($code);
	if ($dep->getComment() != $comment)
		$dep->setComment($comment);
		
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_dep')));

	add_log_line("?depid=".$_POST["depid"]."&action=editdep");
}

// Add group to department --------------------------------------------------------
// else if ($action == "addmember") {
// 
// 	/* Check if the form data comes from a trusted request */
// 	if(!checkFormKey('addmember')) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
// 	}
// 
// 	if (!isset($_POST["axeid"]) || !is_numeric($_POST["axeid"]) || intval($_POST["axeid"])<1) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_axe_id"));
// 	}
// 	
// 	$axeid=$_POST["axeid"];
// 	$axe = $dms->getAxe($axeid);
// 	
// 	if (!is_object($axe)) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_axe_id"));
// 	}
// 
// 	if (!isset($_POST["instrumentid"]) || !is_numeric($_POST["instrumentid"]) || intval($_POST["instrumentid"])<1) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrument_id"));
// 	}
// 	
// 	$newMember = $dms->getInstrument($_POST["instrumentid"]);
// 	if (!is_object($newMember)) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrument_id"));
// 	}
// 
// 	if (!$axe->isMember($newMember)){
// 		$axe->addInstrument($newMember);
// //		if (isset($_POST["manager"])) $axe->toggleManager($newMember);
// 	}
// 	
// 	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_add_axe_member')));
// 
// 	add_log_line("?axeid=".$axeid."&instrumentid=".$_POST["instrumentid"]."&action=addmember");
// }
// 
// // Remove user from axe --------------------------------------------------
// else if ($action == "rmmember") {
// 
// 	/* Check if the form data comes from a trusted request */
// 	if(!checkFormKey('rmmember')) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
// 	}
// 
// 	if (!isset($_POST["axeid"]) || !is_numeric($_POST["axeid"]) || intval($_POST["axeid"])<1) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_axe_id"));
// 	}
// 	
// 	$axeid=$_POST["axeid"];
// 	$axe = $dms->getAxe($axeid);
// 	
// 	if (!is_object($axe)) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_axe_id"));
// 	}
// 
// 	if (!isset($_POST["instrumentid"]) || !is_numeric($_POST["instrumentid"]) || intval($_POST["instrumentid"])<1) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrument_id"));
// 	}
// 	
// 	$oldMember = $dms->getInstrument($_POST["instrumentid"]);
// 	if (!is_object($oldMember)) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_instrument_id"));
// 	}
// 
// 	$axe->removeUser($oldMember);
// 	
// 	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_axe_member')));
// 
// 	add_log_line("axeid=".$axeid."&instrumentid=".$_POST["instrumentid"]."&action=rmmember");
// }

// toggle manager flag
// else if ($action == "tmanager") {
// 
// 	/* Check if the form data comes from a trusted request */
// 	if(!checkFormKey('tmanager')) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
// 	}
// 
// 	if (!isset($_POST["groupid"]) || !is_numeric($_POST["groupid"]) || intval($_POST["groupid"])<1) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_group_id"));
// 	}
// 	
// 	$groupid=$_POST["groupid"];
// 	$group = $dms->getGroup($groupid);
// 	
// 	if (!is_object($group)) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_group_id"));
// 	}
// 
// 	if (!isset($_POST["userid"]) || !is_numeric($_POST["userid"]) || intval($_POST["userid"])<1) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_user_id"));
// 	}
// 	
// 	$usertoedit = $dms->getUser($_POST["userid"]);
// 	if (!is_object($usertoedit)) {
// 		UI::exitError(getMLText("admin_tools"),getMLText("invalid_user_id"));
// 	}
// 	
// 	$group->toggleManager($usertoedit);
// 	
// 	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_toogle_group_manager')));
// 
// 	add_log_line("?groupid=".$groupid."&userid=".$_POST["userid"]."&action=tmanager");
// }

header("Location:../out/out.DepartmentMgr.php?depid=".$depid);

?>
