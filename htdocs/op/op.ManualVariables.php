<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; } 

// add new instrument ---------------------------------------------------------
if ($action == "addmanualvariables") {
	
	if(!checkFormKey('addmanualvariables', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$inspectionID = "";
	$thevars = $_GET['vars'];
	$period = $_GET['period'];
	$var = array();
	foreach ($thevars as $key => $var) {
		$var['axeID'] = $var[0];
		$var['instID'] = $var[1]; 
		$var['varID'] = $var[2];
		$var['accomplish'] = $var[3];
	}

	$result = $dms->addManualVariableEval($period, $var);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('data_saved')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}
	
}

if ($action == "addinspectionmanuals") {
	
	if(!checkFormKey('addinspectionmanuals', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$inspectionID = "";
	$theitems = $_GET['items'];
	$items = array();
	foreach ($theitems as $key => $item) {
		$items[$key]['itemID'] = $item[0]; 
		$items[$key]['value'] = $item[3];
		$inspectionID = $item[2]; 
	}

	/*header('Content-Type: application/json');
	echo json_encode(array('success'=>true, 'inspectionID'=>$inspectionID, 'items' => $items));*/

	$result = $dms->addInspectionManualItems($inspectionID, $items);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('data_saved')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}
	
} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
}



?>
