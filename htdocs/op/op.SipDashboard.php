<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

/*if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}*/

if (isset($_POST["action"])){ 
	$action=$_POST["action"];
} else { $action=NULL; }


if($action == "getusersip"){

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('getuserdocuments', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$result = $dms->getAllUserActiveSIP($_POST['user_id']);
	$html = '';
	if ($result) {
		foreach ($result as $doc_data) {			
	        $document = $dms->getDocument($doc_data['document_id']);
	        $assigned_user = $dms->getUser($doc_data['user_id']);
	        $html .= '<tr>';
	        $html .= '<td class="align-center">'.$document->getName().'</td>';
	        $html .= '<td class="align-center">'.date("d-m-Y", (int)$doc_data['start_date']).'</td>';
	        $html .= '<td class="align-center">'.date("d-m-Y", (int)$doc_data['end_date']).'</td>';
	        $html .= '<td class="align-center">
	            <a href="/out/out.ViewDocument.php?documentid='.$doc_data['document_id'].'" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
	          </td>';
	        $html .= '</tr>';
		}
	}

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'html' => $html));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('error_occured')));	
	}

} else if($action == "getusercc"){

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('getuserdocuments', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$result = $dms->getAllUserActiveCC($_POST['user_id']);
	$html = '';
	if ($result) {
		foreach ($result as $doc_data) {			
	        $document = $dms->getDocument($doc_data['document_id']);
	        $assigned_user = $dms->getUser($doc_data['user_id']);
	        $html .= '<tr>';
	        $html .= '<td class="align-center">'.$document->getName().'</td>';
	        $html .= '<td class="align-center">'.date("d-m-Y", (int)$doc_data['start_date']).'</td>';
	        $html .= '<td class="align-center">'.date("d-m-Y", (int)$doc_data['end_date']).'</td>';
	        $html .= '<td class="align-center">
	            <a href="/out/out.ViewDocument.php?documentid='.$doc_data['document_id'].'" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
	          </td>';
	        $html .= '</tr>';
		}
	}

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'html' => $html));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('error_occured')));	
	}


} else if($action == "getgroupmemo"){

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('getuserdocuments', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}	

	$result = $dms->getAllGroupActiveMemo($_POST['user_id']); // On this case user_id is group_id
	$html = '';
	if ($result) {
		foreach ($result as $doc_data) {			
	        $document = $dms->getDocument($doc_data['document_id']);
	        $assigned_group = $dms->getGroup($doc_data['group_id']);
	        $html .= '<tr>';
	        $html .= '<td class="align-center">'.$document->getName().'</td>';
	        $html .= '<td class="align-center">'.date("d-m-Y", (int)$doc_data['start_date']).'</td>';
	        $html .= '<td class="align-center">'.date("d-m-Y", (int)$doc_data['end_date']).'</td>';
	        $html .= '<td class="align-center">
	            <a href="/out/out.ViewDocument.php?documentid='.$doc_data['document_id'].'" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
	          </td>';
	        $html .= '</tr>';
		}
	}

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'html' => $html));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('error_occured')));	
	}

} else {
	$year = isset($_POST["year"]) ? $_POST["year"] : NULL;

	if ($year == NULL) {
		UI::exitError(getMLText("admin_tools"),getMLText("error_occured"));	
	}

	header("Location:../out/out.SipDashboard.php?year=".$year);
}



?>
