<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

require dirname(dirname(dirname(__FILE__)))."/vendor/autoload.php";

/* Check if the form data comes from a trusted request */
if(!checkFormKey('triggerworkflow')) {
	UI::exitError(getMLText("document_title", array("documentname" => getMLText("invalid_request_token"))),getMLText("invalid_request_token"));
}

if (!isset($_POST["documentid"]) || !is_numeric($_POST["documentid"]) || intval($_POST["documentid"])<1) {
	UI::exitError(getMLText("document_title", array("documentname" => getMLText("invalid_doc_id"))),getMLText("invalid_doc_id"));
}
$documentid = $_POST["documentid"];
$document = $dms->getDocument($documentid);

if (!is_object($document)) {
	UI::exitError(getMLText("document_title", array("documentname" => getMLText("invalid_doc_id"))),getMLText("invalid_doc_id"));
}

if (!isset($_POST["version"]) || !is_numeric($_POST["version"]) || intval($_POST["version"])<1) {
	UI::exitError(getMLText("document_title", array("documentname" => $document->getName())),getMLText("invalid_version"));
}

$version_num = $_POST["version"];
$version = $document->getContentByVersion($version_num);
if (!is_object($version)) {
	UI::exitError(getMLText("document_title", array("documentname" => $document->getName())),getMLText("invalid_version"));
}

$workflow = $version->getWorkflow();
if (!is_object($workflow)) {
	UI::exitError(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))),getMLText("document_has_no_workflow"));
}

$transition = $dms->getWorkflowTransition($_POST["transition"]);
if (!is_object($transition)) {
	UI::exitError(getMLText("document_title", array("documentname" => $document->getName())),getMLText("invalid_workflow_transition"));
}

if(isset($_POST['is_allowed']) && $_POST['is_allowed']){
	$is_allowed = true;
} else {
	$is_allowed = false;
}

if (!$is_allowed) {
	if(!$version->triggerWorkflowTransitionIsAllowed($user, $transition)) {
		UI::exitError(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))),getMLText("access_denied"));
	}	
}


$workflow = $transition->getWorkflow();

if(isset($GLOBALS['SEEDDMS_HOOKS']['triggerWorkflowTransition'])) {
	foreach($GLOBALS['SEEDDMS_HOOKS']['triggerWorkflowTransition'] as $hookObj) {
		if (method_exists($hookObj, 'preTriggerWorkflowTransition')) {
			$hookObj->preTriggerWorkflowTransition(null, array('version'=>$version, 'transition'=>$transition, 'comment'=>$_POST["comment"]));
		}
	}
}

$folder = $document->getFolder();

// GET SIP correlative
$sip_correlative = $dms->getDocumentCorrelative($documentid, "sip");
if($sip_correlative){
	$document_correlative = $sip_correlative['correlative'];
	$document_year = substr($sip_correlative['year'],-2);
	$sip_name = $document_correlative."".$document_year;
} else {
	$sip_name = "0000";
}


// GET CC CORRELATIVE
$cc_correlative = $dms->getDocumentCorrelative($documentid, "cc");
if($cc_correlative){
	$document_correlative = $cc_correlative['correlative'];
	$document_year = substr($cc_correlative['year'],-2);
	$cc_name = $document_correlative."".$document_year;
} else {
	$cc_name = "0000";
}


$redirect_to_root_folder = false;

use Dompdf\Dompdf;

// Is this a memo transition? If this is then process
if (isset($_POST['memo_remitir'])) {
	
	$group = $_POST['group_asigned'];

	$today = date("Y-m-d H:i:s");
	$unix_start_date = strtotime($today);
	

	if (isset($_POST['end_date']) && $_POST['end_date'] != "") {
		
		$temp_end_date = strtotime($_POST['end_date']);
		$date = new DateTime(date('Y-m-d H:i:s', $temp_end_date));
		$date->setTime(23,59,59);
		$end_date = $date->format('Y-m-d H:i:s');
		$unix_end_date = strtotime($end_date);

	} else {
		$limit_days = $_POST['limit_days'];

		$temp_end_date = strtotime("+".$limit_days." day", $unix_start_date);
		$date = new DateTime(date('Y-m-d H:i:s', $temp_end_date));
		$date->setTime(23,59,59);
		$end_date = $date->format('Y-m-d H:i:s');
		$unix_end_date = strtotime($end_date);
	}


	$document_date = $dms->addDocumentDate($documentid, $unix_start_date, $unix_end_date);

	$document_assignment = $dms->addDocumentGroupAssignment($documentid, $group);

	$document_memo = $dms->getDocumentMemoContent($documentid);

	if ($document_memo) {

		/**
		 * Create and save a temporal pdf to change the memo file
		 */
		$dompdf = new Dompdf();
		$dompdf->set_option('isRemoteEnabled', true); // For URL images

		//$html = $dms->getPDFHtmlStart();
		if ($doc_margins = $dms->getDocumentContentMargins($documentid)) {
			$html = $dms->getPDFHtmlStart($doc_margins['margin_left'], $doc_margins['margin_top'], $doc_margins['margin_right'], $doc_margins['margin_bottom']);
		} else {
			$html = $dms->getPDFHtmlStart();
		}
		//$html .= $document_memo['content'];
		$html .= preg_replace("/\xE2\x80\x8B/", "", $document_memo['content']); 
		$html .= $dms->getPDFHtmlEnd();

		$dompdf->loadHtml($html);
		$dompdf->setPaper('letter', 'portrait');
		$dompdf->render();
		$pdf = $dompdf->output();
		$temp_dir = dirname(dirname(dirname(__FILE__)))."/htdocs/temp/memos";

		if (!file_exists($temp_dir)) {
		    mkdir($temp_dir, 0765, true);
		}

		$date_now = preg_replace('/\s+/', '', date("Y-m-d H:i:s"));
		$file_name = "Requerimiento-".$date_now.".pdf";
		file_put_contents($temp_dir."/".$file_name, $pdf);
		$userfiletmp = $temp_dir."/".$file_name;
		$userfiletype = "application/pdf";
		$name = "Requerimiento-".$date_now;
		$fileType = ".pdf";
		$userfilename = $file_name;

		// Update only the document file
		$filesize = SeedDMS_Core_File::fileSize($userfiletmp);
		$checksum = SeedDMS_Core_File::checksum($userfiletmp);

		// Update Document Content For Principal File
		$update_document_principal_file = $dms->updateDocumentPrincipalFileInfo($filesize, $checksum, $version->getID());

		if (!$update_document_principal_file) {
			UI::exitError(getMLText("document_title", array("documentname" => getMLText("error"))),getMLText("error_occured"));
		}

		//Move and replace new file into document's folder
		$target_dir = $folder->_dms->contentDir.$documentid."/";
		$move_file = rename($userfiletmp, $target_dir.$version->getVersion().$fileType);

		$version->setComment("Documento remitido a la unidad responsable de dar respuesta");

		// Send notification to users in the workflow
		if($notifier) {
				$subject = "requirement_assigned_email_subject";
				$message = "requirement_assigned_email_body";
				$params = array();
				$params['name'] = $document->getName();
				$params['version'] = $version->getVersion();
				$params['workflow'] = $workflow->getName();
				$params['folder_path'] = $folder->getFolderPathPlain();
				$params['current_state'] = $transition->getNextState()->getName();
				$params['username'] = $user->getFullName();
				$params['limit_date'] = date("d-m-Y", $unix_end_date);
				$params['sitename'] = $settings->_siteName;
				$params['http_root'] = $settings->_httpRoot;
				$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();
					
				
				// For memo assigned groups only must be notified once
				$assigned_group = $dms->getGroup($group);
				$notifier->toGroup($user, $assigned_group, $subject, $message, $params);
								
		}

		if (file_exists($userfiletmp)) {
			unlink($userfiletmp); // removes	
		}

		add_log_line("?Document_memo_remited_and_updated&documentid=".$documentid);
		
	} else {

		// No any memo was created by workflow users
		if($notifier) {
			$subject = "requirement_assigned_email_subject";
			$message = "requirement_assigned_email_body";
			$params = array();
			$params['name'] = $document->getName();
			$params['version'] = $version->getVersion();
			$params['workflow'] = $workflow->getName();
			$params['folder_path'] = $folder->getFolderPathPlain();
			$params['current_state'] = $transition->getNextState()->getName();
			$params['username'] = $user->getFullName();
			$params['limit_date'] = date("d-m-Y", $unix_end_date);
			$params['sitename'] = $settings->_siteName;
			$params['http_root'] = $settings->_httpRoot;
			$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();
			
			// For memo assigned groups only must be notified once	
			$assigned_group = $dms->getGroup($group);
			$notifier->toGroup($user, $assigned_group, $subject, $message, $params);
						
		}
	}
}

if (isset($_POST['memo_finalizar'])) {
	if ($dms->checkMemoAssignment($documentid)) {
		$finish_memo = $dms->finishMemoAssignment($documentid);
	}

	// NOTIFICATE MEMO CREATOR/OWNER
	if($notifier) {
		$doc_owner = $document->getOwner();
		$subject = "memo_response_by_assigned_group";
		$message = "memo_response_email_body";
		$params = array();
		$params['name'] = $document->getName();
		$params['version'] = "1";
		$params['workflow'] = "Memos";
		$params['action'] = "Responder";
		$params['folder_path'] = $folder->getFolderPathPlain();
		$params['comment'] = $_POST["comment"];
		$params['previous_state'] = 'Requerimiento remitido a unidad';
		$params['current_state'] = 'Requerimiento respondido y publicado';
		$params['username'] = $user->getFullName();
		$params['sitename'] = $settings->_siteName;
		$params['http_root'] = $settings->_httpRoot;
		$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();

		$notifier->toIndividual($user, $doc_owner, $subject, $message, $params);
	}
}

//////////////////////////////////////// SIP ACTIONS ///////////////////////////////////////////////
if (isset($_POST['sip_asignar'])) {

	// Dates
	$date_rule = $dms->getDateRule($_POST['date_rule']);


	// La fecha de inicio debe ser:
	// La fecha de creacion del documento + dos dias + duracion de eventos = Fecha de inicio
	$document_created_at = $document->getDate();

	$document_dates = $dms->calculateDocumentSIPDates($document_created_at, $date_rule);

    // Set dates for document
    $result = $dms->addDocumentDate($documentid, strtotime($document_dates[0]), strtotime($document_dates[1]));

	// Add one more sip assigned for sip counter table
	$user_sip_assigned = $dms->getUser($_POST['user_asigned']);
	$assign_sip = $dms->addSIPAssignment($documentid, $user_sip_assigned->getID());

	// User workflow allowed
	$document_assignment = $dms->addDocumentUserAssignment($documentid, $user_sip_assigned->getID());


	$sip_initials = $dms->getDocumentInitialByType("SIP");
	if ($sip_initials) {
	    $initials = $sip_initials['initials'];
	} else {
	    $initials = "TPTRSS";
	}

	// Change document name
	$new_name = $initials.$sip_name;
	$document->setName($new_name);

}

if (isset($_POST['make_resolution'])) {

	// CREATE A RESOLUTION //
	$folderPathHTML = getFolderPathHTML($folder, true);
	$action_id = $_POST['action_id'];
	$action = $dms->getWorkflowAction($action_id);
	$action_resolution = $dms->getActionResolution($action->getID());

	$resolution_type = $dms->getResolutionTypeRecordById($action_resolution['resolution_id']);

	$owner = $user;
	$comment  = getMLText("new_resolution_created").": ".$resolution_type['name'];
	$version_comment = trim($comment);
	$keywords = getMLText('resolution_of')." ".$resolution_type['name'];
	$categories[] = $dms->getDocumentCategoryByName("Resoluciones de la UAIP");
	$types = array();

	$attributes = array();
	$attributes[82] = trim($resolution_type['name']);
	$attributes[84] = $_POST["comment"];
	$attributes_version = array();
	$reqversion = 1;
	$sequence = 2;
	$expires = 0;

	$resolution_workflow = null;
	$resolution_workflow = $dms->getWorkflow(60); // 60 = Resoluciones	
	$docsource = 'upload';

	$dompdf = new Dompdf();

	$html_temp = $dms->getResolutionHtmlTemp($resolution_type['name'], $_POST['comment']);

	$dompdf->loadHtml($html_temp);
	$dompdf->setPaper('letter', 'portrait');
	$dompdf->render();
	$pdf = $dompdf->output();
	$temp_dir = dirname(dirname(dirname(__FILE__)))."/htdocs/temp/resolutions";
	if (!file_exists($temp_dir)) {
		mkdir($temp_dir, 0765, true);
	}

	$date_now = preg_replace('/\s+/', '', date("Y-m-d H:i:s"));
	$file_name = "Resolucion-".$resolution_type['name']."-".$date_now.".pdf";
	$name = "Resolucion-".$date_now;

	file_put_contents($temp_dir."/".$file_name, $pdf);
	$userfiletmp = $temp_dir."/".$file_name;
	$userfiletype = "application/pdf";
	$fileType = ".pdf";
	$userfilename = $file_name;

	/* Create document */ 
	$result = $folder->addDocument($name, $comment, 0, $owner, $keywords, $categories, $userfiletmp, $userfilename, $fileType, $userfiletype, 0, array(), array(), 0, $comment, $attributes, array(), $resolution_workflow);

	if($result) {
		$resolution_document = $result[0];

		$document_assignment = $dms->addDocumentUserAssignment($resolution_document->getID(), $user->getID());
		$correlative_calculate = $dms->addDocumentCorrelative($resolution_document->getID(), "resolution");
		$resolution_correlative = $dms->getDocumentCorrelative($resolution_document->getID(), "resolution");		
	
		$new_name = "UAIP/".$sip_name."/".strtoupper($resolution_type['abbreviation'])."/".$resolution_correlative['correlative']."/".$resolution_correlative['year']." (".$user->getCode().")";

		$resolution_document->setName($new_name);
		
		if($notifier) {
			if($resolution_workflow) {
				$subject = "request_workflow_action_email_subject";
				$message = "request_workflow_action_email_body";
				$params = array();
				$params['name'] = $resolution_document->getName();
				$params['version'] = $reqversion;
				$params['workflow'] = $resolution_workflow->getName();
				$params['folder_path'] = $folder->getFolderPathPlain();
				$params['current_state'] = $resolution_workflow->getInitState()->getName();
				$params['username'] = $user->getFullName();
				$params['sitename'] = $settings->_siteName;
				$params['http_root'] = $settings->_httpRoot;
				$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$resolution_document->getID();

				$wres_notified_users = array();
				$wres_notified_groups = array();

				foreach($resolution_workflow->getNextTransitions($resolution_workflow->getInitState()) as $ntransition) {

						if ($ntransition->getUsers()) {
							
							foreach($ntransition->getUsers() as $tuser) {

								// If users array aren't empty
								if (!empty($wres_notified_users)) {

									// If user was already notified then is in the array, dont send other notification
									if (!in_array($tuser->getUser()->getID(), $wres_notified_users)) {
										$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
										$wres_notified_users[] = $tuser->getUser()->getID();
									}

								} else {
									// Send first notification
									$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
									$wres_notified_users[] = $tuser->getUser()->getID();
								}	

							}
						}

						if ($ntransition->getGroups()) {
							
							foreach($ntransition->getGroups() as $tgroup) {
								if (!empty($wres_notified_groups)) {

									if (!in_array($tgroup->getGroup()->getID(), $wres_notified_groups)) {
										$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
										$wres_notified_groups[] = $tgroup->getGroup()->getID();
									}
									
								} else {
									$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
									$wres_notified_groups[] = $tgroup->getGroup()->getID();
								}
							}

						}
					}
			}
		}

		if (file_exists($userfiletmp)) {
			unlink($userfiletmp); // removes	
		}

		add_log_line("?name=".$resolution_document->getName()."&folderid=".$folder->getID());
		
		$redirect_to_root_folder = true;
	}
}

if (isset($_POST['sip_reanudar_despues_de_prevenir'])) {

	// Continuar la fecha de la SIP
	// Esta acción se realiza al momento de admitir o inadmitir la SIP luego de prevenida
	$continue_sip = $dms->pauseDocumentDate($documentid, 0);
	$actual_date = date("Y-m-d H:i:s");
	$prevention_date = $_POST['fecha_de_prevencion'];

	$date_result = strtotime($actual_date) - strtotime($prevention_date);
	$days_between = round($date_result / (60*60*24));

	if ($days_between > 0) {
		$dresult = $dms->updateEndDate($documentid, $days_between);		
	}

}

if (isset($_POST['sip_aplicar_prorroga'])) { // Prorrogas
	$extension_rule = $dms->getExtensionTimeRule(); // Returns one integer = extension days

	$document_dates = $dms->getDocumentDate($documentid);

	$new_end_date = $dms->calculateExtensionTimeForSIP($document_dates, $extension_rule);

	$unix_end_date = strtotime($new_end_date);

	$result = $dms->updateDocumentEndDate($documentid, $unix_end_date);
}

if (isset($_POST['sip_admitir'])) {

	$doc_correlative = $dms->getDocumentCorrelative($documentid, "sip");

	if($doc_correlative){
		$document_correlative = $doc_correlative['correlative'];
		$document_year = $doc_correlative['year'];
		$the_new_name = $document_correlative."/".$document_year;
	} else {
		$the_new_name = "0000/0000";
	}

	$document->setName($the_new_name);	

}

if (isset($_POST['sip_finalizar'])) {
	if ($dms->checkSIPAssignment($documentid)) {
		$finish_sip = $dms->finishSIPAssignment($documentid);
	}	
}

if (isset($_POST['sip_pause'])) {
	// Pausar la fecha de la SIP
	$pause_sip = $dms->pauseDocumentDate($documentid, 1);
}

if (isset($_POST['sip_start'])) {
	// Pausar la fecha de la SIP
	$pause_sip = $dms->pauseDocumentDate($documentid, 0);
}

if (isset($_POST['save_information_types'])) {
	$information_types = $dms->getInformationTypesRequested();

	if ($information_types) {
		foreach ($information_types as $info) {
			$input_name = "info_".$info['id'];
			if (isset($_POST[$input_name]) && ((int)$_POST[$input_name] > 0)) {
			//if (isset($_POST[$input_name])) {
				$info_result = $dms->saveDocumentInfoType($documentid, $info['id'], $_POST[$input_name]);
			}
		}
	}
}


/////////////////////////////////////////// CC ACTIONS //////////////////////////////////////////


if (isset($_POST['cc_asignar'])) {
	$unix_created_at = $document->getDate();
	
	$temp_end_date = strtotime($_POST['end_date']);
	$date = new DateTime(date('Y-m-d H:i:s', $temp_end_date));
	$date->setTime(23,59,59);
	$end_date = $date->format('Y-m-d H:i:s');
	$unix_end_date = strtotime($end_date);

    // Set dates for document
    $result = $dms->addDocumentDate($documentid, $unix_created_at, $unix_end_date);

	// Plus one more cc assigned for cc counter table
	$user_cc_assigned = $dms->getUser($_POST['user_asigned']);
	$assign_cc = $dms->addCCAssignment($documentid, $user_cc_assigned->getID());

	// User workflow transitions allowed
	$document_assignment = $dms->addDocumentUserAssignment($documentid, $user_cc_assigned->getID());

	// Change document name
	$cc_initials = $dms->getDocumentInitialByType("CC");
	if ($cc_initials) {
	    $initials = $cc_initials['initials'];
	} else {
	    $initials = "CCPTRSS";
	}

	$new_name = $initials.$cc_name;
	$document->setName($new_name);

}

if (isset($_POST['cc_finalizar'])) {
	if ($dms->checkCCAssignment($documentid)) {
		$finish_cc = $dms->finishCCAssignment($documentid);	
	}	
}



///////////////////////////////// RESOLUTION ACTIONS /////////////////////////////////////////

if (isset($_POST['resolucion_publicar'])) {

	$document_resolution = $dms->getDocumentResolutionContent($documentid);

	if ($document_resolution) {

		/**
		 * Create and save a temporal pdf to change the principal file for resolution
		 */
		$dompdf = new Dompdf();
		$dompdf->set_option('isRemoteEnabled', true); // For URL images

		//$html = $dms->getPDFHtmlStart();
		//$html .= $document_resolution['content'];
		if ($doc_margins = $dms->getDocumentContentMargins($documentid)) {
			$html = $dms->getPDFHtmlStart($doc_margins['margin_left'], $doc_margins['margin_top'], $doc_margins['margin_right'], $doc_margins['margin_bottom']);
		} else {
			$html = $dms->getPDFHtmlStart();
		}

		$html .= preg_replace("/\xE2\x80\x8B/", "", $document_resolution['content']); 
		$html .= $dms->getPDFHtmlEnd();

		$dompdf->loadHtml($html);
		$dompdf->setPaper('letter', 'portrait');
		$dompdf->render();
		$pdf = $dompdf->output();
		$temp_dir = dirname(dirname(dirname(__FILE__)))."/htdocs/temp/resolutions";

		if (!file_exists($temp_dir)) {
		    mkdir($temp_dir, 0765, true);
		}

		$date_now = preg_replace('/\s+/', '', date("Y-m-d H:i:s"));
		$file_name = "Resolucion-".$date_now.".pdf";
		file_put_contents($temp_dir."/".$file_name, $pdf);
		$userfiletmp = $temp_dir."/".$file_name;
		$userfiletype = "application/pdf";
		$name = "Resolucion-".$date_now;
		$fileType = ".pdf";
		$userfilename = $file_name;

		// Update only the document file
		$filesize = SeedDMS_Core_File::fileSize($userfiletmp);
		$checksum = SeedDMS_Core_File::checksum($userfiletmp);

		// Update Document Content For Principal File
		$update_document_principal_file = $dms->updateDocumentPrincipalFileInfo($filesize, $checksum, $version->getID());

		if (!$update_document_principal_file) {
			UI::exitError(getMLText("document_title", array("documentname" => getMLText("error"))),getMLText("error_occured"));
		}

		//Move and replace new file into document's folder
		$target_dir = $folder->_dms->contentDir.$documentid."/";
		$move_file = rename($userfiletmp, $target_dir.$version->getVersion().$fileType);

		if (file_exists($userfiletmp)) {
			unlink($userfiletmp); // removes	
		}
	}


	// NOTIFICAR OFICIALES DE INFORMACION
	if($notifier) {
		$oficiales = $dms->getGroup(168); // 168 = Oficiales de informacion
		$subject = "resolution_published";
		$message = "resolution_published_email_body";
		$params = array();
		$params['name'] = $document->getName();
		$params['version'] = "1";
		$params['workflow'] = "Resoluciones";
		$params['action'] = "Publicar";
		$params['folder_path'] = $folder->getFolderPathPlain();
		$params['comment'] = $_POST["comment"];
		$params['previous_state'] = 'Resolución aprobada';
		$params['current_state'] = 'Resolución finalizada y publicada';
		$params['username'] = $user->getFullName();
		$params['sitename'] = $settings->_siteName;
		$params['http_root'] = $settings->_httpRoot;
		$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();

		$notifier->toGroup($user, $oficiales, $subject, $message, $params);
	}
}


// ---------------> DEFAULT BEHAVIOR FOR TRANSITIONS <------------ //
if($version->triggerWorkflowTransition($user, $transition, $_POST["comment"], $is_allowed)) {
	if ($notifier) {

		// NOTIFICATION FOR DOCUMENT'S SUSCRIBED
		$nl =	$document->getNotifyList();
		$folder = $document->getFolder();
		$subject = "transition_triggered_email_subject";
		$message = "transition_triggered_email_body";
		$params = array();
		$params['name'] = $document->getName();
		$params['version'] = $version->getVersion();
		$params['workflow'] = $workflow->getName();
		$params['action'] = $transition->getAction()->getName();
		$params['folder_path'] = $folder->getFolderPathPlain();
		$params['comment'] = $_POST["comment"];
		$params['previous_state'] = $transition->getState()->getName();
		$params['current_state'] = $transition->getNextState()->getName();
		$params['username'] = $user->getFullName();
		$params['sitename'] = $settings->_siteName;
		$params['http_root'] = $settings->_httpRoot;
		$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();

		$notifier->toList($user, $nl["users"], $subject, $message, $params);
		foreach ($nl["groups"] as $grp) {
			$notifier->toGroup($user, $grp, $subject, $message, $params);
		}


		// IF WORKFLOW IS MEMOS OR RESOLUTIONS SEND NOTIFICATION FOR OWNER
		/*if ($workflow->getID() == 59 || $workflow->getID() == 60) { // memos or resolutions
			$transition_next_state = $transition->getNextState()->getID();
			$next_state = $dms->getWorkflowState($transition_next_state);
			if($next_state->getDocumentStatus() != '2' || $next_state->getDocumentStatus() != '-1'){

				$folder = $document->getFolder();
				$doc_owner = $document->getOwner();
				$subject = "transition_triggered_email_subject";
				$message = "transition_triggered_email_body";
				$params = array();
				$params['name'] = $document->getName();
				$params['version'] = $version->getVersion();
				$params['workflow'] = $workflow->getName();
				$params['action'] = $transition->getAction()->getName();
				$params['folder_path'] = $folder->getFolderPathPlain();
				$params['comment'] = $_POST["comment"];
				$params['previous_state'] = $transition->getState()->getName();
				$params['current_state'] = $transition->getNextState()->getName();
				$params['username'] = $user->getFullName();
				$params['sitename'] = $settings->_siteName;
				$params['http_root'] = $settings->_httpRoot;
				$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();

				$notifier->toIndividual($user, $doc_owner, $subject, $message, $params);
			}
		}*/


		// NOTIFICATIONS FOR NEXT TRANSITIONS USERS
		if($settings->_enableNotificationWorkflow) {
			$subject = "request_workflow_action_email_subject";
			$message = "request_workflow_action_email_body";
			$params = array();
			$params['name'] = $document->getName();
			$params['version'] = $version->getVersion();
			$params['workflow'] = $workflow->getName();
			$params['folder_path'] = $folder->getFolderPathPlain();
			$params['current_state'] = $transition->getNextState()->getName();
			$params['username'] = $user->getFullName();
			$params['sitename'] = $settings->_siteName;
			$params['http_root'] = $settings->_httpRoot;
			$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();

			$notified_users = array();
			$notified_groups = array();

			foreach($workflow->getNextTransitions($transition->getNextState()) as $ntransition) {

				if ((int)$ntransition->getUserAllowed()){

					// NOTIFICATION FOR USER ASSIGNED IF EXISTS
					$temp_user_assigned = $dms->getDocumentUserAssignment($document->getID());
					if ($temp_user_assigned) {
						$user_assigned = $dms->getUser($temp_user_assigned['user_id']);

						$folder = $document->getFolder();
						$subject = "transition_triggered_email_subject";
						$message = "transition_triggered_email_body";
						$params = array();
						$params['name'] = $document->getName();
						$params['version'] = $version->getVersion();
						$params['workflow'] = $workflow->getName();
						$params['action'] = $transition->getAction()->getName();
						$params['folder_path'] = $folder->getFolderPathPlain();
						$params['comment'] = $_POST["comment"];
						$params['previous_state'] = $transition->getState()->getName();
						$params['current_state'] = $transition->getNextState()->getName();
						$params['username'] = $user->getFullName();
						$params['sitename'] = $settings->_siteName;
						$params['http_root'] = $settings->_httpRoot;
						$params['url'] = "http".((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],'off')!=0)) ? "s" : "")."://".$_SERVER['HTTP_HOST'].$settings->_httpRoot."out/out.ViewDocument.php?documentid=".$document->getID();

						//$notifier->toIndividual($user, $user_assigned, $subject, $message, $params);

						// If users array aren't empty
						if (!empty($notified_users)) {

							// If user was already notified then is in the array, dont send other notification
							if (!in_array($user_assigned->getID(), $notified_users)) {
								$notifier->toIndividual($user, $user_assigned, $subject, $message, $params);
								$notified_users[] = $user_assigned->getID();
							}

						} else {
							// Send notification
							$notifier->toIndividual($user, $user_assigned, $subject, $message, $params);
							$notified_users[] = $user_assigned->getID();
						}
					}

				} else {

					foreach($ntransition->getUsers() as $tuser) {

						// If users array aren't empty
						if (!empty($notified_users)) {

							// If user was already notified then is in the array, dont send other notification
							if (!in_array($tuser->getUser()->getID(), $notified_users)) {
								$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
								$notified_users[] = $tuser->getUser()->getID();
							}

						} else {
							// Send first notification
							$notifier->toIndividual($user, $tuser->getUser(), $subject, $message, $params);
							$notified_users[] = $tuser->getUser()->getID();
						}

						

					}

					foreach($ntransition->getGroups() as $tgroup) {
						if (!empty($notified_groups)) {

							if (!in_array($tgroup->getGroup()->getID(), $notified_groups)) {
								$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
								$notified_groups[] = $tgroup->getGroup()->getID();
							}
							
						} else {
							$notifier->toGroup($user, $tgroup->getGroup(), $subject, $message, $params);
							$notified_groups[] = $tgroup->getGroup()->getID();
						}
					}

				}

			}
		}
	}

	if(isset($GLOBALS['SEEDDMS_HOOKS']['triggerWorkflowTransition'])) {
		foreach($GLOBALS['SEEDDMS_HOOKS']['triggerWorkflowTransition'] as $hookObj) {
			if (method_exists($hookObj, 'postTriggerWorkflowTransition')) {
				$hookObj->postTriggerWorkflowTransition(null, array('version'=>$version, 'transition'=>$transition, 'comment'=>$_POST["comment"]));
			}
		}
	}
}

add_log_line("?documentid=".$documentid."&version".$version_num);


if ($redirect_to_root_folder) {
	header("Location:../out/out.ViewFolder.php?folderid=".$folder->getID()."&showtree=1");
} else {
	header("Location:../out/out.ViewDocument.php?documentid=".$documentid."&currenttab=docinfo");
}


?>
