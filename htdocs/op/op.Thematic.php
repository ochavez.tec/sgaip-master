<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; } 

// add new instrument ---------------------------------------------------------
if ($action == "addthematic") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addthematic')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}
	
	$name = $_POST["name"];
	$comment = $_POST["comment"];
	
	$new_thematic = $dms->addThematic($name, $comment);

	if ($new_thematic) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_thematic_added')));	
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}

	add_log_line(".php&action=addthematicn&login=".$user->getLogin());

	header("Location:../out/out.Inspections.php");
}

// delete instrument ------------------------------------------------------------
else if ($action == "removethematic") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removethematic')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$result = $dms->deleteThematic($_POST['thematic_id']);

	if ($result) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_rm_thematic_success')));
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}

	add_log_line(".php&action=removethematic&login=".$user->getLogin());

	header("Location:../out/out.Inspections.php");

}

// modify instrument ------------------------------------------------------------
else if ($action == "editthematic") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('editthematic')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$id = (int)$_POST['thematic_id'];
	$name = $_POST['name'];
	$comment = $_POST['comment'];

	$result = $dms->updateThematic($id, $name, $comment);

	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_thematic_success')));

	add_log_line(".php&action=editthematic&login=".$user->getLogin());

	header("Location:../out/out.Inspections.php");
} 

else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));



?>
