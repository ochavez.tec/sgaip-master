<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

if (isset($_POST["action"])) {
	$action=$_POST["action"];

} else if (isset($_GET["action"])) {
	$action=$_GET["action"];
} else { 
	$action=NULL; 
}

if (isset($_POST['scheduleid'])) {
		$scheduleid = "?scheduleid=".$_POST['scheduleid'];
} else {
		$scheduleid = '';
}

if ($action == "addvariablescheduling") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addvariablescheduling')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$selected_axe	= $_POST["selector_axe"];
	$selected_inst	= $_POST["selector_instrument"];
	$selected_var	= $_POST["selector_variable"];
	$selected_group	= $_POST["selector_group"];
	$selected_type	= $_POST["selector_type"];
	$year 			= $_POST["period"];

	$evalJan 		= (isset($_POST["evalJan"]) ? $_POST["evalJan"] : 0);
	$evalFeb 		= (isset($_POST["evalFeb"]) ? $_POST["evalFeb"] : 0);
	$evalMar 		= (isset($_POST["evalMar"]) ? $_POST["evalMar"] : 0);
	$evalApr 		= (isset($_POST["evalApr"]) ? $_POST["evalApr"] : 0);
	$evalMay 		= (isset($_POST["evalMay"]) ? $_POST["evalMay"] : 0);
	$evalJun 		= (isset($_POST["evalJun"]) ? $_POST["evalJun"] : 0);
	$evalJul 		= (isset($_POST["evalJul"]) ? $_POST["evalJul"] : 0);
	$evalAug 		= (isset($_POST["evalAug"]) ? $_POST["evalAug"] : 0);
	$evalSep 		= (isset($_POST["evalSep"]) ? $_POST["evalSep"] : 0);
	$evalOct 		= (isset($_POST["evalOct"]) ? $_POST["evalOct"] : 0);
	$evalNov 		= (isset($_POST["evalNov"]) ? $_POST["evalNov"] : 0);
	$evalDec 		= (isset($_POST["evalDec"]) ? $_POST["evalDec"] : 0);

	$schedule_id = $dms->addVariableScheduling($selected_axe, $selected_inst, $selected_var, $selected_group, $selected_type, $year, $evalJan, $evalFeb, $evalMar, $evalApr, $evalMay, $evalJun, $evalJul, $evalAug, $evalSep, $evalOct, $evalNov, $evalDec);

	if ($schedule_id && !is_bool($schedule_id)) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('data_saved')));
		$link = "?scheduleid=".$schedule_id;

	} else if ($schedule_id && is_bool($schedule_id)) {
		$founded_schedule = $dms->getSpecificInstrumentVarSchedule($selected_axe, $selected_inst, $selected_var, $selected_group, $selected_type, $year);
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('coincidence_founded')));
		$link = "?scheduleid=".$founded_schedule[0]['id'];
		
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('error_occured')));
		$link = "";
	}

	add_log_line(".php&action=addvariablescheduling&login=".$user->getLogin());
	header("Location:../out/out.AddVariableScheduling.php".$link);

} else if ($action == "removevariableschedule") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removevariableschedule', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$result = $dms->deleteInstrumentVariableSchedule($_GET['schedule_id']);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('schedule_removed')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}
	
} else {
	UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));
}

?>
