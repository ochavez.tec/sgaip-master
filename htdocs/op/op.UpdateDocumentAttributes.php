<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; } 

// delete instrument ------------------------------------------------------------
if ($action == "updateversiondate") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('updateversiondate', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token')));
	}

	$status_log_id = $_GET['statuslogid'];
	$date = $_GET['date'];

	$timestamp = strtotime($date);
	$date_publish_version = date('Y-m-d H:i:s', $timestamp);

	$result = $dms->updateStatusLogDate($status_log_id, $date_publish_version);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('data_saved'), 'date'=>$date));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}

}


else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));



?>
