<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

/* Check if the form data comes from a trusted request */
if(!checkFormKey('addmessage')) {
	UI::exitError(getMLText("message_title"),getMLText("invalid_request_token"));
}

$users = array();
if (isset($_POST["users"]) && is_array($_POST["users"])) {
	if($_POST["users"][0] == 'all') {
		$users = 'all';
	} else {
		foreach($_POST["users"] as $userid) {
			$users[] = $dms->getUser($userid);
		}
	}
}

$groups = array();
if (isset($_POST["groups"]) && is_array($_POST["groups"])) {
	foreach($_POST["groups"] as $groupid) {
		$groups[] = $dms->getGroup($groupid);
	}
}

if(!$users && !$groups) {
	UI::exitError(getMLText("message_title"),getMLText("missing_message_user_group"));
}

$dateSchedule='';
if (isset($_POST["dateSchedule"])) {
	$dateSchedule = $_POST["dateSchedule"];
} //else UI::exitError(getMLText("message_title"),getMLText("invalid_date_schedule"));

if($message = $dms->addNotifySchedule($user->getID(), $dateSchedule, $users, $groups, $_POST["subject"], $_POST["message"])) {
}

add_log_line("?notifyschedule=".$message->getID());

header("Location:../out/out.NotifyMgr.php?id=".$message->getID());
