<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005  Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Utils.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");
include("../inc/inc.ClassPasswordStrength.php");

/*if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}*/

require dirname(dirname(dirname(__FILE__)))."/vendor/autoload.php";
use Dompdf\Dompdf;

if (isset($_POST["action"])) {
	$action=$_POST["action"];
} else if (isset($_GET["action"])){
	$action=$_GET["action"];
} else { $action=NULL; } 

// add new instrument ---------------------------------------------------------
if ($action == "addtemplate") {
	
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('addtemplate')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	$name = $_POST['name'];
	$content = "<p>Design your own template here!</p>";

	$new_template = $dms->addTemplate($name, $content);

	if ($new_template) {
		$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_template_added')));	
	} else {
		$session->setSplashMsg(array('type'=>'error', 'msg'=>getMLText('splash_error')));
	}

	add_log_line(".php&action=addtemplate&login=".$user->getLogin());

	header("Location:../out/out.Templates.php");
}

// delete------------------------------------------------------------
else if ($action == "removetemplate") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('removetemplate', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$result = $dms->deleteTemplate($_GET['template_id']);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('template_removed')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}

}

// modify------------------------------------------------------------
else if ($action == "edittemplatename") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('edittemplatename')) {
		UI::exitError(getMLText("admin_tools"),getMLText("invalid_request_token"));
	}

	if(isset($_POST["template_id"])) {
		$id = $_POST["template_id"];
	}

	if (isset($_POST["name"])) {
		$name = $_POST["name"];
	}

	$result = $dms->updateTemplateName($id, $name);
	$session->setSplashMsg(array('type'=>'success', 'msg'=>getMLText('splash_edit_template_success')));

	add_log_line(".php&action=edittemplate&login=".$user->getLogin());
	header("Location:../out/out.Templates.php?selected_template=".$id);
} 

// modify------------------------------------------------------------
else if ($action == "edittemplatecontent") {

	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('edittemplatecontent', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$id = $_POST["template_id"];

	if(isset($_POST['content']) && $_POST['content'] == "") {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('empty_template_content'), 'data'=>''));	
	}

	$content = $_POST['content'];
	
	$result = $dms->updateTemplateContent($id, $content);

	//echo json_encode(array('success'=>false, 'message'=>getMLText('empty_template_content'), 'data'=>$result));

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('data_saved')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}
} 

else if ($action == "gettemplate") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('gettemplate', 'GET')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	$document = $dms->getDocument($_GET['document_id']);
	$attributes = $document->getAttributes();

	$result = $dms->getTemplate($_GET['template_id']);

	if ($result) {

		$html = $result[0]['content'];
		$template_content = $html;		

		preg_match_all('/{(.*?)}/', $html, $match);

		if (!empty($match[1])) {
			foreach ($match[1] as $shortcut) {
				switch ($shortcut) {
					case 'ID':
						$ID = $document->getID();
						$template_content = str_replace('{ID}', $ID, $template_content);
						break;

					case 'Nombre':
						$name = $document->getName();
						$template_content = str_replace('{Nombre}', $name, $template_content);
						break;

					case 'Propietario':
						$owner = $document->getOwner();
						$owner_fullname = $owner->getFullName();
						$template_content = str_replace('{Propietario}', $owner_fullname, $template_content);
						break;

					case 'Comentarios':
						$comment = $document->getComment();
						$template_content = str_replace('{Comentarios}', $comment, $template_content);
						break;

					case 'Modo de acceso por defecto':
						$template_content = str_replace('{Modo de acceso por defecto}', "Lectura", $template_content);
						break;

					case 'Tipo de acceso':
						$template_content = str_replace('{Tipo de acceso}', "Heredado", $template_content);
						break;

					case 'Espacio de disco utilizado':						
						$size = SeedDMS_Core_File::format_filesize($document->getUsedDiskSpace());
						$template_content = str_replace('{Espacio de disco utilizado}', $size, $template_content);
						break;

					case 'Creación':						
						$creation = date("d-m-Y H:i:s", $document->getDate());
						$template_content = str_replace('{Creación}', $creation, $template_content);
						break;

					case 'Vigente hasta':						
						$expires = date("d-m-Y", $document->getExpires());
						$template_content = str_replace('{Vigente hasta}', $expires, $template_content);
						break;

					case 'Palabras clave':
						$keywords = $document->getKeywords();
						$template_content = str_replace('{Palabras clave}', $keywords, $template_content);
						break;

					case 'Categorías':
						$ct = array();
						foreach($cats as $cat){
							$ct[] = htmlspecialchars($cat->getName());
						}						
						$categories = implode(', ', $ct);
						$template_content = str_replace('{Categorías}', $categories, $template_content);
						break;

					case 'Descripción':
						$attrib = 'N/A';
						if($attributes) {
							foreach($attributes as $attribute) {								
								if($attribute->getAttributeDefinition()->getName() == "Descripción") {
									$attrib = $attribute->getValue();
								}
							}
						}

						$template_content = str_replace('{Descripción}', $attrib, $template_content);
						break;

					case 'Tipo de información solicitada':
						$attrib = 'N/A';
						if($attributes) {
							foreach($attributes as $attribute) {								
								if($attribute->getAttributeDefinition()->getName() == "Tipo de información solicitada") {
									$attrib = $attribute->getValue();
								}
							}
						}

						$template_content = str_replace('{Tipo de información solicitada}', $attrib, $template_content);
						break;

					case 'Tipo de resolución':
						$attrib = 'N/A';
						if($attributes) {
							foreach($attributes as $attribute) {
								if($attribute->getAttributeDefinition()->getName() == "Tipo de resolución") {
									$attrib = $attribute->getValue();
								}
							}
						}

						$template_content = str_replace('{Tipo de resolución}', $attrib, $template_content);
						break;

					case 'Unidad a remitir':
						$attrib = 'N/A';
						if($attributes) {
							foreach($attributes as $attribute) {
								if($attribute->getAttributeDefinition()->getName() == "Unidad a remitir") {
									$attrib = $attribute->getValue();
								}
							}
						}

						$template_content = str_replace('{Unidad a remitir}', $attrib, $template_content);
						break;
					
					default:
						
						break;
				}
			}
		}

		header('Content-Type: application/json');		
		echo json_encode(array('success'=>true, 'message'=>getMLText('template_get_success'), 'html' => $template_content));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}

}

else if ($action == "savememo") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('saveeditorcontent', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	if(isset($_POST['content']) && $_POST['content'] == "") {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('empty_template_content'), 'data'=>''));	
	}

	$document_id = $_POST["document_id"];
	$content = $_POST['content'];
	$result = $dms->saveDocumentMemoContent($document_id, $content);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('data_saved')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}

}

else if ($action == "saveresolution") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('saveeditorcontent', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	if(isset($_POST['content']) && $_POST['content'] == "") {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('empty_template_content'), 'data'=>''));	
	}

	$document_id = $_POST["document_id"];
	$content = $_POST['content'];

	$result = $dms->saveDocumentResolutionContent($document_id, $content);

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('data_saved')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}

}

else if ($action == "savedocumentcontentmargins") {
	/* Check if the form data comes from a trusted request */
	if(!checkFormKey('savedocumentcontentmargins', 'POST')) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('invalid_request_token'), 'data'=>''));
	}

	if((float)$_POST['margin_left'] < 1 || (float)$_POST['margin_top'] < 1 || (float)$_POST['margin_right'] < 1 || (float)$_POST['margin_bottom'] < 1 ) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('incorrect_margin_value'), 'data'=>''));	
	}

	$margin_left = $_POST['margin_left'];
	$margin_top = $_POST['margin_top'];
	$margin_right = $_POST['margin_right'];
	$margin_bottom = $_POST['margin_bottom'];
	$document_id = $_POST['document_id'];

	if ($dms->getDocumentContentMargins($document_id)) {
		$result = $dms->editDocumentContentMargins($document_id, $margin_left, $margin_top, $margin_right, $margin_bottom);
	} else {
		$result = $dms->addDocumentContentMargins($document_id, $margin_left, $margin_top, $margin_right, $margin_bottom);	
	}

	if ($result) {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>true, 'message'=>getMLText('data_saved')));
	} else {
		header('Content-Type: application/json');
		echo json_encode(array('success'=>false, 'message'=>getMLText('splash_error')));	
	}
}

else if ($action == "getpdfdownload") {	

	$document_id = $_POST["document_id"];

	if ($result = $dms->getDocumentResolutionContent($document_id)) {
		$content = $result['content'];
	} else if ($result = $dms->getDocumentMemoContent($document_id)) {
		$content = $result['content'];
	} else {
		$content = '<p>Sin contenido guardado</p>';
	}
	
	$dompdf = new Dompdf();
	$dompdf->set_option('isRemoteEnabled', true); // For URL images

	if ($doc_margins = $dms->getDocumentContentMargins($document_id)) {
		$html = $dms->getPDFHtmlStart($doc_margins['margin_left'], $doc_margins['margin_top'], $doc_margins['margin_right'], $doc_margins['margin_bottom']);
	} else {
		$html = $dms->getPDFHtmlStart();
	}

	//$html = $dms->getPDFHtmlStart();
	$html .= preg_replace("/\xE2\x80\x8B/", "", $content); // clean html of zero width characters
	$html .= $dms->getPDFHtmlEnd();

	$dompdf->loadHtml($html);
	$dompdf->setPaper('letter', 'portrait');
	$dompdf->render();
	$pdf = $dompdf->output();
	$temp_dir = dirname(dirname(dirname(__FILE__)))."/htdocs/temp/pdftemp";

	if (!file_exists($temp_dir)) {
		mkdir($temp_dir, 0765, true);
	}

	$date_now = preg_replace('/\s+/', '', date("Y-m-d H:i:s"));
	$file_name = "temp_pdf-".$date_now.".pdf";	
	file_put_contents($temp_dir."/".$file_name, $pdf);
	$userfiletmp = $temp_dir."/".$file_name;
	
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($userfiletmp));
	header('Content-Disposition: attachment; filename="preview_pdf_file.pdf"');
	header("Content-Type: application/pdf");
	header("Cache-Control: must-revalidate");
	ob_clean();
	readfile($userfiletmp);

	if (file_exists($userfiletmp)) {
		unlink($userfiletmp); // removes	
	}

	//header("Location:../out/out.ViewDocument.php?documentid=".$document_id);
}



else UI::exitError(getMLText("admin_tools"),getMLText("unknown_command"));



?>
