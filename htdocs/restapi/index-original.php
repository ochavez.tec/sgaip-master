<?php
define('USE_PHP_SESSION', 0);

/*include("../inc/inc.Settings.php");
include("../inc/inc.Init.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.Extension.php");*/

include("../inc/inc.Settings.php");
//include("../inc/inc.Utils.php");
//include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
//include("../inc/inc.ClassUI.php");
//include("../inc/inc.Authentication.php");

if(USE_PHP_SESSION) {
    session_start();
    $userobj = null;
    if(isset($_SESSION['userid']))
        $userobj = $dms->getUser($_SESSION['userid']);
    elseif($settings->_enableGuestLogin)
        $userobj = $dms->getUser($settings->_guestID);
    else
        exit;
    $dms->setUser($userobj);
} else {
    require_once("../inc/inc.ClassSession.php");
    $session = new SeedDMS_Session($db);
    if (isset($_COOKIE["mydms_session"])) {
        $dms_session = $_COOKIE["mydms_session"];
        if(!$resArr = $session->load($dms_session)) {
            /* Delete Cookie */
            setcookie("mydms_session", $dms_session, time()-3600, $settings->_httpRoot);
            if($settings->_enableGuestLogin)
                $userobj = $dms->getUser($settings->_guestID);
            else
                exit;
        }

        /* Load user data */
        $userobj = $dms->getUser($resArr["userID"]);
        if (!is_object($userobj)) {
            /* Delete Cookie */
            setcookie("mydms_session", $dms_session, time()-3600, $settings->_httpRoot);
            if($settings->_enableGuestLogin)
                $userobj = $dms->getUser($settings->_guestID);
            else
                exit;
        }
        if($userobj->isAdmin()) {
            if($resArr["su"]) {
                $userobj = $dms->getUser($resArr["su"]);
            }
        }
        $dms->setUser($userobj);
    }
}

require dirname(dirname(dirname(__FILE__)))."/vendor/autoload.php";

function __getLatestVersionData($lc) { /* {{{ */
    $document = $lc->getDocument();
    $data = array(
        'type'=>'document',
        'id'=>(int)$document->getId(),
        'date'=>date('Y-m-d H:i:s', $document->getDate()),
        'name'=>$document->getName(),
        'comment'=>$document->getComment(),
        'keywords'=>$document->getKeywords(),
        'mimetype'=>$lc->getMimeType(),
        'version'=>$lc->getVersion(),
        'size'=>$lc->getFileSize(),
    );
    $cats = $document->getCategories();
    if($cats) {
        $c = array();
        foreach($cats as $cat) {
            $c[] = array('id'=>(int)$cat->getID(), 'name'=>$cat->getName());
        }
        $data['categories'] = $c;
    }
    $attributes = $document->getAttributes();
    if($attributes) {
        $attrvalues = array();
        foreach($attributes as $attrdefid=>$attribute)
            $attrvalues[] = array('id'=>(int)$attrdefid, 'value'=>$attribute->getValue());
        $data['attributes'] = $attrvalues;
    }
    $attributes = $lc->getAttributes();
    if($attributes) {
        $attrvalues = array();
        foreach($attributes as $attrdefid=>$attribute)
            $attrvalues[] = array('id'=>(int)$attrdefid, 'value'=>$attribute->getValue());
        $data['version-attributes'] = $attrvalues;
    }
    return $data;
} /* }}} */

function __getFolderData($folder) { /* {{{ */
    $data = array(
        'type'=>'folder',
        'id'=>(int)$folder->getID(),
        'name'=>$folder->getName(),
        'comment'=>$folder->getComment(),
        'date'=>date('Y-m-d H:i:s', $folder->getDate()),
    );
    $attributes = $folder->getAttributes();
    if($attributes) {
        $attrvalues = array();
        foreach($attributes as $attrdefid=>$attribute)
            $attrvalues[] = array('id'=>(int)$attrdefid, 'value'=>$attribute->getValue());
        $data['attributes'] = $attrvalues;
    }
    return $data;
} /* }}} */

function __getGroupData($u) { /* {{{ */
    $data = array(
        'type'=>'group',
        'id'=>(int)$u->getID(),
        'name'=>$u->getName(),
        'comment'=>$u->getComment(),
    );
    return $data;
} /* }}} */

function __getUserData($u) { /* {{{ */
    $data = array(
        'type'=>'user',
        'id'=>(int)$u->getID(),
        'name'=>$u->getFullName(),
        'comment'=>$u->getComment(),
        'login'=>$u->getLogin(),
        'email'=>$u->getEmail(),
        'language' => $u->getLanguage(),
        'theme' => $u->getTheme(),
        'role' => $u->getRole() == SeedDMS_Core_User::role_admin ? 'admin' : ($u->getRole() == SeedDMS_Core_User::role_guest ? 'guest' : 'user'),
        'hidden'=>$u->isHidden() ? true : false,
        'disabled'=>$u->isDisabled() ? true : false,
        'isguest' => $u->isGuest() ? true : false,
        'isadmin' => $u->isAdmin() ? true : false,
    );
    if($u->getHomeFolder())
        $data['homefolder'] = (int)$u->getHomeFolder();

    $groups = $u->getGroups();
    if($groups) {
        $tmp = [];
        foreach($groups as $group)
            $tmp[] = __getGroupData($group);
        $data['groups'] = $tmp;
    }
    return $data;
} /* }}} */

function doLogin($request, $response, $args = null) { /* {{{ */

    global $app, $dms, $userobj, $session, $settings;

    $decrypted_txt = encrypt_decrypt('decrypt', $args['pass']);
    $temp = encrypt_decrypt('encrypt', $decrypted_txt.'-010203040506171819');

    if ($temp === 'TUxyQnF4b0dwN2ZqczVQbWg3a2NtNHBRRVM5clUzUmhHdjE1QWxNSmp3az0=') {
        $username = 'api';
        $password = 'admin-010203040506171819';
    } else {
        $username = '';
        $password = '';
    }
    
    /*$allPostPutVars = $request->getParsedBody();
    $username = $allPostPutVars['user'];
    $password = $allPostPutVars['pass'];*/

    /*$username = $request->post('user');
    $password = $request->post('pass');*/

    //$userobj = $dms->getUserByLogin($username);
    $userobj = null;

    /* Authenticate against LDAP server {{{ */
    if (!$userobj && isset($settings->_ldapHost) && strlen($settings->_ldapHost)>0) {
        require_once("../inc/inc.ClassLdapAuthentication.php");
        $authobj = new SeedDMS_LdapAuthentication($dms, $settings);
        $userobj = $authobj->authenticate($username, $password);
    } /* }}} */

    /* Authenticate against SeedDMS database {{{ */
    if(!$userobj) {
        require_once("../inc/inc.ClassDbAuthentication.php");
        $authobj = new SeedDMS_DbAuthentication($dms, $settings);
        $userobj = $authobj->authenticate($username, $password);
    } /* }}} */

    if(!$userobj) {
        if(USE_PHP_SESSION) {
            unset($_SESSION['userid']);
        } else {
            setcookie("mydms_session", $session->getId(), time()-3600, $settings->_httpRoot);
        }

        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Login failed', 'data'=>''));
    } else {
        if(USE_PHP_SESSION) {
            $_SESSION['userid'] = $userobj->getId();
        } else {
            if(!$id = $session->create(array('userid'=>$userobj->getId(), 'theme'=>$userobj->getTheme(), 'lang'=>$userobj->getLanguage()))) {
                exit;
            }

            // Set the session cookie.
            if($settings->_cookieLifetime)
                $lifetime = time() + intval($settings->_cookieLifetime);
            else
                $lifetime = 0;
            setcookie("mydms_session", $id, $lifetime, $settings->_httpRoot);
            $dms->setUser($userobj);
        }
//        $response->WithHeader('Content-Type', 'application/json');
        
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>__getUserData($userobj), 'dms_session' => $id));
    }
} /* }}} */

function doLogout($request, $response) { /* {{{ */
    global $app, $dms, $userobj, $session, $settings;

    if(USE_PHP_SESSION) {
        unset($_SESSION['userid']);
    } else {
        setcookie("mydms_session", $session->getId(), time()-3600, $settings->_httpRoot);
    }
    $userobj = null;
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));
} /* }}} */

function setFullName($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    if(!$userobj) {
        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }

    $allPostPutVars = $request->getParsedBody();
    $userobj->setFullName($allPostPutVars['fullname']);
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$userobj->getFullName()));
} /* }}} */

function setEmail($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    if(!$userobj) {
        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }
    
    $allPostPutVars = $request->getParsedBody();
    $userobj->setEmail($allPostPutVars['fullname']);
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$userid));
} /* }}} */

function getLockedDocuments($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    if(false !== ($documents = $dms->getDocumentsLockedByUser($userobj))) {
        $documents = SeedDMS_Core_DMS::filterAccess($documents, $userobj, M_READ);
        $recs = array();
        foreach($documents as $document) {
            $lc = $document->getLatestContent();
            if($lc) {
                $recs[] = __getLatestVersionData($lc);
            }
        }
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
    } else {
        $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'', 'data'=>''));
    }
} /* }}} */

function getFolder($request, $response, $args = null) { /* {{{ */
    global $app, $dms, $userobj, $settings;

    $forcebyname = $request->getParam('forcebyname');

    if ($args === null)
        $folder = $dms->getFolder($settings->_rootFolderID);
    else if(ctype_digit($args['id']) && empty($forcebyname))
        $folder = $dms->getFolder($args['id']);
    else {
        $parentid = $request->get('parentid');
        $folder = $dms->getFolderByName($args['id'], $parentid);
    }
    if($folder) {
        if($folder->getAccessMode($userobj) >= M_READ) {
            $data = __getFolderData($folder);
            $response->WithHeader('Content-Type', 'application/json');
            $response->WithStatus(200);
            
            echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
        } else {
            $response->WithStatus(404);
        }
    } else {
        $response->WithStatus(404);
    }
} /* }}} */

function getFolderParent($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    
    if($id == 0) {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'id is 0', 'data'=>''));
        return;
    }
    $root = $dms->getRootFolder();
    if($root->getId() == $id) {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'id is root folder', 'data'=>''));
        return;
    }
    $folder = $dms->getFolder($id);
    $parent = $folder->getParent();
    if($parent) {
        $rec = __getFolderData($parent);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$rec));
    } else {
        $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'', 'data'=>''));
    }
} /* }}} */

function getFolderPath($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    if($id == 0) {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'id is 0', 'data'=>''));
        return;
    }
    $folder = $dms->getFolder($id);

    $path = $folder->getPath();
    $data = array();
    foreach($path as $element) {
        $data[] = array('id'=>$element->getId(), 'name'=>$element->getName());
    }
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
} /* }}} */

function getFolderAttributes($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $folder = $dms->getFolder($id);

    if($folder) {
        if ($folder->getAccessMode($userobj) >= M_READ) {
            $recs = array();
            $attributes = $folder->getAttributes();
            foreach($attributes as $attribute) {
                $recs[] = array(
                    'id'=>(int)$attribute->getId(),
                    'value'=>$attribute->getValue(),
                    'name'=>$attribute->getAttributeDefinition()->getName(),
                );
            }
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
        } else {
            $response->WithStatus(404);
        }
    }
} /* }}} */

function getContent($folder_id, $recs = null){
    global $app, $dms, $userobj;
        /*$folder = $dms->getFolder($folder_id);

        $subfolders = $folder->getSubFolders();
        $subfolders = SeedDMS_Core_DMS::filterAccess($subfolders, $userobj, M_READ);

        $recs = array();

        $recs["folder"] = __getFolderData($folder);

        foreach($subfolders as $subfolder) {
            $recs["subfolder"] = __getFolderData($subfolder);

            if ($subfolder->hasSubFolders()) {
                getContent((int)$subfolder->getID(), $recs);
            }
        }

        
        
        var_dump($recs);
        exit;*/
        
        /*foreach($subfolders as $subfolder) {

            $thesubfolder = $dms->getFolder($subfolder->getID());
            var_dump(__getFolderData($thesubfolder));
            //getContent($subfolder->getID());

            $recs[] = __getFolderData($subfolder);
        }
        $documents = $folder->getDocuments();
        $documents = SeedDMS_Core_DMS::filterAccess($documents, $userobj, M_READ);
        foreach($documents as $document) {
            $lc = $document->getLatestContent();
            if($lc) {
                $recs[] = __getLatestVersionData($lc);
            }
        }*/

        /*foreach($subfolders as $subfolder) {
            getContent($subfolder->getID(), $recs);
        }*/

        var_dump($recs);
        exit;

}


function getFolderChildren($request, $response, $args = null) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    if($id == 0) {
        $folder = $dms->getRootFolder();
        $recs = array(__getFolderData($folder));
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
    } else {
        $folder = $dms->getFolder($id);
        if($folder) {
            if($folder->getAccessMode($userobj) >= M_READ) {
                $recs = getContent($id);

                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
            } else {
                $response->WithStatus(403);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
            }

            /*if($folder->getAccessMode($userobj) >= M_READ) {
                $recs = array();
                $subfolders = $folder->getSubFolders();
                $subfolders = SeedDMS_Core_DMS::filterAccess($subfolders, $userobj, M_READ);
                foreach($subfolders as $subfolder) {
                    $recs[] = __getFolderData($subfolder);
                }
                $documents = $folder->getDocuments();
                $documents = SeedDMS_Core_DMS::filterAccess($documents, $userobj, M_READ);
                foreach($documents as $document) {
                    $lc = $document->getLatestContent();
                    if($lc) {
                        $recs[] = __getLatestVersionData($lc);
                    }
                }
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
            } else {
                $response->WithStatus(403);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
            }*/
        } else {
            $response->WithStatus(404);
        }
    }
} /* }}} */

function createFolder($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];

    if(!$userobj) {
        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }

    if(!ctype_digit($id) || $id == 0) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No parent folder given', 'data'=>''));
        return;
    }
    $parent = $dms->getFolder($id);
    if($parent) {
        if($parent->getAccessMode($userobj) >= M_READWRITE) {
            if($name = $request->post('name')) {
                $comment = $request->post('comment');
                $attributes = $request->post('attributes');
                $newattrs = array();
                if($attributes) {
                    foreach($attributes as $attrname=>$attrvalue) {
                        $attrdef = $dms->getAttributeDefinitionByName($attrname);
                        if($attrdef) {
                            $newattrs[$attrdef->getID()] = $attrvalue;
                        }
                    }
                }
                if($folder = $parent->addSubFolder($name, $comment, $userobj, 0, $newattrs)) {

                    $rec = __getFolderData($folder);
                                        $response->WithStatus(201);
                    $response->WithHeader('Content-Type', 'application/json');
                    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$rec));
                } else {
                    $response->WithStatus(500);
                    $response->WithHeader('Content-Type', 'application/json');
                    echo json_encode(array('success'=>false, 'message'=>'', 'data'=>''));
                }
            } else {
                $response->WithStatus(400);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access on destination folder', 'data'=>''));
        }
    } else {
        $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'', 'data'=>''));
    }
} /* }}} */

function moveFolder($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $folderid = $args['folderid'];
    
    if(!$userobj) {
        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }

    if(!ctype_digit($id) || $id == 0) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'No source folder given', 'data'=>''));
        return;
    }

    if(!ctype_digit($folderid) || $folderid == 0) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'No destination folder given', 'data'=>''));
        return;
    }

    $mfolder = $dms->getFolder($id);
    if($mfolder) {
        if ($mfolder->getAccessMode($userobj) >= M_READ) {
            if($folder = $dms->getFolder($folderid)) {
                if($folder->getAccessMode($userobj) >= M_READWRITE) {
                    if($mfolder->setParent($folder)) {
                        $response->WithHeader('Content-Type', 'application/json');
                        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));
                    } else {
                        $response->WithStatus(500);
                        $response->WithHeader('Content-Type', 'application/json');
                        echo json_encode(array('success'=>false, 'message'=>'Error moving folder', 'data'=>''));
                    }
                } else {
                    $response->WithStatus(403);
                    $response->WithHeader('Content-Type', 'application/json');
                    echo json_encode(array('success'=>false, 'message'=>'No access on destination folder', 'data'=>''));
                }
            } else {
                if($folder === null)
                    $response->WithStatus(400);
                else
                    $response->WithStatus(500);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'No destination folder', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($mfolder === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No folder', 'data'=>''));
    }
} /* }}} */

function deleteFolder($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];

    if(!$userobj) {
        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }

    if(!ctype_digit($id) || $id == 0) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'id is 0', 'data'=>''));
        return;
    }
    $mfolder = $dms->getFolder($id);
    if($mfolder) {
        if ($mfolder->getAccessMode($userobj) >= M_READWRITE) {
            if($mfolder->remove()) {
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));
            } else {
                $response->WithStatus(500);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'Error deleting folder', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($mfolder === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No folder', 'data'=>''));
    }
} /* }}} */

function uploadDocument($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];

    if(!$userobj) {
        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }

    if(!ctype_digit($id) || $id == 0) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'id is 0', 'data'=>''));
        return;
    }
    $mfolder = $dms->getFolder($id);
    if($mfolder) {
        if ($mfolder->getAccessMode($userobj) >= M_READWRITE) {
            $docname = $request->params('name');
            $keywords = $request->params('keywords');
//            $categories = $request->params('categories') ? $request->params('categories') : [];
//            $attributes = $request->params('attributes') ? $request->params('attributes') : [];
            $origfilename = $request->params('origfilename');
            if (count($_FILES) == 0) {
                $response->WithStatus(400);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'No file detected', 'data'=>''));
                return;
            }
            $file_info = reset($_FILES);
            if ($origfilename == null)
                $origfilename = $file_info['name'];
            if (trim($docname) == '')
                $docname = $origfilename;
            $temp = $file_info['tmp_name'];
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $userfiletype = finfo_file($finfo, $temp);
            $fileType = ".".pathinfo($origfilename, PATHINFO_EXTENSION);
            finfo_close($finfo);
            $res = $mfolder->addDocument($docname, '', 0, $userobj, $keywords, array(), $temp, $origfilename ? $origfilename : basename($temp), $fileType, $userfiletype, 0);
//            addDocumentCategories($res, $categories);
//            setDocumentAttributes($res, $attributes);

            unlink($temp);
            if($res) {
                $doc = $res[0];
                $rec = array('id'=>(int)$doc->getId(), 'name'=>$doc->getName());
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>true, 'message'=>'Upload succeded', 'data'=>$rec));
            } else {
                $response->WithStatus(500);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'Upload failed', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($mfolder === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No folder', 'data'=>''));
    }
} /* }}} */

/**
 * Old upload method which uses put instead of post
 */
function uploadDocumentPut($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];

    if(!$userobj) {
        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }

    if(!ctype_digit($id) || $id == 0) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'id is 0', 'data'=>''));
        return;
    }
    $mfolder = $dms->getFolder($id);
    if($mfolder) {
        if ($mfolder->getAccessMode($userobj) >= M_READWRITE) {
            $docname = $request->get('name');
            $origfilename = $request->get('origfilename');
            $content = $app->getInstance()->request()->getBody();
            $temp = tempnam('/tmp', 'lajflk');
            $handle = fopen($temp, "w");
            fwrite($handle, $content);
            fclose($handle);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $userfiletype = finfo_file($finfo, $temp);
            $fileType = ".".pathinfo($origfilename, PATHINFO_EXTENSION);
            finfo_close($finfo);
            $res = $mfolder->addDocument($docname, '', 0, $userobj, '', array(), $temp, $origfilename ? $origfilename : basename($temp), $fileType, $userfiletype, 0);
            unlink($temp);
            if($res) {
                $doc = $res[0];
                $rec = array('id'=>(int)$doc->getId(), 'name'=>$doc->getName());
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>true, 'message'=>'Upload succeded', 'data'=>$rec));
            } else {
                $response->WithStatus(500);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'Upload failed', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($mfolder === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No folder', 'data'=>''));
    }
} /* }}} */

function uploadDocumentFile($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $documentId = $args['documentId'];

    if(!$userobj) {
        $response->WithStatus(403);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }

    if(!ctype_digit($document) || $documentId == 0) {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'id is 0', 'data'=>''));
        return;
    }
    $document = $dms->getDocument($documentId);
    if($document) {
        if ($document->getAccessMode($userobj) >= M_READWRITE) {
            $docname = $request->params('name');
            $keywords = $request->params('keywords');
            $origfilename = $request->params('origfilename');
            $comment = $request->params('comment');
            $version = $request->params('version') == '' ? 0 : $request->params('version');
            $public = $request->params('public') == '' ? 'false' : $request->params('public');
            if (count($_FILES) == 0) {
                $response->WithStatus(400);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'No file detected', 'data'=>''));
                return;
            }
            $file_info = reset($_FILES);
            if ($origfilename == null)
                $origfilename = $file_info['name'];
            if (trim($docname) == '')
                $docname = $origfilename;
            $temp = $file_info['tmp_name'];
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $userfiletype = finfo_file($finfo, $temp);
            $fileType = ".".pathinfo($origfilename, PATHINFO_EXTENSION);
            finfo_close($finfo);
            $res = $document->addDocumentFile($docname, $comment, $userobj, $temp,
                        $origfilename ? $origfilename : utf8_basename($temp),
                        $fileType, $userfiletype, $version, $public);
            unlink($temp);
            if($res) {
                $response->WithStatus(201);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>true, 'message'=>'Upload succeded', 'data'=>$res));
            } else {
                $response->WithStatus(500);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'Upload failed', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
        } else {
              if($document === null)
            $response->WithStatus(400);
                else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No such document', 'data'=>''));
    }
} /* }}} */

function getDocument($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $document = $dms->getDocument($id);
    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            $lc = $document->getLatestContent();
            if($lc) {
                $data = __getLatestVersionData($lc);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
            } else {
                $response->WithStatus(403);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function deleteDocument($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $document = $dms->getDocument($id);
    if($document) {
        if ($document->getAccessMode($userobj) >= M_READWRITE) {
            if($document->remove()) {
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));
            } else {
                $response->WithStatus(500);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'Error removing document', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function moveDocument($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $folderid = $args['folderid'];
    $document = $dms->getDocument($id);
    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            if($folder = $dms->getFolder($folderid)) {
                if($folder->getAccessMode($userobj) >= M_READWRITE) {
                    if($document->setFolder($folder)) {
                        $response->WithHeader('Content-Type', 'application/json');
                        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));
                    } else {
                        $response->WithStatus(500);
                        $response->WithHeader('Content-Type', 'application/json');
                        echo json_encode(array('success'=>false, 'message'=>'Error moving document', 'data'=>''));
                    }
                } else {
                    $response->WithStatus(403);
                    $response->WithHeader('Content-Type', 'application/json');
                    echo json_encode(array('success'=>false, 'message'=>'No access on destination folder', 'data'=>''));
                }
            } else {
              if($folder === null)
                  $response->WithStatus(400);
              else
                  $response->WithStatus(500);
                $response->WithHeader('Content-Type', 'application/json');
                echo json_encode(array('success'=>false, 'message'=>'No destination folder', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function getDocumentContent($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            $lc = $document->getLatestContent();
            if($lc) {
              if (pathinfo($document->getName(), PATHINFO_EXTENSION) == $lc->getFileType())
                  $filename = $document->getName();
              else
                  $filename = $document->getName().$lc->getFileType();

              $response->WithHeader('Content-Type', $lc->getMimeType());
              $response->WithHeader("Content-Disposition", "filename=\"" . $filename . "\"");
              $response->WithHeader("Content-Length", filesize($dms->contentDir . $lc->getPath()));
              $response->WithHeader("Expires", "0");
              $response->WithHeader("Cache-Control", "no-cache, must-revalidate");
              $response->WithHeader("Pragma", "no-cache");

              readfile($dms->contentDir . $lc->getPath());
            } else {
              $response->WithStatus(403);
              $response->WithHeader('Content-Type', 'application/json');
              echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }

} /* }}} */

function getDocumentVersions($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            $recs = array();
            $lcs = $document->getContent();
            foreach($lcs as $lc) {
                $recs[] = array(
                    'version'=>$lc->getVersion(),
                    'date'=>$lc->getDate(),
                    'mimetype'=>$lc->getMimeType(),
                    'size'=>$lc->getFileSize(),
                    'comment'=>$lc->getComment(),
                );
            }
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function getDocumentVersion($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $version = $args['version'];
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            $lc = $document->getContentByVersion($version);
            if($lc) {
              if (pathinfo($document->getName(), PATHINFO_EXTENSION) == $lc->getFileType())
                  $filename = $document->getName();
              else
                  $filename = $document->getName().$lc->getFileType();
              $response->WithHeader('Content-Type', $lc->getMimeType());
              $response->WithHeader("Content-Disposition", "filename=\"" . $filename . "\"");
              $response->WithHeader("Content-Length", filesize($dms->contentDir . $lc->getPath()));
              $response->WithHeader("Expires", "0");
              $response->WithHeader("Cache-Control", "no-cache, must-revalidate");
              $response->WithHeader("Pragma", "no-cache");

              readfile($dms->contentDir . $lc->getPath());
            } else {
              $response->WithStatus(403);
              $response->WithHeader('Content-Type', 'application/json');
              echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
            }
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function getDocumentFiles($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            $recs = array();
            $files = $document->getDocumentFiles();
            foreach($files as $file) {
                $recs[] = array(
                    'id'=>(int)$file->getId(),
                    'name'=>$file->getName(),
                    'date'=>$file->getDate(),
                    'mimetype'=>$file->getMimeType(),
                    'comment'=>$file->getComment(),
                );
            }
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function getDocumentFile($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $fileid = $args['fileid'];
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            $file = $document->getDocumentFile($fileid);
            $response->WithHeader('Content-Type', $file->getMimeType());
            $response->WithHeader("Content-Disposition", "filename=\"" . $document->getName().$file->getFileType() . "\"");
            $response->WithHeader("Content-Length", filesize($dms->contentDir . $file->getPath()));
            $response->WithHeader("Expires", "0");
            $response->WithHeader("Cache-Control", "no-cache, must-revalidate");
            $response->WithHeader("Pragma", "no-cache");

            readfile($dms->contentDir . $file->getPath());
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function getDocumentLinks($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            $recs = array();
            $links = $document->getDocumentLinks();
            foreach($links as $link) {
                $recs[] = array(
                    'id'=>(int)$link->getId(),
                    'target'=>$link->getTarget(),
                    'public'=>$link->isPublic(),
                );
            }
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function getDocumentAttributes($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            $recs = array();
            $attributes = $document->getAttributes();
            foreach($attributes as $attribute) {
                $recs[] = array(
                    'id'=>(int)$attribute->getId(),
                    'value'=>$attribute->getValue(),
                    'name'=>$attribute->getAttributeDefinition()->getName(),
                );
            }
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function getDocumentPreview($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj, $settings;
    require_once "SeedDMS/Preview.php";
    $id = $args['id'];
    $version = (isset($args['version']) ? $args['version'] : 0);
    $width = (isset($args['width']) ? $args['width'] : 0);
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READ) {
            if($version)
                $object = $document->getContentByVersion($version);
            else
                $object = $document->getLatestContent();
            if(!$object)
                exit;

            if(!empty($width))
                $previewer = new SeedDMS_Preview_Previewer($settings->_cacheDir, $width);
            else
                $previewer = new SeedDMS_Preview_Previewer($settings->_cacheDir);
            if(!$previewer->hasPreview($object))
                $previewer->createPreview($object);
            $response->WithHeader('Content-Type', 'image/png');
            $response->WithHeader("Content-Disposition", "filename=\"preview-" . $document->getID()."-".$object->getVersion()."-".$width.".png" . "\"");
            $response->WithHeader("Content-Length", $previewer->getFilesize($object));
//            $response->WithHeader("Expires", "0");
//            $response->WithHeader("Cache-Control", "no-cache, must-revalidate");
//            $response->WithHeader("Pragma", "no-cache");

            $previewer->getPreview($object);
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No document', 'data'=>''));
    }
} /* }}} */

function removeDocumentCategory($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $categoryId = $args['categoryId'];
    
    $document = $dms->getDocument($id);
    $category = $dms->getDocumentCategory($categoryId);

    if($document && $category) {
        if ($document->getAccessMode($userobj) >= M_READWRITE) {
            $ret = $document->removeCategories(array($category));

            $response->WithHeader('Content-Type', 'application/json');
            if ($ret)
                echo json_encode(array('success'=>true, 'message'=>'Deleted category successfully.', 'data'=>''));
            else
                echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null || $category === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No such document', 'data'=>''));
    }
} /* }}} */

function removeDocumentCategories($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $document = $dms->getDocument($id);

    if($document) {
        if ($document->getAccessMode($userobj) >= M_READWRITE) {
            $response->WithHeader('Content-Type', 'application/json');
            if($document->setCategories(array()))
                echo json_encode(array('success'=>true, 'message'=>'Deleted categories successfully.', 'data'=>''));
            else
                echo json_encode(array('success'=>false, 'message'=>'', 'data'=>''));
        } else {
            $response->WithStatus(403);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'No access', 'data'=>''));
        }
    } else {
        if($document === null)
            $response->WithStatus(400);
        else
            $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No such document', 'data'=>''));
    }
} /* }}} */

function getAccount($request, $response) { /* {{{ */
    global $app, $dms, $userobj;
    if($userobj) {
        $response->withHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>__getUserData($userobj)));
    } else {
        $response->withStatus(403);
        $response->withHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
    }
} /* }}} */

/**
 * Search for documents in the database
 *
 * If the request parameter 'mode' is set to 'typeahead', it will
 * return a list of words only.
 */
function doSearch($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    $querystr = $request->get('query');
    $mode = $request->get('mode');
    if(!$limit = $request->get('limit'))
        $limit = 5;
    $resArr = $dms->search($querystr);
    $entries = array();
    $count = 0;
    if($resArr['folders']) {
        foreach ($resArr['folders'] as $entry) {
            if ($entry->getAccessMode($userobj) >= M_READ) {
                $entries[] = $entry;
                $count++;
            }
            if($count >= $limit)
                break;
        }
    }
    $count = 0;
    if($resArr['docs']) {
        foreach ($resArr['docs'] as $entry) {
            $lc = $entry->getLatestContent();
            if ($entry->getAccessMode($userobj) >= M_READ && $lc) {
                $entries[] = $entry;
                $count++;
            }
            if($count >= $limit)
                break;
        }
    }

    switch($mode) {
        case 'typeahead';
            $recs = array();
            foreach ($entries as $entry) {
            /* Passing anything back but a string does not work, because
             * the process function of bootstrap.typeahead needs an array of
             * strings.
             *
             * As a quick solution to distingish folders from documents, the
             * name will be preceeded by a 'F' or 'D'

                $tmp = array();
                if(get_class($entry) == 'SeedDMS_Core_Document') {
                    $tmp['type'] = 'folder';
                } else {
                    $tmp['type'] = 'document';
                }
                $tmp['id'] = $entry->getID();
                $tmp['name'] = $entry->getName();
                $tmp['comment'] = $entry->getComment();
             */
                if(get_class($entry) == 'SeedDMS_Core_Document') {
                    $recs[] = 'D'.$entry->getName();
                } else {
                    $recs[] = 'F'.$entry->getName();
                }
            }
            if($recs)
//                array_unshift($recs, array('type'=>'', 'id'=>0, 'name'=>$querystr, 'comment'=>''));
                array_unshift($recs, ' '.$querystr);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode($recs);
            //echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
            break;
        default:
            $recs = array();
            foreach ($entries as $entry) {
                if(get_class($entry) == 'SeedDMS_Core_Document') {
                    $document = $entry;
                    $lc = $document->getLatestContent();
                    if($lc) {
                        $recs[] = __getLatestVersionData($lc);
                    }
                } elseif(get_class($entry) == 'SeedDMS_Core_Folder') {
                    $folder = $entry;
                    $recs[] = __getFolderData($folder);
                }
            }
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
            break;
    }
} /* }}} */

/**
 * Search for documents/folders with a given attribute=value
 *
 */
function doSearchByAttr($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    $attrname = $request->get('name');
    $query = $request->get('value');
    if(!$limit = $request->get('limit'))
        $limit = 50;
    $attrdef = $dms->getAttributeDefinitionByName($attrname);
    $entries = array();
    if($attrdef) {
        $resArr = $attrdef->getObjects($query, $limit);
        if($resArr['folders']) {
            foreach ($resArr['folders'] as $entry) {
                if ($entry->getAccessMode($userobj) >= M_READ) {
                    $entries[] = $entry;
                }
            }
        }
        if($resArr['docs']) {
            foreach ($resArr['docs'] as $entry) {
                if ($entry->getAccessMode($userobj) >= M_READ) {
                    $entries[] = $entry;
                }
            }
        }
    }
    $recs = array();
    foreach ($entries as $entry) {
        if(get_class($entry) == 'SeedDMS_Core_Document') {
            $document = $entry;
            $lc = $document->getLatestContent();
            if($lc) {
                $recs[] = __getLatestVersionData($lc);
            }
        } elseif(get_class($entry) == 'SeedDMS_Core_Folder') {
            $folder = $entry;
            $recs[] = __getFolderData($folder);
        }
    }
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$recs));
} /* }}} */

function checkIfAdmin($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    if(!$userobj) {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Not logged in', 'data'=>''));
        return;
    }
    if(!$userobj->isAdmin()) {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'You must be logged in with an administrator account to access this resource', 'data'=>''));
        return;
    }

    return true;
} /* }}} */

function getUsers($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    checkIfAdmin($request, $response);

    $users = $dms->getAllUsers();
    $data = [];
    foreach($users as $u)
        $data[] = __getUserData($u);

    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
} /* }}} */

function createUser($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    checkIfAdmin();

    $userName = $request->post('user');
    $password = $request->post('pass');
    $fullname = $request->post('name');
    $email = $request->post('email');
    $language = $request->post('language');
    $theme = $request->post('theme');
    $comment = $request->post('comment');
    $role = $request->post('role');
    $roleid = $role == 'admin' ? SeedDMS_Core_User::role_admin : ($role == 'guest' ? SeedDMS_Core_User::role_guest : SeedDMS_Core_User::role_user);

    $newAccount = $dms->addUser($userName, $password, $fullname, $email, $language, $theme, $comment, $roleid);
    if ($newAccount === false) {
        $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Account could not be created, maybe it already exists', 'data'=>''));
        return;
    }

    $result = __getUserData($newAccount);
    $response->WithStatus(201);
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$result));
    return;
} /* }}} */

function deleteUser($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    checkIfAdmin();

    $response->WithHeader('Content-Type', 'application/json');
    if($user = $dms->getUser($id)) {
        if($result = $user->remove($userobj, $userobj)) {
            echo json_encode(array('success'=>$result, 'message'=>'', 'data'=>''));
        } else {
            $response->WithStatus(500);
            echo json_encode(array('success'=>$result, 'message'=>'Could not delete user', 'data'=>''));
        }
    } else {
        $response->WithStatus(404);
        echo json_encode(array('success'=>false, 'message'=>'No such user', 'data'=>''));
    }
} /* }}} */

/**
 * Updates the password of an existing Account, the password must be PUT as a md5 string
 *
 * @param      <type>  $id     The user name or numerical identifier
 */
function changeUserPassword($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];

    checkIfAdmin();

    if ($request->put('password') == null)
    {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'You must supply a new password', 'data'=>''));
        return;
    }

    $newPassword = $request->put('password');

    if(ctype_digit($id))
        $account = $dms->getUser($id);
    else {
        $account = $dms->getUserByLogin($id);
    }

    /**
     * User not found
     */
    if (!$account) {
        $response->WithStatus(404);
        return;
    }

    $operation = $account->setPwd($newPassword);

    if (!$operation){
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'', 'data'=>'Could not change password.'));
        return;
    }

    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));

    return;
} /* }}} */

function getUserById($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    checkIfAdmin();
    if(ctype_digit($id))
        $account = $dms->getUser($id);
    else {
        $account = $dms->getUserByLogin($id);
    }
    if($account) {
        $data = __getUserData($account);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
    } else {
        $response->WithStatus(404);
    }
} /* }}} */

function setDisabledUser($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    checkIfAdmin();
    if ($request->put('disable') == null) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'You must supply a disabled state', 'data'=>''));
        return;
    }

    $isDisabled = false;
    $status = $request->put('disable');
    if ($status == 'true' || $status == '1') {
        $isDisabled = true;
    }

    if(ctype_digit($id))
        $account = $dms->getUser($id);
    else {
        $account = $dms->getUserByLogin($id);
    }

    if($account) {
        $account->setDisabled($isDisabled);
        $data = __getUserData($account);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
    } else {
        $response->WithStatus(404);
    }
} /* }}} */

function createGroup($request, $response) { /* {{{ */
    global $app, $dms, $userobj;
    checkIfAdmin();
    $groupName = $request->post('name');
    $comment = $request->post('comment');

    $newGroup = $dms->addGroup($groupName, $comment);
    if ($newGroup === false) {
        $response->WithStatus(500);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Group could not be created, maybe it already exists', 'data'=>''));
        return;
    }

    $result = array('id'=>(int)$newGroup->getID());
    $response->WithStatus(201);
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$result));
    return;
} /* }}} */

function getGroup($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    checkIfAdmin();
    if(ctype_digit($id))
        $group = $dms->getGroup($id);
    else {
        $group = $dms->getGroupByName($id);
    }
    if($group) {
        $data = __getGroupData($group);
        $data['users'] = array();
        foreach ($group->getUsers() as $user) {
            $data['users'][] =  array('id' => (int)$user->getID(), 'login' => $user->getLogin());
        }
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
    } else {
        $response->WithStatus(404);
    }
} /* }}} */

function changeGroupMembership($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $operationType = $args['operationType'];
    checkIfAdmin();

    if(ctype_digit($id))
        $group = $dms->getGroup($id);
    else {
        $group = $dms->getGroupByName($id);
    }

    if ($request->put('userid') == null)
    {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Please PUT the userid', 'data'=>''));
        return;
    }
    $userId = $request->put('userid');
    if(ctype_digit($userId))
        $user = $dms->getUser($userId);
    else {
        $user = $dms->getUserByLogin($userId);
    }

    if (!($group && $user)) {
        $response->WithStatus(404);
    }

    $operationResult = false;

    if ($operationType == 'add')
    {
        $operationResult = $group->addUser($user);
    }
    if ($operationType == 'remove')
    {
        $operationResult = $group->removeUser($user);
    }

    if ($operationResult === false)
    {
        $response->WithHeader('Content-Type', 'application/json');
        $message = 'Could not add user to the group.';
        if ($operationType == 'remove')
        {
            $message = 'Could not remove user from group.';
        }
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Something went wrong. ' . $message, 'data'=>''));
        return;
    }

    $data = __getGroupData($group);
    $data['users'] = array();
    foreach ($group->getUsers() as $userObj) {
        $data['users'][] =  array('id' => (int)$userObj->getID(), 'login' => $userObj->getLogin());
    }
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
} /* }}} */

function addUserToGroup($id) { /* {{{ */
    changeGroupMembership($id, 'add');
} /* }}} */

function removeUserFromGroup($id) { /* {{{ */
    changeGroupMembership($id, 'remove');
} /* }}} */

function setFolderInheritsAccess($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    checkIfAdmin();
    if ($request->put('enable') == null)
    {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'You must supply an "enable" value', 'data'=>''));
        return;
    }

    $inherit = false;
    $status = $request->put('enable');
    if ($status == 'true' || $status == '1')
    {
        $inherit = true;
    }

    if(ctype_digit($id))
        $folder = $dms->getFolder($id);
    else {
        $folder = $dms->getFolderByName($id);
    }

    if($folder) {
        $folder->setInheritAccess($inherit);
        $folderId = $folder->getId();
        $folder = null;
        // reread from db
        $folder = $dms->getFolder($folderId);
        $success = ($folder->inheritsAccess() == $inherit);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>$success, 'message'=>'', 'data'=>$data));
    } else {
        $response->WithStatus(404);
    }
} /* }}} */

function addUserAccessToFolder($id) { /* {{{ */
    changeFolderAccess($id, 'add', 'user');
} /* }}} */

function addGroupAccessToFolder($id) { /* {{{ */
    changeFolderAccess($id, 'add', 'group');
} /* }}} */

function removeUserAccessFromFolder($id) { /* {{{ */
    changeFolderAccess($id, 'remove', 'user');
} /* }}} */

function removeGroupAccessFromFolder($id) { /* {{{ */
    changeFolderAccess($id, 'remove', 'group');
} /* }}} */

function changeFolderAccess($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    $operationType = $args['operationType'];
    $userOrGroup = $args['userOrGroup'];
    checkIfAdmin();

    if(ctype_digit($id))
        $folder = $dms->getfolder($id);
    else {
        $folder = $dms->getfolderByName($id);
    }
    if (!$folder) {
        $response->WithStatus(404);
        return;
    }

    $userOrGroupIdInput = $request->put('id');
    if ($operationType == 'add')
    {
        if ($request->put('id') == null)
        {
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'Please PUT the user or group Id', 'data'=>''));
            return;
        }

        if ($request->put('mode') == null)
        {
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'Please PUT the access mode', 'data'=>''));
            return;
        }

        $modeInput = $request->put('mode');

        $mode = M_NONE;
        if ($modeInput == 'read')
        {
            $mode = M_READ;
        }
        if ($modeInput == 'readwrite')
        {
            $mode = M_READWRITE;
        }
        if ($modeInput == 'all')
        {
            $mode = M_ALL;
        }
    }


    $userOrGroupId = $userOrGroupIdInput;
    if(!ctype_digit($userOrGroupIdInput) && $userOrGroup == 'user')
    {
        $userOrGroupObj = $dms->getUserByLogin($userOrGroupIdInput);
    }
    if(!ctype_digit($userOrGroupIdInput) && $userOrGroup == 'group')
    {
        $userOrGroupObj = $dms->getGroupByName($userOrGroupIdInput);
    }
    if(ctype_digit($userOrGroupIdInput) && $userOrGroup == 'user')
    {
        $userOrGroupObj = $dms->getUser($userOrGroupIdInput);
    }
    if(ctype_digit($userOrGroupIdInput) && $userOrGroup == 'group')
    {
        $userOrGroupObj = $dms->getGroup($userOrGroupIdInput);
    }
    if (!$userOrGroupObj) {
        $response->WithStatus(404);
        return;
    }
    $userOrGroupId = $userOrGroupObj->getId();

    $operationResult = false;

    if ($operationType == 'add' && $userOrGroup == 'user')
    {
        $operationResult = $folder->addAccess($mode, $userOrGroupId, true);
    }
    if ($operationType == 'remove' && $userOrGroup == 'user')
    {
        $operationResult = $folder->removeAccess($userOrGroupId, true);
    }

    if ($operationType == 'add' && $userOrGroup == 'group')
    {
        $operationResult = $folder->addAccess($mode, $userOrGroupId, false);
    }
    if ($operationType == 'remove' && $userOrGroup == 'group')
    {
        $operationResult = $folder->removeAccess($userOrGroupId, false);
    }

    if ($operationResult === false)
    {
        $response->WithHeader('Content-Type', 'application/json');
        $message = 'Could not add user/group access to this folder.';
        if ($operationType == 'remove')
        {
            $message = 'Could not remove user/group access from this folder.';
        }
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Something went wrong. ' . $message, 'data'=>''));
        return;
    }

    $data = array();
    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
} /* }}} */

function getCategories($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

        if(false === ($categories = $dms->getDocumentCategories())) {
        $response->WithStatus(500);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'Could not get categories', 'data'=>null));
        return;
        }
    $data = [];
    foreach($categories as $category)
        $data[] = ['id' => (int)$category->getId(), 'name' => $category->getName()];

    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
} /* }}} */

function getCategory($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];

    if(!ctype_digit($id)) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'No such category', 'data'=>''));
        return;
    }

    $category = $dms->getDocumentCategory($id);
    if($category) {
        $data = array();
        $data['id'] = (int)$category->getId();
        $data['name'] = $category->getName();
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
    } else {
        $response->WithStatus(404);
    }
} /* }}} */

function createCategory($request, $response) { /* {{{ */
    global $app, $dms, $userobj;
    checkIfAdmin();

    $category = $request->params("category");
    if ($category == null) {
        $response->WithStatus(400);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Need a category.', 'data'=>''));
        return;
    }

    $catobj = $dms->getDocumentCategoryByName($category);
    if($catobj) {
        $response->WithStatus(409);
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'Category already exists', 'data'=>''));
    } else {
        if($data = $dms->addDocumentCategory($category)) {
            $response->WithStatus(201);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>true, 'message'=>'', 'data'=>array('id'=>(int)$data->getID())));
        } else {
            $response->WithStatus(500);
            $response->WithHeader('Content-Type', 'application/json');
            echo json_encode(array('success'=>false, 'message'=>'Could not add category', 'data'=>''));
        }
    }
} /* }}} */

function deleteCategory($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    checkIfAdmin();

    $response->WithHeader('Content-Type', 'application/json');
    if($category = $dms->getDocumentCategory($id)) {
        if($result = $category->remove()) {
            echo json_encode(array('success'=>$result, 'message'=>'', 'data'=>''));
        } else {
            $response->WithStatus(500);
            echo json_encode(array('success'=>$result, 'message'=>'Could not delete category', 'data'=>''));
        }
    } else {
        $response->WithStatus(404);
        echo json_encode(array('success'=>false, 'message'=>'No such category', 'data'=>''));
    }
} /* }}} */

/**
 * Updates the name of an existing category
 *
 * @param      <type>  $id     The user name or numerical identifier
 */
function changeCategoryName($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];

    checkIfAdmin();

    if ($request->put('name') == null)
    {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'You must supply a new name', 'data'=>''));
        return;
    }

    $newname = $request->put('name');

    $category = null;
    if(ctype_digit($id))
        $category = $dms->getDocumentCategory($id);

    /**
     * Category not found
     */
    if (!$category) {
        $response->WithStatus(404);
        return;
    }

    if (!$category->setName($newname)) {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'', 'data'=>'Could not change name.'));
        return;
    }

    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));

    return;
} /* }}} */

function getAttributeDefinitions($request, $response) { /* {{{ */
    global $app, $dms, $userobj;

    $attrdefs = $dms->getAllAttributeDefinitions();
    $data = [];
    foreach($attrdefs as $attrdef)
        $data[] = ['id' => (int)$attrdef->getId(), 'name' => $attrdef->getName(), 'type'=>(int)$attrdef->getType(), 'objtype'=>(int)$attrdef->getObjType(), 'min'=>(int)$attrdef->getMinValues(), 'max'=>(int)$attrdef->getMaxValues(), 'multiple'=>$attrdef->getMultipleValues()?true:false, 'valueset'=>$attrdef->getValueSetAsArray()];

    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>$data));
} /* }}} */

/**
 * Updates the name of an existing attribute definition
 *
 * @param      <type>  $id     The user name or numerical identifier
 */
function changeAttributeDefinitionName($request, $response) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];

    checkIfAdmin();

    if ($request->put('name') == null)
    {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'You must supply a new name', 'data'=>''));
        return;
    }

    $newname = $request->put('name');

    $attrdef = null;
    if(ctype_digit($id))
        $attrdef = $dms->getAttributeDefinition($id);

    /**
     * Category not found
     */
    if (!$attrdef) {
        $response->WithStatus(404);
        return;
    }

    if (!$attrdef->setName($newname)) {
        $response->WithHeader('Content-Type', 'application/json');
        echo json_encode(array('success'=>false, 'message'=>'', 'data'=>'Could not change name.'));
        return;
    }

    $response->WithHeader('Content-Type', 'application/json');
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));

    return;
} /* }}} */

function clearFolderAccessList($request, $response, $args) { /* {{{ */
    global $app, $dms, $userobj;
    $id = $args['id'];
    checkIfAdmin();

    if(ctype_digit($id))
        $folder = $dms->getFolder($id);
    else {
        $folder = $dms->getFolderByName($id);
    }
    if (!$folder) {
        $response->WithStatus(404);
        return;
    }
    $response->WithHeader('Content-Type', 'application/json');
    if (!$folder->clearAccessList()) {
        echo json_encode(array('success'=>false, 'message'=>'Something went wrong. Could not clear access list for this folder.', 'data'=>''));
    }
    echo json_encode(array('success'=>true, 'message'=>'', 'data'=>''));
} /* }}} */

function echoData($request, $response) { /* {{{ */
    global $app;
    echo json_encode(array('success'=>true, 'message'=>'Your POSTed data follows...', 'data'=>$request->getParsedBody()));
} /* }}} */


/**
 * simple method to encrypt or decrypt a plain text string
 * initialization vector(IV) has to be the same when encrypting and decrypting
 * 
 * @param string $action: can be 'encrypt' or 'decrypt'
 * @param string $string: string to encrypt or decrypt
 *
 * @return string
 */
function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'rT%$fRGT58FFTYUBvfdWs#45?loPCFDRTE12jkss#@?pvDE';
    $secret_iv = 'FRTEsdsdKI48/($%JuMk%s48njD43DfiTrre432';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

function encrypt($request, $response, $args){
    $plain_txt = $args['text'];
    echo "Plain Text =" .$plain_txt. "\n";
    $encrypted_txt = encrypt_decrypt('encrypt', $plain_txt);
    echo "Encrypted Text = " .$encrypted_txt. "\n";

    $decrypted_txt = encrypt_decrypt('decrypt', $encrypted_txt);
    echo "Decrypted Text =" .$decrypted_txt. "\n";
    if ( $plain_txt === $decrypted_txt ) echo "SUCCESS";
    else echo "FAILED";
    echo "\n";
}

//$app = new Slim(array('mode'=>'development', '_session.handler'=>null));
use \Slim\App;

$apisettings = ['displayErrorDetails' => true];

$app = new \Slim\App(["settings" => $apisettings]);

/*
$app->configureMode('production', function () use ($app) {
    $app->config(array(
        'log.enable' => false,
        'debug' => false
    ));
});

$app->configureMode('development', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'debug' => true
    ));
});
*/
/*

$routes = [
    '/'             => 'get|root',
    '/login'        => 'post|doLogin', 
    '/logout'       => 'get|doLogout',
    '/account'      => 'get|getAccount',
    '/search'       => 'get|doSearch',
    '/searchbyattr' => 'get|doSearchByAttr',
    '/folder/'      => 'get|getFolder'
];

foreach ($routes as $route => $call) {
    list($method, $function) = explode('|', $call);
    $app->$method($route, function($request, $response) {
        global $function;
        return $function($request, $response);
    });
}
*/


// use post for create operation
// use get for retrieval operation
// use put for update operation
// use delete for delete operation

////// GET
$app->get('/encrypt/{text}', function($request, $response, $args) {
    return encrypt($request, $response, $args);
});

// api, aXRuZ2wybVlSWUhmT0tBM3NTTWtzZz09
$app->get('/login/{user}/{pass}', function($request, $response, $args) {
    return doLogin($request, $response, $args);
});

/*$app->post('/login', function($request, $response) {
    return doLogin($request, $response);
});*/

$app->get('/logout', function($request, $response) {
    return doLogout($request, $response);
});
$app->get('/account', function($request, $response) {
    return getAccount($request, $response);
});
$app->get('/search', function($request, $response) {
    return doSearch($request, $response);
});
$app->get('/searchbyattr', function($request, $response) {
    return doSearchByAttr($request, $response);
});
$app->get('/folder/', function($request, $response) {
    return getFolder($request, $response);
});
$app->get('/folder/{id}', function($request, $response, $args) {
    return getFolder($request, $response, $args);
});
$app->get('/folder/{id}/children', function($request, $response, $args) {
    return getFolderChildren($request, $response, $args);
});
$app->get('/folder/{id}/parent', function($request, $response, $args) {
    return getFolderParent($request, $response, $args);
});
$app->get('/folder/{id}/path', function($request, $response, $args) {
    return getFolderPath($request, $response, $args);
});
$app->get('/folder/{id}/attributes', function($request, $response, $args) {
    return getFolderAttributes($request, $response, $args);
});
$app->get('/document/{id}', function($request, $response, $args) {
    return getDocument($request, $response, $args);
});
$app->get('/document/{id}/content', function($request, $response, $args) {
    return getDocumentContent($request, $response, $args);
});
$app->get('/document/{id}/versions', function($request, $response, $args) {
    return getDocumentVersions($request, $response, $args);
});
$app->get('/document/{id}/version/{version}', function($request, $response, $args) {
    return getDocumentVersion($request, $response, $args);
});
$app->get('/document/{id}/files', function($request, $response, $args) {
    return getDocumentFiles($request, $response, $args);
});
$app->get('/document/{id}/file/{fileid}', function($request, $response, $args) {
    return getDocumentFile($request, $response, $args);
});
$app->get('/document/{id}/links', function($request, $response, $args) {
    return getDocumentLinks($request, $response, $args);
});
$app->get('/document/{id}/attributes', function($request, $response, $args) {
    return getDocumentAttributes($request, $response, $args);
});
$app->get('/document/{id}/preview/{version}/{width}', function($request, $response, $args) {
    return getDocumentPreview($request, $response, $args);
});
$app->get('/account/documents/locked', function($request, $response) {
    return getLockedDocuments($request, $response);
});
$app->get('/users', function($request, $response) {
    return getUsers($request, $response);
});
$app->get('/users/{id}', function($request, $response, $args) {
    return getUserById($request, $response, $args);
});
$app->get('/groups/{id}', function($request, $response, $args) {
    return getGroup($request, $response, $args);
});
$app->get('/categories', function($request, $response) {
    return getCategories($request, $response);
});
$app->get('/categories/{id}', function($request, $response, $args) {
    return getCategory($request, $response, $args);
});
$app->get('/attributedefinitions', function($request, $response) {
    return getAttributeDefinitions($request, $response);
});

////// POST
$app->post('/folder/{id}/move/{folderid}', function($request, $response, $args) {
    return moveFolder($request, $response, $args);
});
$app->post('/folder/{id}/createfolder', function($request, $response, $args) {
    return createFolder($request, $response, $args);
});
$app->post('/folder/{id}/document', function($request, $response, $args) {
    return uploadDocument($request, $response, $args);
});
$app->post('/document/{id}/attachment', function($request, $response, $args) {
    return uploadDocumentFile($request, $response, $args);
});
$app->post('/document/{id}/move/{folderid}', function($request, $response, $args) {
    return moveDocument($request, $response, $args);
});
$app->post('/users', function($request, $response) {
    return createUser($request, $response);
});
$app->post('/groups', function($request, $response) {
    return createGroup($request, $response);
});
$app->post('/categories', function($request, $response) {
    return createCategory($request, $response);
});

////// PUT
$app->put('/folder/{id}/document', function($request, $response, $args) {
    return uploadDocumentPut($request, $response, $args);
});
$app->put('/account/fullname', function($request, $response) {
    return setFullName($request, $response);
});
$app->put('/account/email', function($request, $response) {
    return setEmail($request, $response);
});
$app->put('/users/{id}/disable', function($request, $response, $args) {
    return setDisabledUser($request, $response, $args);
});
$app->put('/users/{id}/password', function($request, $response, $args) {
    return changeUserPassword($request, $response, $args);
});
$app->put('/groups/{id}/addUser', function($request, $response, $args) {
    return addUserToGroup($request, $response, $args);
});
$app->put('/groups/{id}/removeUser', function($request, $response, $args) {
    return removeUserFromGroup($request, $response, $args);
});
$app->put('/folder/{id}/setInherit', function($request, $response, $args) {
    return setFolderInheritsAccess($request, $response, $args);
});
$app->put('/folder/{id}/access/group/add', function($request, $response, $args) {
    return addGroupAccessToFolder($request, $response, $args);
});
$app->put('/folder/{id}/access/user/add', function($request, $response, $args) {
    return addUserAccessToFolder($request, $response, $args);
});
$app->put('/folder/{id}/access/group/remove', function($request, $response, $args) {
    return removeGroupAccessFromFolder($request, $response, $args);
});
$app->put('/folder/{id}/access/user/remove', function($request, $response, $args) {
    return removeUserAccessFromFolder($request, $response, $args);
});
$app->put('/folder/{id}/access/clear', function($request, $response, $args) {
    return clearFolderAccessList($request, $response, $args);
});
$app->put('/categories/{id}/name', function($request, $response, $args) {
    return changeCategoryName($request, $response, $args);
});
$app->put('/attributedefinitions/{id}/name', function($request, $response, $args) {
    return changeAttributeDefinitionName($request, $response, $args);
});

////// DELETE
$app->delete('/folder/{id}', function($request, $response, $args) {
    return deleteFolder($request, $response, $args);
});
$app->delete('/document/:id', function($request, $response, $args) {
    return deleteDocument($request, $response, $args);
});
$app->delete('/document/:id/categories', function($request, $response, $args) {
    return removeDocumentCategories($request, $response, $args);
});
$app->delete('/document/:id/category/:categoryId', function($request, $response, $args) {
    return removeDocumentCategory($request, $response, $args);
});
$app->delete('/users/:id', function($request, $response, $args) {
    return deleteUser($request, $response, $args);
});
$app->delete('/categories/:id', function($request, $response, $args) {
    return deleteCategory($request, $response, $args);
});

/////// ANY
$app->any('/echo', function($request, $response) {
    return echoData($request, $response);
});

$app->run();
?>