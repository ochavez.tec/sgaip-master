<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005 Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
$view = UI::factory($theme, $tmp[1], array('dms'=>$dms, 'user'=>$user));

/*if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}*/

$year = isset($_GET["year"]) ? $_GET["year"] : date("Y");

$active_document_sip = $dms->getAllActiveSIP();
$active_document_cc = $dms->getAllActiveCC();
$active_document_memo = $dms->getAllActiveMemo();


// ALL DOCUMENTS BY YEAR
$all_documets_by_year = $dms->getAllDocumentsByYear($year);

// ALL STATUS FOR DOCUMENTS
$sip_all_status = array();
$cc_all_status = array();
$memo_all_status = array();


//var_dump($active_document_cc);
$global_user_documents_sip = array();
$global_user_documents_cc = array();
$global_group_documents = array();

$sip_greens = array();
$sip_yellows = array();
$sip_reds = array();

$cc_greens = array();
$cc_yellows = array();
$cc_reds = array();

$memo_greens = array();
$memo_yellows = array();
$memo_reds = array();

$strtoday = date("Y-m-d H:i:s");

if($active_document_sip){

	// CHECK FOR USER ASSIGNED AND COUNT EVERY DOC
	$temp_user_id = "";	
	$temp_array = array();
	for($i=0; $i < (count($active_document_sip)); $i++) {
		if ($temp_user_id != $active_document_sip[$i]['user_id']) {
			if (!empty($temp_array)) {
				$global_user_documents_sip[] = $temp_array;			
			}

			$temp_array = array();
		}

		$temp_user_id = $active_document_sip[$i]['user_id'];		
		
		$temp_document = $dms->getDocument($active_document_sip[$i]['document_id']);
		$temp_document_created_at = $temp_document->getDate();
		$temp_document_year = date("Y", $temp_document_created_at);
		if ($temp_document_year == $year) {
			$temp_array[] = $active_document_sip[$i];
		}


		if ($i == (count($active_document_sip)-1)) {
			$global_user_documents_sip[] = $temp_array;
		}
	}

	foreach ($active_document_sip as $sip) {
		$document = $dms->getDocument($sip['document_id']);
		$document_created_at = $document->getDate();
		$document_year = date("Y", $document_created_at);

		$latestContent = $document->getLatestContent();
		$temp_status = $latestContent->getStatus();

		// NO REJECTED , NO PUBLISHED
		if($temp_status["status"] != -1 && $temp_status["status"] != 2){
			// El documento debe ser del año solicitado
			if ($document_year == $year) {
				
				//Calcular diferencia
				$diff = (int)$sip['end_date'] - time(); //time returns current time in seconds
				$days_remaining = floor($diff/(60*60*24)); //seconds/minute*minutes/hour*hours/day)

				//var_dump($days_remaining);

				if ( (int)$days_remaining >= 5){ // is green
					$sip_greens[] = $sip; 
				} else if ( (int)$days_remaining <= 4 && (int)$days_remaining >= 3 ){ // is yellow
					$sip_yellows[] = $sip;
				} else if ( (int)$days_remaining <= 2 ){ // is red
					$sip_reds[] = $sip;
				}
			}
		}
	}

	$green_sip = (empty($sip_greens)) ? 0 : count($sip_greens);
	$yellow_sip = (empty($sip_yellows)) ? 0 : count($sip_yellows);
	$red_sip = (empty($sip_reds)) ? 0 : count($sip_reds);

} else {
	$green_sip = 0;
	$yellow_sip = 0;
	$red_sip = 0;
}

if($active_document_cc){

	// CHECK FOR USER ASSIGNED AND COUNT EVERY DOC
	$temp_user_id = "";	
	$temp_array = array();
	for($i=0; $i < (count($active_document_cc)); $i++) {
		if ($temp_user_id != $active_document_cc[$i]['user_id']) {
			if (!empty($temp_array)) {
				$global_user_documents_cc[] = $temp_array;			
			}

			$temp_array = array();
		}

		$temp_user_id = $active_document_cc[$i]['user_id'];

		$temp_document = $dms->getDocument($active_document_cc[$i]['document_id']);
		$temp_document_created_at = $temp_document->getDate();
		$temp_document_year = date("Y", $temp_document_created_at);
		if ($temp_document_year == $year) {
			$temp_array[] = $active_document_cc[$i];
		}

		if ($i == (count($active_document_cc)-1)) {
			$global_user_documents_cc[] = $temp_array;
		}
	}

	foreach ($active_document_cc as $cc) {
		$document = $dms->getDocument($cc['document_id']);
		$document_created_at = $document->getDate();
		$document_year = date("Y", $document_created_at);

		$latestContent = $document->getLatestContent();
		$temp_status = $latestContent->getStatus();

		// NO REJECTED , NO PUBLISHED
		if($temp_status["status"] != -1 && $temp_status["status"] != 2){
			// El documento debe ser del año solicitado
			if ($document_year == $year) {
				
				//Calcular diferencia
				$diff = (int)$cc['end_date'] - time(); //time returns current time in seconds
				$days_remaining = floor($diff/(60*60*24)); //seconds/minute*minutes/hour*hours/day)

				if ( (int)$days_remaining >= 5){ // is green
					$cc_greens[] = $cc; 
				} else if ( (int)$days_remaining <= 4 && (int)$days_remaining >= 3 ){ // is yellow
					$cc_yellows[] = $cc;
				} else if ( (int)$days_remaining <= 2 ){ // is red
					$cc_reds[] = $cc;
				}

			}
		}
	}

	$green_cc = (empty($cc_greens)) ? 0 : count($cc_greens);
	$yellow_cc = (empty($cc_yellows)) ? 0 : count($cc_yellows);
	$red_cc = (empty($cc_reds)) ? 0 : count($cc_reds);

} else {
	$green_cc = 0;
	$yellow_cc = 0;
	$red_cc = 0;
}


if($active_document_memo){

	// CHECK FOR GROUP ASSIGNED AND COUNT EVERY DOC
	$temp_group_id = "";	
	$temp_array = array();
	for($i=0; $i < (count($active_document_memo)); $i++) {
		if ($temp_group_id != $active_document_memo[$i]['group_id']) {
			if (!empty($temp_array)) {
				$global_group_documents[] = $temp_array;			
			}

			$temp_array = array();
		}

		$temp_group_id = $active_document_memo[$i]['group_id'];

		$temp_document = $dms->getDocument($active_document_memo[$i]['document_id']);
		$temp_document_created_at = $temp_document->getDate();
		$temp_document_year = date("Y", $temp_document_created_at);
		if ($temp_document_year == $year) {
			$temp_array[] = $active_document_memo[$i];
		}

		if ($i == (count($active_document_memo)-1)) {
			$global_group_documents[] = $temp_array;
		}
	}

	foreach ($active_document_memo as $memo) {
		$document = $dms->getDocument($memo['document_id']);
		$document_created_at = $document->getDate();
		$document_year = date("Y", $document_created_at);

		$latestContent = $document->getLatestContent();
		$temp_status = $latestContent->getStatus();

		// NO REJECTED , NO PUBLISHED
		if($temp_status["status"] != -1 && $temp_status["status"] != 2){
			// El documento debe ser del año solicitado
			if ($document_year == $year) {
				
				//Calcular diferencia
				$diff = (int)$memo['end_date'] - time(); //time returns current time in seconds
				$days_remaining = floor($diff/(60*60*24)); //seconds/minute*minutes/hour*hours/day)

				//var_dump($days_remaining);

				if ( (int)$days_remaining >= 5){ // is green
					$memo_greens[] = $memo; 
				} else if ( (int)$days_remaining <= 4 && (int)$days_remaining >= 3 ){ // is yellow
					$memo_yellows[] = $memo;
				} else if ( (int)$days_remaining <= 2 ){ // is red
					$memo_reds[] = $memo;
				}

			}
		}
	}

	$green_memo = (empty($memo_greens)) ? 0 : count($memo_greens);
	$yellow_memo = (empty($memo_yellows)) ? 0 : count($memo_yellows);
	$red_memo = (empty($memo_reds)) ? 0 : count($memo_reds);

} else {
	$green_memo = 0;
	$yellow_memo = 0;
	$red_memo = 0;
}



// COUNT EVERY DOC STATUS
//$sip_category = $dms->getDocumentCategory(1); // Categoria Solicitud de información
//$cc_category = $dms->getDocumentCategory(15); // Categoria Consulta ciudadana
//$memo_category = $dms->getDocumentCategory(19); // Categoria Memo

if($all_documets_by_year){
	foreach ($all_documets_by_year as $doc_data) {

		$temp_document = $dms->getDocument($doc_data['id']);
		$latestContent = $temp_document->getLatestContent();
		
		if ($temp_document->hasCategory(1) || $temp_document->hasCategory(15) || $temp_document->hasCategory(19)) {

			$temp_status = $latestContent->getStatus();
			$temp_document_created_at = $temp_document->getDate();
			$temp_document_year = date("Y", $temp_document_created_at);
			if ($temp_document_year == $year) {

				// ALL DOCUMENTS IN THE ARRAY ALWAYS ARE IN WORKFLOW
			    if((int)$temp_status["status"] == S_IN_WORKFLOW) {
			        $workflowstate = $latestContent->getWorkflowState();
			        if ($workflowstate) {
			            $status_name = $workflowstate->getName();
			        }

			    } else if ((int)$temp_status['status'] == 2) {
				        
				        /* ---------------------------------------------------- */
				        $workflow_log = $latestContent->getWorkflowLog();
				        
				        if ($workflow_log AND !empty($workflow_log) AND (count($workflow_log) > 0)) {
				        	$workflow_log_weight = count($workflow_log);			        	

				        	$last_workflow_log_transition = $workflow_log[$workflow_log_weight-1];

				        	if (is_bool($last_workflow_log_transition->getTransition())) {
				        		$status_name = "Publicado";
				        	} else {
				        		$status_name = $last_workflow_log_transition->getTransition()->getNextState()->getName();
				        	}
				        	
				        } else {
				        	$status_name = "Publicado";
				        }			        
				        /* ---------------------------------------------------- */
				        
				} else if ((int)$temp_status['status'] == -1) {
				        /* ---------------------------------------------------- */
				        $workflow_log = $latestContent->getWorkflowLog();
				        
				        if ($workflow_log AND !empty($workflow_log) AND (count($workflow_log) > 0)) {
				        	$workflow_log_weight = count($workflow_log);			  

				        	$last_workflow_log_transition = $workflow_log[$workflow_log_weight-1];

				        	if (is_bool($last_workflow_log_transition->getTransition())) {
				        		$status_name = "Rechazado";
				        	} else {
				        		$status_name = $last_workflow_log_transition->getTransition()->getNextState()->getName();
				        	}

				        	$status_name = $last_workflow_log_transition->getTransition()->getNextState()->getName();
				        } else {
				        	$status_name = "Rechazado";
				        }			        
				        /* ---------------------------------------------------- */			    					  

			    } else {

			    	$status_name = "Sin estado";
			    }	   
			    	
			    if ($temp_document->hasCategory(1)) {
			    	if (isset($sip_all_status[$status_name]) AND !empty($sip_all_status[$status_name])) {
			    		$sip_all_status[$status_name] += 1;
			    	} else {
			    		$sip_all_status[$status_name] = 1;
			    	}
			    } 

				else if ($temp_document->hasCategory(15)) {
			    	if (isset($cc_all_status[$status_name]) AND !empty($cc_all_status[$status_name])) {
			    		$cc_all_status[$status_name] += 1;
			    	} else {
			    		$cc_all_status[$status_name] = 1;
			    	}
			    }

			    else if ($temp_document->hasCategory(19)) {
			    	if (isset($memo_all_status[$status_name]) AND !empty($memo_all_status[$status_name])) {
			    		$memo_all_status[$status_name] += 1;
			    	} else {
			    		$memo_all_status[$status_name] = 1;
			    	}
			    }		    

			}
		}
	}
}


$sip_enero = 0;
$sip_febrero = 0;
$sip_marzo = 0;
$sip_abril = 0;
$sip_mayo = 0;
$sip_junio = 0;
$sip_julio = 0;
$sip_agosto = 0;
$sip_septiembre = 0;
$sip_octubre = 0;
$sip_noviembre = 0;
$sip_diciembre = 0;

$cc_enero = 0;
$cc_febrero = 0;
$cc_marzo = 0;
$cc_abril = 0;
$cc_mayo = 0;
$cc_junio = 0;
$cc_julio = 0;
$cc_agosto = 0;
$cc_septiembre = 0;
$cc_octubre = 0;
$cc_noviembre = 0;
$cc_diciembre = 0;


// $sip_category = $dms->getDocumentCategory(1); // Categoria Solicitud de información
// $cc_category = $dms->getDocumentCategory(15); // Categoria Consulta ciudadana

if($all_documets_by_year){
	foreach ($all_documets_by_year as $doc_data) {

		$temp_document = $dms->getDocument($doc_data['id']);
		$latestContent = $temp_document->getLatestContent();

		if ($temp_document->hasCategory(1) || $temp_document->hasCategory(15)) {

			
			$temp_document_created_at = $temp_document->getDate();
			$temp_document_year = date("Y", $temp_document_created_at);
			if ($temp_document_year == $year) {

				if ($temp_document->hasCategory(1)) {
					switch (date("m", $temp_document_created_at)) {
						case '01':
							$sip_enero++;
							break;
						case '02':	
							$sip_febrero++;
							break;

						case '03':	
							$sip_marzo++;
							break;

						case '04':	
							$sip_abril++;
							break;

						case '05':	
							$sip_mayo++;
							break;

						case '06':	
							$sip_junio++;
							break;

						case '07':	
							$sip_julio++;
							break;

						case '08':	
							$sip_agosto++;
							break;

						case '09':	
							$sip_septiembre++;
							break;

						case '10':	
							$sip_octubre++;
							break;

						case '11':	
							$sip_noviembre++;
							break;

						case '12':	
							$sip_diciembre++;
							break;

						default:
							break;
					}
				} 

				else if ($temp_document->hasCategory(15)) {
					switch (date("m", $temp_document_created_at)) {
						case '01':
							$cc_enero++;
							break;
						case '02':	
							$cc_febrero++;
							break;

						case '03':	
							$cc_marzo++;
							break;

						case '04':	
							$cc_abril++;
							break;

						case '05':	
							$cc_mayo++;
							break;

						case '06':	
							$cc_junio++;
							break;

						case '07':	
							$cc_julio++;
							break;

						case '08':	
							$cc_agosto++;
							break;

						case '09':	
							$cc_septiembre++;
							break;

						case '10':	
							$cc_octubre++;
							break;

						case '11':	
							$cc_noviembre++;
							break;

						case '12':	
							$cc_diciembre++;
							break;

						default:
							break;
					}

				}
			}
		}
	}
}


$all_documents_sip_created = array(
	$year."-01" => $sip_enero,
	$year."-02" => $sip_febrero,
	$year."-03" => $sip_marzo,
	$year."-04" => $sip_abril,
	$year."-05" => $sip_mayo,
	$year."-06" => $sip_junio,
	$year."-07" => $sip_julio,
	$year."-08" => $sip_agosto,
	$year."-09" => $sip_septiembre,
	$year."-10" => $sip_octubre,
	$year."-11" => $sip_noviembre,
	$year."-12" => $sip_diciembre
);

$all_documents_cc_created = array(
	$year."-01" => $cc_enero,
	$year."-02" => $cc_febrero,
	$year."-03" => $cc_marzo,
	$year."-04" => $cc_abril,
	$year."-05" => $cc_mayo,
	$year."-06" => $cc_junio,
	$year."-07" => $cc_julio,
	$year."-08" => $cc_agosto,
	$year."-09" => $cc_septiembre,
	$year."-10" => $cc_octubre,
	$year."-11" => $cc_noviembre,
	$year."-12" => $cc_diciembre
);

/*var_dump($green_sip);
var_dump($yellow_sip);
var_dump($red_sip);*/
//exit;

if($view) {
	$view->setParam('strictformcheck', $settings->_strictFormCheck);
	$view->setParam('cachedir', $settings->_cacheDir);
	$view->setParam('previewWidthList', $settings->_previewWidthList);
	$view->setParam('workflowmode', $settings->_workflowMode);
	$view->setParam('timeout', $settings->_cmdTimeout);
	$view->setParam('year', $year);
	
	// sip counters
	$view->setParam('green_sip', $green_sip);
	$view->setParam('yellow_sip', $yellow_sip);
	$view->setParam('red_sip', $red_sip);

	// sip document arrays
	$view->setParam('documents_green_sip', $sip_greens);
	$view->setParam('documents_yellow_sip', $sip_yellows);
	$view->setParam('documents_red_sip', $sip_reds);

	// cc counters
	$view->setParam('green_cc', $green_cc);
	$view->setParam('yellow_cc', $yellow_cc);
	$view->setParam('red_cc', $red_cc);

	// cc document arrays
	$view->setParam('documents_green_cc', $cc_greens);
	$view->setParam('documents_yellow_cc', $cc_yellows);
	$view->setParam('documents_red_cc', $cc_reds);

	// memo counters
	$view->setParam('green_memo', $green_memo);
	$view->setParam('yellow_memo', $yellow_memo);
	$view->setParam('red_memo', $red_memo);

	// memo document arrays
	$view->setParam('documents_green_memo', $memo_greens);
	$view->setParam('documents_yellow_memo', $memo_yellows);
	$view->setParam('documents_red_memo', $memo_reds);
	

	$view->setParam('global_user_documents_sip', $global_user_documents_sip);
	$view->setParam('global_user_documents_cc', $global_user_documents_cc);
	$view->setParam('global_group_documents', $global_group_documents);
	
	$view->setParam('sip_all_status', $sip_all_status);
	$view->setParam('cc_all_status', $cc_all_status);
	$view->setParam('memo_all_status', $memo_all_status);
	

	$view->setParam('all_documents_sip_created', $all_documents_sip_created);
	$view->setParam('all_documents_cc_created', $all_documents_cc_created);

	$view($_GET);
}
