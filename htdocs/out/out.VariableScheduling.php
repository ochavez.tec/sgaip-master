<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005 Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));

$view = UI::factory($theme, $tmp[1], array('dms'=>$dms, 'user'=>$user));
if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}


$axes = $dms->getAllAxes();
if (is_bool($axes)) {
	UI::exitError(getMLText("admin_tools"),getMLText("internal_error"));
}

if (isset($_GET['selected_axe'])) {
	$axe = $_GET['selected_axe'];

	$instruments = $dms->getAxeMembers($axe);

} else {
	$instruments = array();
}

if (isset($_GET['selected_inst'])) {
	$instrument = $_GET['selected_inst'];

	$variables = $dms->getInstrumentVarsByInstrumentID($instrument);

} else {
	$variables = array();
}

if (isset($_GET['selected_inst'])) {
	$selected_inst = $_GET['selected_inst'];
} else {
	$selected_inst = false;
}

if (isset($_GET['selected_axe'])) {
	$selected_axe = $_GET['selected_axe'];
} else {
	$selected_axe = false;
}

if (isset($_GET['selected_var'])) {
	$selected_var = $_GET['selected_var'];
} else {
	$selected_var = false;
}

if (isset($_GET['selected_year'])) {
	$selected_year = $_GET['selected_year'];
} else {
	$selected_year = false;
}


if (isset($_GET['selected_axe']) && isset($_GET['selected_inst']) && isset($_GET['selected_var']) && isset($_GET['year'])) {
	$inst = $_GET['selected_inst'];
	$axe = $_GET['selected_axe'];
	$var = $_GET['selected_var'];
	$year = $_GET['year'];

	$result = $dms->getInstrumentVarScheduling($axe, $inst, $var, $year);

	if ((false !== $result) && !empty($result)) {
		$schedule = $result;
	} else {
		$schedule = array();
	}

} else {
	$schedule = array();
}

if($view) {

	$view->setParam('axes', $axes);
	$view->setParam('instruments', $instruments);
	$view->setParam('variables', $variables);
	$view->setParam('schedule', $schedule);

	$view->setParam('inst', $selected_inst);
	$view->setParam('var', $selected_var);
	$view->setParam('axe', $selected_axe);
	$view->setParam('year', $selected_year);
	
	$view->setParam('httproot', $settings->_httpRoot);
	$view->setParam('strictformcheck', $settings->_strictFormCheck);
	$view($_GET);
}
