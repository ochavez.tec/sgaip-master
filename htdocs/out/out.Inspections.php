<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005 Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));

$view = UI::factory($theme, $tmp[1], array('dms'=>$dms, 'user'=>$user));
if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

$inspections = $dms->getAllInspections();

if (is_bool($inspections)) {
	UI::exitError(getMLText("admin_tools"),getMLText("internal_error"));
}

if(isset($_GET['selected_inspection']) && $_GET['selected_inspection']) {
	$selected_inspection = $_GET['selected_inspection'];
	$inspection = $dms->getInspection($selected_inspection);	
}


$thematics = $dms->getAllThematics();
$all_thematics = array();
if (!empty($thematics) && is_array($thematics)) {
	foreach ($thematics as $idx => $thematic) {
		$items_by_thematic = $dms->getItemsByThematic($thematic['id']);
		$all_thematics[$idx] = $thematic;
		if (!empty($items_by_thematic) && is_array($items_by_thematic)) {
			$all_thematics[$idx]['items'] = $items_by_thematic;
		}

	}
}


if($view) {
	if(isset($selected_inspection)){
		$view->setParam('selected_inspection', $selected_inspection);
		$view->setParam('inspection', $inspection);
	}

	$view->setParam('all_thematics', $all_thematics);
	$view->setParam('inspections', $inspections);
	$view->setParam('httproot', $settings->_httpRoot);
	$view->setParam('strictformcheck', $settings->_strictFormCheck);
	$view($_GET);
}
