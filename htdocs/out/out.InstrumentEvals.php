<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005 Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
//var_dump($tmp[1]);
$view = UI::factory($theme, $tmp[1], array('dms'=>$dms, 'user'=>$user));
if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

$instruments = $dms->getAllInstruments('name');
if (is_bool($instruments)) {
	UI::exitError(getMLText("admin_tools"),getMLText("internal_error"));
}

$evaldates = $dms->getAllEvalDates();

$axes = $dms->getAllAxes();
if (is_bool($axes)) {
	UI::exitError(getMLText("admin_tools"),getMLText("internal_error"));
}

if(isset($_GET['seleval']) && $_GET['seleval']) {
	$seleval = $_GET['seleval'];
	$allevals = $dms->getInstrumentEvalsByDate($seleval);
} else {
	$allevals = '';
}

//var_dump($evaldates);

if($view) {
	if(isset($seleval))
		$view->setParam('seleval', $seleval);
	$view->setParam('allinstruments', $instruments);
	$view->setParam('allevals', $allevals);
	$view->setParam('evaldates', $evaldates);
	$view->setParam('allaxes', $axes);
	$view->setParam('httproot', $settings->_httpRoot);
	$view->setParam('strictformcheck', $settings->_strictFormCheck);
	$view($_GET);
}
