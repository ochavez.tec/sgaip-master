<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005 Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
$view = UI::factory($theme, $tmp[1], array('dms'=>$dms, 'user'=>$user));
if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}

$all_date_rules = $dms->getAllDateRules();
if (is_bool($all_date_rules) || empty($all_date_rules)) {
	$all_date_rules = false;
}

$extension_time = $dms->getExtensionTime(1);
if(is_bool($extension_time) || empty($extension_time)){
	$extension_time = false;
}

/*
$allDepartments = $dms->getAllDepartments();
if (is_bool($allDepartments)) {
	UI::exitError(getMLText("admin_tools"),getMLText("internal_error"));
}

/*if(isset($_GET['depid']) && $_GET['depid']) {
	$seldep = $dms->getDepartment($_GET['depid']);
} else {
	$seldep = null;
}*/

if($view) {
	//$view->setParam('seldep', $seldep);
	//$view->setParam('alldeps', $allDepartments);
	$view->setParam('alldaterules', $all_date_rules);
	$view->setParam('extension_time', $extension_time);
	$view->setParam('strictformcheck', $settings->_strictFormCheck);
	$view->setParam('cachedir', $settings->_cacheDir);
	$view->setParam('previewWidthList', $settings->_previewWidthList);
	$view->setParam('workflowmode', $settings->_workflowMode);
	$view->setParam('timeout', $settings->_cmdTimeout);
	$view($_GET);
}
