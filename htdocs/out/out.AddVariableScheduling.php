<?php
//    MyDMS. Document Management System
//    Copyright (C) 2002-2005 Markus Westphal
//    Copyright (C) 2006-2008 Malcolm Cowe
//    Copyright (C) 2010 Matteo Lucarelli
//    Copyright (C) 2010-2016 Uwe Steinmann
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

include("../inc/inc.Settings.php");
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.Init.php");
include("../inc/inc.Extension.php");
include("../inc/inc.DBInit.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.Authentication.php");

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));

$view = UI::factory($theme, $tmp[1], array('dms'=>$dms, 'user'=>$user));
if (!$user->isAdmin()) {
	UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
}


$axes = $dms->getAllAxes();
if (is_bool($axes)) {
	UI::exitError(getMLText("admin_tools"),getMLText("internal_error"));
}

if (isset($_GET['selected_axe'])) {
	$axe = $_GET['selected_axe'];
	$instruments = $dms->getAxeMembers($axe);
} else {
	$instruments = array();
}

if (isset($_GET['selected_instrument'])) {
	$instrument = $_GET['selected_instrument'];
	$variables = $dms->getInstrumentVarsByInstrumentID($instrument);
	$selected_instrument = $dms->getInstrument($instrument);

} else {
	$variables = array();
	$selected_instrument = false;
}

if (isset($_GET['selected_variable'])) {
	$variable = $_GET['selected_variable'];
	$selected_variable = $dms->getInstrumentVar($variable);
} else {
	$selected_variable = false;
}


if (isset($_GET['scheduleid'])) {
	$schedule = $dms->getInstrumentVarSchedulingByID($_GET['scheduleid']);

} else {
	$schedule = false;
	$instrument = 0;
	$axe = 0;
}

$allGroups = $dms->getAllGroups();
if (is_bool($allGroups)) {
	UI::exitError(getMLText("admin_tools"),getMLText("no_groups_founded"));
}

// For groups array
if(isset($_GET['groupid']) && $_GET['groupid']) {
	$selgroup = $dms->getGroup($_GET['groupid']);
} else {
	$selgroup = null;
}

// For categories array
if(isset($_GET['selected_group']) && $_GET['selected_group']) {
	$group = $dms->getGroup($_GET['selected_group']);
	$thecategories = $group->getCategoriesListTwo();

	if (!empty($thecategories)) {
	 	$categories = $thecategories;
	} else {
		$categories = array();
	} 

} else {
	$categories = array();
}

// For types 
$types = $dms->getDocumentTypes();
if(isset($_GET['typeid']) && $_GET['typeid']) {
	$seltype = $dms->getDocumentType($_GET['typeid']);
} else {
	$seltype = null;
}

if (isset($_GET['selected_category'])) {
	$types_category = $dms->getTypesByCategoryID($_GET['selected_category']);

	if ($types_category) {
		$types_by_category = $types_category;
	} else {
		$types_by_category = array();
	}

} else {
	$types_by_category = array();
}

if($view) {

	$view->setParam('axes', $axes);
	$view->setParam('instruments', $instruments);
	$view->setParam('variables', $variables);
	$view->setParam('selgroup', $selgroup);
	$view->setParam('allgroups', $allGroups);
	$view->setParam('types', $types);
	$view->setParam('types_by_category', $types_by_category);
	$view->setParam('seltype', $seltype);
	$view->setParam('schedule', $schedule);
	$view->setParam('categories', $categories);

	$view->setParam('selected_instrument', $selected_instrument);
	$view->setParam('selected_variable', $selected_variable);

	$view->setParam('httproot', $settings->_httpRoot);
	$view->setParam('strictformcheck', $settings->_strictFormCheck);
	$view($_GET);
}
