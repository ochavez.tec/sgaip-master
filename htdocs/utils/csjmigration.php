<?php
include("../inc/inc.ClassSettings.php");
setlocale(LC_CTYPE, "en_US.UTF-8");

######################### INICIO CONFIGURACI√ìN ################################

$original_path = "C:\\inetpub\\wwwroot\\Filemaster\\InformacionGeneral\\Documentacion\\";
//$source_path = "/Volumes/MULTIBOOT/InformacionGeneral/Documentacion/";
$source_path = "/home/admin-dis/Documentacion/";

$mssql_config = [
	'server' => '10.0.0.72',
	'username' => 'sa',
	'password' => 'Sa123456',
	'port' => '1433',
	'database' => 'FM_ES_Transparencia',
];

/*
$original_path = "C:\\inetpub\\wwwroot\\Filemaster\\InformacionGeneral\\Documentacion\\";
//$source_path = "/Volumes/MULTIBOOT/InformacionGeneral/Documentacion/";
$source_path = "/Users/herson/Downloads/Documentacion/";

$mssql_config = [
	'server' => '52.73.42.18',
	'username' => 'sa',
	'password' => 'M!th0l0gy2018',
	'port' => '1433',
	'database' => 'FM_ES_Transparencia',
];
*/


############################ FIN CONFIGURACI√ìN ################################
$skip_attrs = ['Nombre del Documento'];

$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );

// Dirección Financiera 144
$dirFinanName = 'Dirección Financiera Institucional';
$dirFinanMapping = [
	'Documento|Contratos' => 221,
	'Documento|Ley de salarios' => 221,
	'Documento|Presupuesto Votado vigente' => 222,
	'Documento|Estado de Rendición económica' => 222,
	'Documento|Estado de Situación Financiera' => 222,
	'Documento|Por Línea de trabajo' => 223,
	'Documento|Por proyecto de Inversión' => 223,
	'Documento|Por rubro de agrupación' => 223,
	'Documento|Por unidad presupuestaria, Línea de trabajo y rubro de agrupación' => 223,
];

// Secretaría General 152
$secGerName = 'Secretaría General';
$secGerMapping = [
	'Documento|ACTAS' => [
		'root'         => 172, 
	 	'Periodo|2011' => 181, 
	 	'Periodo|2012' => 180, 
	 	'Periodo|2013' => 179, 
	 	'Periodo|2014' => 178, 
	 	'Periodo|2015' => 177, 
	 	'Periodo|2016' => 176, 
	 	'Periodo|2017' => 175, 
	 	'Periodo|2018' => 174
	 ],
	'Documento|AGENDAS' => [
		'root'         => 173, 
	 	'Periodo|2011' => 189, 
	 	'Periodo|2012' => 188, 
	 	'Periodo|2013' => 187, 
	 	'Periodo|2014' => 186, 
	 	'Periodo|2015' => 185, 
	 	'Periodo|2016' => 184, 
	 	'Periodo|2017' => 183, 
	 	'Periodo|2018' => 182
	 ],
	'Documento|JUECES/ZAS' => [
		'root'                      => 190, 
	 	'Departamento|San Salvador' => 191, 
	 	'Departamento|San Miguel '  => 193, 
	 	'Departamento|Santa Ana'    => 194, 
	 	'Departamento|Ahuachapán'   => 195, 
	 	'Departamento|Cabañas'      => 196, 
	 	'Departamento|La Libertad'  => 192, 
	 	'Departamento|Sonsonate'    => 200, 
	 	'Departamento|Chalatenango' => 199,
	 	'Departamento|Cuscatlán'    => 198,
	 	'Departamento|La Paz'       => 197,
	 	'Departamento|La Unión'     => 204,
	 	'Departamento|Morazán'      => 203,
	 	'Departamento|Usulután'     => 202,
	 	'Departamento|San Vicente'  => 201
	 ],
	'Documento|MAGISTRADOS/AS DE CAMARA' => [
		'root'                      => 205, 
	 	'Departamento|San Salvador' => 206, 
	 	'Departamento|San Miguel '  => 207, 
	 	'Departamento|Santa Ana'    => 208, 
	 	'Departamento|Ahuachapán'   => 209, 
	 	'Departamento|Cabañas'      => 210, 
	 	'Departamento|La Libertad'  => 211, 
	 	'Departamento|Sonsonate'    => 212, 
	 	'Departamento|Chalatenango' => 213,
	 	'Departamento|Cuscatlán'    => 214,
	 	'Departamento|La Paz'       => 215,
	 	'Departamento|La Unión'     => 216,
	 	'Departamento|Morazán'      => 217,
	 	'Departamento|Usulután'     => 218,
	 	'Departamento|San Vicente'  => 219
	 ],
];

// Direccion de Adquisiciones y Contrataciones Institucional 135
$dirAdqName = 'Direccion de Adquisiciones y Contrataciones Institucional';
$dirAdqMapping = [
	'Documento|04-Contratos' => [
		'root'         => 225, 
	 	'Periodo|2011' => 235, 
	 	'Periodo|2012' => 234, 
	 	'Periodo|2013' => 233, 
	 	'Periodo|2014' => 232, 
	 	'Periodo|2015' => 230, 
	 	'Periodo|2016' => 229, 
	 	'Periodo|2017' => 228, 
	 	'Periodo|2018' => 227
	 ],
	'Documento|03-Informes' => [
		'root'         => 226, 
	 	'Periodo|2011' => 236, 
	 	'Periodo|2012' => 237, 
	 	'Periodo|2013' => 238, 
	 	'Periodo|2014' => 239, 
	 	'Periodo|2015' => 240, 
	 	'Periodo|2016' => 241, 
	 	'Periodo|2017' => 242, 
	 	'Periodo|2018' => 243
	 ]
];

$destinationFolders = [$dirFinanName, $secGerName, $dirAdqName];

function usage() { /* {{{ */
	echo "Usage:\n";
	echo "  seeddms-csjmigration [--config <file>] [-h] [-d] [-v] -F <migration folder id>\n";
	echo "\n";
	echo "Description:\n";
	echo "  This program creates a new folder in SeedDMS.\n";
	echo "\n";
	echo "Options:\n";
	echo "  -h, --help: print usage information and exit.\n";
	echo "  -d, --dry-run: run and don't migrate anything (just print messages).\n";
	echo "  -v, --version: print version and exit.\n";
	echo "  --config: set alternative config file.\n";
	echo "  -F <migration folder id>: id of migration folder\n";
} /* }}} */

function process_document($doc) {
	global $unwanted_array, $original_path, $source_path;
	echo "Processing document ".$doc['DocumentID']."\n";
	$nameArray = explode('.', $doc['NombreArchivo']);
	$docExtension = end($nameArray);
	
	$doc['NombreArchivo'] = strtr($doc['NombreArchivo'], $unwanted_array);
	
	$filename = str_replace($original_path, $source_path, $doc['Direccion']);
	$filename .= utf8_decode('/'.$doc['FolderID'].'/'. preg_replace("/[^a-zA-Z0-9\_\-\.]/i", '', $doc['NombreArchivo']));
	$fnEnconding = mb_detect_encoding($filename);
	echo "Filename encoding: $fnEnconding \n";
	
	//$filename = mb_convert_encoding($filename, 'CP1252', $fnEnconding);
	//$filename = iconv($fnEnconding, 'UTF-8', $filename);
	
	if(is_readable($filename)) {
		if(filesize($filename)) {
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			if(!isset($mimetype)) {
				$mimetype = $finfo->file($filename);
				echo "MIME/TYPE: $mimetype \n";
			}
			$filetype = "." . pathinfo($filename, PATHINFO_EXTENSION);
			$foundfile = true;
		} else {
			echo "File $filename has zero size\n";
			$foundfile = false;
//				exit(1);
		}
	} else {
		echo "File $filename is not readable\n";
		$foundfile = false;
//			exit(1);
	}

	if(!isset($name))
		$name = basename($filename);
	$filetmp = $filename;
	
	return [
		'filename' => $filename, 
	    'mimetype' => $mimetype, 
	    'foundfile' => $foundfile, 
	    'docExtension' => $docExtension, 
	    'name' => $name, 
	    'filetmp' => $filetmp,
	    'filetype' => $filetype
	    ];
}


$version = "0.0.1";
$shortoptions = "F:c:n:hvd";
$longoptions = array('help', 'version', 'config:', 'dry-run');
$res = false;
if(false === ($options = getopt($shortoptions, $longoptions))) {
	usage();
	exit(0);
}

/* Print help and exit */
if(isset($options['h']) || isset($options['help'])) {
	usage();
	exit(0);
}

/* Print version and exit */
if(isset($options['v']) || isset($options['version'])) {
	echo $version."\n";
	exit(0);
}

/* Set alternative config file */
if(isset($options['config'])) {
	$settings = new Settings($options['config']);
} else {
	$settings = new Settings();
}

if(isset($settings->_extraPath))
	ini_set('include_path', $settings->_extraPath. PATH_SEPARATOR .ini_get('include_path'));

require_once("SeedDMS/Core.php");
require_once("mssql.php");

if(isset($options['d'])  || isset($options['dry-run'])) {
	$dryRun = true;
} else {
	$dryRun = false;
}

if(isset($options['F'])) {
	$folderid = (int) $options['F'];
} else {
	echo "Missing migration folder ID\n";
	usage();
	exit(1);
}

$comment = '';

$sequence = 0;

$categories = array();

$name = '';
if(isset($options['n'])) {
	$name = $options['n'];
}

$reqversion = 0;

if($reqversion<1)
	$reqversion=1;
	
$version_attributes = array();
$version_comment = '';

$mssql_db = New \mssql();
$mssql_db->connect($mssql_config) or die ("Could not connect to db-server \"" . $mssql_config["server"] . "\"");;

$db = new SeedDMS_Core_DatabaseAccess($settings->_dbDriver, $settings->_dbHostname, $settings->_dbUser, $settings->_dbPass, $settings->_dbDatabase);
$db->connect() or die ("Could not connect to db-server \"" . $settings->_dbHostname . "\"");
//$db->_conn->debug = 1;

$dms = new SeedDMS_Core_DMS($db, $settings->_contentDir.$settings->_contentOffsetDir);
if(!$dms->checkVersion()) {
	echo "Database update needed.";
	exit;
}

$dms->setRootFolderID($settings->_rootFolderID);
$dms->setMaxDirID($settings->_maxDirID);
$dms->setEnableConverting($settings->_enableConverting);
$dms->setViewOnlineFileTypes($settings->_viewOnlineFileTypes);

$user = $dms->getUser(1);

$migrationFolder = $dms->getFolder($folderid);

if (!is_object($migrationFolder)) {
	echo "Could not find specified folder\n";
	exit(1);
}

if ($migrationFolder->getAccessMode($user) < M_READWRITE) {
	echo "Not sufficient access rights\n";
	exit(1);
}

$groupsQuery = "SELECT DISTINCT RTRIM(LTRIM(REPLACE(REPLACE(Nombre, '- Responsable', ''),'- Operador', ''))) Nombre FROM Grupo";
$groups = $mssql_db->getQuery($groupsQuery);
$countGroups = $mssql_db->numRows($groups);
$groupSequence = 0;
$groupComment = '';
$groupsAdded = array();

echo "Groups found: $countGroups \n";
echo "Starting processing Groups...\n";
for( $g = 1; $g <= $countGroups; $g++) {
	$group = $mssql_db->row($groups);
	
	$groupName = $group['Nombre'];
	
	$exists = $dms->getFolderByName($groupName, $migrationFolder);
	
	if(!$exists) {
		echo "Creating folder with name [".$groupName."]\n";
//		if(!$dryRun) {
		$res = $migrationFolder->addSubFolder($groupName, $groupComment, $user, $groupSequence);
//		}
	} else {
		echo "Folder with name [".$groupName."] already exists!\n";
		$res = $exists;
	}
	
	if (is_bool($res) && !$res) {
		echo "Could not add group folder: ".$groupName."\n";
		exit(1);
	} else {
		$groupsAdded[] = $res;
	}
}

$foldersQuery = "
    SELECT RTRIM(LTRIM(REPLACE(REPLACE(g.Nombre, '- Responsable', ''),'- Operador', ''))) Grupo, e.*
      FROM Expediente e
INNER JOIN UsuarioGrupo ug
        ON ug.IdUsuario = e.IdUsuarioCreador
INNER JOIN Grupo g
        ON g.IdGrupo = ug.IdGrupo
     WHERE e.Borrado = 0
  ORDER BY 1,2;
";

$folders = $mssql_db->getQuery($foldersQuery);

$countFolder = $mssql_db->numRows($folders);

echo "Folders found: $countFolder \n";
echo "Starting processing Folders...\n";
for( $i = 1; $i <= $countFolder; $i++ ) { // EXPEDIENTES
	echo "############################ INICIO EXPEDIENTE ############################\n";
	unset($expKey);
	unset($docAttachments);
	$docAttachments = array();
	$folder = $mssql_db->row($folders);
	
	$newFolderName = $folder['Grupo'];
	$newFolder = $folder["IdExpediente"];

	$comment = '';
	$sequence = 0;
	
	//$expFolderID = array_search($newFolderName, $groupsAdded);
	
	//$expFolder = $groupsAdded[$expFolderID];
	$expFolder = $dms->getFolderByName($newFolderName, $migrationFolder);
	
	echo "expFolder: ".$expFolder->getName()." \n";
	
	//$newFolderObj = $dms->getFolderByName($expFolder);
	$newFolderObj = $expFolder;
	
//	echo "Current folder: ".$newFolderObj->getName()."\n";
/*
	$exists = $dms->getFolderByName($newFolderName, $migrationFolder);
	
	if(!$exists) {
		echo "Creating folder with name: ".$newFolder."\n";
		$newFolderObj = $expFolder->addSubFolder($newFolder, $comment, $user, $sequence);
	} else {
		echo "Folder with name: ".$newFolder." already exists!\n";
		$newFolderObj = $exists;
	}
//	$newFolderID = $newFolder;
	
	if (is_bool($newFolderObj) && !$newFolderObj) {
		echo "Could not add folder\n";
		exit(1);
	}
*/
	
	$docsQuery = "
    SELECT ed.IdExpedienteDocumento DocumentID, 
           ed.IdExpediente FolderID, 
           ed.IdUsuario,
           RTRIM(LTRIM(REPLACE(REPLACE(g.Nombre, '- Responsable', ''),'- Operador', ''))) Grupo,
           ed.NombreArchivo,
           ed.Direccion,
           ed.Fecha,
           ed.TipoArchivo,
           ed.Comentario,
           ed.Publicado,
           ed.FechaPublicacion
      FROM ExpedienteDocumento ed
INNER JOIN UsuarioGrupo ug
        ON ug.IdUsuario = ed.IdUsuario
INNER JOIN Grupo g
        ON g.IdGrupo = ug.IdGrupo
     WHERE ed.IdExpediente = $newFolder;
	";
	
//	echo "docsQuery: $docsQuery \n";
	
	$folderDocs = $mssql_db->getQuery($docsQuery);
	$countDoc = $mssql_db->numRows($folderDocs);
	$foundMainFile = false;
	$foundAttachment = false;
	$docObj = new StdClass();
	echo "Documents found: $countDoc \n";

	for( $j = 1; $j <= $countDoc; $j++ ) { // DOCUMENTOS
		echo "############################ INICIO DOCUMENTO ############################\n";
		unset($key);
		unset($subFolder);
		unset($filename);
		unset($nameArray);
		unset($docExtension);

		$attachObj = new StdClass();
		
		$doc = $mssql_db->row($folderDocs);

		$docReturn = process_document($doc);
		
		extract($docReturn);
		
//		var_dump($docReturn);
		
		if($foundfile) {
			if(strtolower($docExtension) == 'pdf') {
				$foundMainFile = true;
				echo "Found Main File (PDF)! \n";
				$docObj->id 		   = $doc['DocumentID'];
				$docObj->folderId 	   = $doc['FolderID'];
				$docObj->name 		   = $doc['NombreArchivo'];
				$docObj->comment 	   = json_encode([ 'legacyID' 	=> $doc['DocumentID']]);
				$docObj->date 	       = $doc['Fecha']->format('U');
				$docObj->expires 	   = 0;
				$docObj->owner 		   = $dms->getUser($doc['IdUsuario']);
		//		$docObj->folder 	   = $newFolderObj->getID();
				$docObj->ingeritAccess = 1;
				$docObj->defaulAccess  = 2;
				$docObj->locked        = -1;
				$docObj->keywords 	   = '';
				$docObj->sequence 	   = 0;
				
				$docObj->filename = $filename;
				$docObj->mimetype = $mimetype;
				$docObj->foundfile = $foundfile;
				$docObj->docExtension = $docExtension;
				$docObj->filetmp = $filetmp;
				$docObj->filetype = $filetype;
				
				if(is_null($docObj->owner)) $docObj->owner = $dms->getUser(13);
		
				$attrQuery = "
		SELECT DISTINCT cf.Etiqueta, 
		      COALESCE((SELECT RTRIM(LTRIM(CAST(va.Valor AS NVARCHAR(150)))) 
		                  FROM ValorAutonumerico va 
		                 WHERE cf.IdCampoFicha = va.IdCampoFicha 
		                   AND va.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vb.Valor AS NVARCHAR(150)))) 
		                  FROM ValorBinario vb 
		                 WHERE cf.IdCampoFicha = vb.IdCampoFicha 
		                   AND vb.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vc.Valor AS NVARCHAR(150)))) 
		                  FROM ValorCalculado vc 
		                 WHERE cf.IdCampoFicha = vc.IdCampoFicha 
		                   AND vc.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vcm.Valor AS NVARCHAR(150)))) 
		                  FROM ValorComentario vcm 
		                 WHERE cf.IdCampoFicha = vcm.IdCampoFicha 
		                   AND vcm.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vcp.Valor AS NVARCHAR(150)))) 
		                  FROM ValorCompuesto vcp 
		                 WHERE cf.IdCampoFicha = vcp.IdCampoFicha 
		                   AND vcp.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vd.Valor AS NVARCHAR(150)))) 
		                  FROM ValorDecimal vd 
		                 WHERE cf.IdCampoFicha = vd.IdCampoFicha 
		                   AND vd.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vf.Valor AS NVARCHAR(150)))) 
		                  FROM ValorFecha vf 
		                 WHERE cf.IdCampoFicha = vf.IdCampoFicha 
		                   AND vf.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vh.Valor AS NVARCHAR(150)))) 
		                  FROM ValorHora vh 
		                 WHERE cf.IdCampoFicha = vh.IdCampoFicha 
		                   AND vh.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vi.Valor AS NVARCHAR(150)))) 
		                  FROM ValorImagen vi 
		                 WHERE cf.IdCampoFicha = vi.IdCampoFicha 
		                   AND vi.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(el.Etiqueta AS NVARCHAR(150)))) 
		                  FROM ValorLista vl 
		            INNER JOIN EntradaLista el 
		                    ON el.IdEntradaLista = vl.IdEntradaLista 
		                 WHERE cf.IdCampoFicha = vl.IdCampoFicha 
		                   AND vl.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vn.Valor AS NVARCHAR(150)))) 
		                  FROM ValorNota vn 
		                 WHERE cf.IdCampoFicha = vn.IdCampoFicha 
		                   AND vn.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vnu.Valor AS NVARCHAR(150)))) 
		                  FROM ValorNumerico vnu 
		                 WHERE cf.IdCampoFicha = vnu.IdCampoFicha 
		                   AND vnu.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vtc.Valor AS NVARCHAR(150)))) 
		                  FROM ValorTablaComentario vtc 
		                 WHERE cf.IdCampoFicha = vtc.IdCampoFicha 
		                   AND vtc.IdExpediente = ".$docObj->folderId."),
		               (SELECT RTRIM(LTRIM(CAST(vt.Valor AS NVARCHAR(150)))) 
		                  FROM ValorTexto vt 
		                 WHERE cf.IdCampoFicha = vt.IdCampoFicha 
		                   AND vt.IdExpediente = ".$docObj->folderId.")
		              ) AS Valor 
		  FROM CampoFicha cf
		ORDER BY 2 DESC;
				";
		
				$docAttrs = $mssql_db->getQuery($attrQuery);
		
				$countDocAttr = $mssql_db->numRows($docAttrs);
				$documentAttributes = array();
				$versionAttributes = array();
				$versionComment = '';
				$hasAttrDocumento = false;
				$hasAttrPeriodo = false;
				$hasAttrDepartamento = false;
				unset($documentoKey);
				unset($periodoKey);
				unset($departamentoKey);
				
				unset($documentAttributes);
				for( $k = 1; $k <= $countDocAttr; $k++ ) {
					unset($attrKey);
					unset($attrVal);
					unset($attr);
					unset($attrdef);
					
					$attr = $mssql_db->row($docAttrs);
					
		//			var_dump($attr);
		
					$attrKey = $attr['Etiqueta'];
					$attrVal = $attr['Valor'];
					
					//echo "ATRIBUTO: $attrKey => $attrVal \n";
					
					if($attrKey == 'Nombre del Documento') {
						$docObj->name = $attrVal;
					}
				
	//				echo "newFolderName: $newFolderName \n";	
	//				echo "destinationFolders: \n";	
	//				var_dump($destinationFolders);
					if(in_array($newFolderName, $destinationFolders)) {
						if($attrKey == 'Documento') {
							$hasAttrDocumento = true;
							$documentoKey = $attrKey.'|'.$attrVal;
							echo "documentoKey: $documentoKey \n";
						}
		
						if($attrKey == 'Periodo') {
							$hasAttrPeriodo = true;
							$periodoKey = $attrKey.'|'.$attrVal;
							echo "periodoKey: $periodoKey \n";
						}			
		
						if($attrKey == 'Departamento') {
							$hasAttrDepartamento = true;
							$departamentoKey = $attrKey.'|'.$attrVal;
							echo "departamentKey: $departamentoKey \n";
						}
					}
					
					if($attrVal == null) break 1; // Skip the rest of null values
		
					if(!in_array($attr['Etiqueta'], $skip_attrs)) {
						$attrdef = $dms->getAttributeDefinitionByName($attrKey);
					
						if (!$attrdef) {
							echo "Document attribute [$attrKey] unknown\n";
			//				exit(1);
						} else {
							echo "Document attribute [$attrKey => $attrVal] found!\n";
							$documentAttributes[$attrdef->getID()] = $attrVal;
						}
					}
				}
			
			} else { // END IF PDF, ANY OTHER EXTENSION IS AN ATTACHMENT
				$foundAttachment = true;
				echo "Found attachment! \n";
				$attachObj->name        = $doc['NombreArchivo'];
				$attachObj->comment     = '';
				$attachObj->user        = $dms->getUser($doc['IdUsuario']);
				$attachObj->tmpFile     = $filetmp;
				$attachObj->orgFileName = $doc['NombreArchivo'];
				$attachObj->fileType    = $filetype;
				$attachObj->mimeType    = $mimetype;
				$attachObj->version     = 0;
				$attachObj->published   = 0;
				
				if(is_null($attachObj->user)) $attachObj->user = $dms->getUser(13);
				
				$docAttachments[] = $attachObj;
				unset($attachObj);
			}
		}

		$reviewers = array();
		$approvers = array();
		
	echo "############################ FIN DOCUMENTO ############################\n";
	} // END FOR DOCUMENTS
	
	
	//var_dump($docObj);
	echo "foundMainFile: ";
	echo $foundMainFile ? 'true' : 'false';
	echo "\n";
	if(isset($docObj->name) && $foundMainFile) {
		unset($destinationFolderID);
		// Finding destination folder
		switch($newFolderName) {
			case $dirFinanName:
				if($hasAttrDocumento) {
					$destinationFolderID = $dirFinanMapping[$documentoKey];
				}
			break;
			case $secGerName:
				if($hasAttrDocumento && ($hasAttrPeriodo || $hasAttrDepartamento)) {
					$destinationFolderID = $dirFinanMapping[$documentoKey][$periodoKey];
				} elseif($hasAttrDocumento) {
					$destinationFolderID = $dirFinanMapping[$documentoKey]['root'];
				}
			break;
			case $dirAdqName:
				if($hasAttrDocumento && $hasAttrPeriodo) {
					$destinationFolderID = $dirAdqMapping[$documentoKey][$periodoKey];
				} elseif($hasAttrDocumento) {
					$destinationFolderID = $dirAdqMapping[$documentoKey]['root'];
				}
			break;
		}
		
		if(isset($destinationFolderID)) {
			$newFolderObj = $dms->getFolder($destinationFolderID);
		}
		
		// Check if document exists
		$exists = $dms->getDocumentByName($docObj->name, $newFolderObj);
		
		// If not, add it!
		if(!$exists) {
		
			echo "Creating document with id: ".$docObj->id." and name: ".$docObj->name." in folder: ".$newFolderObj->getID()." \n";
			if(!$dryRun) {
				$res = $newFolderObj->addDocument($docObj->name, $docObj->comment, $docObj->expires, $docObj->owner, $docObj->keywords,
				                            $categories, $docObj->filetmp, basename($docObj->filename),
				                            $docObj->filetype, $docObj->mimetype, $sequence, $reviewers,
				                            $approvers, $reqversion, $versionComment,
				                            $documentAttributes, $versionAttributes);
				$newDocObj = $res[0];
			}
			
			foreach($docAttachments as $docAttach) {
				echo "Adding attachment: ".$docAttach->name." for document with name: ".$docObj->name."\n";
				if(!$dryRun) {
					$newDocObj->addDocumentFile($docAttach->name, $docAttach->comment, $docAttach->user, $docAttach->tmpFile, $docAttach->orgFileName, $docAttach->fileType, $docAttach->mimeType, $docAttach->version, $docAttach->published);
				}
			}
			unset($docAttachments);
			
		} else {
			echo "Document with id: ".$docObj->id." and name: ".$docObj->name." already exists in folder: ".$newFolderObj->getID()."\n";
			$res = $exists;
		}
	
		// Check if creation was OK!
		if (is_bool($res) && !$res) {
			echo "Could not add document to folder\n";
			var_dump($res);
		}
		
/*
		foreach($docAttachments as $docAttach) {
			if(isset($newDocObj)) {
				$newDocObj->addDocumentFile($docAttach->name, $docAttach->comment, $docAttach->user, $docAttach->tmpFile, $docAttach->orgFileName, $docAttach->fileType, $docAttach->mimeType, $docAttach->version, $docAttach->published);
			}
		}
*/
	} else {
		echo "Name not set or MAIN file not found!\n";
		var_dump($docObj);
	}
	
	echo "############################ FIN EXPEDIENTE ############################\n";

} // END FOR FOLDERS
?>

