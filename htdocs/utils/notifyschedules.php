<?php
include("../inc/inc.ClassSettings.php");

function usage() { /* {{{ */
	echo "Usage:\n";
	echo "  seeddms-notifyschedules [--config <file>] [-h] [-v]\n";
	echo "\n";
	echo "Description:\n";
	echo "  This program sends notifications created in SeedDMS.\n";
	echo "\n";
	echo "Options:\n";
	echo "  -h, --help: print usage information and exit.\n";
	echo "  -v, --version: print version and exit.\n";
	echo "  --config: set alternative config file.\n";
} /* }}} */

$version = "0.0.1";
$shortoptions = "F:c:s:n:hv";
$longoptions = array('help', 'version', 'config:');
if(false === ($options = getopt($shortoptions, $longoptions))) {
	usage();
	exit(0);
}

/* Print help and exit */
if(isset($options['h']) || isset($options['help'])) {
	usage();
	exit(0);
}

/* Print version and exit */
if(isset($options['v']) || isset($options['verѕion'])) {
	echo $version."\n";
	exit(0);
}

/* Set alternative config file */
if(isset($options['config'])) {
	$settings = new Settings($options['config']);
} else {
	$settings = new Settings();
}

if(isset($settings->_extraPath))
	ini_set('include_path', $settings->_extraPath. PATH_SEPARATOR .ini_get('include_path'));

require_once("SeedDMS/Core.php");
require_once("PHPMailer/PHPMailerAutoload.php");

$db = new SeedDMS_Core_DatabaseAccess($settings->_dbDriver, $settings->_dbHostname, $settings->_dbUser, $settings->_dbPass, $settings->_dbDatabase);
$db->connect() or die ("Could not connect to db-server \"" . $settings->_dbHostname . "\"");
//$db->_conn->debug = 1;

$dms = new SeedDMS_Core_DMS($db, $settings->_contentDir.$settings->_contentOffsetDir);
if(!$dms->checkVersion()) {
	echo "Database update needed.";
	exit;
}

$dms->setRootFolderID($settings->_rootFolderID);
$dms->setMaxDirID($settings->_maxDirID);
$dms->setEnableConverting($settings->_enableConverting);
$dms->setViewOnlineFileTypes($settings->_viewOnlineFileTypes);

/* Create a global user object */
$user = $dms->getUser(1);

$notifications = $dms->getNotificationsToSend();


foreach($notifications as $notif) {
	$toUsers = explode(',',$notif->getToUsers());
	$toGroups = explode(',',$notif->getToGroups());
	
	$gObj = array();
	$uObj = array();
	$mailTo = array();
	
	foreach($toGroups as $g) {
		if($g > 0) {
			$gObj[] = $dms->getGroup($g);
			foreach($gObj as $go) {
				$ugs = $go->getUsers();
				foreach($ugs as $ug) {
					$uObj[] = $ug;
				}
			}
		}
	}

	foreach($toUsers as $u) {
		if($u == '*') {
			$uObj = $dms->getAllUsers();
		} elseif($u > 0) {
			$uObj[] = $dms->getUser($u);
		}
	}

	$mailTo = array_unique($uObj);
	
	foreach($mailTo as $mto) {
		if($mto->getEmail()) {
			$emailobj = new PHPMailer;
			$params = array();
			
			$emailobj->setFrom($settings->_smtpSendFrom);
			$emailobj->addAddress($mto->getEmail(), $mto->getFullName());
			$emailobj->Subject  = $notif->getSubject();
			$emailobj->Body     = $notif->getMessage();
			

			if($emailobj->send()) {
				$notif->setSent();
				$notif->setDateSent(date("Y-m-d H:i:s"));
				$res = true;
				echo json_encode(array("error"=>0, "msg"=>"Sending email succeded"))."\n";
			} else {
				$res = false;
				echo json_encode(array("error"=>1, "msg"=>"Sending email failed"))."\n";
			}
		} else {
			$res = false;
			echo json_encode(array("error"=>1, "msg"=>"No email address for user: ".$mto->getID()))."\n";
		}
	}
	
}

if (is_bool($res) && !$res) {
	echo "Could not send all notifications\n";
	exit(1);
}
?>

