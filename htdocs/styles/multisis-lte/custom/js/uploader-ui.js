$(document).ready(function () {

var counter = 1;

$("#create-file-input").on("click", function(){
  var table = $("#targettable");

  var tr = $("<tr id='tr-"+counter+"'></tr>");
  //var tr = document.createElement("tr");
  var td_one = $("<td></td>");
  var td_two = $("<td></td>");
  var td_three = $("<td></td>");
  var td_four = $("<td></td>");
  var td_five = $("<td></td>");

  $('<input>').attr({
    type: 'file',
    id: 'file-input-'+counter,
    name: 'file-'+counter,
    class: 'attach-selector'
  }).appendTo(td_one);

  $('<input>').attr({
    type: 'text',
    name: 'file-name-'+counter,
    class: 'form-control',
    required: true
  }).appendTo(td_two);

  $('<textarea></textarea>').attr({
    name: 'file-comment-'+counter,
    class: 'form-control',
    required: true,
    rows: 3,
    cols: 40
  }).appendTo(td_three);

  $('<input>').attr({
    type: 'checkbox',
    name: 'file-public-'+counter,
    value: 1,
    checked: false,
  }).appendTo(td_four); 

  $('<a href="#">x</a>').attr({
    class: 'btn btn-danger delete-attach',
    data_id: counter,
    tittle: "Eliminar"
  }).appendTo(td_five);

  tr.append(td_one);
  tr.append(td_two);
  tr.append(td_three);
  tr.append(td_four);
  tr.append(td_five);

  table.append(tr);

  $("#attach-counter").val(counter);
  counter++;
});



});

$(document).on('click', 'a.delete-attach', function(event) {
    event.preventDefault();

    var id = $(this).attr("data_id");

    $("#tr-"+id).remove();

});

/*// -- Detect words -- //*/
$(document).on('change', 'input.attach-selector', function(event) {
    event.preventDefault();
    $("#attach_file_scan").fadeIn();
    var data = new FormData();
    jQuery.each(jQuery(this)[0].files, function (i, file) {
      data.append('file-' + i, file);
    });
    jQuery.ajax({
            url: '/op/op.DetectSensitiveWords.php',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
              $("#attach_file_scan").hide('fast');
              if (data != "file-not-allowed" && data != false) {
                $("#attach_file_scan_response").text(data);
                $("#attach_file_scan_response_two").fadeOut();
                $("#attach_file_scan_response_one").fadeIn();
                $("#attach_file_scan_response_three").fadeOut();
              } else if (data == "file-not-allowed" && data != false) {
                $("#attach_file_scan_response_two").fadeOut();
                $("#attach_file_scan_response_one").fadeOut();
                $("#attach_file_scan_response_three").fadeIn();
              } else if (!data) {
                $("#attach_file_scan_response_one").fadeOut();
                $("#attach_file_scan_response_two").fadeIn();
                $("#attach_file_scan_response_three").fadeOut();
              }
            }
    });
});