<?php
/**
 * Implementation of EditThematic view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for AddEvent view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_EditThematic extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		header('Content-Type: application/javascript; charset=UTF-8');
?>
function checkForm()
{
	msg = new Array();
	if (document.form1.name.value == "") msg.push("<?php printMLText("js_no_name");?>");
<?php
	if (isset($settings->_strictFormCheck) && $settings->_strictFormCheck) {
?>
	if (document.form1.comment.value == "") msg.push("<?php printMLText("js_no_comment");?>");
<?php
	}
?>
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$(document).ready(function() {
	$('body').on('submit', '#form1', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});
});
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$httproot = $this->params['httproot'];
		$thematic = $this->params['thematic'];

		$this->htmlStartPage(getMLText("edit_thematic"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();
?>
<div class="gap-15"></div>
<div class="row">
	<div class="col-md-12">

	<?php $this->startBoxPrimary(getMLText("add_thematic")); ?>

	<form action="../op/op.Thematic.php" id="form1" name="form1" method="post">
		<input type="hidden" name="action" value="editthematic">
		<input type="hidden" name="thematic_id" value="<?php echo $thematic[0]['id']; ?>">
		<?php echo createHiddenFieldWithKey('editthematic'); ?>
		<div class="form-group">
			<label><?php printMLText("name");?>:</label>
			<div><input class="form-control" value="<?php echo $thematic[0]['name']; ?>" type="text" name="name" required="required" /></div>
		</div>
		<div class="form-group">
			<label><?php printMLText("comment");?>:</label>
			<div><textarea class="form-control" name="comment" rows="4" cols="80" required="required"><?php echo $thematic[0]['comment']; ?></textarea></div>
		</div>
		<div class="box-footer">
			<a href="<?php echo '/out/out.Inspections.php'; ?>" class="btn btn-default"><?php echo getMLText('cancel'); ?></a>
			<button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> <?php printMLText("save");?></button>
		</div>

	</form>

	<?php $this->endsBoxPrimary(); ?>
	</div>
</div>

<?php
		echo "</div>";

		$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>