<?php
/**
 * Implementation of EditComment view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditComment view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_EditReport extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$strictformcheck = $this->params['strictformcheck'];
		header('Content-Type: application/javascript; charset=UTF-8');
?>
function checkForm()
{
	msg = new Array();
<?php

		if ($strictformcheck) {
?>
	if (document.form1.query.value == "") msg.push("<?php printMLText("js_no_query");?>");
<?php
		}
?>
	if (msg != "")
	{
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else return true;
}
$(document).ready(function() {
	$('body').on('submit', '#form1', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});
});
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$report = $this->params['report'];
		$reportid = $this->params['reportid'];
		$strictformcheck = $this->params['strictformcheck'];

		$this->htmlStartPage(getMLText("report_title", array("reportname" => htmlspecialchars($report->getName()))), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();		

		echo "&nbsp;"; //$this->getDefaultFolderPathHTML($folder, true, $document);

		//// Document content ////
		echo "<div class=\"row\">";
		echo "<div class=\"col-md-12\">";

		$this->startBoxSuccess(getMLText("edit_report"));
?>
<form class="form-horizontal" action="../op/op.ReportMgr.php" id="form1" name="form1" method="post">
	<?php echo createHiddenFieldWithKey('editreport'); ?>
	<input type="Hidden" name="reportid" value="<?php print $report->getID();?>">
	<input type="Hidden" name="action" value="editreport">
	<div class="control-group">
			<label class="control-label"><?php printMLText("name");?>:</label>
			<div class="controls">
				<input type="text" name="name" class="form-control" value="<?php print htmlspecialchars($report->getName());?>">
			</div>
	</div>
	<div class="control-group">
			<label class="control-label"><?php printMLText("comment");?>:</label>
			<div class="controls">
				<textarea name="comment" class="form-control" rows="4" cols="80"><?php print htmlspecialchars($report->getComment());?></textarea>
			</div>
	</div>
	<div class="control-group">
			<label class="control-label"><?php printMLText("query");?>:</label>
			<div class="controls">
				<textarea name="query" class="form-control" rows="10" cols="80"><?php print htmlspecialchars($report->getQuery());?></textarea>
			</div>
	</div>
	<div class="box-footer">
		<a href='/out/out.ReportMgr.php' class='btn btn-default'><i class='fa fa-chevron-left'></i> <?php printMLText("cancel") ?></a>
		<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php printMLText("save") ?></button>
	</div>
</form>
<?php
		
		$this->endsBoxSuccess();
		echo "</div>";
		echo "</div>"; 
		echo "</div>"; // Ends row
		$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */ 
}
?>
