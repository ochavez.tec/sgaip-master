<?php
/**
 * Implementation of EditDocumentFile view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditDocumentFile view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_EditDocumentFile extends SeedDMS_Bootstrap_Style {

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$folder = $this->params['folder'];
		$document = $this->params['document'];
		$file = $this->params['file'];

		$this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))), "skin-blue sidebar-mini");
        $this->containerStart();
        $this->mainHeader();
        $this->mainSideBar();
        $this->contentStart();    

        $user_root_folder_id = $this->params['user']->getHomeFolder();
        $user_root_folder = $this->params['dms']->getFolder($user_root_folder_id);
        echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document);

        //// Document content ////
        echo "<div class=\"row\">";
        echo "<div class=\"col-md-12\">";

        echo "<div class=\"box box-success div-green-border\">";
        echo "<div class=\"box-header with-border\">";
        echo "<h3 class=\"box-title\">".getMLText("edit_attributes")."</h3>";
        echo "</div>";
        echo "<div class=\"box-body\">";

?>
<form action="../op/op.EditDocumentFile.php" class="form-horizontal" name="form1" method="post">
  <?php echo createHiddenFieldWithKey('editdocumentfile'); ?>
	<input type="hidden" name="documentid" value="<?php echo $document->getID()?>">
	<input type="hidden" name="fileid" value="<?php echo $file->getID()?>">
	<div class="control-group">
		<label class="control-label"><?php printMLText("version");?>:</label>
		<div class="controls">
		<select class="form-control" name="version" id="version">
			<option value=""><?= getMLText('document') ?></option>
<?php
		$versions = $document->getContent();
		foreach($versions as $version)
			echo "<option value=\"".$version->getVersion()."\"".($version->getVersion() == $file->getVersion() ? " selected" : "").">".getMLText('version')." ".$version->getVersion()."</option>";
?>
		</select></div>
	</div>
	<div class="control-group">
			<label class="control-label"><?php printMLText("name");?>:</label>
			<div class="controls">
				<input class="form-control" name="name" type="text" value="<?php print htmlspecialchars($file->getName());?>" />
			</div>
	</div>
	<div class="control-group">
			<label class="control-label"><?php printMLText("comment");?>:</label>
			<div class="controls">
				<textarea class="form-control" name="comment" rows="4" cols="80"><?php print htmlspecialchars($file->getComment());?></textarea>
			</div>
	</div>
	<div class="control-group">
			<label class="control-label"><?php printMLText("document_link_public");?>:</label>
			<div class="controls">
				<input type="checkbox" name="public" value="true"<?php echo ($file->isPublic() ? " checked" : "");?> />
			</div>
	</div>
	<div class="controls">
		<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save") ?></button>
	</div>
</form>
<?php
	
	echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>"; 
    echo "</div>"; // Ends row
    $this->contentEnd();
    $this->mainFooter();    
    $this->containerEnd();
    $this->htmlEndPage();
    } /* }}} */
}
?>
