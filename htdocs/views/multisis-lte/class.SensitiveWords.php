<?php
/**
 * Implementation of SensitiveWords view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for SensitiveWords view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_SensitiveWords extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		header('Content-Type: application/javascript');
?>
$(document).ready( function() {
	$("#sensitive-words-table").DataTable({
		  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": true,
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ningún registro ha sido encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar",
            "paginate": {
              "first":"Primero",
              "last":"Último",
              "next":"Siguiente",
              "previous":"Anterior"
            },
      }
	});
});
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$sensitive_words = $this->params['sensitive_words'];
		
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini"); ?>
		<style type="text/css">
			.tr-hi:hover {
				background-color: #8fdbf9;
			}
		</style>
		<?php
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
		

	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("global_sensitive_words"));


?>

<div class="row-fluid">
	<div class="col-md-12">
		<div class="box box-success box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo getMLText("add_global_sensitive_words"); ?></h3>				
			</div>
			<div class="box-body">
				<form class="form-horizontal" style="margin-bottom: 0px;" action="../op/op.SensitiveWords.php" method="post">
				<?php echo createHiddenFieldWithKey('addsensitiveword'); ?>
				<input type="hidden" name="action" value="addword">				
				<div class="control-group">
					<label class="control-label"><?php echo getMLText("word_or_phrase")?>:</label>
					<div class="controls">
						<input name="name" class="form-control" type="text" required="required">
					</div>
				</div>
	          	<br>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save");?></button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
	
	<div class="col-md-12">
		<div class="box box-info box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo getMLText("global_sensitive_words"); ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body table-responsive">
				<table id="sensitive-words-table" class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th style="text-align: center;" width="10%"><?php echo getMLText("id"); ?></th>
						<th style="text-align: center;" width="60%"><?php echo getMLText("word_or_phrase"); ?></th>
						<th style="text-align: center;" width="30%"><?php echo getMLText("action"); ?></th>
					</tr>		
				</thead>					
				<tbody>
			<?php 
			if (!empty($sensitive_words)) {
				foreach ($sensitive_words as $word) { ?>
					<tr class="tr-hi">
						<td style="text-align: center;" ><?php echo $word['id']; ?></td>
						<td><?php echo $word['name']; ?></td>
						<td>
							<form style="display: inline-block;" method="post" action="../op/op.SensitiveWords.php" > 
							<?php echo createHiddenFieldWithKey('removesensitiveword'); ?>
							<input type="hidden" name="wordid" value="<?php echo $word['id'] ?>">
							<input type="hidden" name="action" value="removeword">
							<button class="btn btn-danger" type="submit"><i class="fa fa-times"></i> <?php echo getMLText("delete")?></button>
							</form>
						</td>
					</tr>

			<?php } 

			} ?>
			</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

	
<?php
		$this->endsBoxPrimary();

		echo "</div>";
		echo "</div>";
		echo "</div>";
		
    	$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();	
	} /* }}} */
}
?>
