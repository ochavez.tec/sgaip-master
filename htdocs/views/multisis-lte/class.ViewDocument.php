<?php
/**
 * Implementation of ViewDocument view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Include class to preview documents
 */
require_once("SeedDMS/Preview.php");

/**
 * Class which outputs the html page for ViewDocument view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_ViewDocument extends SeedDMS_Bootstrap_Style {

	protected function getAccessModeText($defMode) { /* {{{ */
		switch($defMode) {
			case M_NONE:
				return getMLText("access_mode_none");
				break;
			case M_READ:
				return getMLText("access_mode_read");
				break;
			case M_READWRITE:
				return getMLText("access_mode_readwrite");
				break;
			case M_ALL:
				return getMLText("access_mode_all");
				break;
		}
	} /* }}} */

	protected function printAccessList($obj) { /* {{{ */
		$accessList = $obj->getAccessList();
		if (count($accessList["users"]) == 0 && count($accessList["groups"]) == 0)
			return;

		$content = '';
		for ($i = 0; $i < count($accessList["groups"]); $i++)
		{
			$group = $accessList["groups"][$i]->getGroup();
			$accesstext = $this->getAccessModeText($accessList["groups"][$i]->getMode());
			$content .= $accesstext.": ".htmlspecialchars($group->getName());
			if ($i+1 < count($accessList["groups"]) || count($accessList["users"]) > 0)
				$content .= "<br />";
		}
		for ($i = 0; $i < count($accessList["users"]); $i++)
		{
			$user = $accessList["users"][$i]->getUser();
			$accesstext = $this->getAccessModeText($accessList["users"][$i]->getMode());
			$content .= $accesstext.": ".htmlspecialchars($user->getFullName());
			if ($i+1 < count($accessList["users"]))
				$content .= "<br />";
		}

		if(count($accessList["groups"]) + count($accessList["users"]) > 3) {
			$this->printPopupBox(getMLText('list_access_rights'), $content);
		} else {
			echo $content;
		}
	} /* }}} */

	/**
	 * Output a single attribute in the document info section
	 *
	 * @param object $attribute attribute
	 */
	protected function printAttribute($attribute, $is_in_workflow = false) { /* {{{ */
		$attrdef = $attribute->getAttributeDefinition();

?>
		<tr>
		<td class='doc-info-title'><strong><?php echo htmlspecialchars($attrdef->getName()); ?>:</strong></td>
		
		<?php if ($attrdef->getName() == "Información solicitada"): ?>
			
			<td class='doc-info-desc' id="selection_text">
			
		<?php else: ?>
			<td class='doc-info-desc'>
		<?php endif ?>
		

		<?php
		switch($attrdef->getType()) {
		case SeedDMS_Core_AttributeDefinition::type_url:
			$attrs = $attribute->getValueAsArray();
			$tmp = array();
			foreach($attrs as $attr) {
				$tmp[] = '<a href="'.htmlspecialchars($attr).'">'.htmlspecialchars($attr).'</a>';
			}
			echo implode('<br />', $tmp);
			break;
		case SeedDMS_Core_AttributeDefinition::type_email:
			$attrs = $attribute->getValueAsArray();
			$tmp = array();
			foreach($attrs as $attr) {
				$tmp[] = '<a mailto="'.htmlspecialchars($attr).'">'.htmlspecialchars($attr).'</a>';
			}
			echo implode('<br />', $tmp);
			break;
		default:
			echo htmlspecialchars(implode(', ', $attribute->getValueAsArray()));
		} 
		?>

		</td>
		</tr>
<?php
	} /* }}} */

	function timelinedata() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$document = $this->params['document'];

		$jsondata = array();
		if($user->isAdmin()) {
			$data = $document->getTimeline();

			foreach($data as $i=>$item) {
				switch($item['type']) {
				case 'add_version':
					$msg = getMLText('timeline_'.$item['type'], array('document'=>htmlspecialchars($item['document']->getName()), 'version'=> $item['version']));
					break;
				case 'add_file':
					$msg = getMLText('timeline_'.$item['type'], array('document'=>htmlspecialchars($item['document']->getName())));
					break;
				case 'status_change':
					$msg = getMLText('timeline_'.$item['type'], array('document'=>htmlspecialchars($item['document']->getName()), 'version'=> $item['version'], 'status'=> $item['status']));
					break;
				default:
					$msg = '???';
				}
				$data[$i]['msg'] = $msg;
			}

			foreach($data as $item) {
				if($item['type'] == 'status_change')
					$classname = $item['type']."_".$item['status'];
				else
					$classname = $item['type'];
				$d = makeTsFromLongDate($item['date']);
				
				$jsondata[] = array('start'=>date('c', $d)/*$item['date']*/, 'content'=>$item['msg'], 'className'=>$classname);
			}
		}
		header('Content-Type: application/json');
		echo json_encode($jsondata);
	} /* }}} */

	function js() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$document = $this->params['document'];
		$folder = $this->params['folder'];

		if(isset($this->params['document_content']) && ($this->params['document_content'] !== false)) {
			$document_content_temp = $this->params['document_content'];
			$document_content = $document_content_temp['content'];

		} else {
			$document_content = "<p></p>";
		}


		/////////////////////////////////
		$cc_response = $dms->getDocumentAttributeValue(85, $document->getID());

		if ($cc_response) {
			$cc_response_text = '"'.addslashes($cc_response).'"';
		} else {
			$cc_response_text = "<p>Respuesta</p>";
		}
		/////////////////////////////////
		

		header('Content-Type: application/javascript');
		if($user->isAdmin()) {
			$this->printTimelineJs('out.ViewDocument.php?action=timelinedata&documentid='.$document->getID(), 300, '', date('Y-m-d'));
		}
		$this->printDocumentChooserJs("form1");
	
		?>
			$(document).on("ready", function(){				


				$("#document-info-widget").on("click", function(){
					$("#document-info").addClass("div-hidden");
					$("#tab-information").removeClass("col-md-8").addClass("col-md-12");
					$("#memo-editor").removeClass("col-md-8").addClass("col-md-12");
				});

				$("#document-tabs").on("click", function(){
					$("#tab-information").addClass("div-hidden");
					$("#document-info").removeClass("col-md-4").addClass("col-md-12");
				});

				$("#open-editor").on("click", function(){
					$("#tab-information").addClass("div-hidden");
					$("#memo-editor").removeClass("div-hidden");
				});

				$(".close-memo-editor").on("click", function(){
					$("#memo-editor").addClass("div-hidden");
					$("#tab-information").removeClass("div-hidden");
				});
				

				/* ---- For document previews ---- */
			  $(".preview-doc-btn").on("click", function(){
			  	$("#thedocinfo").hide();
					$("#thetimeline").hide();

			  	var docID = $(this).attr("id");
			  	var version = $(this).attr("rel");
			  	$("#doc-title").text($(this).attr("title"));
			  	$("#document-previewer").show('slow');
			  	$("#iframe-charger").attr("src","../pdfviewer/web/viewer.html?file=..%2F..%2Fop%2Fop.Download.php%3Fdocumentid%3D"+docID+"%26version%3D"+version);
			  });

			  $(".preview-attach-btn").on("click", function(){
			  	$("#thedocinfo").hide();
					$("#thetimeline").hide();

			  	var docID = $(this).attr("id");
			  	var file = $(this).attr("rel");
			  	$("#doc-title").text($(this).attr("title"));
			  	$("#document-previewer").show('slow');
			  	$("#iframe-charger").attr("src","../pdfviewer/web/viewer.html?file=..%2F..%2Fop%2Fop.Download.php%3Fdocumentid%3D"+docID+"%26file%3D"+file);
			  });

			  $(".close-doc-preview").on("click", function(){
			  	$("#document-previewer").hide();
			  	$("#iframe-charger").attr("src","");
			  	$("#thedocinfo").show('slow');
			  	$("#thetimeline").show('slow');
			  });

			});

			$('#version-date').datepicker({
				language: 'es',
				format: 'dd-mm-yyyy',
			    autoclose: true,
			});

			$("#edit-version-date").on("click", function(){
				if($("#date-div").hasClass("hidden")){
					$("#date-div").removeClass("hidden");
					$("#date-div").fadeIn("slow");
				} else {
					$("#date-div").addClass("hidden");
					$("#date-div").fadeOut("slow");
				}
				
			});

			$("#update-version-date").on("submit", function(ev){
				ev.preventDefault();

				var statuslogid = $("#statuslogid").val();
				var date = $("#version-date").val();

				formtoken = <?php echo "'".createFormKey("updateversiondate")."'"; ?>;

			  	$.get("/op/op.UpdateDocumentAttributes.php",
					{ action: "updateversiondate", statuslogid: statuslogid, date: date, formtoken: formtoken })
					.always(function(data) {			

					if(data.success) {
						$("#actual-date").text(data.date);
						$("#version-date").val('');
						noty({
							text: data.message,
							type: "success",
							dismissQueue: true,
							layout: "topRight",
							theme: "defaultTheme",
							timeout: 1500,
						});
					} else {
						noty({
							text: data.message,
							type: "error",
							dismissQueue: true,
							layout: "topRight",
							theme: "defaultTheme",
							timeout: 3500,
						});
					}
				});
			});


			/* Select text and make a requirement */
			function getSelected() {
			    if (window.getSelection) {
			        return window.getSelection();
			    }
			    else if (document.getSelection) {
			        return document.getSelection();
			    }
			    else {
			        var selection = document.selection && document.selection.createRange();
			        if (selection.text) {
			            return selection.text;
			        }
			        return false;
			    }
			    return false;
			}


			$('#selection_text').mouseup(function() {
			    var selection = getSelected();
			    if(selection && (selection = new String(selection).replace(/^\s+|\s+$/g,''))) {
			    	
			    	$("#requirement_text").text(selection);
			        $('#memoModal').modal('show');
			    }
			});

		/* FOR MEMO-RESOLUTION */
    	CKEDITOR.replace( 'editor', {
			toolbar: [
				{ name: 'document', items: [ 'Print','NewPage' ] },
				{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },
				{ name: 'styles', items: [ 'Styles','Format', 'Font', 'FontSize' ] },
				{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting'] },
				{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
				{ name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
				{ name: 'links', items: [ 'Link', 'Unlink' ] },
				{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'lineheight','-', 'Blockquote'] },
				{ name: 'insert', items: [ 'Image', 'Table','HorizontalRule','SpecialChar','PageBreak' ] },
				{ name: 'tools', items: [ 'Maximize' ] },
				{ name: 'editing', items: [ 'Find','Replace','-','SelectAll','Scayt','Source' ] }
			],
			customConfig: '',
			disallowedContent: 'img{width,height,float}',
			extraAllowedContent: 'img[width,height,align]',
			extraPlugins: 'tableresize,pagebreak,sourcearea,filebrowser,lineheight,pastefromword',
			height: 800,
			contentsCss: [ '/styles/multisis-lte/plugins/ckeditor-4.10/contents.css', '/styles/multisis-lte/plugins/ckeditor-4.10/mystyles.css' ],
			bodyClass: 'document-editor',
			format_tags: 'p;h1;h2;h3;pre',
			removeDialogTabs: 'image:advanced;link:advanced',
			line_height: '1;1.5;2;3;',

	        filebrowserUploadMethod : 'form',
	        filebrowserBrowseUrl : '/styles/multisis-lte/plugins/kcfinder/browse.php?opener=ckeditor&type=files',
		   	filebrowserImageBrowseUrl : '/styles/multisis-lte/plugins/kcfinder/browse.php?opener=ckeditor&type=images',
		   	filebrowserFlashBrowseUrl : '/styles/multisis-lte/plugins/kcfinder/browse.php?opener=ckeditor&type=flash',
		   	filebrowserUploadUrl : '/styles/multisis-lte/plugins/kcfinder/upload.php?opener=ckeditor&type=files',
		   	filebrowserImageUploadUrl : '/styles/multisis-lte/plugins/kcfinder/upload.php?opener=ckeditor&type=images',
		   	filebrowserFlashUploadUrl : '/styles/multisis-lte/plugins/kcfinder/upload.php?opener=ckeditor&type=flash',
		});
		CKEDITOR.instances['editor'].setData(<?php echo "'".addslashes($document_content)."'"; ?>);
		/* FOR MEMO-RESOLUTION */
		

		/* FOR CC RESPONSE */
		if($('#cc_response_text').length){
		
			<?php if ($cc_response_text == "<p>Respuesta</p>"){ ?>
				var response_text = '<?php echo $cc_response_text; ?>';
			<?php } else { ?>
				var response_text =	<?php echo trim(preg_replace('/\s+/', ' ', $cc_response_text)); ?>;
			<?php } ?>			
			
			CKEDITOR.replace( 'cc_response_text', {
				toolbar: [
					
					{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },				
					{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike'] },				
					{ name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
					{ name: 'links', items: [ 'Link', 'Unlink' ] },
					{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList'] },
					
				],
				customConfig: '',
				disallowedContent: 'img{width,height,float}',
				extraAllowedContent: 'img[width,height,align]',
				extraPlugins: 'tableresize,pagebreak,sourcearea,filebrowser',
				height: 500,
				contentsCss: [ '/styles/multisis-lte/plugins/ckeditor-4.10/contents.css', '/styles/multisis-lte/plugins/ckeditor-4.10/mystyles.css' ],
				bodyClass: 'document-editor',
				format_tags: 'p;h1;h2;h3;pre',
				removeDialogTabs: 'image:advanced;link:advanced',	        
			});
			CKEDITOR.instances['cc_response_text'].setData(response_text);
		}
		/* FOR CC RESPONSE */


		$("#apply-template").on("click", function(ev){
			ev.preventDefault();

			var template_id = $("#template").val();
			var id = $(this).attr('rel');
			
			if (parseInt(template_id) < 1) {
				noty({
					text: <?php echo "'".getMLText("no_template_selected")."'"; ?>,
					type: "error",
					dismissQueue: true,
					layout: "topRight",
					theme: "defaultTheme",
					timeout: 3500,
				});
			} else {
				var formtoken = <?php echo "'".createFormKey("gettemplate")."'"; ?>;
				$.get("/op/op.Templates.php",
				{ action: "gettemplate", document_id: id, template_id: template_id, formtoken: formtoken })
				.always(function(data) {
					if(data.success) {
						CKEDITOR.instances['editor'].setData(data.html);
					} else {
						noty({
							text: data.message,
							type: "error",
							dismissQueue: true,
							layout: "topRight",
							theme: "defaultTheme",
							timeout: 3500,
						});		
					}
				});
			}
		});

		$('#save-content').on('click', function(ev){
			ev.preventDefault();
			var html = $(".cke_wysiwyg_frame").contents().find("body").html();
			var id = $(this).attr('rel');
			var data_action = $("#open-editor").attr("data-action");

			formtoken = '<?php echo createFormKey("saveeditorcontent"); ?>';
		  	$.post('/op/op.Templates.php',
			{ action: data_action, document_id: id, content: html, formtoken: formtoken })
			.always(function(data) {						
				if(data.success) {							
					noty({
						text: data.message,
						type: 'success',
						dismissQueue: true,
						layout: 'topRight',
						theme: 'defaultTheme',
						timeout: 1500,
					});
				} else {
					noty({
						text: data.message,
						type: 'error',
						dismissQueue: true,
						layout: 'topRight',
						theme: 'defaultTheme',
						timeout: 3500,
					});
				}
			});
		});

		$("#requirementForm").on("submit", function(){
			$('i', "#requirement-btn").removeClass("fa-save");	
			$('i', "#requirement-btn").addClass("fa-refresh fa-spin");
			$("#requirement-btn").attr("disabled", true);
		});

		
		$('.delete-cookie').on('click', function(ev){
			$('#sip-alert').fadeOut('slow');
		});


		$('#resolution_type_select').on('change', function() {

			var option = $('option:selected', this).attr('rel'); /* 0 */
			if(!Number(option)){
				$("#group_asigned_select").addClass("hidden");
				$("#create_resolution_check").addClass("hidden");
			} else {
				if ($('#group_asigned_select').hasClass('hidden')) {
					$("#group_asigned_select").removeClass("hidden");
				}				

				$("#create_resolution_check").removeClass("hidden");
			}
			
		});

		
		$('#save-margins').on('click', function(ev){
			ev.preventDefault();			
			var id = $(this).attr('rel');
			var data_action = $(this).attr("data-action");
			var margin_left = $('#margin-left').val();
			var margin_top = $('#margin-top').val();
			var margin_right = $('#margin-right').val();
			var margin_bottom = $('#margin-bottom').val();

			formtoken = '<?php echo createFormKey("savedocumentcontentmargins"); ?>';
		  	$.post('/op/op.Templates.php',
			{ 	action: data_action, 
				document_id: id, 
				margin_left: margin_left, 
				margin_top: margin_top, 
				margin_right: margin_right, 
				margin_bottom: margin_bottom, 
				formtoken: formtoken })
			.always(function(data) {

				if(data.success) {							
					noty({
						text: data.message,
						type: 'success',
						dismissQueue: true,
						layout: 'topRight',
						theme: 'defaultTheme',
						timeout: 1500,
					});
				} else {
					noty({
						text: data.message,
						type: 'error',
						dismissQueue: true,
						layout: 'topRight',
						theme: 'defaultTheme',
						timeout: 3500,
					});
				}
			});
		});

		<?php

	} /* }}} */


	/**
	 * Show document information
	 */

	function documentInfos() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$document = $this->params['document'];

		//$this->contentHeading(getMLText("document_infos"));
		//$this->contentContainerStart();
		?>
		<div class="box box-success">
      	<div class="box-header with-border">
        <h3 class="box-title"><?php echo getMLText("document_infos"); ?></h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" id="document-info-widget" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
	    </div>
	    <div class="box-body table-responsive">
		<?php
		$txt = $this->callHook('preDocumentInfos', $document);
		if(is_string($txt))
			echo $txt;
		$txt = $this->callHook('documentInfos', $document);
		if(is_string($txt))
			echo $txt;
		else {
?>
		<table class="table table-striped">
<?php
		if($user->isAdmin()) {
			echo "<tr>";
			echo "<td class='doc-info-title'><strong>".getMLText("id").":</strong></td>\n";
			echo "<td class='doc-info-desc'>".htmlspecialchars($document->getID())."</td>\n";
			echo "</tr>";
		}
?>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("name");?>:</strong></td>
		<td class='doc-info-desc'><?php print htmlspecialchars($document->getName());?></td>
		</tr>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("owner");?>:</strong></td>
		<td class='doc-info-desc'>
<?php
		$owner = $document->getOwner();
		print "<a class=\"infos\" href=\"mailto:".$owner->getEmail()."\">".htmlspecialchars($owner->getFullName())."</a>";
?>
		</td>
		</tr>
<?php
		if($document->getComment()) {
?>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("comment");?>:</strong></td>
		<td class='doc-info-desc'><?php print htmlspecialchars($document->getComment());?></td>
		</tr>
<?php
		}
		if($user->isAdmin()) {
			echo "<tr>";
			echo "<td class='doc-info-title'><strong>".getMLText('default_access').":</strong></td>";
			echo "<td class='doc-info-desc'>".$this->getAccessModeText($document->getDefaultAccess())."</td>";
			echo "</tr>";
			if($document->inheritsAccess()) {
				echo "<tr>";
				echo "<td class='doc-info-title'><strong>".getMLText("access_mode").":</strong></td>\n";
				echo "<td class='doc-info-desc'>";
				echo getMLText("inherited")."<br />";
				$this->printAccessList($document);
				echo "</td>";
				echo "</tr>";
			} else {
				echo "<tr>";
				echo "<td class='doc-info-title'><strong>".getMLText('access_mode').":</strong></td>";
				echo "<td class='doc-info-desc'>";
				$this->printAccessList($document);
				echo "</td>";
				echo "</tr>";
			}
		}
?>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("used_discspace");?>:</strong></td>
		<td class='doc-info-desc'><?php print SeedDMS_Core_File::format_filesize($document->getUsedDiskSpace());?></td>
		</tr>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("creation_date");?>:</strong></td>
		<td class='doc-info-desc'><?php print getLongReadableDate($document->getDate()); ?></td>
		</tr>
<?php
		if($document->expires()) {
?>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("expires");?>:</strong></td>
		<td class='doc-info-desc'><?php print getReadableDate($document->getExpires()); ?></td>
		</tr>
<?php
		}
		if($document->getKeywords()) {
?>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("keywords");?>:</strong></td>
		<td class='doc-info-desc'><?php print htmlspecialchars($document->getKeywords());?></td>
		</tr>
<?php
		}
		if($cats = $document->getCategories()) {
?>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("categories");?>:</strong></td>
		<td class='doc-info-desc'>
		<?php
			$ct = array();
			foreach($cats as $cat)
				$ct[] = htmlspecialchars($cat->getName());
			echo implode(', ', $ct);
		?>
		</td>
		</tr>
<?php
		}
		if($types = $document->getTypes()) {
?>
		<tr>
		<td class='doc-info-title'><strong><?php printMLText("type");?>:</strong></td>
		<td class='doc-info-desc'>
		<?php
			$ct = array();
			foreach($types as $type)
				$ty[] = htmlspecialchars($type->getName());
			echo implode(', ', $ty);
		?>
		</td>
		</tr>
<?php
		}
?>
		<?php
		$attributes = $document->getAttributes();
		$latestContent = $document->getLatestContent();
		
		$wkf = $latestContent->getWorkflow();
		$is_in_workflow = false;
		if($wkf){
			$is_in_workflow = true;
		}
		

		if($attributes) {
			foreach($attributes as $attribute) {
				$arr = $this->callHook('showDocumentAttribute', $document, $attribute);
				if(is_array($arr)) {
					echo "<tr>";
					echo "<td class='doc-info-title'><strong>".$arr[0].":</strong></td>";
					echo "<td class='doc-info-desc'>".$arr[1]."</td>";
					echo "</tr>";
				} else {
					$this->printAttribute($attribute, $is_in_workflow);
				}
			}
		}
?>
		</table>
<?php
		}
		$txt = $this->callHook('postDocumentInfos', $document);
		if(is_string($txt))
			echo $txt;
		//$this->contentContainerEnd();

		echo "</div>";
		echo "</div>";
	} /* }}} */

	function preview() { /* {{{ */
		$dms = $this->params['dms'];
		$document = $this->params['document'];
		$timeout = $this->params['timeout'];
		$showfullpreview = $this->params['showFullPreview'];
		$converttopdf = $this->params['convertToPdf'];
		$cachedir = $this->params['cachedir'];

		if(!$showfullpreview)
			return;

		$latestContent = $document->getLatestContent();
		$txt = $this->callHook('preDocumentPreview', $latestContent);
		if(is_string($txt))
			echo $txt;
		else {
			switch($latestContent->getMimeType()) {
			case 'audio/mpeg':
			case 'audio/mp3':
			case 'audio/ogg':
			case 'audio/wav':
				$this->contentHeading(getMLText("preview"));
	?>
			<audio controls style="width: 100%;">
			<source  src="../op/op.Download.php?documentid=<?php echo $document->getID(); ?>&version=<?php echo $latestContent->getVersion(); ?>" type="audio/mpeg">
			</audio>
	<?php
				break;
			case 'application/pdf':
				$this->contentHeading(getMLText("preview"));
	?>
				<iframe id="este-es-el-id" src="../pdfviewer/web/viewer.html?file=<?php echo urlencode('../../op/op.Download.php?documentid='.$document->getID().'&version='.$latestContent->getVersion()); ?>" width="100%" height="700px"></iframe>
	<?php
				break;
			case 'image/svg+xml':
				$this->contentHeading(getMLText("preview"));
	?>
				<img src="../op/op.Download.php?documentid=<?php echo $document->getID(); ?>&version=<?php echo $latestContent->getVersion(); ?>" width="100%">
	<?php
				break;
			default:
				$txt = $this->callHook('additionalDocumentPreview', $latestContent);
				if(is_string($txt))
					echo $txt;
				break;
			}
		}
		$txt = $this->callHook('postDocumentPreview', $latestContent);
		if(is_string($txt))
			echo $txt;

		if($converttopdf) {
			$pdfpreviewer = new SeedDMS_Preview_PdfPreviewer($cachedir, $timeout);
			if($pdfpreviewer->hasConverter($latestContent->getMimeType())) {
				$this->contentHeading(getMLText("preview"));
?>
				<iframe src="../pdfviewer/web/viewer.html?file=<?php echo urlencode('../../op/op.PdfPreview.php?documentid='.$document->getID().'&version='.$latestContent->getVersion()); ?>" width="100%" height="700px"></iframe>
<?php
			}
		}
	} /* }}} */

	function show() { /* {{{ */
		parent::show();
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$folder = $this->params['folder'];
		$document = $this->params['document'];
		$accessop = $this->params['accessobject'];
		$viewonlinefiletypes = $this->params['viewonlinefiletypes'];
		$enableownerrevapp = $this->params['enableownerrevapp'];
		$workflowmode = $this->params['workflowmode'];
		$cachedir = $this->params['cachedir'];
		$previewwidthlist = $this->params['previewWidthList'];
		$previewwidthdetail = $this->params['previewWidthDetail'];
		$documentid = $document->getId();
		$currenttab = $this->params['currenttab'];
		$timeout = $this->params['timeout'];

		$information_types = $this->params['information_types'];
		$resolution_types = $this->params['resolution_types'];

		$versions = $document->getContent();

		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/timeline/timeline.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/timeline/timeline-min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/timeline/timeline-locales.js"></script>'."\n", 'js');

		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/ckeditor-4.10/ckeditor.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/ckeditor-4.10/lang/es.js"></script>'."\n", 'js');


		$this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))), "skin-blue sidebar-mini");

		$this->containerStart();
		echo "<style>.table-hover tr:hover {background:#cfedff !important;}</style>";
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		$user_root_folder_id = $this->params['user']->getHomeFolder();
		$user_root_folder = $this->params['dms']->getFolder($user_root_folder_id);


		$legal_partners = $dms->getGroup(164); // 164 = Colaboradores Jurídicos
		$secretaries = $dms->getGroup(165); // 165 = Secretarias

		if ($legal_partners->isMember($user) || $secretaries->isMember($user) || $user->isAdmin() || $document->getAccessMode($user) >= M_READWRITE) {
			echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document, $user, true);
		} else {
			echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document, $user, false);
		}

		echo "<div class=\"row\" id=\"thedocinfo\">";

		//$this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))));
		//$this->globalNavigation($folder);
		//$this->contentStart();
		//$this->pageNavigation($this->getFolderPathHTML($folder, true, $document), "view_document", $document);

		if ($document->isLocked()) {
			$lockingUser = $document->getLockingUser();
			$txt = $this->callHook('documentIsLocked', $document, $lockingUser);
			if(is_string($txt))
				echo $txt;
			else {
?>
	<div class="col-md-12">
		<div class="callout callout-warning alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
			<?php printMLText("lock_message", array("email" => $lockingUser->getEmail(), "username" => htmlspecialchars($lockingUser->getFullName())));?>
		</div>
	</div>

<?php
			}
		}

		/* Retrieve attacheѕ files */
		$files = $document->getDocumentFiles();

		/* Retrieve linked documents */
		$links = $document->getDocumentLinks();
		$links = SeedDMS_Core_DMS::filterDocumentLinks($user, $links, 'target');

		/* Retrieve reverse linked documents */
		$reverselinks = $document->getReverseDocumentLinks();
		$reverselinks = SeedDMS_Core_DMS::filterDocumentLinks($user, $reverselinks, 'source');

		/* Retrieve latest content */
		$latestContent = $document->getLatestContent();
		$needwkflaction = false;
		if($workflowmode == 'traditional' || $workflowmode == 'traditional_only_approval') {
		} else {
			$workflow = $latestContent->getWorkflow();
			if($workflow) {
				$workflowstate = $latestContent->getWorkflowState();
				$transitions = $workflow->getNextTransitions($workflowstate);
				$needwkflaction = $latestContent->needsWorkflowAction($user);
			}
		}


		// IMPORTANT VARIABLES ///////////////////////////
		$memo_cat = false;
		$user_is_notificator = false;
		$cc_user_is_in_action = false; // Only for display cc response submit button

		if (isset($workflowstate) && $workflowstate) {
			$global_workflowstate = $workflowstate;
		} else {
			$global_workflowstate = false;
		}
		// IMPORTANT VARIABLES ///////////////////////////


		// If the current user needs review the workflow
		if($needwkflaction) {
			echo "<div class='col-md-12'>";
			$this->infoMsg(getMLText('needs_workflow_action'));
			echo "</div>";
		}

		// If the current user is assigned to sip
		$temp_workflow = $latestContent->getWorkflow();
		if($temp_workflow){
			$temp_status = $latestContent->getStatus();
			$temp_workflowstate = $latestContent->getWorkflowState();
			if ((int)$temp_workflow->getID() == 12) { // 12 = solicitudes
				if ($temp_status['status'] != 2 && (int)$temp_workflowstate->getID() == 14) { // 14 = Solicitud en Análisis						
						echo "<div class='col-md-12' id='sip-alert'>";
						echo "<div class='callout callout-warning alert-dismissible'>";
						echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>";
						echo getMLText("sip_assigned_notice");
						echo "</div>";
						echo "</div>";
					
				}
			}
		}
		
		$status = $latestContent->getStatus();
		$reviewStatus = $latestContent->getReviewStatus();
		$approvalStatus = $latestContent->getApprovalStatus();
?>

<!-- Memos form to create requirements -->
<div class="modal modal-primary fade modal-fullscreen force-fullscreen" id="memoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        	<h4 class="modal-title"><?php printMLText("create_requirement") ?></h4>
        </div>
        <form action="/op/op.MemoMgr.php" method="post" id="requirementForm">
        <div class="modal-body" id="requirementModalContent">
        	<?php echo createHiddenFieldWithKey('createnewmemo'); ?>
			<input type="hidden" name="action" value="createnewmemo">
			<input type="hidden" name="folder_id" value="<?php echo $folder->getID(); ?>">
			<input type="hidden" name="document_name" value="<?php echo $document->getName(); ?>">
			<input type="hidden" name="documentid" value="<?php echo $document->getID(); ?>">
          	<div class="control-group">
			<div class="controls">
				<label class="control-label"><?php printMLText("information_type_requested");?>:</label>
				<select class="chzn-select" name="information_type_requested" required="true">
					<option value="-1"><?php printMLText("choose_information_type"); ?></option>
					<?php
					if (!empty($information_types)) {
						foreach ($information_types as $information_type) {
							print "<option value=\"".$information_type['name']."\" >" . htmlspecialchars($information_type['name']) . "</option>";				
						}
					} ?>
				</select>
			</div>
			</div>
			<br/>

			<div class="control-group">
			<div class="controls">
				<label class="control-label"><?php printMLText("resolution_type");?>:</label>
				<select class="chzn-select" name="resolution_type" id="resolution_type_select">
					<option value="-1"><?php printMLText("choose_resolution_type"); ?></option>
					<?php
					if (!empty($resolution_types)) {
						foreach ($resolution_types as $resolution_type) {
							print "<option rel=\"".$resolution_type['make_memo']."\"  value=\"".$resolution_type['id']."\" >" . htmlspecialchars($resolution_type['name']) . "</option>";				
						}
					} ?>
				</select>
			</div>
			</div>
			<br/>

			<div class="control-group hidden" id="create_resolution_check">
			<div class="controls">
				<label class="control-label">¿<?php printMLText("create_resolution");?>?:&nbsp;&nbsp;</label>
				<input type="checkbox" name="make_resolution" value="1" checked="true" />
			</div>
			<br/>
			</div>
			

			<div class="control-group" id="group_asigned_select">
			<div class="controls">
				<label class="control-label"><?php printMLText("group_asigned");?>:</label>
				<select class="chzn-select" name="group_asigned">
				<?php
				   $allGroups = $dms->getAllGroups();
				   foreach ($allGroups as $groupObj) {
				   		print "<option value='".$groupObj->getName()."'>" . htmlspecialchars($groupObj->getName()) . "</option>";
				   }
				?>
				</select>
			</div>
			<br/>
			</div>
			

          	<div class="control-group">
			<label class="control-label"><?php printMLText("requirement_description");?>:</label>
			<div class="controls">
				<textarea class="form-control" id="requirement_text" name="description" rows="4" cols="80" required="true"></textarea>
			</div>
			</div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("cancel"); ?></button>
          <button type="submit" class="btn btn-primary" id="requirement-btn"><i class="fa fa-save"></i> <?php printMLText("save"); ?></button>
        </div>

        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /Ends .modal -->

<div class="col-md-4" id="document-info">
<?php
		// Document information
		$this->documentInfos();
		//$this->preview();
?>
</div>

<?php /* ------------------------------------------- MEMO EDITOR ------------------------------------------- */ ?>
<div class="col-md-8 div-hidden" id="memo-editor">
<div class="box box-primary">
<div class="box-header">
<div class="box-tools pull-right">
<button type="button" class="btn btn-box-tool close-memo-editor"><i class="fa fa-times"></i></button>
</div>
</div>
<div class="box-body">
	<div class="col-md-6" style="padding-left:0;">
		<form class="form-inline pull-left" method="post" action="/op/op.MemoMgr.php" style="">
			<div class="form-group">				
				<select class="form-control" name="template" id="template">
					<option value="-1"><?php echo getMLText("template_select")?></option>
					<?php
						$templates = $dms->getTemplates();
						if ($templates && !empty($templates)) {
							foreach ($templates as $template) {
								print "<option value=\"".$template['id']."\" >" . htmlspecialchars($template['name']) . "</option>";
							}
						}
					?>
				</select>			
				<button type="button" class="btn btn-info" id="apply-template" rel="<?php echo $document->getID(); ?>" title="<?php printMLText("template_advert"); ?>"><?php printMLText("apply"); ?></button>
			</div>

		</form>
	</div>
	<div class="col-md-6" style="padding-right:0;">
			<?php $doc_margins = $dms->getDocumentContentMargins($documentid); ?>
			<div style="float:left;max-width:15%;">
				<label style="width: 25%;float: left;font-size: 11px;text-align: center;"><i class="fa fa-arrow-left"></i></label>
				<input style="width: 75%;float: right; padding: 0px 0px 0px 4px;" type="number" min="1" step="0.1" class="form-control" id="margin-left" value="<?php echo ($doc_margins) ? $doc_margins['margin_left'] : 1; ?>">
			</div>
			<div style="float:left;max-width:15%;">
				<label style="width: 25%;float: left;font-size: 11px;text-align: center;"><i class="fa fa-arrow-up"></i></label>
				<input style="width: 75%;float: right; padding: 0px 0px 0px 4px;" type="number" min="1" step="0.1" class="form-control" id="margin-top" value="<?php echo ($doc_margins) ? $doc_margins['margin_top'] : 1; ?>">
			</div>
			<div style="float:left;max-width:15%;">
				<label style="width: 25%;float: left;font-size: 11px;text-align: center;"><i class="fa fa-arrow-right"></i></label>
				<input style="width: 75%;float: right; padding: 0px 0px 0px 4px;" type="number" min="1" step="0.1" class="form-control" id="margin-right" value="<?php echo ($doc_margins) ? $doc_margins['margin_right'] : 1; ?>">
			</div>
			<div style="float:left;max-width:15%;">
				<label style="width: 25%;float: left;font-size: 11px;text-align: center;"><i class="fa fa-arrow-down"></i></label>
				<input style="width: 75%;float: right; padding: 0px 0px 0px 4px;" type="number" min="1" step="0.1" class="form-control" id="margin-bottom" value="<?php echo ($doc_margins) ? $doc_margins['margin_bottom'] : 1; ?>">
			</div>			


			<button type="button" class="btn btn-info" id="save-margins" data-action="savedocumentcontentmargins" rel="<?php echo $document->getID(); ?>" title="<?php printMLText("save_pdf_margins"); ?>" style="margin-left: 5px;"><i class="fa fa-save"></i></button>

			<form action="/op/op.Templates.php" method="post" style="display: inline;">
				<input type="hidden" name="action" value="getpdfdownload">
				<input type="hidden" name="document_id" value="<?php echo $document->getID(); ?>">				
				<button type="submit" class="btn btn-success pull-right" data-action="getpdfdownload" rel="<?php echo $document->getID(); ?>" title="<?php printMLText("download_pdf"); ?>"><i class="fa fa-download"></i></button>
			</form>
		
	</div>
	<br>
	<br>

	<textarea id="editor"></textarea>

    <br>
    <div class="col-md-12">
    	<div class="">
          <button type="button" class="btn btn-default pull-left close-memo-editor" data-dismiss="modal"><?php printMLText("cancel"); ?></button>

          <?php if ( isset($temp_workflow) && $temp_workflow ): ?>
          <?php if ($temp_workflow->getID() == 59 || $temp_workflow->getID() == 60): ?> <!-- Memos o resoluciones -->

          	<?php 
          	// CHECK IF USER GROUP IS ALLOWED IN THE CURRENT DOCUMENT
			$oficiales = $dms->getGroup(168); // 165 = Oficiales de informacion

			$memo_owner = $document->getOwner();
			
			if (($user->getID() == $memo_owner->getID()) && $temp_workflowstate->getID() == 9) { // 9 = requiere cambios
				echo '<button type="button" class="btn btn-primary pull-right" id="save-content" rel="'.$document->getID().'"><i class="fa fa-save"></i> '.getMLText("save_changes").'</button>';
			}

			else if (($user->getID() == $memo_owner->getID()) && $temp_workflowstate->getID() == 5){ // 5 = elaborado
				echo '<button type="button" class="btn btn-primary pull-right" id="save-content" rel="'.$document->getID().'"><i class="fa fa-save"></i> '.getMLText("save_changes").'</button>';
			}

			else if ($oficiales->isMember($user) && $temp_workflowstate->getID() == 30) { // Memorandum creado
				echo '<button type="button" class="btn btn-primary pull-right" id="save-content" rel="'.$document->getID().'"><i class="fa fa-save"></i> '.getMLText("save_changes").'</button>';
			}

			else if ($oficiales->isMember($user) && $temp_workflowstate->getID() == 31) { // Resolución creada
				echo '<button type="button" class="btn btn-primary pull-right" id="save-content" rel="'.$document->getID().'"><i class="fa fa-save"></i> '.getMLText("save_changes").'</button>';
			}

			else if ($oficiales->isMember($user) && $temp_workflowstate->getID() == 13) { // Modificado
				echo '<button type="button" class="btn btn-primary pull-right" id="save-content" rel="'.$document->getID().'"><i class="fa fa-save"></i> '.getMLText("save_changes").'</button>';
			}

			?>
          
          <?php endif ?>
          <?php endif ?>
          

        </div>
    </div>
</div>
</div>
</div>
<?php /* ------------------------------------------- MEMO EDITOR ------------------------------------------- */ ?>


<div class="col-md-8" id="tab-information">

	<div class="box box-info">
   	<div class="box-header" style="padding: 0;">
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" id="document-tabs" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
	</div>
	<div class="box-body">

	<div class="nav-tabs-custom" style="margin-bottom: 0;">
    <ul class="nav nav-tabs" id="docinfotab">
		  <li class="<?php if(!$currenttab || $currenttab == 'docinfo') echo 'active'; ?>"><a href="#docinfo" data-toggle="tab" aria-expanded="true"><?php printMLText('current_version'); ?></a></li>
			<?php if (count($versions)>1) { ?>
		  <li class="<?php if($currenttab == 'previous') echo 'active'; ?>"><a href="#previous" data-toggle="tab" aria-expanded="false"><?php printMLText('previous_versions'); ?></a></li>
<?php
			}


			// CONTEO DE REQUERIMIENTOS
			if ($document->hasCategory(1) && (((string)$status['status'] == "2") || ((string)$status['status'] == "-1")) ) { ?>
				<li class="<?php if($currenttab == 'counter') echo 'active'; ?>"><a href="#counter" data-toggle="tab" aria-expanded="false"><?php printMLText('counter'); ?></a></li>
			<?php }


			if($workflowmode == 'traditional' || $workflowmode == 'traditional_only_approval') {
				if((is_array($reviewStatus) && count($reviewStatus)>0) ||
					(is_array($approvalStatus) && count($approvalStatus)>0)) {
?>
		  <li class="<?php if($currenttab == 'revapp') echo 'active'; ?>"><a href="#revapp" data-toggle="tab" aria-expanded="false"><?php if($workflowmode == 'traditional') echo getMLText('reviewers')."/"; echo getMLText('approvers'); ?></a></li>
<?php
				}
			} else {
				if($workflow) {
?>
		  <li class="<?php if($currenttab == 'workflow') echo 'active'; ?>"><a href="#workflow" data-toggle="tab" aria-expanded="false"><?php echo getMLText('workflow'); ?></a></li>
<?php
				}
			}
?>
		  	<li class="<?php if($currenttab == 'attachments') echo 'active'; ?>"><a href="#attachments" data-toggle="tab" aria-expanded="false"><?php printMLText('linked_files'); echo (count($files)) ? " (".count($files).")" : ""; ?></a></li>		  	


			<?php // Link for version log ?>
			<?php $version_wkflog = $latestContent->getWorkflowLog(); ?>

			<?php //if ($workflow) { ?>

				<!--<li class="<?php //if($currenttab == 'version_log') echo 'active'; ?>"><a href="#version_log" data-toggle="tab" aria-expanded="false"><?php //printMLText('version_log'); ?></a></li>-->

			<?php //} else if (($status['status']."-rejected") == "-1-rejected") { ?>

				<!--<li class="<?php //if($currenttab == 'version_log') echo 'active'; ?>"><a href="#version_log" data-toggle="tab" aria-expanded="false"><?php //printMLText('version_log'); ?></a></li>-->

			<?php //} ?>

			<?php if ($version_wkflog) { ?>
				<li class="<?php if($currenttab == 'version_log') echo 'active'; ?>"><a href="#version_log" data-toggle="tab" aria-expanded="false"><?php printMLText('version_log'); ?></a></li>
			<?php } ?>

		</ul>

		
		<div class="tab-content">

		<?php // TAB: Document info ?>
		<div class="tab-pane <?php if(!$currenttab || $currenttab == 'docinfo') echo 'active'; ?>" id="docinfo">
		<?php
		if(!$latestContent) {
			print getMLText('document_content_missing');
			$this->contentEnd();
			$this->mainFooter();		
			$this->containerEnd();
			$this->htmlEndPage();
			exit;
		}

		// verify if file exists
		$file_exists=file_exists($dms->contentDir . $latestContent->getPath());

		print "<div class=\"table-responsive\">";
		print "<table class=\"table table-striped\">";
		print "<thead>\n<tr>\n";
		print "<th width='*' class='align-center th-info-background'></th>\n";
		print "<th width='*' class='align-center th-info-background'>".getMLText("file")."</th>\n";
		print "<th width='25%' class='align-center th-info-background'>".getMLText("comment_for_current_version")."</th>\n";
		print "<th width='15%' class='align-center th-info-background'>".getMLText("status")."</th>\n";
		print "<th width='20%' class='align-center th-info-background'>".getMLText("actions")."</th>\n";
		print "</tr></thead><tbody>\n";
		print "<tr>\n";
		print "<td>";
		
		$previewer = new SeedDMS_Preview_Previewer($cachedir, $previewwidthdetail, $timeout);
		$previewer->createPreview($latestContent);
		if ($file_exists && !$document->isLocked()) {
			print "<a href=\"../op/op.Download.php?documentid=".$documentid."&version=".$latestContent->getVersion()."\">";
			/*if ($viewonlinefiletypes && in_array(strtolower($latestContent->getFileType()), $viewonlinefiletypes)) {
				print "<a target=\"_self\" href=\"../op/op.ViewOnline.php?documentid=".$documentid."&version=". $latestContent->getVersion()."\">";
			} else {
				print "<a href=\"../op/op.Download.php?documentid=".$documentid."&version=".$latestContent->getVersion()."\">";
			}*/
		}

		if($previewer->hasPreview($latestContent)) {

				print("<img class=\"mimeicon\" width=\"".$previewwidthdetail."\" src=\"../op/op.Preview.php?documentid=".$document->getID()."&version=".$latestContent->getVersion()."&width=".$previewwidthdetail."\" title=\"".htmlspecialchars($latestContent->getMimeType())."\">");

		} else {

			if (strtolower($latestContent->getFileType()) == '.pdf') {
				print "<i class='fa fa-file-pdf-o color-red fa-2x'  title=\"".htmlspecialchars($latestContent->getMimeType())."\"></i>";
			} else if (strtolower($latestContent->getFileType()) == '.doc' || strtolower($latestContent->getFileType()) == '.docx') {
				print "<i class='fa fa-file-word-o color-blue fa-2x'  title=\"".htmlspecialchars($latestContent->getMimeType())."\"></i>";
			} else if (strtolower($latestContent->getFileType()) == '.ppt' || strtolower($latestContent->getFileType()) == '.pptx') {
				print "<i class='fa  fa-file-powerpoint-o color-orange fa-2x'  title=\"".htmlspecialchars($latestContent->getMimeType())."\"></i>";
			} else if (strtolower($latestContent->getFileType()) == '.xls' || strtolower($latestContent->getFileType()) == '.xlsx') {
				print "<i class='fa fa-file-excel-o color-green fa-2x'  title=\"".htmlspecialchars($latestContent->getMimeType())."\"></i>";
			} else {			
				print "<img class=\"mimeicon\" src=\"".$this->getMimeIcon($latestContent->getFileType())."\" title=\"".htmlspecialchars($latestContent->getMimeType())."\">";
			}
			
		}
		if ($file_exists && !$document->isLocked()) {
			print "</a>";
		}
		print "</td>\n";
		print "<td><ul class=\"actions unstyled\">\n";
		print "<li class=\"wordbreak\">".$latestContent->getOriginalFileName() ."</li>\n";
		print "<li>".getMLText('version').": ".$latestContent->getVersion()."</li>\n";

		if ($file_exists)
			print "<li>". SeedDMS_Core_File::format_filesize($latestContent->getFileSize()) .", ".htmlspecialchars($latestContent->getMimeType())."</li>";
		else print "<li><span class=\"warning\">".getMLText("document_deleted")."</span></li>";

		$updatingUser = $latestContent->getUser();
		print "<li>".getMLText("uploaded_by")." <a href=\"mailto:".$updatingUser->getEmail()."\">".htmlspecialchars($updatingUser->getFullName())."</a></li>";
		print "<li>".getLongReadableDate($latestContent->getDate())."</li>";

    print "</ul></td>\n";

    print "<td>";
    print $latestContent->getComment();
    print "</td>";

		print "<ul class=\"actions unstyled\">\n";
		$attributes = $latestContent->getAttributes();
		if($attributes) {
      print "<td>";
			foreach($attributes as $attribute) {
				$arr = $this->callHook('showDocumentContentAttribute', $latestContent, $attribute);
				if(is_array($arr)) {
					print "<li>".$arr[0].": ".$arr[1]."</li>\n";
				} else {
					$attrdef = $attribute->getAttributeDefinition();
					print "<li>".htmlspecialchars($attrdef->getName()).": ".htmlspecialchars(implode(', ', $attribute->getValueAsArray()))."</li>\n";
				}
			}
      print "</ul></td>\n";
		}


		print "<td width='10%'>";

		if ($status["status"] == S_REJECTED) {
			$workflow_log = $latestContent->getWorkflowLog();
			$workflow_log_weight = count($workflow_log);
			$last_workflow_log_transition = $workflow_log[$workflow_log_weight-1];

			//var_dump($last_workflow_log_transition->getTransition()->getNextState()->getName());
			print '<p class="text-red align-center"><b>'.$last_workflow_log_transition->getTransition()->getNextState()->getName().'</b></p>';

		} else {
			print getOverallStatusText($status["status"]);
		}
		
		if ( $status["status"]==S_DRAFT_REV || $status["status"]==S_DRAFT_APP || $status["status"]==S_IN_WORKFLOW || $status["status"]==S_EXPIRED ){
			print "<br><span".($document->hasExpired()?" class=\"warning\" ":"").">".(!$document->getExpires() ? getMLText("does_not_expire") : getMLText("expires").": ".getReadableDate($document->getExpires()))."</span>";
		}
		print "</td>";

		print "<td class='align-center'>";

		// Document actions --------------------------------------------------------------------------------------------------------------- 

		echo "<div class=\"btn-group-horizontal\">";

		/* Block for allow view/download the file*/

		if ($file_exists){
			print "<a type=\"button\" class=\"btn btn-flat btn-primary btn-action\" href=\"../op/op.Download.php?documentid=".$documentid."&version=".$latestContent->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("download")."\"><i class=\"fa fa-download\"></i></a>";

			if (htmlspecialchars($latestContent->getMimeType()) == 'application/pdf' ) {
				print '<a type="button" class="btn btn-flat btn-info preview-doc-btn btn-action" id="'.$documentid.'" rel="'.$latestContent->getVersion().'" title="'.htmlspecialchars($document->getName()).' - '.getMLText('current_version').': '.$latestContent->getVersion().'"><i class="fa fa-eye"></i></a>';
			} else {
				print "<a type=\"button\" class=\"btn btn-flat btn-info btn-action\" target=\"_self\" href=\"../op/op.ViewOnline.php?documentid=".$documentid."&version=". $latestContent->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("preview")."\"><i class=\"fa fa-eye\"></i></a>";
			}
					
		}

		/* Only admin has the right to remove version in any case or a regular
		 * user if enableVersionDeletion is on
		 */
		if($user->isAdmin()) {
			//print "<a type=\"button\" class=\"btn btn-flat btn-danger btn-action\" href=\"../out/out.RemoveVersion.php?documentid=".$documentid."&version=".$latestContent->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("rm_version")."\"><i class=\"fa fa-times\"></i></a>";
		}
		if($user->isAdmin()) {
			if($status["status"] != -2){
				//print "<a type=\"button\" class=\"btn btn-flat btn-warning btn-action\" href='../out/out.OverrideContentStatus.php?documentid=".$documentid."&version=".$latestContent->getVersion()."' data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("change_status")."\"><i class=\"fa fa-align-justify\"></i></a>";
			}
		}

		if ($user->isAdmin()) {
			if($workflowmode == 'traditional' || $workflowmode == 'traditional_only_approval') {
				// Allow changing reviewers/approvals only if not reviewed
				if($accessop->maySetReviewersApprovers()) {
					print "<a type=\"button\" class=\"btn btn-flat btn-success btn-action\" href='../out/out.SetReviewersApprovers.php?documentid=".$documentid."&version=".$latestContent->getVersion()."' data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("change_assignments")."\"><i class=\"fa fa-edit\"></i></a>";
				}
			} else {
				if($accessop->maySetWorkflow()) {
					if($status["status"] != -2){
						if(!$workflow) {
							//print "<a type=\"button\" class=\"btn btn-flat btn-warning btn-action\" href='../out/out.SetWorkflow.php?documentid=".$documentid."&version=".$latestContent->getVersion()."' data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("set_workflow")."\"><i class=\"fa fa-random\"></i></a>";
						}
					}
				}
			}
		}

		$the_file_change_is_allowed = false;
		if ($workflow) {
			$thetransitions = $workflow->getTransitions();	

			foreach($thetransitions as $transition) {
				$transition_state_name = $transition->getState();
				$is_file_change_allowed = (int)$transition->getFileChangeAllowed();

				$actual_workflow_state_name = $workflowstate->getName();
				
				if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
					if ($actual_workflow_state_name === $transition_state_name->getName()) {
						if ($is_file_change_allowed) {
							$the_file_change_is_allowed = true;
						}
					}
				}
			}
		}		
		
		if ($needwkflaction || $user->isAdmin()){
	
			//print "<a type=\"button\" class=\"btn btn-flat btn-success btn-action\" href=\"out.EditComment.php?documentid=".$documentid."&version=".$latestContent->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("edit_comment")."\"><i class=\"fa fa-comment\"></i></a>";
				
			if($status["status"] != -2){
				if($the_file_change_is_allowed || $user->isAdmin()){
					// For document file and comment
					//print "<a type=\"button\" class=\"btn btn-flat btn-success btn-action\" href=\"out.EditAttributes.php?documentid=".$documentid."&version=".$latestContent->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("change_document_file")."\"><i class=\"fa fa-file\"></i></a>";

					// For document data
					//print '<a type="button" href="../out/out.EditDocument.php?documentid='.$documentid.'&showtree=1" class="btn btn-success btn-flat btn-action" data-toggle="tooltip" data-placement="bottom" title="'.getMLText("edit_document_props").'"><i class="fa fa-pencil"></i></a>';
				}
			}
		}

		$document_owner = $document->getOwner();

		//if (($status["status"] == S_RELEASED && $user->getID() == $document_owner->getID()) || $user->isAdmin()) {
		if ($status["status"] == S_RELEASED && $user->isAdmin()) {
			//print "<a type=\"button\" class=\"btn btn-primary btn-flat btn-action\" href=\"out.UpdateDocument.php?documentid=".$documentid."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("update_document")."\"><i class=\"fa fa-refresh\"></i></a>";
		}

		echo "</div>";
		print "</td>";
		print "</tr></tbody>\n</table>\n";
		print "</div>";

		/* ------------------------------------------------------------------------------------------------ */
?>
		</div> <!-- End first tab -->


		<?php // TAB: MEMO COUNTER ?>
		<div class="tab-pane <?php if($currenttab == 'counter') echo 'active'; ?>" id="counter">
		<?php
			if ($document->hasCategory(1) && (((string)$status['status'] == "2") || ((string)$status['status'] == "-1")) ) {
				$document_info_types = $dms->getDocumentInfoTypes($documentid);

				echo "<table class='table table-striped'>";				
				echo "<thead>";
				echo "<th width='10%' class='align-center th-info-background'><b>#</b></th>";
				echo "<th width='70%' class='align-center th-info-background'><b>".getMLText("information_type")."</b></th>";
				echo "<th width='20%' class='align-center th-info-background'><b>".getMLText("total")."</b></th>";
				echo "</thead>";
				echo "<tbody>";

				if ($document_info_types) {
					$counter = 1;
					$general_total = 0;
					foreach ($document_info_types as $info_types) {
						$information_type = $dms->getInformationTypeRequestedRow($info_types['information_type_id']);
						echo "<tr>";
						echo "<td class='align-center'>".$counter."</td>";
						if ($information_type) {
							echo "<td class=''>".$information_type['name']."</td>";
						} else {
							echo "<td class=''>N/A</td>";
						}
						echo "<td class='align-center'>".$info_types['count']."</td>";
						echo "</tr>";

						$general_total += (int)$info_types['count'];
						$counter++;
					}
					echo "<td></td>";
					echo "<td></td>";
					echo "<td class='align-center'><b>".$general_total."</b></td>";

				} else {
					echo "<tr><td colspan='3'>".getMLText("any_resgister_founded")."</td></tr>";
				}

				echo "</tbody>";
				echo "</table>";
			}
		?>
		</div> <!-- End counter tab -->

		<?php
		if($workflowmode == 'traditional' || $workflowmode == 'traditional_only_approval') {
			if((is_array($reviewStatus) && count($reviewStatus)>0) ||
				(is_array($approvalStatus) && count($approvalStatus)>0)) {
		?>

		<?php // TAB: Document workflow ?>
		<div class="tab-pane <?php if($currenttab == 'revapp') echo 'active'; ?>" id="revapp">
<?php
		$this->contentContainerstart();
		print "<table class=\"table-condensed\">\n";

		/* Just check fo an exting reviewStatus, even workflow mode is set
		 * to traditional_only_approval. There may be old documents which
		 * are still in S_DRAFT_REV.
		 */
		if (/*$workflowmode != 'traditional_only_approval' &&*/ is_array($reviewStatus) && count($reviewStatus)>0) {

			print "<tr><td colspan=5>\n";
			$this->contentSubHeading(getMLText("reviewers"));
			print "</tr>";

			print "<tr>\n";
			print "<td width='20%'><b>".getMLText("name")."</b></td>\n";
			print "<td width='20%'><b>".getMLText("last_update")."</b></td>\n";
			print "<td width='25%'><b>".getMLText("comment")."</b></td>";
			print "<td width='15%'><b>".getMLText("status")."</b></td>\n";
			print "<td width='20%'></td>\n";
			print "</tr>\n";

			foreach ($reviewStatus as $r) {
				$required = null;
				$is_reviewer = false;
				switch ($r["type"]) {
					case 0: // Reviewer is an individual.
						$required = $dms->getUser($r["required"]);
						if (!is_object($required)) {
							$reqName = getMLText("unknown_user")." '".$r["required"]."'";
						}
						else {
							$reqName = htmlspecialchars($required->getFullName()." (".$required->getLogin().")");
							if($required->getId() == $user->getId()/* && ($user->getId() != $owner->getId() || $enableownerrevapp == 1)*/)
								$is_reviewer = true;
						}
						break;
					case 1: // Reviewer is a group.
						$required = $dms->getGroup($r["required"]);
						if (!is_object($required)) {
							$reqName = getMLText("unknown_group")." '".$r["required"]."'";
						}
						else {
							$reqName = "<i>".htmlspecialchars($required->getName())."</i>";
							if($required->isMember($user)/* && ($user->getId() != $owner->getId() || $enableownerrevapp == 1)*/)
								$is_reviewer = true;
						}
						break;
				}
				print "<tr>\n";
				print "<td>".$reqName."</td>\n";
				print "<td><ul class=\"unstyled\"><li>".$r["date"]."</li>";
				/* $updateUser is the user who has done the review */
				$updateUser = $dms->getUser($r["userID"]);
				print "<li>".(is_object($updateUser) ? htmlspecialchars($updateUser->getFullName()." (".$updateUser->getLogin().")") : "unknown user id '".$r["userID"]."'")."</li></ul></td>";
				print "<td>".htmlspecialchars($r["comment"]);
				if($r['file']) {
					echo "<br />";
					echo "<a href=\"../op/op.Download.php?documentid=".$documentid."&reviewlogid=".$r['reviewLogID']."\" class=\"btn btn-mini\"><i class=\"icon-download\"></i> ".getMLText('download')."</a>";
				}
				print "</td>\n";
				print "<td>".getReviewStatusText($r["status"])."</td>\n";
				print "<td><ul class=\"unstyled\">";

				if($accessop->mayReview()) {
					if ($is_reviewer) {
						if ($r["status"]==0) {
							print "<li><a href=\"../out/out.ReviewDocument.php?documentid=".$documentid."&version=".$latestContent->getVersion()."&reviewid=".$r['reviewID']."\" class=\"btn btn-sm btn-warning\">".getMLText("add_review")."</a></li>";
						} elseif ($accessop->mayUpdateReview($updateUser) && (($r["status"]==1)||($r["status"]==-1))) {
							print "<li><a href=\"../out/out.ReviewDocument.php?documentid=".$documentid."&version=".$latestContent->getVersion()."&reviewid=".$r['reviewID']."\" class=\"btn btn-sm btn-warning\">".getMLText("edit")."</a></li>";
						}
					}
				}

				print "</ul></td>\n";	
				print "</tr>\n";
			}
		}

		if (is_array($approvalStatus) && count($approvalStatus)>0) {

			print "<tr><td colspan=5>\n";
			$this->contentSubHeading(getMLText("approvers"));
			print "</tr>";

			print "<tr>\n";
			print "<td width='20%'><b>".getMLText("name")."</b></td>\n";
			print "<td width='20%'><b>".getMLText("last_update")."</b></td>\n";	
			print "<td width='25%'><b>".getMLText("comment")."</b></td>";
			print "<td width='15%'><b>".getMLText("status")."</b></td>\n";
			print "<td width='20%'></td>\n";
			print "</tr>\n";

			foreach ($approvalStatus as $a) {
				$required = null;
				$is_approver = false;
				switch ($a["type"]) {
					case 0: // Approver is an individual.
						$required = $dms->getUser($a["required"]);
						if (!is_object($required)) {
							$reqName = getMLText("unknown_user")." '".$a["required"]."'";
						}
						else {
							$reqName = htmlspecialchars($required->getFullName()." (".$required->getLogin().")");
							if($required->getId() == $user->getId())
								$is_approver = true;
						}
						break;
					case 1: // Approver is a group.
						$required = $dms->getGroup($a["required"]);
						if (!is_object($required)) {
							$reqName = getMLText("unknown_group")." '".$a["required"]."'";
						}
						else {
							$reqName = "<i>".htmlspecialchars($required->getName())."</i>";
							if($required->isMember($user)/* && ($user->getId() != $owner->getId() || $enableownerrevapp == 1)*/)
								$is_approver = true;
						}
						break;
				}
				print "<tr>\n";
				print "<td>".$reqName."</td>\n";
				print "<td><ul class=\"unstyled\"><li>".$a["date"]."</li>";
				/* $updateUser is the user who has done the approval */
				$updateUser = $dms->getUser($a["userID"]);
				print "<li>".(is_object($updateUser) ? htmlspecialchars($updateUser->getFullName()." (".$updateUser->getLogin().")") : "unknown user id '".$a["userID"]."'")."</li></ul></td>";	
				print "<td>".htmlspecialchars($a["comment"]);
				if($a['file']) {
					echo "<br />";
					echo "<a href=\"../op/op.Download.php?documentid=".$documentid."&approvelogid=".$a['approveLogID']."\" class=\"btn btn-mini\"><i class=\"icon-download\"></i> ".getMLText('download')."</a>";
				}
				echo "</td>\n";
				print "<td>".getApprovalStatusText($a["status"])."</td>\n";
				print "<td><ul class=\"unstyled\">";

				if($accessop->mayApprove()) {
					if ($is_approver) {
						if ($a['status'] == 0) {
							print "<li><a class=\"btn btn-mini\" href=\"../out/out.ApproveDocument.php?documentid=".$documentid."&version=".$latestContent->getVersion()."&approveid=".$a['approveID']."\">".getMLText("add_approval")."</a></li>";
						} elseif ($accessop->mayUpdateApproval($updateUser) && (($a["status"]==1)||($a["status"]==-1))) {
							print "<li><a class=\"btn btn-mini\" href=\"../out/out.ApproveDocument.php?documentid=".$documentid."&version=".$latestContent->getVersion()."&approveid=".$a['approveID']."\">".getMLText("edit")."</a></li>";
						}
					}
				}

				print "</ul>";
				print "</td>\n</tr>\n";
			}
		}

		print "</table>\n";

		$this->contentContainerEnd();

		if($user->isAdmin()) {
?>
		<div class="row">
			<div class="col-md-12">
<?php

			/* Check for an existing review log, even if the workflowmode
			 * is set to traditional_only_approval. There may be old documents
			 * that still have a review log if the workflow mode has been
			 * changed afterwards.
			 */
			if($latestContent->getReviewStatus(10) /*$workflowmode != 'traditional_only_approval'*/) {
?>
				<div class="col-md-6">
				<?php $this->printProtocol($latestContent, 'review'); ?>
				</div>
<?php
			}
?>
				<div class="col-md-6">
				<?php $this->printProtocol($latestContent, 'approval'); ?>
				</div>
			</div>
		</div>
<?php
		}
?>
		  </div>
<?php
		}
		} else {

		///////////////////// Wokflow Advanced /////////////////////////
		$user_is_allowed = false;
		if($workflow) {
			/* Check if user is involved in workflow */
			$user_is_involved = false;
			foreach($transitions as $transition) {
				if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
					$user_is_involved = true;
				}
			}

			/* Check for allowed user */
			$check_user_assigned = $dms->getDocumentUserAssignment($documentid);
			if ($check_user_assigned) {					
				if(((int)$user->getID() == (int)$check_user_assigned['user_id'])) {
					$user_is_allowed = true;
				}			
			}

			$notificator = $dms->getGroup(167); // 167 = Notificadores
			if ($user->isMemberOfGroup($notificator)){
				$user_is_notificator = true;
			}
?>
		  <div class="tab-pane <?php if($currenttab == 'workflow') echo 'active'; ?>" id="workflow">
<?php
			echo "<div class=\"row\">";
			//echo "<div class=\"col-md-12 delete-wokflow-div\">";
			// user should be member of Administrador Transparencia
			/* Block for leave the request */
			/*$oi_group = $dms->getGroupByName("Administrador Transparencia");
			var_dump($user->isMemberOfGroup($oi_group));

			$status = $latestContent->getStatus();
			$actual_workflowstate = $latestContent->getWorkflowState();


			if ($user->isMemberOfGroup($oi_group) && $status['status'] != 2){
				echo "<div class='pull-left'>";
				print "<form action=\"../out/out.LeaveRequest.php\" method=\"post\">".createHiddenFieldWithKey('rewindworkflow')."<input type=\"hidden\" name=\"documentid\" value=\"".$documentid."\" /><input type=\"hidden\" name=\"folderid\" value=\"".$folder->getId()."\" /><input type=\"hidden\" name=\"version\" value=\"".$latestContent->getVersion()."\" /><button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-sign-out\"></i> ".getMLText('leave_request')."</button></form>";
				echo "</div>";
			}*/

			if($user->isAdmin()){
			
				//echo "<div class=\"delete-wokflow-div\">";

				/* Block for remove the workflow of the document*/
				if(SeedDMS_Core_DMS::checkIfEqual($workflow->getInitState(), $latestContent->getWorkflowState())) {
					/*echo "<div class='pull-right'>";
						print "<form action=\"../out/out.RemoveWorkflowFromDocument.php\" method=\"post\">".createHiddenFieldWithKey('removeworkflowfromdocument')."<input type=\"hidden\" name=\"documentid\" value=\"".$documentid."\" /><input type=\"hidden\" name=\"folderid\" value=\"".$folder->getId()."\" /><input type=\"hidden\" name=\"version\" value=\"".$latestContent->getVersion()."\" /><button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-times\"></i> ".getMLText('rm_workflow')."</button></form>";
					echo "</div>";*/
				}


				/* Block for rewind the workflow status */
				/*echo "<div class='pull-right'>";
				print "<form action=\"../out/out.RewindWorkflow.php\" method=\"post\">".createHiddenFieldWithKey('rewindworkflow')."<input type=\"hidden\" name=\"documentid\" value=\"".$documentid."\" /><input type=\"hidden\" name=\"folderid\" value=\"".$folder->getId()."\" /><input type=\"hidden\" name=\"version\" value=\"".$latestContent->getVersion()."\" /><button type=\"submit\" class=\"btn btn-warning\"><i class=\"fa fa-refresh\"></i> ".getMLText('rewind_workflow')."</button></form>";
				echo "</div>";*/

				//echo "</div>";
			}

			//echo "</div>";

			$msg_currentstate = $latestContent->getWorkflowState();
			$msg_wkflog = $latestContent->getWorkflowLog();
			$msg_workflow = $latestContent->getWorkflow();
			$msg = "";
			if($msg_workflow) {
				if ((int)$msg_currentstate->getID() == 15) {	// 15 = solicitud prevenida (state confirmed)
					if($msg_wkflog) {
						foreach($msg_wkflog as $entry) {
							if($entry->getTransition()->getNextState()->getID() == $msg_currentstate->getID()) {
								$enterdate = $entry->getDate();
								$enterts = makeTsFromLongDate($enterdate);
							}
						}
						$msg_unix_date = strtotime($enterdate);
						$msg .= getMLText("status_date_record_prevention").":&nbsp;<b>".date("d-m-Y H:i:s",$msg_unix_date)."</b><br/>";
						$msg .= getMLText("status_date_record_prevention_two").":&nbsp;<b>".getReadableDuration((time()-$enterts))."</b><br/>";
					}

					echo "<div class=\"col-md-12 col-no-padding\">";
					$this->warningMsg($msg);
					echo "</div>";
				}
			}			
			
			/*echo "<br/>";
			echo "<br/>";*/

			echo "<div class=\"col-md-12 col-no-padding\">";
			echo "<div class=\"box box-primary box-solid\" id=\"workflow-information\">";
      		echo "<div class=\"box-header with-border\">";
      		echo "<h5 class=\"\"><strong>".getMLText("workflow").":</strong> ".$workflow->getName()." - (".getMLText('current_state').": ".$workflowstate->getName().")</h5>";
      		
			if($cats = $document->getCategories()) {
				foreach($cats as $cat) {
					if($cat->getName() == "Memo"){
						$memo_cat = true;
					}
				}
			}

			$actual_workflowstate = $latestContent->getWorkflowState();
			$user_is_member = false;

			// The current workflow to allow transitions
			$current_workflow_id = $workflow->getID();

			if ((int)$current_workflow_id == 59) { // 59 = memos
				// "Remitido" is an specific workflow state name		
				if ($status['status'] != 2 && (int)$actual_workflowstate->getID() === 44) { // 44 = remitido - memos
					$group = $dms->getDocumentGroupAssignment($documentid);
					
					$real_group = $dms->getGroup($group['group_id']);
					
					echo "<h5 class=\"\"><strong>".getMLText("group_responsible").":</strong> ".$real_group->getName()."</h5>";
				}
			}

			// Si es una SIP entonces se asigna despues de Solicitud presentada

			if ((int)$current_workflow_id == 12) { // 12 = solicitudes
				if ($status['status'] != 2 && (int)$actual_workflowstate->getID() != 36) { // 36 = Solicitud presentada
					$user_assignment = $dms->getDocumentUserAssignment($documentid);
					
					if ($user_assignment) {
						$user_assg = $dms->getUser($user_assignment['user_id']);
						echo "<h5 class=\"\"><strong>".getMLText("user_asigned").":</strong> ".$user_assg->getFullName()."</h5>";	
					}					
				}
			}

			// Si es una CC entonces se asigna despues de Elaborado

			if ((int)$current_workflow_id == 9) { // 9 = consultas
				if ($status['status'] != 2 && $actual_workflowstate->getID() != 5) { // 5 = elaborado
					$user_assignment = $dms->getDocumentUserAssignment($documentid);
					if ($user_assignment) {
						$user_assg = $dms->getUser($user_assignment['user_id']);
						echo "<h5 class=\"\"><strong>".getMLText("user_asigned").":</strong> ".$user_assg->getFullName()."</h5>";
					}
				}
			}


      		echo "</div>";
      		echo "<div class=\"box-body\">";

			if($parentworkflow = $latestContent->getParentWorkflow()) {
				echo "<p>Sub workflow of '".$parentworkflow->getName()."'</p>";
			}			

			echo "<div class='table-responsive'>";
			echo "<table class=\"table table-bordered\" style='border: 1px solid #888888;'>\n";
			echo "<tr>";
			echo "<td style='border: 1px solid #888888;'><strong>".getMLText('next_state').":</strong></td>";
			foreach($transitions as $transition) {
				$nextstate = $transition->getNextState();
				$docstatus = $nextstate->getDocumentStatus();
				echo "<td class='' style='border: 1px solid #888888;'>
				<p style='margin:0;text-align:center;'><i class=\"fa fa-arrow-circle-down".($docstatus == S_RELEASED ? " released" : ($docstatus == S_REJECTED ? " rejected" : " in-workflow"))."\"></i></p> 
				<p style='margin:0;color:#232323;text-align:center;'>".$nextstate->getName()."</p></td>";
			}
			echo "</tr>";
			echo "<tr>";
			echo "<td style='border: 1px solid #888888;'><strong>".getMLText('action').":</strong></td>";
			foreach($transitions as $transition) {
				$action = $transition->getAction();
				echo "<td class='td-action-bkg-two' style='border: 1px solid #888888;'>
				<p style='margin:0;text-align:center;'><i class=\"fa fa-cog\"></i></p>
				<p style='margin:0;text-align:center;'>".getMLText('action_'.strtolower($action->getName()), array(), $action->getName())."</p>
				</td>";
			}
			echo "</tr>";
			echo "<tr>";
			echo "<td></td>";

			$allowedtransitions = array();
			$curren_number_version = $latestContent->getVersion();

			foreach($transitions as $transition) {

				echo "<td class='align-center'>";
				
				if((int)$current_workflow_id == 59) { // 59 = memos
					// Get user assignment
					$user_assigned = $dms->getDocumentUserAssignment($documentid);

					// Cuando es un memo solo se permitira una accion Responder para grupos asignados
					if ($status['status'] != 2 && (int)$actual_workflowstate->getID() === 44){ // 44 = remitido - memos
						$assigned_group = $dms->getDocumentGroupAssignment($documentid);
						$the_assigned_group = $dms->getGroup($assigned_group['group_id']);

						if ($the_assigned_group->isMember($user)) {
							$this->printButtonActionForAllowedUser($transition, $documentid, $curren_number_version);
							$allowedtransitions[] = $transition;
						}
						
					} else if($user_assigned) { // Individual user assigned

						if(((int)$user->getID() == (int)$user_assigned['user_id']) && (int)$transition->getUserAllowed()) {

							$this->printButtonActionForAllowedUser($transition, $documentid, $curren_number_version);
							$allowedtransitions[] = $transition;
						
						} else {
							// Check default workflow behavior
							if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
								$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
								$allowedtransitions[] = $transition;
							}
						}

					} else {
						// Check default workflow behavior
						if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
							$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
							$allowedtransitions[] = $transition;
						}
					}


		
				} else if((int)$current_workflow_id == 60) { // 60 = resoluciones

					// Get user assignment
					$user_assigned = $dms->getDocumentUserAssignment($documentid);

					if($user_assigned){ // Individual user assigned
						if(((int)$user->getID() == (int)$user_assigned['user_id']) && (int)$transition->getUserAllowed()) {

							$this->printButtonActionForAllowedUser($transition, $documentid, $curren_number_version);
							$allowedtransitions[] = $transition;
							
						} else {
							// Check default workflow behavior
							if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
								$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
								$allowedtransitions[] = $transition;
							}
						}

					} else {
						// Check default workflow behavior
						if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
							$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
							$allowedtransitions[] = $transition;
						}
					}

				} else if((int)$current_workflow_id == 12) { // 12 = solicitudes

					if ($status['status'] != 2 && (int)$actual_workflowstate->getID() === 5){ // 5 = elaborado

						if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
							$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
							$allowedtransitions[] = $transition;
						}

					} else {
						// Get user assignment
						$user_assigned = $dms->getDocumentUserAssignment($documentid);

						if($user_assigned){ // Individual user assigned
							if(((int)$user->getID() == (int)$user_assigned['user_id']) && (int)$transition->getUserAllowed()) {
								$this->printButtonActionForAllowedUser($transition, $documentid, $curren_number_version);
								$allowedtransitions[] = $transition;								

							} else {
								// Check default workflow behavior
								if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
									$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
									$allowedtransitions[] = $transition;
								}
							}
						} else {
							// Check default workflow behavior
							if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
								$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
								$allowedtransitions[] = $transition;
							}
						}
					}



				} else if((int)$current_workflow_id == 9) { // 9 = consultas				

					if ($status['status'] != 2 && (int)$actual_workflowstate->getID() === 5){ // 5 = elaborado

						if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
							$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
							$allowedtransitions[] = $transition;
						}

					} else {
						// Get user assignment
						$user_assigned = $dms->getDocumentUserAssignment($documentid);

						if($user_assigned){ // Individual user assigned
							if(((int)$user->getID() == (int)$user_assigned['user_id']) && (int)$transition->getUserAllowed()) {
								$this->printButtonActionForAllowedUser($transition, $documentid, $curren_number_version);
								$cc_user_is_in_action = true;
								$allowedtransitions[] = $transition;
							} else {
								// Check default workflow behavior
								if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
									$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
									$cc_user_is_in_action = true;
									$allowedtransitions[] = $transition;
								}
							}
						} else {
							// Check default workflow behavior
							if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
								$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);
								$cc_user_is_in_action = true;
								$allowedtransitions[] = $transition;
							}
						}
					}

				} else {

					// Check default workflow behavior
					if($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
						$this->printDefaultButtonAction($transition, $documentid, $curren_number_version);						
						$allowedtransitions[] = $transition;
					}

				}

				echo "</td>";
			}
			echo "</tr>";
			echo "</table>";
			echo "</div>";
			echo "</div>";
			echo "</div>";
			echo "</div>";
			echo "</div>";

?>
		</div> <!-- End wokflow tab -->
<?php
			}
		}
		if (count($versions)>1) {
?>
		  <?php // TAB: Document versions ?>
		  <div class="tab-pane <?php if($currenttab == 'previous') echo 'active'; ?>" id="previous">
<?php
			//$this->contentContainerStart();
			print "<div class=\"table-responsive\">";
			print "<table class=\"table table-striped\">";
			print "<thead>\n<tr>\n";
			print "<th width='10%'></th>\n";
			print "<th width='30%'>".getMLText("file")."</th>\n";
			print "<th width='25%'>".getMLText("comment")."</th>\n";
			print "<th width='15%'>".getMLText("status")."</th>\n";
			print "<th width='20%'></th>\n";
			print "</tr>\n</thead>\n<tbody>\n";

			for ($i = count($versions)-2; $i >= 0; $i--) {
				$version = $versions[$i];
				$vstat = $version->getStatus();
				$workflow = $version->getWorkflow();
				$workflowstate = $version->getWorkflowState();

				// verify if file exists
				$file_exists=file_exists($dms->contentDir . $version->getPath());

				print "<tr>\n";
				print "<td nowrap>";
				if($file_exists) {
					if ($viewonlinefiletypes && in_array(strtolower($version->getFileType()), $viewonlinefiletypes)) {
							print "<a target=\"_self\" href=\"../op/op.ViewOnline.php?documentid=".$documentid."&version=".$version->getVersion()."\">";
					} else {
						print "<a href=\"../op/op.Download.php?documentid=".$documentid."&version=".$version->getVersion()."\">";
					}
				}
				$previewer->createPreview($version);
				if($previewer->hasPreview($version)) {
					print("<img class=\"mimeicon\" width=\"".$previewwidthdetail."\" src=\"../op/op.Preview.php?documentid=".$document->getID()."&version=".$version->getVersion()."&width=".$previewwidthdetail."\" title=\"".htmlspecialchars($version->getMimeType())."\">");
				} else {
					print "<img class=\"mimeicon\" src=\"".$this->getMimeIcon($version->getFileType())."\" title=\"".htmlspecialchars($version->getMimeType())."\">";
				}
				if($file_exists) {
					print "</a>\n";
				}
				print "</td>\n";
				print "<td><ul class=\"unstyled\">\n";
				print "<li>".$version->getOriginalFileName()."</li>\n";
				print "<li>".getMLText('version').": ".$version->getVersion()."</li>\n";
				if ($file_exists) print "<li>". SeedDMS_Core_File::format_filesize($version->getFileSize()) .", ".htmlspecialchars($version->getMimeType())."</li>";
				else print "<li><span class=\"warning\">".getMLText("document_deleted")."</span></li>";
				$updatingUser = $version->getUser();
				print "<li>".getMLText("uploaded_by")." <a href=\"mailto:".$updatingUser->getEmail()."\">".htmlspecialchars($updatingUser->getFullName())."</a></li>";
				print "<li>".getLongReadableDate($version->getDate())."</li>";
				print "</ul>\n";
				print "<ul class=\"actions unstyled\">\n";
				$attributes = $version->getAttributes();
				if($attributes) {
					foreach($attributes as $attribute) {
						$arr = $this->callHook('showDocumentContentAttribute', $version, $attribute);
						if(is_array($arr)) {
							print "<li>".$arr[0].": ".$arr[1]."</li>\n";
						} else {
							$attrdef = $attribute->getAttributeDefinition();
							print "<li>".htmlspecialchars($attrdef->getName()).": ".htmlspecialchars(implode(', ', $attribute->getValueAsArray()))."</li>\n";
						}
					}
				}
				print "</ul></td>\n";
				print "<td>".htmlspecialchars($version->getComment())."</td>";
				print "<td>".getOverallStatusText($vstat["status"])."</td>";
				print "<td>";

				print "<div class=\"btn-group-horizontal\">";

				if ($file_exists){
					print "<a type=\"button\" class=\"btn btn-flat btn-primary btn-action\" href=\"../op/op.Download.php?documentid=".$documentid."&version=".$version->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("download")."\"><i class=\"fa fa-download\"></i></a>";
					if (htmlspecialchars($latestContent->getMimeType()) == 'application/pdf' ) {
						print '<a type="button" class="btn btn-flat btn-info preview-doc-btn btn-action" id="'.$documentid.'" rel="'.$version->getVersion().'" title="'.htmlspecialchars($document->getName()).' - '.getMLText('current_version').': '.$version->getVersion().'"><i class="fa fa-eye"></i></a>';
					} else {
						print "<a type=\"button\" class=\"btn btn-flat btn-info btn-action\" target=\"_self\" href=\"../op/op.ViewOnline.php?documentid=".$documentid."&version=". $latestContent->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("preview")."\"><i class=\"fa fa-eye\"></i></a>";
					}

				}
				/* Only admin has the right to remove version in any case or a regular
				 * user if enableVersionDeletion is on
				 */
				if($user->isAdmin()) {
					print "<a type=\"button\" class=\"btn btn-flat btn-danger btn-action\" href=\"out.RemoveVersion.php?documentid=".$documentid."&version=".$version->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("rm_version")."\"><i class=\"fa fa-remove\"></i></a>";
				}
				if($user->isAdmin()) {
					print "<a type=\"button\" class=\"btn btn-flat btn-success btn-action\" href=\"out.EditComment.php?documentid=".$document->getID()."&version=".$version->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("edit_comment")."\"><i class=\"fa fa-comment\"></i></a>";
				}
				if($user->isAdmin()) {
					print "<a type=\"button\" class=\"btn btn-flat btn-success btn-action\" href=\"out.EditAttributes.php?documentid=".$document->getID()."&version=".$version->getVersion()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("edit_attributes")."\"><i class=\"fa fa-edit\"></i></a>";
				}
				print "<a type=\"button\" class=\"btn btn-flat btn-info btn-action\" href='../out/out.DocumentVersionDetail.php?documentid=".$documentid."&version=".$version->getVersion()."' data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("details")."\"><i class=\"fa fa-list\"></i></a>";

				print "</div>\n";
				print "</td>\n</tr>\n";
			}
			print "</tbody>\n</table>\n";
			print "</div>\n";
			
			//$this->contentContainerEnd();
?>
		  </div>
<?php
		}
?>		
		<?php // TAB: Document attachments ?>
		<div class="tab-pane <?php if($currenttab == 'attachments') echo 'active'; ?>" id="attachments">
<?php

		//$this->contentContainerStart();  ///////////////////////////////////////////////////////////////////

		if (count($files) > 0) {
			print "<div class=\"table-responsive\">";
			print "<table class=\"table table-striped\">";
			print "<thead>\n<tr>\n";
			print "<th width='10%' class='align-center td-warning-background'></th>\n";
			print "<th width='20%' class='align-center td-warning-background'>".getMLText("file")."</th>\n";
			print "<th width='30%' class='align-center td-warning-background'>".getMLText("comment")."</th>\n";
			print "<th width='10%' class='align-center td-warning-background'>".getMLText("public")."</th>\n";
			print "<th width='30%' class='align-center td-warning-background'>".getMLText("actions")."</th>\n";
			print "</tr>\n</thead>\n<tbody>\n";

			foreach($files as $file) {
				$file_exists=file_exists($dms->contentDir . $file->getPath());

				$responsibleUser = $file->getUser();

				print "<tr>";
				print "<td class='align-center'>";
				$previewer->createPreview($file, $previewwidthdetail);
				if($file_exists) {
						print "<a href=\"../op/op.Download.php?documentid=".$documentid."&file=".$file->getID()."\">";
				}
				if($previewer->hasPreview($file)) {
					print("<img class=\"mimeicon\" width=\"".$previewwidthdetail."\" src=\"../op/op.Preview.php?documentid=".$document->getID()."&file=".$file->getID()."&width=".$previewwidthdetail."\" title=\"".htmlspecialchars($file->getMimeType())."\">");
				} else {

					if (strtolower($file->getFileType()) == '.pdf') {
						print "<i class='fa fa-file-pdf-o color-red fa-2x'  title=\"".htmlspecialchars($file->getMimeType())."\"></i>";
					} else if (strtolower($file->getFileType()) == '.doc' || strtolower($file->getFileType()) == '.docx') {
						print "<i class='fa fa-file-word-o color-blue fa-2x'  title=\"".htmlspecialchars($file->getMimeType())."\"></i>";
					} else if (strtolower($file->getFileType()) == '.ppt' || strtolower($file->getFileType()) == '.pptx') {
						print "<i class='fa  fa-file-powerpoint-o color-orange fa-2x'  title=\"".htmlspecialchars($file->getMimeType())."\"></i>";
					} else if (strtolower($file->getFileType()) == '.xls' || strtolower($file->getFileType()) == '.xlsx') {
						print "<i class='fa fa-file-excel-o color-green fa-2x'  title=\"".htmlspecialchars($file->getMimeType())."\"></i>";
					} else {

						print "<img class=\"mimeicon\" src=\"".$this->getMimeIcon($file->getFileType())."\" title=\"".htmlspecialchars($file->getMimeType())."\">";
					}

				}
				if($file_exists) {
					print "</a>";
				}
				print "</td>";
				
				print "<td>\n";
				print "<span>".htmlspecialchars($file->getName())."</span><br>";
				print "<i>".getMLText("uploaded_by")." <a href=\"mailto:".$responsibleUser->getEmail()."\">".htmlspecialchars($responsibleUser->getFullName())."</a></i>";
				print "</td>";
				print "<td>".htmlspecialchars($file->getComment())."</td>";
				
				print "<td class='align-center'>";
				if ((int)$file->isPublic()) {
					printMLText("yes");
				} else {
					printMLText("no");
				}
				print "</td>";

				print "<td>";
				print "<div class=\"btn-group-horizontal align-center\">";

				if ($file_exists) {
					print "<a type=\"button\" class=\"btn btn-flat btn-primary btn-action\" href=\"../op/op.Download.php?documentid=".$documentid."&file=".$file->getID()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("download")."\"><i class=\"fa fa-download\"></i></a>";

					if (htmlspecialchars($file->getMimeType()) == 'application/pdf' ) {
						print '<a type="button" class="btn btn-flat btn-info preview-attach-btn btn-action" id="'.$documentid.'" rel="'.$file->getID().'" data-toggle=\"tooltip\" data-placement=\"bottom\" title="'.htmlspecialchars($file->getName()).'"><i class="fa fa-eye"></i></a>';
					} else {
						print "<a type=\"button\" class=\"btn btn-flat btn-info btn-action\" target=\"_self\" href=\"../op/op.ViewOnline.php?documentid=".$documentid."&file=".$file->getID()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("view_online")."\"><i class=\"fa fa-eye\"></i></a>";
					}

				}

				
				if ($user->isAdmin()) {
					if ($status['status'] != 2 && $status['status'] != -1) {
						print "<a type=\"button\" class=\"btn btn-flat btn-success btn-action\" href=\"out.EditDocumentFile.php?documentid=".$documentid."&fileid=".$file->getID()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("edit")."\"><i class=\"fa fa-pencil\"></i></a>";
					}	
				}
								
				if($user->isAdmin()){
					if ($status['status'] != 2 && $status['status'] != -1) {
						print "<a type=\"button\" class=\"btn btn-flat btn-danger btn-action\" href=\"out.RemoveDocumentFile.php?documentid=".$documentid."&fileid=".$file->getID()."\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"".getMLText("delete")."\"><i class=\"fa fa-remove\"></i></a>";
					}
				}

				$docowner = $document->getOwner();
				if ($status['status'] != -1 && $document->hasCategory(19)) { // 19 = MEMO CATEGORY
					if(($docowner->getID() == $user->getID()) || $user->isAdmin()){
						print '<form action="/op/op.AddMemoAttachToSIP.php" method="post" style="display:inline-block;">'.createHiddenFieldWithKey('addattachtosip').'
							<input type="hidden" name="documentid" value="'.$documentid.'" />
							<input type="hidden" name="fileid" value="'.$file->getID().'" />
								<button title="Transferir adjunto al documento de la solicitud" style="display:inline;" type="submit" class="btn btn-flat btn-success btn-action"><i class="fa fa-exchange"></i></button></form>';
					}
				}

				print "</div>";		
				print "</td>";			
				print "</tr>";
			}
			print "</tbody>\n</table>\n";	
			print "</div>\n";

		} else {
			echo "<p class='label label-warning' style='font-size:12px;padding:8px;'><i class='fa fa-warning'></i>&nbsp;&nbsp;".getMLText("no_attached_files")."</p><br/><br/>";
		}
		
		
		if($needwkflaction || $user->isAdmin()){
			if ($status['status'] != 2 && $status['status'] != -1) {
				print "<a type=\"button\" href=\"../out/out.AddFile.php?documentid=".$documentid."\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> ".getMLText("add")."</a>";
			}
		} else if($memo_cat) {

			$user_is_member = false;
			if ($status['status'] != 2 && (int)$actual_workflowstate->getID() === 44){ // 44 = remitido - memos
				$assigned_group = $dms->getDocumentGroupAssignment($documentid);
				$the_assigned_group = $dms->getGroup($assigned_group['group_id']);
							
				if ($the_assigned_group->isMember($user)) {
					$user_is_member = true;
				}
			}

			if ($status['status'] != 2 && (int)$actual_workflowstate->getID() === 44 && $user_is_member) { // 44 = remitido - memos
				print "<a type=\"button\" href=\"../out/out.AddFile.php?documentid=".$documentid."\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> ".getMLText("add")."</a>";
			}

		} else if ($user_is_allowed || $user_is_notificator){
			if ($status['status'] != 2 && $status['status'] != -1) {
				print "<a type=\"button\" href=\"../out/out.AddFile.php?documentid=".$documentid."\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> ".getMLText("add")."</a>";
			}
		}
			
?>
		</div> <!-- Ends attachment tab -->

		<?php // TAB: Document links to other documents // REMOVED ?>
		

		<?php // TAB: Version log 
			$version_wkflog = $latestContent->getWorkflowLog();

				//var_dump($version_wkflog);
				//var_dump((($status['status']."-rejected") == "-1-rejected"));
				//var_dump(getOverallStatusText($status["status"]));
				?>
				<?php $wkflow = $latestContent->getWorkflow(); ?>
				<?php //if ($wkflow && $version_wkflog) { ?>
				<?php //if (($wkflow && $version_wkflog) || (($status['status']."-rejected") == "-1-rejected")) { ?>
				<?php if ($version_wkflog) { ?>
				<div class="tab-pane <?php if($currenttab == 'version_log') echo 'active'; ?>" id="version_log">
					<?php
							echo "<div class=\"table-responsive\">";
							echo "<table class=\"table table-bordered table-striped table-hover\">";
							echo "<tr>
								<th width='' class=\"align-center th-info-background\" style='border: 1px solid #888888;'>#</th>
								<th width='' class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('action')."</th>
								<th width='' class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('from')."</th>
								<th width='' class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('to')."</th>
								<th width='' class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('date')."</th>
								<th width='' class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('user_action_executed')."</th>
								<th width='' class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('comment')."</th>
								</tr>";
							$i = 1;
							foreach($version_wkflog as $entry) {
								echo "<tr>";
								echo "<td style='border: 1px solid #888888;'>".$i."</td>";
								//echo "<td style='border: 1px solid #888888;'>".getMLText('action')." ".strtolower($entry->getTransition()->getAction()->getName())."</td>";
								echo "<td style='border: 1px solid #888888;'>".$entry->getTransition()->getAction()->getName()."</td>";
								echo "<td style='border: 1px solid #888888;'>".$entry->getTransition()->getState()->getName()."</td>";
								echo "<td style='border: 1px solid #888888;'>".$entry->getTransition()->getNextState()->getName()."</td>";
								echo "<td style='border: 1px solid #888888;'>".date("d-m-Y H:i:s", strtotime($entry->getDate()))."</td>";
								echo "<td style='border: 1px solid #888888;'>".$entry->getUser()->getFullname()."</td>";
								echo "<td style='border: 1px solid #888888;'>".$entry->getComment()."</td>";
								echo "</tr>";
								
								$i++;
							}
							echo "</table>\n";
							echo "</div>";
					?>
				</div>
				<?php } ?>
			</div>
		</div>


	<?php 

		$owner_id = $document->getOwner()->getID();
		
		if ($status['status'] === '2' || $status['status'] === '-1') {
			$user_assigned = $dms->getDocumentUserAssignment($documentid); //Get user assignment
				$this->startBoxPrimary();

				$lcs = $document->getContent();
			    foreach($lcs as $lcc) {
			        $versions[] = array(
			            'version'=>$lcc->getVersion(),
			            'date'=>date('Y-m-d H:i:s', $lcc->getDate()),
			            'size'=>$lcc->getFileSize(),
			            'status' =>$lcc->getStatus()
			        );
			    }

			    // Get the actual version of document workflow if exists or not
			    $versions_counter = count($versions);
			    $actual_version = $versions[$versions_counter - 1];
			    $timestamp = strtotime($actual_version['status']['date']);
			    $date_publish_version = date('d-m-Y', $timestamp);
			    
				echo '<div class="col-md-6 col-xs-12">';
				echo '<table class="table">';
				echo '<tbody>';

				echo '<tr>';
				echo '<td><b>'.getMLText('current_version').':</b></td>';
				echo '<td>'.$actual_version['version'].'</td>';
				echo '<td></td>';
				echo '</tr>';

				echo '<tr>';
				echo '<td><b>'.getMLText('update_version_date').':</b></td>';
				echo '<td><span id="actual-date">'.$date_publish_version.'</span></td>';

				if (($owner_id === $user->getID()) || $user->isAdmin() || (int)$user->getID() == (int)$user_assigned['user_id']) {
					echo '<td><button id="edit-version-date" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> '.getMLText('edit').'</button></td>';
					echo '<td></td>';
				} else {
					echo '<td></td>';
				}

				echo '</tr>';

				echo '</tbody>';
				echo '</table>';
				echo '</div>';
				echo '<div class="col-md-6">';
				echo '</div>';


				// Edit date form 
				echo '<div class="col-md-6 col-xs-12 hidden" id="date-div">';
				echo '<form id="update-version-date" method="get">';
				echo '<input type="hidden" id="statuslogid" value="'.$actual_version['status']['statusLogID'].'">';
				echo '<table class="table">';
				echo '<tbody>';

				echo '<tr>';
				echo '<td><b>'.getMLText('new_date').':</b></td>';
				echo '<td>';
				?>
				<div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="version-date" required="required">
                </div>
				<?php
				echo '</td>';
				echo '<td><button id="save-version-date" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> '.getMLText('save').'</button></td>';
				echo '</tr>';

				echo '</tbody>';
				echo '</form>';
				echo '</table>';

				echo '</div>';

				$this->endsBoxPrimary();
				
		}

		/// CONSULTAS RESPUESTA //

		/// VERIFICAR QUE LOS USUARIOS QUE LE DAN SEGUIMIENTO A LAS TRANSICIONES PUEDAN VER ESTO Y SUBIR ADJUNTOS

		if($workflow) {
			//var_dump($workflow->getID());
			if($user_is_allowed || $user->isAdmin()){
				if ((int)$current_workflow_id == 9) { // 9 = consultas

					$cc_response = $dms->getDocumentAttributeValue(85, $document->getID());

					if ($status['status'] != 2 && (int)$actual_workflowstate->getID() != 5) { // 5 = elaborado
						$this->startBoxPrimary(getMLText("cc_response"));
							echo '<div class="col-md-12">';
							echo '<form id="update-cc-response" action="/op/op.CCMgr.php" method="post">';
							echo createHiddenFieldWithKey('ccresponseform');
							
							if ($cc_response) {
								echo '<input type="hidden" name="action" value="editccresponse">';
								$cc_response_text = $cc_response;
							} else {
								echo '<input type="hidden" name="action" value="addccresponse">';	
								$cc_response_text = "";
							}
							
							echo '<input type="hidden" name="documentid" value="'.$documentid.'" />';
							echo '<textarea width="100%" style="padding:8px;width:100%;height:200px;" id="cc_response_text" name="cc_response_text" required>'.$cc_response_text.'</textarea>';
							echo '<br/>';

							if ($cc_user_is_in_action) { // Shows only if user is in action and workflow is CC
								echo '<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> '.getMLText("save").'</button>';
							}


							echo '</form>';
							echo '</div>';
						$this->endsBoxPrimary();
					}
				}
			}
		} else {

			$cc_response = $dms->getDocumentAttributeValue(85, $document->getID()); // 85 attribute for cc response
			if ($cc_response) {
				$this->startBoxPrimary(getMLText("cc_response"));
				echo '<div class="col-md-12">';
				echo '<textarea width="100%" style="padding:8px;width:100%;height:200px;" id="cc_response_text" name="cc_response_text" required readonly>'.$cc_response.'</textarea>';
				echo '</div>';
				$this->endsBoxPrimary();
			}
		}


	?>

</div>
</div>
</div>
</div> <!-- Ends All Tabs -->

<?php
if($user->isAdmin()) {
$timeline = $document->getTimeline(); ?>
<div class="row" id="thetimeline">
	<div class="col-md-12">
		<div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo getMLText("timeline"); ?></h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
      	<?php
      		if($timeline) {
						foreach($timeline as &$item) {
							switch($item['type']) {
							case 'add_version':
								$msg = getMLText('timeline_'.$item['type'], array('document'=>$item['document']->getName(), 'version'=> $item['version']));
								break;
							case 'add_file':
								$msg = getMLText('timeline_'.$item['type'], array('document'=>$item['document']->getName()));
								break;
							case 'status_change':
								$msg = getMLText('timeline_'.$item['type'], array('document'=>$item['document']->getName(), 'version'=> $item['version'], 'status'=> $item['status']));
								break;
							default:
								$msg = $this->callHook('getTimelineMsg', $document, $item);
								if(!is_string($msg))
									$msg = '???';
							}
							$item['msg'] = $msg;
						}
		//				$this->printTimeline('out.ViewDocument.php?action=timelinedata&documentid='.$document->getID(), 300, '', date('Y-m-d'));
						$this->printTimelineHtml(300);
					}
				?>
      </div>
	</div>
</div>
</div>
<?php } ?>
		
<?php
		//// Document preview ////
		echo "<div class=\"row div-hidden\" id=\"document-previewer\">";
		echo "<div class=\"col-md-12\">";
		echo "<div class=\"box box-info\">";
		echo "<div class=\"box-header with-border box-header-doc-preview\">";
    echo "<span id=\"doc-title\" class=\"box-title\"></span>";
    echo "<span class=\"pull-right\">";
    //echo "<a class=\"btn btn-sm btn-primary\"><i class=\"fa fa-chevron-left\"></i></a>";
    //echo "<a class=\"btn btn-sm btn-primary\"><i class=\"fa fa-chevron-right\"></i></a>";
    echo "<a class=\"close-doc-preview btn btn-box-tool\"><i class=\"fa fa-times\"></i></a>";
    echo "</span>";
    echo "</div>";
    echo "<div class=\"box-body\">";
    echo "<iframe id=\"iframe-charger\" src=\"\" width=\"100%\" height=\"700px\"></iframe>";
    echo "</div>";
		echo "</div>";
		echo "</div>";
		echo "</div>"; // End document preview
		
		echo "</div>"; // Ends content wraper

		$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();

	} /* }}} */


	protected function printButtonActionForAllowedUser($transition, $documentid, $version){
		$action = $transition->getAction();
		print "<form action=\"../out/out.TriggerWorkflow.php\" method=\"post\">
			<input type=\"hidden\" name=\"!@ramdomtextforallowedtransitions@!\" value='1' />
			<input type=\"hidden\" name=\"documentid\" value=\"".$documentid."\" />
			<input type=\"hidden\" name=\"version\" value=\"".$version."\" />
			<input type=\"hidden\" name=\"transition\" value=\"".$transition->getID()."\" />
			<input type=\"submit\" class=\"btn btn-primary\" value=\"".getMLText('action_'.strtolower($action->getName()), array(), $action->getName())."\" />
			</form>";
	}

	protected function printDefaultButtonAction($transition, $documentid, $version){
		$action = $transition->getAction();
		print "<form action=\"../out/out.TriggerWorkflow.php\" method=\"post\">
			<input type=\"hidden\" name=\"documentid\" value=\"".$documentid."\" />
			<input type=\"hidden\" name=\"version\" value=\"".$version."\" />
			<input type=\"hidden\" name=\"transition\" value=\"".$transition->getID()."\" />
			<input type=\"submit\" class=\"btn btn-primary\" value=\"".getMLText('action_'.strtolower($action->getName()), array(), $action->getName())."\" /></form>";
	}
}
?>
