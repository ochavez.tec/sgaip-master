<?php
/**
 * Implementation of EditComment view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditComment view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_EditResolutionType extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$strictformcheck = $this->params['strictformcheck'];
		header('Content-Type: application/javascript; charset=UTF-8');
?>
function checkForm()
{
	msg = new Array();
<?php
		if ($strictformcheck) {
?>
	if (document.form1.comment.value == "") msg.push("<?php printMLText("js_no_comment");?>");
<?php
		}
?>
	if (msg != "")
	{
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else return true;
}
$(document).ready(function() {
	$('body').on('submit', '#form1', function(ev){
		if(!checkForm()) {
			ev.preventDefault();
		} else {
			$("#box-form1").append("<div class=\"overlay\"><i class=\"fa fa-refresh fa-spin\"></i></div>");
		}
	});
});


<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$resolution_type = $this->params['resolution_type'];
		$strictformcheck = $this->params['strictformcheck'];

		$this->htmlStartPage(getMLText("edit_resolution_type"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		echo "<br/>";
		echo "<div class=\"row\">";
		echo "<div class=\"col-md-12\">";
		echo "<div class=\"box box-success div-green-border\" id=\"box-form1\">";
        echo "<div class=\"box-header with-border\">";
        echo "<h3 class=\"box-title\">".getMLText("edit_resolution_type")."</h3>";
        echo "</div>";
        echo "<div class=\"box-body\">";

?>
<form class="form-horizontal" action="../op/op.EditInformation.php" id="form1" name="form1" method="post">
	<?php echo createHiddenFieldWithKey('editresolutiontype'); ?>
	<input type="hidden" name="action" value="editresolutiontype">
	<input type="hidden" name="resolution_type_id" value="<?php echo $resolution_type['id']; ?>">			
	<div class="control-group">
		<label class="control-label"><?php echo getMLText("resolution_type")?>:</label>
		<div class="controls">
			<input name="resolution_type_name" class="form-control" type="text" required="required" value="<?php echo $resolution_type['name']; ?>">
		</div>
	</div>
	<br/>
	<div class="control-group">
		<label class="control-label"><?php echo getMLText("comment")?>:</label>
		<div class="controls">
			<input name="resolution_type_comment" class="form-control" type="text" required="required" value="<?php echo $resolution_type['comment']; ?>">
		</div>
	</div>
	        <br/>
	<div class="control-group">
		<label class="control-label"><?php echo getMLText("abbreviation")?>:</label>
		<div class="controls">
			<input name="resolution_type_abbreviation" class="form-control" type="text" required="required" value="<?php echo $resolution_type['abbreviation']; ?>">
		</div>
	</div>				
	<br/>
	<div class="control-group">
		<label class="control-label"><?php echo getMLText("make_memo")?>:</label>
		<div class="controls">
			<input name="make_memo" type="checkbox" value="1" <?php echo ((int)$resolution_type['make_memo']) ? "checked" : ""; ?> />
		</div>
	</div>
	<br/>
	<div class="box-footer">
		<a href="/out/out.Types.php" class="btn btn-default"><i class="fa fa-chevron-left"></i> <?php printMLText("cancel"); ?></a>
		<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php printMLText("save") ?></button>
	</div>
</form>
<?php
		
		echo "</div>";
		echo "</div>";
		echo "</div>";
		echo "</div>"; 
		echo "</div>"; // Ends row
		$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>
