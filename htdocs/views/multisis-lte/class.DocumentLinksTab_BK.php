<!-- // LINKED FILES TAB -->
<!--<li class="<?php //if($currenttab == 'links') echo 'active'; ?>"><a href="#links" data-toggle="tab" aria-expanded="false"><?php //printMLText('linked_documents'); echo (count($links)) ? " (".count($links).")" : ""; ?></a></li>-->
<!-- // LINKED FILES TAB -->

<div class="tab-pane <?php if($currenttab == 'links') echo 'active'; ?>" id="links">
		<?php
		
		if (count($links) > 0) {
			print "<div class=\"table-responsive\">";
			print "<table class=\"table table-striped\">";
			print "<thead>\n<tr>\n";
			print "<th width='10%' class='align-center td-success-background'></th>\n";
			print "<th width='10%' class='align-center td-success-background'>".getMLText("document")."</th>\n";
			print "<th width='40%' class='align-center td-success-background'>".getMLText("comment")."</th>\n";
			print "<th width='40%' class='align-center td-success-background'>".getMLText("details")."</th>\n";
			print "<th width='10%' class='align-center td-success-background'>".getMLText("actions")."</th>\n";
			print "</tr>\n</thead>\n<tbody>\n";

			foreach($links as $link) {
				$responsibleUser = $link->getUser();
				$targetDoc = $link->getTarget();
				$targetlc = $targetDoc->getLatestContent();

				$previewer->createPreview($targetlc, $previewwidthlist);
				print "<tr>";
				print "<td><a href=\"#\">";
				if($previewer->hasPreview($targetlc)) {
					print "<img class=\"mimeicon\" width=\"".$previewwidthlist."\"src=\"../op/op.Preview.php?documentid=".$targetDoc->getID()."&version=".$targetlc->getVersion()."&width=".$previewwidthlist."\" title=\"".htmlspecialchars($targetlc->getMimeType())."\">";
				} else {
					print "<img class=\"mimeicon\" src=\"".$this->getMimeIcon($targetlc->getFileType())."\" title=\"".htmlspecialchars($targetlc->getMimeType())."\">";
				}
				print "</td>";
				print "<td><a href=\"out.ViewDocument.php?documentid=".$targetDoc->getID()."\" class=\"linklist\">".htmlspecialchars($targetDoc->getName())."</a></td>";
				print "<td>".htmlspecialchars($targetDoc->getComment())."</td>";
				print "<td>".getMLText("document_link_by")." ".htmlspecialchars($responsibleUser->getFullName());
				if (($user->getID() == $responsibleUser->getID()) || ($document->getAccessMode($user) == M_ALL ))
					print "<br />".getMLText("document_link_public").": ".(($link->isPublic()) ? getMLText("yes") : getMLText("no"));
				print "</td>";
				print "<td class='align-center'><span class=\"actions\">";

				if($memo_cat) {
				 	if($user->isAdmin()){
				 		print "<form action=\"../op/op.RemoveDocumentLink.php\" method=\"post\">".createHiddenFieldWithKey('removedocumentlink')."<input type=\"hidden\" name=\"documentid\" value=\"".$documentid."\" /><input type=\"hidden\" name=\"linkid\" value=\"".$link->getID()."\" /><button type=\"submit\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-remove\"></i></button></form>";
				 	}
				} else {
					if ($needwkflaction || $user->isAdmin()){
					print "<form action=\"../op/op.RemoveDocumentLink.php\" method=\"post\">".createHiddenFieldWithKey('removedocumentlink')."<input type=\"hidden\" name=\"documentid\" value=\"".$documentid."\" /><input type=\"hidden\" name=\"linkid\" value=\"".$link->getID()."\" /><button type=\"submit\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-remove\"></i></button></form>";
					}
				}
				

				print "</span></td>";
				print "</tr>";
			}
			print "</tbody>\n</table>\n";
			print "</div>";
		}
		else echo "<p class='label label-warning' style='font-size:12px;padding:8px;'><i class='fa fa-warning'></i>&nbsp;&nbsp;".getMLText("no_linked_files")."</p><br>";

		//if (!$user->isGuest() && $document->getAccessMode($user) >= M_READWRITE ){
		if ($needwkflaction || $user->isAdmin()){
			if ($status['status'] != 2 && $status['status'] != -1) { ?>
			<br>
			<form action="../op/op.AddDocumentLink.php" name="form1">
			<input type="hidden" name="documentid" value="<?php print $documentid;?>">
			<table class="table-condensed">
			<tr>
			<td><?php printMLText("add_document_link");?>:</td>
			<td><?php $this->printDocumentChooserHtml("form1");?></td>
			</tr>
			<?php
			if ($document->getAccessMode($user) >= M_READWRITE) { 
				print "<tr><td>".getMLText("document_link_public")."</td>";
				print "<td>";
				print "<input type=\"checkbox\" name=\"public\" value=\"true\" />";
				print "</td></tr>";
			} ?>
			<tr>
			<td></td>
			<td><button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save")?></button></td>
			</tr>
			</table>
			</form>
		<?php }
		} else if($memo_cat) {
			if ((int)$actual_workflowstate->getID() === 44 && $user_is_member) { // 44 = remitido - memos 
				if ($status['status'] != 2 && $status['status'] != -1) {?> 
			<br>
			<form action="../op/op.AddDocumentLink.php" name="form1">
			<input type="hidden" name="documentid" value="<?php print $documentid;?>">
			<table class="table-condensed">
			<tr>
			<td><?php printMLText("add_document_link");?>:</td>
			<td><?php $this->printDocumentChooserHtml("form1");?></td>
			</tr>
			<?php
			if ($document->getAccessMode($user) >= M_READWRITE) { 
				print "<tr><td>".getMLText("document_link_public")."</td>";
				print "<td>";
				print "<input type=\"checkbox\" name=\"public\" value=\"true\" />";
				print "</td></tr>";
			} ?>
			<tr>
			<td></td>
			<td><button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save")?></button></td>
			</tr>
			</table>
			</form>
			<?php 
			}

			}
		}

		if (count($reverselinks) > 0) {
			print "<div class=\"gap-20\"></div>";
			print "<div class=\"box box-primary box-solid\">";
      print "<div class=\"box-header with-border\">";
      print "<h5 class=\"\"><strong>".getMLText("reverse_links")."</strong></h5>";
      print "</div>";
      print "<div class=\"box-body\">";

			print "<div class=\"table-responsive\">";
			print "<table class=\"table table-striped\">";
			print "<thead>\n<tr>\n";
			print "<th width='10%' class='align-center th-info-background'></th>\n";
			print "<th width='10%' class='align-center th-info-background'>".getMLText("document")."</th>\n";
			print "<th width='40%' class='align-center th-info-background'>".getMLText("comment")."</th>\n";
			print "<th width='40%' class='align-center th-info-background'>".getMLText("details")."</th>\n";
			//print "<th width='10%' class='align-center td-info-background'>".getMLText("actions")."</th>\n";
			print "</tr>\n</thead>\n<tbody>\n";

			foreach($reverselinks as $link) {
				$responsibleUser = $link->getUser();
				$sourceDoc = $link->getDocument();
				$sourcelc = $sourceDoc->getLatestContent();

				$previewer->createPreview($sourcelc, $previewwidthlist);
				print "<tr>";
				print "<td><a href=\"#\">";
				if($previewer->hasPreview($sourcelc)) {
					print "<img class=\"mimeicon\" width=\"".$previewwidthlist."\"src=\"../op/op.Preview.php?documentid=".$sourceDoc->getID()."&version=".$sourcelc->getVersion()."&width=".$previewwidthlist."\" title=\"".htmlspecialchars($sourcelc->getMimeType())."\">";
				} else {
					print "<img class=\"mimeicon\" src=\"".$this->getMimeIcon($sourcelc->getFileType())."\" title=\"".htmlspecialchars($sourcelc->getMimeType())."\">";
				}
				print "</td>";
				print "<td><a href=\"out.ViewDocument.php?documentid=".$sourceDoc->getID()."\" class=\"linklist\">".htmlspecialchars($sourceDoc->getName())."</a></td>";
				print "<td>".htmlspecialchars($sourceDoc->getComment())."</td>";
				print "<td>".getMLText("document_link_by")." ".htmlspecialchars($responsibleUser->getFullName());
				if (($user->getID() == $responsibleUser->getID()) || ($document->getAccessMode($user) == M_ALL ))
					print "<br />".getMLText("document_link_public").": ".(($link->isPublic()) ? getMLText("yes") : getMLText("no"));
				print "</td>";
				/*print "<td><span class=\"actions\">";
				if (($user->getID() == $responsibleUser->getID()) || ($document->getAccessMode($user) == M_ALL ))
					print "<form action=\"../op/op.RemoveDocumentLink.php\" method=\"post\">".createHiddenFieldWithKey('removedocumentlink')."<input type=\"hidden\" name=\"documentid\" value=\"".$documentid."\" /><input type=\"hidden\" name=\"linkid\" value=\"".$link->getID()."\" /><button type=\"submit\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-times\"></i></button></form>";
				print "</span></td>";*/
				print "</tr>";
			}
			print "</tbody>\n</table>\n";
			print "</div>";
			print "</div>";
			print "</div>";
		}
		?>
		</div>