<?php
/**
 * Implementation of TriggerWorkflow view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for TriggerWorkflow view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_TriggerWorkflow extends SeedDMS_Bootstrap_Style {


	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$folder = $this->params['folder'];
		$document = $this->params['document'];
		$transition = $this->params['transition'];
		$is_allowed = $this->params['is_allowed'];
		$today = date("d-m-Y");

		$this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))), "skin-blue sidebar-mini");
		$this->containerStart();

		echo "<style>.table-hover tr:hover {background:#cfedff !important;}</style>";
		
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();		

		$user_root_folder_id = $this->params['user']->getHomeFolder();
		$user_root_folder = $this->params['dms']->getFolder($user_root_folder_id);

		$legal_partners = $dms->getGroup(164); // 164 = Colaboradores Jurídicos
		$secretaries = $dms->getGroup(165); // 165 = Secretarias

		if ($legal_partners->isMember($user) || $secretaries->isMember($user) || $user->isAdmin() || $document->getAccessMode($user) >= M_READWRITE) {
			echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document, $user, true);
		} else {
			echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document, $user, false);
		}

		//echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document);

		//// Document content ////
		echo "<div class=\"row\">";
		echo "<div class=\"col-md-12\">";
		$action = $transition->getAction();

		$this->startBoxPrimary(getMLText("trigger_workflow_action", array("action" => $action->getName())));

?>

<?php
		$latestContent = $document->getLatestContent();
		
		$currentstate = $latestContent->getWorkflowState();
		$wkflog = $latestContent->getWorkflowLog();
		$workflow = $latestContent->getWorkflow();
		$enterdate = '0';

		$msg = getMLText("status_current_info").$currentstate->getName()."<br />";
		if($wkflog) {
			foreach($wkflog as $entry) {
				if($entry->getTransition()->getNextState()->getID() == $currentstate->getID()) {
					$enterdate = $entry->getDate();
					$enterts = makeTsFromLongDate($enterdate);
				}
			}
			$msg .= getMLText("status_date_record_one").$enterdate.getMLText("status_date_record_two");
			$msg .= getReadableDuration((time()-$enterts)).".<br />";
		}
		//$msg .= "The document may stay in this state for ".$currentstate->getMaxTime()." sec.";
		$this->infoMsg($msg);

		$transition_next_state = $transition->getNextState()->getID();
		$next_state = $dms->getWorkflowState($transition_next_state);

		// Display the Workflow form.
?>
	
	<div class="col-md-6">
	<div class="box box-warning div-bkg-color" id="box-form">
	<div class="box-body">
		<form class="form-horizontal" method="post" action="../op/op.TriggerWorkflow.php" id="form1" name="form1">
		<?php echo createHiddenFieldWithKey('triggerworkflow'); ?>
		<div id="box-form">
			<div class="control-group">
				<label class="control-label"><?php printMLText('comment'); ?>:</label>
				<div class="controls">
					<textarea class="form-control" name="comment" cols="80" rows="4" id="trigger_workflow_comment" required="true"></textarea>
				</div>
			</div>
			<input type='hidden' name='documentid' value='<?php echo $document->getID(); ?>'/>
			<input type='hidden' name='version' value='<?php echo $latestContent->getVersion(); ?>'/>
			<input type='hidden' name='transition' value='<?php echo $transition->getID(); ?>'/>
			<input type='hidden' name='is_allowed' value='<?php echo $is_allowed; ?>'/>

		<?php 

		if ((int)$workflow->getID() == 59 && (int)$action->getID() == 24) { // 59 = memos, 24 = Remitir

			// ------------------------ FOR MEMOS ------------------------ //
			
			$last_transition = $dms->getWorkflowNextTransition($workflow->getID(), $next_state->getID());

			echo '<input type="hidden" name="memo_remitir" value="1"/>';
			echo '<input type="hidden" name="limit_days" value="'.$last_transition->getMaxTime().'"/>';

			echo '<div class="control-group">';
			echo '<label class="control-label">'.getMLText('group_asigned').':</label>';
			echo '<div class="controls">';
			
			// 83 is the specific id for the document attribute (Unidad a remitir)
			$assigned_group = $dms->getDocumentAttributeValue(83, $document->getID()); 
			echo '<p>'.$assigned_group.'</p>';
			echo '</div>';
			echo '</div>';

			echo '<div class="control-group">';
			echo '<div class="controls">';
			echo '<label class="control-label">'.getMLText("group_asigned_edit").':</label>';
			echo '<select class="chzn-select" name="group_asigned" required="true">';			
			$allGroups = $dms->getAllGroups();
			foreach ($allGroups as $groupObj) {
				$founded = false;
			   	if ($groupObj->getName() == $assigned_group) {
					$founded = true;
				}
				
				if($founded){
					echo "<option value='".$groupObj->getID()."' selected>" . htmlspecialchars($groupObj->getName()) . "</option>";
				} else {
					echo "<option value='".$groupObj->getID()."'>" . htmlspecialchars($groupObj->getName()) . "</option>";	
				}
				
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';

			echo '<br/>';

			echo '<div class="control-group">';
			echo '<p>'.getMLText('transition_days_msg', array('days' => $last_transition->getMaxTime())).'</p>';
			echo '<label>'.getMLText("limit_date").':</label>';
			echo '<div class="input-group date">';
            echo '<div class="input-group-addon">';
           	echo '<i class="fa fa-calendar"></i>';
            echo '</div>';
            echo '<input type="text" name="end_date" class="form-control chosen-date" id="date" value="" />';
            echo '</div>';
            echo '</div>';
			echo '<br/>';
		
		} else if ((int)$workflow->getID() == 59 && (int)$action->getID() == 39) { // 59 = memos and 39 = Responder memo

			echo '<input type="hidden" name="memo_finalizar" value="1"/>';

		} else if((int)$workflow->getID() == 9) { // 9 = consultas
			
			// ------------------------ FOR CC ------------------------ //

			if((int)$action->getID() == 12){ // Asignar
				echo '<input type="hidden" name="cc_asignar" value="1"/>';

				echo '<div class="control-group">';
				echo '<div class="controls">';
				echo '<label class="control-label">'.getMLText("user_to_asign").':</label>';
				echo '<select class="chzn-select" name="user_asigned"  required="true">';			
				//$all_users = $dms->getAllUsers();
				$juridicos_group = $dms->getGroupByName("Colaboradores Jurídicos");
				$all_users = $juridicos_group->getUsers();
				foreach ($all_users as $user) {
					$cc_assigned = $dms->getActiveCCAssignments($user->getID());

					if(is_array($cc_assigned)){
						$total_cc = count($cc_assigned);	
					} else {
						$total_cc = 0;
					}					
					echo "<option value='".$user->getID()."'>" . htmlspecialchars($user->getFullName()) ." - (".$total_cc.")</option>";
				}
				echo '</select>';
				echo '</div>';
				echo '</div>';
				echo '<br/>';

				echo '<div class="control-group">';
				echo '<label>'.getMLText("limit_date").':</label>';
				echo '<div class="input-group date">';
                echo '<div class="input-group-addon">';
                echo '<i class="fa fa-calendar"></i>';
                echo '</div>';
                echo '<input type="text" name="end_date" class="form-control chosen-date" id="date" required="required" value="'.$today.'" readonly />';
                echo '</div>';
                echo '</div>';
				echo '<br/>';
			}
			
			if((int)$action->getID() == 19){ // 19 = Responder
				echo '<input type="hidden" name="cc_finalizar" value="1"/>';
			}

		} 

		else if((int)$workflow->getID() == 12) { // 12 = Solicitudes
			
			// ------------------------ FOR SIP ------------------------ //

			if((int)$action->getID() == 12){ // 12 = Asignar
				echo '<input type="hidden" name="sip_asignar" value="1"/>';

				echo '<div class="control-group">';
				echo '<div class="controls">';
				echo '<label class="control-label">'.getMLText("user_to_asign").':</label>';
				echo '<select class="chzn-select" name="user_asigned"  required="true">';			
				//$all_users = $dms->getAllUsers();
				$juridicos_group = $dms->getGroupByName("Colaboradores Jurídicos");
				$all_users = $juridicos_group->getUsers();
				foreach ($all_users as $user) {
					$sip_assigned = $dms->getActiveSIPAssignments($user->getID());

					if(is_array($sip_assigned)){
						$total_sip = count($sip_assigned);	
					} else {
						$total_sip = 0;
					}					
					echo "<option value='".$user->getID()."'>" . htmlspecialchars($user->getFullName()) ." - (".$total_sip.")</option>";
				}
				echo '</select>';
				echo '</div>';
				echo '</div>';
				echo '<br/>';

				echo '<div class="control-group">';
				echo '<div class="controls">';
				echo '<label class="control-label">'.getMLText("date_for_sip").':</label>';
				echo '<select class="chzn-select" name="date_rule"  required="true">';			
				$all_rules = $dms->getAllDateRules();
				if ($all_rules) {
					$j = 0;
					foreach ($all_rules as $rule) {
						$j++;
						if ($j == 1) {
							echo "<option value='".$rule['id']."'>".getMLText('date_rule_notice_one', array ("years" => htmlspecialchars($rule['years']), "days" => htmlspecialchars($rule['days'])))."</option>";
						} else {						
							echo "<option value='".$rule['id']."'>".getMLText('date_rule_notice_two', array ("years" => htmlspecialchars($rule['years']), "days" => htmlspecialchars($rule['days'])))."</option>";
						}

					}	
				} else {
					echo "<option value='-1'>".getMLText('any_rule_added')."</option>";
				}
				
				echo '</select>';
				echo '</div>';
				echo '</div>';
				echo '<br/>';

			}				
				

			// CREA RESOLUCIONES
			if ($action->getMakeResolution()) {
				if (!is_bool($dms->getActionResolution($action->getID()))) {
					echo '<input type="hidden" name="make_resolution" value="1"/>';
					echo '<input type="hidden" name="action_id" value="'.$action->getID().'"/>';	
				}				
			}

			// CONFIRMAR PREVENCION PAUSA LA SIP
			if((int)$action->getID() == 17){ // 35 = confirmar prevencion
				echo '<input type="hidden" name="sip_pause" value="1"/>';
			}


			// EJECUTAR ADMITIR O INADMITIR DESDE EL ESTADO 15 = SOLICITUD PREVENIDA
			if((int)$currentstate->getID() == 15){ 

				// 15 = SOLICITUD PREVENIDA => Si se admite o inadmite, siempre se reanuda y cambia fecha de vencimiento aumentando los días que se tardó en subsanar la prevencion
				
				echo '<input type="hidden" name="sip_reanudar_despues_de_prevenir" value="1"/>';
				echo '<input type="hidden" name="fecha_de_prevencion" value="'.$enterdate.'" />';				
				echo '<br/>';
			}

			// CONFIRMAR ADMISION CAMBIA EL NOMBRE DE LA SIP
			if((int)$action->getID() == 35){ // 35 = confirmar admision
				echo '<input type="hidden" name="sip_admitir" value="1"/>';
			}			

			// RESPONDER CAMBIA EL NOMBRE DE LA SIP
			if((int)$action->getID() == 19){ // 19 = responder
				echo '<input type="hidden" name="sip_admitir" value="1"/>';
			}
			

			// CONFIRMAR PRORROGA AUMENTA LOS DIAS DE LA FECHA DE FINALIZACION
			if((int)$action->getID() == 22){ // 22 = Confirmar prorroga

				echo '<input type="hidden" name="sip_aplicar_prorroga" value="1"/>';

				$extension_rule = $dms->getExtensionTimeRule();
				echo '<br/>';
				echo '<div class="control-group">';
				echo '<div class="controls">';
				echo '<p>'.getMLText("extension_time_to_apply").':&nbsp;<b>'.$extension_rule['extension'].'</b>&nbsp;'.getMLText("days").'</p>';
				echo '<p>'.getMLText("extension_time_before").':&nbsp;<b>'.$extension_rule['days_before'].'</b>&nbsp;'.getMLText("days").'</p><br/>';
				echo '</div>';
				echo '</div>';
			}

			// FINALIZA EL FLUJO
			if((int)$action->getID() == 36){ // 36 = Confirmar Improcedencia
				echo '<input type="hidden" name="sip_finalizar" value="1"/>';
			}

			if((int)$action->getID() == 37){ // 37 = Confirmar Incompetencia
				echo '<input type="hidden" name="sip_finalizar" value="1"/>';
			}

			if((int)$action->getID() == 20){ // 20 = Confirmar Respuesta
				echo '<input type="hidden" name="sip_finalizar" value="1"/>';
			}

			if((int)$action->getID() == 18){ // 18 = Confirmar Inadmisión
				echo '<input type="hidden" name="sip_finalizar" value="1"/>';
			}

			if((int)$action->getID() == 38){ // 38 = Confirmar Desistimiento
				echo '<input type="hidden" name="sip_finalizar" value="1"/>';
			}	
			// FINALIZA EL FLUJO


			if($next_state->getDocumentStatus() == '2' || $next_state->getDocumentStatus() == '-1'){
				$information_types = $dms->getInformationTypesRequested();

				echo '<div class="row">';
				if ($information_types) {
					echo '<input type="hidden" name="save_information_types" value="1"/>';				
					echo '<div class="control-group">';
					echo '<div class="controls">';
					echo '<label class="control-label">'.getMLText("requirement_count").':</label>';
					echo '<br/>';
					foreach ($information_types as $info) {						
						echo '<div class="col-md-6">';
						echo '<label class="control-label">'.$info['name'].'</label>';
						echo '<input type="number" name="info_'.$info['id'].'" class="form-control" min="0" value="0">';
						echo '</div>';
					}
					echo '</div>';
					echo '</div>';
				} else {
					echo '<br/>';
					echo '<div class="col-md-6">';
					echo '<p>'.getMLText("any_information_type").'</p>';
					echo '</div>';
				}
				echo '</div>';
				echo '<br/>';
			}
			
		} 

		else if((int)$workflow->getID() == 60) { // 60 = Resoluciones

			// ------------------------ FOR RESOLUTIONS ------------------------ //

			if((int)$action->getID() == 4){ // 4 = Publicar
				echo '<input type="hidden" name="resolucion_publicar" value="1"/>';
			}
		}

		// ------------------------------------------------------ //

		echo '<div class="box-footer">';
		echo '<a href="/out/out.ViewDocument.php?documentid='.$document->getID().'&currenttab=docinfo" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;'.getMLText("cancel").'</a>';
		echo '<button type="submit" class="btn btn-primary pull-right" id="apply-action"><i class="fa fa-save"></i> '.getMLText("action_".strtolower($action->getName()), array(), $action->getName()).'</button>';
		echo '</div></div>';
		echo '</form>';
		echo '</div></div>';
		echo "</div>";

		// WORKFLOW LOG TABLE //
		if($wkflog) {
			echo "<div class=\"col-md-6\">";
			echo "<div class=\"table-responsive\">";
			echo "<table class=\"table table-condensed table-striped table-hover\">";
			echo "<tr>
				<th class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('action')."</th>
				<th class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('status_from')."</th>
				<th class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('status_to')."</th>
				<th class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('date')."</th>
				<th class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('user')."</th>
				<th class=\"align-center th-info-background\" style='border: 1px solid #888888;'>".getMLText('comment')."</th>
				</tr>";
			foreach($wkflog as $entry) {
				echo "<tr>";
				echo "<td style='border: 1px solid #888888;'>".getMLText('action')." ".strtolower($entry->getTransition()->getAction()->getName())."</td>";
				echo "<td style='border: 1px solid #888888;'>".$entry->getTransition()->getState()->getName()."</td>";
				echo "<td style='border: 1px solid #888888;'>".$entry->getTransition()->getNextState()->getName()."</td>";
				echo "<td style='border: 1px solid #888888;'>".$entry->getDate()."</td>";
				echo "<td style='border: 1px solid #888888;'>".$entry->getUser()->getFullname()."</td>";
				echo "<td style='border: 1px solid #888888;'>".$entry->getComment()."</td>";
				echo "</tr>";
			}
			echo "</table>\n";
			echo "</div>";
			echo "</div>";
		}

		$this->endsBoxPrimary();
		echo "</div>";
		echo "</div>";
		echo "</div>"; // Ends row
		?>

		<script type="text/javascript">
			$(document).on("ready", function(){

				$('#form1').on('submit', function(){
					$("#box-form").append("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");
				});

				$('.chosen-date').datepicker({
					language: 'es',
					format: 'dd-mm-yyyy',
					autoclose: true,
				});
			});
		</script>

		<?php
		$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();

	} /* }}} */
}
?>
