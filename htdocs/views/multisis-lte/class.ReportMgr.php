<?php
/**
 * Implementation of LogManagement view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for LogManagement view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_ReportMgr extends SeedDMS_Bootstrap_Style {

	function reportlist($entries) { /* {{{ */
		$print_header = true;
		foreach ($entries as $entry){
			
			if ($print_header){
				print "<div class='table-responsive'>";
				print "<form action=\"out.ReportMgr.php\" method=\"get\">\n";
				print "<table class=\"table table-bordered table-condensed\">\n";
				print "<thead>\n<tr>\n";
				print "<th>".getMLText("name")."</th>\n";
				print "<th>".getMLText("comment")."</th>\n";
				print "<th></th>\n";
				print "</tr>\n</thead>\n<tbody>\n";
				$print_header=false;
			}
					
			print "<tr>\n";
			print "<td>".$entry->getName()."</td>\n";
			print "<td>".$entry->getComment()."</td>\n";
			print "<td>";
			print "<a href=\"out.EditReport.php?id=".$entry->getID()."\" class=\"btn btn-mini btn-primary\"><i class=\"fa fa-edit\"></i></a>";
			print "&nbsp;<a href=\"out.RunReport.php?id=".$entry->getID()."\" class=\"btn btn-mini btn-success\"><i class=\"fa fa-play\"></i></a>";
			print "&nbsp;<a href=\"../op/op.Download.php?reportid=".$entry->getID()."\" class=\"btn btn-mini btn-warning\"><i class=\"fa fa-download\"></i></a>";
			print "<a href=\"out.RemoveReport.php?id=".$entry->getID()."\" class=\"btn btn-mini btn-danger pull-right\"><i class=\"fa fa-times\"></i></a>";
			print "&nbsp;";
			//print "<a href=\"../op/op.SendNotifySchedule.php?id=".$entry->getID()."\" class=\"btn btn-mini btn-paper-plane\"><i class=\"fa fa-paper-plane\"></i> ".getMLText("send_now")."</a>";
			print "&nbsp;";
			print "</tr>\n";
		}

		if ($print_header) printMLText("empty_report_list");
		else print "</table></form></div>\n";

	} /* }}} */

	function js() { /* {{{ */
		header('Content-Type: application/javascript');
?>

function checkForm()
{
	msg = new Array();
//	eval("var formObj = document.form1;");
	var formObj = document.getElementById("form1");

	if (formObj.name.value == "") msg.push("<?php printMLText("js_no_name");?>");
	if (formObj.query.value == "") msg.push("<?php printMLText("js_no_query");?>");
	if (msg != "")
	{
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$(document).ready( function() {
	$('body').on('submit', '#form1', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});	
	$("#add-report").on("click", function(){
		$("#div-add-report").show('slow');
	});
	$(".cancel-add-report").on("click", function(){
		$("#div-add-report").hide('slow');
	});

});		
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$reports = $this->params['reports'];
		
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');


//		if(!$logname) {
		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
	<ul class="breadcrumb default-bread">
		<li class="pull-right breadcrumb-btn">
			<a id="add-report" type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="bottom" title="Nuevo reporte">
				<i class="fa fa-plus"></i> <i class="fa fa-table"></i>
			</a>
		</li>
	</ul>
		
    <div class="gap-10"></div>
<?php
		//// Add Report ////
		echo "<div class=\"col-md-12 div-hidden\" id=\"div-add-report\">";
		echo "<div class=\"box box-success div-green-border\" id=\"box-form1\">";
    echo "<div class=\"box-header with-border\">";
    echo "<h3 class=\"box-title\">".getMLText("add_report")."</h3>";
    echo "<div class=\"box-tools pull-right\">";
    echo "<button type=\"button\" class=\"cancel-add-report btn btn-box-tool\" data-widget=\"\"><i class=\"fa fa-times\"></i></button>";
    echo "</div>";
    echo "<!-- /.box-tools -->";
    echo "</div>";
    echo "<!-- /.box-header -->";
    echo "<div class=\"box-body\">";
	
?>
   <form class="form-horizontal" action="../op/op.ReportMgr.php" id="form1" name="form1" method="post">
			<?php echo createHiddenFieldWithKey('addreport'); ?>
			<input type="hidden" name="reportid" value="<?php //print $message->getId();?>">
			<input type="hidden" name="action" value="addreport">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php printMLText("name");?>:<span class="is-required">*</span></label>
					<div class="col-sm-10"><input class="form-control" type="text" name="name" size="60" required></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php printMLText("comment");?>:</label>
					<div class="col-sm-10"><textarea class="form-control" name="comment" rows="4" cols="80"></textarea></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php printMLText("query");?>:<span class="is-required">*</span></label>
					<div class="col-sm-10"><textarea class="form-control" name="query" rows="10" cols="80" required></textarea></div>
				</div>
			
				<div class="box-footer">
					<a id="cancel-add-report" type="button" class="cancel-add-report btn btn-default"><?php echo getMLText("cancel"); ?></a type="button">
					<button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> <?php printMLText("save")?></button>
				</div>
				
		</div>
		</form>	
		
    <?php
    echo "</div>";
    echo "<!-- /.box-body -->";
    echo "</div>";
		echo "</div>";
?>

    <div class="row">
    <div class="col-md-12">
    <?php 

		$this->startBoxPrimary(getMLText("report_management"));

		$entries = array();
		$wentries = array();
?>




	<div class="nav-tabs-custom">
<!--
  <ul class="nav nav-tabs" id="logtab">
	  <li <?php echo ($mode == 'toSend') ? 'class="active"' : ''; ?>><a href="" data-target="#toSend" data-toggle="tab">Por enviar</a></li>
	  <li <?php echo ($mode == 'sent') ? 'class="active"' : ''; ?>><a href="" data-target="#sent" data-toggle="tab">Enviados</a></li>
	</ul>
-->
	<div class="box box-primary">
	  <div class="box-header with-border">
<?php
		$this->contentContainerStart();
		$this->reportlist($reports);
		$this->contentContainerEnd();
?>
	  </div>
	</div>
</div>
 
  <div class="modal fade" id="logViewer" tabindex="-1" role="dialog" aria-labelledby="docChooserLabel" aria-hidden="true">
  <div class="modal-dialog modal-primary modal-lg" role="document">
  	<div class="modal-content">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    <h3 class="modal-title"><?php printMLText("log_management") ?></h3>
		  </div>
		  <div class="modal-body">
				<p><?php printMLText('tree_loading') ?></p>
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php printMLText("close") ?></button>
		  </div>
	  </div>
	 </div>
	</div>

<?php
		$this->endsBoxPrimary();

		echo "</div>";
		echo "</div>";
		echo "</div>";
		
    $this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();

/*
		} elseif(file_exists($this->contentdir.$logname)){
			echo $logname."<pre>\n";
			readfile($this->contentdir.$logname);
			echo "</pre>\n";
		} else {
			UI::exitError(getMLText("admin_tools"),getMLText("access_denied"));
		}
*/

	} /* }}} */
}
?>
