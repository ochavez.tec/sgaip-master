<?php
/**
 * Implementation of WorkspaceMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for WorkspaceMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_WorkflowMgr extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		header('Content-Type: application/javascript; charset=UTF-8');
?>

function checkForm(num)
{
	msg = new Array();
	eval("var formObj = document.form" + num + ";");

	if (formObj.name.value == "") msg.push("<?php printMLText("js_no_name");?>");
	if (msg != "")
	{
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

function to_save_graph(graph, workflow){
	$.ajax({
		type: 'post',
		dataType: 'json',
        url: '/views/multisis-lte/graphs/save.php',
        data: {
        	key : "dRte$ERT",
			graph : graph,
			workflow : workflow 
        },
        success: function (data) {
        	if (data.success == true) {
				alert("Gráfico guardado correctamante");	
			} else {
				console.log("Ha ocurrido un error");
			}
        }
	});
}

$(document).ready(function() {
	$('body').on('submit', '#form1', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});
	$( "#selector" ).change(function() {
		$('div.ajax').trigger('update', {workflowid: $(this).val()});
	});

	$("#document-workflow-widget").on("click", function(){
		$("#workflow-preview").addClass("div-hidden");
		$("#workflow-info").removeClass("col-md-6").addClass("col-md-12");
	});
});

$(document).on('click', '.edit-transition', function(event) {
    event.preventDefault();
    var transition_id = $(this).attr("rel");
    if($("#transition-form-"+transition_id).hasClass("div-hidden")){
    	$("#transition-form-"+transition_id).fadeIn("slow");
    	$("#transition-form-"+transition_id).removeClass("div-hidden");
	} else {
		
    	$("#transition-form-"+transition_id).fadeOut("slow");
    	$("#transition-form-"+transition_id).addClass("div-hidden");

	}
});

$(document).on('click', '.delete-transition', function(ev) {
    ev.preventDefault();
    var workflow_id = $(this).attr("data-workflow");
    var transition_id = $(this).attr("rel");
    confirmmsg = $(ev.currentTarget).attr('confirmmsg');
	msg = $(ev.currentTarget).attr('msg');

	var formtoken = "<?php echo createFormKey('removeworkflowtransition'); ?>";
	bootbox.confirm({
    	message: confirmmsg,
    	buttons: {
        	confirm: {
            	label: "<i class='fa fa-times'></i> "+msg,
            	className: 'btn-danger'
        	},
        	cancel: {
            	label: "<?php echo getMLText("cancel"); ?>",
            	className: 'btn-default'
        	}
    	},
	    callback: function (result) {
	    	if (result) {
	    		$.post('/op/op.WorkflowTransitions.php',
				{ action: 'removeworkflowtransition', transition: transition_id, workflow: workflow_id, formtoken: formtoken })
				.always(function(data) {
					if(data.success) {							
						noty({
							text: data.message,
							type: 'success',
							dismissQueue: true,
							layout: 'topRight',
							theme: 'defaultTheme',
							timeout: 1500,
						});
												
						$("#transition-"+transition_id).fadeOut("slow");
						$("#transition-form-"+transition_id).fadeOut("slow");

					} else {
						noty({
							text: data.message,
							type: 'error',
							dismissQueue: true,
							layout: 'topRight',
							theme: 'defaultTheme',
							timeout: 3500,
						});
					}
				});
			}
	    }
	});
});

$(document).on('click', '.save-transition', function(event) {
    event.preventDefault();

    var workflow_id = $(this).attr("data-workflow");
    var transition_id = $(this).attr("rel");

    var state 				= $("#form-state-"+transition_id).val();
	var nextstate 			= $("#form-nextstate-"+transition_id).val();
	var transition_action	= $("#form-action-"+transition_id).val();
	var users 				= $("#form-users-"+transition_id).val();
	var groups 				= $("#form-groups-"+transition_id).val();

	if($("#form-file-change-allowed-"+transition_id).is(":checked")){
		var file_change_allowed = 1;
	} else {
		var file_change_allowed = 0;
	}

	if($("#form-user-allowed-"+transition_id).is(":checked")){
		var user_allowed = 1;
	} else {
		var user_allowed = 0;
	}

	var maxtime 			= $("#form-maxtime-"+transition_id).val();

    var formtoken = '<?php echo createFormKey("editworkflowtransition"); ?>';
  	$.post('/op/op.WorkflowTransitions.php',
	{ 
		action: 			'editworkflowtransition',
		workflow_id: 		workflow_id, 
		transition_id: 		transition_id, 
		formtoken: 			formtoken,
		state: 				state,
		nextstate: 			nextstate,
		transition_action: 	transition_action,
		users: 				users,
		groups: 			groups,
		file_change_allowed: file_change_allowed,
		user_allowed: 		user_allowed,
		maxtime: 			maxtime
	})
	.always(function(data) {
							
		if(data.success) {

    		$("#transition-form-"+transition_id).fadeOut("slow");
    		$("#transition-form-"+transition_id).addClass("div-hidden");

			var new_state = data.data.state;
			var new_nextstate = data.data.nextstate;
			var new_transition_action = data.data.transition_action;
			var new_users = data.data.users;
			var new_groups = data.data.groups;
			var new_docstatus = data.data.doc_status;

			if(data.data.file_change_allowed == 1){
				var new_file_change_allowed = "Sí";
			} else {
				var new_file_change_allowed = "No";
			}

			if(data.data.user_allowed == 1){
				var new_user_allowed = "Sí";
			} else {
				var new_user_allowed = "No";
			}

			var new_maxtime = data.data.maxtime;

			/* Change text on specific tr */
			$("#span-state-"+transition_id).text(String(new_state));
			$("#span-nextstate-"+transition_id).text(String(new_nextstate));
			$("#span-action-"+transition_id).text(String(new_transition_action));
			$("#p-users-"+transition_id).html(new_users);
			$("#p-groups-"+transition_id).html(new_groups);

			if(new_docstatus != "2" && new_docstatus != "-1"){
				$("#span-docstatus-"+transition_id).text("");				
			}

			$("#span-file-allowed-"+transition_id).text(new_file_change_allowed);
			$("#span-user-allowed-"+transition_id).text(new_user_allowed);
			$("#span-maxtime-"+transition_id).text(String(new_maxtime));


			noty({
				text: data.message,
				type: 'success',
				dismissQueue: true,
				layout: 'topRight',
				theme: 'defaultTheme',
				timeout: 1500,
			});

		} else {
			noty({
				text: data.message,
				type: 'error',
				dismissQueue: true,
				layout: 'topRight',
				theme: 'defaultTheme',
				timeout: 3500,
			});
		}
	});
});

<?php
	} /* }}} */

	function info() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$selworkflow = $this->params['selworkflow'];
		if($selworkflow) { ?>
		<div class="box box-success">
	    <div class="box-header with-border">
	        <h3 class="box-title"><?php echo getMLText("workflow"); ?></h3>
	        <div class="box-tools pull-right">
	          <button type="button" class="btn btn-box-tool" id="document-workflow-widget" data-widget="remove"><i class="fa fa-times"></i></button>
	        </div>
	    </div>
	    <div class="box-body">
			<div id="workflowgraph">
			<iframe id="theworkflow" src="out.WorkflowGraph.php?workflow=<?php echo $selworkflow->getID(); ?>" width="100%" height="661" style="border: 1px solid #e3e3e3; border-radius: 4px; margin: -1px;"></iframe>
			</div>
		</div>
		</div>
		<?php }
	} /* }}} */

	function showWorkflowForm($workflow) { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$workflows = $this->params['allworkflows'];
		$workflowstates = $this->params['allworkflowstates'];

		
	
 if($workflow) { ?>
 <div class="col-md-6">
	<div class="well">
	<form class="form-horizontal" action="../op/op.WorkflowMgr.php" method="post" enctype="multipart/form-data">

	<?php echo createHiddenFieldWithKey('editworkflow'); ?>
	<input type="hidden" name="workflowid" value="<?php print $workflow->getID();?>">
	<input type="hidden" name="action" value="editworkflow">

<?php
		if($workflow && !$workflow->isUsed()) {
?>
		<div class="controls">
			  <a type="button" class="btn btn-danger" href="../out/out.RemoveWorkflow.php?workflowid=<?php print $workflow->getID();?>"><i class="fa fa-remove"></i> <?php printMLText("rm_workflow");?></a>
		</div>
<?php
		}
?>
		<div class="control-group">
			<label class="control-label"><?php printMLText("workflow_name");?>:</label>
			<div class="controls">
				<input type="text" name="name" class="form-control" value="<?php print ($workflow ? htmlspecialchars($workflow->getName()) : "");?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php printMLText("workflow_initstate");?>:</label>
			<div class="controls">
				<select name="initstate" class="form-control">
<?php
			foreach($workflowstates as $workflowstate) {
				echo "<option value=\"".$workflowstate->getID()."\"";
				if($workflow && $workflow->getInitState()->getID() == $workflowstate->getID())
					echo " selected=\"selected\"";
				echo ">".htmlspecialchars($workflowstate->getName())."</option>\n";
			}
?>
			</select>
			</div>
		</div>
		<br>
		<div class="controls">
			<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save")?></button>
		</div>

	</form>
	</div>
	<div style="margin-bottom: 10px; display: block;">
		<button type="button" class="btn btn-success btn-lg"><?php echo getMLText("workflow_id").": ".$workflow->getID(); ?></button>
	</div>
</div>

<?php } 
echo '<div class="col-md-6">';
if($workflow) {

	$path = $workflow->checkForCycles();
	if($path) {
		$names = array();
		foreach($path as $state) {
			$names[] = $state->getName();
		}
		$this->warningMsg(getMLText('workflow_has_cycle').": ".implode(' <i class="fa fa-arrow-right"></i> ', $names));
	}

	$transitions = $workflow->getTransitions();
	$initstate = $workflow->getInitState();
	$hasinitstate = true;
	$hasreleased = true;
	$hasrejected = true;
	$missesug = false;
	if($transitions) {
		$hasinitstate = false;
		$hasreleased = false;
		$hasrejected = false;
		foreach($transitions as $transition) {
			$transusers = $transition->getUsers();
			$transgroups = $transition->getGroups();
			if(!$transusers && !$transgroups) {
				$missesug = true;
			}
			if($transition->getNextState()->getDocumentStatus() == S_RELEASED)
				$hasreleased = true;
			if($transition->getNextState()->getDocumentStatus() == S_REJECTED)
				$hasrejected = true;
			if($transition->getState()->getID() == $initstate->getID())
				$hasinitstate = true;
		}
	}
	if($missesug)
		$this->errorMsg(getMLText('workflow_transition_without_user_group'));
	if(!$hasinitstate)
		$this->errorMsg(getMLText('workflow_no_initial_state'));
	if(!$hasreleased)
		$this->errorMsg(getMLText('workflow_no_doc_released_state'));
	if(!$hasrejected)
		$this->warningMsg(getMLText('workflow_no_doc_rejected_state'));
	if($workflow->isUsed()) {
		$this->infoMsg(getMLText('workflow_in_use'));
	}
}

echo "</div>";
echo '<div class="col-md-12">';		
		/* ------ TRANSITIONS IFNORMATION -------*/
		if($workflow) {
		$actions = $dms->getAllWorkflowActions();
		if($actions) {
		$transitions = $workflow->getTransitions();
		echo "<div class=\"table-responsive\">";
		echo "<table class=\"table table-bordered table-striped transitions\"><thead>";
		echo "<tr>
			<th class='align-center table-head-success' width='20%'>".getMLText('state_and_next_state')."</th>
			<th class='align-center table-head-success' width='20%'>".getMLText('action')."</th>
			<th class='align-center table-head-success' width='20%'>".getMLText('users_and_groups')."</th>
			<th class='align-center table-head-success' width='10%'>".getMLText('is_file_change_allowed')."</th>
			<th class='align-center table-head-success' width='10%'>".getMLText('is_user_allowed')."</th>
			<th class='align-center table-head-success' width='10%'>".getMLText('state_max_time')."</th>
			<th class='align-center table-head-success' width='10%'></th>
			</tr>
			</thead>
			<tbody>";
		if($transitions) {
			foreach($transitions as $transition) {
				$state = $transition->getState();
				$nextstate = $transition->getNextState();
				$action = $transition->getAction();
				$transusers = $transition->getUsers();
				$transgroups = $transition->getGroups();
				$is_file_change_allowed = (int)$transition->getFileChangeAllowed();
				$is_user_allowed = (int)$transition->getUserAllowed();

				echo "<tr";
				if(!$transusers && !$transgroups) {
					echo " class=\"error\"";
				}
				echo " id='transition-".$transition->getID()."'>";

				echo "<td>".'<i class="fa fa-circle'.($workflow->getInitState()->getId() == $state->getId() ? ' initstate' : ' in-workflow').'"></i> <span id="span-state-'.$transition->getID().'">'.$state->getName()."</span><br />";

				$docstatus = $nextstate->getDocumentStatus();
				echo '<i class="fa fa-circle'.($docstatus == S_RELEASED ? ' released' : ($docstatus == S_REJECTED ? ' rejected' : ' in-workflow')).'"></i> <span id="span-nextstate-'.$transition->getID().'">'.$nextstate->getName()."</span>";

				if($docstatus == S_RELEASED || $docstatus == S_REJECTED) {
					echo '<br /><span id="span-docstatus-'.$transition->getID().'"><i class="fa fa-arrow-right"></i> '.getOverallStatusText($docstatus).'</span>';
				}

				echo "</td>";

				echo '<td><i class="fa fa-square workflow-action"></i> <span id="span-action-'.$transition->getID().'">'.$action->getName().'</td>';

				echo "<td>";
				echo "<p id='p-users-".$transition->getID()."'>";
				foreach($transusers as $transuser) {
					$u = $transuser->getUser();
					echo getMLText('user').": ".$u->getFullName();
					echo "<br />";
				}

				echo "</p>";
				echo "<p id='p-groups-".$transition->getID()."'>";
				foreach($transgroups as $transgroup) {
					$g = $transgroup->getGroup();
					echo getMLText('at_least_n_users_of_group',
						array("number_of_users" => $transgroup->getNumOfUsers(),
							"group" => $g->getName()));
					echo "<br />";
				}
				echo "</p>";
				echo "</td>";

				echo "<td class='align-center'>";
				if ($is_file_change_allowed) {
					echo "<span id='span-file-allowed-".$transition->getID()."'>".getMLText("yes")."</span>";
				} else {
					echo "<span id='span-file-allowed-".$transition->getID()."'>".getMLText("no")."</span>";
				}
				echo "</td>";

				echo "<td class='align-center'>";
				if ($is_user_allowed) {
					echo "<span id='span-user-allowed-".$transition->getID()."'>".getMLText("yes")."</span>";
				} else {
					echo "<span id='span-user-allowed-".$transition->getID()."'>".getMLText("no")."</span>";
				}
				echo "</td>";

				echo "<td class='align-center'>";
				echo "<span id='span-maxtime-".$transition->getID()."'>".$transition->getMaxTime();
				echo "</td>";

				echo "<td>";

				?>
				<button type="button" class="btn btn-danger delete-transition" msg="<?php echo getMLText('rm_transition'); ?>" confirmmsg="<?php echo htmlspecialchars(getMLText("confirm_rm_transition")); ?>" rel="<?php echo $transition->getID(); ?>" data-workflow="<?php echo $workflow->getID(); ?>"><i class="fa fa-remove"></i></button>
				<button type="button" class="btn btn-success edit-transition" rel="<?php echo $transition->getID(); ?>"><i class="fa fa-pencil"></i></button>
				<?php

				echo "</td>";
				echo "</tr>";

				/* ------ TRANSITIONS EDITION -------*/
				echo "<tr class='div-hidden' id='transition-form-".$transition->getID()."'>";
				
					echo "<td>";
					echo "<select id='form-state-".$transition->getID()."' name=\"state\" class=\"form-control\">";
					$first_state = $transition->getState();
					$states = $dms->getAllWorkflowStates();
					foreach($states as $state) {
						if ($first_state->getName() == $state->getName()) {
							echo "<option value=\"".$state->getID()."\" selected>".$state->getName()."</option>";
						} else {
							echo "<option value=\"".$state->getID()."\">".$state->getName()."</option>";	
						}						
					}

					echo "</select><br />";

					echo "<select id='form-nextstate-".$transition->getID()."' name=\"nextstate\" class=\"form-control\">";
					$second_state = $transition->getNextState();
					$states = $dms->getAllWorkflowStates();
					foreach($states as $state) {
						if ($second_state->getName() == $state->getName()) {
							echo "<option value=\"".$state->getID()."\" selected>".$state->getName()."</option>";
						} else {
							echo "<option value=\"".$state->getID()."\">".$state->getName()."</option>";	
						}						
					}
					echo "</select>";
					echo "</td>";

					echo "<td>";
					echo "<select id='form-action-".$transition->getID()."' name=\"action\" class=\"form-control\">";

					$action_name = $transition->getAction()->getName();
					foreach($actions as $action) {
						if ($action_name == $action->getName()) {
							echo "<option value=\"".$action->getID()."\" selected>".$action->getName()."</option>";	
						} else {
							echo "<option value=\"".$action->getID()."\">".$action->getName()."</option>";	
						}
						
					}
					echo "</select>";
					echo "</td>";

					echo "<td>";
				    echo "<select id='form-users-".$transition->getID()."' class=\"chzn-select form-control\" name=\"users[]\" multiple=\"multiple\" data-placeholder=\"".getMLText('users')."\" data-no_results_text=\"".getMLText('unknown_user')."\">";
					$allusers = $dms->getAllUsers('', true);
					foreach($allusers as $usr) {
						print "<option value=\"".$usr->getID()."\">". htmlspecialchars($usr->getLogin()." - ".$usr->getFullName())."</option>";
					}
					echo "</select>";
					echo "<br />";
				    echo "<select id='form-groups-".$transition->getID()."' class=\"chzn-select form-control\" name=\"groups[]\" multiple=\"multiple\" data-placeholder=\"".getMLText('groups')."\" data-no_results_text=\"".getMLText('unknown_group')."\">";
					$allgroups = $dms->getAllGroups();
					foreach($allgroups as $grp) {
						print "<option value=\"".$grp->getID()."\">". htmlspecialchars($grp->getName())."</option>";
					}
					echo "</select>";
					echo "</td>";

					echo "<td class='align-center'>";
					echo "<div class='checkbox'>";
				    echo "<label>";
				    if ($is_file_change_allowed) {
						echo "<input id='form-file-change-allowed-".$transition->getID()."' name='file_change_allowed' value='1' type='checkbox' checked='true'>";
					} else {
						echo "<input id='form-file-change-allowed-".$transition->getID()."' name='file_change_allowed' value='1' type='checkbox'>";
					}
				    echo "</label>";
				    echo "</div>";
				    echo "</td>";

				    echo "<td class='align-center'>";
					echo "<div class='checkbox'>";
				    echo "<label>";
				    if ($is_user_allowed) {
						echo "<input id='form-user-allowed-".$transition->getID()."' name='user_allowed' value='1' type='checkbox' checked='true'>";
					} else {
						echo "<input id='form-user-allowed-".$transition->getID()."' name='user_allowed' value='1' type='checkbox'>";
					}
				    echo "</label>";
				    echo "</div>";
				    echo "</td>";

				    echo "<td>";
					echo "<div class='control-group'>";            
				    echo "<input id='form-maxtime-".$transition->getID()."' type='number' min='0' name='maxtime' class='form-control' placeholder='0' value='".$transition->getMaxTime()."' />";
				    echo "</div>";
				    echo "</td>";

					echo "<td class='align-center'>";					
					echo "<button type='button' class='btn btn-info save-transition' rel='".$transition->getID()."' data-workflow='".$workflow->getID()."'><i class='fa fa-save'></i></button>";					
					echo "</td>";
				
				echo "</tr>";
			}
		}
		echo "</tbody></table>";
		echo "</div>";
?>
<form class="" action="../op/op.AddTransitionToWorkflow.php" method="post">
<?php

/* ------ TRANSITIONS CREATION -------*/
echo "<div class=\"table-responsive\">";
echo "<table class=\"table table-bordered table-striped\">
	<thead>
	<th class='align-center table-head-primary' width='20%'>".getMLText('state_and_next_state')."</th>
	<th class='align-center table-head-primary' width='20%'>".getMLText('action')."</th>
	<th class='align-center table-head-primary' width='20%'>".getMLText('users_and_groups')."</th>
	<th class='align-center table-head-primary' width='10%'>".getMLText('is_file_change_allowed')."</th>
	<th class='align-center table-head-primary' width='10%'>".getMLText('is_user_allowed')."</th>
	<th class='align-center table-head-primary' width='10%'>".getMLText('state_max_time')."</th>
	<th class='align-center table-head-primary' width='10%'></th>
	</thead>
	<tbody>";
	echo "<tr>";
	echo "<td>";
	echo "<select name=\"state\" class=\"form-control\">";
	$states = $dms->getAllWorkflowStates();
	foreach($states as $state) {
		echo "<option value=\"".$state->getID()."\">".$state->getName()."</option>";
	}
	echo "</select><br />";
	echo "<select name=\"nextstate\" class=\"form-control\">";
	$states = $dms->getAllWorkflowStates();
	foreach($states as $state) {
		echo "<option value=\"".$state->getID()."\">".$state->getName()."</option>";
	}
	echo "</select>";
	echo "</td>";
	echo "<td>";
	echo "<select name=\"action\" class=\"form-control\">";
	foreach($actions as $action) {
		echo "<option value=\"".$action->getID()."\">".$action->getName()."</option>";
	}
	echo "</select>";
	echo "</td>";
	echo "<td>";
    echo "<select class=\"chzn-select form-control\" name=\"users[]\" multiple=\"multiple\" data-placeholder=\"".getMLText('users')."\" data-no_results_text=\"".getMLText('unknown_user')."\">";
	$allusers = $dms->getAllUsers('', true);
	foreach($allusers as $usr) {
		print "<option value=\"".$usr->getID()."\">". htmlspecialchars($usr->getLogin()." - ".$usr->getFullName())."</option>";
	}
	echo "</select>";
	echo "<br />";
    echo "<select class=\"chzn-select form-control\" name=\"groups[]\" multiple=\"multiple\" data-placeholder=\"".getMLText('groups')."\" data-no_results_text=\"".getMLText('unknown_group')."\">";
	$allgroups = $dms->getAllGroups();
	foreach($allgroups as $grp) {
		print "<option value=\"".$grp->getID()."\">". htmlspecialchars($grp->getName())."</option>";
	}
	echo "</select>";
	echo "</td>";

	echo "<td class='align-center'>";
	echo "<div class='checkbox'>";
    echo "<label>";
    echo "<input name='file_change_allowed' value='1' type='checkbox'>";
    echo "</label>";
    echo "</div>";
    echo "</td>";

    echo "<td class='align-center'>";
	echo "<div class='checkbox'>";
    echo "<label>";
    echo "<input name='user_allowed' value='1' type='checkbox'>";
    echo "</label>";
    echo "</div>";
    echo "</td>";

    echo "<td>";
	echo "<div class='control-group'>";            
    echo "<input type='number' min='0' name='maxtime' class='form-control' placeholder='' value='0' />";
    echo "</div>";
    echo "</td>";

	echo "<td class='align-center'>";
	echo createHiddenFieldWithKey('addtransitiontoworkflow'); ?>
	<input type="hidden" name="workflow" value="<?php print $workflow->getID();?>">
	<button type="submit" class="btn btn-info"><i class="fa fa-save"></i></button>
	<?php
	echo "</td>";
	echo "</tr>\n";
echo "</tbody>";
echo "</table>";
echo "</div>";

echo "</div>";
?>
</form>
<?php
		}
		}
	} /* }}} */

	function form() { /* {{{ */
		$selworkflow = $this->params['selworkflow'];

		$this->showWorkflowForm($selworkflow);
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$selworkflow = $this->params['selworkflow'];
		$workflows = $this->params['allworkflows'];
		$workflowstates = $this->params['allworkflowstates'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		echo "<style>table.transitions tr:hover {background:#cfedff !important;}</style>";
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
    <div class="gap-10"></div>
    <?php 
?>

<div class="row">
<div class="col-md-6">
<?php $this->startBoxCollapsablePrimary(getMLText("selection")); ?>
<form class="form-horizontal">
	<div class="control-group">
		<div class="controls">
<select id="selector" class="form-control">
<option value="-1"><?php echo getMLText("choose_workflow")?>
<option value="0"><?php echo getMLText("add_workflow")?>
<?php
		foreach ($workflows as $currWorkflow) {
			print "<option value=\"".$currWorkflow->getID()."\" ".($selworkflow && $currWorkflow->getID()==$selworkflow->getID() ? 'selected' : '').">" . htmlspecialchars($currWorkflow->getName());
		}
?>
</select>
		</div>
	</div>
</form>

<?php $this->endsBoxCollapsablePrimary(); ?>

</div>
<div class="col-md-6">
	<?php $this->startBoxCollapsablePrimary(getMLText("add_workflow")); ?>
	<form class="form-horizontal" action="../op/op.WorkflowMgr.php" method="post" enctype="multipart/form-data">
		<?php	echo createHiddenFieldWithKey('addworkflow'); ?>
		<input type="hidden" name="action" value="addworkflow">

		<div class="control-group">
			<label class="control-label"><?php printMLText("workflow_name");?>:</label>
			<div class="controls">
				<input type="text" class="form-control" name="name">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php printMLText("workflow_initstate");?>:</label>
			<div class="controls">
				<select name="initstate" class="form-control">
				<?php
				foreach($workflowstates as $workflowstate) {
					echo "<option value=\"".$workflowstate->getID()."\"";
					echo ">".htmlspecialchars($workflowstate->getName())."</option>\n";
				}
				?>
			</select>
			</div>
		</div>
		<br>
		<div class="controls">
			<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save")?></button>
		</div>
	</form>
	</div>
<?php $this->endsBoxCollapsablePrimary(); ?>
</div>
<div class="row">
<div class="col-md-12">
<?php $this->startBoxSolidPrimary(getMLText("workflow")); ?>

<div id="workflow-info" class="col-md-12">
		<div class="ajax" data-view="WorkflowMgr" data-action="form" <?php echo ($selworkflow ? "data-query=\"workflowid=".$selworkflow->getID()."\"" : "") ?>></div>
</div>

<div id="workflow-preview" class="col-md-12 ajax" data-view="WorkflowMgr" data-action="info" <?php echo ($selworkflow ? "data-query=\"workflowid=".$selworkflow->getID()."\"" : "") ?>></div>
</div>

<?php $this->endsBoxSolidPrimary(); ?>
</div>

<?php

		echo "</div>";
		
    $this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>