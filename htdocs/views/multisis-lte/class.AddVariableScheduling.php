<?php
/**
 * Implementation of ManualVariables view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_AddVariableScheduling extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		if(isset($this->params['seleval']))
			$currEval = $this->params['seleval'];

		if(isset($this->params['schedule']))
			$schedule = $this->params['schedule'];

		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		//$this->printFolderChooserJs("form".($currEval ? $currEval->getDate() : '0'));
?>

$(document).ready( function() {
	// For tables
	$(".table-two").DataTable({
	    "paging": false,
	    "lengthChange": false,
	    "searching": false,
	    "ordering": false,
	    "info": false,
	    "autoWidth": false,
	});

	$('.date').datepicker({
		language: 'es',
		format: 'yyyy',
		autoclose: true,
		minViewMode: 2,
	});
});

$('#form1').on('submit', function(e){
	selected_axe = $("#selector_one option:selected").val();
	selected_inst = $("#selector_two option:selected").val();
	selected_var = $("#selector_three option:selected").val();
	selected_group = $("#selector_group option:selected").val();
	selected_type = $("#selector_type option:selected").val();
	period = $("#period").val();

	if(selected_axe == -1 || selected_inst == -1 || selected_var == -1 || selected_group == -1 || selected_type == -1 || period == ""){
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
		event.preventDefault();
	}	
});

$( "#selector_one" ).change(function() {
	selected_axe = $(this).val();
	if(selected_axe == -1) {
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#instruments-ajax').trigger('update', { selected_axe: selected_axe });
	}
});

$(document).on('change', '#selector_two', function(event) {
    event.preventDefault();

	selected_instrument = $(this).val();
	if(selected_instrument == -1) {
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#variables-ajax').trigger('update', { selected_instrument: selected_instrument });
	}
});

$( "#selector_group" ).change(function() {
	selected_group = $(this).val();
	if(selected_group == -1) {
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#categories-ajax').trigger('update', { selected_group: selected_group });
	}
});

$(document).on('change', '#selector_category', function(event) {
    event.preventDefault();

    selected_category = $(this).val();
	if(selected_category == -1) {
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#types-ajax').trigger('update', { selected_category: selected_category });
	}
});

<?php
	} /* }}} */

	function instruments(){
		$dms = $this->params['dms'];
		$instruments = $this->params['instruments'];
		$selected_instrument = $this->params['selected_instrument'];
		$schedule = $this->params['schedule'];

		
			echo '<br/>';
			echo '<label class="control-label">'.getMLText("choose_instrument").':</label>';
			echo '<div class="controls">';

			if ($schedule) {
				echo '<select class="chzn-select" id="selector_two" name="selector_instrument" disabled="true">';
				print "<option value=\"".$selected_instrument->getID()."\" selected='true'>" . htmlspecialchars($selected_instrument->getName()) . "</option>";
			} else {
				echo '<select class="chzn-select" id="selector_two" name="selector_instrument">';
				echo '<option value="-1">'.getMLText("choose_instrument").'</option>';

				if (!empty($instruments)) {
					foreach ($instruments as $instrument) {
						$inst = $dms->getInstrument($instrument['instrumentID']);
						if ($schedule && ((int)$inst->getID() === (int)$schedule[0]['instrumentID'])) {
							print "<option value=\"".$inst->getID()."\" selected='true'>" . htmlspecialchars($inst->getName()) . "</option>";	
						} else {
							print "<option value=\"".$inst->getID()."\" >" . htmlspecialchars($inst->getName()) . "</option>";
						}
						
					}
				}

			}
			
			

			echo '</select>';
			echo '</div>';
		
		
	}

	function variables(){
		$dms = $this->params['dms'];
		$variables = $this->params['variables'];
		$selected_variable = $this->params['selected_variable'];
		$schedule = $this->params['schedule'];

			echo '<br/>';
			echo '<label class="control-label">'.getMLText("choose_auto_variable").':</label>';
			echo '<div class="controls">';

			if ($selected_variable) {
				echo '<select class="chzn-select" id="selector_three" name="selector_variable" disabled="true">';
				print "<option value=\"".$selected_variable->getID()."\" selected='true'>" . htmlspecialchars($selected_variable->getName()) . "</option>";
			} else {
				echo '<select class="chzn-select" id="selector_three" name="selector_variable">';
				echo '<option value="-1">'.getMLText("choose_auto_variable").'</option>';
				if (!empty($variables)) {
					foreach ($variables as $variable) {
						if (!((int)$variable->getIsManual())) {
							if ($schedule && ((int)$variable->getID() === (int)$schedule[0]['variableID'])) {
								print "<option value=\"".$variable->getID()."\" selected='true'>" . htmlspecialchars($variable->getCode()." - ".$variable->getName()) . "</option>";
							} else {
								print "<option value=\"".$variable->getID()."\" >" . htmlspecialchars($variable->getCode()." - ".$variable->getName()) . "</option>";	
							}
						}		
					}
				}
			}
			
			echo '</select>';
			echo '</div>';
		
		
	}

	function categories_by_group(){
		$dms = $this->params['dms'];
		$schedule = $this->params['schedule'];
		$categories = $this->params['categories'];

		echo '<label class="control-label">'.getMLText("categories_to_publish").':</label>';
		echo '<div class="controls">';

		if ($schedule) {
			echo '<select class="form-control" id="selector_category" name="selector_category" disabled="true">';
		} else {
			echo '<select class="form-control" id="selector_category" name="selector_category">';	
		}

		echo '<option value="-1">'.getMLText("choose_category").'</option>';
		
		if (!empty($categories)) {
				foreach ($categories as $category) {
					print "<option value=\"".$category['id']."\" >" . htmlspecialchars($category['name']) . "</option>";	
							
				}
		}

		echo '</select>';
		echo '</div>';
		
	}

	function types_by_category(){
		$dms = $this->params['dms'];
		$types_by_category = $this->params['types_by_category'];
		$types = $this->params['types'];
		$selected_type = $this->params['seltype'];
		$schedule = $this->params['schedule'];

		echo '<label class="control-label">'.getMLText("type_linked_to_category").':</label>';
		echo '<div class="controls">';
		
		if ($selected_type != null) {
			echo '<select class="form-control" id="selector_type" name="selector_type" disabled="true">';
			print "<option value=\"".$selected_type->getID()."\" selected='true'>" . htmlspecialchars($selected_type->getName())."</option>";
		} else {
			echo '<select class="form-control" id="selector_type" name="selector_type">';
			echo '<option value="-1">'.getMLText("choose_the_type").'</option>';

			if (!empty($types_by_category)) {
				foreach ($types_by_category as $type) {
					if ($schedule && ((int)$type['id'] === (int)$schedule[0]['typeID'])) {
						print "<option value=\"".$type['id']."\" selected='true'>" . htmlspecialchars($type['name'])."</option>";
					} else {
						print "<option value=\"".$type['id']."\">" . htmlspecialchars($type['name'])."</option>";
					}
				}
			}
		}
		
		echo '</select>';
		echo '</div>';
	}

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$axes = $this->params['axes'];
		$httproot = $this->params['httproot'];
		$allGroups = $this->params['allgroups'];
		$selgroup = $this->params['selgroup'];
		$types = $this->params['types'];
		$seltype = $this->params['seltype'];
		$schedule = $this->params['schedule'];

		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datepicker/datepicker3.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/bootstrap-datepicker.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/chartjs/Chart.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');
		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>

		<style type="text/css">
			.table-bordered>thead>tr>th {
  				background-color: #3d8dbc;
    			color: #ffffff;
			}
		</style>

	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("instrumentvars"));

?>

<div class="row-fluid">
<form action="/op/op.AddVariableScheduling.php" method="POST" id="form1">
<input type="hidden" name="action" value="addvariablescheduling">
<?php echo createHiddenFieldWithKey("addvariablescheduling"); ?>

<?php if ($schedule): ?>
	<input type="hidden" name="selector_axe" value="<?php echo $schedule[0]['axeID']; ?>">
	<input type="hidden" name="selector_instrument" value="<?php echo $schedule[0]['instrumentID']; ?>">
	<input type="hidden" name="selector_variable" value="<?php echo $schedule[0]['variableID']; ?>">
	<input type="hidden" name="selector_group" value="<?php echo $schedule[0]['groupID']; ?>">
	<input type="hidden" name="selector_type" value="<?php echo $schedule[0]['typeID']; ?>">
	<input type="hidden" name="period" value="<?php echo $schedule[0]['year']; ?>">
	<input type="hidden" name="scheduleid" value="<?php echo $schedule[0]['id']; ?>">
<?php endif ?>

<div class="col-md-8">
<div class="well">

<div class="control-group">
<label class="control-label"><?php printMLText("choose_axe");?>:</label>
<div class="controls">
<?php if ($schedule): ?>
	<select class="chzn-select" id="selector_one" name="selector_axe" disabled="true">		
<?php else: ?>
	<select class="chzn-select" id="selector_one" name="selector_axe">
<?php endif ?>
<option value="-1"><?php echo getMLText("choose_axe")?></option>
<?php
	if (!empty($axes)) {
		foreach ($axes as $axe) {
			if ($schedule && ((int)$axe->getID() === (int)$schedule[0]['axeID'])) {
				print "<option value=\"".$axe->getID()."\" selected='true'>" . htmlspecialchars($axe->getName()) . "</option>";		
					
			} else {
				print "<option value=\"".$axe->getID()."\" >" . htmlspecialchars($axe->getName()) . "</option>";		
			}
		}
	}
?>
</select>
</div>
</div>


<?php if ($schedule) {

	echo '<div class="control-group">';
	echo '<br>';
	echo '<label class="control-label">'.getMLText("choose_instrument").':</label>';
	echo '<div class="controls">';

	$inst = $dms->getInstrument($schedule[0]['instrumentID']);

	echo '<select class="chzn-select" id="selector_two" name="selector_instrument" disabled="true">';
	print "<option value=\"".$inst->getID()."\" selected='true'>" . htmlspecialchars($inst->getName()) . "</option>";
			
	echo '</select>';
	echo '</div>';
	echo '</div>';

	echo '<div class="control-group">';
	echo '<br>';
	echo '<label class="control-label">'.getMLText("choose_auto_variable").':</label>';
	echo '<div class="controls">';

	$var = $dms->getInstrumentVar($schedule[0]['variableID']);

	echo '<select class="chzn-select" id="selector_three" name="selector_variable" disabled="true">';
	print "<option value=\"".$var->getID()."\" selected='true'>" . htmlspecialchars($var->getCode()." - ".$var->getName()) . "</option>";	
			
	echo '</select>';
	echo '</div>';
	echo '</div>';

} else { ?>

<!-- Show instrument result -->
<div class="control-group">
<div id="instruments-ajax" class="ajax" data-view="AddVariableScheduling" data-action="instruments">
</div>
</div>

<!-- Show variables result -->
<div class="control-group">
<div id="variables-ajax" class="ajax" data-view="AddVariableScheduling" data-action="variables">
</div>
</div>

<?php } ?>

</div>
</div>


<div class="col-md-4">
<div class="well">

<!-- Choose the group -->
<div class="form-group">
<label class="control-label"><?php printMLText("choose_the_group");?>:</label>
<div class="controls">
<?php if ($schedule): ?>
	<select class="chzn-select form-control" id="selector_group" name="selector_group" disabled="true">
<?php else: ?>
	<select class="chzn-select form-control" id="selector_group" name="selector_group">
<?php endif ?>
<option value="-1"><?php echo getMLText("choose_the_group")?></option>
<?php
	foreach ($allGroups as $group) {
		if ($schedule && ((int)$group->getID() === (int)$schedule[0]['groupID'])) {
			print "<option value=\"".$group->getID()."\" selected='true'>" . htmlspecialchars($group->getName()) . "</option>";
		} else {
			print "<option value=\"".$group->getID()."\" >" . htmlspecialchars($group->getName()) . "</option>";	
		}
		
	}
?>
</select>
</div>
</div>

<?php if ($schedule) {

	echo '<div class="form-group">';
	echo '<label class="control-label">'.getMLText("categories_to_publish").':</label>';
	echo '<div class="controls">';

	echo '<select class="form-control" id="selector_category" name="selector_category" disabled="true">';
	echo '<option value="-1">'.getMLText("choose_category").'</option>';
			
	echo '</select>';
	echo '</div>';
	echo '</div>';

	echo '<div class="form-group">';
	echo '<label class="control-label">'.getMLText("type_linked_to_category").':</label>';
	echo '<div class="controls">';

	$type = $dms->getDocumentType($schedule[0]['typeID']);

	echo '<select class="form-control" id="selector_type" name="selector_type" disabled="true">';
	print "<option value=\"".$type->getID()."\" selected='true'>" . htmlspecialchars($type->getName())."</option>";
			
	echo '</select>';
	echo '</div>';
	echo '</div>';

} else { ?>

<!-- Show category result -->
<div class="form-group">
<div id="categories-ajax" class="ajax" data-view="AddVariableScheduling" data-action="categories_by_group">
</div>
</div>


<!-- Show types result -->
<div class="form-group">
<div id="types-ajax" class="ajax" data-view="AddVariableScheduling" data-action="types_by_category">
</div>
</div>

<?php } ?>



<!-- Select year -->
<div class="form-group">
<label class="control-label" for="login"><?php printMLText("choose_year");?>:</label>
<div class="input-group">
<div class="input-group-addon">
<i class="fa fa-calendar"></i>
</div>
<input type="text" id="period" name="period" value="<?php echo ($schedule) ? $schedule[0]['year'] : ''; ?>" class="form-control date" required="required" autocomplete="off" placeholder="yyyy" <?php echo ($schedule) ? "disabled='true'" : '' ?>/> 
</div>
</div>


</div>
</div>


</div>

<!-- Get Result -->
<div class="row">
<br/>
<br/>
<div class="col-md-12">
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="align-center">Enero</th>
					<th class="align-center">Febrero</th>
					<th class="align-center">Marzo</th>
					<th class="align-center">Abril</th>
					<th class="align-center">Mayo</th>
					<th class="align-center">Junio</th>
					<th class="align-center">Julio</th>
					<th class="align-center">Agosto</th>
					<th class="align-center">Septiembre</th>
					<th class="align-center">Octubre</th>
					<th class="align-center">Noviembre</th>
					<th class="align-center">Diciembre</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><input class="form-control" id="evalJan" name="evalJan" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalJan'] : '' ?>"></td>
					<td><input class="form-control" id="evalFeb" name="evalFeb" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalFeb'] : '' ?>"></td>
					<td><input class="form-control" id="evalMar" name="evalMar" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalMar'] : '' ?>"></td>
					<td><input class="form-control" id="evalApr" name="evalApr" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalApr'] : '' ?>"></td>
					<td><input class="form-control" id="evalMay" name="evalMay" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalMay'] : '' ?>"></td>
					<td><input class="form-control" id="evalJun" name="evalJun" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalJun'] : '' ?>"></td>
					<td><input class="form-control" id="evalJul" name="evalJul" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalJul'] : '' ?>"></td>
					<td><input class="form-control" id="evalAug" name="evalAug" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalAug'] : '' ?>"></td>
					<td><input class="form-control" id="evalSep" name="evalSep" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalSep'] : '' ?>"></td>
					<td><input class="form-control" id="evalOct" name="evalOct" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalOct'] : '' ?>"></td>
					<td><input class="form-control" id="evalNov" name="evalNov" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalNov'] : '' ?>"></td>
					<td><input class="form-control" id="evalDec" name="evalDec" type="number" min="0" value="<?php echo ($schedule) ? $schedule[0]['evalDec'] : '' ?>"></td>
				</tr>
			</tbody>
		</table>	
	</div>
	
</div>

	<!-- Save for variable -->
<div class="col-md-12 align-center">
	<button type="submit" id="save_scheduling" class="btn btn-info"><i class="fa fa-save"></i> <?php echo getMLText("save") ?></button>
</div>
</form>

</div>

<div class="row-fluid">
	<div class="col-md-6">
		<div class="pull-left">
		<?php
			if ($schedule) {
				$link = "/out/out.VariableScheduling.php?selected_axe=".$schedule[0]['axeID']."&selected_inst=".$schedule[0]['instrumentID']."&selected_var=".$schedule[0]['variableID']."&selected_year=".$schedule[0]['year'];
			} else {
				$link = "/out/out.VariableScheduling.php";
			}
		?>
		<a type="button" href="<?php echo $link; ?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> <?php echo getMLText("come_back") ?></a>	
		</div>
	</div>
	<div class="col-md-6">
		<div class="pull-right">
		<a type="button" href="/out/out.AddVariableScheduling.php" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo getMLText("add_new") ?></a>	
		</div>
	</div>
</div>

<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
