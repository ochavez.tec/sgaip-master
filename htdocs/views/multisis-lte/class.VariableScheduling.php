<?php
/**
 * Implementation of ManualVariables view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_VariableScheduling extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		if(isset($this->params['seleval']))
			$currEval = $this->params['seleval'];
		
		$selected_instrument = $this->params['inst'];
		$selected_variable = $this->params['var'];
		$selected_axe = $this->params['axe'];
		$selected_year = $this->params['year'];

		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		//$this->printFolderChooserJs("form".($currEval ? $currEval->getDate() : '0'));
?>

$( "#get-result" ).click(function() {
	selected_axe = $("#selector_one option:selected").val();
	selected_inst = $("#selector_two option:selected").val();
	selected_var = $("#selector_three option:selected").val();
	period = $("#period").val();

	if(selected_axe == -1 || selected_inst == -1 || selected_var == -1 || period == ""){
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#ajax2').trigger('update', {selected_var: selected_var, selected_inst: selected_inst, selected_axe: selected_axe, year: period });
	}

	
});

$( "#selector_one" ).change(function() {
	selected_axe = $(this).val();
	if(selected_axe == -1) {
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#ajax1').trigger('update', { selected_axe: selected_axe });
	}
});

$(document).on('change', '#selector_two', function(event) {
    event.preventDefault();

	selected_instrument = $(this).val();
	if(selected_instrument == -1) {
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#ajax3').trigger('update', { selected_inst: selected_instrument });
	}
});

$(document).ready( function() {
	// For tables
	$(".vars-table").DataTable({
	    "paging": false,
	    "lengthChange": false,
	    "searching": true,
	    "ordering": false,
	    "info": false,
	    "autoWidth": true,
	});

	$('.date').datepicker({
		language: 'es',
		format: 'yyyy',
		autoclose: true,
		minViewMode: 2,
	});

	<?php if ($selected_axe) { ?>
		$('#ajax1').trigger('update', { selected_axe: <?php echo $selected_axe; ?>, selected_inst: <?php echo $selected_instrument; ?> });
	<?php } ?>

	<?php if ($selected_instrument) { ?>
		$('#ajax3').trigger('update', { selected_inst: <?php echo $selected_instrument; ?>, selected_var: <?php echo $selected_variable; ?> });
	<?php } ?>

	<?php if ($selected_axe && $selected_instrument && $selected_variable && $selected_year) { ?>
		$('#ajax2').trigger('update', {selected_var:<?php echo $selected_variable; ?>, selected_inst: <?php echo $selected_instrument; ?>, selected_axe: <?php echo $selected_axe; ?>, year: <?php echo $selected_year; ?> });
	<?php } ?>
});



<?php
	} /* }}} */

	function instruments(){
		$dms = $this->params['dms'];
		$instruments = $this->params['instruments'];

		$selected_instrument = $this->params['inst'];
		
		
			echo '<br/>';
			echo '<label class="control-label">'.getMLText("choose_instrument").':</label>';
			echo '<div class="controls">';
			echo '<select class="chzn-select" id="selector_two">';
			echo '<option value="-1">'.getMLText("choose_instrument").'</option>';
		
			if (!empty($instruments)) {
				foreach ($instruments as $instrument) {
					$inst = $dms->getInstrument($instrument['instrumentID']);
					if ($selected_instrument && ((int)$inst->getID() === (int)$selected_instrument)) {
						print "<option value=\"".$inst->getID()."\" selected='true'>" . htmlspecialchars($inst->getName()) . "</option>";	
					} else {
						print "<option value=\"".$inst->getID()."\" >" . htmlspecialchars($inst->getName()) . "</option>";
					}
					
				}
			}

			echo '</select>';
			echo '</div>';
		
		
	}

	function variables(){
		$dms = $this->params['dms'];
		$variables = $this->params['variables'];

		$selected_variable = $this->params['var'];

		
			echo '<br/>';
			echo '<label class="control-label">'.getMLText("choose_auto_variable").':</label>';
			echo '<div class="controls">';
			echo '<select class="chzn-select" id="selector_three">';
			echo '<option value="-1">'.getMLText("choose_auto_variable").'</option>';
		
			if (!empty($variables)) {
				foreach ($variables as $variable) {
					if (!((int)$variable->getIsManual())) {
						if ($selected_variable && ((int)$variable->getID() === (int)$selected_variable)) {
							print "<option value=\"".$variable->getID()."\" selected='true'>" . htmlspecialchars($variable->getCode()." - ".$variable->getName()) . "</option>";
						} else {
							print "<option value=\"".$variable->getID()."\" >" . htmlspecialchars($variable->getCode()." - ".$variable->getName()) . "</option>";
						} 
					}		
				}
			}

			echo '</select>';
			echo '</div>';
		
		
	}

	function schedule(){
		$dms = $this->params['dms'];
		$schedule = $this->params['schedule'];

		if (!empty($schedule)) {
			echo '<br/>';
			echo '<div class="box box-success">';					
			echo '<div class="box-body">';
			echo '<div class="table-responsive">';
            echo '<table class="vars-table table table-bordered table-striped">';
            echo '<thead>';
            echo '<tr>';
            echo '<th class="align-center">'.getMLText('group').'</th>';
            echo '<th class="align-center">'.getMLText('type').'</th>';
            echo '<th class="align-center">Ene</th>';
            echo '<th class="align-center">Feb</th>';
            echo '<th class="align-center">Mar</th>';
            echo '<th class="align-center">Abr</th>';
            echo '<th class="align-center">May</th>';
            echo '<th class="align-center">Jun</th>';
            echo '<th class="align-center">Jul</th>';
            echo '<th class="align-center">Ago</th>';
            echo '<th class="align-center">Sep</th>';
            echo '<th class="align-center">Oct</th>';
            echo '<th class="align-center">Nov</th>';
            echo '<th class="align-center">Dic</th>';
            echo '<th class="align-center"></th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            	foreach ($schedule as $var) {
            		echo '<tr id="tr-'.$var['id'].'">';
	                
	                echo '<td class="align-center">';
	                $group = $dms->getGroup($var['groupID']);
	                echo $group->getName();
	                echo '</td>';
	                
	                echo '<td class="align-center">';
	                $type = $dms->getDocumentType($var['typeID']);
	                echo $type->getName();
	                echo '</td>';

	                echo '<td>'.$var['evalJan'].'</td>';
	                echo '<td>'.$var['evalFeb'].'</td>';
	                echo '<td>'.$var['evalMar'].'</td>';
	                echo '<td>'.$var['evalApr'].'</td>';
	                echo '<td>'.$var['evalMay'].'</td>';
	                echo '<td>'.$var['evalJun'].'</td>';
	                echo '<td>'.$var['evalJul'].'</td>';
	                echo '<td>'.$var['evalAug'].'</td>';
	                echo '<td>'.$var['evalSep'].'</td>';
	                echo '<td>'.$var['evalOct'].'</td>';
	                echo '<td>'.$var['evalNov'].'</td>';
	                echo '<td>'.$var['evalDec'].'</td>';

	                echo '<td class="align-center">';
	               	echo '<a data-id="'.$var['id'].'" href="/out/out.AddVariableScheduling.php?scheduleid='.$var['id'].'" type="button" class="variable-action btn btn-xs btn-success"><i class="fa fa-pencil fa-1x"></i></a>';	               
	                echo '<a type="button" class="btn btn-danger delete-btn btn-xs" rel="'.$var['id'].'" msg="'.getMLText('rm_schedule').'" confirmmsg="'.htmlspecialchars(getMLText("confirm_rm_schedule")).'" data-toggle="tooltip" data-placement="bottom" title="'.getMLText("rm_schedule").'"><i class="fa fa-times fa-1x"></i></a>';
	                echo '</td>';

	                echo '</tr>';
            	}

            echo '</tbody>';
            echo '</table>';
            echo '</div>';
            echo '</div>';
            echo '</div>';

		}

		echo '<script>';
        echo '$(function () {';
        echo 'formtoken = "'.createFormKey("removevariableschedule").'";
  					/* DELETE */
					$(".delete-btn").on("click", function(ev){
						id = $(this).attr("rel");
						confirmmsg = $(ev.currentTarget).attr("confirmmsg");
						msg = $(ev.currentTarget).attr("msg");
						formtoken = "'.createFormKey("removevariableschedule").'";
						bootbox.confirm({
				    		message: confirmmsg,
				    		buttons: {
				        		confirm: {
				            		label: "<i class=\"fa fa-times\"></i> '.getMLText("rm_schedule").'",
				            		className: "btn-danger"
				        		},
				        		cancel: {
				            		label: "'.getMLText("cancel").'",
				            		className: "btn-default"
				        		}
				    		},
					    	callback: function (result) {
					    		if (result) {
					    			$.get("/op/op.AddVariableScheduling.php",
									{ action: "removevariableschedule", schedule_id: id, formtoken: formtoken })
									.done(function(data) {
										if(data.success) {							
											noty({
												text: data.message,
												type: "success",
												dismissQueue: true,
												layout: "topRight",
												theme: "defaultTheme",
												timeout: 1500,
											});

											$("#tr-"+id).remove();
											
										} else {
											noty({
												text: data.message,
												type: "error",
												dismissQueue: true,
												layout: "topRight",
												theme: "defaultTheme",
												timeout: 3500,
											});
										}
									});
								}
					    	}
						});
					});;
		});';

        echo '</script>';
	}

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$axes = $this->params['axes'];
		$httproot = $this->params['httproot'];

		$selected_instrument = $this->params['inst'];
		$selected_variable = $this->params['var'];
		$selected_axe = $this->params['axe'];
		$selected_year = $this->params['year'];

		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datepicker/datepicker3.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/bootstrap-datepicker.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/chartjs/Chart.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');
		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("automatic_variables_validation"));

?>

<div class="row-fluid">

<div class="col-md-8">
<div class="well">

<div class="control-group">
<label class="control-label"><?php printMLText("choose_axe");?>:</label>
<div class="controls">
<select class="chzn-select" id="selector_one">
<option value="-1"><?php echo getMLText("choose_axe")?></option>
<?php
	if (!empty($axes)) {
		foreach ($axes as $axe) {
			if ($selected_axe && ((int)$axe->getID() === (int)$selected_axe)) {
				print "<option value=\"".$axe->getID()."\" selected='true'>" . htmlspecialchars($axe->getName()) . "</option>";
			} else {
				print "<option value=\"".$axe->getID()."\" >" . htmlspecialchars($axe->getName()) . "</option>";
			}
		}
	}
?>
</select>
</div>
</div>

<!-- Show instrument result -->
<div class="control-group">
<div id="ajax1" class="ajax" data-view="VariableScheduling" data-action="instruments">
</div>
</div>

<!-- Show variables result -->
<div class="control-group">
<div id="ajax3" class="ajax" data-view="VariableScheduling" data-action="variables">
</div>
</div>

</div>
</div>

<!-- Select year -->
<div class="col-md-4">
<div class="well">
<div class="form-group">
<label class="control-label" for="login"><?php printMLText("choose_year");?>:</label>
<div class="input-group">
<div class="input-group-addon">
<i class="fa fa-calendar"></i>
</div>
<input type="text" id="period" name="date" value="<?php echo ($selected_year) ? $selected_year : ""; ?>" class="form-control date" required="required" autocomplete="off" placeholder="yyyy" /> 
</div>
</div>
</div>
</div>


<!-- Make consult for variable -->
<div class="col-md-12 align-center">
	<a type="button" id="get-result" class="btn btn-info"><i class="fa fa-refresh"></i> <?php echo getMLText("consult") ?></a>
</div>
<div class="col-md-12">
	<div class="pull-right">
		<a type="button" href="/out/out.AddVariableScheduling.php" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo getMLText("add") ?></a>	
	</div>
</div>
</div>

<!-- Get Result -->
<div class="row-fluid">
<div class="col-md-12">
	<div id="ajax2" class="ajax" data-view="VariableScheduling" data-action="schedule">
	</div>
</div>
</div>



<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
