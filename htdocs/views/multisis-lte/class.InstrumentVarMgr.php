<?php
/**
 * Implementation of UsrMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_InstrumentVarMgr extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$currInstrumentVar = $this->params['selinstrumentvar'];
		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		$this->printFolderChooserJs("form".($currInstrumentVar ? $currInstrumentVar->getID() : '0'));
?>
function folderSelected(id, name) {
	window.location = '../out/out.ViewFolder.php?folderid=' + id;
}

function checkForm()
{
	msg = new Array();

	if($("#code").val() == "") msg.push("<?php printMLText("js_no_code");?>");
	if($("#name").val() == "") msg.push("<?php printMLText("js_no_name");?>");
	if($("#weight").val() == "") msg.push("<?php printMLText("js_no_weight");?>");
//	if($("#instrumentid").val() == "") msg.push("<?php printMLText("js_no_instrumentid");?>");	
<?php
	if ($strictformcheck) {
?>
	if($("#comment").val() == "") msg.push("<?php printMLText("js_no_comment");?>");
<?php
	}
?>
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$(document).ready( function() {
	$('body').on('submit', '#form', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});
	$( "#selector" ).change(function() {
		if($(this).val() == -1){
			noty({
				text: "<?php echo getMLText('no_var_selected'); ?>",
				type: "error",
				dismissQueue: true,
				layout: "topRight",
				theme: "defaultTheme",
				timeout: 3500,
			});
		} else {
			$('div.ajax').trigger('update', {instrumentvarid: $(this).val()});
		}
		
	});
	
	$('.input[type="checkbox"]').iCheck();
	
});
<?php
	} /* }}} */

	function info() { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrumentvar']))
			$instrumentvar = $this->params['instrumentvar'];
		if(isset($this->params['selinstrumentvar']))
			$selinstrumentvar = $this->params['selinstrumentvar'];

		if(isset($selinstrumentvar)) {
			$sessionmgr = new SeedDMS_SessionMgr($dms->getDB());

 			$this->startBoxCollapsableInfo(getMLText("instrumentvar_info"));
// 			echo "<table class=\"table table-striped table-bordered\">\n";
// 			echo "<tr><td>".getMLText('discspace')."</td><td>";
// 			$qt = $selinstrument->getQuota() ? $selinstrument->getQuota() : $quota;
// 			echo SeedDMS_Core_File::format_filesize($selinstrument->getUsedDiskSpace())." / ".SeedDMS_Core_File::format_filesize($qt)."<br />";
// 			echo $this->getProgressBar($selinstrument->getUsedDiskSpace(), $qt);
// 			echo "</td></tr>\n";
// 			$documents = $selinstrument->getDocuments();
// 			echo "<tr><td>".getMLText('documents')."</td><td>".count($documents)."</td></tr>\n";
// 			$documents = $selinstrument->getDocumentsLocked();
// 			echo "<tr><td>".getMLText('documents_locked')."</td><td>".count($documents)."</td></tr>\n";
// 			if($workflowmode == "traditional") {
// 				$reviewStatus = $selinstrument->getReviewStatus();
// 				if($reviewStatus['indstatus']) {
// 					$i = 0;
// 					foreach($reviewStatus['indstatus'] as $rv) {
// 						if($rv['status'] == 0) {
// 							$i++;
// 						}
// 					}
// 					echo "<tr><td>".getMLText('pending_reviews')."</td><td>".$i."</td></tr>\n";
// 				}
// 			}
// 			if($workflowmode == "traditional" || $workflowmode == 'traditional_only_approval') {
// 				$approvalStatus = $selinstrument->getApprovalStatus();
// 				if($approvalStatus['indstatus']) {
// 					$i = 0;
// 					foreach($approvalStatus['indstatus'] as $rv) {
// 						if($rv['status'] == 0) {
// 							$i++;
// 						}
// 					}
// 					echo "<tr><td>".getMLText('pending_approvals')."</td><td>".$i."</td></tr>\n";
// 				}
// 			}
// 			if($workflowmode == 'advanced') {
// 				$workflowStatus = $selinstrument->getWorkflowStatus();
// 				if($workflowStatus['u'])
// 					echo "<tr><td>".getMLText('pending_workflows')."</td><td>".count($workflowStatus['u'])."</td></tr>\n";
// 			}
// 			$sessions = $sessionmgr->getUserSessions($seluser);
// 			if($sessions) {
// 				$session = array_shift($sessions);
// 				echo "<tr><td>".getMLText('lastaccess')."</td><td>".getLongReadableDate($session->getLastAccess())."</td></tr>\n";
// 			}
// 			echo "</table>";
// 			echo "<br>";


// 			if($user->isAdmin() && $selinstrument->getID() != $instrument->getID())
// 					echo "<a class=\"btn btn-success\" href=\"../op/op.SubstituteUser.php?userid=".((int) $seluser->getID())."&formtoken=".createFormKey('substituteuser')."\"><i class=\"fa fa-exchange\"></i> ".getMLText('substitute_user')."</a> ";
			$this->endsBoxCollapsableInfo();
		}
	} /* }}} */

	function form() { /* {{{ */
		$selinstrumentvar = $this->params['selinstrumentvar'];

		$this->showInstrumentVarForm($selinstrumentvar);
	} /* }}} */

	function showInstrumentVarForm($currInstrumentVar) { /* {{{ */
		if(isset($this->params['instrumentvar']))
			$instrumentvar = $this->params['instrumentvar'];
		$instrumentvars = $this->params['allinstrumentvars'];
		$instruments = $this->params['allinstruments'];
		$httproot = $this->params['httproot'];
?>
	<form action="../op/op.InstrumentVarMgr.php" method="post" enctype="multipart/form-data" name="<?php echo "form".($currInstrumentVar ? $currInstrumentVar->getID() : '0'); ?>" id="<?php echo "form".($currInstrumentVar ? $currInstrumentVar->getID() : '0'); ?>">
<?php
		if($currInstrumentVar) {
			echo createHiddenFieldWithKey('editinstrumentvar');
?>
	<input type="hidden" name="instrumentvarid" id="instrumentvarid" value="<?php print $currInstrumentVar->getID();?>">
	<input type="hidden" name="action" value="editinstrumentvar">
<?php
		} else {
			echo createHiddenFieldWithKey('addinstrumentvar');
?>
	<input type="hidden" name="instrumentvarid" id="instrumentvarid" value="0">
	<input type="hidden" name="action" value="addinstrumentvar">
<?php
		}
?>
<div class="table-responsive">
	<table class="table-condensed">
<?php
	if(isset($currInstrumentVar)) {
?>
		<tr>
			<td></td>
			<td><a class="btn btn-danger" href="../out/out.RemoveInstrumentVar.php?instrumentvarid=<?php print $currInstrumentVar->getID();?>"><i class="fa fa-times"></i> <?php printMLText("rm_instrumentvar");?></a></td>
		</tr>
<?php
	}
?>
		<tr>
			<td><?php printMLText("code");?>:</td>
			<td><input type="text" class="form-control" name="code" id="code" value="<?php print $currInstrumentVar ? htmlspecialchars($currInstrumentVar->getCode()) : "";?>"></td>
		</tr>
		<tr>
			<td><?php printMLText("name");?>:</td>
			<td><input type="text" class="form-control" name="name" id="name" value="<?php print $currInstrumentVar ? htmlspecialchars($currInstrumentVar->getName()) : "";?>"></td>
		</tr>
		<tr>
			<td><?php printMLText("score");?>:</td>
			<td><input type="text" class="form-control" name="weight" id="weight" value="<?php print $currInstrumentVar ? htmlspecialchars($currInstrumentVar->getWeight()) : "";?>"></td>
		</tr>
		<tr>
			<td><?php printMLText("comment");?>:</td>
			<td><textarea name="comment" class="form-control" id="comment" rows="4" cols="50"><?php print $currInstrumentVar ? htmlspecialchars($currInstrumentVar->getComment()) : "";?></textarea></td>
		</tr>
		<tr>
			<td><?php printMLText("is_manual_variable");?>:</td>
			<td><input type="checkbox" name="is_manual" value="1" <?php echo (isset($currInstrumentVar) && $currInstrumentVar->getIsManual() === '1') ? "checked" : "" ?>></td>
		</tr>
		<tr>
			<td><?php printMLText("instrument");?>:</td>
			<td><select class="chzn-select form-control" name="instruments[]" data-placeholder="<?php printMLText('select_instruments'); ?>">
<?php
		foreach($instruments as $instrument) {
			echo '<option value="'.$instrument->getID().'"'.($currInstrumentVar && ($currInstrumentVar->getInstrumentID() == $instrument->getID()) ? ' selected' : '').'>'.$instrument->getName().'</option>';
		}
?>
			</select></td>
		</tr>
		

		<tr><td>&nbsp;</td></tr>
		<tr>
			<td></td>
			<td><button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText($currInstrumentVar ? "save" : "add_instrumentvar")?></button></td>
		</tr>
	</table>
</div>
	</form>
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrumentvar']))
			$instrumentvar = $this->params['instrumentvar'];
		$selinstrumentvar = $this->params['selinstrumentvar'];
		$instrumentvars = $this->params['allinstrumentvars'];
		$instruments = $this->params['allinstruments'];
		$httproot = $this->params['httproot'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
    <div class="gap-10"></div>
    <div class="row">
    <div class="col-md-12">
    <?php 

		$this->startBoxPrimary(getMLText("instrumentvar_management"));
?>

<?php //if ($user->_comment != "client-admin") { ?>
<div class="row-fluid">
<div class="span12">
<div class="well">
<form class="form-horizontal">
	<div class="control-group">
		<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
		<div class="controls">
<select class="chzn-select" id="selector">
<option value="-1"><?php echo getMLText("choose_instrumentvar")?></option>
<option value="0"><?php echo getMLText("add_instrumentvar")?></option>
<?php
		foreach ($instrumentvars as $currInstrumentVar) {
			print "<option value=\"".$currInstrumentVar->getID()."\" ".($selinstrumentvar && $currInstrumentVar->getID()==$selinstrumentvar->getID() ? 'selected' : '')." data-subtitle=\"".getMLText("instrumentvar_weight").": ".htmlspecialchars($currInstrumentVar->getWeight())."\">" . htmlspecialchars($currInstrumentVar->getName()) . "</option>";
		}
?>
</select>
		</div>
	</div>
</form>
</div>
	<div class="ajax" data-view="InstrumentVarMgr" data-action="info" <?php echo ($selinstrumentvar ? "data-query=\"instrumentvarid=".$selinstrumentvar->getID()."\"" : "") ?>></div>
</div>

<div class="span12">
	<div class="well">
		<div class="ajax" data-view="InstrumentVarMgr" data-action="form" <?php echo ($selinstrumentvar ? "data-query=\"instrumentvarid=".$selinstrumentvar->getID()."\"" : "") ?>></div>
	</div>
</div>
</div>

<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
