<?php
/**
 * Implementation of TransferObjects view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2017 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for TransferObjects view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2017 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_TransferObjects extends SeedDMS_Bootstrap_Style {

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$rmuser = $this->params['rmuser'];
		$allusers = $this->params['allusers'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("transfer_objects"));

?>
<div class="alert">
<?php printMLText("confirm_transfer_objects", array ("username" => htmlspecialchars($rmuser->getFullName())));?>
</div>
<?php
		$this->contentContainerStart();
?>
<form class="form-horizontal" action="../op/op.UsrMgr.php" name="form1" method="post">
<input type="hidden" name="userid" value="<?php print $rmuser->getID();?>">
<input type="hidden" name="action" value="transferobjects">
<?php echo createHiddenFieldWithKey('transferobjects'); ?>

<div class="control-group">
	<label class="control-label" for="assignTo">
<?php printMLText("transfer_objects_to_user"); ?>:
	</label>
	<div class="controls">
<select name="assignTo" class="chzn-select">
<?php
		foreach ($allusers as $currUser) {
			if ($currUser->isGuest() || ($currUser->getID() == $rmuser->getID()) )
				continue;

			if ($rmuser && $currUser->getID()==$rmuser->getID()) $selected=$count;
			print "<option value=\"".$currUser->getID()."\">" . htmlspecialchars($currUser->getLogin()." - ".$currUser->getFullName());
		}
?>
</select>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<button type="submit" class="btn btn-warning"><i class="fa fa-warning"></i> <?php printMLText("transfer_objects");?></button>
	</div>
</div>

</form>
<?php
		$this->endsBoxPrimary();

		echo "</div>";
		echo "</div>";
		echo "</div>";
		echo "</div>";
		
    	$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>
