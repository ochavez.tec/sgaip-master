<?php
/**
 * Implementation of UsrMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_Inspections extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		if(isset($this->params['seleval']))
			$currEval = $this->params['seleval'];
		
		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		//$this->printFolderChooserJs("form".($currEval ? $currEval->getDate() : '0'));
?>



function checkForm() {
	msg = new Array();
	if($("#inspection-year").val() == "") msg.push("<?php printMLText("js_no_inpection_year");?>");
	if($("#inspection-start").val() == "") msg.push("<?php printMLText("js_no_inpection_start");?>");
	if($("#inspection-end").val() == "") msg.push("<?php printMLText("js_no_inpection_end");?>");
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      	dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$( "#selector" ).change(function() {
	selected_insp = $(this).val();
	$('div.ajax').trigger('update', {selected_inspection: selected_insp});
});

$(document).ready( function() {
	$('body').on('submit', '#form1', function(ev){
		if(!checkForm()) {
			return;
			ev.preventDefault();
		} else {
			$("#box-form1").append("<div class=\"overlay\"><i class=\"fa fa-refresh fa-spin\"></i></div>");
		}
	});

	/* For dates */
	$('#inspection-year').datepicker({
		language: 'es',
		format: 'yyyy',
	    viewMode: "years", 
    	minViewMode: "years",
    	autoclose: true,
	});
	/* For dates */
	/*$('.inspection-date').datepicker({
		language: 'es',
		format: 'mm-yyyy',
	    autoclose: true,
	    minViewMode: 1,
	});*/

	// For tables
	/*$(".items-table").DataTable({
	    "paging": false,
	    "lengthChange": false,
	    "searching": true,
	    "ordering": false,
	    "info": false,
	    "autoWidth": true,
	});*/

	/* DELETE INSPECTIONS */
	$('body').on('click', 'a.delete-inspection-btn', function(ev){
		id = $(ev.currentTarget).attr('rel');
		confirmmsg = $(ev.currentTarget).attr('confirmmsg');
		msg = $(ev.currentTarget).attr('msg');
		formtoken = "<?php echo createFormKey('removeinspection'); ?>";
		bootbox.confirm({
    		message: confirmmsg,
    		buttons: {
        		confirm: {
            		label: "<i class='fa fa-times'></i> <?php echo getMLText("rm_inspection"); ?>",
            		className: 'btn-danger'
        		},
        		cancel: {
            		label: "<?php echo getMLText("cancel"); ?>",
            		className: 'btn-default'
        		}
    		},
	    	callback: function (result) {
	    		if (result) {
	    			$.get('/op/op.Inspections.php',
					{ action: 'removeinspection', inspection_id: id, formtoken: formtoken })
					.done(function(data) {
						if(data.success) {							
							noty({
								text: data.message,
								type: 'success',
								dismissQueue: true,
								layout: 'topRight',
								theme: 'defaultTheme',
								timeout: 1500,
							});
							window.location.href = '/out/out.Inspections.php';		
						} else {
							noty({
								text: data.message,
								type: 'error',
								dismissQueue: true,
								layout: 'topRight',
								theme: 'defaultTheme',
								timeout: 3500,
							});
						}
					});
				}
	    	}
		});
	});
});

<?php
	} /* }}} */

	function info() { /* {{{ */
		
	} /* }}} */

	function form() { /* {{{ */
		if(isset($this->params['selected_inspection'])){
			$selected_inspection = $this->params['selected_inspection'];
		} else {
			$selected_inspection = '';
		}

		if ($selected_inspection >= 1) {
			$this->printInspectionForm();

		} else if ($selected_inspection == 0){
			$this->printInspectionForm();
		}

	} /* }}} */

	function printInspectionForm(){
		if(isset($this->params['selected_inspection'])){
			$selected_inspection = $this->params['selected_inspection'];
			$inspection = $this->params['inspection'];
		} else {
			$selected_inspection = 0;
			$inspection = array();
		}
		
		?>

		<div class="col-md-6">
		<div class="box box-success box-solid" id="box-form1">
		<div class="box-header with-border">
		<h3 class="box-title"><?php echo getMLText("add_inspection"); ?></h3>
		</div>
		<div class="box-body">
			<form id="form1" class="form-horizontal" action="../op/op.Inspections.php" method="post">
			
			<?php if ($selected_inspection >= 1) {
				echo '<input type="hidden" name="action" value="editinspection">';
				echo '<input type="hidden" name="inspection_id" value="'.$selected_inspection.'">';
				echo createHiddenFieldWithKey('editinspection');
			} else {
				echo '<input type="hidden" name="action" value="addinspection">';
				echo createHiddenFieldWithKey('addinspection');
			}?>
			<div class="control-group">
			<label class="control-label" for="login"><?php printMLText("inspection");?>:</label>
			<div class="input-group date">
		    <div class="input-group-addon">
		    <i class="fa fa-calendar"></i>
		    </div>
		    <input type="text" name="inspection_year" value="<?php echo (isset($inspection[0]['year']) ? $inspection[0]['year'] : ''); ?>" class="form-control" id="inspection-year" required="required" autocomplete="off" placeholder="yyyy" /> 
		    </div>
		    </div>
			<div class="control-group">
			<label class="control-label" for="login"><?php printMLText("inspection_start");?>:</label>
			<div class="input-group date">
		    <div class="input-group-addon">
		    <i class="fa fa-calendar"></i>
		    </div>
		    <input type="text" name="inspection_start" value="<?php echo (isset($inspection[0]['duration_start']) ? $inspection[0]['duration_start'] : ''); ?>" class="form-control inspection-date" required="required" autocomplete="off" placeholder="mm-yyyy" /> 
		    </div>
			</div>
			<div class="control-group">
			<label class="control-label" for="login"><?php printMLText("inspection_end");?>:</label>
			<div class="input-group date">
		    <div class="input-group-addon">
		    <i class="fa fa-calendar"></i>
		    </div>
		    <input type="text" name="inspection_stop" value="<?php echo (isset($inspection[0]['duration_stop']) ? $inspection[0]['duration_stop'] : ''); ?>" class="form-control inspection-date" required="required" autocomplete="off" placeholder="mm-yyyy" /> 
		    </div>
			</div>
			<br>
			<div class="controls">
			<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save")?></button>
			<?php if ($selected_inspection >= 1) {
				echo '<a type="button" class="btn btn-danger delete-inspection-btn" rel="'.$selected_inspection.'" msg="'.getMLText('rm_inspection').'" confirmmsg="'.htmlspecialchars(getMLText("confirm_rm_inspection")).'" data-toggle="tooltip" data-placement="bottom" title="'.getMLText("rm_inspection").'"><i class="fa fa-times"></i> '.getMLText('rm_inspection').'</a>';
			} ?>
			</div>
			</form>
		</div>
		</div>
		</div>

		<script type="text/javascript">
			/* For dates */
			$('#inspection-year').datepicker({
				language: 'es',
				format: 'yyyy',
			    viewMode: "years", 
		    	minViewMode: "years",
		    	autoclose: true,
			});
			/* For dates */
			$('.inspection-date').datepicker({
				language: 'es',
				format: 'mm-yyyy',
			    autoclose: true,
			    minViewMode: 1,
			});
		</script>
	<?php }

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$inspections = $this->params['inspections'];
		$all_thematics = $this->params['all_thematics'];
		$httproot = $this->params['httproot'];
		if(isset($this->params['selected_inspection']))
			$selected_inspection = $this->params['selected_inspection'];
		else
			$selected_inspection = 0;

		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datepicker/datepicker3.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/bootstrap-datepicker.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');
		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("inspections_management"));
?>

<div class="row-fluid">
<div class="col-md-6">
<div class="well row-fluid">
<form class="form-horizontal">
<div class="control-group">
<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
<div class="controls">
<select class="chzn-select" id="selector">
<option value="-1"><?php echo getMLText("choose_inspection")?></option>
<option value="0"><?php echo getMLText("add_inspection")?></option>
<?php
	if (!empty($inspections)) {
		foreach ($inspections as $inspection) {
			print "<option value=\"".$inspection['id']."\" >" . htmlspecialchars($inspection['year']) . "</option>";
		}
	}
?>
</select>
</div>
</div>
</form>
</div>
</div>


<div class="ajax" data-view="Inspections" data-action="form" <?php echo ($selected_inspection ? "data-query=\"selected_inspection=".$selected_inspection."\"" : "") ?>>
</div>


<div class="col-md-12">
<?php $this->startBoxPrimary("Checklist"); ?>

	<div class="col-md-12"><div class="pull-right"><a type="button" class="btn btn-success" href="/out/out.AddThematic.php"><i class="fa fa-plus"></i> <?php echo getMLText('add_thematic'); ?></a></div></div>
	<br/>
	<br/>
	<div class="col-md-12">
	<?php 
		foreach ($all_thematics as $thematic) {
			echo '<div class="box box-solid box-info">';
			echo '<div class="box-header with-border">';
			echo '<h3 class="box-title">'.$thematic['name'].'</h3>';
			echo '<div class="box-tools pull-right">';
			echo '<a type="button" title="'.getMLText("add_item").'" class="btn btn-primary" href="/out/out.ItemMgr.php?thematic_id='.$thematic['id'].'"><i class="fa fa-plus"></i></a>&nbsp';
			echo '<a type="button" title="'.getMLText("edit_thematic").'" class="btn btn-success" href="/out/out.EditThematic.php?thematic_id='.$thematic['id'].'"><i class="fa fa-pencil"></i></a>&nbsp';
			
			if (!isset($thematic['items'])) {				
				echo '<a type="button" title="'.getMLText("remove_thematic").'" class="btn btn-danger" href="/out/out.RemoveThematic.php?thematic_id='.$thematic['id'].'"><i class="fa fa-times"></i></a>';
			}
			echo '</div>';
			echo '</div>';
			echo '<div class="box-body">';

			if (isset($thematic['items']) && !empty($thematic['items'])) {
				echo '<div class="table-responsive">';
                echo '<table class="items-table table table-bordered table-striped">';
                echo '<thead>';
                echo '<tr>';
                echo '<th class="align-center">Nombre</th>';
                echo '<th class="align-center">Comentario</th>';
                echo '<th class="align-center">Tipo de calificación</th>';
                echo '<th class="align-center">Tipo de evaluación</th>';
                echo '<th class="align-center">Acciones</th>';
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
                	foreach ($thematic['items'] as $indx => $item) {
                		echo '<tr>';
                		echo '<td>';
                		echo $item['name'];
                		echo '</td>';
                		echo '<td>';
                		echo $item['comment'];
                		echo '</td>';
                		echo '<td class="align-center">';
                		if ($item['qualification_type']) {
                			echo '<button type="button" class="btn bg-purple">'.getMLText('manual').'</button>';
                		} else {
                			echo '<button type="button" class="btn bg-orange">'.getMLText('automatic').'</button>';
                		}
                		echo '</td>';
                		echo '<td class="align-center">';
                		if ($item['evaluation_type']) {
                			echo '<button type="button" class="btn bg-olive">'.getMLText('evaluated').'</button>';
                		} else {
                			echo '<button type="button" class="btn bg-navy">'.getMLText('good_practice').'</button>';
                		}
                		echo '</td>';
                		echo '<td class="align-center">';
                		echo '<div class="btn-group">';
		                echo '<a type="button" href="/out/out.ItemMgr.php?thematic_id='.$thematic['id'].'&item_id='.$item['id'].'" class="btn btn-success"><i class="fa fa-pencil"></i></a>';
		                echo '<a type="button" href="/out/out.RemoveItem.php?item_id='.$item['id'].'" class="btn btn-danger"><i class="fa fa-times"></i></a>';
		                echo '</div>';
                		echo '</td>';
                		echo '</tr>';
                	}
                echo '</tbody>';
                echo '</table>';
                echo '</div>';
			}

			echo '</div>';
			echo '</div>';
		}

	?>
	</div>

<?php $this->endsBoxSolidPrimary(); ?>
</div>

</div>

<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
