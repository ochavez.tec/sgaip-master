<?php
/**
 * Implementation of UsrMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_Templates extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		if(isset($this->params['selected_template'])) {
			$selected_temp = $this->params['selected_template'];
			$selected_template = $selected_temp[0]['content'];
		} else {
			$selected_template = "<p></p>";
		}
		
		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		//$this->printFolderChooserJs("form".($currEval ? $currEval->getDate() : '0'));
?>

$( "#selector" ).change(function() {
	var selected_temp = $(this).val();
	if(parseInt(selected_temp) >= 1){
		window.location.href = "/out/out.Templates.php?selected_template="+selected_temp;
	} else {
		window.location.href = "/out/out.Templates.php";
	}
});

$(document).ready( function() {

	/* DELETE TEMPLATES */
	$('body').on('click', 'a.delete-template-btn', function(ev){
		id = $(ev.currentTarget).attr('rel');
		confirmmsg = $(ev.currentTarget).attr('confirmmsg');
		msg = $(ev.currentTarget).attr('msg');
		formtoken = "<?php echo createFormKey('removetemplate'); ?>";
		bootbox.confirm({
    		message: confirmmsg,
    		buttons: {
        		confirm: {
            		label: "<i class='fa fa-times'></i> <?php echo getMLText("rm_template"); ?>",
            		className: 'btn-danger'
        		},
        		cancel: {
            		label: "<?php echo getMLText("cancel"); ?>",
            		className: 'btn-default'
        		}
    		},
	    	callback: function (result) {
	    		if (result) {
	    			$.get('/op/op.Templates.php',
					{ action: 'removetemplate', template_id: id, formtoken: formtoken })
					.done(function(data) {
						if(data.success) {							
							noty({
								text: data.message,
								type: 'success',
								dismissQueue: true,
								layout: 'topRight',
								theme: 'defaultTheme',
								timeout: 1500,
							});
							window.location.href = '/out/out.Templates.php';		
						} else {
							noty({
								text: data.message,
								type: 'error',
								dismissQueue: true,
								layout: 'topRight',
								theme: 'defaultTheme',
								timeout: 3500,
							});
						}
					});
				}
	    	}
		});
	});

	CKEDITOR.replace( 'editor', {
		toolbar: [
			{ name: 'document', items: [ 'Print','NewPage' ] },
			{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },
			{ name: 'styles', items: [ 'Styles','Format', 'Font', 'FontSize' ] },
			{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting'] },
			{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
			{ name: 'links', items: [ 'Link', 'Unlink' ] },
			{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'lineheight','-', 'Blockquote'] },
			{ name: 'insert', items: [ 'Image', 'Table','HorizontalRule','SpecialChar','PageBreak' ] },
			{ name: 'tools', items: [ 'Maximize' ] },
			{ name: 'editing', items: [ 'Find','Replace','-','SelectAll','Scayt','Source' ] }
		],

		customConfig: '',
		disallowedContent: 'img{width,height,float}',
		extraAllowedContent: 'img[width,height,align]',
		extraPlugins: 'tableresize,pagebreak,sourcearea,lineheight,pastefromword',
		height: 800,
		contentsCss: [ '/styles/multisis-lte/plugins/ckeditor-4.10/contents.css', '/styles/multisis-lte/plugins/ckeditor-4.10/mystyles.css' ],
		bodyClass: 'document-editor',
		format_tags: 'p;h1;h2;h3;pre',
		removeDialogTabs: 'image:advanced;link:advanced',
		line_height: '1;1.5;2;3;',

		filebrowserUploadMethod : 'form',
	    filebrowserBrowseUrl : '/styles/multisis-lte/plugins/kcfinder/browse.php?opener=ckeditor&type=files',
		filebrowserImageBrowseUrl : '/styles/multisis-lte/plugins/kcfinder/browse.php?opener=ckeditor&type=images',
		filebrowserFlashBrowseUrl : '/styles/multisis-lte/plugins/kcfinder/browse.php?opener=ckeditor&type=flash',
		filebrowserUploadUrl : '/styles/multisis-lte/plugins/kcfinder/upload.php?opener=ckeditor&type=files',
		filebrowserImageUploadUrl : '/styles/multisis-lte/plugins/kcfinder/upload.php?opener=ckeditor&type=images',
		filebrowserFlashUploadUrl : '/styles/multisis-lte/plugins/kcfinder/upload.php?opener=ckeditor&type=flash',
	});

	CKEDITOR.instances['editor'].setData(<?php echo "'".addslashes($selected_template)."'"; ?>);

});

$('#save-template-content').on('click', function(){
	var html = $(".cke_wysiwyg_frame").contents().find("body").html();
	var id = $(this).attr('rel');

	formtoken = '<?php echo createFormKey("edittemplatecontent"); ?>';
  	$.post('/op/op.Templates.php',
	{ action: 'edittemplatecontent', template_id: id, content: html, formtoken: formtoken })
	.always(function(data) {
		console.log(data);						
		if(data) {							
			noty({
				text: '<?php echo getMLText('data_saved'); ?>',
				type: 'success',
				dismissQueue: true,
				layout: 'topRight',
				theme: 'defaultTheme',
				timeout: 1500,
			});
			} else {
				noty({
					text: '<?php echo getMLText('splash_error'); ?>',
					type: 'error',
					dismissQueue: true,
					layout: 'topRight',
					theme: 'defaultTheme',
					timeout: 3500,
				});
			}
	});
});

<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$templates = $this->params['templates'];
		$httproot = $this->params['httproot'];

		if(isset($this->params['selected_template']))
			$selected_template = $this->params['selected_template'];
		else
			$selected_template = 0;

		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datepicker/datepicker3.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/bootstrap-datepicker.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');

		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/ckeditor-4.10/ckeditor.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/ckeditor-4.10/lang/es.js"></script>'."\n", 'js');

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("templates"));
?>

<div class="row-fluid">
<div class="col-md-6">
<div class="well row-fluid">
<form class="form-horizontal">
<div class="control-group">
<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
<div class="controls">
<select class="chzn-select" id="selector">
<option value="-1"><?php echo getMLText("template_select")?></option>
<option value="0"><?php echo getMLText("add_template")?></option>
<?php
	if ($templates && !empty($templates)) {
		foreach ($templates as $template) {
			print "<option value=\"".$template['id']."\" >" . htmlspecialchars($template['name']) . "</option>";
		}
	}
?>
</select>
</div>
</div>
</form>
</div>
</div>


<!--<div id="ajax-template" class="ajax" data-view="Templates" data-action="form" <?php //echo ($selected_template ? "data-query=\"selected_template=".$selected_template."\"" : "") ?>>
</div>-->

<div class="col-md-6">
		<div class="box box-success box-solid" id="box-form1">
		<div class="box-header with-border">
		<h3 class="box-title"><?php echo getMLText("template"); ?></h3>
		</div>
		<div class="box-body">
			<form id="form1" class="form-horizontal" action="../op/op.Templates.php" method="post">
			
			<?php if ($selected_template) {
				echo '<input type="hidden" name="action" value="edittemplatename">';
				echo '<input type="hidden" name="template_id" value="'.$selected_template[0]['id'].'">';
				echo createHiddenFieldWithKey('edittemplatename');
			} else {
				echo '<input type="hidden" name="action" value="addtemplate">';
				echo createHiddenFieldWithKey('addtemplate');
			}?>
			<div class="control-group">
			<label class="control-label" for="name"><?php printMLText("name");?>:</label>
		    <input type="text" name="name" value="<?php echo (isset($selected_template[0]['name']) ? $selected_template[0]['name'] : ''); ?>" class="form-control" required="required" /> 
		    </div>
			<br>
			<div class="controls">
			
			<?php if ($selected_template) {
				echo '<button type="submit" class="btn btn-warning"><i class="fa fa-save"></i>&nbsp;'.getMLText("update_name").'</button>&nbsp;';
				echo '<a type="button" class="btn btn-danger delete-template-btn" rel="'.$selected_template[0]['id'].'" msg="'.getMLText('rm_template').'" confirmmsg="'.htmlspecialchars(getMLText("confirm_rm_template")).'" data-toggle="tooltip" data-placement="bottom" title="'.getMLText("rm_template").'"><i class="fa fa-times"></i> '.getMLText('rm_template').'</a>';
			} else {
				echo '<button type="submit" class="btn btn-info"><i class="fa fa-save"></i>'.getMLText("save").'</button>';
			} 

			?>
			</div>
			</form>
		</div>
		</div>
</div>

<div class="col-md-12">
	<?php $this->startBoxPrimary(getMLText("template_editor")); ?>
	<textarea id="editor"></textarea>
	<br>	
	<div class="col-md-12">
	<?php if ($selected_template): ?>
		<button type="button" class="btn btn-primary pull-right" rel="<?php echo $selected_template[0]['id']; ?>" id="save-template-content"><i class="fa fa-save"></i> <?php printMLText("save_changes"); ?></button>	
	<?php endif ?>
	</div>
	<?php $this->endsBoxSolidPrimary(); ?>
</div>
</div>


<?php

	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
