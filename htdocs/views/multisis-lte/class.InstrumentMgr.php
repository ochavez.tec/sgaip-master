<?php
/**
 * Implementation of UsrMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_InstrumentMgr extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$currInstrument = $this->params['selinstrument'];
		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		$this->printFolderChooserJs("form".($currInstrument ? $currInstrument->getId() : '0'));
?>
function folderSelected(id, name) {
	window.location = '../out/out.ViewFolder.php?folderid=' + id;
}

function checkForm()
{
	msg = new Array();

	if($("#name").val() == "") msg.push("<?php printMLText("js_no_name");?>");
	if($("#weight").val() == "") msg.push("<?php printMLText("js_no_weight");?>");
	if($("#score").val() == "") msg.push("<?php printMLText("js_no_score");?>");	
<?php
	if ($strictformcheck) {
?>
	if($("#comment").val() == "") msg.push("<?php printMLText("js_no_comment");?>");
<?php
	}
?>
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$(document).ready( function() {
	$('body').on('submit', '#form', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});
	$( "#selector" ).change(function() {
		$('div.ajax').trigger('update', {instrumentid: $(this).val()});
	});
});
<?php
	} /* }}} */

	function info() { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
		if(isset($this->params['selinstrument']))
			$selinstrument = $this->params['selinstrument'];

		if(isset($selinstrument)) {
			$sessionmgr = new SeedDMS_SessionMgr($dms->getDB());

 			$this->startBoxCollapsableInfo(getMLText("instrument_info"));
// 			echo "<table class=\"table table-striped table-bordered\">\n";
// 			echo "<tr><td>".getMLText('discspace')."</td><td>";
// 			$qt = $selinstrument->getQuota() ? $selinstrument->getQuota() : $quota;
// 			echo SeedDMS_Core_File::format_filesize($selinstrument->getUsedDiskSpace())." / ".SeedDMS_Core_File::format_filesize($qt)."<br />";
// 			echo $this->getProgressBar($selinstrument->getUsedDiskSpace(), $qt);
// 			echo "</td></tr>\n";
// 			$documents = $selinstrument->getDocuments();
// 			echo "<tr><td>".getMLText('documents')."</td><td>".count($documents)."</td></tr>\n";
// 			$documents = $selinstrument->getDocumentsLocked();
// 			echo "<tr><td>".getMLText('documents_locked')."</td><td>".count($documents)."</td></tr>\n";
// 			if($workflowmode == "traditional") {
// 				$reviewStatus = $selinstrument->getReviewStatus();
// 				if($reviewStatus['indstatus']) {
// 					$i = 0;
// 					foreach($reviewStatus['indstatus'] as $rv) {
// 						if($rv['status'] == 0) {
// 							$i++;
// 						}
// 					}
// 					echo "<tr><td>".getMLText('pending_reviews')."</td><td>".$i."</td></tr>\n";
// 				}
// 			}
// 			if($workflowmode == "traditional" || $workflowmode == 'traditional_only_approval') {
// 				$approvalStatus = $selinstrument->getApprovalStatus();
// 				if($approvalStatus['indstatus']) {
// 					$i = 0;
// 					foreach($approvalStatus['indstatus'] as $rv) {
// 						if($rv['status'] == 0) {
// 							$i++;
// 						}
// 					}
// 					echo "<tr><td>".getMLText('pending_approvals')."</td><td>".$i."</td></tr>\n";
// 				}
// 			}
// 			if($workflowmode == 'advanced') {
// 				$workflowStatus = $selinstrument->getWorkflowStatus();
// 				if($workflowStatus['u'])
// 					echo "<tr><td>".getMLText('pending_workflows')."</td><td>".count($workflowStatus['u'])."</td></tr>\n";
// 			}
// 			$sessions = $sessionmgr->getUserSessions($seluser);
// 			if($sessions) {
// 				$session = array_shift($sessions);
// 				echo "<tr><td>".getMLText('lastaccess')."</td><td>".getLongReadableDate($session->getLastAccess())."</td></tr>\n";
// 			}
// 			echo "</table>";
// 			echo "<br>";


// 			if($user->isAdmin() && $selinstrument->getID() != $instrument->getID())
// 					echo "<a class=\"btn btn-success\" href=\"../op/op.SubstituteUser.php?userid=".((int) $seluser->getID())."&formtoken=".createFormKey('substituteuser')."\"><i class=\"fa fa-exchange\"></i> ".getMLText('substitute_user')."</a> ";
			$this->endsBoxCollapsableInfo();
		}
	} /* }}} */

	function form() { /* {{{ */
		$selinstrument = $this->params['selinstrument'];

		$this->showInstrumentForm($selinstrument);
	} /* }}} */

	function showInstrumentForm($currInstrument) { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
		$instruments = $this->params['allinstruments'];
		$axes = $this->params['allaxes'];
		$httproot = $this->params['httproot'];
?>
	<form action="../op/op.InstrumentMgr.php" method="post" enctype="multipart/form-data" name="<?php echo "form".($currInstrument ? $currInstrument->getId() : '0'); ?>" id="<?php echo "form".($currInstrument ? $currInstrument->getId() : '0'); ?>">
<?php
		if($currInstrument) {
			echo createHiddenFieldWithKey('editinstrument');
?>
	<input type="hidden" name="instrumentid" id="instrumentid" value="<?php print $currInstrument->getID();?>">
	<input type="hidden" name="action" value="editinstrument">
<?php
		} else {
			echo createHiddenFieldWithKey('addinstrument');
?>
	<input type="hidden" id="instrumentid" value="0">
	<input type="hidden" name="action" value="addinstrument">
<?php
		}
?>
<div class="table-responsive">
	<table class="table-condensed">
<?php
	if(isset($currInstrument)) {
?>
		<tr>
			<td></td>
			<td><a class="btn btn-danger" href="../out/out.RemoveInstrument.php?instrumentid=<?php print $currInstrument->getID();?>"><i class="fa fa-times"></i> <?php printMLText("rm_instrument");?></a></td>
		</tr>
<?php
	}
?>
		<tr>
			<td><?php printMLText("instrument_name");?>:</td>
			<td><input type="text" class="form-control" name="name" id="name" value="<?php print $currInstrument ? htmlspecialchars($currInstrument->getName()) : "";?>"></td>
		</tr>
		<tr>
			<td><?php printMLText("weight");?>:</td>
			<td><input type="text" class="form-control" name="weight" id="weight" value="<?php print $currInstrument ? htmlspecialchars($currInstrument->getWeight()) : "";?>"></td>
		</tr>
		<input type="hidden" name="score" id="score" value="0" />
		<tr>
			<td><?php printMLText("comment");?>:</td>
			<td><textarea name="comment" class="form-control" id="comment" rows="4" cols="50"><?php print $currInstrument ? htmlspecialchars($currInstrument->getComment()) : "";?></textarea></td>
		</tr>
		<tr>
			<td><?php printMLText("transparency_axe");?>:</td>
			<td><select class="chzn-select form-control" name="axes[]" data-placeholder="<?php printMLText('select_axes'); ?>">
<?php
		foreach($axes as $axe) {
			echo '<option value="'.$axe->getID().'"'.($currInstrument && $axe->isMember($currInstrument) ? ' selected' : '').'>'.$axe->getName().'</option>';
		}
?>
			</select></td>
		</tr>
		<tr>
			<td></td>
			<td><button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText($currInstrument ? "save" : "add_instrument")?></button></td>
		</tr>
	</table>
</div>
	</form>
	
<?php
	
		if($currInstrument) {
			$this->contentSubHeading(getMLText("intrument_vars"));
?>
		<div class="table-responsive">
		<table class="table table-striped table-bordered">
<?php
			$intrumentVars = $dms->getInstrumentVarsByInstrumentID($currInstrument->getID());
			if (count($intrumentVars) == 0)
				print "<tr><td>".getMLText("no_var_associated")."</td></tr>";
			else {
			
				foreach ($intrumentVars as $member) {
				
					print "<tr>";
					print "<td><i class=\"fa fa-gear\"></i></td>";
					print "<td>" . htmlspecialchars($member->getCode()." - ".$member->getName()) . "</td>";
// 					print "<td>" . ($axe->isMember($member,true)?getMLText("manager"):"&nbsp;") . "</td>";
					print "<td>";
					print "<form action=\"../out/out.InstrumentVarMgr.php\" method=\"get\" class=\"form-inline\" style=\"display: inline-block; margin-bottom: 0px;\"><input type=\"hidden\" name=\"instrumentvarid\" value=\"".$member->getID()."\" /><button type=\"submit\" class=\"btn btn-mini btn-info\"><i class=\"fa fa-edit\"></i> ".getMLText("edit")."</button></form>";
					print "&nbsp;";
// 					print "<form action=\"../op/op.AxeMgr.php\" method=\"post\" class=\"form-inline\" style=\"display: inline-block; margin-bottom: 0px;\"><input type=\"hidden\" name=\"axeid\" value=\"".$axe->getID()."\" /><input type=\"hidden\" name=\"action\" value=\"tmanager\" /><input type=\"hidden\" name=\"instrumentid\" value=\"".$member->getID()."\" />".createHiddenFieldWithKey('tmanager')."<button type=\"submit\" class=\"btn btn-mini btn-primary\"><i class=\"fa fa-exchange\"></i> ".getMLText("toggle_manager")."</button></form>";
					print "</td></tr>";
				}
			}
?>
		</table>
		</div>
<?php
		}
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
		$selinstrument = $this->params['selinstrument'];
		$instruments = $this->params['allinstruments'];
		$axes = $this->params['allaxes'];
		$httproot = $this->params['httproot'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
    <div class="gap-10"></div>
    <div class="row">
    <div class="col-md-12">
    <?php 

		$this->startBoxPrimary(getMLText("instrument_management"));
?>

<?php //if ($user->_comment != "client-admin") { ?>
<div class="row-fluid">
<div class="span12">
<div class="well">
<form class="form-horizontal">
	<div class="control-group">
		<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
		<div class="controls">
<select class="chzn-select" id="selector">
<option value="-1"><?php echo getMLText("choose_instrument")?></option>
<option value="0"><?php echo getMLText("add_instrument")?></option>
<?php
		foreach ($instruments as $currInstrument) {
			print "<option value=\"".$currInstrument->getID()."\" ".($selinstrument && $currInstrument->getID()==$selinstrument->getID() ? 'selected' : '')." data-subtitle=\"".getMLText("instrument_weight").": ".htmlspecialchars($currInstrument->getWeight())."\">" . htmlspecialchars($currInstrument->getName()) . "</option>";
		}
?>
</select>
		</div>
	</div>
</form>
</div>
	<div class="ajax" data-view="InstrumentMgr" data-action="info" <?php echo ($selinstrument ? "data-query=\"instrumentid=".$selinstrument->getID()."\"" : "") ?>></div>
</div>

<div class="span12">
	<div class="well">
		<div class="ajax" data-view="InstrumentMgr" data-action="form" <?php echo ($selinstrument ? "data-query=\"instrumentid=".$selinstrument->getID()."\"" : "") ?>></div>
	</div>
</div>
</div>

<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
