<?php
/**
 * Implementation of AxeMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Include class to preview documents
 */
require_once("SeedDMS/Preview.php");

/**
 * Class which outputs the html page for AxeMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_AxeMgr extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$selaxe = $this->params['selaxe'];
		$strictformcheck = $this->params['strictformcheck'];

		header("Content-type: text/javascript");
?>
function checkForm1() {
	msg = new Array();
	
	if($("#name").val() == "") msg.push("<?php printMLText("js_no_name");?>");
<?php
	if ($strictformcheck) {
?>
	if($("#comment").val() == "") msg.push("<?php printMLText("js_no_comment");?>");
<?php
	}
?>
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	} else
		return true;
}

function checkForm2() {
	msg = "";
	
		if($("#instrumentid").val() == -1) msg += "<?php printMLText("js_select_instrument");?>\n";

		if (msg != "") {
			noty({
				text: msg,
				type: 'error',
				dismissQueue: true,
				layout: 'topRight',
				theme: 'defaultTheme',
				_timeout: 1500,
			});
			return false;
		} else
			return true;
	}

$(document).ready( function() {
	$('body').on('submit', '#form_1', function(ev){
		if(checkForm1())
			return;
		ev.preventDefault();
	});

	$('body').on('submit', '#form_2', function(ev){
		if(checkForm2())
			return;
		ev.preventDefault();
	});

	$( "#selector" ).change(function() {
		$('div.ajax').trigger('update', {axeid: $(this).val()});
	});
});
<?php
	} /* }}} */

	function info() { /* {{{ */
		$dms = $this->params['dms'];
		$selaxe = $this->params['selaxe'];
		$cachedir = $this->params['cachedir'];
		$previewwidth = $this->params['previewWidthList'];
		$workflowmode = $this->params['workflowmode'];
		$timeout = $this->params['timeout'];

		if($selaxe) {
			$previewer = new SeedDMS_Preview_Previewer($cachedir, $previewwidth, $timeout);
			$this->startBoxCollapsableInfo(getMLText("axe_info"));
			echo "<table class=\"table table-bordered table-striped\">\n";
/*
			if($workflowmode == "traditional") {
				$reviewstatus = $selaxe->getReviewStatus();
				$i = 0;
				foreach($reviewstatus as $rv) {
					if($rv['status'] == 0) {
						$i++;
					}
				}
			}
			if($workflowmode == "traditional" || $workflowmode == 'traditional_only_approval') {
				echo "<tr><td>".getMLText('pending_reviews')."</td><td>".$i."</td></tr>";
				$approvalstatus = $selaxe->getApprovalStatus();
				$i = 0;
				foreach($approvalstatus as $rv) {
					if($rv['status'] == 0) {
						$i++;
					}
				}
				echo "<tr><td>".getMLText('pending_approvals')."</td><td>".$i."</td></tr>";
			}
			if($workflowmode == 'advanced') {
				$workflowStatus = $selaxe->getWorkflowStatus();
				if($workflowStatus)
					echo "<tr><td>".getMLText('pending_workflows')."</td><td>".count($workflowStatus)."</td></tr>\n";
			}
*/
			echo "</table>";
			$this->endsBoxCollapsableInfo();
		}
	} /* }}} */

	function showAxeForm($axe) { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
		$allInstruments = $this->params['allinstruments'];
		$axes = $this->params['allaxes'];
?>
	<form class="form-horizontal" action="../op/op.AxeMgr.php" name="form_1" id="form_1" method="post">
<?php
		if($axe) {
			echo createHiddenFieldWithKey('editaxe');
?>
	<input type="hidden" name="axeid" value="<?php print $axe->getID();?>">
	<input type="hidden" name="action" value="editaxe">
<?php
		} else {
			echo createHiddenFieldWithKey('addaxe');
?>
	<input type="hidden" name="action" value="addaxe">
<?php
		}
?>

<?php
		if($axe) {
?>
		<div class="control-group">
			<div class="controls">
				<a href="../out/out.RemoveAxe.php?axeid=<?php print $axe->getID();?>" class="btn btn-danger"><i class="fa fa-times"></i> <?php printMLText("rm_axe");?></a>
			</div>
		</div>
<?php
		}
?>
		<div class="control-group">
			<label class="control-label"><?php printMLText("name");?>:</label>
			<div class="controls">
				<input type="text" class="form-control" name="name" id="name" value="<?php print $axe ? htmlspecialchars($axe->getName()) : '';?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php printMLText("points");?>:</label>
			<div class="controls">
				<input type="text" class="form-control" name="maxScore" id="maxScore" value="<?php print $axe ? htmlspecialchars($axe->getMaxScore()) : '';?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php printMLText("comment");?>:</label>
			<div class="controls">
				<textarea name="comment" class="form-control" id="comment" rows="4" cols="50"><?php print $axe ? htmlspecialchars($axe->getComment()) : '';?></textarea>
			</div>
		</div>
		<div class="control-group">
			<br>
			<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save")?></button>
		</div>

	</form>
<?php
		if($axe) {
			$this->contentSubHeading(getMLText("axe_members"));
?>
		<div class="table-responsive">
		<table class="table table-striped table-bordered">
<?php
			$members = $axe->getInstruments();
			if (count($members) == 0)
				print "<tr><td>".getMLText("no_axe_members")."</td></tr>";
			else {
			
				foreach ($members as $member) {
				
					print "<tr>";
					print "<td><i class=\"fa fa-gear\"></i></td>";
					print "<td>" . htmlspecialchars($member->getName()) . "</td>";
// 					print "<td>" . ($axe->isMember($member,true)?getMLText("manager"):"&nbsp;") . "</td>";
					print "<td>";
					print "<form action=\"../out/out.InstrumentMgr.php\" method=\"get\" class=\"form-inline\" style=\"display: inline-block; margin-bottom: 0px;\"><input type=\"hidden\" name=\"instrumentid\" value=\"".$member->getID()."\" /><button type=\"submit\" class=\"btn btn-mini btn-info\"><i class=\"fa fa-edit\"></i> ".getMLText("edit")."</button></form>&nbsp;";
					print "<form action=\"../op/op.AxeMgr.php\" method=\"post\" class=\"form-inline\" style=\"display: inline-block; margin-bottom: 0px;\"><input type=\"hidden\" name=\"action\" value=\"rmmember\" /><input type=\"hidden\" name=\"axeid\" value=\"".$axe->getID()."\" /><input type=\"hidden\" name=\"instrumentid\" value=\"".$member->getID()."\" />".createHiddenFieldWithKey('rmmember')."<button type=\"submit\" class=\"btn btn-mini btn-danger\"><i class=\"fa fa-times\"></i> ".getMLText("delete")."</button></form>";
					print "&nbsp;";
// 					print "<form action=\"../op/op.AxeMgr.php\" method=\"post\" class=\"form-inline\" style=\"display: inline-block; margin-bottom: 0px;\"><input type=\"hidden\" name=\"axeid\" value=\"".$axe->getID()."\" /><input type=\"hidden\" name=\"action\" value=\"tmanager\" /><input type=\"hidden\" name=\"instrumentid\" value=\"".$member->getID()."\" />".createHiddenFieldWithKey('tmanager')."<button type=\"submit\" class=\"btn btn-mini btn-primary\"><i class=\"fa fa-exchange\"></i> ".getMLText("toggle_manager")."</button></form>";
					print "</td></tr>";
				}
			}
?>
		</table>
		</div>
		
<?php
//			$this->contentSubHeading(getMLText("add_member"));
?>
		<div class="table-responsive">
		<form class="form-inline" action="../op/op.AxeMgr.php" method="POST" name="form_2" id="form_2">
		<?php echo createHiddenFieldWithKey('addmember'); ?>
		<input type="Hidden" name="action" value="addmember">
		<input type="Hidden" name="axeid" value="<?php print $axe->getID();?>">
		
		<table class="table-condensed">
			<tr>
				<td>
<!--
					<select name="instrumentid" class="form-control" id="instrumentid">
						<option value="-1"><?php printMLText("select_one");?></option>
						<?php
							foreach ($allInstruments as $currInstrument)
								if (!$axe->isMember($currInstrument))
									print "<option value=\"".$currInstrument->getID()."\">" . htmlspecialchars($currInstrument->getLogin()." - ".$currInstrument->getName()) . "</option>\n";
						?>
					</select>
-->
				</td>
<!--
				<td>
					<label class="checkbox"><input type="checkbox" name="manager" value="1"> <?php printMLText("manager");?></label>
				</td>
-->
				<td>
<!-- 					<button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> <?php printMLText("add");?></button> -->
				</td>
			</tr>
		</table>
		
		</form>
		</div>
<?php
		}
	} /* }}} */

	function form() { /* {{{ */
		$selaxe = $this->params['selaxe'];

		$this->showAxeForm($selaxe);
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
		$selaxe = $this->params['selaxe'];
		$allInstruments = $this->params['allinstruments'];
		$allAxes = $this->params['allaxes'];
		$strictformcheck = $this->params['strictformcheck'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
    <div class="gap-10"></div>
    <div class="row">
    <div class="col-md-12">
<?php 
	$this->startBoxPrimary(getMLText("axe_management"));
?>

<div class="well">
<form class="form-horizontal">
	<div class="control-group">
		<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
		<div class="controls">
<select class="chzn-select form-control" id="selector">
<option value="-1"><?php echo getMLText("choose_axe")?></option>
<option value="0"><?php echo getMLText("add_axe")?></option>
<?php
		foreach ($allAxes as $axe) {
			print "<option value=\"".$axe->getID()."\" ".($selaxe && $axe->getID()==$selaxe->getID() ? 'selected' : '').">" . htmlspecialchars($axe->getName()) . "</option>";
		}
?>
</select>
		</div>
	</div>
</form>
</div>
<div class="ajax" data-view="AxeMgr" data-action="info" <?php echo ($selaxe ? "data-query=\"axeid=".$selaxe->getID()."\"" : "") ?>></div>


<div class="span12">
	<div class="well">
		<div class="ajax" data-view="AxeMgr" data-action="form" <?php echo ($selaxe ? "data-query=\"axeid=".$selaxe->getID()."\"" : "") ?>></div>
	</div>
</div>


<?php
		$this->endsBoxPrimary();

		echo "</div>";
		echo "</div>";
		echo "</div>";
		
    $this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>
