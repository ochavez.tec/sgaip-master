<?php
/**
 * Implementation of EditComment view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditComment view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_RunReport extends SeedDMS_Bootstrap_Style {
	
	var $count = 0;
	var $perpage = 10;
	var $page = 0;

	function js() { /* {{{ */
		$strictformcheck = $this->params['strictformcheck'];
		header('Content-Type: application/javascript; charset=UTF-8');
?>
function checkForm()
{
	msg = new Array();
<?php

		if ($strictformcheck) {
?>
	if (document.form1.query.value == "") msg.push("<?php printMLText("js_no_query");?>");
<?php
		}
?>
	if (msg != "")
	{
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else return true;
}
$(document).ready(function() {
	$('body').on('submit', '#form1', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});

  $(function () {
    $('#results').DataTable({
      dom: 'Bfrtip',
	  buttons: [
		{
		    extend:    'print',
		    text:      '<i class="fa fa-print"></i>',
		    titleAttr: 'Imprimir'
		},
        {
            extend:    'csvHtml5',
            text:      '<i class="fa fa-file-text-o"></i>',
            titleAttr: 'CSV'
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fa fa-file-excel-o"></i>',
            titleAttr: 'Excel'
        },
      ],
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      'scrollX'		: true,
      'language': {
            'lengthMenu': 'Mostrar _MENU_ registros por página',
            'zeroRecords': 'Ningún registro ha sido encontrado',
            'info': 'Mostrando página _PAGE_ de _PAGES_',
            'infoEmpty': 'Ningún registro disponible',
            'infoFiltered': '(filtrado de un total de _MAX_ registros)',
            'search': 'Buscar',
            'paginate': {
              'first':'Primero',
              'last':'Último',
              'next':'Siguiente',
              'previous':'Anterior'
            },
      },
      
    });
  });
});
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$report = $this->params['report'];
		$reportid = $this->params['reportid'];
		$strictformcheck = $this->params['strictformcheck'];
		
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/extensions/pdfmake/pdfmake.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/extensions/pdfmake/vfs_fonts.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/extensions/buttons/js/dataTables.buttons.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/extensions/buttons/js/buttons.print.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/extensions/buttons/js/buttons.html5.min.js"></script>'."\n", 'js');

		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');

		$this->htmlStartPage(getMLText("report_title", array("reportname" => htmlspecialchars($report->getName()))), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();		

		echo "&nbsp;"; //$this->getDefaultFolderPathHTML($folder, true, $document);

		//// Document content ////
		echo "<div class=\"row\">";
		echo "<div class=\"col-md-12\">";

		$this->startBoxSuccess(getMLText("run_report", array("reportname" => htmlspecialchars($report->getName()))));
		
		$query = $report->getQuery();
		
		$db = $dms->getDB();

		$resArr = $db->getResultArray($query);

		if (is_bool($resArr) && $resArr == false)
			return false;
		
		$this->count = count($resArr);
		
		$this->top();
		foreach($resArr as $idx => $row) {
//			echo "\nROW: $idx\n";
			if($idx == 0) {
				$this->head($row);
			}
			
			$this->row($row);
/*
			foreach($row as $k => $v){
				echo "key: $k => val: $v\n";
			}
*/
		}
		$this->bottom();
		$this->endsBoxSuccess();
		echo "</div>";
		echo "</div>"; 
		echo "</div>"; // Ends row
		$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */ 
	
	function top(){
		echo "
<div class='box'>
  <div class='box-header'>
    <h3 class='box-title'>Resultados</h3>
    <div class='box-tools pull-right'>
        <a href='/out/out.ReportMgr.php' class='btn btn-info'><i class='fa fa-chevron-left'></i> Volver</a>
    </div>
  </div>
  <!-- /.box-header -->
  <div class='box-body'>
    <div id='results_wrapper' class='dataTables_wrapper form-inline dt-bootstrap'>
      <div class='row'>
        <div class='col-sm-6'>
          <div class='dataTables_length' id='results_length'>
          </div>
        </div>
        <div class='col-sm-6'>
          <div id='results_filter' class='dataTables_filter pull-right'>
          </div>
        </div>
      </div>
      <div class='row'>
        <div class='col-sm-12 table-responsive'>
          <table id='results' class='table table-bordered table-striped dataTable' role='grid' aria-describedby='results_info'>
		";
	}
	
	function head($row) {
		echo "
            <thead>
              <tr role='row'>";
		foreach($row as $k => $v){
//			echo "key: $k => val: $v\n";
			echo "
                <th class='sorting_asc' tabindex='0' aria-controls='results' rowspan='1' colspan='1' aria-sort='ascending' aria-label='".ucfirst($k).": activate to sort column descending' style='width: 201px;'>".ucfirst($k)."</th>
             ";
		}
        echo "
              </tr>
            </thead>
            <tbody>
		";
		
	}
	
	function row($row) {
		echo "<tr role='row' class='odd'>";
        foreach($row as $k => $v) {
            echo "<td class='sorting_1'>$v</td>";
        }
        echo "</tr>";
	}
	
	function bottom() {
        $pages = ceil($this->count / $this->perpage);
		echo "
            </tbody>
          </table>
        </div>
      </div>
      <div class='row'>
        <div class='col-sm-5'>
          <div class='dataTables_info' id='results_info' role='status' aria-live='polite'>
          </div>
        </div>
        <div class='col-sm-7'>
          <div class='dataTables_paginate paging_simple_numbers pull-right' id='results_paginate'>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>
		";
	}
}
?>
