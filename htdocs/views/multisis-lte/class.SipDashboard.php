<?php
/**
 * Implementation of DepMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Include class to preview documents
 */
require_once("SeedDMS/Preview.php");

/**
 * Class which outputs the html page for DepMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_SipDashboard extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
    $strictformcheck = $this->params['strictformcheck'];
    $the_year = $this->params['year'];

    $all_documents_sip_created = $this->params['all_documents_sip_created'];
		$all_documents_cc_created = $this->params['all_documents_cc_created'];

    if ($the_year != NULL) {
      $year = $the_year;
    } else {
      $year = date("Y");
    }

		header("Content-type: text/javascript");
?>

$(document).ready( function() {
  

  $("<div id='tooltip'></div>").css({
    position: "absolute",
    display: "none",
    padding: "5px",
    color: "white",
    "background-color": "#000",
    "border-radius": "5px",
    opacity: 0.80
  }).appendTo("body");

	var data_sip = [
    <?php foreach ($all_documents_sip_created as $key => $value) { ?>
      ["<?php echo $key ?>", <?php echo $value ?>],  
    <?php } ?>
  ];

  var data_cc = [
    <?php foreach ($all_documents_cc_created as $key => $value) { ?>
      ["<?php echo $key ?>", <?php echo $value ?>],  
    <?php } ?>
  ];

  var dataset_one = [{
        label: "Solicitudes",
        data: data_sip,
        color: "#9C27B0"
  }];

  var dataset_two = [{
        label: "Consultas",
        data: data_cc,
        color: "#5482FF"
  }];

  var options = {
    xaxis: {
        mode: "categories",
        tickLength: 0,
      },
      series: {
        bars: {
          show: true,
          align: "center",
          barWidth: 0.8,
        },
      },
      grid: {
        hoverable: true,
        clickable: true,
        borderWidth: 1,
        backgroundColor: { colors: ["#f2f2f2", "#d4d4d4"] }
      }
  };

  $.plot($("#chart_one"), dataset_one, options);

  $.plot($("#chart_two"), dataset_two, options);    

  $("#chart_one").bind("plothover", function (event, pos, item) {
      if(item) {
        var x = item.datapoint[0];
            y = item.datapoint[1];
        $("#tooltip").html(item.series.xaxis.ticks[x].label + ": " + y)
          .css({top: pos.pageY-35, left: pos.pageX+5})
          .fadeIn(200);
      } else {
        $("#tooltip").hide();
      }
  });

  $("#chart_two").bind("plothover", function (event, pos, item) {
      if(item) {
        var x = item.datapoint[0];
            y = item.datapoint[1];
        $("#tooltip").html(item.series.xaxis.ticks[x].label + ": " + y)
          .css({top: pos.pageY-35, left: pos.pageX+5})
          .fadeIn(200);
      } else {
        $("#tooltip").hide();
      }
  });

  $('.date').datepicker({
    language: 'es',
    format: 'yyyy',
    autoclose: true,
    minViewMode: 2,
  });

  /* DATA TABLES */
  $(".documents-table").DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": true,
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ningún registro ha sido encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar",
            "paginate": {
              "first":"Primero",
              "last":"Último",
              "next":"Siguiente",
              "previous":"Anterior"
            },
      }
  });
  
  $("#dt-user-docs").DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": true,
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ningún registro ha sido encontrado",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de un total de _MAX_ registros)",
      "search": "Buscar",
      "paginate": {
        "first":"Primero",
        "last":"Último",
        "next":"Siguiente",
        "previous":"Anterior"
      },
    }
  });

  $('.view-user-docs').on('click', function(e){
    e.preventDefault();
    var user_id = $(this).attr('rel');
    var data_action = $(this).attr("data-action");

    formtoken = '<?php echo createFormKey("getuserdocuments"); ?>';
    $.post('/op/op.SipDashboard.php', { action: data_action, user_id: user_id, formtoken: formtoken })
      .always(function(data) {          
        if(data.success) {             
          $("#tbody-user-docs").empty();

          var table = $('#dt-user-docs').DataTable();
          table.clear().draw();
          table.rows.add($(data.html)).draw();
          $('#userDocsModal').modal('show');

        } else {
          console.log( "Error ocurred" );
        }
      });
  });

});

<?php
} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$strictformcheck = $this->params['strictformcheck'];
    $the_year = $this->params['year'];

    // counters
    $green_sip = $this->params['green_sip'];
    $yellow_sip = $this->params['yellow_sip'];
    $red_sip = $this->params['red_sip'];

    $green_cc = $this->params['green_cc'];
    $yellow_cc = $this->params['yellow_cc'];
    $red_cc = $this->params['red_cc'];

    $green_memo = $this->params['green_memo'];
    $yellow_memo = $this->params['yellow_memo'];
    $red_memo = $this->params['red_memo'];

    // document arrays
    $documents_green_sip = $this->params['documents_green_sip'];
    $documents_yellow_sip = $this->params['documents_yellow_sip'];
    $documents_red_sip = $this->params['documents_red_sip'];

    $documents_green_cc = $this->params['documents_green_cc'];
    $documents_yellow_cc = $this->params['documents_yellow_cc'];
    $documents_red_cc = $this->params['documents_red_cc'];


    // document arrays
    $documents_green_memo = $this->params['documents_green_memo'];
    $documents_yellow_memo = $this->params['documents_yellow_memo'];
    $documents_red_memo = $this->params['documents_red_memo'];
    
    // USERS AND DOCUMENTS
    $global_user_documents_sip = $this->params['global_user_documents_sip'];
    $global_user_documents_cc = $this->params['global_user_documents_cc'];
    $global_group_documents = $this->params['global_group_documents'];

    // ALL STATUS
    $sip_all_status = $this->params['sip_all_status'];
    $cc_all_status = $this->params['cc_all_status'];
    $memo_all_status = $this->params['memo_all_status'];


    if ($the_year != NULL) {
      $year = $the_year;
    } else {
      $year = date("Y");
    }

    $this->htmlAddHeader(
      '<script type="text/javascript" src="../styles/'.$this->theme.'/flot/jquery.flot.min.js"></script>'."\n".
      '<script type="text/javascript" src="../styles/'.$this->theme.'/flot/jquery.flot.categories.min.js"></script>'."\n".
      '<script type="text/javascript" src="../styles/'.$this->theme.'/flot/jquery.flot.time.min.js"></script>'."\n");

    $this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
    $this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
   $this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		
    $this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datepicker/datepicker3.css" rel="stylesheet">'."\n", 'css');
    $this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/bootstrap-datepicker.js"></script>'."\n", 'js');
    $this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>'."\n", 'js');


    $this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");

    ?>

    <style type="text/css">
      .circle{
        height:90px;
        width:90px;
        -webkit-border-radius: 45px;
        -moz-border-radius: 45px;
        border-radius: 45px;
      }
      .gray-background {
        background-color: #f2f2f2;
      }
    </style> 

    <?php
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		echo '<div class="gap-10"></div>';
?>


<!-- ////////////////////// MODALS ////////////////////////// -->
<div class="row">
<div class="col-md-12">

<!-- INDIVIDUAL USER DOCUMENTS -->
<div class="modal modal-default fade" id="userDocsModal">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">Documentos</h4>
  </div>
  <div class="modal-body">    
    <table id="dt-user-docs" class="table table-striped" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody id="tbody-user-docs">    
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- SIP MODAL 1 -->
<div class="modal modal-success fade" id="sip-modal-success">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("sip_on_green_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Usuario asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_green_sip)): ?>
      <?php foreach ($documents_green_sip as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_user = $dms->getUser($doc_data['user_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_user->getFullName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- SIP MODAL 2 -->
<div class="modal modal-warning fade" id="sip-modal-warning">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("sip_on_yellow_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Usuario asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_yellow_sip)): ?>
      <?php foreach ($documents_yellow_sip as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_user = $dms->getUser($doc_data['user_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_user->getFullName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- SIP MODAL 3 -->
<div class="modal modal-danger fade" id="sip-modal-danger">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("sip_on_red_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Usuario asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_red_sip)): ?>
      <?php foreach ($documents_red_sip as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_user = $dms->getUser($doc_data['user_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_user->getFullName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- CC MODAL 1 -->
<div class="modal modal-success fade" id="cc-modal-success">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("cc_on_green_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Usuario asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_green_cc)): ?>
      <?php foreach ($documents_green_cc as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_user = $dms->getUser($doc_data['user_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_user->getFullName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- CC MODAL 2 -->
<div class="modal modal-warning fade" id="cc-modal-warning">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("cc_on_yellow_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Usuario asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_yellow_cc)): ?>
      <?php foreach ($documents_yellow_cc as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_user = $dms->getUser($doc_data['user_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_user->getFullName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- CC MODAL 3 -->
<div class="modal modal-danger fade" id="cc-modal-danger">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("cc_on_red_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Usuario asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_red_cc)): ?>
      <?php foreach ($documents_red_cc as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_user = $dms->getUser($doc_data['user_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_user->getFullName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- MEMO MODAL 1 -->
<div class="modal modal-success fade" id="memo-modal-success">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("memo_on_green_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Grupo asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_green_memo)): ?>
      <?php foreach ($documents_green_memo as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_group = $dms->getGroup($doc_data['group_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_group->getName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- MEMO MODAL 2 -->
<div class="modal modal-warning fade" id="memo-modal-warning">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("memo_on_yellow_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Grupo asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_yellow_memo)): ?>
      <?php foreach ($documents_yellow_memo as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_group = $dms->getGroup($doc_data['group_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_group->getName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>

<!-- MEMO MODAL 3 -->
<div class="modal modal-danger fade" id="memo-modal-danger">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php printMLText("memo_on_red_light"); ?></h4>
  </div>
  <div class="modal-body">    
    <table class="documents-table table" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de inicio</th>
    <th class="align-center">Fecha de vencimiento</th>
    <th class="align-center">Grupo asignado</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($documents_red_memo)): ?>
      <?php foreach ($documents_red_memo as $doc_data): ?>        
        <?php $document = $dms->getDocument($doc_data['document_id']); ?>
        <?php $assigned_group = $dms->getGroup($doc_data['group_id']); ?>
        <tr>
          <td class="align-center"><?php echo $document->getName(); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['start_date']); ?></td>
          <td class="align-center"><?php echo date("d-m-Y", (int)$doc_data['end_date']); ?></td>
          <td class="align-center"><?php echo $assigned_group->getName(); ?></td>
          <td class="align-center">
            <a href="<?php echo "/out/out.ViewDocument.php?documentid=".$doc_data['document_id']; ?>" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php printMLText("close"); ?></button>
  </div>
  </div>
  </div>
</div>


</div>
</div>


<!-- /////////////////////////////////////////////////// MODALS ENDS //////////////////////////////////////////////////////////// -->

<div class="row">
<div class="col-md-8">
<div class="well">
<form class="horizontal" method="post" action="../op/op.SipDashboard.php">

<table class="table table-condensed">
  <tr>
    <td>
      <label class="control-label" for="login"><?php printMLText("choose_year");?>:</label>
    </td>
    <td>
      <div class="input-group">
      <div class="input-group-addon">
      <i class="fa fa-calendar"></i>
      </div>
      <input type="text" id="period" name="year" value="<?php echo ($year) ? $year : ""; ?>" class="form-control date" required="required" autocomplete="off" placeholder="yyyy" readonly="true" /> 
      </div>
    </td>
    <td>
      <button class="btn btn-primary" type="submit"><i class="fa fa-refresh"></i> <?php printMLText("apply"); ?></button>
    </td>
  </tr>  
</table>
</form> 
</div>
</div>
<div class="col-md-4">
</div>
</div>

<div class="row">
<div class="col-md-12">

<?php $this->startBoxSolidPrimary(getMLText("sip_light_status")); ?>

<!-- // SIP LIGHTS -->
<div class="row">
<div class="col-md-8">
<div class="box box-default box-solid">
<div class="box-header with-border">
<h3 class="box-title"></h3>
</div>
<div class="box-body gray-background">
	<div class="col-md-4">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $green_sip; ?></h3>
        <p><?php echo "5 o más ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#sip-modal-success"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
	</div>
  <div class="col-md-4">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo $yellow_sip; ?></h3>
        <p><?php echo "Entre 4 y 3 ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#sip-modal-warning"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box bg-red">
      <div class="inner">
        <h3><?php echo $red_sip; ?></h3>
        <p><?php echo "Menos de 2 ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#sip-modal-danger"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>
</div>
</div>

<div class="col-md-4">
<?php $this->startBoxSolidPrimary(getMLText("sip_status")); ?>	

<!-- // SIP STATUS // -->
<table class="table table-bordered table-striped">
  <thead>
    <th class="align-center"><?php printMLText("status"); ?></th>
    <th class="align-center"><?php printMLText("quantity"); ?></th>
  </thead>
  <tbody>
    <?php if (!empty($sip_all_status)): ?>
      <?php foreach ($sip_all_status as $key => $value): ?>
        <tr>
          <td class=""><?php echo $key; ?></td>
          <td class="align-center"><span><b><?php echo $value; ?></b></span></td>
        </tr>  
      <?php endforeach ?>        
    <?php endif ?>
  </tbody>
</table>
<?php $this->endsBoxSolidPrimary(); ?>
</div>

<div class="col-md-12">
  <?php $this->startBoxSolidDefault("Solicitudes por colaborador"); ?>
  <table class="documents-table table table-striped" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Colaborador</th>
    <th class="align-center">Solicitudes asignadas</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($global_user_documents_sip) && (isset($global_user_documents_sip[0]) && !empty($global_user_documents_sip[0]))): ?>
      <?php foreach ($global_user_documents_sip as $sip): ?>
        <?php $assigned_user = $dms->getUser($sip[0]['user_id']); ?>
        <tr>
          <td class=""><?php echo $assigned_user->getFullName(); ?></td>
          <td class="align-center"><b><?php echo count($sip); ?></b></td>
          <td class="align-center">
            <a href="#" type="button" rel="<?php echo $sip[0]['user_id']; ?>" data-action="getusersip" class="btn btn-info view-user-docs"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  <?php $this->endsBoxSolidDefault(); ?>
</div>

</div>
<!-- // SIP END // -->
<?php $this->endsBoxSolidPrimary(); ?>

<?php $this->startBoxSolidPrimary(getMLText("cc_light_status")); ?>
<!-- START CC LIGHTS -->
<div class="row">
<div class="col-md-8">
<div class="box box-default box-solid">
<div class="box-header with-border">
<h3 class="box-title"></h3>
</div>
<div class="box-body gray-background">
  <div class="col-md-4">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $green_cc; ?></h3>
        <p><?php echo "5 o más ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#cc-modal-success"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo $yellow_cc; ?></h3>
        <p><?php echo "Entre 4 y 3 ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#cc-modal-warning"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box bg-red">
      <div class="inner">
        <h3><?php echo $red_cc; ?></h3>
        <p><?php echo "Menos de 2 ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#cc-modal-danger"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>
</div>
</div>

<div class="col-md-4">
<?php $this->startBoxSolidPrimary(getMLText("cc_status")); ?> 

<!-- // CC STATUS // -->
<table class="table table-bordered table-striped">
  <thead>
    <th class="align-center"><?php printMLText("status"); ?></th>
    <th class="align-center"><?php printMLText("quantity"); ?></th>
  </thead>
  <tbody>
    <?php if (!empty($cc_all_status)): ?>
      <?php foreach ($cc_all_status as $key => $value): ?>
        <tr>
          <td class=""><?php echo $key; ?></td>
          <td class="align-center"><span><b><?php echo $value; ?></b></span></td>
        </tr>  
      <?php endforeach ?>        
    <?php endif ?>
  </tbody>
</table>
<?php $this->endsBoxSolidPrimary(); ?>
</div>

<div class="col-md-12">
  <?php $this->startBoxSolidDefault("Consultas por colaborador"); ?>
  <table class="documents-table table table-striped" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Colaborador</th>
    <th class="align-center">Consultas asignadas</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($global_user_documents_cc) && (isset($global_user_documents_cc[0]) && !empty($global_user_documents_cc[0]))): ?>
      <?php foreach ($global_user_documents_cc as $cc): ?>
        <?php $assigned_user = $dms->getUser($cc[0]['user_id']); ?>
        <tr>
          <td class=""><?php echo $assigned_user->getFullName(); ?></td>
          <td class="align-center"><b><?php echo count($cc); ?></b></td>
          <td class="align-center">
            <a href="#" type="button" rel="<?php echo $cc[0]['user_id']; ?>" data-action="getusercc" class="btn btn-info view-user-docs"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  <?php $this->endsBoxSolidDefault(); ?>
</div>
</div>
<!-- END CC LIGHTS -->
<?php $this->endsBoxSolidPrimary(); ?>

<?php $this->startBoxSolidPrimary(getMLText("req_light_status")); ?>
<!-- MEMOS CC LIGHTS -->
<div class="row">
<div class="col-md-8">
<div class="box box-default box-solid">
<div class="box-header with-border">
<h3 class="box-title"></h3>
</div>
<div class="box-body gray-background">
<div class="box-body gray-background">
  <div class="col-md-4">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $green_memo; ?></h3>
        <p><?php echo "5 o más ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#memo-modal-success"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo $yellow_memo; ?></h3>
        <p><?php echo "Entre 4 y 3 ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#memo-modal-warning"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box bg-red">
      <div class="inner">
        <h3><?php echo $red_memo; ?></h3>
        <p><?php echo "Menos de 2 ".getMLText("days"); ?></p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer" data-toggle="modal" data-target="#memo-modal-danger"><?php printMLText("see");?> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>
</div>
</div>
</div>

<div class="col-md-4">
<?php $this->startBoxSolidPrimary(getMLText("memo_status")); ?>	

<!-- // CC STATUS // -->
<table class="table table-bordered table-striped">
  <thead>
    <th class="align-center"><?php printMLText("status"); ?></th>
    <th class="align-center"><?php printMLText("quantity"); ?></th>
  </thead>
  <tbody>
    <?php if (!empty($memo_all_status)): ?>
      <?php foreach ($memo_all_status as $key => $value): ?>
        <tr>
          <td class=""><?php echo $key; ?></td>
          <td class="align-center"><span><b><?php echo $value; ?></b></span></td>
        </tr>  
      <?php endforeach ?>        
    <?php endif ?>
  </tbody>
</table>
<?php $this->endsBoxSolidPrimary(); ?>
</div>

<div class="col-md-12">
  <?php $this->startBoxSolidDefault("Requerimientos por unidad asignada"); ?>
  <table class="documents-table table table-striped" style="background-color: #f2f2f2;color: #000;">
    <thead>
    <tr>
    <th class="align-center">Unidad</th>
    <th class="align-center">Requerimientos asignados</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($global_group_documents) && (isset($global_group_documents[0]) && !empty($global_group_documents[0]))): ?>
      <?php foreach ($global_group_documents as $group): ?>
        <?php $assigned_group = $dms->getGroup($group[0]['group_id']); ?>
        <tr>
          <td class=""><?php echo $assigned_group->getName(); ?></td>
          <td class="align-center"><b><?php echo count($group); ?></b></td>
          <td class="align-center">
            <a href="#" type="button" rel="<?php echo $group[0]['group_id']; ?>" data-action="getgroupmemo" class="btn btn-info view-user-docs"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>      
    <?php endif ?>
    </tbody>
    </table>
  <?php $this->endsBoxSolidDefault(); ?>
</div>

</div>
<!-- ENDS MEMOS LIGHTS -->
<?php $this->endsBoxSolidPrimary(); ?>

<?php $this->startBoxSolidPrimary(getMLText("charts")); ?>
<div class="row">
<div class="col-md-6">
<?php $this->startBoxSolidSuccess(getMLText("sip_by_year")); ?>

<div id="chart_one" style="height: 400px;" class="chart"></div>

<?php $this->endsBoxSolidSuccess(); ?>
</div>

<div class="col-md-6">
<?php $this->startBoxSolidSuccess(getMLText("cc_by_year")); ?>	

<div id="chart_two" style="height: 400px;" class="chart"></div>

<?php $this->endsBoxSolidSuccess(); ?>
</div>
</div>

<?php $this->endsBoxSolidPrimary(); ?>
<?php
		echo "</div>";
		echo "</div>";
		echo "</div>";
		
    $this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>