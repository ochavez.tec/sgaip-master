<?php
/**
 * Implementation of RemoveInstrument view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for RemoveUser view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_RemoveNotifySchedule extends SeedDMS_Bootstrap_Style {

	function show() { /* {{{ */
		$dms = $this->params['dms'];
//		$instrumentvar = $this->params['instrumentvar'];
		$rmnotify = $this->params['rmnotify'];
		$id = $this->params['id'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
    <div class="gap-10"></div>
    <div class="row">
    <div class="col-md-12">
    <?php 

		$this->startBoxDanger(getMLText("rm_notify"));

?>
<form action="../op/op.NotifyScheduleMgr.php" name="form1" method="post">
<input type="hidden" name="id" value="<?php print $rmnotify->getID();?>">
<input type="hidden" name="action" value="removenotify">
<?php echo createHiddenFieldWithKey('removenotify'); ?>
<p>
<?php printMLText("confirm_rm_notify", array ("notifyid" => htmlspecialchars($rmnotify->getID())));?>
</p>

<p><button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> <?php printMLText("rm_notify");?></button></p>

</form>
<?php
		$this->endsBoxDanger();
		$this->contentEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>
