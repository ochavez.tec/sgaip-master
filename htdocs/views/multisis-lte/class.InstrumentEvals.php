<?php
/**
 * Implementation of UsrMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_InstrumentEvals extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		if(isset($this->params['seleval']))
			$currEval = $this->params['seleval'];
		
		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		//$this->printFolderChooserJs("form".($currEval ? $currEval->getDate() : '0'));
?>
function folderSelected(id, name) {
	window.location = '../out/out.ViewFolder.php?folderid=' + id;
}

function checkForm()
{
	msg = new Array();

	if($("#result").val() == "") msg.push("<?php printMLText("js_no_result");?>");
<?php
	if ($strictformcheck) {
?>
	if($("#comment").val() == "") msg.push("<?php printMLText("js_no_comment");?>");
<?php
	}
?>
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$(document).ready( function() {
	$('body').on('submit', '#form', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});
	$( "#selector" ).change(function() {
		seldate = $(this).val();
		seldate = seldate.substr(0,10);
		seldate = seldate.replace(/-/g,"/");
		$('div.ajax').trigger('update', {seleval: seldate});
		//document.getElementById("evaldateInput").setAttribute("disabled", true);
		//$("#evaldateInput").attr('readonly', true);
		//console.log($(this).val());
	});
});
<?php
	} /* }}} */

	function info() { /* {{{ */
		$dms = $this->params['dms'];
/*
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
*/
		if(isset($this->params['seleval']))
			$seleval = $this->params['seleval'];

		if(isset($seleval)) {
			$sessionmgr = new SeedDMS_SessionMgr($dms->getDB());


// 			echo "<table class=\"table table-striped table-bordered\">\n";
// 			echo "<tr><td>".getMLText('discspace')."</td><td>";
// 			$qt = $seleval->getQuota() ? $seleval->getQuota() : $quota;
// 			echo SeedDMS_Core_File::format_filesize($seleval->getUsedDiskSpace())." / ".SeedDMS_Core_File::format_filesize($qt)."<br />";
// 			echo $this->getProgressBar($seleval->getUsedDiskSpace(), $qt);
// 			echo "</td></tr>\n";
// 			$documents = $seleval->getDocuments();
// 			echo "<tr><td>".getMLText('documents')."</td><td>".count($documents)."</td></tr>\n";
// 			$documents = $seleval->getDocumentsLocked();
// 			echo "<tr><td>".getMLText('documents_locked')."</td><td>".count($documents)."</td></tr>\n";
// 			if($workflowmode == "traditional") {
// 				$reviewStatus = $seleval->getReviewStatus();
// 				if($reviewStatus['indstatus']) {
// 					$i = 0;
// 					foreach($reviewStatus['indstatus'] as $rv) {
// 						if($rv['status'] == 0) {
// 							$i++;
// 						}
// 					}
// 					echo "<tr><td>".getMLText('pending_reviews')."</td><td>".$i."</td></tr>\n";
// 				}
// 			}
// 			if($workflowmode == "traditional" || $workflowmode == 'traditional_only_approval') {
// 				$approvalStatus = $seleval->getApprovalStatus();
// 				if($approvalStatus['indstatus']) {
// 					$i = 0;
// 					foreach($approvalStatus['indstatus'] as $rv) {
// 						if($rv['status'] == 0) {
// 							$i++;
// 						}
// 					}
// 					echo "<tr><td>".getMLText('pending_approvals')."</td><td>".$i."</td></tr>\n";
// 				}
// 			}
// 			if($workflowmode == 'advanced') {
// 				$workflowStatus = $seleval->getWorkflowStatus();
// 				if($workflowStatus['u'])
// 					echo "<tr><td>".getMLText('pending_workflows')."</td><td>".count($workflowStatus['u'])."</td></tr>\n";
// 			}
// 			$sessions = $sessionmgr->getUserSessions($seluser);
// 			if($sessions) {
// 				$session = array_shift($sessions);
// 				echo "<tr><td>".getMLText('lastaccess')."</td><td>".getLongReadableDate($session->getLastAccess())."</td></tr>\n";
// 			}
// 			echo "</table>";
// 			echo "<br>";


// 			if($user->isAdmin() && $seleval->getID() != $instrument->getID())
// 					echo "<a class=\"btn btn-success\" href=\"../op/op.SubstituteUser.php?userid=".((int) $seluser->getID())."&formtoken=".createFormKey('substituteuser')."\"><i class=\"fa fa-exchange\"></i> ".getMLText('substitute_user')."</a> ";
		}
	} /* }}} */

	function form() { /* {{{ */
		if(isset($this->params['seleval']))
			$seleval = $this->params['seleval'];
		else
			$seleval = '';

		$this->showInstrumentEvalsForm($seleval);
	} /* }}} */

	function showInstrumentEvalsForm($currEval) { /* {{{ */
		$dms = $this->params['dms'];
/*
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
*/
//		$instruments = $this->params['allinstruments'];
		$axes = $this->params['allaxes'];
		$httproot = $this->params['httproot'];
?>
	<form action="../op/op.InstrumentEvals.php" method="post" enctype="multipart/form-data" name="<?php echo "form".($currEval ? $currEval : '0'); ?>" id="<?php echo "form".($currEval ? $currEval : '0'); ?>">
<?php
		if($currEval) {
			echo createHiddenFieldWithKey('editinstrumenteval');
?>
	<input type="hidden" name="seleval" id="seleval" value="<?php print $currEval;?>">
	<input type="hidden" name="action" value="editinstrumenteval">
<?php
		} else {
			echo createHiddenFieldWithKey('addinstrumenteval');
			$today = date("Y/m/d");
?>
	<input type="hidden" name="seleval" id="seleval" value="<?php print $today;?>">
	<input type="hidden" name="action" value="addinstrumenteval">
<?php
		}
 	$this->startBoxCollapsableInfo(getMLText("instrumenteval_info"));
?>
<div class="col-sm-12">
	<table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
<?php
	if(isset($currEval) && !empty($currEval)) {
//		var_dump($evaldate);
		$evaldate = $currEval;
?>
		<tr>
			<td colspan="3"><a class="btn btn-danger" href="../out/out.RemoveInstrumentEval.php?evaldate=<?php print $currEval;?>"><i class="fa fa-times"></i> <?php printMLText("rm_instrumenteval");?></a></td>
		</tr>
<?php
	} else {
		$evaldate = '';
	}
	
?>

		<div class="control-form-group">
			<label><?php printMLText("eval_date");?>:</label>
			<div><?php //$this->printDateChooser(-1, "evaldateInput");?>
    		<span class="input-append date span12" id="evaldate" data-date="<?php echo $evaldate; ?>" data-date-format="yyyy/mm/dd">
      		<input class="form-control" name="evaldateInput" id="evaldateInput" type="text" value="<?php echo ($evaldate ? $evaldate : $today); ?>">
      		<span class="add-on"><i class="icon-calendar"></i></span>
    		</span>
			</div>
		</div>

<?php	
	
	
	foreach($axes as $axe) {
?>
		<tr style="background-color:#ffe3c1;">
			<th colspan="2"><h4><?php printMLText("transparency_axe"); echo ": ".$axe->getName(); ?></h4></th>
			<th><h4><?php printMLText("axe_points"); echo ": ".$axe->getMaxScore(); ?></h4></th>
		</tr>
<?php
	
	$axeInstruments = $axe->getInstruments();

?>
		<tr>
			<th><?php printMLText("transparency_instrument");?></th>
			<th><?php printMLText("instrument_weight");?></th>
			<th><?php printMLText("instrument_score");?></th>
		</tr>
<?php

	
	foreach($axeInstruments as $axeInst) {
?>
		<tr>
			<td><?php echo $axeInst->getName(); ?></td>
			<td><?php echo $axeInst->getWeight(); ?></td>
<?php
		$thisEval = new SeedDMS_Core_InstrumentEval($axeInst->getID(), $evaldate, 0, 0, '');
		$thisEval = $thisEval->getInstance($axeInst->getID(), $evaldate, $dms);
?>
			<td><input type="text" class="form-control" name="evalscore[<?php print $axeInst->getID();?>]" id="evalscore_<?php print $axeInst->getID();?>" value="<?php print $thisEval ? htmlspecialchars($thisEval->getScore()) : "";?>"></td>
		</tr>

<?php 
	} // END FOREACH AXEINSTRUMENTS
	} // END FOEARCH AXES 
?>
		<tr>
			<td colspan="3"><button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText($currEval ? "save" : "add_instrument_eval")?></button></td>
		</tr>
	</table>
</div>
<?php
	$this->endsBoxCollapsableInfo();
?>
	</form>
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
		if(isset($this->params['seleval']))
			$seleval = $this->params['seleval'];
		else
			$seleval = '';
//		$instruments = $this->params['allinstruments'];
		$axes = $this->params['allaxes'];
		$httproot = $this->params['httproot'];
		$evaldates = $this->params['evaldates'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
    <div class="gap-10"></div>
    <div class="row">
    <div class="col-md-12">
    <?php 

		$this->startBoxPrimary(getMLText("instrumenteval_management"));
?>

<?php //if ($user->_comment != "client-admin") { ?>
<div class="row-fluid">
<div class="span12">
<div class="well row-fluid">
<form class="form-horizontal">
	<div class="control-group">
		<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
		<div class="controls">
<select class="chzn-select" id="selector">
<option value="-1"><?php echo getMLText("choose_instrument_eval")?></option>
<option value="0"><?php echo getMLText("add_instrument_eval")?></option>
<?php
		foreach ($evaldates as  $ekey => $eval) {
			$date_string = date("Y/m/d", strtotime($eval['date']));
			print "<option value=\"".$eval['date']."\" ".($seleval && $eval['date']==$seleval ? 'selected' : '')."\">" . htmlspecialchars($date_string) . "</option>";
		}
?>
</select>
</div>
</div>
</form>
</div>
	<div class="ajax" data-view="InstrumentEvals" data-action="info" <?php echo ($seleval ? "data-query=\"seleval=".$seleval."\"" : "") ?>></div>
</div>

<div class="span12">
	<div class="">
		<div class="ajax" data-view="InstrumentEvals" data-action="form" <?php echo ($seleval ? "data-query=\"seleval=".$seleval."\"" : "") ?>></div>
	</div>
</div>
</div>

<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
