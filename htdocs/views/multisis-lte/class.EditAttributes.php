<?php
/**
 * Implementation of EditAttributes view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditAttributes view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_EditAttributes extends SeedDMS_Bootstrap_Style {
    function js() {
        ?>
        $('body').on('submit', '#form1', function(ev){
            $("#box-form1").append("<div class=\"overlay\"><i class=\"fa fa-refresh fa-spin\"></i></div>");
        });

        /*// -- Detect words -- //*/
        $(document).ready( function() {
            $("#filechooser").change(function(){
                $("#file_scan").fadeIn();

                var data = new FormData();
                jQuery.each(jQuery('#filechooser')[0].files, function(i, file) {
                    data.append('file-'+i, file);
                });

                jQuery.ajax({
                    url: '/op/op.DetectSensitiveWords.php',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){
                        $("#file_scan").hide('fast');
                        if(data != "file-not-allowed" && data != false){
                            $("#file_scan_response").text(data);
                            $("#file_scan_response_two").fadeOut();
                            $("#file_scan_response_one").fadeIn();
                            $("#file_scan_response_three").fadeOut();
                        } else if(data == "file-not-allowed" && data != false){
                            $("#file_scan_response_two").fadeOut();
                            $("#file_scan_response_one").fadeOut();
                            $("#file_scan_response_three").fadeIn();
                        } else if(!data){
                            $("#file_scan_response_one").fadeOut();
                            $("#file_scan_response_two").fadeIn();
                            $("#file_scan_response_three").fadeOut();
                        }
                    }
                });
            });
        });

        <?php
    }
    function show() { /* {{{ */
        $dms = $this->params['dms'];
        $user = $this->params['user'];
        $folder = $this->params['folder'];
        $document = $this->params['document'];
        $version = $this->params['version'];
        $attrdefs = $this->params['attrdefs'];

        $this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))), "skin-blue sidebar-mini");
        $this->containerStart();
        $this->mainHeader();
        $this->mainSideBar();
        $this->contentStart();    

        $user_root_folder_id = $this->params['user']->getHomeFolder();
        $user_root_folder = $this->params['dms']->getFolder($user_root_folder_id);
        echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document);

        //// Document content ////
        echo "<div class=\"row\">";
        echo "<div class=\"col-md-12\">";

        echo "<div class=\"box box-success div-green-border\" id=\"box-form1\">";
        echo "<div class=\"box-header with-border\">";
        echo "<h3 class=\"box-title\">".getMLText("edit_attributes")."</h3>";
        echo "</div>";
        echo "<div class=\"box-body\">";
?>
<form class="form-horizontal" action="../op/op.EditAttributes.php" id="form1" name="form1" method="POST" enctype="multipart/form-data">
    <?php echo createHiddenFieldWithKey('editattributes'); ?>
    <input type="hidden" name="documentid" value="<?php print $document->getID();?>">
    <input type="hidden" name="version" value="<?php print $version->getVersion();?>">
    <table class="table-condensed">
        <tr>
            <td class="lbl-right"><?php printMLText("name");?>:</td>
            <td class="lbl-left"><?php print $version->_orgFileName; ?></td>
        </tr>
        <tr>
            <td class="lbl-right"><?php printMLText("version");?>:</td>
            <td class="lbl-left"><?php print $version->getVersion(); ?></td>
        </tr>
        <tr>
            <td class="lbl-right"><?php printMLText("change_local_file");?>:</td>
            <td>
                <div id="upload-files">
                    <div id="upload-file">
                        <div class="input-append">
                            <input type="text" class="form-control" readonly>
                            <span class="btn btn-primary btn-file">
                                       <i class="fa fa-search"></i> <?php printMLText("browse");?> <input id="filechooser" type="file" name="filename[]">
                                    </span>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr id="file_scan" style="display: none;">
            <td></td>
            <td>
            <div class="col-md-12" style="height: 115px;">
                <p><b><?php printMLText("checking_file"); ?></b></p>
                <div class="overlay" style="height: 50px;"><i class="fa fa-refresh fa-spin"></i></div>
            </div>
            </td>
        </tr>
        <tr id="file_scan_response_one" style="display: none;">
            <td></td>
            <td>
            <div class="col-md-12" style="height: 115px;">
                    <div class="alert alert-warning">                   
                    <h4><i class="icon fa fa-warning"></i> <?php echo getMLText("sensitive_words_warning"); ?></h4>
                    <p><?php echo getMLText("sensitive_words_msg_one"); ?><b><span id="file_scan_response"></span></b></p>
                    </div>                  
            </div>
            </td>
        </tr>
        <tr id="file_scan_response_two" style="display: none;">
            <td></td>
            <td>
            <div class="col-md-12" style="height: 115px;">
                    <div class="alert alert-info">                  
                    <h4><i class="icon fa fa-info"></i> <?php echo getMLText("sensitive_words_info"); ?></h4>
                    <p><?php echo getMLText("sensitive_words_msg_two"); ?></p>
                    </div>                  
            </div>
            </td>
        </tr>
        <tr id="file_scan_response_three" style="display: none;">
            <td></td>
            <td>
            <div class="col-md-12" style="height: 115px;">
                    <div class="alert alert-info">                  
                    <h4><i class="icon fa fa-info"></i> <?php echo getMLText("sensitive_words_info"); ?></h4>
                    <p><?php echo getMLText("sensitive_words_msg_three"); ?></p>
                    </div>                  
            </div>
            </td>
        </tr>

        <tr>
            <td class="lbl-right"><?php printMLText("comment_for_current_version");?>:</td>
            <td><textarea class="form-control" name="version_comment" rows="6" cols="80"><?php echo $version->getComment(); ?></textarea><br />
        </tr>
        <tr>
            <?php
            if($attrdefs) {
                foreach($attrdefs as $attrdef) {
                    $arr = $this->callHook('editDocumentContentAttribute', $version, $attrdef);
                    if(is_array($arr)) {
                        if($arr) {
                            echo "<div class=\"control-group\">";
                            echo "<label class=\"control-label\">".$arr[0].":</label>";
                            echo "<div class=\"controls\">".$arr[1]."</div>";
                            echo "</div>";
                        }
                    } else {
            ?>
            <div class="control-group">
            <label class="control-label"><?php echo htmlspecialchars($attrdef->getName()); ?></label>
                <div class="controls">
                    <?php $this->printAttributeEditField($attrdef, $version->getAttribute($attrdef)) ?>
                </div>
            </div>
            <?php
                            }
                        }
                    }
            ?>
        </tr>
        <tr>
            <td>
                <div class="controls">
                    <a href="<?php echo '/out/out.ViewDocument.php?documentid='.$document->getID().'&currenttab=docinfo'; ?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> <?php printMLText("cancel"); ?></a>
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php printMLText("save") ?></button>
                </div>
            </td>
        </tr>
    </table>
</form>
<?php
    
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>"; 
    echo "</div>"; // Ends row
    $this->contentEnd();
    $this->mainFooter();    
    $this->containerEnd();
    $this->htmlEndPage();
    } /* }}} */
}
?>
