<?php
	
	$_name = str_replace(' ', '', $_POST['workflow']);
	$file_name = strtolower($_name);

	$dir = realpath(dirname(__FILE__));

	$temp_dir = $dir."/json";

	if (!file_exists($temp_dir)) {
		mkdir($temp_dir, 0765, true);
	}

	$fp = fopen($dir."/json/".$file_name.".json", 'w');

	fwrite($fp, json_encode($_POST['graph']));
	fclose($fp);
	chmod($dir."/json/".$file_name.".json", 0777);

	echo json_encode(array('success' => true, 'workflow' => $_POST['workflow']));
	
?>