<?php
   /**
    * Implementation of ViewFolder view
    *
    * @category   DMS
    * @package    SeedDMS
    * @license    GPL 2
    * @version    @version@
    * @author     Uwe Steinmann <uwe@steinmann.cx>
    * @copyright  Copyright (C) 2002-2005 Markus Westphal,
    *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
    *             2010-2012 Uwe Steinmann
    * @version    Release: @package_version@
    */
   /**
    * Include parent class
    */
   require_once("class.Bootstrap.php");
   /**
    * Include class to preview documents
    */
   require_once("SeedDMS/Preview.php");
   /**
    * Class which outputs the html page for ViewFolder view
    *
    * @category   DMS
    * @package    SeedDMS
    * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
    * @copyright  Copyright (C) 2002-2005 Markus Westphal,
    *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
    *             2010-2012 Uwe Steinmann
    * @version    Release: @package_version@
    */
   class SeedDMS_View_ViewFolder extends SeedDMS_Bootstrap_Style {
   	function getAccessModeText($defMode) { /* {{{ */
   		switch($defMode) {
   			case M_NONE:
   				return getMLText("access_mode_none");
   				break;
   			case M_READ:
   				return getMLText("access_mode_read");
   				break;
   			case M_READWRITE:
   				return getMLText("access_mode_readwrite");
   				break;
   			case M_ALL:
   				return getMLText("access_mode_all");
   				break;
   		}
   	} /* }}} */
   	function printAccessList($obj) { /* {{{ */
   		$accessList = $obj->getAccessList();
   		if (count($accessList["users"]) == 0 && count($accessList["groups"]) == 0)
   			return;
   		$content = '';
   		for ($i = 0; $i < count($accessList["groups"]); $i++)
   		{
   			$group = $accessList["groups"][$i]->getGroup();
   			$accesstext = $this->getAccessModeText($accessList["groups"][$i]->getMode());
   			$content .= $accesstext.": ".htmlspecialchars($group->getName());
   			if ($i+1 < count($accessList["groups"]) || count($accessList["users"]) > 0)
   				$content .= "<br />";
   		}
   		for ($i = 0; $i < count($accessList["users"]); $i++)
   		{
   			$user = $accessList["users"][$i]->getUser();
   			$accesstext = $this->getAccessModeText($accessList["users"][$i]->getMode());
   			$content .= $accesstext.": ".htmlspecialchars($user->getFullName());
   			if ($i+1 < count($accessList["users"]))
   				$content .= "<br />";
   		}
   		if(count($accessList["groups"]) + count($accessList["users"]) > 3) {
   			$this->printPopupBox(getMLText('list_access_rights'), $content);
   		} else {
   			echo $content;
   		}
   	} /* }}} */
   	function js() { /* {{{ */
   		$user = $this->params['user'];
   		$folder = $this->params['folder'];
   		$orderby = $this->params['orderby'];
   		$expandFolderTree = $this->params['expandFolderTree'];
   		$enableDropUpload = $this->params['enableDropUpload'];
   		header('Content-Type: application/javascript; charset=UTF-8');
   		parent::jsTranslations(array('cancel', 'splash_move_document', 'confirm_move_document', 'move_document', 'splash_move_folder', 'confirm_move_folder', 'move_folder'));
   		
   ?>
function folderSelected(id, name) {
  window.location = '../out/out.ViewFolder.php?folderid=' + id;
}

function checkForm() {
  msg = new Array();
  if (document.form1.name.value == "") msg.push("<?php printMLText("js_no_name");?>");
  if (document.form1.comment.value == "") msg.push("<?php printMLText("js_no_comment ");?>");
  if (msg != "") {
    noty({
      text: msg.join('<br />'),
      type: 'error',
      dismissQueue: true,
      layout: 'topRight',
      theme: 'defaultTheme',
      _timeout: 1500,
    });
    return false;
  } else
    return true;
}

function checkForm2() {
  msg = new Array();
  if (document.form2.name.value == "") msg.push("<?php printMLText("js_no_name");?>");
  if (document.form2.comment.value == "") msg.push("<?php printMLText("js_no_comment");?>");
  /*if (document.form2.expdate.value == "") msg.push("<?php printMLText("js_no_expdate");?>");*/
  if (document.form2.theuserfile.value == "") msg.push("<?php printMLText("js_no_file");?>");
  if (msg != "") {
    noty({
      text: msg.join('<br />'),
      type: 'error',
      dismissQueue: true,
      layout: 'topRight',
      theme: 'defaultTheme',
      _timeout: 1500,
    });
    return false;
  } else
    return true;
}
$(document).ajaxStart(function () {
  Pace.restart();
});
//  $('.ajax').click(function(){
//    $.ajax({url: '#', success: function(result){
//    $('.ajax-content').html('<hr>Ajax Request Completed !');
//  }});
//});
$(document).ready(function () {
      $('body').on('submit', '#form1', function (ev) {
        if (!checkForm()) {
          ev.preventDefault();
        } else {
          $("#box-form1").append("<div class = \"overlay\"><i class=\"fa fa-refresh fa-spin\"></i></div>");
          }
        }); $('body').on('submit', '#form2', function (ev) {
          if (!checkForm2()) {
            ev.preventDefault();
          } else {
            $("#box-form2").append("<div class = \"overlay\"><i class=\"fa fa-refresh fa-spin\"></i></div>");
            }
          }); $("#form1").validate({
          invalidHandler: function (e, validator) {
            noty({
              text: (validator.numberOfInvalids() == 1) ? "<?php printMLText("js_form_error ");?>".replace('#', validator.numberOfInvalids()): "<?php printMLText("js_form_errors ");?>".replace('#', validator.numberOfInvalids()),
              type: 'error',
              dismissQueue: true,
              layout: 'topRight',
              theme: 'defaultTheme',
              timeout: 1500,
            });
          },
          messages: {
            name: "<?php printMLText("js_no_name ");?>",
            comment: "<?php printMLText("js_no_comment ");?>"
          },
        }); $("#form2").validate({
          invalidHandler: function (e, validator) {
            noty({
              text: (validator.numberOfInvalids() == 1) ? "<?php printMLText("js_form_error ");?>".replace('#', validator.numberOfInvalids()): "<?php printMLText("js_form_errors ");?>".replace('#', validator.numberOfInvalids()),
              type: 'error',
              dismissQueue: true,
              layout: 'topRight',
              theme: 'defaultTheme',
              timeout: 1500,
            });
          },
          messages: {
            name: "<?php printMLText("js_no_name ");?>",
            comment: "<?php printMLText("js_no_comment ");?>",
            /*expdate: "<?php printMLText("js_no_expdate");?>",*/
            theuserfile: "<?php printMLText("js_no_file ");?>",
          },
        });

        $("#add-folder").on("click", function () {
          $("#div-add-folder").show('slow');
        }); 

        $(".cancel-add-folder").on("click", function () {
          $("#div-add-folder").hide('slow');

        });

        $("#drag-and-drop").on("click", function () {
          $("#div-drop-upload").show('slow');
        }); 

        $(".cancel-drop-upload").on("click", function () {
          $("#div-drop-upload").hide('slow');
        }); 

        $("#add-document").on("click", function () {
          $("#div-add-document").show('slow');
        }); $(".cancel-add-document").on("click", function () {
          $("#div-add-document").hide('slow');
        }); $(".move-doc-btn").on("click", function (ev) {
          id = $(ev.currentTarget).attr('rel');
          $("#table-move-document-" + id).show('slow');
        }); $(".cancel-doc-mv").on("click", function (ev) {
          id = $(ev.currentTarget).attr('rel');
          $("#table-move-document-" + id).hide('slow');
        }); $(".move-folder-btn").on("click", function (ev) {
          id = $(ev.currentTarget).attr('rel');
          $("#table-move-folder-" + id).show('slow');
        }); $(".cancel-folder-mv").on("click", function (ev) {
          id = $(ev.currentTarget).attr('rel');
          $("#table-move-folder-" + id).hide('slow');
        }); $("#btn-next-1").on("click", function () {
          $("#nav-tab-1").removeClass("active");
          $("#nav-tab-2").addClass("active");
          $('html, body').animate({
            scrollTop: 0
          }, 800);
        }); $("#btn-next-2").on("click", function () {
          $("#nav-tab-2").removeClass("active");
          $("#nav-tab-3").addClass("active");
          $('html, body').animate({
            scrollTop: 0
          }, 800);
        }); $("#btn-next-3").on("click", function () {
          $("#nav-tab-3").removeClass("active");
          $("#nav-tab-4").addClass("active");
          $('html, body').animate({
            scrollTop: 0
          }, 800);
        }); $("#btn-next-4").on("click", function () {
          $("#nav-tab-4").removeClass("active");
          $("#nav-tab-5").addClass("active");
          $('html, body').animate({
            scrollTop: 0
          }, 800);
        });
        /* ---- For document previews ---- */
        $(".preview-doc-btn").on("click", function () {
          $("#div-add-folder").hide();
          $("#div-add-document").hide();
          $("#div-drop-upload").hide();
          $("#folder-content").hide();
          var docID = $(this).attr("id");
          var version = $(this).attr("rel");
          $("#doc-title").text($(this).attr("title"));
          $("#document-previewer").show('slow');
          $("#iframe-charger").attr("src","../pdfviewer/web/viewer.html?file=..%2F..%2Fop%2Fop.Download.php%3Fdocumentid%3D"+docID+"%26version%3D"+version);
        }); $(".close-doc-preview").on("click", function () {
          $("#document-previewer").hide();
          $("#iframe-charger").attr("src", "");
          $("#folder-content").show('slow');
        });
        /* ---- For datatables ---- */
        $('.table').DataTable({
          "paging": true,
          "lengthChange": false,
          "pageLength": 20,
          "searching": true,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ningún registro ha sido encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar",
            "paginate": {
              "first": "Primero",
              "last": "Último",
              "next": "Siguiente",
              "previous": "Anterior"
            }
          }
        });
        /* ---- For category fields ---- */
        var allTypeOptions = $('#type-selector option'); $("#cat-selector").on("change", function () {
          var currentCat = this.value
          /*console.log('current_cat: ' + currentCat);*/
          var attrs = $('[id^=attr-group_]');
          // Filter types according to cat selected
          $('#type-selector option').remove(); //remove all options
          for (var k = 0, klen = allTypeOptions.length; k < klen; k++) {
            var catsT = allTypeOptions[k].getAttribute('data-cats');
            var catsTArr = JSON.parse(catsT);
            var foundT = catsTArr.indexOf(currentCat);
            if (foundT >= 0) {
              $('#type-selector').append(allTypeOptions[k]);
            }
          }
          // Filter attributes according to cat selected
          for (var i = 0, len = attrs.length; i < len; i++) {
            var cats = attrs[i].getAttribute('data-cats');
            var catsArr = JSON.parse(cats);
            var id = attrs[i].getAttribute('id');
            var found = catsArr.indexOf(currentCat);
            //     console.log(id);      
            //     console.log(found);
            if (found < 0) {
              $("#" + id).hide();
            } else {
              $("#" + id).show();
            }
          }
        });
        /*// -- Detect words -- //*/
        $("#filechooser").change(function () {
          $("#file_scan").fadeIn();
          var data = new FormData();
          jQuery.each(jQuery('#filechooser')[0].files, function (i, file) {
            data.append('file-' + i, file);
          });
          jQuery.ajax({
            url: '/op/op.DetectSensitiveWords.php',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
              $("#file_scan").hide('fast');
              if (data != "file-not-allowed" && data != false) {
                $("#file_scan_response").text(data);
                $("#file_scan_response_two").fadeOut();
                $("#file_scan_response_one").fadeIn();
                $("#file_scan_response_three").fadeOut();
              } else if (data == "file-not-allowed" && data != false) {
                $("#file_scan_response_two").fadeOut();
                $("#file_scan_response_one").fadeOut();
                $("#file_scan_response_three").fadeIn();
              } else if (!data) {
                $("#file_scan_response_one").fadeOut();
                $("#file_scan_response_two").fadeIn();
                $("#file_scan_response_three").fadeOut();
              }
            }
          });
        });
      });
      $(document).ready(function () {
        var allTypeOptionsTwo = $('#type-selector option');
        var currentCatTwo = $("#cat-selector option:first").val();
        /*console.log(currentCatTwo);*/
        var attrsTwo = $('[id^=attr-group_]');
        // Filter types according to cat selected
        $('#type-selector option').remove(); //remove all options
        for (var k = 0, klen = allTypeOptionsTwo.length; k < klen; k++) {
          var catsT = allTypeOptionsTwo[k].getAttribute('data-cats');
          var catsTArr = JSON.parse(catsT);
          var foundT = catsTArr.indexOf(currentCatTwo);
          if (foundT >= 0) {
            $('#type-selector').append(allTypeOptionsTwo[k]);
          }
        }
        // Filter attributes according to cat selected
        for (var i = 0, len = attrsTwo.length; i < len; i++) {
          var cats = attrsTwo[i].getAttribute('data-cats');
          var catsArr = JSON.parse(cats);
          var id = attrsTwo[i].getAttribute('id');
          var found = catsArr.indexOf(currentCatTwo);
          if (found < 0) {
            $("#" + id).hide();
          } else {
            $("#" + id).show();
          }
        }
      });

<?php
   if ($enableDropUpload && $folder->getAccessMode($user) >= M_READWRITE) {
   	echo "SeedDMSUpload.setUrl('../op/op.Ajax.php');";
   	echo "SeedDMSUpload.setAbortBtnLabel('".getMLText("cancel")."');";
   	echo "SeedDMSUpload.setEditBtnLabel('".getMLText("edit_document_props")."');";
   	echo "SeedDMSUpload.setMaxFileSize(".SeedDMS_Core_File::parse_filesize(ini_get("upload_max_filesize")).");";
   	echo "SeedDMSUpload.setMaxFileSizeMsg('".getMLText("uploading_maxsize")."');";
   }
   $this->printDeleteFolderButtonJs();
   $this->printDeleteDocumentButtonJs();
   $this->printKeywordChooserJs("form2");
   $this->printFolderChooserJs("form3");
   $this->printFolderChooserJs("form4");
   } /* }}} */
   
   function show() { /* {{{ */
   $dms = $this->params['dms'];
   $user = $this->params['user'];
   $folder = $this->params['folder'];
   $orderby = $this->params['orderby'];
   $enableFolderTree = $this->params['enableFolderTree'];
   $enableClipboard = $this->params['enableclipboard'];
   $enableDropUpload = $this->params['enableDropUpload'];
   $expandFolderTree = $this->params['expandFolderTree'];
   $showtree = $this->params['showtree'];
   $cachedir = $this->params['cachedir'];
   $workflowmode = $this->params['workflowmode'];
   $enableRecursiveCount = $this->params['enableRecursiveCount'];
   $maxRecursiveCount = $this->params['maxRecursiveCount'];
   $previewwidth = $this->params['previewWidthList'];
   $timeout = $this->params['timeout'];
   $folderid = $folder->getId();
   $user_root_folder_id = $this->params['user']->getHomeFolder();
   $user_root_folder = $this->params['dms']->getFolder($user_root_folder_id);
   
   $this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
   $this->htmlAddHeader('<link href="../styles/'.$this->theme.'/custom/css/styles-uploader.css" rel="stylesheet">'."\n", 'css');

   $this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
   $this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');

   $this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/custom/js/uploader-ui.js"></script>'."\n", 'js');

   $this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');
   
   echo $this->callHook('startPage');
   $this->htmlStartPage(getMLText("folder_title", array("foldername" => htmlspecialchars($folder->getName()))), "skin-blue sidebar-mini");
   $this->containerStart();
   $this->mainHeader();
   $this->mainSideBar($folder->getID(),0,0);
   $previewer = new SeedDMS_Preview_Previewer($cachedir, $previewwidth, $timeout);
   echo $this->callHook('preContent');
   $this->contentStart();		
   echo $this->getFolderPathHTML($user_root_folder, $folder);
   echo "<div class=\"row\">";
    

    // Faster Upload by Drag & Drop
    if ($enableDropUpload && $folder->getAccessMode($user) >= M_READWRITE) {
      echo "<div class=\"col-md-12 div-hidden\" id=\"div-drop-upload\">";

      echo "<div class=\"box box-primary box-solid\">";
      echo "<div class=\"box-header with-border\">";
      echo "<h3 class=\"box-title\">".getMLText("dropupload")."</h3>";
      echo "<div class=\"box-tools pull-right\">";
      echo "<button type=\"button\" class=\"btn btn-box-tool cancel-drop-upload\"><i class=\"fa fa-times\"></i></button>";
      echo "</div>";
      echo "</div>";
      echo "<div class=\"box-body\">";

    ?>
    <br>
    <div style="border:4px dashed #3d8dbc; color: #3d8dbc; background-color: #eaf7ff; text-align: center; font-size: 16px;" id="dragandrophandler" class="alert" data-target="<?php echo $folder->getID(); ?>" data-formtoken="<?php echo createFormKey('adddocument'); ?>">
    <br>
    <br>
    <?php printMLText('drop_files_here'); ?>
    <br>
    <br>
    <br>
    </div>
    <?php
      
      echo "</div>";
      echo "</div>";
      echo "</div>";
    }   


   if($user->isAdmin()) {
   //// Add Folder ////
   echo "<div class=\"col-md-12 div-hidden\" id=\"div-add-folder\">";
   echo "<div class=\"box box-success div-green-border\" id=\"box-form1\">";
     echo "<div class=\"box-header with-border\">";
     echo "<h3 class=\"box-title\">".getMLText("add_subfolder")."</h3>";
     echo "<div class=\"box-tools pull-right\">";
     echo "<button type=\"button\" class=\"btn btn-box-tool cancel-add-folder\"><i class=\"fa fa-times\"></i></button>";
     echo "</div>";
     echo "<!-- /.box-tools -->";
     echo "</div>";
     echo "<!-- /.box-header -->";
     echo "<div class=\"box-body\">";
     ?>
<form class="form-horizontal" action="../op/op.AddSubFolder.php" id="form1" name="form1" method="post">
   <?php echo createHiddenFieldWithKey('addsubfolder'); ?>
   <input type="hidden" name="folderid" value="<?php print $folder->getId();?>">
   <input type="hidden" name="showtree" value="<?php echo showtree();?>">
   <div class="box-body">
      <div class="form-group">
         <label class="col-sm-2 control-label"><?php printMLText("name");?>:</label>
         <div class="col-sm-10"><input class="form-control" type="text" name="name" size="60" required></div>
      </div>
      <div class="form-group">
         <label class="col-sm-2 control-label"><?php printMLText("comment");?>:</label>
         <div class="col-sm-10"><textarea class="form-control" name="comment" rows="4" cols="80"></textarea></div>
      </div>
      <div class="form-group">
         <label class="col-sm-2 control-label"><?php printMLText("sequence");?>:</label>
         <div class="col-sm-10">
            <?php $this->printSequenceChooser($folder->getSubFolders('s')); if($orderby != 's') echo "<br />".getMLText('order_by_sequence_off');?>
         </div>
      </div>
      <?php
         $attrdefs = $dms->getAllAttributeDefinitions(array(SeedDMS_Core_AttributeDefinition::objtype_folder, SeedDMS_Core_AttributeDefinition::objtype_all));
         if($attrdefs) {
         	foreach($attrdefs as $attrdef) {
         	?>
      <div class="form-group" id="<?php echo $attrdef->getID(); ?>">
         <label class="col-sm-2 control-label"><?php echo htmlspecialchars($attrdef->getName()); ?>:</label>
         <div class="col-sm-10"><?php $this->printAttributeEditField($attrdef, '') ?></div>
      </div>
      <?php
         }
         }
         ?>
      <div class="box-footer">
         <a type="button" class="btn btn-default cancel-add-folder">
            <?php echo getMLText("cancel"); ?></a type="button">
            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> <?php printMLText("save")?></button>
      </div>
   </div>
</form>
<?php
   echo "</div>";
   echo "<!-- /.box-body -->";
   echo "</div>";
   echo "</div>";
   
   }
   //// Add Document ////
   echo "<div class=\"col-md-12 div-hidden\" id=\"div-add-document\">";
   echo "<div class=\"box box-warning div-bkg-color\" id=\"box-form2\">";
   echo "<div class=\"box-header with-border\">";
   echo "<h3 class=\"box-title\">".getMLText("add_document")."</h3>";
   echo "<div class=\"box-tools pull-right\">";
   echo "<button type=\"button\" class=\"btn btn-box-tool cancel-add-document\"><i class=\"fa fa-times\"></i></button>";
   echo "</div>";
   echo "<!-- /.box-tools -->";
   echo "</div>";
   echo "<!-- /.box-header -->";
   echo "<div class=\"box-body\">";
   ?>
<form action="../op/op.AddDocument.php" enctype="multipart/form-data" method="post" id="form2" name="form2">
<?php echo createHiddenFieldWithKey('adddocument'); ?>
<input type="hidden" name="folderid" value="<?php print $folderid; ?>">
<input type="hidden" name="showtree" value="<?php echo showtree();?>">
<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
<li class="active" id="nav-tab-1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">1 - <?php echo getMLText("document_infos"); ?></a></li>
<li class="" id="nav-tab-2"><a href="#tab_2" data-toggle="tab" aria-expanded="false">2 - <?php echo getMLText("version_info"); ?></a></li>
<li class="" id="nav-tab-3"><a href="#tab_3" data-toggle="tab" aria-expanded="false">3 - <?php echo getMLText("linked_files"); ?></a></li>
<li class="" id="nav-tab-4"><a href="#tab_4" data-toggle="tab" aria-expanded="false">4 - <?php echo getMLText("linked_documents"); ?></a></li>
<li class="" id="nav-tab-5"><a href="#tab_5" data-toggle="tab" aria-expanded="false">5 - <?php echo getMLText("add_document_notify"); ?></a></li>
</ul>
<div class="tab-content">
<div class="tab-pane active" id="tab_1">
<div class="form-group">
<label><?php echo getMLText("name"); ?>: <span class="is-required">*</span></label>
<input type="text" class="form-control" name="name" id="" placeholder="" required>
</div>
<div class="form-group">
<label><?php echo getMLText("comment"); ?>: <span class="is-required">*</span></label>
<textarea name="comment" class="form-control" rows="3" placeholder="" required></textarea>
</div>
<div class="form-group">
<label><?php echo getMLText("keywords");?>:</label>
<?php $this->printKeywordChooserHtml("form2");?>
</div>
<div class="form-group">
<label><?php printMLText("category")?>:</label>
<select class="form-control chzn-select" name="categories[]" id="cat-selector" data-no_results_text="<?php printMLText('unknown_document_category'); ?>">
<?php
   if($user->isAdmin())
   	$categories = $dms->getDocumentCategories();
   else
   	$categories = $user->getCategories();
   $count = 1;
   $default_category = 0;
   foreach($categories as $category) {
   	if($count==1 && ($default_category == 0)) $default_category = $category->getID();
   	echo "<option value=\"".$category->getID()."\"";
   	echo ">".$category->getName()."</option>";	
   	$count++;
   }
   ?>
</select>
</div>
<div class="form-group">
<label><?php printMLText("type")?>:</label>
<select class="form-control chzn-select" name="types[]" id="type-selector" data-no_results_text="<?php printMLText('unknown_document_type'); ?>">
<?php
   $types = $dms->getDocumentTypes();
   $count = 1;
   $default_type = 0;
   foreach($types as $type) {
   	if($count==1 && ($default_type == 0)) $default_type = $type->getID();
   	echo "<option value=\"".$type->getID()."\" data-cats='".json_encode($type->getCategoriesID())."'";
   	echo ">".$type->getName()."</option>";	
   	$count++;
   }
   ?>
</select>
</div>
<!--
   <div class="form-group">
   <label><?php printMLText("instrumentvar")?>:</label>
   <select class="form-control chzn-select" name="instrumentvars[]" id="cat-selector" data-no_results_text="<?php printMLText('unknown_document_instrumentvar'); ?>">
   <?php
      $instrumentVars = $dms->getDocumentInstrumentVars();
      $count = 1;
      $default_instrument_var = 0;
      foreach($instrumentVars as $instVar) {
      	if($count==1 && ($default_instrument_var == 0)) $default_instrument_var = $instVar->getID();
      		echo "<option value=\"".$instVar->getID()."\"";
      		echo ">".$instVar->getCode()." - ".$instVar->getName()."</option>";	
      		$count++;
      	}
      ?>
   </select>
     </div>
   -->
<div class="form-group">
<label><?php printMLText("sequence");?>:</label>
<?php $this->printSequenceChooser($folder->getDocuments('s')); if($orderby != 's') echo "<br />".getMLText('order_by_sequence_off'); ?>
</div>
<div class="form-group">
<?php if($user->isAdmin()) { ?>
<label><?php printMLText("owner");?>:</label>
<select class="chzn-select form-control" name="ownerid">
<?php
   $allUsers = $dms->getAllUsers('', true);
   foreach ($allUsers as $currUser) {
   	if ($currUser->isGuest())
   		continue;
   	print "<option value=\"".$currUser->getID()."\" ".($currUser->getID()==$user->getID() ? 'selected' : '')." data-subtitle=\"".htmlspecialchars($currUser->getFullName())."\"";
   	print ">" . htmlspecialchars($currUser->getLogin()) . "</option>\n";
   }
   ?>
</select>
<?php } ?>
</div>
<?php
   $attrdefs = $dms->getAllAttributeDefinitions(array(SeedDMS_Core_AttributeDefinition::objtype_document, SeedDMS_Core_AttributeDefinition::objtype_all));
   if($attrdefs) {
   	foreach($attrdefs as $attrdef) {
   		if(in_array($default_category, $attrdef->getCategoriesID())) {
   			$display = "block";
   		} else {
   			$display = "none";
   		}
   		
   		echo "<div style='display:$display;' class='form-group' id='attr-group_".$attrdef->getID()."' data-cats='".json_encode($attrdef->getCategoriesID())."'>";
   		$arr = $this->callHook('editDocumentAttribute', null, $attrdef);
   		if(is_array($arr)) {
   			echo "<label>".$arr[0].":</label>";
   			echo $arr[1];
   		} else {
   			?>
<label><?php echo htmlspecialchars($attrdef->getName()); ?></label>
<?php $this->printAttributeEditField($attrdef, ''); ?>
<?php
   }
   echo "</div>";
   } // foreach
   }
   ?>
<div class="form-group">
<label><?php printMLText("expires");?>: <span class="is-required">*</span></label>
<div class="input-append date span12" id="expirationdate" data-date="" data-date-format="yyyy-mm-dd" data-date-language="<?php echo str_replace('_', '-', $this->params['session']->getLanguage()); ?>" data-checkbox="#expires">
<input class="form-control" size="16" name="expdate" id="expdate" type="text" value="">
<input name="presetexpdate" id="presetexpdate" type="hidden" value="date">
<span class="add-on"></span>
</div>
<div class="checkbox">
<label>
<input type="checkbox" id="expires" name="expires" value="1" checked="true"><?php printMLText("does_not_expire");?>
</label>
</div>
</div>
<div class="box-footer">
<a type="button" class="btn btn-default cancel-add-document"><?php echo getMLText("cancel"); ?></a>
<a id="btn-next-1" href="#tab_2" data-toggle="tab" type="button" class="btn btn-info pull-right"><?php echo getMLText("next"); ?> <i class="fa fa-arrow-right"></i></a>
</div>
</div>
<!-- /.tab-pane 1 -->
<div class="tab-pane" id="tab_2">
<div class="form-group">
<label><?php printMLText("version");?>:</label>
<input type="text" class="form-control" name="reqversion" value="1">
</div>
<?php $msg = getMLText("max_upload_size").": ".ini_get( "upload_max_filesize"); ?>
<?php $this->warningMsg($msg); ?>
<div class="form-group">
<label><?php printMLText("local_file");?>: <span class="is-required">*</span></label>
<?php
   $this->printFileChooser('userfile[]', false);
   ?>
</div>
<br>
<div id="file_scan" class="col-md-12" style="display: none; height: 115px;">
<p><b><?php printMLText("checking_file"); ?></b></p>
<div class="overlay" style="height: 50px;"><i class="fa fa-refresh fa-spin"></i></div>
</div>
<div id="file_scan_response_one" class="col-md-12" style="display: none; height: 115px;">
<div class="alert alert-warning">	                
<h4><i class="icon fa fa-warning"></i> <?php echo getMLText("sensitive_words_warning"); ?></h4>
<p><?php echo getMLText("sensitive_words_msg_one"); ?><b><span id="file_scan_response"></span></b></p>
</div>	      	 		
</div>
<div id="file_scan_response_two" class="col-md-12" style="display: none; height: 115px;">
<div class="alert alert-info">	                
<h4><i class="icon fa fa-info"></i> <?php echo getMLText("sensitive_words_info"); ?></h4>
<p><?php echo getMLText("sensitive_words_msg_two"); ?></p>
</div>	      	 		
</div>
<div id="file_scan_response_three" class="col-md-12" style="display: none; height: 115px;">
<div class="alert alert-info">	                
<h4><i class="icon fa fa-info"></i> <?php echo getMLText("sensitive_words_info"); ?></h4>
<p><?php echo getMLText("sensitive_words_msg_three"); ?></p>
</div>	      	 		
</div>
<div class="form-group">
<label><?php printMLText("comment_for_current_version");?>:</label>
<textarea class="form-control" name="version_comment" rows="3" cols="80"></textarea>
<div class="checkbox">
<label><input type="checkbox" name="use_comment" value="1" /> <?php printMLText("use_comment_of_document"); ?></label>
</div>
</div>
<?php
   $attrdefs = $dms->getAllAttributeDefinitions(array(SeedDMS_Core_AttributeDefinition::objtype_documentcontent, SeedDMS_Core_AttributeDefinition::objtype_all));
   	if($attrdefs) {
   		foreach($attrdefs as $attrdef) {
   			$arr = $this->callHook('editDocumentAttribute', null, $attrdef);
   			if(is_array($arr)) {
   				echo "<label>".$arr[0].":</label>";
   				echo $arr[1];
   			} else {
   		?>
<label><?php echo htmlspecialchars($attrdef->getName()); ?></label>
<?php $this->printAttributeEditField($attrdef, '', 'attributes_version') ?>
<?php
   }
   }
   }
   if($workflowmode == 'advanced') { ?>
<div class="form-group">
<label><?php printMLText("workflow");?>:</label>
<?php
   $mandatoryworkflows = $user->getMandatoryWorkflows();
   if($mandatoryworkflows) {
   	if(count($mandatoryworkflows) == 1) { ?>
<?php echo htmlspecialchars($mandatoryworkflows[0]->getName()); ?>
<input type="hidden" name="workflow" value="<?php echo $mandatoryworkflows[0]->getID(); ?>">
<?php
   } else { ?>
<select class="_chzn-select-deselect span9 form-control" name="workflow" data-placeholder="<?php printMLText('select_workflow'); ?>">
<?php
   foreach ($mandatoryworkflows as $workflow) {
   	print "<option value=\"".$workflow->getID()."\"";
   	print ">". htmlspecialchars($workflow->getName())."</option>";
   } ?>
</select>
<?php
   }
   } else { ?>
<select class="_chzn-select-deselect span9 form-control" name="workflow" data-placeholder="<?php printMLText('select_workflow'); ?>">
<?php
   $workflows=$dms->getAllWorkflows();
   print "<option value=\"\">"."</option>";
   foreach ($workflows as $workflow) {
   	print "<option value=\"".$workflow->getID()."\"";
   	print ">". htmlspecialchars($workflow->getName())."</option>";
   } ?>
</select>
<?php } ?>
<br/>
<?php //$this->infoMsg(getMLText("add_doc_workflow_warning")); ?>
<?php } else { echo $this->warningMsg("This theme only works with advanced workflows"); }?>
</div>
<div class="box-footer">
<a type="button" class="btn btn-default cancel-add-document"><?php echo getMLText("cancel"); ?></a>
<a id="btn-next-2" href="#tab_3" data-toggle="tab" aria-expanded="true" type="button" class="btn btn-info pull-right"><?php echo getMLText("next"); ?> <i class="fa fa-arrow-right"></i></a>
</div>
</div>
<!-- /.tab-pane 2 -->







<div class="tab-pane" id="tab_3">

<div class="row">
  <div class="col-md-12 col-xs-12">
    <?php $this->startBoxSolidPrimary(getMLText("attachments_added")); ?>
      <div class="col-md-12"> 
         <a id="create-file-input"  class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo getMLText("add"); ?></a>
         <input type="hidden" name="attach-counter" id="attach-counter">
      </div>
      <br>
      <br>
      <div class="col-md-12"> 
        <div id="attach_file_scan" class="col-md-12" style="display: none; height: 115px;">
        <p><b><?php printMLText("checking_file"); ?></b></p>
        <div class="overlay" style="height: 50px;"><i class="fa fa-refresh fa-spin"></i></div>
        </div>
        <div id="attach_file_scan_response_one" class="col-md-12" style="display: none; height: 115px;">
        <div class="alert alert-warning">                 
        <h4><i class="icon fa fa-warning"></i> <?php echo getMLText("sensitive_words_warning"); ?></h4>
        <p><?php echo getMLText("sensitive_words_msg_one"); ?><b><span id="attach_file_scan_response"></span></b></p>
        </div>              
        </div>
        <div id="attach_file_scan_response_two" class="col-md-12" style="display: none; height: 115px;">
        <div class="alert alert-info">                  
        <h4><i class="icon fa fa-info"></i> <?php echo getMLText("sensitive_words_info"); ?></h4>
        <p><?php echo getMLText("sensitive_words_msg_two"); ?></p>
        </div>              
        </div>
        <div id="attach_file_scan_response_three" class="col-md-12" style="display: none; height: 115px;">
        <div class="alert alert-info">                  
        <h4><i class="icon fa fa-info"></i> <?php echo getMLText("sensitive_words_info"); ?></h4>
        <p><?php echo getMLText("sensitive_words_msg_three"); ?></p>
        </div>              
        </div>
      </div>
      <br>
      <br>
      <div class="col-md-12 col-sm-12" id="files-div"> 
        <table class="table-condensed" id="targettable">
          <thead>
            <tr width="100%">
            <th width="33%" class="align-center"><?php printMLText("file"); ?></th>
            <th width="33%" class="align-center"><?php printMLText("name"); ?></th>
            <th width="33%" class="align-center"><?php printMLText("comment"); ?></th>           
            <th width="33%" class="align-center"><?php printMLText("public"); ?></th>           
            <th width="33%" class="align-center"></th>            
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    <?php $this->endsBoxSolidPrimary(); ?>
  </div>

</div>

<div class="box-footer">
<a type="button" class="btn btn-default cancel-add-document"><?php echo getMLText("cancel"); ?></a>
<a id="btn-next-3" href="#tab_4" data-toggle="tab" aria-expanded="true" type="button" class="btn btn-info pull-right"><?php echo getMLText("next"); ?> <i class="fa fa-arrow-right"></i></a>
</div>



</div>
<!-- /.tab-pane 3 -->
<div class="tab-pane" id="tab_4">
<div class="row">
  <div class="col-md-12 col-xs-12">
  <?php $this->startBoxSolidPrimary(getMLText("linked_documents")); ?>
  <div class="col-md-6">
    <table class="table-condensed">
      <tr>
      <td><?php printMLText("add_document_link");?> 1:</td>
      <td><?php $this->printDocumentChooser("form1");?></td>
      </tr>
      <tr>
        <td><?php printMLText("document_link_public"); ?></td>
        <td><input type="checkbox" name="public_form_1" value="true" /></td>
      </tr>
      <tr>
      <td><?php printMLText("add_document_link");?> 2:</td>
      <td><?php $this->printDocumentChooser("form2");?></td>
      </tr>
      <tr>
        <td><?php printMLText("document_link_public"); ?></td>
        <td><input type="checkbox" name="public_form_2" value="true" /></td>
      </tr>
      <tr>
      <td><?php printMLText("add_document_link");?> 3:</td>
      <td><?php $this->printDocumentChooser("form3");?></td>
      </tr>
      <tr>
        <td><?php printMLText("document_link_public"); ?></td>
        <td><input type="checkbox" name="public_form_3" value="true" /></td>
      </tr>
    </table>
  </div>
  <div class="col-md-6">
    <table class="table-condensed">
      <tr>
      <td><?php printMLText("add_document_link");?> 4:</td>
      <td><?php $this->printDocumentChooser("form4");?></td>
      </tr>
      <tr>
        <td><?php printMLText("document_link_public"); ?></td>
        <td><input type="checkbox" name="public_form_4" value="true" /></td>
      </tr>
      <tr>
      <td><?php printMLText("add_document_link");?> 5:</td>
      <td><?php $this->printDocumentChooser("form5");?></td>
      </tr>
      <tr>
        <td><?php printMLText("document_link_public"); ?></td>
        <td><input type="checkbox" name="public_form_5" value="true" /></td>
      </tr>
      <tr>
      <td><?php printMLText("add_document_link");?> 6:</td>
      <td><?php $this->printDocumentChooser("form6");?></td>
      </tr>
      <tr>
        <td><?php printMLText("document_link_public"); ?></td>
        <td><input type="checkbox" name="public_form_6" value="true" /></td>
      </tr>
    </table>
  </div>
  <?php $this->endsBoxSolidPrimary(); ?>
  </div>
</div>

<div class="box-footer">
<a type="button" class="btn btn-default cancel-add-document"><?php echo getMLText("cancel"); ?></a>
<a id="btn-next-4" href="#tab_5" data-toggle="tab" aria-expanded="true" type="button" class="btn btn-info pull-right"><?php echo getMLText("next"); ?> <i class="fa fa-arrow-right"></i></a>
</div>
</div>





<!-- /.tab-pane 4 -->
<div class="tab-pane" id="tab_5">
<div class="form-group">
<label><?php printMLText("individuals");?>:</label>
<select class="chzn-select span9 form-control" name="notification_users[]" multiple="multiple"">
<?php
   $allUsers = $dms->getAllUsers("", true);
   foreach ($allUsers as $userObj) {
   	// The user must have permissions on the folder - Filter removed
   	//if (!$userObj->isGuest() && $folder->getAccessMode($userObj) >= M_READ)
   		print "<option value=\"".$userObj->getID()."\">" . htmlspecialchars($userObj->getLogin() . " - " . $userObj->getFullName()) . "\n";
   }
   ?>
</select>
</div>
<div class="form-group">
<label><?php printMLText("groups");?>:</label>
<select class="chzn-select span9" name="notification_groups[]" multiple="multiple">
<?php
   $allGroups = $dms->getAllGroups();
   foreach ($allGroups as $groupObj) {
   	// The group must have permissions on the folder - Filter removed
   	//if ($folder->getGroupAccessMode($groupObj) >= M_READ)
   		print "<option value=\"".$groupObj->getID()."\">" . htmlspecialchars($groupObj->getName()) . "\n";
   }
   ?>
</select>
</div>
<div class="box-footer">
<a type="button" class="btn btn-default cancel-add-document"><?php echo getMLText("cancel"); ?></a>
<button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> <?php echo getMLText("save"); ?></button>
</div>	
</div>
<!-- /.tab-pane 5 -->
</div>
<!-- /.tab-content -->
</div>
</form>
<?php
   echo "</div>";
   echo "<!-- /.box-body -->";
   echo "</div>";
   echo "</div>";
   //// Folder content ////
   $subFolders = $folder->getSubFolders($orderby);
   //$subFolders = $folder->getSubFolders("l", "none", 0, 0);

   $subFolders = SeedDMS_Core_DMS::filterAccess($subFolders, $user, M_READ);
   $documents = $folder->getDocuments($orderby);
   $documents = SeedDMS_Core_DMS::filterAccess($documents, $user, M_READ);
   
   if ((count($subFolders) > 0)||(count($documents) > 0)){
   echo "<div class=\"col-md-12\" id=\"folder-content\">";
   echo "<div class=\"box box-primary\">";
    echo "<div class=\"box-body no-padding\">";
   echo "<div class=\"table-responsive\">";
   $txt = $this->callHook('folderListHeader', $folder, $orderby);
   if(is_string($txt))
   echo $txt;
   else {
   print "<table id=\"viewfolder-table\" class=\"table table-hover table-striped table-condensed\">";
   print "<thead>\n<tr>\n";
   print "<th></th>\n";	
   print "<th>".getMLText("name")."</th>\n";
   print "<th>ID</th>\n";
   //			print "<th>".getMLText("owner")."</th>\n";
   print "<th>".getMLText("status")."</th>\n";
   //			print "<th>".getMLText("version")."</th>\n";
   print "<th>".getMLText("action")."</th>\n";
   print "</tr>\n</thead>\n<tbody>\n";
   }
   
   foreach($subFolders as $subFolder) {
   $txt = $this->callHook('folderListItem', $subFolder);
   if(is_string($txt))
   echo $txt;
   else {
   $folder_owner = $subFolder->getOwner();
   
   //if ($user->getID() == $folder_owner->getID() || $user->isAdmin() || $subFolder->getAccessMode($user) >= M_READ){ // OLD <---

   // SHOW SUB-FOLDERS
   if ($user->getID() == $folder_owner->getID() || $user->isAdmin() || $subFolder->getAccessMode($user) >= M_READ) {
   	echo $this->folderListRow($subFolder);
   	$formFol = "formFol".$subFolder->getID();
   	?>


<tr id="table-move-folder-<?php echo $subFolder->getID(); ?>" class="table-row-folder odd tr-border" style="display:none;">
   <td>
      <form action="../op/op.MoveFolder.php" name="<?php echo $formFol; ?>">
         <input type="hidden" name="folderid" value="<?php print $subFolder->getID();?>">
   </td>
   <td>
   <div>
   <label><?php printMLText("choose_target_folder");?>:</label>
   <?php $this->printFolderChooserHtml($formFol, M_READWRITE, $subFolder->getID(), null);?>
   </div>
   </td>
   <td>
   <a class="btn btn-default cancel-folder-mv" rel="<?php echo $subFolder->getID(); ?>"><?php echo getMLText("cancel"); ?></a>
   <input class="btn btn-success" type="submit" value="<?php printMLText("move"); ?>">
   </td>
   <td></td>
   <td></td>
   </form>
</tr>

<?php
   }

   }
   }

   foreach($documents as $document) {
   $document->verifyLastestContentExpriry();
   $txt = $this->callHook('documentListItem', $document, $previewer);
   if(is_string($txt))
   echo $txt;
   else {
   $document_owner = $document->getOwner();
   
   //$logged_user_groups = $user->getGroups();
   //$owner_user_groups = $document_owner->getGroups();
   $logged_groups = array();
   $owner_groups = array();
   
   /*if (false !== $logged_user_groups && false !== $owner_user_groups) {
   	foreach ($logged_user_groups as $logged_group) {				
   		$logged_groups[] = $logged_group->getID();
   	}
   
   	foreach ($owner_user_groups as $owner_group) {						
   		$owner_groups[] = $owner_group->getID();
   	}
   
   }*/
   

    $legal_partners = $dms->getGroup(164); // 164 = Colaboradores Jurídicos
    $secretaries = $dms->getGroup(165); // 165 = Secretarias
    $user_is_lp = $legal_partners->isMember($user);
    $user_is_secretary = $secretaries->isMember($user);


    // Find if logged in user is in the same group of document's owner groups
    //$user_is_in_group = array_intersect($owner_groups,$logged_groups);
   
    //if ($user->getID() === $document_owner->getID() || $user->isAdmin() || !empty($user_is_in_group)){ // OLD <---
   	//if ($user->getID() == $document_owner->getID() || $user->isAdmin() || $document->getAccessMode($user) >= M_READ){
    //
    // SHOW DOCUMENTS IN FOLDER
   	if ($user->getID() == $document_owner->getID() || $user->isAdmin() || $document->getAccessMode($user) >= M_READ || $user_is_lp || $user_is_secretary) {
     	echo $this->documentListRow($document, $previewer);
     	$formDoc = "formDoc".$document->getID();
   	?>

<tr id="table-move-document-<?php echo $document->getID(); ?>" class="table-row-document odd tr-border" style="display:none;">
   <td>
      <form action="../op/op.MoveDocument.php" name="<?php echo $formDoc; ?>">
         <input type="hidden" name="documentid" value="<?php print $document->getID();?>">
   </td>
   <td>
   <div>
   <label><?php printMLText("choose_target_folder");?>:</label>
   <?php $this->printFolderChooserHtml($formDoc, M_READWRITE, -1);?>
   </div>
   </td>
   <td>
   <a class="btn btn-default cancel-doc-mv" rel="<?php echo $document->getID(); ?>"><?php echo getMLText("cancel"); ?></a>
   <input class="btn btn-success" type="submit" value="<?php printMLText("move"); ?>">
   </td>
   <td></td>
   <td></td>
   </form>
   </td>
</tr>
<?php
   }
   
   
   
   }
   }
   if ((count($subFolders) > 0)||(count($documents) > 0)) {
   $txt = $this->callHook('folderListFooter', $folder);
   if(is_string($txt))
   echo $txt;
   else
   echo "</tbody>\n</table>\n";
   }
   echo "</div>";
   echo "</div>";
   echo "</div>";
   echo "</div>"; 
   } else {
   echo "<div class=\"col-md-12\">";
   $this->infoMsg(getMLText("empty_folder_list"));
   echo "</div>";
   }
   //// Document preview ////
   echo "<div class=\"col-md-12 div-hidden\" id=\"document-previewer\">";
   echo "<div class=\"box box-info\">";
   echo "<div class=\"box-header with-border box-header-doc-preview\">";
   echo "<span id=\"doc-title\" class=\"box-title\"></span>";
   echo "<span class=\"pull-right\">";
   //echo "<a class=\"btn btn-sm btn-primary\"><i class=\"fa fa-chevron-left\"></i></a>";
   //echo "<a class=\"btn btn-sm btn-primary\"><i class=\"fa fa-chevron-right\"></i></a>";
   echo "<a class=\"close-doc-preview btn btn-box-tool\"><i class=\"fa fa-times\"></i></a>";
   echo "</span>";
   echo "</div>";
   echo "<div class=\"box-body\">";
   echo "<iframe id=\"iframe-charger\" src=\"\" width=\"100%\" height=\"700px\"></iframe>";
   echo "</div>";
   echo "</div>";
   echo "</div>"; // End document preview
   ?>
<?php
   echo "</div>\n"; // End of row
   echo "</div>\n"; // End of container
   
   echo $this->callHook('postContent');
   
   $this->contentEnd();
   $this->mainFooter();		
   $this->containerEnd();
   $this->htmlEndPage();
   } /* }}} */
   }
   
   ?>