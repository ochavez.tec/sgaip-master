<?php
/**
 * Implementation of ManualVariables view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_ManualVariables extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		if(isset($this->params['seleval']))
			$currEval = $this->params['seleval'];
		
		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		//$this->printFolderChooserJs("form".($currEval ? $currEval->getDate() : '0'));
?>

$( "#get-manuals" ).click(function() {
	selected_axe = $("#selector_one option:selected").val();
	selected_inst = $("#selector_two option:selected").val();
	period = $("#period").val();

	if(selected_axe == -1 || selected_inst == -1 || period == ""){
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#ajax2').trigger('update', {selected_inst: selected_inst, selected_axe: selected_axe, period: period });
	}

	
});

$( "#selector_one" ).change(function() {
	selected_axe = $(this).val();
	if(selected_axe == -1) {
		noty({
			text: "<?php echo getMLText('no_values_selected'); ?>",
			type: "error",
			dismissQueue: true,
			layout: "topRight",
			theme: "defaultTheme",
			timeout: 3500,
		});
	} else {
		$('#ajax1').trigger('update', { selected_axe: selected_axe });
	}
});

$(document).ready( function() {

	// For tables
	$(".vars-table").DataTable({
	    "paging": false,
	    "lengthChange": false,
	    "searching": true,
	    "ordering": false,
	    "info": false,
	    "autoWidth": true,
	});

	$('.date').datepicker({
		language: 'es',
		format: 'mm-yyyy',
		autoclose: true,
		minViewMode: 1,
	});

	
});

<?php
	} /* }}} */

	function instruments(){
		$dms = $this->params['dms'];
		$instruments = $this->params['instruments'];

		if (!empty($instruments)) {
			echo '<br/>';
			echo '<label class="control-label">'.getMLText("choose_instrument").':</label>';
			echo '<div class="controls">';
			echo '<select class="chzn-select" id="selector_two">';
			echo '<option value="-1">'.getMLText("choose_instrument").'</option>';
		
			if (!empty($instruments)) {
				foreach ($instruments as $instrument) {
					$inst = $dms->getInstrument($instrument['instrumentID']);
					print "<option value=\"".$inst->getID()."\" >" . htmlspecialchars($inst->getName()) . "</option>";
				}
			}

			echo '</select>';
			echo '</div>';
		}
		
	}

	function variables(){
		$dms = $this->params['dms'];
		$variables = $this->params['variables'];
		$axe = $this->params['axe'];
		$instrument = $this->params['instrument'];
		$period = $this->params['period'];

		if (!empty($variables)) {
			echo '<br/>';
			echo '<div class="box box-info">';					
			echo '<div class="box-body">';
			echo '<div class="table-responsive">';
            echo '<table class="vars-table table table-bordered table-striped">';
            echo '<thead>';
            echo '<tr>';
            echo '<th class="align-center">'.getMLText('name').'</th>';
            echo '<th class="align-center">'.getMLText('score').'</th>';
            echo '<th class="align-center">'.getMLText('actions').'</th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            foreach ($variables as $var) {
            	if ($var->getIsManual() == '1') {
            		echo '<tr>';
	                echo '<td>';
	                echo $var->getCode()." - ".$var->getName();
	                echo '</td>';
	                echo '<td class="align-center">';
	                echo $var->getWeight();
	                echo '</td>';
	                echo '<td class="align-center">';
	                $accomplish = $dms->getManualVariableEval($axe, $instrument, $var->getID(), $period);
	                
	                if(!empty($accomplish) && !is_bool($accomplish)){
	                	if ($accomplish[0]['accomplish'] === "1") {
	                		echo '<a data-axe="'.$axe.'" data-inst="'.$instrument.'" data-var="'.$var->getID().'" data-value="1" type="button" class="variable-action btn btn-xs btn-success"><i class="fa fa-check fa-1x"></i></a>';	
	                	} else {
	                		echo '<a data-axe="'.$axe.'" data-inst="'.$instrument.'" data-var="'.$var->getID().'" data-value="0" type="button" class="variable-action btn btn-xs btn-danger"><i class="fa fa-check fa-1x"></i></a>';	
	                	}
	                	
	                } else {
	                	echo '<a data-axe="'.$axe.'" data-inst="'.$instrument.'" data-var="'.$var->getID().'" data-value="0" type="button" class="variable-action btn btn-xs btn-danger"><i class="fa fa-check fa-1x"></i></a>';
	                }

	                echo '</td>';
	                echo '</tr>';
            	}
            	
            }

            echo '</tbody>';
            echo '</table>';
            echo '</div>';
            echo '</div>';
            echo '</div>';

		}

		echo '<script>';
        echo '$(function () {      	
            	$(".variable-action").on("click", function(){
            		var period = $("#period").val();
            		var temp = $(this).attr("data-value");
            		
            		if (temp == "1"){
            			$(this).removeClass( "btn-success" ).addClass( "btn-danger" );
            			$(this).attr("data-value","0");
            		} else if(temp == "0") {
            			$(this).removeClass( "btn-danger" ).addClass( "btn-success" );
            			$(this).attr("data-value","1");
            		}
            
            		var manuals_array = new Array();
            		jQuery.each( $(this), function( elem, val ) {
            			var values = new Array();
            			jQuery.each( val.attributes, function( el, value ) {            				
            				var thevalue = value.value;
  							values.push(thevalue);
            			});

            			manuals_array.push(values);
            		});

            		console.log(period);
            		console.log(manuals_array);

            		formtoken = "'.createFormKey("addmanualvariables").'";
  					$.get("/op/op.ManualVariables.php",
					{ action: "addmanualvariables", vars: manuals_array, period: period, formtoken: formtoken })
					.always(function(data) {
						
						if(data) {							
							noty({
								text: "'.getMLText('data_saved').'",
								type: "success",
								dismissQueue: true,
								layout: "topRight",
								theme: "defaultTheme",
								timeout: 1500,
							});
						} else {
							noty({
								text: "'.getMLText('splash_error').'",
								type: "error",
								dismissQueue: true,
								layout: "topRight",
								theme: "defaultTheme",
								timeout: 3500,
							});
						}
					});

            		
            		
            	});
            });';
        echo '</script>';
	}

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$axes = $this->params['axes'];

		$httproot = $this->params['httproot'];

		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datepicker/datepicker3.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/bootstrap-datepicker.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/chartjs/Chart.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');
		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("instrumentvars"));

?>

<div class="row-fluid">

<div class="col-md-8">
<div class="well">

<div class="control-group">
<label class="control-label"><?php printMLText("choose_axe");?>:</label>
<div class="controls">
<select class="chzn-select" id="selector_one">
<option value="-1"><?php echo getMLText("choose_axe")?></option>
<?php
	if (!empty($axes)) {
		foreach ($axes as $axe) {
			print "<option value=\"".$axe->getID()."\" >" . htmlspecialchars($axe->getName()) . "</option>";
		}
	}
?>
</select>
</div>
</div>

<div class="control-group">
<div id="ajax1" class="ajax" data-view="ManualVariables" data-action="instruments">
</div>
</div>
</div>
</div>

<div class="col-md-4">
<div class="well">
<div class="form-group">
<label class="control-label" for="login"><?php printMLText("select_period");?>:</label>
<div class="input-group">
<div class="input-group-addon">
<i class="fa fa-calendar"></i>
</div>
<input type="text" id="period" name="date" value="" class="form-control date" required="required" autocomplete="off" placeholder="mm-yyyy" /> 
</div>
</div>
</div>
</div>

<div class="col-md-12 align-center">
	<a type="button" id="get-manuals" class="btn btn-info"><i class="fa fa-refresh"></i> <?php echo getMLText("get_variables") ?></a>
</div>
</div>


<div class="row-fluid">
<div class="col-md-12">
	<div id="ajax2" class="ajax" data-view="ManualVariables" data-action="variables">
	</div>
</div>
</div>



<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
