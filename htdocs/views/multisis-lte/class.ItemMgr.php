<?php
/**
 * Implementation of ItemMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_ItemMgr extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$strictformcheck = $this->params['strictformcheck'];
		header('Content-Type: application/javascript');
?>

function checkForm()
{
	msg = new Array();

	if($("#code").val() == "") msg.push("<?php printMLText("js_no_code");?>");
	if($("#name").val() == "") msg.push("<?php printMLText("js_no_name");?>");
	if($("#weight").val() == "") msg.push("<?php printMLText("js_no_weight");?>");
//	if($("#instrumentid").val() == "") msg.push("<?php printMLText("js_no_instrumentid");?>");	
<?php
	if ($strictformcheck) {
?>
	if($("#comment").val() == "") msg.push("<?php printMLText("js_no_comment");?>");
<?php
	}
?>
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$(document).ready( function() {
	$('body').on('submit', '#form', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});
	
	$('.input[type="checkbox"]').iCheck();
	
});
<?php
} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$httproot = $this->params['httproot'];
		$thematic = $this->params['thematic'];

		if(isset($this->params['item']) && !empty($this->params['item'])){
			$item = $this->params['item'];
			$is_item = true;
		} else {
			$item = null;
			$is_item = false;
		}

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("mgr_items"));
?>

<div class="row-fluid">
<div class="col-md-12">
	<form action="../op/op.ItemMgr.php" method="post" id="form1">
	<input type="hidden" name="thematic_id" value="<?php echo $thematic[0]['id']; ?>">
		<?php if ($is_item) { 
			echo '<input type="hidden" name="action" value="edititem">';
			echo '<input type="hidden" name="item_id" value="'.$item[0]['id'].'">';
			echo createHiddenFieldWithKey('edititem');
		} else {
			echo '<input type="hidden" name="action" value="additem">';
			echo createHiddenFieldWithKey('additem');
		} ?>
<div class="table-responsive">
	<table class="table-condensed">
		<tr>
			<td><?php printMLText("name");?>:</td>
			<td><input type="text" class="form-control" name="name" id="name" value="<?php print $is_item ? htmlspecialchars($item[0]['name']) : '';?>"  required="required"></td>
		</tr>
		<tr>
			<td><?php printMLText("comment");?>:</td>
			<td><textarea name="comment" class="form-control" id="comment" rows="4" cols="80" required="required"><?php print $is_item ? htmlspecialchars($item[0]['comment']) : "";?></textarea></td>
		</tr>
		<tr>
			<td><?php printMLText("is_qualification_type_manual");?>:</td>
			<td><input type="checkbox" name="qualification_type" value="1" <?php echo ($is_item && (int)$item[0]['qualification_type']) ? "checked" : "" ?>></td>
		</tr>
		<tr>
			<td><?php printMLText("is_evaluated");?>:</td>
			<td><input type="checkbox" name="evaluation_type" value="1" <?php echo ($is_item && (int)$item[0]['evaluation_type']) ? "checked" : "" ?>></td>
		</tr>
		<tr>
			<td>Meses evaluación:</td>
			<td>
				<div class="col-md-3">
					Ene
					<input class="form-control" id="evalJan" name="evalJan" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalJan']."\""; 
					?> required="required">
				</div>
				<div class="col-md-3">
					Feb
					<input class="form-control" id="evalFeb" name="evalFeb" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalFeb']."\""; 
					?> required="required">
				</div>
				<div class="col-md-3">
					Mar
					<input class="form-control" id="evalMar" name="evalMar" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalMar']."\""; 
					?> required="required">
				</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<div class="col-md-3">
					Abr
					<input class="form-control" id="evalApr" name="evalApr" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalApr']."\""; 
					?> required="required">
				</div>
				<div class="col-md-3">
					May
					<input class="form-control" id="evalMay" name="evalMay" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalMay']."\""; 
					?> required="required">
				</div>
				<div class="col-md-3">
					Jun
					<input class="form-control" id="evalJun" name="evalJun" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalJun']."\""; 
					?> required="required">
				</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<div class="col-md-3">
					Jul
					<input class="form-control" id="evalJul" name="evalJul" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalJul']."\""; 
					?> required="required">
				</div>
				<div class="col-md-3">
					Ago
					<input class="form-control" id="evalAug" name="evalAug" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalAug']."\""; 
					?> required="required">
				</div>
				<div class="col-md-3">
					Sep
					<input class="form-control" id="evalSep" name="evalSep" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalSep']."\""; 
					?> required="required">
				</div>
			</td>
		</tr>
			<td></td>
			<td>
				<div class="col-md-3">
					Oct
					<input class="form-control" id="evalOct" name="evalOct" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalOct']."\""; 
					?> required="required">
				</div>
				<div class="col-md-3">
					Nov
					<input class="form-control" id="evalNov" name="evalNov" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalNov']."\""; 
					?> required="required">
				</div>
				<div class="col-md-3">
					Dic
					<input class="form-control" id="evalDec" name="evalDec" type="number" min="0" data-toggle="toggle" 
					<?php if($is_item) 
							echo " value=\"".$item[0]['evalDec']."\""; 
					?> required="required">
				</div>
			</td>
		</tr>

		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><a href="<?php echo '/out/out.Inspections.php'; ?>" class="btn btn-default"><?php echo getMLText('cancel'); ?></a></td>
			<td><button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText($is_item ? "save" : "add_item")?></button></td>
		</tr>
		</table>
		</div>
</form>
</div>
</div>

<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
