<?php
/**
 * Implementation of InspectionReport view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for InstrumentMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_InspectionReport extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		if(isset($this->params['seleval']))
			$currEval = $this->params['seleval'];
		
		$strictformcheck = $this->params['strictformcheck'];

		header('Content-Type: application/javascript');
		//$this->printFolderChooserJs("form".($currEval ? $currEval->getDate() : '0'));
?>



function checkForm() {
	msg = new Array();
	if($("#inspection-year").val() == "") msg.push("<?php printMLText("js_no_inpection_year");?>");
	if($("#inspection-start").val() == "") msg.push("<?php printMLText("js_no_inpection_start");?>");
	if($("#inspection-end").val() == "") msg.push("<?php printMLText("js_no_inpection_end");?>");
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      	dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$( "#selector" ).change(function() {
	selected_insp = $(this).val();
	$('div.ajax').trigger('update', {selected_inspection: selected_insp});
});

$(document).ready( function() {
	$('body').on('submit', '#form1', function(ev){
		if(!checkForm()) {
			return;
			ev.preventDefault();
		} else {
			$("#box-form1").append("<div class=\"overlay\"><i class=\"fa fa-refresh fa-spin\"></i></div>");
		}
	});

	// For tables
	$(".items-table").DataTable({
	    "paging": false,
	    "lengthChange": false,
	    "searching": true,
	    "ordering": false,
	    "info": false,
	    "autoWidth": true,
	});

	
});

<?php
	} /* }}} */

	function info() { /* {{{ */
		
	} /* }}} */

	function form() { /* {{{ */
		if(isset($this->params['selected_inspection'])){
			$selected_inspection = $this->params['selected_inspection'];
			$inspection = $this->params['inspection'];
			$manuals_accomplish = $this->params['manuals_accomplish'];
		} else {
			$selected_inspection = '';
		}

		if ($selected_inspection >= 1) {
			$this->printInspectionReport($inspection, $manuals_accomplish);
		}

	} /* }}} */

	function printInspectionReport($inspection, $manuals_accomplish = 0){
		$dms = $this->params['dms'];
		$year = $inspection[0]['year'];
		$start = $inspection[0]['duration_start'];
		$end = $inspection[0]['duration_stop'];

		$result = $dms->calculateInspection($year, $start, $end);

		echo '<div class="row">';
		echo '<div class="col-md-8" id="print-area-01">';
		$this->startBoxPrimary(getMLText('accomplish_result'));
		foreach ($result['thematic'] as $key => $thematic) {
			echo '<div class="box box-info">';
			echo '<div class="box-header with-border">';
			echo '<h3 class="box-title">'.$thematic['thematicName'].'</h3>';
			echo '</div>';
			echo '<div class="box-body">';
			echo '<div class="table-responsive">';
            echo '<table class="items-table table table-bordered table-striped">';
            echo '<thead>';
            echo '<tr>';
            echo '<th class="align-center">'.getMLText('name').'</th>';
            echo '<th class="align-center">'.getMLText('document_type').'</th>';
            echo '<th class="align-center">'.getMLText('qualification_type').'</th>';
            echo '<th class="align-center">'.getMLText('evaluation_type').'</th>';
            echo '<th class="align-center">'.getMLText('accomplish').'</th>';
            echo '<th class="align-center">'.getMLText('actions').'</th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            foreach ($thematic['items'] as $indx => $item) {
            	echo '<tr>';
                echo '<td>';
                echo $item['itemName'];
                echo '</td>';
                echo '<td class="align-center">';
                if ($item['itemDocType']) {
                	$type = $dms->getDocumentType($item['itemDocType']);
                	echo $type->getName();
                } else {
                	echo '<p style="color: grey;"><i class="fa fa-minus"></i></p>';	
                }
                                          
                echo '</td>';               
          		echo '<td class="align-center">';
                if ($item['qualificationType'] === '1') {
                	echo '<span style="font-size: 12px;" class="label bg-purple">'.getMLText('manual').'</span>';
                } else {
                	echo '<span style="font-size: 12px;" class="label bg-orange">'.getMLText('automatic').'</span>';
                }
                echo '</td>';
                echo '<td class="align-center">';          
                if ($item['evaluationType'] === '1') {
                	echo '<span style="font-size: 12px;" class="label bg-olive">'.getMLText('evaluated').'</span>';
                } else {
                	echo '<span style="font-size: 12px;" class="label bg-navy">'.getMLText('good_practice').'</span>';
                }
                echo '</td>';
                echo '<td class="align-center">';
                if ((isset($item['accomplished']) && $item['accomplished']) && $item['qualificationType'] === '0') {
                	echo '<p style="color: green;"><i class="fa fa-check"></i></p>';	
                } else if ((isset($item['accomplished']) && !$item['accomplished']) && $item['qualificationType'] === '0') {
                	echo '<p style="color:red;"><i class="fa fa-times"></i></p>';

            	} else if (!isset($item['accomplished']) && $item['evaluationType'] === '1' && $item['qualificationType'] === '1') {
            		$manual_item = $dms->getInspectionManualItem($inspection[0]['id'], $item['itemID']);
            		
            		if (!empty($manual_item) && $manual_item !== false) {
            			if ($manual_item['value'] === '1') {
            				echo '<p id="'.$item['itemID'].'" class="manuals" data-inspectionid="'.$inspection[0]['id'].'" data-value="1" style="color:green;"><i class="fa fa-check"></i></p>';	
            			} else if ($manual_item['value'] == '0') {
            				echo '<p id="'.$item['itemID'].'" class="manuals" data-inspectionid="'.$inspection[0]['id'].'" data-value="0" style="color:red;"><i class="fa fa-times"></i></p>';	
            			}
            		} else {
            			echo '<p id="'.$item['itemID'].'" class="manuals" data-inspectionid="'.$inspection[0]['id'].'" data-value="0" style="color:red;"><i class="fa fa-times"></i></p>';
            		}

                } else if ($item['evaluationType'] === '0') {
                	echo '<p style="color: grey;"><i class="fa fa-minus"></i></p>';	
                } 
                echo '</td>';
                echo '<td class="align-center">';
                if ($item['evaluationType'] === '0' || $item['qualificationType'] === '0') {
                	echo '<p style="color: grey;"><i class="fa fa-minus"></i></p>';
                } else {
                	echo '<a type="button" data-item="'.$item['itemID'].'" class="item-action btn btn-xs btn-success"><i class="fa fa-check fa-1x"></i></a>';
                }	        
                echo '</td>';
                echo '</tr>';
            }

            echo '</tbody>';
            echo '</table>';
            echo '</div>';
            echo '</div>';
            echo '</div>';


		}

		echo '<script>';
        echo '$(function () {
            	
            });';
        echo '</script>';

		$this->endsBoxPrimary();
		echo '</div>';
		echo '<div class="col-md-4">';
		$automatic_and_manuals = (int)$result['global_accomplished_items'] + (int)$manuals_accomplish;
		$accomplished = round(($automatic_and_manuals/(int)$result['evaluations_total']) * 100, 2); 
		$missing = 100 - $accomplished;
		$this->startBoxSuccess(getMLText('charts'));
		echo '<div id="print-area-02">';

		echo "<p><span>".getMLText('inspection_start') .": <b>".$inspection[0]['duration_start']."</b></span>&nbsp;&nbsp;
				<span>".getMLText('inspection_end') .": <b>".$inspection[0]['duration_stop']."</b></span>
				</p>";
		

		echo "<p>".getMLText("programmed_items").": <span data-toggle='tooltip' class='badge bg-yellow'>".$result['evaluations_total']."</span></p>";
		echo "<p>".getMLText("accomplished_items").": <span data-toggle='tooltip' class='badge bg-olive'>".$automatic_and_manuals."</span></p>";
		echo "</div>";
		echo '<canvas id="pieChart" style="height:250px"></canvas>';
		echo '<script>';
		echo "$(function () {
				var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
			    var pieChart       = new Chart(pieChartCanvas)
			    var PieData        = [
			      {
			        value    : ".$missing.",
			        color    : '#f56954',
			        highlight: '#f56954',
			        label    : '".getMLText('missing_items')."'
			      },
			      {
			        value    : ".$accomplished.",
			        color    : '#00a65a',
			        highlight: '#00a65a',
			        label    : '".getMLText('accomplished_items')."'
			      }
			    ]
			    var pieOptions     = {
			      segmentShowStroke    : true,
			      segmentStrokeColor   : '#fff',
			      segmentStrokeWidth   : 2,
			      percentageInnerCutout: 50,
			      animationSteps       : 100,
			      animationEasing      : 'easeOutBounce',
			      animateRotate        : true,
			      animateScale         : false,
			      responsive           : true,
			      maintainAspectRatio  : true,
			    }
			    pieChart.Doughnut(PieData, pieOptions);
			});
			";
		echo '</script>';
		echo '<br/>';
		echo '<br/>';
		echo '<div id="print-area-03">';
		echo '<div class="" style="max-height: 10px;">';
		if ($accomplished <= 60) {
			echo '<div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" style="width:'.$accomplished.'%">';
        	echo '<strong style="color: black; font-size: 16px;">'.$accomplished.'%</strong>';
		} else if ($accomplished >= 61 && $accomplished <= 80) {
			echo '<div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" style="width:'.$accomplished.'%">';
        	echo '<strong style="color: black; font-size: 16px;">'.$accomplished.'%</strong>';
		} else if ($accomplished >= 81 && $accomplished <= 95) {
			echo '<div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" style="width:'.$accomplished.'%">';
        	echo '<strong style="color: black; font-size: 16px;">'.$accomplished.'%</strong>';
		} else if ($accomplished >= 96 && $accomplished <= 100) {
			echo '<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" style="width:'.$accomplished.'%">';
        	echo '<strong style="color: black; font-size: 16px;">'.$accomplished.'%</strong>';
		}
   		
        echo "</div>";
        echo "</div>";
        

		echo "</div>";
		echo '<br/>';
		$this->endsBoxSuccess();
		
   		echo '<br/>';
		$this->startBoxSuccess(getMLText('actions'));
		echo '<div>';
		//echo '<a type="button" class="btn btn-info" id="save-manuals"><i class="fa fa-save"></i> '.getMLText('save').'</a>&nbsp';
		echo '<a type="button" class="btn btn-primary" id="printer"><i class="fa fa-print"></i> '.getMLText('print').'</a>';
		echo '</div>';

		echo '<script>';
        echo '$(function () {
        		
        		$("#printer").on("click", function(){
        			var headstr = "<html><head><title></title></head><body>";
					var footstr = "</body>";
					var newstr1 = document.all.item("print-area-01").innerHTML;
					var newstr2 = document.all.item("print-area-02").innerHTML;
					/*var newstr3 = document.all.item("print-area-03").innerHTML;*/
					var oldstr = document.body.innerHTML;
					document.body.innerHTML = headstr+newstr2+newstr1+footstr;
					window.print();
					document.body.innerHTML = oldstr;
        		});

            	$(".item-action").on("click", function(){
            		var item_id = $(this).attr("data-item");

            		var is_checked = $("#"+item_id).attr("data-value");

            		if(is_checked == "0"){
            			$("#"+item_id).attr("data-value","1");
            			$("#"+item_id).css("color", "green");
						$("#"+item_id).html("<i class=\"fa fa-check\"></i>");
            		} else {
            			$("#"+item_id).attr("data-value","0");
            			$("#"+item_id).css("color", "red")
            			$("#"+item_id).html("<i class=\"fa fa-times\"></i>");
            		}

            		var item = $("#"+item_id);
            		var manuals_array = new Array();

            		jQuery.each( item, function( elem, val ) {
            			var values = new Array();
            			jQuery.each( val.attributes, function( el, value ) {            				
            				var thevalue = value.value;
  							values.push(thevalue);
            			});

            			manuals_array.push(values);
            		});
            		
            		formtoken = "'.createFormKey("addinspectionmanual").'";
  					$.get("/op/op.InspectionReport.php",
					{ action: "addinspectionmanual", items: manuals_array, formtoken: formtoken })
					.always(function(data) {
						
						if(data) {							
							noty({
								text: "'.getMLText('data_saved').'",
								type: "success",
								dismissQueue: true,
								layout: "topRight",
								theme: "defaultTheme",
								timeout: 1500,
							});
						} else {
							noty({
								text: "'.getMLText('splash_error').'",
								type: "error",
								dismissQueue: true,
								layout: "topRight",
								theme: "defaultTheme",
								timeout: 3500,
							});
						}
					});
            	});
            });';
        echo '</script>';

		$this->endsBoxSuccess();
		echo "</div>";
		echo "</div>";
	}

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$inspections = $this->params['inspections'];
		$httproot = $this->params['httproot'];
		if(isset($this->params['selected_inspection']))
			$selected_inspection = $this->params['selected_inspection'];
		else
			$selected_inspection = 0;

		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<link href="../styles/'.$this->theme.'/plugins/datepicker/datepicker3.css" rel="stylesheet">'."\n", 'css');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/bootstrap-datepicker.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/jquery.dataTables.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/datatables/dataTables.bootstrap.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/plugins/chartjs/Chart.min.js"></script>'."\n", 'js');
		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');
		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
	    <div class="gap-10"></div>
	    <div class="row">
	    <div class="col-md-12">
	    <?php 

		$this->startBoxPrimary(getMLText("inspection_report"));
?>

<div class="row-fluid">
<div class="col-md-8">
<div class="well row-fluid">
<form class="form-horizontal">
<div class="control-group">
<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
<div class="controls">
<select class="chzn-select" id="selector">
<option value="-1"><?php echo getMLText("choose_inspection")?></option>
<?php
	if (!empty($inspections)) {
		foreach ($inspections as $inspection) {
			print "<option value=\"".$inspection['id']."\" >" . htmlspecialchars($inspection['year']) . "</option>";
		}
	}
?>
</select>
</div>
</div>
</form>
</div>
</div>

<div class="col-md-4">
</div>

<div class="row-fluid">
<div class="ajax" data-view="InspectionReport" data-action="form" <?php echo ($selected_inspection ? "data-query=\"selected_inspection=".$selected_inspection."\"" : "") ?>>
</div>
</div>

</div>

<?php

	//}
	$this->endsBoxPrimary();

	echo "</div>";
	echo "</div>";
	echo "</div>";
		
    $this->contentEnd();
	$this->mainFooter();		
	$this->containerEnd();
	$this->htmlEndPage();

	} /* }}} */
}
?>
