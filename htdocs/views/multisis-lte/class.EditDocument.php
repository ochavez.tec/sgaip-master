<?php
/**
 * Implementation of EditDocument view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditDocument view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_EditDocument extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$strictformcheck = $this->params['strictformcheck'];
		header('Content-Type: application/javascript');

?>

function checkForm()
{
	msg = new Array();
	if ($("#name").val() == "") msg.push("<?php printMLText("js_no_name");?>");
<?php
	if ($strictformcheck) {
	?>
	if ($("#comment").val() == "") msg.push("<?php printMLText("js_no_comment");?>");
	if ($("#keywords").val() == "") msg.push("<?php printMLText("js_no_keywords");?>");
<?php
	}
?>
	if (msg != "")
	{
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	}
	else
		return true;
}

$(document).ready( function() {
/*
	$('body').on('submit', '#form1', function(ev){
		if(checkForm()) return;
		ev.preventDefault();
	});
*/
	$("#form1").validate({
		invalidHandler: function(e, validator) {
			noty({
				text:  (validator.numberOfInvalids() == 1) ? "<?php printMLText("js_form_error");?>".replace('#', validator.numberOfInvalids()) : "<?php printMLText("js_form_errors");?>".replace('#', validator.numberOfInvalids()),
				type: 'error',
				dismissQueue: true,
				layout: 'topRight',
				theme: 'defaultTheme',
				timeout: 1500,
			});
		},
		messages: {
			name: "<?php printMLText("js_no_name");?>",
			comment: "<?php printMLText("js_no_comment");?>",
			keywords: "<?php printMLText("js_no_keywords");?>"
		}
	});

  /* ---- For category fields ---- */
  $("#cat-selector").on("change", function(){
	 var currentCat = this.value
	 console.log('current_cat: '+currentCat);
	 //attrs = document.querySelectorAll('id^=attr-group_');
	 var attrs = $('[id^=attr-group_]');
//	 console.log(attrs);
	 for(var i = 0, len = attrs.length; i < len; i++) {
		 var cats = attrs[i].getAttribute('data-cats');
	     var catsArr = JSON.parse(cats);
	     var id = attrs[i].getAttribute('id');
	     
	     var found = catsArr.indexOf(currentCat);
//		 console.log(id);	     
//		 console.log(found);

		 if(found < 0) {
			 $("#"+id).hide();
		 } else {
			 $("#"+id).show();
		 }
	 }	 
  });

  	$('#presetexpdate').on('change', function(ev){
		if($(this).val() == 'date')
			$('#control_expdate').show();
		else
			$('#control_expdate').hide();
	});
});
<?php
		$this->printKeywordChooserJs('form1');
		
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$folder = $this->params['folder'];
		$document = $this->params['document'];
		$attrdefs = $this->params['attrdefs'];
		$strictformcheck = $this->params['strictformcheck'];
		$orderby = $this->params['orderby'];

		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');

		$this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))), "skin-blue sidebar-mini");

		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();		

		$user_root_folder_id = $this->params['user']->getHomeFolder();
		$user_root_folder = $this->params['dms']->getFolder($user_root_folder_id);
		echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document);

		//// Document content ////
		echo "<div class=\"row\">";
		echo "<div class=\"col-md-12\">";

		$this->startBoxPrimary(getMLText("edit_document_props"));

		if($document->expires())
			$expdate = date('Y-m-d', $document->getExpires());
		else
			$expdate = '';
?>
<div class="table-responsive">
<form action="../op/op.EditDocument.php" name="form1" id="form1" method="post">
	<input type="hidden" name="documentid" value="<?php echo $document->getID() ?>">
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1">
		<div class="form-group">
			<label><?php printMLText("name");?>:</label>
			<input class="form-control" type="text" name="name" id="name" value="<?php print htmlspecialchars($document->getName());?>" size="60" required>
		</div>
		<div class="form-group">
			<label><?php printMLText("comment");?>:</label>
			<textarea class="form-control" name="comment" id="comment" rows="4" cols="80"<?php echo $strictformcheck ? ' required' : ''; ?>><?php print htmlspecialchars($document->getComment());?></textarea>
		</div>
		<!--	<tr>
		<td valign="top" class=""><?php //printMLText("keywords");?>:</td>
			<td class="standardText"> -->
				<?php
					//$this->printKeywordChooserHtml('form1', $document->getKeywords());
				?>
		<!--	</td>
		</tr> -->
		<div class="form-group">
			<label><?php printMLText("category")?>:</label>
        <select class="chzn-select form-control" name="categories[]" id="cat-selector" data-placeholder="<?php printMLText('select_category'); ?>" data-no_results_text="<?php printMLText('unknown_document_category'); ?>">
<?php
			if($user->isAdmin())
				$categories = $dms->getDocumentCategories();
			else
				$categories = $user->getCategories();
			$count = 1;
			$default_category = 0;
			foreach($categories as $category) {
				if($count==1 && ($default_category == 0)) $default_category = $category->getID();
				echo "<option value=\"".$category->getID()."\"";
				if(in_array($category, $document->getCategories()))
					echo " selected";
				echo ">".$category->getName()."</option>";
				$count++;
			}
?>
				</select>
		</div>
		<div class="form-group">
			<label><?php printMLText("type")?>:</label>
        <select class="chzn-select form-control" name="types[]" id="type-selector" data-placeholder="<?php printMLText('select_type'); ?>" data-no_results_text="<?php printMLText('unknown_document_type'); ?>">
<?php
			$types = $dms->getDocumentTypes();
			$count = 1;
			$default_type = 0;
			foreach($types as $type) {
				if($count==1 && ($default_type == 0)) $default_type = $type->getID();
				echo "<option value=\"".$type->getID()."\"";
				if(in_array($type, $document->getTypes()))
					echo " selected";
				echo ">".$type->getName()."</option>";
				$count++;
			}
?>
				</select>
		</div>
<!--
		<div class="form-group">
			<label><?php printMLText("instrumentvar")?>:</label>
			<select class="chzn-select" name="intrumentvars[]" data-placeholder="<?php printMLText('select_intrumentvar'); ?>" data-no_results_text="<?php printMLText('unknown_document_instrumentvar'); ?>">
<?php
			$instrumentvars = $dms->getDocumentInstrumentVars();
			foreach($instrumentvars as $instrumentvar) {
				echo "<option value=\"".$instrumentvar->getID()."\"";
				echo ">".$instrumentvar->getCode()." - ".$instrumentvar->getName()."</option>";	
			}
?>
			</select>
		</div>
-->
		<div class="form-group">
		
		<label><?php printMLText("expires");?>:</label>
			
		<select class="form-control span3" name="presetexpdate" id="presetexpdate">
			<option value="never"><?php printMLText('does_not_expire');?></option>
			<option value="date"<?php echo ($expdate != '' ? " selected" : ""); ?>><?php printMLText('expire_by_date');?></option>
			<option value="1w"><?php printMLText('expire_in_1w');?></option>
			<option value="1m"><?php printMLText('expire_in_1m');?></option>
			<option value="1y"><?php printMLText('expire_in_1y');?></option>
			<option value="2y"><?php printMLText('expire_in_2y');?></option>
		</select>
			
		
		<div id="control_expdate" <?php echo (!$expdate ? 'style="display: none;"' : ''); ?>>
		<label><?php printMLText("expires");?>:</label>
        <span class="input-append date span6" id="expirationdate" data-date="<?php echo ($expdate ? $expdate : ''); ?>" data-date-format="yyyy-mm-dd" data-date-language="<?php echo str_replace('_', '-', $this->params['session']->getLanguage()); ?>" data-checkbox="#expires">
          <input class="form-control span3" size="16" name="expdate" type="text" value="<?php echo ($expdate ? $expdate : ''); ?>">
          <span class="add-on"><i class="icon-calendar"></i></span>
        </span>
		</div>


		</div>
<?php
		if ($folder->getAccessMode($user) > M_READ) {
			print "<div class='form-group'>";
			print "<label>" . getMLText("sequence") . ":</label>";
			$this->printSequenceChooser($folder->getDocuments('s'), $document->getID());
			if($orderby != 's') echo "<br />".getMLText('order_by_sequence_off'); 
			print "</div>";
		}
		if($attrdefs) {
			foreach($attrdefs as $attrdef) {
				print "<div class='form-group'>";
				if(in_array($default_category, $attrdef->getCategoriesID())) {
					$display = "block";
				} else {
					$display = "none";
				}
				
				echo "<div style='display:$display;' class='form-group' id='attr-group_".$attrdef->getID()."' data-cats='".json_encode($attrdef->getCategoriesID())."'>";
				$arr = $this->callHook('editDocumentAttribute', null, $attrdef);
				if(is_array($arr)) {
					echo "<label>".$arr[0].":</label>";
					echo $arr[1];
				} else {
					?>
					<label><?php echo htmlspecialchars($attrdef->getName()); ?></label>
					<?php 
						$attrValue = $dms->getDocumentAttributeValue($attrdef->getID(), $document->getID());
						$this->printAttributeEditField($attrdef, $attrValue); 
						?>
					<?php
				}
				echo "</div>";
				print "</div>";
			} // foreach
		}
?>
		<div class="form-group">
			<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save")?></button>
		</div>
		</div>
	</div>
</form>
</div>
<?php
	
		$this->endsBoxPrimary();

		echo "</div>";
		echo "</div>"; 
		echo "</div>"; // Ends row

		$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>
