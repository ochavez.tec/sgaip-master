<?php
/**
 * Implementation of AddFile view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for AddFile view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_AddFile extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$enablelargefileupload = $this->params['enablelargefileupload'];
		$partitionsize = $this->params['partitionsize'];
		$maxuploadsize = $this->params['maxuploadsize'];
		header('Content-Type: application/javascript');
		if($enablelargefileupload)
			$this->printFineUploaderJs('../op/op.UploadChunks.php', $partitionsize, $maxuploadsize);
?>

$(document).ready( function() {
	/* The fineuploader validation is actually checking all fields that can contain
	 * a file to be uploaded. First checks if an alternative input field is set,
	 * second loops through the list of scheduled uploads, checking if at least one
	 * file will be submitted.
	 */
	jQuery.validator.addMethod("fineuploader", function(value, element, params) {
		uploader = params[0];
		arr = uploader.getUploads();
		for(var i in arr) {
			if(arr[i].status == 'submitted')
				return true;
		}
		return false;
	}, "<?php printMLText("js_no_file");?>");
	$("#form1").validate({
		debug: false,
		ignore: ":hidden:not(.do_validate)",
		invalidHandler: function(e, validator) {
			noty({
				text:  (validator.numberOfInvalids() == 1) ? "<?php printMLText("js_form_error");?>".replace('#', validator.numberOfInvalids()) : "<?php printMLText("js_form_errors");?>".replace('#', validator.numberOfInvalids()),
				type: 'error',
				dismissQueue: true,
				layout: 'topRight',
				theme: 'defaultTheme',
				timeout: 1500,
			});
		},
		highlight: function(e, errorClass, validClass) {
			$(e).parent().parent().removeClass(validClass).addClass(errorClass);
		},
		unhighlight: function(e, errorClass, validClass) {
			$(e).parent().parent().removeClass(errorClass).addClass(validClass);
		},
<?php
		if($enablelargefileupload) {
?>
		submitHandler: function(form) {
			userfileuploader.uploadStoredFiles();
		},
<?php
		}
?>
		rules: {
<?php
		if($enablelargefileupload) {
?>
			fineuploaderuuids: {
				fineuploader: [ userfileuploader ]
			}
<?php
		} else {
?>
			'userfile[]': {
				required: true
			}
<?php
		}
?>
		},
		messages: {
			name: "<?php printMLText("js_no_name");?>",
			comment: "<?php printMLText("js_no_comment");?>",
			'userfile[]': "<?php printMLText("js_no_file");?>"
		},
		errorPlacement: function( error, element ) {
			if ( element.is( ":file" ) ) {
				error.appendTo( element.parent().parent().parent());
			} else {
				error.appendTo( element.parent());
			}
		}
	});

	/*// -- Detect words -- //*/

  	$("#filechooser").change(function(){
       	$("#file_scan").fadeIn();

       	var data = new FormData();
		jQuery.each(jQuery('#filechooser')[0].files, function(i, file) {
		    data.append('file-'+i, file);
		});

		jQuery.ajax({
		    url: '/op/op.DetectSensitiveWords.php',
		    data: data,
		    cache: false,
		    contentType: false,
		    processData: false,
		    type: 'POST',
		    success: function(data){
		    	$("#file_scan").hide('fast');
		        if(data != "file-not-allowed" && data != false){
					$("#file_scan_response").text(data);
		        	$("#file_scan_response_two").fadeOut();
		        	$("#file_scan_response_one").fadeIn();
		        	$("#file_scan_response_three").fadeOut();
		        } else if(data == "file-not-allowed" && data != false){
		    		$("#file_scan_response_two").fadeOut();
		        	$("#file_scan_response_one").fadeOut();
		        	$("#file_scan_response_three").fadeIn();
		    	} else if(!data){
		        	$("#file_scan_response_one").fadeOut();
		    		$("#file_scan_response_two").fadeIn();
		    		$("#file_scan_response_three").fadeOut();
		    	}
		    }
		});
 	});


});
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$folder = $this->params['folder'];
		$document = $this->params['document'];
		$strictformcheck = $this->params['strictformcheck'];
		$enablelargefileupload = $this->params['enablelargefileupload'];

		$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/validate/jquery.validate.js"></script>'."\n", 'js');
		if($enablelargefileupload) {
			$this->htmlAddHeader('<script type="text/javascript" src="../styles/'.$this->theme.'/fine-uploader/jquery.fine-uploader.min.js"></script>'."\n", 'js');
			$this->htmlAddHeader($this->getFineUploaderTemplate(), 'js');
		}

		$this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();
		
		$user_root_folder_id = $this->params['user']->getHomeFolder();
		$user_root_folder = $this->params['dms']->getFolder($user_root_folder_id);

		$legal_partners = $dms->getGroup(164); // 164 = Colaboradores Jurídicos
		$secretaries = $dms->getGroup(165); // 165 = Secretarias

		if ($legal_partners->isMember($user) || $secretaries->isMember($user) || $user->isAdmin() || $document->getAccessMode($user) >= M_READWRITE) {
			echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document, $user, true);
		} else {
			echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document, $user, false);
		}

		//echo $this->getDefaultFolderPathHTML($user_root_folder, $folder, true, $document);

		//// Atach file ////
		echo "<div class=\"row\">";
		echo "<div class=\"col-md-12\">";

		?>
<div class="alert alert-warning">
<?php echo getMLText("max_upload_size").": ".ini_get( "upload_max_filesize"); ?>
<?php
	if(0 && $enablelargefileupload) {
  	printf('<p>'.getMLText('link_alt_updatedocument').'</p>', "out.AddFile2.php?documentid=".$document->getId());
	}
?>
</div>

<?php
		echo "<div class=\"box box-primary\">";
		echo "<div class=\"box-header with-border\">";
    echo "<h3 class=\"box-title\">".getMLText("linked_files")."</h3>";
    echo "</div>";
    echo "<div class=\"box-body\">";
?>

<form class="form-horizontal" action="../op/op.AddFile.php" enctype="multipart/form-data" method="post" name="form1" id="form1">
<input type="hidden" name="documentid" value="<?php print $document->getId(); ?>">
<div class="control-group">
	<label class="control-label"><?php printMLText("local_file");?>:</label>
	<div class="controls">
<?php
		if($enablelargefileupload)
			$this->printFineUploaderHtml();
		else
			$this->printFileChooser('userfile[]', false);
?>
	</div>
</div>
<br>
<div class="control-group">
<div id="file_scan" class="col-md-12" style="display: none; height: 115px;">
	<p><b><?php printMLText("checking_file"); ?></b></p>
	<div class="overlay" style="height: 50px;"><i class="fa fa-refresh fa-spin"></i></div>
</div>
<div id="file_scan_response_one" class="col-md-12" style="display: none; height: 115px;">
	<div class="alert alert-warning">	                
    <h4><i class="icon fa fa-warning"></i> <?php echo getMLText("sensitive_words_warning"); ?></h4>
    <p><?php echo getMLText("sensitive_words_msg_one"); ?><b><span id="file_scan_response"></span></b></p>
	</div>
</div>
<div id="file_scan_response_two" class="col-md-12" style="display: none; height: 115px;">
	<div class="alert alert-info">	                
    <h4><i class="icon fa fa-info"></i> <?php echo getMLText("sensitive_words_info"); ?></h4>
    <p><?php echo getMLText("sensitive_words_msg_two"); ?></p>
	</div>
</div>
<div id="file_scan_response_three" class="col-md-12" style="display: none; height: 115px;">
	<div class="alert alert-info">	                
	<h4><i class="icon fa fa-info"></i> <?php echo getMLText("sensitive_words_info"); ?></h4>
	<p><?php echo getMLText("sensitive_words_msg_three"); ?></p>
	</div>
</div>
</div>
<div class="control-group">
	<label class="control-label"><?php printMLText("version");?>:</label>
	<div class="controls">
		<select name="version" class="form-control" id="version">
		<option value=""><?= getMLText('document') ?></option>
<?php
		$versions = $document->getContent();
		foreach($versions as $version)
			echo "<option value=\"".$version->getVersion()."\">".getMLText('version')." ".$version->getVersion()."</option>";
?>
	</select></div>
</div>
<div class="control-group">
	<label class="control-label"><?php printMLText("name");?>:</label>
	<div class="controls">
		<input type="text" class="form-control" name="name" id="name" size="60">
	</div>
</div>
<div class="control-group">
	<label class="control-label"><?php printMLText("comment");?>:</label>
	<div class="controls">
		<textarea class="form-control" name="comment" id="comment" rows="4" cols="80"<?php echo $strictformcheck ? ' required' : ''; ?>></textarea>
	</div>
</div>
<?php
	if ($user->isAdmin()) {
		print "<div class=\"control-group\"><label class=\"control-label\">".getMLText("document_link_public")."</label>";
		print "<div class=\"controls\">";
		print "<input type=\"checkbox\" name=\"public\" value=\"true\" />";
		print "</div></div>";
	}
?>
<div class="control-group">
	<label class="control-label"></label>
	<div class="controls">
		<a href="<?php echo '/out/out.ViewDocument.php?documentid='.$document->getID().'&currenttab=docinfo'; ?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> <?php printMLText("cancel"); ?></a>
		<input class="btn btn-primary" type="submit" value="<?php printMLText("add");?>">
	</div>
</div>
</form>
<?php

		echo "</div>";
		echo "</div>";
		echo "</div>";
		echo "</div>";
		echo "</div>";

		$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();

	} /* }}} */
}
?>
