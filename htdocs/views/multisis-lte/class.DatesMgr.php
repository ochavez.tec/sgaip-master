<?php
/**
 * Implementation of DepMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Include class to preview documents
 */
require_once("SeedDMS/Preview.php");

/**
 * Class which outputs the html page for DepMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_DatesMgr extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$strictformcheck = $this->params['strictformcheck'];

		header("Content-type: text/javascript");
?>

$(document).ready( function() {

});
<?php
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$strictformcheck = $this->params['strictformcheck'];
		$dates = $this->params['alldaterules'];
		$extension_time= $this->params['extension_time'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		echo '<div class="gap-10"></div>';
		echo '<div class="row">';
		echo '<div class="col-md-12">';
?>
<?php $this->startBoxSolidPrimary(getMLText("dates_mgr")); ?>

<div class="col-md-6">
<?php $this->startBoxPrimary(getMLText("dates_mgr_add_date")); ?>

<form class="form-horizontal" action="../op/op.DatesMgr.php" name="form_1" id="form_1" method="post">
	<?php echo createHiddenFieldWithKey('adddaterule'); ?>
	<input type="hidden" name="action" value="adddaterule">
	<table class="table table-striped">
		<tr>
			<td width="50%" style="width: 50%;">
				<label class="control-label"><?php printMLText("dates_mgr_desc_one");?>: </label>
			</td>
			<td style="text-align: center;" width="30%" style="width: 30%;">
				<div class="controls">
				<input type="number" min="1" class="form-control" name="years" id="years" required="true">
				</div>
			</td>
			<td style="text-align: center;" width="20%" style="width: 20%;">
				<label class="control-label"><?php printMLText("dates_mgr_years");?></label>
			</td>
		</tr>
	</table>
	<table class="table table-striped">
		<tr>
			<td width="50%" style="width: 50%;">
				<label class="control-label"><?php printMLText("dates_mgr_desc_two");?>: </label>
			</td>
			<td style="text-align: center;" width="30%" style="width: 30%;">
				<div class="controls">
				<input type="number" min="1" class="form-control" name="days" id="days" required="true">
				</div>
			</td>
			<td style="text-align: center;" width="20%" style="width: 20%;">
				<label class="control-label"><?php printMLText("dates_mgr_days");?></label>
			</td>
		</tr>
	</table>

	<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <?php printMLText("save"); ?></button>
</form>



<?php $this->endsBoxPrimary(); ?>
</div>

<div class="col-md-6">
<?php $this->startBoxSuccess(getMLText("dates_mgr_added_dates")); ?>	
<table class="table table-bordered table-striped">
	<thead>
		<th style="text-align: center;"><?php printMLText("years"); ?></th>
		<th style="text-align: center;"><?php printMLText("date_mgr_to_many_days"); ?></th>
		<th style="text-align: center;"><?php printMLText("delete"); ?></th>
	</thead>
	<tbody>
		<?php if ($dates): ?>
			<?php $j = 0; ?>
			<?php foreach ($dates as $date): ?>
				<?php $j++; ?>
				<tr>
					<?php if ($j == 1): ?>
						<td style="text-align: center;"><?php echo "< " . $date['years']; ?></td>
					<?php else: ?>
						<td style="text-align: center;"><?php echo "> " . $date['years']; ?></td>
					<?php endif ?>

					<td style="text-align: center;"><?php echo $date['days'] ?></td>
					<td style="text-align: center;">
						<form style="display: inline-block;" method="post" action="../op/op.DatesMgr.php" > 
							<?php echo createHiddenFieldWithKey('removedaterule'); ?>
							<input type="hidden" name="id" value="<?php echo $date['id']; ?>">
							<input type="hidden" name="action" value="removedaterule">
							<button class="btn btn-danger" type="submit"><i class="fa fa-times"></i> <?php echo getMLText("delete")?></button>
						</form>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>		
	</tbody>
</table>


<?php $this->endsBoxSuccess(); ?>
</div>

<?php $this->endsBoxSolidPrimary(); ?>

<?php $this->startBoxSolidSuccess(getMLText("extension_time")); ?>
	<div class="col-md-6">
		<form class="form-horizontal" action="../op/op.DatesMgr.php" name="form_2" id="form_2" method="post">
			<?php if ($extension_time): ?>
				<?php echo createHiddenFieldWithKey('editextensiontime'); ?>
				<input type="hidden" name="action" value="editextensiontime">
				<input type="hidden" name="id" value="<?php echo $extension_time['id']; ?>">
			<?php else: ?>
				<?php echo createHiddenFieldWithKey('addextensiontime'); ?>
				<input type="hidden" name="action" value="addextensiontime">
			<?php endif ?>
			
			<table class="table table-striped">
				<tr>
					<td width="50%" style="width: 70%;">
						<label class="control-label"><?php printMLText("extension_time_desc");?>: </label>
					</td>
					<td style="text-align: center;" width="30%" style="width: 30%;">
						<div class="controls">
						<?php if ($extension_time): ?>
						<input type="number" min="1" class="form-control" name="extension" value="<?php echo $extension_time['extension']; ?>" required="true">	
						<?php else: ?>
						<input type="number" min="1" class="form-control" name="extension" required="true">
						<?php endif ?>
						</div>
					</td>					
				</tr>
				<tr>
					<td width="50%" style="width: 70%;">
						<label class="control-label"><?php printMLText("extension_time_desc_two");?>: </label>
					</td>
					<td style="text-align: center;" width="30%" style="width: 30%;">
						<div class="controls">
						<?php if ($extension_time): ?>
						<input type="number" min="1" class="form-control" name="days_before" value="<?php echo $extension_time['days_before']; ?>" required="true">	
						<?php else: ?>
						<input type="number" min="1" class="form-control" name="days_before" required="true">
						<?php endif ?>
						</div>
					</td>					
				</tr>
			</table>
			<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <?php printMLText("save"); ?></button>
		</form>
	</div>
	<div class="col-md-6">
	</div>
<?php $this->endsBoxSolidSuccess(); ?>

<?php
		echo "</div>";
		echo "</div>";
		echo "</div>";
		
    	$this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>