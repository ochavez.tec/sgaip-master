<?php
/**
 * Implementation of DepMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Include class to preview documents
 */
require_once("SeedDMS/Preview.php");

/**
 * Class which outputs the html page for DepMgr view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_DepartmentMgr extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$seldep = $this->params['seldep'];
		$strictformcheck = $this->params['strictformcheck'];

		header("Content-type: text/javascript");
?>
function checkForm1() {
	msg = new Array();
	
	if($("#name").val() == "") msg.push("<?php printMLText("js_no_name");?>");
<?php
	if ($strictformcheck) {
?>
	if($("#comment").val() == "") msg.push("<?php printMLText("js_no_comment");?>");
<?php
	}
?>
	if (msg != "") {
  	noty({
  		text: msg.join('<br />'),
  		type: 'error',
      dismissQueue: true,
  		layout: 'topRight',
  		theme: 'defaultTheme',
			_timeout: 1500,
  	});
		return false;
	} else
		return true;
}

function checkForm2() {
	msg = "";
	
		if($("#instrumentid").val() == -1) msg += "<?php printMLText("js_select_instrument");?>\n";

		if (msg != "") {
			noty({
				text: msg,
				type: 'error',
				dismissQueue: true,
				layout: 'topRight',
				theme: 'defaultTheme',
				_timeout: 1500,
			});
			return false;
		} else
			return true;
	}

$(document).ready( function() {
	$('body').on('submit', '#form_1', function(ev){
		if(checkForm1())
			return;
		ev.preventDefault();
	});

	$('body').on('submit', '#form_2', function(ev){
		if(checkForm2())
			return;
		ev.preventDefault();
	});

	$( "#selector" ).change(function() {
		$('div.ajax').trigger('update', {depid: $(this).val()});
	});
});
<?php
	} /* }}} */

	function info() { /* {{{ */
		$dms = $this->params['dms'];
		$seldep = $this->params['seldep'];
		$cachedir = $this->params['cachedir'];
		$previewwidth = $this->params['previewWidthList'];
		$workflowmode = $this->params['workflowmode'];
		$timeout = $this->params['timeout'];

		if($seldep) {
			$previewer = new SeedDMS_Preview_Previewer($cachedir, $previewwidth, $timeout);
			$this->startBoxCollapsableInfo(getMLText("dep_info"));
			echo "<table class=\"table table-bordered table-striped\">\n";
/*
			if($workflowmode == "traditional") {
				$reviewstatus = $seldep->getReviewStatus();
				$i = 0;
				foreach($reviewstatus as $rv) {
					if($rv['status'] == 0) {
						$i++;
					}
				}
			}
			if($workflowmode == "traditional" || $workflowmode == 'traditional_only_approval') {
				echo "<tr><td>".getMLText('pending_reviews')."</td><td>".$i."</td></tr>";
				$approvalstatus = $seldep->getApprovalStatus();
				$i = 0;
				foreach($approvalstatus as $rv) {
					if($rv['status'] == 0) {
						$i++;
					}
				}
				echo "<tr><td>".getMLText('pending_approvals')."</td><td>".$i."</td></tr>";
			}
			if($workflowmode == 'advanced') {
				$workflowStatus = $seldep->getWorkflowStatus();
				if($workflowStatus)
					echo "<tr><td>".getMLText('pending_workflows')."</td><td>".count($workflowStatus)."</td></tr>\n";
			}
*/
			echo "</table>";
			$this->endsBoxCollapsableInfo();
		}
	} /* }}} */

	function showDepForm($dep) { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['group']))
			$instrument = $this->params['group'];
		$allInstruments = $this->params['allgroups'];
		$deps = $this->params['alldeps'];
?>
	<form class="form-horizontal" action="../op/op.DepartmentMgr.php" name="form_1" id="form_1" method="post">
<?php
		if($dep) {
			echo createHiddenFieldWithKey('editdep');
?>
	<input type="hidden" name="depid" value="<?php print $dep->getID();?>">
	<input type="hidden" name="action" value="editdep">
<?php
		} else {
			echo createHiddenFieldWithKey('adddep');
?>
	<input type="hidden" name="action" value="adddep">
<?php
		}
?>

<?php
		if($dep) {
?>
		<div class="control-group">
			<div class="controls">
				<a href="../out/out.RemoveDepartment.php?depid=<?php print $dep->getID();?>" class="btn btn-danger"><i class="fa fa-times"></i> <?php printMLText("rm_dep");?></a>
			</div>
		</div>
<?php
		}
?>
		<div class="control-group">
			<label class="control-label"><?php printMLText("name");?>:</label>
			<div class="controls">
				<input type="text" class="form-control" name="name" id="name" value="<?php print $dep ? htmlspecialchars($dep->getName()) : '';?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php printMLText("code");?>:</label>
			<div class="controls">
				<input type="text" class="form-control" name="code" id="code" value="<?php print $dep ? htmlspecialchars($dep->getCode()) : '';?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php printMLText("comment");?>:</label>
			<div class="controls">
				<textarea name="comment" class="form-control" id="comment" rows="4" cols="50"><?php print $dep ? htmlspecialchars($dep->getComment()) : '';?></textarea>
			</div>
		</div>
		<div class="control-group">
			<br>
			<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save")?></button>
		</div>

	</form>

<?php
	} /* }}} */

	function form() { /* {{{ */
		$seldep = $this->params['seldep'];

		$this->showDepForm($seldep);
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		if(isset($this->params['instrument']))
			$instrument = $this->params['instrument'];
		$seldep = $this->params['seldep'];
		$allGroups = $this->params['allgroups'];
		$allDeps = $this->params['alldeps'];
		$strictformcheck = $this->params['strictformcheck'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
    <div class="gap-10"></div>
    <div class="row">
    <div class="col-md-12">
<?php 
	$this->startBoxPrimary(getMLText("dep_management"));
?>

<div class="well">
<form class="form-horizontal">
	<div class="control-group">
		<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
		<div class="controls">
<select class="chzn-select form-control" id="selector">
<option value="-1"><?php echo getMLText("choose_dep")?></option>
<option value="0"><?php echo getMLText("add_dep")?></option>
<?php
		foreach ($allDeps as $dep) {
			print "<option value=\"".$dep->getID()."\" ".($seldep && $dep->getID()==$seldep->getID() ? 'selected' : '').">" . htmlspecialchars($dep->getName()) . "</option>";
		}
?>
</select>
		</div>
	</div>
</form>
</div>
<div class="ajax" data-view="DepartmentMgr" data-action="info" <?php echo ($seldep ? "data-query=\"depid=".$seldep->getID()."\"" : "") ?>></div>


<div class="span12">
	<div class="well">
		<div class="ajax" data-view="DepartmentMgr" data-action="form" <?php echo ($seldep ? "data-query=\"depid=".$seldep->getID()."\"" : "") ?>></div>
	</div>
</div>


<?php
		$this->endsBoxPrimary();

		echo "</div>";
		echo "</div>";
		echo "</div>";
		
    $this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();
	} /* }}} */
}
?>