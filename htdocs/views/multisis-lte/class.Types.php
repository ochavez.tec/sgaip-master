<?php
/**
 * Implementation of Types view
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for Types view
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010-2012 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_View_Types extends SeedDMS_Bootstrap_Style {

	function js() { /* {{{ */
		$seltype = $this->params['seltype'];
		header('Content-Type: application/javascript');
?>
$(document).ready( function() {
	$( "#selector" ).change(function() {
		if($(this).val() != -1){
			$('div.ajax').trigger('update', {typeid: $(this).val()});
		}		
	});
});
<?php
	} /* }}} */

	function info() { /* {{{ */
		$dms = $this->params['dms'];
		$seltype = $this->params['seltype'];

		if($seltype) {
			$this->startBoxCollapsableInfo(getMLText("type_info"));
			$documents = $seltype->getDocumentsByType();
			echo "<table class=\"table table-bordered table-condensed\">\n";
			echo "<tr><td>".getMLText('document_count')."</td><td>".(count($documents))."</td></tr>\n";
			echo "</table>";
			$this->endsBoxCollapsableInfo();
		}
	} /* }}} */

	function showTypeForm($type) { /* {{{ */
		$dms = $this->params['dms'];
?>
				<div class="control-group">
					<label class="control-label"></label>

					<div class="controls">
<?php
		if($type) {
			if($type->isUsed()) {
?>
						<p><?php $this->warningMsg(getMLText('type_in_use')); ?></p>
<?php
			} else {
?>
						<form style="display: inline-block;" method="post" action="../op/op.Types.php" >
						<?php echo createHiddenFieldWithKey('removetype'); ?>
						<input type="hidden" name="typeid" value="<?php echo $type->getID()?>">
						<input type="hidden" name="action" value="removetype">
						<button class="btn btn-danger" type="submit"><i class="fa fa-times"></i> <?php echo getMLText("rm_document_type")?></button>
						</form>
<?php
			}
		}
?>
					</div>
				</div>
				<br>
				<form class="form-horizontal" style="margin-bottom: 0px;" action="../op/op.Types.php" method="post">
				<?php if(!$type) { ?>
					<?php echo createHiddenFieldWithKey('addtype'); ?>
					<input type="hidden" name="action" value="addtype">
				<?php } else { ?>
					<?php echo createHiddenFieldWithKey('edittype'); ?>
					<input type="hidden" name="action" value="edittype">
					<input type="hidden" name="typeid" value="<?php echo $type->getID()?>">
				<?php } ?>
				<div class="control-group">
					<label class="control-label"><?php echo getMLText("name")?>:</label>
					<div class="controls">
							<input required name="name" class="form-control" type="text" value="<?php echo $type ? htmlspecialchars($type->getName()) : '' ?>">&nbsp;
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><?php echo getMLText("codeMask")?>:</label>
					<div class="controls">
							<input required name="codeMask" class="form-control" type="text" value="<?php echo $type ? htmlspecialchars($type->getCodeMask()) : '' ?>">&nbsp;
					</div>
				</div>
					<div class="control-group">
						<label>
							<?php printMLText("Categories");?>:
						</label>
						<select class="form-control chzn-select" name="categories[]" multiple="multiple" data-no_results_text="<?php printMLText('unknown_type_category'); ?>">
							<?php 
								$categories = $dms->getDocumentCategories();
								if(isset($type)) 
									$cat_list = $type->getCategories(); 
								else
									$cat_list = array();
								foreach($categories as $category) {
									echo "<option value=\"".$category->getID()."\"";
									if(in_array($category, $cat_list))
										echo " selected";
									echo ">".$category->getName()."</option>";	
								}
							?>
						</select>
					</div>
	          <div class="control-group">
	            <label class="control-label"><?php printMLText("instrumentvar")?>:</label>
	            <div class="controls">	    
	            <select class="form-control chzn-select" multiple="multiple" name="instvars[]" id="instvar-selector" data-no_results_text="<?php printMLText('unknown_instrumentvar'); ?>">
							<?php
								$instvars = $dms->getInstrumentVars();
								$count = 1;
								$default_instvar = 0;
								foreach($instvars as $instvar) {
									if($instvar->getIsManual() === '0') {
										if($count==1 && ($default_instvar == 0)) $default_instvar = $instvar->getID();
										echo "<option value=\"".$instvar->getID()."\"";
										if($type != null) { 
											if(in_array($instvar, $type->getInstrumentVars())) {
												echo " selected";
											}
										}
										echo ">".$instvar->getCode()."-".$instvar->getName()."</option>";	
										$count++;
									}
								}
							?>
				</select>
	            </div>
	          </div>
	          <div class="control-group">
	            <label class="control-label"><?php printMLText("checklist_item")?>:</label>
	            <div class="controls">
	            <select class="form-control chzn-select" multiple="multiple" name="items[]" id="item-selector" data-no_results_text="<?php printMLText('no_results'); ?>">
							<?php
								$items = $dms->getAllThematicItemsAuto();
								$count = 1;
								$default_item = 0;
								foreach($items as $item) {
									if($count==1 && ($default_item == 0)) $default_item = (int)$item['id'];
									echo "<option value=\"".$item['id']."\"";
									if($type != null) { 
										if(in_array($item, $dms->getItemsByType($type->getID()))) {
											echo " selected";
										}
									}
									echo ">".$item['name']."</option>";	
									$count++;
								}
							?>
				</select>
	            </div>
	          </div>
	          <div  class="control-group">&nbsp;</div>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save");?></button>
					</div>
				</div>
				</form>

<?php
	} /* }}} */

	function form() { /* {{{ */
		$dms = $this->params['dms'];
		$seltype = $this->params['seltype'];

		$this->showTypeForm($seltype);
	} /* }}} */

	function show() { /* {{{ */
		$dms = $this->params['dms'];
		$user = $this->params['user'];
		$types = $this->params['types'];
		$information_types = $this->params['information_types'];
		$resolution_types = $this->params['resolution_types'];
		$seltype = $this->params['seltype'];

		$this->htmlStartPage(getMLText("admin_tools"), "skin-blue sidebar-mini");
		$this->containerStart();
		$this->mainHeader();
		$this->mainSideBar();
		$this->contentStart();

		?>
    <div class="gap-10"></div>
    <div class="row">
    <div class="col-md-12">
    <?php 

		$this->startBoxPrimary(getMLText("global_document_types"));
?>
<div class="row-fluid">
	<div class="span12">
		<div class="well">
<form class="form-horizontal">
	<div class="control-group">
		<label class="control-label" for="login"><?php printMLText("selection");?>:</label>
		<div class="controls">
			<select id="selector" class="form-control">
				<option value="-1"><?php echo getMLText("choose_type")?>
				<option value="0"><?php echo getMLText("new_document_type")?>
<?php
				foreach ($types as $type) {
					print "<option value=\"".$type->getID()."\" ".($seltype && $type->getID()==$seltype->getID() ? 'selected' : '').">" . htmlspecialchars($type->getName());
				}
?>
			</select>
		</div>
	</div>
</form>
		</div>
		<div class="ajax" data-view="Types" data-action="info" <?php echo ($seltype ? "data-query=\"typeid=".$seltype->getID()."\"" : "") ?>></div>
	</div>

	<div class="span12">
		<div class="well">
			<div class="ajax" data-view="Types" data-action="form" <?php echo ($seltype ? "data-query=\"typeid=".$seltype->getID()."\"" : "") ?>></div>

		</div>
	</div>
</div>
	
<?php
		$this->endsBoxPrimary();


		$this->startBoxPrimary(getMLText("information_type_requested"));


?>

<div class="row-fluid">
	<div class="col-md-4">
		<div class="box box-success box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo getMLText("add_information_type_requested"); ?></h3>				
			</div>
			<div class="box-body">
				<form class="form-horizontal" style="margin-bottom: 0px;" action="../op/op.Types.php" method="post">
				<?php echo createHiddenFieldWithKey('addinformationtyperequested'); ?>
				<input type="hidden" name="action" value="addinformationtyperequested">				
				<div class="control-group">
					<label class="control-label"><?php echo getMLText("information_type_requested")?>:</label>
					<div class="controls">
						<input name="information_type_requested" class="form-control" type="text" required="required">
					</div>
				</div>
	          	<br>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save");?></button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
	
	<div class="col-md-8">
		<div class="box box-info box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo getMLText("information_type_requested"); ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body table-responsive">
				<table id="add_information_type_requested_table" class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th style="text-align: center;" width="10%"><?php echo getMLText("id"); ?></th>
						<th style="text-align: center;" width="60%"><?php echo getMLText("information_type_requested"); ?></th>
						<th style="text-align: center;" width="30%"><?php echo getMLText("action"); ?></th>
					</tr>		
				</thead>					
				<tbody>
			<?php 
				if (!empty($information_types)) {
				foreach ($information_types as $information_type) { ?>
					<tr class="tr-hi">
						<td style="text-align: center;" ><?php echo $information_type['id']; ?></td>
						<td><?php echo $information_type['name']; ?></td>
						<td>
							<form style="display: inline-block;" method="post" action="../op/op.Types.php" > 
							<?php echo createHiddenFieldWithKey('removeinformationtype'); ?>
							<input type="hidden" name="information_type_id" value="<?php echo $information_type['id'] ?>">
							<input type="hidden" name="action" value="removeinformationtype">
							<button class="btn btn-danger" type="submit"><i class="fa fa-times"></i></button>
							</form>
							<a class="btn btn-success" type="button" href="/out/out.EditInformationType.php?informationtype=<?php echo $information_type['id']; ?>"><i class="fa fa-pencil"></i></a>
						</td>
					</tr>

				<?php } 

				}
			 ?>
			</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

	
<?php
		$this->endsBoxPrimary();

		$this->startBoxPrimary(getMLText("resolution_type"));


?>

<div class="row-fluid">
	<div class="col-md-4">
		<div class="box box-success box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo getMLText("add_resolution_type"); ?></h3>				
			</div>
			<div class="box-body">
				<form class="form-horizontal" style="margin-bottom: 0px;" action="../op/op.Types.php" method="post">
				<?php echo createHiddenFieldWithKey('addresolutiontype'); ?>
				<input type="hidden" name="action" value="addresolutiontype">				
				<div class="control-group">
					<label class="control-label"><?php echo getMLText("resolution_type")?>:</label>
					<div class="controls">
						<input name="resolution_type_name" class="form-control" type="text" required="required">
					</div>
				</div>
				<br/>
				<div class="control-group">
					<label class="control-label"><?php echo getMLText("comment")?>:</label>
					<div class="controls">
						<input name="resolution_type_comment" class="form-control" type="text" required="required">
					</div>
				</div>
	          	<br/>
				<div class="control-group">
					<label class="control-label"><?php echo getMLText("abbreviation")?>:</label>
					<div class="controls">
						<input name="resolution_type_abbreviation" class="form-control" type="text" required="required">
					</div>
				</div>				
				<br/>
				<div class="control-group">
					<label class="control-label"><?php echo getMLText("make_memo")?>:</label>
					<div class="controls">
						<input name="make_memo" type="checkbox" value="1" />
					</div>
				</div>
				<br/>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> <?php printMLText("save");?></button>
					</div>
				</div>
				<br/>
				</form>
			</div>
		</div>
	</div>
	
	<div class="col-md-8">
		<div class="box box-info box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo getMLText("resolution_type"); ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body table-responsive">
				<table id="resolution_type_table" class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th style="text-align: center;" width="10%"><?php echo getMLText("id"); ?></th>
						<th style="text-align: center;" width="10%"><?php echo getMLText("abbreviation"); ?></th>
						<th style="text-align: center;" width="10%"><?php echo getMLText("make_memo"); ?></th>
						<th style="text-align: center;" width="20%"><?php echo getMLText("resolution_type"); ?></th>
						<th style="text-align: center;" width="30%"><?php echo getMLText("comment"); ?></th>
						<th style="text-align: center;" width="20%"><?php echo getMLText("action"); ?></th>
					</tr>		
				</thead>					
				<tbody>
			<?php 
				if (!empty($resolution_types)) {
				foreach ($resolution_types as $resolution_type) { ?>
					<tr class="tr-hi">
						<td style="text-align: center;" ><?php echo $resolution_type['id']; ?></td>
						<td style="text-align: center;" ><?php echo $resolution_type['abbreviation']; ?></td>
						<td style="text-align: center;" ><?php echo ((int)$resolution_type['make_memo'] == 1) ? getMLText('yes') : getMLText('no'); ?></td>
						<td><?php echo $resolution_type['name']; ?></td>
						<td><?php echo $resolution_type['comment']; ?></td>
						<td>
							<form style="display: inline-block;" method="post" action="../op/op.Types.php" > 
							<?php echo createHiddenFieldWithKey('removeresolutiontype'); ?>
							<input type="hidden" name="resolution_type_id" value="<?php echo $resolution_type['id'] ?>">
							<input type="hidden" name="action" value="removeresolutiontype">
							<button class="btn btn-danger" type="submit"><i class="fa fa-times"></i></button>
							</form>
							<a class="btn btn-success" type="button" href="/out/out.EditResolutionType.php?resolutiontypeid=<?php echo $resolution_type['id']; ?>"><i class="fa fa-pencil"></i></a>
						</td>
					</tr>

				<?php } 

				}
			 ?>
			</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<?php
		$this->endsBoxPrimary();

		$this->startBoxPrimary(getMLText("document_initials"));
		$initials = $dms->getDocumentInitials();
?>
<div class="row-fluid">
	<div class="col-md-12">
		<div class="box box-info box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo getMLText("document_initials"); ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body table-responsive">
				<table id="resolution_type_table" class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th style="text-align: center;" width="10%"><?php echo getMLText("id"); ?></th>
						<th style="text-align: center;" width="50%"><?php echo getMLText("initials"); ?></th>
						<th style="text-align: center;" width="20%"><?php echo getMLText("type"); ?></th>
						<th style="text-align: center;" width="20%"><?php echo getMLText("action"); ?></th>
					</tr>		
				</thead>					
				<tbody>
			<?php 
				if (!empty($initials)) {
				foreach ($initials as $initial) { ?>
					<tr class="tr-hi">
						<td style="text-align: center;" ><?php echo $initial['id']; ?></td>
						<td style="text-align: center;" ><?php echo $initial['initials']; ?></td>
						<td style="text-align: center;" ><?php echo $initial['doctype']; ?></td>
						<td>
						<a class="btn btn-success" type="button" href="/out/out.EditInitial.php?initialid=<?php echo $initial['id']; ?>"><i class="fa fa-pencil"></i> <?php echo getMLText("edit")?></a>
						</td>
					</tr>
				<?php }
				}
			 ?>
			</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<?php
		$this->endsBoxPrimary();

		echo "</div>";
		echo "</div>";
		echo "</div>";
		
    $this->contentEnd();
		$this->mainFooter();		
		$this->containerEnd();
		$this->htmlEndPage();	
	} /* }}} */
}
?>
