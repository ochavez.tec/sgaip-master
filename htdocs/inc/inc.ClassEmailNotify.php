<?php
/**
 * Implementation of notifation system using email
 *
 * @category   DMS
 * @package    SeedDMS
 * @license    GPL 2
 * @version    @version@
 * @author     Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010 Uwe Steinmann
 * @version    Release: @package_version@
 */

/**
 * Include parent class
 */
require_once("inc.ClassNotify.php");
require_once("inc.Settings.php");
require_once("Mail.php");
require_once('swiftmailer/lib/swift_required.php');

/**
 * Class to send email notifications to individuals or groups
 *
 * @category   DMS
 * @package    SeedDMS
 * @author     Markus Westphal, Malcolm Cowe, Uwe Steinmann <uwe@steinmann.cx>
 * @copyright  Copyright (C) 2002-2005 Markus Westphal,
 *             2006-2008 Malcolm Cowe, 2010 Matteo Lucarelli,
 *             2010 Uwe Steinmann
 * @version    Release: @package_version@
 */
class SeedDMS_EmailNotify extends SeedDMS_Notify {
	/**
	 * Instanz of DMS
	 */
	protected $_dms;

	protected $smtp_server;

	protected $smtp_port;

	protected $smtp_user;

	protected $smtp_password;

	protected $from_address;

	function __construct($dms, $from_address='', $smtp_server='', $smtp_port='', $smtp_username='', $smtp_password='') { /* {{{ */
		$this->_dms = $dms;
		$this->smtp_server = $smtp_server;
		$this->smtp_port = $smtp_port;
		$this->smtp_user = $smtp_username;
		$this->smtp_password = $smtp_password;
		$this->from_address = $from_address;
	} /* }}} */

	/**
	 * Send mail to individual user
	 *
	 * @param mixed $sender individual sending the email. This can be a
	 *        user object or a string. If it is left empty, then
	 *        $this->from_address will be used.
	 * @param object $recipient individual receiving the mail
	 * @param string $subject key of string containing the subject of the mail
	 * @param string $message key of string containing the body of the mail
	 * @param array $params list of parameters which replaces placeholder in
	 *        the subject and body
	 * @return false or -1 in case of error, otherwise true
	 */
	function toIndividual($sender, $recipient, $subject, $message, $params=array()) { /* {{{ */
		global $settings;

		if(is_object($recipient) && !strcasecmp(get_class($recipient), $this->_dms->getClassname('user')) && !$recipient->isDisabled() && $recipient->getEmail()!="") {
			$to = $recipient->getEmail();
			$to_name = $recipient->getFullName();
			$lang = $recipient->getLanguage();
		} elseif(is_string($recipient) && trim($recipient) != "") {
			$to = $recipient;
			$to_name = "DMS Sender";
			if(isset($params['__lang__']))
				$lang = $params['__lang__'];
			else
				$lang = 'en_GB';
		} else {
			return false;
		}

		$returnpath = '';
		if(is_object($sender) && !strcasecmp(get_class($sender), $this->_dms->getClassname('user'))) {
			$from = $sender->getFullName() ." <". $sender->getEmail() .">";
			if($this->from_address)
				$returnpath = $this->from_address;
		} elseif(is_string($sender) && trim($sender) != "") {
			$from = $sender;
			if($this->from_address)
				$returnpath = $this->from_address;
		} else {
			$from = $this->from_address;
		}


		/*$message = getMLText("email_header", array(), "", $lang)."\r\n\r\n".getMLText($message, $params, "", $lang);
		$message .= "\r\n\r\n".getMLText("email_footer", array(), "", $lang);*/

		$html = $this->renderMessage($params, $subject, $message, $lang, $settings->_siteName);

		$headers = array ();
		$headers['From'] = $from;
		if($returnpath)
			$headers['Return-Path'] = $returnpath;
		$headers['To'] = $to;
		$preferences = array("input-charset" => "UTF-8", "output-charset" => "UTF-8");
		$encoded_subject = iconv_mime_encode("Subject", getMLText($subject, $params, "", $lang), $preferences);
		$headers['Subject'] = substr($encoded_subject, strlen('Subject: '));
		$headers['MIME-Version'] = "1.0";
		$headers['Content-type'] = "text/plain; charset=utf-8";

		$mail_params = array();
		if($this->smtp_server) {

			$mail_params['host'] = $this->smtp_server;
			if($this->smtp_port) {
				$mail_params['port'] = $this->smtp_port;
			}
			if($this->smtp_user) {
				$mail_params['auth'] = true;
				$mail_params['username'] = $this->smtp_user;
				$mail_params['password'] = $this->smtp_password;
			}

			$transport = (new \Swift_SmtpTransport($this->smtp_server, $this->smtp_port, 'ssl'))
            ->setUsername($this->smtp_user)
            ->setPassword($this->smtp_password);

		    $mailer = new \Swift_Mailer($transport);

		    $thesubject = getMLText($subject);

			/* The sender always must be a static global address for all the system */
			$server_name = str_replace("", "www.", strtolower($_SERVER['SERVER_NAME']));

			if ($server_name == "") {
				$sender_email = "dms-sender@multisisdms.com";	
			} else {
				$sender_email = "dms-sender@".$server_name;	
			}

	        $themessage = new \Swift_Message;
	        $themessage->setSubject($thesubject);
	        //$themessage->setFrom(['email-sender@multisisdms.com' => $settings->_siteName]);
	        $themessage->setFrom([$sender_email => $settings->_siteName]);
	        			
			try {
			    $themessage->setTo([$to => $to_name]);
			} catch(\Swift_RfcComplianceException $e) {
			    echo "Address ".$to." seems invalid";
			}

			//$themessage->addPart($html);
			$themessage->setBody($html, 'text/html');

			try {
			    $result = $mailer->send($themessage);
			} catch (\Swift_TransportException $Ste) {
			    echo $Ste;
			}

		} else {

			$result = Mail::factory('mail', $mail_params);
		}

		// If result == 0, then any email was sent

		if (PEAR::isError($result)) {
			return false;
		} else {
			return true;
		}
	} /* }}} */

	function toGroup($sender, $groupRecipient, $subject, $message, $params=array()) { /* {{{ */
		if ((!is_object($sender) && strcasecmp(get_class($sender), $this->_dms->getClassname('user'))) ||
				(!is_object($groupRecipient) || strcasecmp(get_class($groupRecipient), $this->_dms->getClassname('group')))) {
			return -1;
		}

		foreach ($groupRecipient->getUsers() as $recipient) {
			$this->toIndividual($sender, $recipient, $subject, $message, $params);
		}

		return true;
	} /* }}} */

	function toList($sender, $recipients, $subject, $message, $params=array()) { /* {{{ */
		if ((!is_object($sender) && strcasecmp(get_class($sender), $this->_dms->getClassname('user'))) ||
				(!is_array($recipients) && count($recipients)==0)) {
			return -1;
		}

		foreach ($recipients as $recipient) {
			$this->toIndividual($sender, $recipient, $subject, $message, $params);
		}

		return true;
	} /* }}} */

	function renderMessage($params, $subject, $message, $lang, $site_name){ /* {{{ */
		$html = "";

		$html .= '<!DOCTYPE html>
				<html><head><title>DMS Notification</title>
			   	</head><body style="font-family:Helvetica,Verdana,sans-serif;"><div style="width:100%;margin:0;padding:0;background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" id="m_4317075561180000941backgroundTable" style="height:auto!important;margin:0;padding:0;width:100%!important;background-color:#f0f0f0;color:#222222;font-family:Helvetica,Verdana,sans-serif;font-size:14px;line-height:19px;margin-top:0;padding:0;font-weight:normal">
            	<tbody><tr><td><div id="m_4317075561180000941tablewrap" style="width:100%!important;max-width:600px!important;text-align:center;margin:0 auto">
                <table id="m_4317075561180000941contenttable" width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="background-color:#ffffff;margin:0 auto;text-align:center;border:none;width:100%!important;max-width:600px!important;border-top: 8px solid #0a69b5;border-bottom:8px solid #0a69b5;border-left: 2px solid #0a69b5;border-right: 2px solid #0a69b5">
                <tbody><tr><td width="100%"><table bgcolor="#FFF" border="0" cellspacing="10" cellpadding="0" width="100%" style="border-top: 3px solid #4CAF50;"><tbody><tr>
                <td width="100%" bgcolor="#ffffff" style="text-align:center"><p style="color:#222222;font-family:Helvetica,Verdana,sans-serif;font-size:9px;line-height:14px;margin-top:0;padding:0;font-weight:normal;">
                '.getMLText("email_header").'
                </p></td></tr><tr><td align="left" valign="top" style="line-height:22px;font:14px">
                <b>Asunto:&nbsp;</b>'.getMLText($subject).' <br>
                <b>Sistema DMS:&nbsp;</b>'.$site_name.'</td>
                </tr></tbody></table><table bgcolor="#F0F0F0" border="0" cellspacing="0" cellpadding="10" width="100%" style="border-top:2px solid #f0f0f0;margin-top:5px;text-align:left"><tbody><tr><td width="100%" bgcolor="#ffffff" style="text-align:left;font:14px">
                <p><b>Información:</b></p>
                <div>'.getMLText($message, $params, "", $lang).'</div>
                <p></p></td></tr></tbody></table><table bgcolor="#F0F0F0" border="0" cellspacing="0" cellpadding="10" width="100%" style="border-top:2px solid #f0f0f0;margin-top:10px;text-align:center"><tbody>
                <tr><td width="100%" bgcolor="#ffffff" style="font:14px"><div style="text-align: center;width: 100%;">';
                
                if (isset($params["url"]) && $params["url"] != "") {
                	$html .= '<a style="background:#0a69b5;padding: 10px;border-radius: 4px;text-decoration: none;color: #ffffff;display: block;" type="button" target="_blank" href="'.$params["url"].'">'.getMLText("open_link").'</a>';
                }                

        $html .= '</div></td></tr></tbody>
                </table><table bgcolor="#F0F0F0" border="0" cellspacing="0" cellpadding="10" width="100%" style="border-top:2px solid #f0f0f0;margin-top:5px;border-bottom:3px solid #4CAF50"><tbody>
                <tr><td width="100%" bgcolor="#ffffff" style="text-align:center"><p style="color:#222222;font-family:Helvetica,Verdana,sans-serif;font-size:11px;line-height:14px;margin-top:0;padding:0;font-weight:normal;padding-top:5px">'.getMLText("email_footer").'</p>
                </td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table></div></body></html>';

		return $html;
	} /* }}} */
}
?>
