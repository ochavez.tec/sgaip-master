<?php
define("SEEDDMS_INSTALL", "on");
include("../inc/inc.Settings.php");
$settings = new Settings();
$rootDir = realpath ("..");
$settings->_rootDir = $rootDir.'/';

$theme = "multisis-lte";
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

$ui = new UI($theme);
$ui->htmlStartPage("INSTALL", "skin-blue layout-top-nav");
$ui->globalBanner();
$ui->contentStart();
$ui->contentHeading("Instalación de Multisistemas DMS");
$ui->contentContainerStart();

echo "<h2>".getMLText('settings_install_welcome_title')."</h2>";
echo "<div style=\"width: 600px;\">".getMLText('settings_install_welcome_text')."</div>";
echo '<p><a type="button" class="btn btn-primary" href="install.php">' . getMLText("settings_start_install") . '</a></p>';

$UI->contentContainerEnd();
$UI->contentEnd();
$UI->htmlEndPage();
?>
